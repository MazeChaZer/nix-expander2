module Tcl where


struct TclCmd =
  doNothing	:: Action
  package	:: Template TclCmdPackage
  clock		:: Template TclCmdClock
  source_	:: String -> Action
  source	:: String -> Request String
  --set_		:: String -> String -> Action
  proc_		:: [String] -> [String] -> [String] -> Action
  proc		:: [String] -> [String] -> [String] -> Request String

struct TclCmdPackage = 
  forget_	:: String -> Action
  ifneeded_	:: String -> Action
  names_	:: Action
  provide_	:: String -> Action
  require_	:: String -> Action
  unknown_	:: String -> Action
  vcompare_	:: String -> Action
  versions_	:: String -> Action
  vsatisfies_	:: String -> Action
  
struct TclCmdClock =
  add		:: String -> Action
  clicks	:: String -> Request Integer
  format	:: String -> Action
  microseconds	:: Action
  scan		:: String -> Action
  seconds	:: Action
  
tclCmd	  :: Template TclCmd
tclCmd    =
   template in
   let 	
   	doNothing	= primExTcl_ [""]
   	package 	= tclCmdPackage
   	clock		= tclCmdClock
   	source_	nm 	= primExTcl_ ["source",nm]
   	source nm	= request
         	res <- primExecuteTcl nm
         	return res
        {-
        proc procName { args1 args2 ... } { cmds1
        cmds2
        ...
        }
        -}
        --set_ varName value		= primExTcl_ ["set",varName,value]
        proc_ procName args cmds	= primExTcl_ ["proc",head procName,"{", unwords args, "} {", unlines cmds,"}"]
        proc procName args cmds		= request
				primExTcl ["proc",head procName,"{", unwords args, "} {", unlines cmds,"}"]
				return (head procName)
        					
   in struct .. TclCmd


tclCmdPackage :: Template TclCmdPackage
tclCmdPackage  = 
    template 
    in struct
      forget_	nm	= primExTcl_ ["package forget",nm]
      ifneeded_	nm	= primExTcl_ ["package ifneeded",nm]
      names_		= primExTcl_ ["package names"]
      provide_	nm	= primExTcl_ ["package provide",nm]
      require_	nm	= primExTcl_ ["package require",nm]
      unknown_	nm	= primExTcl_ ["package unknown",nm]
      vcompare_	nm	= primExTcl_ ["package vcompare",nm]
      versions_	nm	= primExTcl_ ["package versions",nm]
      vsatisfies_ nm	= primExTcl_ ["package vsatisfies",nm]
      
      
tclCmdClock :: Template TclCmdClock
tclCmdClock =
	template
	in struct
		add nm		= primExTcl_ ["clock add",nm]
		clicks nm 	= request
         				res <- primExTcl ["clock clicks",nm]
         				return (read res)
		format nm	= primExTcl_ ["clock format",nm]
		microseconds	= primExTcl_ ["clock microseconds"]
		scan nm		= primExTcl_ ["clock scan",nm]
		seconds		= primExTcl_ ["clock seconds"]

primExTcl  = primExecuteTcl . unwords
primExTcl_ = primExecuteTcl_ . unwords


primitive primTclDebug "primTclDebug" :: Bool -> Action
primitive primInitTcl "primInitTcl" :: Request Bool
primitive primRunTcl "primRunTcl" :: Request Bool
primitive primExecuteTcl "primExecuteTcl" :: String -> Request String
primitive primExecuteTcl_ "primExecuteTcl_" :: String -> Action
primitive primSetVar "primSetVar" :: String -> String -> Action
primitive primGetTcl "primGetTcl" :: Request String
primitive primGetPath "primGetPath" :: Request String
primitive primAddCallBack "primAddCallBack" :: (String -> Cmd ()) -> Request Int
primitive primNextCallBack "primNextCallBack" :: Request Int
