module Fractal where

import Tk
import TkUtil
import Random

-- Drawing fractal images using
-- iterated functional systems a la Barnsley.

-- First some two-dimensional linear algebra

type Vector2 = [Float]   -- with two elements
type Matrix2 = [[Float]] -- 2 x 2 !!

type Affine = (Matrix2,Vector2)

vsum :: Vector2 -> Vector2 -> Vector2
vsum = zipWith (+)

scalprod :: Vector2 -> Vector2 -> Float
scalprod v1 v2 = sum (zipWith (*) v1 v2)

mul :: Matrix2 -> Vector2 -> Vector2
mul a v = map (scalprod v) a

mulM :: Matrix2 -> Matrix2 -> Matrix2
mulM m1 m2 = map (mul m1) m2

det :: Matrix2 -> Float
det[[a11,a12],[a21,a22]] = a11*a22 - a21*a12

affineMap :: Affine -> Vector2 -> Vector2
affineMap (a,v) x = vsum (mul a x) v

scale k = [[k,0],[0,k]]

rot v = [[c,-s],[s,c]]
          where c = cos v
                s = sin v
 

-- Now iterated functional systems and a simple structure for 
-- iterating an initial point through the system by randomly 
-- selecting one of the maps. 

type IFS = [Affine]

struct Fract =
  setIFS :: IFS -> Action
  draw   :: Action

fract :: RandomGenerator Float -> TkEnv -> Photo -> Label -> Template Fract
fract gen tk img lab = 
  template 
    wifs     := []
    pt       := [0,0]
    npts     := 0
  in let setIFS ifs = action 
           img.blank
           let dets = map ((+0.05) . abs . det . fst) ifs
               detsum = sum dets
               weights = map (/detsum) (tail(sums dets))
               sums xs = sums' 0 xs where
                    sums' s [] = [s]
                    sums' s (z:zs)= s:sums' (s+z) zs
           wifs := zip weights ifs
           npts := 0
           lab.set [Text "0"] 

         draw = action
               forall _ <- [1..20] do
                 r <- gen.next
                 let chooseAffine p ((w,a):ws)
                                     |p<w  = a
                                     |True = chooseAffine p ws
                 pt := affineMap (chooseAffine r wifs) pt
                 let [x,y] = pt
                     x' = 250+floor(500*x)
                     y' = 500-floor(500*y)   
                 img.putPixel (x',y') black
               npts := npts+20
               lab.set [Text (show npts)]

      in struct ..Fract        



-- Some example IFS's

fern :: IFS
fern = [([[0.0,0.0],
          [0.0,0.16]],
         [0.0,0.0]
        ),
        ([[ 0.85,0.04],
          [-0.04,0.85]],
         [0.0,0.16]
        ),
        ([[0.2,-0.26],
          [0.23,0.22]],
         [0.0,0.16]
        ),
        ([[-0.15,0.28],
          [0.26,0.24]],
         [0.0,0.044]
        )
       ]

spruce :: IFS
spruce = [(scale 0.85,[0,0.15]),
          (mulM (scale 0.4) (rot 0.7),[0,0.2]),
          (mulM (scale 0.4) (rot (-0.7)),[0,0.2]),
          ([[0.4,0],[0,1]],[0,0])
         ]

sierpinski :: IFS
sierpinski = [(scale 0.5,[-0.125,0]),
              (scale 0.5,[0.125,0]),
              (scale 0.5,[0,0.5])
             ]

snowflake :: IFS
snowflake = (scale 0.35,[0,0.325]) :
   map (\n -> (mulM (scale 0.4) (rot(fromInt n*pi/3)),[0,0.5])) [1..6]
       


main tk = do 
   win  <- tk.window [Title "Fractals"]
   canv <- win.canvas [Background white, 
                       Width 500, Height 500]
   img <- tk.photo [Width 500, Height 500]
   canv.image (250,250) [Img img]
   gen <- uniformGenerator 7
   lab <- win.label [Width 8, Background white, Anchor E, 
                     Relief Groove, Font "helvetica 16"]
   fr  <- fract gen tk img lab
   fr.setIFS sierpinski
   sch <- tk.periodic 20 fr.draw
   let rs = map (\(t,c) -> [Text t, Command c, Width 10, Anchor W]) 
                    [("Sierpinski", fr.setIFS sierpinski),
                     ("Spruce", fr.setIFS spruce),
                     ("Fern", fr.setIFS fern),
                     ("Snowflake",fr.setIFS snowflake)]
   rad <- radio win (zip rs [1..]) [BorderWidth 4, Relief Ridge] 2
   rad.setValue(Just 1)
   qb <- win.button [Text "Quit",  Command tk.quit]
   sb <- win.button [Text "Start", Command sch.start]
   tb <- win.button [Text "Stop",  Command sch.stop]
   pack (row [rad,qb,sb,tb,lab] ^^^ canv)

