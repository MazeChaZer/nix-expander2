module GLForms where

import Tcl3d


--Definition bestimmter Typen
type Point2D 	= (Float,Float)
type Point3D 	= (Float,Float,Float)
type Triangle	= (Point3D,Point3D,Point3D)
type Quad	= (Point3D,Point3D,Point3D,Point3D)

--Zeichnet eine Linie zwischen zwei 2-Dimensionalen
--Punkten mit der Tiefe 0 im Raum
line2d :: Point2D -> Point2D -> Cmd ()
line2d (x1,y1) (x2,y2) = line (x1,y1,0) (x2,y2,0)

--Zeichnet eine Linie zwischen zwei 
--3-Dimensionalen Punkten
line :: Point3D -> Point3D -> Cmd ()
line (x1,y1,z1) (x2,y2,z2) = do
	glBegin GL_LINES
	glVertex3f x1 y1 z1
	glVertex3f x2 y2 z2
	glEnd

--Hilfsfunktion um Eckpunkte zu Zeichnen welche
--später in anderen Funktionen benutzt werden können
drawpoints :: [Point3D] -> Cmd ()
drawpoints ((x,y,z):[]) = do
	glVertex3f x y z
drawpoints ((x,y,z):xs) = do
	glVertex3f x y z
	drawpoints xs

--Zeichnet ein abstraktes Polygon mit den Eckpunkten 
--welche in der Liste angegeben sind
abstract ::  [Point3D] -> Cmd ()
abstract points = do
	glBegin GL_POLYGON
	drawpoints points
	glEnd

--Zeichnet einen Fächer aus Dreiecken.
--Der Erste Punkt der Liste bildet das Zentrum
--Alle weiteren bilden dreiecke zwischen dem ersten Punkt
--seinem vorgänger und sich selbst.
abstractFan ::  [Point3D] -> Cmd ()
abstractFan points = do
	glBegin GL_TRIANGLE_FAN
	drawpoints points
	glEnd

--Zeichnet eine geschlossene Linie über die Punkte der Liste
--Erster und letzter Punkt der Linie müssen identisch sein.
abstractLine ::  [Point3D] -> Cmd ()
abstractLine points = do
	glBegin GL_LINE_LOOP
	drawpoints points
	glEnd

--Zeichnet einen Kreis mit Mittelpunkt (x,y) und einem
--Radius von r auf der z-Ebene mit Tiefe 0
circle2d ::  Point2D -> Float -> Cmd ()
circle2d (x,y) r = circle (x,y,0) r

--Zeichnet einen Kreis mit Mittelpunkt (x,y,z) und dem
--Radius r auf der z-Ebene mit tiefe z
circle :: Point3D -> Float -> Cmd ()
circle (x,y,z) r = abstractFan ((x,y,z):[(x + (sin i) * r, y + (cos i) * r, z) | i <- [1*(pi/180),2*(pi/180)..361*(pi/180)]])

--Zeichnet eine Ellipse  mit Mittelpunkt (x,y), seinen 
--beiden Radien xr und yr mit einer tiefe von 0
ellipse2d ::  Point2D -> Float -> Float -> Cmd ()
ellipse2d (x,y) xr yr = ellipse (x,y,0) xr yr

--Zeichnet eine Ellipse  mit Mittelpunkt (x,y,z), seinen 
--beiden Radien xr und yr mit einer tiefe von z
ellipse :: Point3D -> Float -> Float -> Cmd ()
ellipse (x,y,z) xr yr = abstractFan ((x,y,z):[(x + (cos i) * xr, y + (sin i) * yr, z) | i <- [1*(pi/180),2*(pi/180)..361*(pi/180)]])

{--
      d---------b	a = v2 + (v3-v1)
     /|        /|	b = a + (v4-v1) = v2 + (v3-v1) + (v4-v1)
    / |       / |	c = v2 + (v4-v1)
   v4-|------c  |       d = v4 + (v3-v1)
   |  |      |  |
   |  v3-----|--a
   | /       | /
   |/________|/
   v1        v2

Zeichnet einen Quader mit den vier Punkten v1-v4. Die Restlichen werden Kontruiert
--}
quader :: Point3D -> Point3D -> Point3D -> Point3D -> Cmd ()
quader (x1,y1,z1) (x2,y2,z2) (x3,y3,z3) (x4,y4,z4) = do
	glBegin GL_QUADS
	--untere Fläche
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f  x3 y3 z3							-- v3
        glColor4f   1.0 0.0 0.0 0.2     -- Red
	glVertex3f  x1 y1 z1							-- v1
	glColor4f   0.0 1.0 0.0 0.2     -- Green
	glVertex3f  x2 y2 z2							-- v2
	glColor4f   1.0 1.0 1.0 0.2     -- white
	glVertex3f  (x2+x3-x1) (y2+y3-y1) (z2+z3-z1)				-- a  
	--vordere Fläche
	glColor4f   1.0 1.0 1.0 0.2     -- white
	glVertex3f  x4 y4 z4							-- v4
        glColor4f   1.0 0.0 0.0 0.2     -- Red
	glVertex3f  x1 y1 z1							-- v1
	glColor4f   0.0 1.0 0.0 0.2     -- Green
	glVertex3f  x2 y2 z2							-- v2
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f  (x2+x4-x1) (y2+y4-y1) (z2+z4-z1)				-- c
	--linke Fläche
	glColor4f   1.0 1.0 1.0 0.2     -- white
	glVertex3f  x4 y4 z4							-- v4
        glColor4f   1.0 0.0 0.0 0.2     -- Red
	glVertex3f  x1 y1 z1							-- v1
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f  x3 y3 z3							-- v3
	glColor4f   0.0 1.0 0.0 0.2     -- Green
	glVertex3f  (x4+x3-x1) (y4+y3-y1) (z4+z3-z1)				-- d
	-- obere Fläche
	glVertex3f  (x4+x3-x1) (y4+y3-y1) (z4+z3-z1)				-- d
	glColor4f   1.0 1.0 1.0 0.2     -- white
	glVertex3f  x4 y4 z4							-- v4
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f  (x2+x4-x1) (y2+y4-y1) (z2+z4-z1)				-- c	
        glColor4f   1.0 0.0 0.0 0.2     -- Red
	glVertex3f  (x2+x3-(2*x1)+x4) (y2+y3-(2*y1)+y4) (z2+z3-(2*z1)+z4)	-- b
        -- rechte Fläche
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f  (x2+x4-x1) (y2+y4-y1) (z2+z4-z1)				-- c
	glColor4f   0.0 1.0 0.0 0.2     -- Green
	glVertex3f  x2 y2 z2							-- v2
	glColor4f   1.0 1.0 1.0 0.2     -- white
	glVertex3f  (x2+x3-x1) (y2+y3-y1) (z2+z3-z1)				-- a 
        glColor4f   1.0 0.0 0.0 0.2     -- Red
	glVertex3f  (x2+x3-(2*x1)+x4) (y2+y3-(2*y1)+y4) (z2+z3-(2*z1)+z4)	-- b
	-- hintere Fläche
	glColor4f   0.0 1.0 0.0 0.2     -- Green
	glVertex3f  (x4+x3-x1) (y4+y3-y1) (z4+z3-z1)				-- d
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f  x3 y3 z3							-- v3
	glVertex3f  (x2+x3-x1) (y2+y3-y1) (z2+z3-z1)				-- a 
        glColor4f   1.0 0.0 0.0 0.2     -- Red
	glVertex3f  (x2+x3-(2*x1)+x4) (y2+y3-(2*y1)+y4) (z2+z3-(2*z1)+z4)	-- b
	glEnd

{--
     d
    /|\
   / | \
  /  |  \
 c---|---b
   \ | /
     a

Zeichnet einen tetraeder anhand seiner 4 Eckpunkte
--}
tetraeder :: Point3D -> Point3D -> Point3D -> Point3D -> Cmd ()
tetraeder (x1,y1,z1) (x2,y2,z2) (x3,y3,z3) (x4,y4,z4) = do
	glBegin GL_TRIANGLES
	-- untere Fläche
        glColor4f   1.0 0.0 0.0 0.2     -- Red
	glVertex3f x1 y1 z1	--a
	glColor4f   0.0 1.0 0.0 0.2     -- Green
	glVertex3f x2 y2 z2	--b
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f x3 y3 z3	--c
	-- linke Fläche
	glColor4f   1.0 0.0 0.0 0.2     -- Red
	glVertex3f x1 y1 z1	--a
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f x3 y3 z3	--c
	glColor4f   1.0 1.0 1.0 0.2     -- white
	glVertex3f x4 y4 z4	--d
	-- rechte Fläche
	glColor4f   1.0 0.0 0.0 0.2     -- Red
	glVertex3f x1 y1 z1	--a
	glColor4f   0.0 1.0 0.0 0.2     -- Green
	glVertex3f x2 y2 z2	--b
	glColor4f   1.0 1.0 1.0 0.2     -- white
	glVertex3f x4 y4 z4	--d
	-- hintere Fläche
	glColor4f   0.0 1.0 0.0 0.2     -- Green
	glVertex3f x2 y2 z2	--b
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f x3 y3 z3	--c
	glColor4f   1.0 1.0 1.0 0.2     -- white
	glVertex3f x4 y4 z4	--d
	glEnd


{--
 _,.-d-.,_ 
 c--/-\--x
 | /   \ |
 |/     \|
 a-------b

Zeichnet eine Pyramide.Die ersten drei parameter sind 
3 punkte der grundfläche. Der vierte ist die Spitze
--}
pyramide ::  Point3D -> Point3D -> Point3D -> Point3D -> Cmd ()
pyramide (x1,y1,z1) (x2,y2,z2) (x3,y3,z3) (x4,y4,z4) = do
	glBegin GL_QUADS
	--untere Fläche
        glColor4f   1.0 0.0 0.0 0.2     -- Red
	glVertex3f  x3 y3 z3
	glVertex3f  x1 y1 z1
	glVertex3f  x2 y2 z2
	glVertex3f  (x2+x3-x1) (y2+y3-y1) (z2+z3-z1)
	glEnd
	glBegin GL_TRIANGLES
        -- vorne
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f  x1 y1 z1
	glVertex3f  x2 y2 z2
	glVertex3f  x4 y4 z4
	-- links
	glColor4f   0.0 1.0 0.0 0.2     -- Green
	glVertex3f  x3 y3 z3
	glVertex3f  x1 y1 z1
	glVertex3f  x4 y4 z4
	-- rechts
	glColor4f   0.0 1.0 0.0 0.2     -- Green
	glVertex3f  x2 y2 z2
	glVertex3f  (x2+x3-x1) (y2+y3-y1) (z2+z3-z1)
	glVertex3f  x4 y4 z4
	-- hinten
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f  x3 y3 z3
	glVertex3f  (x2+x3-x1) (y2+y3-y1) (z2+z3-z1)
	glVertex3f  x4 y4 z4
	glEnd

{--
 d-------y     x = a + (d-c)
 | \   / |     y = b + (d-c)
 |   x   |
 |   |   |
 |   |   |
 c---|---b
   \ | /
     a

Zeichnet ein Prisma mit a,b,c als Grundfläche und d bestimmt die Höhe
--}
prism ::  Point3D -> Point3D -> Point3D -> Point3D -> Cmd ()
prism (x1,y1,z1) (x2,y2,z2) (x3,y3,z3) (x4,y4,z4) = do
	glBegin GL_TRIANGLES
	glColor4f   1.0 1.0 1.0 0.2     -- white
	-- unten
	glVertex3f x1 y1 z1
	glVertex3f x2 y2 z2
	glVertex3f x3 y3 z3
        -- oben
        glVertex3f (x1+x4-x3) (y1+y4-y3) (z1+z4-z3)
        glVertex3f (x2+x4-x3) (y2+y4-y3) (z2+z4-z3)
	glVertex3f x4 y4 z4
	glEnd
	glBegin GL_QUADS
	-- links
 	glColor4f   1.0 0.0 0.0 0.2     -- Red
	glVertex3f x1 y1 z1
        glVertex3f (x1+x4-x3) (y1+y4-y3) (z1+z4-z3)
	glVertex3f x4 y4 z4
	glVertex3f x3 y3 z3
	-- rechts
	glColor4f   0.0 0.0 1.0 0.2     -- Blue
	glVertex3f x1 y1 z1
	glVertex3f x2 y2 z2
        glVertex3f (x2+x4-x3) (y2+y4-y3) (z2+z4-z3)
	glVertex3f (x1+x4-x3) (y1+y4-y3) (z1+z4-z3)
	-- hinten
	glColor4f   0.0 1.0 0.0 0.2     -- Green
	glVertex3f x2 y2 z2
	glVertex3f x3 y3 z3
	glVertex3f x4 y4 z4
        glVertex3f (x2+x4-x3) (y2+y4-y3) (z2+z4-z3)
	glEnd 


--Ein paar Hilffunktionen für die berechnung der Punkte auf der Kugel
helperz0, helperzr0, helperz1, helperzr1 :: Int -> Int -> Float
helperz0 x lats		= sin(pi * ((-0.5) + (fromIntegral(x)-1)/fromIntegral(lats)))
helperzr0 x lats	= cos(pi * ((-0.5) + (fromIntegral(x)-1)/fromIntegral(lats)))
helperz1 x lats		= sin(pi * ((-0.5) + fromIntegral(x)/fromIntegral(lats)))
helperzr1 x lats 	= cos(pi * ((-0.5) + fromIntegral(x)/fromIntegral(lats)))
helperlng x longs	= 2.0 * pi * (fromIntegral(x)-1)/fromIntegral(longs)

--Hilfsfunktion für qstrip
internal :: (Float,Float,Float,Float) -> [(Float,Float)] -> Cmd ()
internal (z0,zr0,z1,zr1) ((x,y):[]) = do
	glNormal3f (x * zr0) (y * zr0) z0
	glVertex3f (x * zr0) (y * zr0) z0
	glNormal3f (x * zr1) (y * zr1) z1
	glVertex3f (x * zr1) (y * zr1) z1
internal z@(z0,zr0,z1,zr1) ((x,y):ys) = do
	glNormal3f (x * zr0) (y * zr0) z0
	glVertex3f (x * zr0) (y * zr0) z0
	glNormal3f (x * zr1) (y * zr1) z1
	glVertex3f (x * zr1) (y * zr1) z1
	internal z ys 

--Zeichnet die einzelnen Vierecke der Kugel anhand 
--der ihr übergebenen beiden Listen von Punkten
qstrip :: [(Float,Float,Float,Float)] -> [(Float,Float)] -> Cmd ()
qstrip (x:[]) ys = do
	glBegin GL_QUAD_STRIP
	internal x ys
	glEnd
qstrip (x:xs) ys = do
	glBegin GL_QUAD_STRIP
	internal x ys
	glEnd
	qstrip xs ys

--Zeichnet eine Kugel im derzeitigen 0-Punkt mit einem Radius von rad
--(ein radius von 1 bedeutet ganze frame größe) und der anzahl der längen
--und breiten Einteilungen für die genauigkeit
sphere :: Float -> Int -> Int -> Cmd ()
sphere rad lats longs = do
	let 	helper1 = [(rad * (helperz0 x lats), helperzr0 x lats, rad * (helperz1 x lats), helperzr1 x lats) | x <- [0,1..lats]]
		helper2 = [(rad * sin(helperlng x longs),rad * cos(helperlng x longs)) | x <- [0,1..longs]]
	qstrip helper1 helper2

--Zeichnet ein Ellipsoid im derzeitigen 0-Punkt mit den drei Radien rad1,rad2 und rad3
--(ein radius von 1 bedeutet ganze frame größe) und der anzahl der längen
--und breiten Einteilungen für die genauigkeit
elipsoid :: Float -> Float -> Float -> Int -> Int -> Cmd ()
elipsoid rad1 rad2 rad3 lats longs = do
	let 	helper1 = [(rad3 * (helperz0 x lats), helperzr0 x lats, rad3 * (helperz1 x lats), helperzr1 x lats) | x <- [0,1..lats]]
		helper2 = [(rad1 * sin(helperlng x longs),rad2 * cos(helperlng x longs)) | x <- [0,1..longs]]
	qstrip helper1 helper2

--Zeichnet drei Punkte in einer beliebigen Struktur
--die Punkte sin in einem Triple eingebettet
drawTriangle :: Triangle -> Cmd ()
drawTriangle ((x1,y1,z1),(x2,y2,z2),(x3,y3,z3)) = do
	glVertex3f x1 y1 z1
	glVertex3f x2 y2 z2
	glVertex3f x3 y3 z3

--Zeichnet 4 punkte in einer beliebigen Struktur
--die Punkte sin in einem Quadrupel eingebettet
drawQuad :: Quad -> Cmd ()
drawQuad ((x1,y1,z1),(x2,y2,z2),(x3,y3,z3),(x4,y4,z4)) = do
	glVertex3f x1 y1 z1
	glVertex3f x2 y2 z2
	glVertex3f x3 y3 z3
	glVertex3f x4 y4 z4

--Zeichnet eine Liste von Vierecken
quadList :: [Quad] -> Cmd ()
quadList (x:[]) = do
	glBegin GL_QUADS
	drawQuad x
	glEnd

quadList (x:xs) = do
	glBegin GL_QUADS
	drawQuad x
	quadList xs
	glEnd

--Zeichnet eine Liste von Dreiecken
triangleList :: [Triangle] -> Cmd ()
triangleList (x:[]) = do
	glBegin GL_TRIANGLES
	drawTriangle x
	glEnd

triangleList (x:xs) = do
	glBegin GL_TRIANGLES
	drawTriangle x
	triangleList xs
	glEnd

