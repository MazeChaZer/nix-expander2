module Tix where

import Tk


struct TixEnv < TkEnv =
  tixWindow  :: [WindowOpt]     -> Request TixWindow
  fileDialog :: [FileDialogOpt] -> Request FileDialog
  popupMenu  :: [PopupMenuOpt]  -> Request PopupMenu


struct TixBasicWindow a < BasicWindow a =
  tixframe           :: [FrameOpt]         -> Request TixFrame   
  toolTip            :: [FrameOpt]         -> Request ToolTip
  comboBox           :: Bool -> [ComboOpt] -> Request ComboBox
  spinBox            :: [SpinBoxOpt]       -> Request SpinBox
  hList              :: [HListOpt]         -> Request HList
  noteBook           :: [NoteBookOpt]      -> Request NoteBook
  panedWindow        :: [PanedWindowOpt]   -> Request PanedWindow
  optionMenu         :: [String] -> [OptionMenuOpt] -> Request OptionMenu
  scrolledHList      :: [ScrHListOpt]      -> Request ScrolledHList
  scrolledListBox    :: [ScrListBoxOpt]    -> Request ScrolledListBox
  scrolledTextEditor :: [ScrTextEditorOpt] -> Request ScrolledTextEditor
  scrolledTree       :: [ScrTreeOpt]       -> Request ScrolledTree
  scrolledBasicWindow:: [ScrWindowOpt]     -> Request ScrolledBasicWindow


struct TixWindow < TixBasicWindow WindowOpt, ManagedWindow, Window

struct TixFrame < Frame, TixBasicWindow FrameOpt

type HListPath = String


struct HasBasicWindow =
  subBasicWindow :: TixBasicWindow FrameOpt

struct HasLabel =
  subLabel :: Label

struct HasEntry =
  subEntry :: Entry

struct HasListBox =
  subListBox :: ListBox

struct HasMenu =
  subMenu :: Menu


struct HasMenuButton =
  subMenuButton :: MenuButton

struct HasHList =
  subHList :: HList

struct HasTextEditor =
  subTextEditor :: TextEditor


struct PopupWidget =
   popup   :: Action
   popdown :: Action

struct ToolTip < WWidget FrameOpt, HasLabel = 
   attachTo     :: Widget -> String -> Action

struct ComboBox < WWidget ComboOpt, LineEditable, 
                  Cell Int, HasLabel, HasListBox, HasEntry

struct SpinBox < WWidget SpinBoxOpt, Cell Int, HasLabel, HasEntry

struct FileDialog < ConfWidget FileDialogOpt, PopupWidget =
  subOK, subFilter, subCancel, subHelp :: Button

struct HList < WWidget HListOpt, Cell [String] =
  addEntry :: [HListEntryOpt] -> Request HListEntry
  childAt  :: HListPath -> Request HListEntry

struct HListEntry < Configurable HListEntryOpt, Cell String = 
  addChild  :: [HListEntryOpt] -> Request HListEntry
  hide, unhide :: Action
  see       :: Action
  delete    :: Action
  depth     :: Int
  getParent :: Request(Maybe HListEntry)


struct PanedWindow < WWidget PanedWindowOpt =
  pane :: [PaneOpt] -> Request (TixBasicWindow PaneOpt)
  
struct NoteBook < WWidget FrameOpt =
  page   :: [PageOpt] -> Request (TixBasicWindow PageOpt)
  raised :: Request (Maybe(TixBasicWindow PageOpt))

struct ScrolledTree < WWidget ScrTreeOpt, HasHList = 
  decorate :: Action

struct ScrolledListBox < WWidget ScrListBoxOpt, HasListBox 
   
struct ScrolledBasicWindow < WWidget ScrWindowOpt, HasBasicWindow 
   
struct ScrolledTextEditor < WWidget ScrTextEditorOpt, HasTextEditor

struct ScrolledHList < WWidget ScrHListOpt, HasHList

struct OptionMenu < WWidget OptionMenuOpt, Cell Int  

struct PopupMenu < Configurable PopupMenuOpt, HasMenu, HasMenuButton =
  popupIn       :: Widget -> Action

data Min      = Min Int
data Max      = Max Int
data Dropdown = Dropdown Bool
data Step     = Step Int
data CmdString = CmdString (String -> Cmd ())
data RaiseCmd  = RaiseCmd (Cmd ())
data DrawBranch = DrawBranch Bool
data ComboBoxEditable =  ComboBoxEditable Bool

data Status > None = On | Off | Default 
data ScrPolicyType = Auto | XBar | YBar | XYBar | NoBar
data ScrollPolicy  = ScrollPolicy ScrPolicyType
data ExpandRate    = ExpandRate Int

type PopupMenuOpt = Title
data PanedWindowOpt > StdOpt, Orientation
data PaneOpt        > Min, Max, ExpandRate
data PageOpt        > CLabel, Img, Btmp, Enabled, Underline, Justify, RaiseCmd
data FileDialogOpt  > StdOpt, CmdString
data ComboOpt       > StdOpt, CmdString, CLabel, Dropdown --Fancy
data SpinBoxOpt     > StdOpt, CmdInt, CLabel, Min, Max, Step

type NoteBookOpt = StdOpt

data HListOpt         > StdOpt, Font, Foreground, 
                        SelectMode, CmdString, DrawBranch
data HListEntryOpt    > Btmp, Img, Text
data ScrWidgetOpt     > StdOpt, ScrollPolicy
type ScrTextEditorOpt = ScrWidgetOpt
type ScrHListOpt      = ScrWidgetOpt
type ScrTreeOpt       = ScrWidgetOpt
type ScrWindowOpt     = ScrWidgetOpt
data ScrListBoxOpt    > ScrWidgetOpt, Command, Anchor
data OptionMenuOpt    > StdOpt, CmdInt, Enabled, CLabel

--type CheckListOpt     = HListOpt


data TixOpt > AllOpt, ComboOpt, SpinBoxOpt, FileDialogOpt,
              ScrListBoxOpt, PageOpt, PanedWindowOpt, PaneOpt,
              ScrTextEditorOpt, ScrWindowOpt, OptionMenuOpt,
              HListOpt, HListEntryOpt, ComboBoxEditable

tixOpt :: TixOpt -> Cmd String
tixOpt opt = 
  let  mklist ps = concatMap mkpair ps 
       mkpair (ps,str) = "{{"++unwords ps++"} {"++str++"}} "
  in  case opt of
          Anchor a      -> return(unwords ["anchor",show a])
          Angles (x,y)  -> return(unwords ["start",show x,"-extent",show y])
          ArcStyle a    -> return(unwords ["style",show a])
          Arrow a       -> return(unwords ["arrow",show a])
          Background c  -> return(unwords ["background",show c])
          BitmapData str -> return(unwords ["data {",str,"}"])
          BorderWidth n -> return(unwords ["borderwidth",show n])
          Btmp bmp      -> return(unwords ["bitmap",bmp.imageName])
          CapStyle c    -> return(unwords ["capstyle",show c])
          CLabel str    -> return(unwords["label {",str,"}"])
          ComboBoxEditable b -> return(unwords["editable",show b]) 
          Command a     -> do n <- primAddCallBack (\_ -> a)
                              return ("command {doEvent " ++ show n ++ "}")
          CmdInt a      -> do let f str = a(read y) where [x,y] = words str
                              n <- primAddCallBack f
                              return ("command {doEvent " ++ show n ++ "}")
          CmdString a   -> do let f str = a y where [x,y] = words str
                              n <- primAddCallBack f
                              return ("command {doEvent " ++ show n ++ "}")
          Cursor str    -> return(unwords ["cursor",str])
          DrawBranch b  -> return(unwords ["drawbranch",show b])
          Dropdown b    -> return("dropdown "++show b)
          Enabled s     -> return(unwords ["state",enabled s])   
          ExpandRate n  -> return(unwords ["expand",show n])
          File p        -> return(unwords ["file",p])
          Fill c        -> return(unwords ["fill",show c])
          Foreground c  -> return(unwords ["foreground",show c])
          Font str      -> return(unwords ["font",'{':str++"}"])
          From x        -> return(unwords ["from",show x])
          Height h      -> return(unwords ["height",show h])
          Img image     -> return(unwords ["image",image.imageName])
          Indicatoron b -> return(unwords ["indicatoron", show b])
          JoinStyle c   -> return(unwords ["joinstyle",show c])
          Length n      -> return(unwords ["length",show n])
          Max n         -> return(unwords ["max",show n])
          Min n         -> return(unwords ["min",show n])
          Orientation d -> return(unwords ["orient",show d])
          Outline c     -> return(unwords ["outline",show c])
          RaiseCmd a   -> do n <- primAddCallBack (\_ -> a)
                             return ("raisecmd {doEvent " ++ show n ++ "}")
          Relief r      -> return(unwords ["relief",show r])
          ScrollPolicy p -> return(unwords["scrollbar",show p])
          ScrollRegion (x1,y1) (x2,y2) -> return(unwords("scrollregion {":[show x1, show y1, show x2, show y2]++["}"]))
          SelectMode s  -> return(unwords ["selectmode",textSelect s])
          SelectColor Nothing  -> return(unwords ["selectcolor","\"\""])
          SelectColor (Just c) -> return(unwords ["selectcolor", show c])
          Smooth b      -> return(unwords ["smoth",show b])   
          Step n        -> return(unwords ["step",show n])   
          Stipple file  -> return(unwords ["stipple",'@':file])   
          Text str      -> return(unwords ["text",'{':str++"}"])
          Title  str    -> return(unwords ["title",'{':str++"}"])
          To x          -> return(unwords ["to",show x])
          Width  w      -> return(unwords ["width",show w])
          Wrap w        -> return(unwords ["wrap",textWrap w])


instance Show ScrPolicyType where  
   showsPrec _  Auto  rest = "auto"++rest
   showsPrec _  XBar  rest = "x"++rest
   showsPrec _  YBar  rest = "y"++rest
   showsPrec _  XYBar rest = "both"++rest
   showsPrec _  NoBar rest = "none"++rest


instance Show Status where
   showsPrec _ On rest  = "on"++rest
   showsPrec _ Off rest = "off"++rest
   showsPrec _ Default rest = "default"++rest
   showsPrec _ None rest = "none"++rest

tixOpts :: [TixOpt ] -> Cmd String
tixOpts opts = do
  os <- mapM tixOpt opts
  return (concatMap (" -"++) os)


-- tixbwin :: String -> Template (TixBasicWindow TixOpt)
tixbwin cnfstr nm = do
  tkwin <- bwin nm
  fix w <- tixbwin' tkwin w
  return (w nm)
  where
  tixbwin' tkwin slf =
    template in
    \nm ->
    let
      ident   = tkwin.ident
      destroy = tkwin.destroy
      exists  = tkwin.exists
      set os  = action gsetcmd cnfstr os
      focus   = tkwin.focus
      lower   = tkwin.lower
      raise   = tkwin.raise
      bind    = tkwin.bind
      button  = tkwin.button
      canvas     = tkwin.canvas
      checkButton = tkwin.checkButton
      entry       = tkwin.entry
      frame       = tkwin.frame
      label       = tkwin.label
      listBox     = tkwin.listBox
      menuButton  = tkwin.menuButton
      radioButton = tkwin.radioButton
      scrollBar   = tkwin.scrollBar
      slider      = tkwin.slider
      textEditor  = tkwin.textEditor
 
      wwid wname f opts = request
         x <- primGetPath
         let nm0 = app nm x
         os <- tixOpts opts
         primExTcl_[wname,nm0,os]
         f nm nm0 
      
      toolTip   = wwid "tixBalloon" bln
      comboBox editable os = wwid "tixComboBox" cbx (ComboBoxEditable editable:os)
      spinBox   = wwid "tixControl" cntrl
      scrolledBasicWindow = wwid "tixScrolledWindow" scrfrm
      scrolledListBox = wwid "tixScrolledListBox" scrlstbox
      scrolledTextEditor = wwid "tixScrolledText" scrtxted
      scrolledHList = wwid "tixScrolledHList" scrhlist
      scrolledTree = wwid "tixTree" scrtree
      noteBook  = wwid "tixNoteBook" ntbook
      panedWindow = wwid "tixPanedWindow" pndwin
      optionMenu os = wwid "tixOptionMenu" (optmen os)
      hList = wwid "tixHList" hlist 
      tixframe opts = request
           frm <- tkwin.frame opts
           tixfrm frm (slf frm.ident)
   in struct ..TixBasicWindow

tixfrm frm win = 
  template in
    struct
      ident = frm.ident
      destroy = frm.destroy
      exists = frm.exists
      focus = frm.focus
      raise = frm.raise
      lower = frm.lower
      bind = frm.bind
      set = frm.set
      packIn = frm.packIn
      wname = frm.wname
      button = frm.button
      canvas = frm.canvas
      checkButton = frm.checkButton
      entry = frm.entry
      frame = frm.frame
      label = frm.label
      listBox = frm.listBox
      menuButton = frm.menuButton
      radioButton = frm.radioButton
      scrollBar = frm.scrollBar
      slider = frm.slider
      textEditor = frm.textEditor
      tixframe = win.tixframe
      toolTip = win.toolTip
      comboBox = win.comboBox
      spinBox = win.spinBox
      hList = win.hList
      noteBook = win.noteBook
      panedWindow = win.panedWindow
      optionMenu = win.optionMenu
      scrolledHList = win.scrolledHList
      scrolledListBox = win.scrolledListBox
      scrolledTextEditor = win.scrolledTextEditor
      scrolledTree = win.scrolledTree
      scrolledBasicWindow = win.scrolledBasicWindow
      

tixwin nm =
   template
      win <- tixbwin  (nm++" configure") nm
      mw  <- managedWindow nm
   in struct 
      ident   = win.ident
      destroy = win.destroy
      exists  = win.exists
      set os  = action winsetcmd nm os
      focus   = win.focus
      lower   = win.lower
      raise   = win.raise
      bind    = win.bind
      button  = win.button
      canvas     = win.canvas
      checkButton = win.checkButton
      entry       = win.entry
      frame       = win.frame
      label       = win.label
      listBox     = win.listBox
      menuButton  = win.menuButton
      radioButton = win.radioButton
      scrollBar   = win.scrollBar
      slider      = win.slider
      textEditor  = win.textEditor
      tixframe  = win.tixframe
      toolTip   = win.toolTip
      comboBox  = win.comboBox
      spinBox   = win.spinBox
      scrolledBasicWindow = win.scrolledBasicWindow
      scrolledListBox = win.scrolledListBox
      scrolledTextEditor = win.scrolledTextEditor
      scrolledHList = win.scrolledHList
      scrolledTree = win.scrolledTree
      noteBook  = win.noteBook
      panedWindow = win.panedWindow
      optionMenu = win.optionMenu
      hList = win.hList
      getGeometry = mw.getGeometry
      setSize = mw.setSize
      setPosition = mw.setPosition
      iconify  = mw.iconify
      deiconify = mw.deiconify





tsetcmd nm os = do
   ostr <- tixOpts os
   primExTcl_ [nm,"configure",ostr]


gsetcmd nmcf os = do
   ostr <- tixOpts os
   primExTcl_ [nmcf,ostr]


subwidget wname nm typ fn opts = do
   nm1 <- primExTcl [nm,"subwidget",typ]
   if not(null opts) then
      os1 <- tixOpts opts
      primExTcl_ [nm1,"configure",os1]
   fn wname nm1
 

tixfdial :: String -> String -> Cmd FileDialog
tixfdial wname nm = do
  box <- primExTcl [nm,"subwidget btns"]
  ok <- subwidget wname box "ok" but []
  filter <- subwidget wname box "apply" but []
  cancel <- subwidget wname box "cancel" but []
  help  <- subwidget wname box "help" but []
  template in
   struct
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    popup      = primExTcl_ [nm,"popup"]
    popdown    = primExTcl_ [nm,"popdown"] 
    subOK      = ok
    subFilter  = filter
    subCancel  = cancel
    subHelp    = help

popupm :: String -> String -> Cmd PopupMenu
popupm wname nm = do
  men <- subwidget wname nm "menu" men []
  menbut <- subwidget wname nm "menubutton" menubut [] 
  template in
   struct
    set os = action tsetcmd nm os 
    subMenu    = men
    subMenuButton  = menbut
    popupIn w  = primExTcl_ [nm,"bind",w.ident]


ntbook:: String -> String -> Template NoteBook
ntbook wname nm =
  template in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    page opts = request
         x <- primGetPath
         let nm0 = app nm x
             pg  = tail x
         os <- tixOpts opts
         nm1 <- primExTcl[nm,"add",pg,os]
         tixbwin (nm++" pageconfigure "++pg) nm1
    raised = request
        pg <- primExTcl [nm,"raised"]
        case pg of 
          "" -> return Nothing
          _  -> nm1 <- primExTcl[nm,"subwidget",pg]
                win <- tixbwin (nm++" pageconfigure "++pg) nm1
                return(Just win)

pndwin:: String -> String -> Template PanedWindow
pndwin wname nm =
  template in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    pane opts = request
         x <- primGetPath
         let nm0 = app nm x
             pn = tail x
         os <- tixOpts opts
         primExTcl_[nm,"add",pn,os]
         nm1 <- primExTcl[nm,"subwidget",pn]
         tixbwin (" paneconfigure "++pn) nm1


optmen:: [String] -> String -> String -> Cmd OptionMenu
optmen os wname nm = do
  x <- primGetPath
  let var = app nm x
  primExTcl_ [nm,"configure -variable",var]
  forall (n,o) <- zip [0..] os do 
    primExTcl_ [nm,"add command",show n,"-label",'{':o++"}"]
  template in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    setValue n = primExTcl_["set",var,show n]      
    getValue = request 
      nstr <- primExTcl["global",var,"; set",var]
      return(read nstr)

hlist :: String -> String -> Template HList
hlist wname nm = 
  template 
   id := 0
  in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    addEntry opts = request 
      os <- tixOpts opts
      let id1 = show id
      primExTcl_ [nm,"add",id1,"-itemtype imagetext",os]
      id := id+1
      hlent nm id1 0 0
    childAt id1 = request
       cs <- primExTcl [nm,"info children",id1]
       hlent nm id1 (length (filter (=='.') id1)) (length (words cs))
    setValue strs = action
       primExTcl_[nm,"selection clear"]
       forall str <- strs do
         primExTcl_[nm,"selection set",str]
    getValue = request
       ss <- primExTcl [nm,"selection get"]
       return(words ss)
    

hlent :: String -> String -> Int -> Int -> Template HListEntry
hlent nm id d children = 
  template
    id0 := children 
  in 
   struct
   set opts = action
     os <- tixOpts opts
     primExTcl_ [nm,"entryconfigure",id,os]
   addChild opts = request
      os <- tixOpts opts
      let id1 = app id ('.':show id0)
      id0 := id0+1
      primExTcl_ [nm,"add",id1,"-itemtype imagetext",os]
      hlent nm id1 (d+1) 0
   hide = primExTcl_[nm,"hide entry",id]
   unhide = primExTcl_[nm,"show entry",id]
   see = primExTcl_[nm,"see",id]
   delete = primExTcl_[nm,"delete entry",id]
   depth = d
   getValue = request
      s <- primExTcl[nm,"entryconfigure",id,"-text"]
      return(if last s == '}' then reverse(takeWhile (/= '{') (tail(reverse s))) else last(words s))
   setValue s = primExTcl_[nm,"entryconfigure",id,"-text",'{':s++"}"]
   getParent = request 
      case dropWhile (/= '.') (reverse id) of
        []   -> return Nothing
        c:cs -> cn <- primExTcl [nm,"info children",reverse cs]
                ent <- hlent nm (reverse cs) (d-1) (length(words cn))
                return(Just ent)

bln :: String -> String -> Cmd ToolTip
bln wname nm = do
  lab <- subwidget wname nm "message" lab []
  template in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    attachTo mw str = primExTcl_ [nm,"bind", mw.ident,"-msg {",str,"}"]
    subLabel  = lab

cbx :: String -> String -> Cmd ComboBox
cbx wname nm = do
  lab <- subwidget wname nm "label" lab []
  ent <- subwidget wname nm "entry" ent []
  lstbx <- subwidget wname nm "listbox" lstbox []
  template in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    lines   = request 
        x <- primExTcl[nm, "subwidget listbox size"]
        return (read x)
    getLine n = primExTcl[nm, "subwidget listbox get", show n] 
    insertLines n ss = action
      forall (s,k) <- zip ss [n..] do
        primExTcl_[nm, "insert", show k, '{':s++"}"] 
    deleteLine n = primExTcl_[nm, "subwidget listbox delete", show n] 
    setValue n = primExTcl_[nm, "pick",show n]      
    getValue = request 
        nstr <- primExTcl[nm, "subwidget listbox curselection"]
        return(read nstr)
    subLabel = lab
    subEntry = ent
    subListBox = lstbx

cntrl :: String -> String -> Cmd SpinBox
cntrl wname nm = do
  lab <- subwidget wname nm "label" lab []
  ent <- subwidget wname nm "entry" ent []
  template in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    setValue n = primExTcl_[nm, "configure -value", show n]      
    getValue = request
       str <- primExTcl[nm, "configure -value"]  
       return (read(last(words str)))
    subLabel = lab
    subEntry = ent

scrlstbox :: String -> String -> Cmd ScrolledListBox
scrlstbox wname nm = do 
  lstbx <- subwidget wname nm "listbox" lstbox []
  template in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    subListBox = lstbx

scrfrm :: String -> String -> Cmd ScrolledBasicWindow
scrfrm wname nm = do 
  bw <- subwidget wname nm "window" (const (tixbwin (nm++" configure"))) []
  template in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    subBasicWindow  = bw

scrhlist :: String -> String -> Cmd ScrolledHList
scrhlist wname nm = do
  hl <- subwidget wname nm "hlist" hlist []
  template in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    subHList = hl

scrtree :: String -> String -> Cmd ScrolledTree
scrtree wname nm = do
  hl <- subwidget wname nm "hlist" hlist []
  template in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    subHList = hl
    decorate = primExTcl_ [nm,"autosetmode"]

scrtxted :: String -> String -> Cmd ScrolledTextEditor
scrtxted wname nm = do
  ted <- subwidget wname nm "text" editor []
  template in
   struct 
    ident      = nm
    destroy    = primExTcl_["tixDestroy",nm]
    exists     = request excmd nm
    set os     = action tsetcmd nm os 
    focus      = primExTcl_ ["focus",nm]
    lower      = primExTcl_ ["lower",nm]
    raise      = primExTcl_ ["raise",nm]
    bind evs   = action bnd ["bind",nm] evs
    packIn  = wpackIn nm
    wname   = wname
    subTextEditor = ted


primTixEnv :: Template TixEnv
primTixEnv = 
  template
       tkenv <- primTkEnv
  in let
       window = tkenv.window
       bell = tkenv.bell
       delay = tkenv.delay
       periodic = tkenv.periodic
       bitmap = tkenv.bitmap
       photo = tkenv.photo
       putChar = tkenv.putChar
       putStr = tkenv.putStr
       putStrLn = tkenv.putStrLn
       setReader = tkenv.setReader
       setLineReader = tkenv.setLineReader
       writeFile = tkenv.writeFile
       appendFile = tkenv.appendFile
       readFile = tkenv.readFile
       devices = tkenv.devices
       inet = tkenv.inet
       sendMIDI = tkenv.sendMIDI
       timeOfDay = tkenv.timeOfDay
       progArgs = tkenv.progArgs
       getEnv   = tkenv.getEnv
       quit     = tkenv.quit
       tixWindow opts = request
         x <- primGetPath
         primExTcl_["toplevel",x]
         w <- tixwin x
         w.set opts
         return w
       fileDialog opts = request
         x <- primGetPath
         primExTcl_ ["tixFileSelectDialog",x]
         fd <- tixfdial x x
         fd.set opts
         return fd
       popupMenu opts = request
         x <- primGetPath
         primExTcl_ ["tixPopupMenu",x]
         pm <- popupm x x
         pm.set opts
         return pm

   in struct ..TixEnv

