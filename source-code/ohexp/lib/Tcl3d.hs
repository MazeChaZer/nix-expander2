module Tcl3d where

-- 20.2.2012

-- glBE replaces glBegin/End.
-- glClearColor and glColor3f now read RGB colors.
-- glNormal3f, glVertex2f and glVertex3f are decascadized and thus now read 
-- Point2/3 tuples.

import Tk

type Point2 = (Float,Float)
type Point3 = (Float,Float,Float)

struct Tcl3d < TkEnv = initTcl3d  :: Action
		       toGLwidget :: String -> [GLopt] -> Request GLwidget

tcl3d :: Template Tcl3d
tcl3d = 
  template tkenv <- primTkEnv
  in let window        = tkenv.window
	 bell          = tkenv.bell
	 delay         = tkenv.delay
	 periodic      = tkenv.periodic
	 bitmap        = tkenv.bitmap
	 photo         = tkenv.photo
	 putChar       = tkenv.putChar
	 putStr        = tkenv.putStr
	 putStrLn      = tkenv.putStrLn
	 setReader     = tkenv.setReader
	 setLineReader = tkenv.setLineReader
	 writeFile     = tkenv.writeFile
	 appendFile    = tkenv.appendFile
	 readFile      = tkenv.readFile
	 devices       = tkenv.devices
	 inet          = tkenv.inet
	 timeOfDay     = tkenv.timeOfDay
	 progArgs      = tkenv.progArgs
	 getEnv        = tkenv.getEnv
	 quit          = tkenv.quit
	 sendMIDI      = tkenv.sendMIDI
	 font 	       = tkenv.font
	 runTclCmd     = tkenv.runTclCmd
	 runTclCmd_    = tkenv.runTclCmd_
	 path = "set auto_path [linsert $auto_path 0 tclPackages/" ++
		--"tcl3d0.5.0]"
		"tcl3d-Darwin-0.5.0]"
		--"tcl3d-Linux64-0.5.0]"
         initTcl3d = action mapM_ primExecuteTcl_ [path,"package require tcl3d"]
	 toGLwidget wn opts = 
	 		   request x <- primGetPath
		                   let fullname = if wn == "." then x else wn++x
				   primExTcl_ ["togl",fullname]
				   primExTcl_ ["pack",fullname,"-in",wn,
				               "-side top -fill both -expand 1"]
 				   t <- togl wn fullname x
				   t.setOpts opts
                                   return t
     in struct ..Tcl3d
	
togl :: String -> String -> String -> Template GLwidget         -- used in tcl3d
togl wn fullname x =
  template wid <- widget x					-- see Tk.hs
  in let ident			= fullname
         destroy		= wid.destroy
	 exists			= wid.exists
	 focus			= wid.focus
	 raise			= wid.raise
	 lower			= wid.lower
	 bind			= wid.bind
	 packIn			= wpackIn fullname
	 wname			= wn
         glName	                = x
	 ex                     = primExTcl . (fullname:)
	 ex_ 		        = primExTcl_ . (fullname:)
         f str g                = request result <- ex ["configure",str]
	                                  return $ g $ last $ words result 
{- Besteht result aus Blanks, dann wird hier der program error "last []" 
erzeugt. Was tut f eigentlich? Warum ist result manchmal leer? -}		  
	 cget op 		= f op id
	 getCurrentWidth        = f "-width" read
	 getCurrentHeight       = f "-height" read
	 
	 -- http://togl.sourceforge.net/tclapi.html#toglfuncs
	 setOpts opts           = action os <- mapM optToCmd opts
	 		                 ex_ ["configure",concatMap (" -"++) os]					 
	 configure              = request ex ["configure"] 
	 			  -- lack of -option value ???
	 reconfigure op v       = ex_ ["configure",op,v]
	 contexttag             = request result <- ex ["contexttag"]
			                  return $ read result
	 extensions             = request ex ["extensions"]
	 postredisplay          = ex_ ["postredisplay"]
	 render			= ex_ ["render"]
	 swapbuffers		= ex_ ["swapbuffers"]
	 makecurrent		= ex_ ["makecurrent"]
	 takephoto imgname	= ex_ ["takephoto",imgname]
	 loadbitmapfont	font	= ex_ ["loadbitmapfont",font]
	 unloadbitmapfont tfont	= ex_ ["uploadbitmapfont",tfont]
	 write tfont str        = ex_ ["write",tfont,str]
	 			  -- lack of [-pos xyzw][-color rgba] ???
	 uselayer layer		= ex_ ["uselayer",layer]
	 showoverlay		= ex_ ["showoverlay"]
	 hideoverlay		= ex_ ["hideoverlay"]
	 postredisplayoverlay	= ex_ ["postredisplayoverlay"]
	 renderoverlay		= ex_ ["renderoverlay"]
	 existsoverlay		= request ex ["existsoverlay"]
	 ismappedoverlay        = request ex ["ismappedoverlay"]
	 getoverlaytransparentvalue 
	   		        = request ex ["getoverlaytransparentvalue"]
	 drawbuffer mode        = ex_ ["drawbuffer",mode]
	 glclear mask           = ex_ ["clear",mask]
	 frustum left right bottom top near far 
	 		        = ex_ ["frustum",left,right,bottom,top,near,far]
	 ortho left right bottom top near far 
	 			= ex_ ["ortho",left,right,bottom,top,near,far]
	 numeyes		= request ex ["numeyes"]
    in struct ..GLwidget
    
optToCmd :: GLopt -> Cmd String					-- used in togl
optToCmd opt = case opt of   Cursor a         -> f "cursor" a
			     Height a         -> f "height" a
			     Width a          -> f "width" a
			     Setgrid a 	      -> f "setgrid" a
			     GLtime a         -> f "time" a
			     Swapinterval a   -> f "swapinterval" a
			     Rgba a	      -> f "rgba" a
			     Redsize a	      -> f "redsize" a
			     Greensize a      -> f "greensize" a
			     Bluesize a	      -> f "bluesize" a
			     Alpha a          -> f "alpha" a
			     Alphasize a      -> f "alphasize" a
			     Accum a          -> f "accum" a
			     Accumredsize a   -> f "accumredsize" a
			     Accumgreensize a -> f "accumgreensize" a
			     Accumbluesize a  -> f "accumbluesize" a
			     Stencil a        -> f "stencil" a
			     Stencilsize a    -> f "stencilsize" a
			     GLdouble a       -> f "double" a
			     Depth a          -> f "depth" a
			     Depthsize a      -> f "depthsize" a
			     Auxbuffers a     -> f "auxbuffers" a
			     Privatecmap a    -> f "privatecmap" a
			     Overlay a	      -> f "overlay" a
			     Indirect a	      -> f "indirect" a
			     Sharelist a      -> f "sharelist" a
			     Sharecontext a   -> f "sharecontext" a
			     Pixelformat a    -> f "pixelformat" a
			     Stereo a	      -> f "stereo" a
	       where f :: Show a => String -> a -> Cmd String
	             f str a = return $ str++' ':show a
    
-- Widgets
	       
struct GLwidget < Widget, Packable = 
            glName  :: String
	    setOpts :: [GLopt] -> Action
	    
	    -- http://togl.sourceforge.net/tclapi.html#toglfuncs
	    postredisplay, render, swapbuffers, makecurrent, showoverlay, 
	     hideoverlay, postredisplayoverlay, renderoverlay :: Action
	    takephoto, loadbitmapfont, unloadbitmapfont, uselayer, drawbuffer, 
	     glclear           :: String -> Action
	    reconfigure, write :: String -> String -> Action
	    frustum, ortho     :: String -> String -> String -> String -> String 
				         -> String -> Action
	    getCurrentWidth, getCurrentHeight, contexttag :: Request Int
	    configure, extensions, existsoverlay, ismappedoverlay, 
	     getoverlaytransparentvalue, numeyes :: Request String
	    cget                                 :: String -> Request String
		  
-- http://togl.sourceforge.net/tclapi.html#options
			  
data GLopt   > GeoOpt, FormOpt, Cursor = GLtime Int | Swapinterval Int
		       
data GeoOpt  > DimOpt = Setgrid Int   	 	

data FormOpt > GLrgb, GLalpha, GLaccum, GLstencil
		        = Auxbuffers Int | Depth Bool | Depthsize Int | 
		          GLdouble Bool | Privatecmap Bool | Overlay Bool |
		          Indirect Bool | Sharelist String | Stereo StereoType |
		          Sharecontext String | Pixelformat Int

-- http://togl.sourceforge.net/stereo.html

data StereoType = Native | Anaglyph | CrossEye | WallEye| Dti | LeftEye | 
		  RightEye | Sgioldstyle

instance Show StereoType where show Native      = "native"
			       show Anaglyph    = "anaglyph"
			       show CrossEye    = "cross-eye"
			       show WallEye     = "wall-eye"
			       show Dti         = "dti"
			       show LeftEye     = "left eye"
			       show RightEye    = "right eye"
			       show Sgioldstyle = "sgioldstyle"

data GLrgb     = Rgba Bool | Redsize Int | Greensize Int | Bluesize Int
data GLalpha   = Alpha Bool | Alphasize Int
data GLaccum   = Accum Bool | Accumredsize Int | Accumgreensize Int |
		 Accumbluesize Int
data GLstencil = Stencil Bool | Stencilsize Int
						
data GlBeginModeType   = GL_POINTS | GL_LINES | GL_LINE_STRIP | GL_LINE_LOOP | 
			 GL_TRIANGLES | GL_TRIANGLE_STRIP | GL_TRIANGLE_FAN | 
			 GL_QUADS | GL_QUAD_STRIP | GL_POLYGON deriving Show
			 
data GlCapabilityType  = GL_ALPHA_TEST | GL_AUTO_NORMAL | GL_BLEND | 
			 GL_CLIP_PLANE | GL_COLOR_LOGIC_OP | GL_COLOR_MATERIAL | 
			 GL_COLOR_SUM | GL_COLOR_TABLE | GL_CONVOLUTION_1D | 
			 GL_CONVOLUTION_2D | GL_CULL_FACE | GL_DEPTH_TEST | 
			 GL_DITHER | GL_FOG | GL_HISTOGRAM | GL_INDEX_LOGIC_OP | 
			 GL_LIGHT |  GL_LIGHT0 | GL_LIGHT1 | GL_LIGHT2 |
			 GL_LIGHTING | GL_LINE_SMOOTH | GL_LINE_STIPPLE | 
			 GL_MAP1_COLOR_4 | GL_MAP1_INDEX | GL_MAP1_NORMAL | 
			 GL_MAP1_TEXTURE_COORD_1 | GL_MAP1_TEXTURE_COORD_2 | 
			 GL_MAP1_TEXTURE_COORD_3 | GL_MAP1_TEXTURE_COORD_4 | 
			 GL_MAP1_VERTEX_3 | GL_MAP1_VERTEX_4 | GL_MAP2_COLOR_4 | 
			 GL_MAP2_INDEX | GL_MAP2_NORMAL | 
			 GL_MAP2_TEXTURE_COORD_1 | GL_MAP2_TEXTURE_COORD_2 | 
			 GL_MAP2_TEXTURE_COORD_3 | GL_MAP2_TEXTURE_COORD_4 | 
			 GL_MAP2_VERTEX_3 | GL_MAP2_VERTEX_4 | GL_MINMAX | 
			 GL_MULTISAMPLE | GL_NORMALIZE | GL_POINT_SMOOTH | 
			 GL_POINT_SPRITE | GL_POLYGON_OFFSET_FILL | 
			 GL_POLYGON_OFFSET_LINE | GL_POLYGON_OFFSET_POINT | 
			 GL_POLYGON_SMOOTH | GL_POLYGON_STIPPLE | 
			 GL_POST_COLOR_MATRIX_COLOR_TABLE | 
			 GL_POST_CONVOLUTION_COLOR_TABLE | GL_RESCALE_NORMAL | 
			 GL_SAMPLE_ALPHA_TO_COVERAGE | GL_SAMPLE_ALPHA_TO_ONE | 
			 GL_SAMPLE_COVERAGE | GL_SEPARABLE_2D | 
			 GL_SCISSOR_TEST | GL_STENCIL_TEST | GL_TEXTURE_1D | 
			 GL_TEXTURE_2D | GL_TEXTURE_3D | GL_TEXTURE_CUBE_MAP | 
			 GL_TEXTURE_GEN_Q | GL_TEXTURE_GEN_R | 
			 GL_TEXTURE_GEN_S | GL_TEXTURE_GEN_T | 
			 GL_VERTEX_PROGRAM_POINT_SIZE | 
			 GL_VERTEX_PROGRAM_TWO_SIZE deriving Show
			 
data GlFaceModeType    = GL_FRONT | GL_BACK | GL_FRONT_AND_BACK deriving Show

data GlListType        = GL_COMPILE | GL_COMPILE_AND_EXECUTE deriving Show

data GlMatrixModeType  = GL_MODELVIEW | GL_PROJECTION | GL_TEXTURE | GL_COLOR
			 deriving Show
			 
data GlParamType       = GL_AMBIENT | GL_DIFFUSE | GL_SPECULAR | GL_POSITION | 
			 GL_SPOT_DIRECTION | GL_SPOT_EXPONENT | GL_SPOT_CUTOFF | 
			 GL_CONSTANT_ATTENUATION | GL_LINEAR_ATTENUATION | 
			 GL_QUADRATIC_ATTENUATION | GL_EMISSION | GL_SHININESS |
			 GL_AMBIENT_AND_DIFFUSE | GL_COLOR_INDEXES deriving Show
			 
data GlPolygonModeType = GL_POINT | GL_LINE | GL_FILL deriving Show

data GlShadeModelType  = GL_FLAT | GL_SMOOTH deriving Show
					
glBE :: GlBeginModeType -> Cmd () -> Cmd ()
glBE e act = do primExTcl_ ["glBegin",show e]; act; primExTcl_ ["glEnd"]
					
--glBegin :: GlBeginModeType -> Action
--glBegin e = primExTcl_ ["glBegin",show e]

--glEnd :: Action
--glEnd = primExTcl_ ["glEnd"]

glFlush :: Action
glFlush = primExTcl_ ["glFlush"]

glBlendFunc, glClear :: String -> Action
glBlendFunc s = primExTcl_ ["glBlendFunc",s]
glClear	s     = primExTcl_ ["glClear",s]
		
glCallList :: Int -> Action
glCallList i = primExTcl_ ["glCallList",show i]

glClearColor :: Color -> Int -> Action
glClearColor (RGB r g b) a = primExTcl_ $ "glClearColor":map f [r,g,b,a]
			     where f = show . (/255.0) . fromInt 

--glClearColor :: Float -> Float -> Float -> Float -> Action
--glClearColor r g b a = primExTcl_ $ "glClearColor":map show [r,g,b,a]

glClearDepth :: Float -> Action
glClearDepth i = primExTcl_ ["glClearDepth", show i]

glColor3f :: Color -> Action
glColor3f (RGB r g b) = primExTcl_ $ "glColor3f": map f [r,g,b]
			where f = show . (/255.0) . fromInt 

--glColor3f :: Float -> Float -> Float -> Action
--glColor3f r g b = primExTcl_ $ "glColor3f": map show [r,g,b]

glColor4f :: Float -> Float -> Float -> Float -> Action
glColor4f f1 f2 f3 f4 = primExTcl_ $ "glColor4f": map show [f1,f2,f3,f4]

glColor3fv, glColor4fv :: [Float] -> Action
glColor3fv fs = primExTcl_ $ "glColor3fv {":map show fs++["}"]
glColor4fv fs = primExTcl_ $ "glColor4fv {":map show fs++["}"]

glCullFace :: GlFaceModeType -> Action
glCullFace e = primExTcl_ ["glCullFace",show e]

glDisable, glEnable :: GlCapabilityType -> Action
glDisable e = primExTcl_ ["glDisable",show e]
glEnable e  = primExTcl_ ["glEnable",show e]

glFrustum :: Float -> Float -> Float -> Float -> Float -> Float -> Action
glFrustum f1 f2 f3 f4 f5 f6 = primExTcl_ $ "glFrustum":
					   map show [f1,f2,f3,f4,f5,f6]

glHint :: String -> Action
glHint s = primExTcl_ ["glHint",s]

glDepthFunc :: String -> Action
glDepthFunc s = primExTcl_ ["glDepthFunc",s]

glLightfv :: GlCapabilityType -> GlParamType -> [Float]-> Action
glLightfv e1 e2 fs = primExTcl_ $ "glLightfv":show e1:show e2:
				  "{":map show fs++["}"]

glColorMaterial :: GlFaceModeType -> GlParamType -> Action
glColorMaterial e1 e2 = primExTcl_ ["glColorMaterial",show e1,show e2]

glLineWidth :: Float -> Action
glLineWidth f = primExTcl_ ["glLineWidth",show f]

glLoadIdentity :: Action
glLoadIdentity = primExTcl_ ["glLoadIdentity"]
		
glMaterialf :: GlFaceModeType -> GlParamType -> Float -> Action
glMaterialf e1 e2 f = primExTcl_ ["glMaterialf",show e1,show e2,show f]

glMaterialfv :: GlFaceModeType -> GlParamType -> [Float] -> Action
glMaterialfv e1 e2 fs = primExTcl_ $ "glMaterialfv":show e1:show e2:
				     "{":map show fs++["}"]
				     
glMaterialiv :: GlFaceModeType -> GlParamType -> [Int] -> Action
glMaterialiv e1 e2 is = primExTcl_ $ "glMaterialiv":show e1:show e2:
				     "{":map show is++["}"]
				       
glMatrixMode :: GlMatrixModeType -> Action	
glMatrixMode e = primExTcl_ ["glMatrixMode",show e]

glNewList :: Int -> GlListType -> Action
glNewList s e = primExTcl_ ["glNewList",show s,show e]

glEndList :: Action
glEndList = primExTcl_ ["glEndList"]

glNormal3f :: Point3 -> Action
glNormal3f (x,y,z) = primExTcl_ $ "glNormal3f":map show [x,y,z]

--glNormal3f :: Float -> Float -> Float -> Action
--glNormal3f f1 f2 f3 = primExTcl_ $ "glNormal3f":map show [f1,f2,f3]
	
glOrtho :: Float -> Float -> Float -> Float -> Float -> Float -> Action
glOrtho f1 f2 f3 f4 f5 f6 = primExTcl_ $ "glOrtho":map show [f1,f2,f3,f4,f5,f6]

glPolygonMode :: GlFaceModeType -> GlPolygonModeType -> Action
glPolygonMode e1 e2 = primExTcl_ ["glPolygonMode",show e1,show e2]

glPopMatrix, glPushMatrix :: Action
glPopMatrix  = primExTcl_ ["glPopMatrix"]
glPushMatrix = primExTcl_ ["glPushMatrix"]

glRotatef :: Float -> Float -> Float -> Float -> Action
glRotatef f1 f2 f3 f4 = primExTcl_ $ "glRotatef":map show [f1,f2,f3,f4]
		
glScalef :: Float -> Float -> Float -> Action
glScalef f1 f2 f3 = primExTcl_ $ "glScalef":map show [f1,f2,f3]

glShadeModel :: GlShadeModelType -> Action
glShadeModel s = primExTcl_ ["glShadeModel",show s]

glTranslatef :: [Float] -> Action
glTranslatef fs = primExTcl_ $ "glTranslatef":map show fs

gluLookAt :: Float -> Float -> Float -> Float -> Float -> Float -> Float 
		   -> Float -> Float -> Action
gluLookAt f1 f2 f3 f4 f5 f6 f7 f8 f9 = primExTcl_ $ "gluLookAt":
				       map show [f1,f2,f3,f4,f5,f6,f7,f8,f9]
				       
gluOrtho2D :: Float -> Float -> Float -> Float -> Action
gluOrtho2D f1 f2 f3 f4 = primExTcl_ $ "gluOrtho2D":map show [f1,f2,f3,f4]

gluPerspective :: Float -> Float -> Float -> Float -> Action
gluPerspective f1 f2 f3 f4 = primExTcl_ $ "gluPerspective":
					  map show [f1,f2,f3,f4]
					  
glutWireCube, glutWireTeapot :: Float -> Action
glutWireCube f   = primExTcl_ ["glutWireCube",show f]
glutWireTeapot f = primExTcl_ ["glutWireTeapot",show f]

glutSolidSphere :: Float -> Int -> Int -> Action
glutSolidSphere f i1 i2 = primExTcl_ ["glutSolidSphere",show f,show i1,show i2]

glVertex2f :: Point2 -> Action
glVertex2f (x,y) = primExTcl_ $ "glVertex2f":map show [x,y]

--glVertex2f :: Float -> Float -> Action
--glVertex2f f1 f2 = primExTcl_ ["glVertex2f",show f1,show f2]

glVertex3f :: Point3 -> Action
glVertex3f (x,y,z) = primExTcl_ $ "glVertex3f":map show [x,y,z]

--glVertex3f :: Float -> Float -> Float -> Action
--glVertex3f f1 f2 f3 = primExTcl_ $ "glVertex3f":map show [f1,f2,f3]

glViewport :: Int -> Int -> Int -> Int -> Action
glViewport i1 i2 i3 i4 = primExTcl_ $ "glViewport":map show [i1,i2,i3,i4]

doNothing :: Action
doNothing = primExTcl_ [""]
