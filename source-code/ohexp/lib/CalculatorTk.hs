module CalculatorTk where

import Tk
import TkUtil

struct Calculator =
   digit :: Char -> Action
   enter :: Action
   op    :: (Int -> Int -> Int) -> Action
   reset :: Action

calculator display err = 
   template
      num   := []
      stack := []
   in let
      digit n = action
         num := n:num
         display (reverse num)

      enter = action enter'

      enter' = do
         if num /= [] then
            stack   := read (reverse num):stack
            num     := []

      op f = action
         enter'
         case stack of
            a:b:rest -> let res = f b a 
                        stack := res:rest
                        display (show res)
            _        -> err

      reset = action
         num     := []
         stack   := []
         display "0"

      in struct ..Calculator      
         
cmd calc 'C'         = calc.reset
cmd calc 'E'         = calc.enter
cmd calc '+'         = calc.op (+)
cmd calc '-'         = calc.op (-)
cmd calc '*'         = calc.op (*)
cmd calc '/'         = calc.op div
cmd calc c|isDigit c = calc.digit c
          |otherwise = error "illegal operation"


main tk = do 
   win  <- tk.window  [Title "Calculator"]
   lab  <- win.label  [Text "0", Font "Helvetica 20", 
                       Width 13, Background white, Anchor E]
   quit <- win.button [Text "Quit", Foreground red, Command tk.quit]
   calc <- calculator (\str -> lab.set [Text str]) tk.bell
   let mkBut c = win.button [Text [c], Command (cmd calc c), 
                             Width 3, Font "Helvetica 16"]
   bs   <- mapM mkBut "123+456-789*0CE/"     
   pack ((quit <<< lab) ^^^ matrix 4 bs)
             
