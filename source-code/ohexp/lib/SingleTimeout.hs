module SingleTimeout where

-- The SingleTimeout manages the common situation where at most one
-- outstanding timeout is wanted at any time. An outstanding timeout
-- can be cancelled. If this is attempted before the timeout action is
-- scheduled, it is guaranteed that it is not scheduled at all. A new
-- timeout request cancels an old, if there is one.

{-
    action to <- singletimeout
           to.after s (action t)
           to.cancel
           done
    ==
    action done


    action to <- singletimeout
           to.after s (action t)
           to.after s2 (action t2)
    ==
    action to <- singletimeout
           to.after s2 (action t2)
-}

struct SingleTimeout a =
    after  :: TimeDiff -> Action -> Request ()
    cancel :: Request Bool -- False if already scheduled

singletimeout = 
   template
        lastmsg := Nothing
   in let
        abortlast = do case lastmsg of
                         Just m  -> m.abort
                         Nothing -> return True
   in struct
        after t a = request
                    abortlast
                    m <- tag (after t a)
                    lastmsg := Just m
        cancel = request
                    abortlast
