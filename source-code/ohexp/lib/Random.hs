module Random where

struct RandomGenerator a = 
           next :: Request a

natGenerator :: Num a => Int -> Template (RandomGenerator a)
natGenerator seed = 
    template 
      state := f (f seed)
    in struct
         next = request
                  state := f state
                  return (fromInt state)
   where a = 16807                   -- = 7^5
         m = 2147483647              -- = maxBound = 2^31-1
         q = 127773                  -- = m `div` a
         r = 2836                    -- = m `mod` a
         f n = if tmp<0 then m+tmp else tmp
                 where tmp = a*(n `mod` q) - r*(n `div` q)
			     
uniformGenerator :: Int -> Template (RandomGenerator Float)
uniformGenerator seed =
  do r <- natGenerator seed
     return {next = do n <- r.next
                       return(fromInt n/fromInt maxBound)
            }

expGenerator :: Float -> Int -> Template (RandomGenerator Float)
expGenerator avg seed =
  do r <- uniformGenerator seed
     return {next = do u <- r.next
                       return(-avg*log u)
            }

