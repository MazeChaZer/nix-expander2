import Tk
import TkUtil

mkeditor tk file = do
  w  <- tk.window [Title "Editor"]
  f  <- w.frame []
  ed <- f.textEditor [Width 80, Height 40, Background (rgb 200 220 240), 
                      Wrap NoWrap, Font "courier 10"]
  scred <- addScrollbars f ed
  scred.set [BorderWidth 3, Relief Raised]
  lab <- w.label []
  f <- tk.readFile file 
  let lns = lines f
  forall l <- lns do ed.insertLines maxBound [l]
  q  <- w.button [Text "Close", Command w.destroy]
  l  <- w.button [Text "First Line", 
                  Command (do s <- ed.getLine 1; lab.set [Text s])]
  i  <- w.button [Text "Delete First Line", Command (ed.deleteLine 1 )]
  pack (scred ^^^ fillX (rigid(row[q,l,i])<<<lab))
  


files = ["Balls.hs","Calculator.hs", "Colors.hs", "Fifteen.hs", 
         "Fractal.hs", "OPaint.hs", "Scribbler.hs", "Sources.hs"]

main tk = do
  win <- tk.window [Title "Tk Example files"]
  header <- win.label [Text "Double-click to see\nsource code"]
  lb <-  win.listBox []
  lb.insertLines 0 files
  q  <- win.button [Text "Quit", Command tk.quit]
  let openSelection = do
    ls <- lb.getValue
    mkeditor tk (files!!(head ls))
  pack (header ^^^ (lb <<< q))
  lb.bind [Double (ButtonPress 1 (\_ -> openSelection))]

