import Tk
import TkUtil

w = 600 
h = 400 

data Shape = L | R | RF | O | OF deriving Show

struct Painting =
  initShape :: Pos   -> Action
  dragShape :: Pos   -> Action
  setColor  :: Color -> Action
  setShape  :: Shape -> Action
  undo      :: Action
  new       :: Action

painting :: Canvas -> Template Painting
painting canv = 
 template
   os := []
   ip := (0,0)
   shape := L
   currcol := blue
 in let 
      f col L  p1 p2 = canv.line [p1,p2] [Fill col, Width 2]
      f col R  p1 p2 = canv.rectangle p1 p2 [Outline col, Width 2]
      f col RF p1 p2 = canv.rectangle p1 p2 [Outline col, Fill col]
      f col O  p1 p2 = canv.oval p1 p2 [Outline col, Width 2]
      f col OF p1 p2 = canv.oval p1 p2 [Outline col, Fill col]

      g col s p1 p2 = do
         obj <- f col s p1 p2
         obj.bind [ButtonPress 3 (\_ -> obj.raise), ButtonPress 2 initMove,
                   Motion 2 (move obj)]
         return obj

      initShape p  = action
        r <- g currcol shape p p
        os := r:os
        ip := p

      dragShape p = action
        (head os).destroy
        r1 <- g currcol shape ip p
        os := r1:tail os

      setColor c = action currcol := c

      setShape s = action shape := s

      undo = action
        if not(null os) then
           (head os).destroy
           os := tail os

      new = action
        forall o <- os do
           o.destroy
        os := []

      initMove p = action ip := p

      move obj p@(x',y') = action 
        let (x,y) = ip
        obj.move (x'-x,y'-y)
        ip := (x',y')

   in struct ..Painting

cols = [blue, RGB 0 0 200, RGB 0 0 150,
        green, RGB 0 200 0, RGB 0 150 0,
        yellow, RGB 0 200 200, RGB 0 150 150,
        red, RGB 200 0 0, RGB 150 0 0,
        white, RGB 128 128 128, black]


mkTool win label cmd bmp cols xs = do 
   f <- win.frame [BorderWidth 4, Relief Raised]
   tools <- f.label [Text label]
   let toolbut s = do 
          b <- bmp s
          return [Img b, Command (cmd s), 
                  Indicatoron False, SelectColor Nothing]
   toolbs <- mapM toolbut xs
   r <- radio f (zip toolbs [1..]) [] cols
   r.setValue (Just 1)
   pack (tools ^^^ r)
   return f

main tk = do
  win  <- tk.window [Title "OPaint"]
  canv <- win.canvas [Background white, Width w, Height h,
                      Relief Ridge, BorderWidth 6, Cursor "crosshair"]
  pnt  <- painting canv 
  let save = dialog2 tk "Save as" "Save as:" "opaint.ps" canv.save 
  mb <- menubar win [("File",[("New",pnt.new),("Save",save),("Quit",tk.quit)]),
                     ("Edit",[("Undo",pnt.undo)])]

  f1 <- mkTool win "Shapes" pnt.setShape  
       (\s -> tk.bitmap [File ("bitmaps/"++show s++".bitmap")]) 1 [L,R,RF,O,OF]
  f2 <- mkTool win "Colors" pnt.setColor 
       (\c -> tk.bitmap [File ("bitmaps/frame.bitmap"), Background c]) 3 cols
  pack (mb ^^^ (canv <<< rigid (f1 ^^^ f2)))
  
  canv.bind [ButtonPress 1 pnt.initShape, Motion 1 pnt.dragShape] 
