import Tk

port :: Port
port = Port 1984

client name env peer =   
  template
     w := undefined
     e := undefined
  in let connect = action
           w' <- env.window [Title name]
           w := w'
           e' <- w.entry []
           e := e'
           hl <- w.label []
           env.inet.getNameOfHost peer.host $ \ms -> action
                hl.set [Text ("Host: " ++ maybe (inet_ntoa peer.host) id ms)]
           let Port p = peer.port
           pl <- w.label [Text ("Port: " ++ show p)]
           b <- w.button [Text "Send", Command (e.getValue >>= peer.deliver)]
           q <- w.button [Text "Quit", Command finish]
           pack (col[hl,pl,e',b<<<q])
         close   = action w.destroy
         deliver d | d == closeCmd = action e.setValue d; peer.close
                   | otherwise     = action e.setValue d
         neterror (NetError n) = action e.setValue ("Error: "++n++"")
         finish = action peer.deliver closeCmd
                         peer.close
         closeCmd = "close\n"
     in struct ..Client

main env = do
  env.inet.udp.open (HostName "localhost") port (client "Client 1" env)
  env.inet.udp.open (HostName "localhost") port (client "Client 2" env)
  env.inet.udp.listen port (client "Server" env)
  done
