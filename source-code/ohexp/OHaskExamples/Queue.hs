module Queue where

struct Queue a =
   insert   :: a -> Action
   announce :: (a -> Action) -> Action
   
queue =
   template
      packets := []
      servers := []
   in struct
      insert p = action
         case servers of
            []   -> do packets := packets ++ [p]
            s:ss -> do s p; servers := ss
      announce s = action
         case packets of
            []   -> do servers := servers ++ [s]
            p:ps -> do s p; packets := ps

