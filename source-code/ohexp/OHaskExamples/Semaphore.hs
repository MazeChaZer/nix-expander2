module Semaphore where

struct Semaphore =
   claim   :: Action -> Action
   release :: Action

semaphore =
   template
      active := True
      wakeup := []
   in struct
      claim grant = action
         if not active then
            active := True
            grant
         else
            wakeup := wakeup ++ [grant]
      release = action
         case wakeup of
            []   -> active := False
            w:ws -> w; wakeup := ws

