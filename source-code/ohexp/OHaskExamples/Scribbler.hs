module Scribbler where

import Tk

struct Scribbler = 
   penUp   :: Int -> Pos -> Action
   penDown :: Int -> Pos -> Action
   movePen :: Pos -> Action

scribbler :: Canvas -> Template Scribbler 
scribbler canv = 
   template
     pos := Nothing
     col := red
   in struct
       penUp   _   _ = action pos := Nothing
       penDown but p = action (pos,col) := (Just p,color but)
       movePen     p = action
          case pos of 
            Nothing -> done
            Just p' -> canv.line [p,p'] [Width 3, Fill col]; pos := Just p

color 1 = red
color 2 = green 
color _ = blue


main tk = do   
  win   <- tk.window [Title "Scribbler"]
  canv  <- win.canvas [Background white]
  quit  <- win.button [Text "Quit", Command tk.quit]
  clear <- win.button [Text "Clear", Command canv.clear]
  win.setPosition (200,200)
  win.setSize (500,500)
  pack (canv ^^^ rigid (clear <<< quit))
  s <- scribbler canv
  canv.bind [AnyButtonPress   s.penDown,
             AnyButtonRelease s.penUp,
             AnyMotion        s.movePen]
