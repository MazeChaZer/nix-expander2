module CardPile where

import Tk
import Card

dy = 25

struct Stack a = 
  size    :: Request Int
  push    :: a -> Action
  top     :: Request a
  pop     :: Request a
  mkEmpty :: Action

struct CardPile < Stack Card =
   canTake :: Card -> Request Bool
	    

cardPile ::([Card] -> Card -> Bool) -> 
             Bool -> Pos -> Template CardPile 
cardPile ct shift (x,y) =  
  template 
    cards := []
   in struct
       size   = request return (length cards)
       push c = action 
          cards := c:cards
          c.setCoords [(x,if shift then y+dy*(length cards-1) else y)]
          c.raise
       top    = request return (head cards)
       pop    = request
          case cards of
            c:cs -> cards := cs
                    return c
       mkEmpty = action cards := []
       canTake card = request return(ct cards card)


defaultPile ::  Pos -> Template CardPile
defaultPile = cardPile defCT False 
    where defCT _ _ = False


tableauPile :: Pos -> Template CardPile 
tableauPile = cardPile tabCT True 
    where tabCT cards card = 
            case cards of
              []  -> card.rank == 13
              c:_ -> c.rank == card.rank+1 && c.color /= card.color


suitPile :: Pos -> Template CardPile 
suitPile = cardPile suitCT False
   where suitCT cards card = 
            case cards of
              []  -> card.rank == 1
              c:_ -> c.rank == card.rank-1 && c.suit == card.suit
 
