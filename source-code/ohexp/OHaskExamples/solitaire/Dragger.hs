module Dragger where

import Tk
import CardPile

struct Dragger  < Stack Card =
  init   :: Pos -> Stack Card -> Action
  dragTo :: Pos -> Action
  getSrc :: Request(Stack Card)

dragger :: Template Dragger 
dragger = 
  template
    items  := []
    pos    := undefined
    source := undefined
  in struct
       size   = request return (length items)
       push i = action 
         items := i:items
         forall c <- items do c.raise
       top    = request return (head items)
       pop    = request
         case items of
            i:is -> items := is
                    return i
       mkEmpty = action items := []

       init p src = action 
          pos := p
          source := src
       dragTo (x1,y1) = action 
         let (x,y) = pos
             pd = (x1-x,y1-y)
         forall c <- items do  
             c.move pd
         pos := (x1,y1)
       getSrc = request return source
