import Tk
import Card
import CardPile
import CardBindings
import Dragger
import Random
import Array
import TkUtil

w = 73
h = 97

main env = do
  win  <- env.window [Title "Solitaire"]
  canv <- win.canvas [Background (rgb 0 160 0), Width 700, Height 500]
  forall (x,y) <- [(14+n*100,152)| n <- [0..6]]++
                  [(14+n*100,22)|n <- [0..3]]++
                  [(514,22)] do
     canv.rectangle (x,y) (x+w,y+h-1) [Fill (rgb 0 130 0)]
  deckRect <- canv.rectangle (614,22) (687,118) [Fill (rgb 0 130 0)]
  
  tps  <- mapM (\n -> tableauPile (50+n*100,200)) [0..6]
  sps  <- mapM (\n -> suitPile (50+n*100,70)) [0..3]
  dump <- defaultPile (550,70)
  deck <- defaultPile (650,70) 
  let piles = listArray (1,13) (tps++sps++[dump,deck])

  tod <- env.timeOfDay
  let seed = fromInteger (tod `mod` 2^31)
  gen  <- natGenerator seed
  dr   <- dragger 
  bim  <- env.photo [File (pictureDirectory++"back.gif")]
  let mkCard (r,s) = card canv env bim r s
  cards <- mapM mkCard [(r,s)| r <- [1..13], 
                               s <- [Spades,Hearts,Diamonds,Clubs]]
  deckRect.bind [ButtonPress 1 (emptyDeckPress gen piles dr dump)]
  mb <- menubar win [("Game",[("New",newGame gen cards dr piles),
                              ("Help",help env),
                              ("Quit",env.quit)])]
  pack (mb ^^^ canv)


deal piles dr = do
  let dp = piles!13
  forall i <- [1..7] do
    forall j <- [1..8-i] do
      c <- dp.pop
      (piles!j).push c
      c.bind [ButtonPress 1 (\_ -> done)]
    c <- (piles!(8-i)).top
    c.faceUp
    c.bind (faceBindings piles dr c)

newGame gen cards dr piles = do
  shuffleToPile gen cards dr piles
  forall i <- [1..12] do (piles!i).mkEmpty
  dr.mkEmpty
  deal piles dr


help env = do
  win <- env.window [Title "Solitaire Help"]
  ed  <- win.textEditor [Background white, Font "helvetica 12",
                         Height 32, Width 70]
  helpText <- env.readFile "help.txt"
  forall l <- lines helpText do
    ed.insertLines maxBound [l] 
  ed.set [Enabled False]
  close <- win.button [Text "Close", Command win.destroy]
  pack (ed ^^^ close)
