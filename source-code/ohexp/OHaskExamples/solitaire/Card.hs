module Card where

import Tk

pictureDirectory = "tkExamples/solitaire/cards/"

data Suit = Spades | Hearts | Diamonds | Clubs deriving (Eq,Ord,Enum)

type Rank = Int 

rs = "a23456789tjqk"   -- first letter of image file name 
ss = "shdc"            -- second           -"-

struct Card < CWidget Img =
   rank     :: Rank
   suit     :: Suit
   color    :: Color
   faceUp   :: Action
   faceDn   :: Action
   isFaceUp :: Request Bool

card :: Canvas -> Tk -> Image -> Rank -> Suit -> Cmd Card
card canv env backImage r s = do
  faceImage <- env.photo [File (pictureDirectory++
                                (rs!!(r-1):ss!!fromEnum s:".gif"))]
  cim       <- canv.image (-100,-100) [Img faceImage] 
  template
    up := True
   in struct
       rank = r
       suit = s
       color = if s==Spades || s==Clubs then black else red
       faceUp = action
          cim.set [Img faceImage]
          up := True
       faceDn = action
          cim.set [Img backImage]
          up := False
       isFaceUp  = request return up
       ident     = rs!!(r-1):show(fromEnum s)
       destroy   = cim.destroy
       exists    = cim.exists
       set       = cim.set 
       raise     = cim.raise
       lower     = cim.lower
       focus     = cim.focus
       bind      = cim.bind 
       setCoords = cim.setCoords 
       getCoords = cim.getCoords 
       move      = cim.move

