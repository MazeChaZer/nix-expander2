module CardBindings where

import Random

import Array
import Card
import CardPile
import Dragger
import List

faceBindings :: Array Int CardPile -> Dragger -> Card -> [Event]
faceBindings piles dr c =  [ButtonPress 1 (facePress piles dr c),
                            Motion 1 (faceMotion dr),
                            ButtonRelease 1 (faceRelease piles dr),
                            ButtonPress 2 (facePress2 piles dr c)]
     

emptyDeckPress gen piles dr dump _ = do
  n <- dump.size
  cs <- mapM (const dump.pop) [1..n]
  shuffleToPile gen cs dr piles

turnTopFaceUp pile piles dr = do 
   s <- pile.size
   if s > 0 then
      t <- pile.top
      t.faceUp
      t.bind (faceBindings piles dr t)

backPress piles dr pos = do 
   c <- (piles!13).pop
   c.faceUp
   c.bind (faceBindings piles dr c)
   (piles!12).push c

facePress piles dr card pos  = do
   let n = pile pos
       copyToDragger = do 
          c <- (piles!n).pop
          dr.push c
          if c.ident /= card.ident then copyToDragger
   dr.init pos (piles!n)
   copyToDragger
     
facePress2 piles dr card pos = do
    let n = pile pos
        try 12 = done
        try k = do 
           ok <- (piles!k).canTake card
           if ok then                
             (piles!n).pop
             (piles!k).push card
             turnTopFaceUp (piles!n) piles dr
           else
             try(k+1)
    if n <=7 || n==12 then 
       c <- (piles!n).top
       if card.ident == c.ident then try 8

faceMotion dr pos = dr.dragTo pos

faceRelease piles dr pos = do
    let n = pile pos
        copyFromDragger target s = do
          forall _ <- [1..s] do
            c <- dr.pop
            target.push c
    src <- dr.getSrc
    s   <- dr.size
    t   <- dr.top
    if n > 0 then 
       ok  <- (piles!n).canTake t
       if ok then
          copyFromDragger (piles!n) s
          turnTopFaceUp src piles dr 
       else
          copyFromDragger src s
    else 
        copyFromDragger src s


shuffleToPile gen deck dr piles = do
  rnds <- mapM (const gen.next) [1..52]
  let cmp a b   = compare (snd a) (snd b)
      (deck',_) = unzip (sortBy cmp (zip deck rnds))
  forall c <- deck' do
     (piles!13).push c
     c.faceDn
     c.bind [ButtonPress 1 (backPress piles dr)]


pile (x,y) 
  |y > 150 && x < 700 = x `div` 100 + 1
  |abs (y-70) < 50 && x < 400 = x `div` 100 + 8
  |abs (y-70) < 50 && x > 500 && x < 700 = x `div` 100 + 7
  |otherwise = 0


