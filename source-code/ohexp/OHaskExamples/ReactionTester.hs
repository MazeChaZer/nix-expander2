{-

This file contains a simple reactive program: a reaction time tester.
The GUI should be self-explanatory: when  pressing Start, 
the lamp turns red for a few seconds, after which it turns Green. 
The user's task is to press Stop as soon as possible after this happens. 
The reaction time in seconds (and hundreds of seconds) is then displayed.

The Start button is disabled during this cycle. To prevent cheating by
continuous clicking on Stop, this button is *not* disabled; instead the
user is accused of cheating when clicking Stop prematurely.

Some interesting(?) aspects:
- the use of the tag returned from delay to get rid of spurious timeouts 
  because of (repeated) cheating; 
- the building of a Lamp from an Oval without the need of an extra
  adapter object.
- two alternative Testers; one by pattern matching on the State in
  the methods and one using the State design pattern (cf also the POTS
  example).
- the use of bind on the Stop button to react to the user's *press* of
  the button (rather than the release as when using the Command option).
- even though the help dialog is modal (the user must press OK before 
  interacting further with the program), it does not impair the
  reactivity of the application, ie its ability to react to other 
  events: if Help is pressed directly after Start, the lamp still turns 
  green after a few seconds.

-} 

import Tk
import TkUtil
import Random

struct Lamp =
  setBlack, setRed, setGreen :: Action

struct Tester < Runnable

data State = Idle | Holding | Running

tester delay randInt timeOfDay lamp display startBut =
  template
    state := Idle
    tag   := undefined
    startTime := undefined
  in let
       start = action
         display ""
         r <- randInt
         t <- delay (3000+r `mod` 4000) timeout
         tag := t
         state := Holding
         startBut.set [Enabled False]
         lamp.setRed
       stop = action
         stopTime <- timeOfDay
         case state of
           Idle    -> done
           Holding -> display "Cheating! Wait for green."
                      mkIdle
           Running -> 
                      display(interval stopTime startTime)
                      mkIdle
       timeout t = action
            case state of
             Holding ->
               if t==tag then 
                 state := Running
                 lamp.setGreen
                 st <- timeOfDay
                 startTime := st
             _ -> done 
       mkIdle = do
          lamp.setBlack
          startBut.set [Enabled True]
          state := Idle
      in struct ..Tester

-- this  auxiliary function is non-local since it is
-- used in both versions of tester.
interval us2 us1 =
  let n = fromInteger (us2-us1) `div` 10000
      r = n `mod` 100
  in if r < 10 
     then show (n `div` 100)++ ".0"++show r
     else show (n `div` 100)++ '.':show r


lamp :: Oval -> Lamp
lamp ov = struct
  setBlack = ov.set [Fill black]
  setRed   = ov.set [Fill red]
  setGreen = ov.set [Fill green]
  

help env = messageDialog env "Help" 
            (unlines ["Press Start; wait for green; press Stop.",
                      "Read reaction time.",
                      "Repeat until bored."])
             done

main env = do
  win  <- env.window [Title "Reaction Time Tester"]
  canv <- win.canvas [Width 60,Height 60, Relief Groove, BorderWidth 6]
  ov   <- canv.oval (16,16) (56,56) [Fill black]
  lab  <- win.label [Font "helvetica 20", Background white, Anchor E,
                     Relief Ridge, BorderWidth 4]
  startBut <- win.button [Text "Start"]
  stopBut  <- win.button [Text "Stop"]
  quitBut  <- win.button [Text "Quit", Command env.quit]
  helpBut  <- win.button [Text "Help", Command (help env)]
  tod <- env.timeOfDay
  let seed = fromInteger (tod `mod` 2^31)
  ran    <- natGenerator seed
  tester <- tester env.delay ran.next env.timeOfDay 
                   (lamp ov) (\s -> lab.set [Text s]) startBut
  
  startBut.set [Command tester.start]
  stopBut.bind [ButtonPress 1 (\_ -> tester.stop)] 

  pack ((lab ^^^ row [quitBut, startBut, stopBut,helpBut]) <<< canv)


{-

Below follows an alternative tester following the State design pattern.

struct S = 
  start', stop' :: O (String,(Int,Int),S) ()
  timeout' :: String -> O (String,(Int,Int),S) ()

tester delay randInt timeOfDay lamp display startBut =
  template
    tag   := undefined
    startTime := undefined
    state := 
      let 
        start'     = done
        stop'      = done
        timeout' _ = done
        
        idle = struct
          start' = do 
            display ""
            r <- randInt
            t <- delay (3000+r `mod` 4000) (\t -> action state.timeout' t)
            tag := t
            startBut.set [Enabled False]
            lamp.setRed
            state := holding
          ..S

        holding = struct
          stop' = do
            display "Cheating! Wait for green."
            mkIdle
          timeout' t = do
            if t==tag then 
               lamp.setGreen
               st <- timeOfDay 
               startTime := st
               state := running
          ..S

        running = struct
          stop' = do
            stopTime <- timeOfDay 
            display(interval stopTime startTime)
            mkIdle 
          ..S

        mkIdle = do
          lamp.setBlack
          startBut.set [Enabled True]
          state := idle

      in idle     
  in struct
       start = action state.start'
       stop  = action state.stop'


-}
