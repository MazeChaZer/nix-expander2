module Channel where

struct Channel a =
   canRead  :: (a -> Action) -> Action
   canWrite :: Request a -> Action

channel = 
   template
      readers := []
      writers := []
   in struct
      canRead r = action
         case writers of
            []   -> do readers := readers ++ [r]
            w:ws -> do x <- w
                       r x
                       writers := ws
      canWrite w = action
         case readers of
            []   -> do writers := writers ++ [w]
            r:rs -> do x <- w
                       r x
                       readers := rs

