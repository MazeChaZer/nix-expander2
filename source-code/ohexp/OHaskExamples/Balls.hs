import Random
import Tk

w = 300 
h = 300 
rad = 20
acc = 2

struct Ball = 
  moveBall  :: Action

ball :: Oval -> Pos -> Int -> Template Ball
ball ov (x',y') dx' = 
       template
         (x,y,dx,dy) := (x',y',dx',0)
       in struct 
            moveBall = action
              ov.move (dx,dy)
              let x1 = x+dx
                  y1 = y+dy
              (x,y,dx,dy) := (x1,
                              y1,
                              if x1<rad && dx<0 || x1>w-rad && dx>0 
                                 then -dx else dx,
                              if y1<rad && dy<0 || y1>h-rad && dy>0 
                                 then -dy else dy+acc)


struct BallWorld =
  tick :: Action
  addBall :: Ball -> Action
  clearWorld :: Action
 
ballWorld :: Template BallWorld
ballWorld = 
  template
     balls := []
  in struct
      tick       = action forall b <- balls do b.moveBall
      addBall b  = action balls := b:balls
      clearWorld = action balls := []


main tk = do
   win  <- tk.window [Title "Balls"]
   canv <- win.canvas [Background white, Width w, Height h]
   rand <- natGenerator 13
   w <- ballWorld
   let mkBall (x,y) = do 
         rcol <- rand.next 
         let (col,dx) = f rcol 
         ov <- canv.oval (x-rad,y-rad) (x+rad,y+rad) [Fill col]
         b <- ball ov (x,y) dx
         w.addBall b
       f rand = (rgb r g b,dx) where
           r = rand `mod` 256
           g = rand `div` 256 `mod` 256
           b = rand `div` 256 `div` 256 `mod` 256
           dx = (rand `mod` 13 - 4)
   quit <- win.button [Text "Quit", Command tk.quit]
   pack (canv ^^^ rigid quit)
   canv.bind [ButtonPress 1 mkBall]
   sch <- tk.periodic 20 w.tick
   sch.start

