module Sieve1 where

struct Cell = 
   feed  :: (Int -> Action) -> Int -> Action

cell n = 
  template
     next  := Nothing
  in struct
     feed print k = action
        if k `mod` n /= 0 then
           case next of
              Nothing ->
                 c <- cell k
                 next := Just c
                 print k
              Just c  ->
                 c.feed print k


root print n = do c <- cell 2
                  forall i <- [3..n] do
                     c.feed print i

main env = root (env.putStr . (++" ") . show) 1000
