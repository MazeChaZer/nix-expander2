telnet :: StdEnv -> Peer -> Template Client
telnet env peer =
   template
      line := ""
   in let
      getChar '\n' = action
	    peer.deliver (reverse line++"\n")
	    line := ""
	    env.putChar '\n'
      getChar '\b' = action
            if not (null line) then 
               env.putStr "\b "
            line := drop 1 line
      getChar ch = action
            line := ch:line
	    env.putChar ch

      connect = action
	    env.setReader getChar
            env.inet.getNameOfHost peer.host connect2

      connect2 ms = action
            env.putStr ("[Connected to " ++ 
                        maybe (inet_ntoa peer.host) id ms ++ 
                        ", port " ++ show p ++ "]\n")
              where Port p = peer.port

      close = action 
            env.putStr "[Disconnected]\n"
            env.quit

      deliver s = env.putStr s

      neterror (NetError e) = env.putStr ("[Net error: " ++ e ++ "]\n")

   in struct ..Client

port :: Port
port = Port 12345
