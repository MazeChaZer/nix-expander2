module Concur where

 struct Pair = a :: Int; b :: Bool

 o = struct a = 111; b = False

 struct Record = print :: String -> Action
		 printc :: String -> Action -> Action
		 skip :: Action
                 f :: Int -> Request Int	 
	
 cl :: StdEnv -> Template Record
 cl env = template ready := False 
          in let print str = action env.putStr str
    	         printc str act = action env.putStr str; act
	         skip = action done
	         mult1 x y = return (if x == 0 then 0 else x*y)
	         mult2 x y = return (if y == 0 then 0 else x*y)
                 f 0 = return 0
                 f x = do y <- f (x-1)
			  if y == 0 then return 0 else z <- f (x+1); mult1 y z
             in struct ..Record
			     			  
 main env = do 	o1 <- cl env; o2 <- cl env
		o1.print "a1 "
		o1.print "a1 "
		o1.print "b1 "
		o1.print "b1 "
		o2.print "b2 "
		o2.print "b2 "
		o2.print "c2 "
		o2.print "c2 "

 main2 env = do o1 <- cl env; o2 <- cl env
		o1.printc "a1 " 
	         (o1.printc "a1 " 
                   (o1.printc "b1 "
		    (o1.printc "b1 "
		     (o2.printc "b2 "
		      (o2.printc "b2 "
		       (o2.printc "c2 "
		        (o2.printc "c2 " o2.skip)))))))

 main3 env = do o <- cl env
		x <- o.f 5
	        o.print (show x)

-- main  --->  a1 b2 a1 b2 b1 c2 b1 c2
-- main2 --->  a1 a1 b1 b1 b2 b2 c2 c2
