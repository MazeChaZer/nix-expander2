import Tix
import TkUtil

struct ColorManager = 
  setRed, setGreen, setBlue :: Int -> Action
  setRGB :: (Int,Int,Int) -> Action
  getColor :: Request Color
  getRGB   :: Request (Int,Int,Int)

colorManager :: Template ColorManager
colorManager = 
  template 
    (r,g,b) := (0,0,0) 
  in struct
      setRed   newr = action r := newr
      setGreen newg = action g := newg
      setBlue  newb = action b := newb
      setRGB   rgb  = action (r,g,b) := rgb
      getColor      = request return (rgb r g b)
      getRGB        = request return (r,g,b)
 




sliderChooser canv win cm = do
   let updateCanvas cmd n = do
         cmd n
         col <- cm.getColor
         canv.set [Background col]

       colSlider col cmd = 
         win.slider [From 0, To 255, Background col, Orientation Hor, 
                     CmdInt (updateCanvas cmd), Relief Ridge]

   sr   <- colSlider red   cm.setRed
   sg   <- colSlider green cm.setGreen
   sb   <- colSlider blue  cm.setBlue
   sb.set [Foreground white]

   let updateSliders = do
         (r,g,b) <- cm.getRGB
         sr.setValue r
         sg.setValue g
         sb.setValue b

   pack (fillX(col [sr,sg,sb]))
   win.set [RaiseCmd updateSliders]

spinBoxChooser canv win cm = do
   let updateCanvas cmd n = do
         cmd n
         col <- cm.getColor
         canv.set [Background col]

       colSpinBox col cmd = do
         cntrl <- win.spinBox [Min 0, Max 255, CLabel col, Step 5,
                               CmdInt (updateCanvas cmd), Relief Ridge]
         cntrl.subLabel.set [Width 8]
         cntrl.subLabel.set [Anchor E]
         return cntrl

   sr   <- colSpinBox "Red"   cm.setRed
   sg   <- colSpinBox "Green" cm.setGreen
   sb   <- colSpinBox "Blue"  cm.setBlue

   let updateControls = do
         (r,g,b) <- cm.getRGB
         sr.setValue r
         sg.setValue g
         sb.setValue b
       
   pack (rigid(col[sr,sg,sb]))
   win.set [RaiseCmd updateControls]


listBoxChooser canv win cm (rgbs,cnames) env = do
  slb <- win.scrolledListBox []
  let lb = slb.subListBox
  forall c <- cnames do lb.insertLines maxBound [c]

  let updateCanvas = do
        ns <- lb.getValue
        let (r,g,b) = rgbs!!(head ns)
        cm.setRGB (r,g,b)
        canv.set [Background (rgb r g b)]

  slb.set [Command updateCanvas]

  let updateListBox = do
        c <- cm.getRGB
        let (n,(r,g,b)) = findCol c rgbs 0
        cm.setRGB (r,g,b)
        lb.setValue [n]
        lb.view n
        canv.set [Background (rgb r g b)]
        
  pack slb 
  win.set [RaiseCmd updateListBox]

radioChooser canv win cm (rgbs,cnames) env = do
   bmp <- env.bitmap [File "frame.bitmap"]
   let updateCanvas n = do
        let (r,g,b) = rgbs!!n
        cm.setRGB (r,g,b)
        canv.set [Background (rgb r g b)]

   rad <- radio win ([([Img bmp, Indicatoron False, SelectColor Nothing, Background (rgb r g b), Command (updateCanvas n)],n) | (c@(r,g,b),n) <- zip rgbs [0..]]) [] 12
   rad.setValue (Just 0)

   let updateRadio = do
        c <- cm.getRGB
        let (n,(r,g,b)) = findCol c rgbs 0
        cm.setRGB (r,g,b)
        rad.setValue (Just n)
        canv.set [Background (rgb r g b)]

   pack (rigid rad)
   win.set [RaiseCmd updateRadio]

        
main env = do   
  win <- env.tixWindow [Title "Color Demo"]
  cm  <- colorManager

  pw  <- win.panedWindow []
  pack pw

  nbPane    <- pw.pane [ExpandRate 1, Min 300]
  canvPane  <- pw.pane [ExpandRate 1, Min 250]
  
  canv <- canvPane.canvas [Background black]
  qBut <- canvPane.button [Text "Quit", Command env.quit]
  pack (canv ^^^ rigid qBut)

  button <- nbPane.button []
  nb  <- nbPane.noteBook []
  pack nb

  pg1 <- nb.page [CLabel "Sliders"]
  sliderChooser canv pg1 cm

  pg2 <- nb.page [CLabel "SpinBoxes"]
  spinBoxChooser canv pg2 cm

  db <- env.readFile "rgb.txt"
  let (rgbs,cnames) = unzip (map mkPair (lines db))
      mkPair line = ((read r,read g,read b),unwords rest) 
                         where r:g:b:rest = words line

  pg3 <- nb.page [CLabel "ListBox"]
  listBoxChooser canv pg3 cm (rgbs,cnames) env
  
  pg4 <- nb.page [CLabel "Radiobuttons"]
  radioChooser canv pg4 cm (rgbs,cnames) env
  
close1 c c' = (c < 120 && c' < 100) || abs(c-c') < 20
close (r,g,b) (r',g',b') = close1 r r' && close1 g g' && close1 b b'

findCol c [] _ = (0,(250,250,250))
findCol c (d:ds) n|close c d = (n,d)
                  |otherwise = findCol c ds (n+1)
