module PopupDemo where

import Tix

main tix = do
  win   <- tix.tixWindow [Title "Popup Menu Demo"]
  canv  <- win.canvas [Background white]
  canv.text (175,125) [Text "Click right button for menu", 
                       Font "helvetica 20"]
  pp    <- tix.popupMenu [Title "PopupEx"]
  let mn = pp.subMenu
  mn.mButton [CLabel "Quit", Command tix.quit]
  mn.mButton [CLabel "Bell", Command tix.bell]
  mn1 <- mn.cascade [CLabel "Colors"]
  mn1.mButton [CLabel "Red",   Command (canv.set [Background red])]
  mn1.mButton [CLabel "Green", Command (canv.set [Background green])]
  mn1.mButton [CLabel "Blue",  Command (canv.set [Background blue])]
  pp.popupIn canv
  pack canv
