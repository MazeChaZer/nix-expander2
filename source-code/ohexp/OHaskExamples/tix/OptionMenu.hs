import Tix

main env = do
  win <- env.tixWindow []
  lab1 <- win.label [Text "OptionMenu Demo"]
  lab2 <-  win.label [] 
  let alts = ["Alt 0","Alt 1","Alt 2","Alt 3"]
      report n = lab2.set [Text ("Selected "++alts!!n)]
  om <- win.optionMenu alts [CmdInt report]
  om.setValue 3
  cb <- win.checkButton [Text "Menu enabled"]
  let enable = do
     b <- cb.checked
     om.set [Enabled b]
  cb.set [Command enable]
  cb.toggle
  pack (col [lab1,lab2,cb,rigid om])


