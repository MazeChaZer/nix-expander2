{- Simple demo of ScrolledTree and HList. -}

import Tix

labels = ["A new node","A Child", "A grandchild","A leaf"]

main env = do
  win  <- env.tixWindow [Title "ScrolledTree Demo"]
  t <- win.scrolledTree []
  let hl = t.subHList
  a  <- hl.addEntry [Text "Root of first tree"]
  b  <- a.addChild [Text "First child"]
  b1 <- b.addChild [Text "Grandchild"]
  c  <- a.addChild [Text "Second child"]
  d  <- a.addChild [Text "Third child"]
  e  <- hl.addEntry [Text "Root of second tree"]
  t.decorate

  combo <- win.comboBox True []
  combo.subLabel.set [Text "New leaf label:"]
  combo.subListBox.insertLines 0 labels
  combo.setValue 0
  let addChild path = do
    elem <- hl.childAt path
    str <- combo.subEntry.getValue
    elem.addChild [Text str]
    t.decorate
    done
  hl.set [SelectMode Multiple, Background  (rgb 230 255 230), 
          CmdString addChild]

  lab <- win.label [Text "Click here to show selection on stdout",
                    BorderWidth 2, Relief Raised]
  tt <- win.toolTip []
  tt.attachTo lab "For multiple selection, use Ctrl-Button 1"
  let showSelection = do
        ps <- hl.getValue
        forall p <- ps do 
          ent <- hl.childAt p
          s <- ent.getValue
          env.putStr(s++"\n")
  lab.bind [ButtonPress 1 (\_ -> showSelection)]

  lab2 <- win.label [Text "Double-click on tree element to add child\nwith entry text as label",
          BorderWidth 2, Relief Raised]

  win.bind [Destroy env.quit]
  pack (col [lab,lab2,combo,t])
