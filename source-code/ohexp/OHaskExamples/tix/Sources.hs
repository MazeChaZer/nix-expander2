
import Tix

mkeditor tix file = do
  w  <- tix.tixWindow [Title "Editor"]
  scred <- w.scrolledTextEditor [BorderWidth 3, Relief Raised]
  let ed= scred.subTextEditor
  ed.set [Width 60, Height 30, 
          Background (rgb 200 220 240), Wrap NoWrap]
  lab <- w.label []
  f <- tix.readFile file 
  let lns = lines f
  forall l <- lns do ed.insertLines maxBound [l]
  q  <- w.button [Text "Close", Command tix.quit]
  l  <- w.button [Text "First Line", 
                  Command (do s <- ed.getLine 1; lab.set [Text s])]
  i  <- w.button [Text "Delete First Line", Command (ed.deleteLine 1 )]
  pack (scred ^^^ fillX (rigid(row[q,l,i])<<<lab))
  


main tix = do
  fd <- tix.fileDialog [CmdString (mkeditor tix)]
  fd.popup


