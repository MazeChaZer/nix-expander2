module CHinOH where

type IO' a    = Ref () -> (a -> O () ()) -> O () ()

returnIO     :: a -> IO'  a
returnIO a    = \_ c -> c a

bindIO       :: IO' a -> (a -> IO' b) -> IO' b
e `bindIO` f  = \s c -> e s (\a -> f a s c)

runIO' :: IO' () -> Cmd ()
runIO' io = do
   o <- template in action io self return
   o
   done

lift         :: Cmd a -> IO' a
lift cmd      = \_ c -> cmd >>= c

forkIO       :: IO' () -> IO' ()
forkIO p      = \_ c -> do o <- (template in action p self return)
                           o
                           c ()

struct MVar a = 
   put       :: a -> Request ()
   take      :: (a -> Action) -> Action
    
newMVar      :: IO' (MVar a)
newMVar       = \_ c -> do v <- mvartempl
                           c v 

putMVar      :: MVar a -> a -> IO' ()
putMVar v a   = \_ c -> do v.put a
                           c ()

takeMVar     :: MVar a -> IO' a
takeMVar v    = \s c -> v.take (\a -> primAct s (c a))


mvartempl    :: Template (MVar a)
mvartempl     = 
   template
      val    := Nothing    -- :: Maybe a
      takers := []         -- :: [a -> Action]
   in struct
      put a = request
         case takers of
            []   -> case val of
                       Nothing -> do val := Just a
                       Just _  -> error "putMVar"
            t:ts -> do t a
                       takers := ts

      take t = action
          case val of
             Nothing -> do takers := takers ++ [t]
             Just a  -> t a

