module Sieve2 where

struct Cell = 
   feed :: (Int -> Action) -> Int -> Action

cell m n = 
  template
     next := Nothing
  in struct
     feed print k = action
        if k `mod` n /= 0 then
           case next of
              Nothing ->
                 if k*k <= m then
                    c <- cell m k
                    next := Just c
                 print k
              Just c ->
                 c.feed print k


root print n = do 
   w <- cell n 2
   forall i <- [3..n] do
      w.feed print i

main env = root (env.putStr . (++" ") . show) 1000
