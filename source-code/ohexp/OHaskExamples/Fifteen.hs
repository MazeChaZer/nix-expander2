import Random
import Array
import Tk

sq   = 60
half = sq `div` 2

fontSize = half
pieceFont =  "Helvetica "++show fontSize++" bold italic"
labelFont =  pieceFont

struct Fifteen =
  drawBoard :: Action
  movePiece :: Pos -> Action
  reset     :: Action
  shuffle   :: Action

fifteen bell lab canv = 
 let initArray = listArray (0,3) (map (listArray (0,3)) [[4*i-3..4*i] | i <- [1..4]])
 in template
     state := initArray
     (hr,hc) := (3,3)
     count := 0
   in let move loud (r,c) = do
            if (hr == r) == (hc == c) then
               if loud then bell else done
            else case (compare hc c,compare hr r) of
                   (LT,_) -> forall i <- [hc..c-1]      do swap (r,i) (r,i+1)
                   (GT,_) -> forall i <- [hc,hc-1..c+1] do swap (r,i) (r,i-1)
                   (_,LT) -> forall i <- [hr..r-1]      do swap (i,c) (i+1,c)
                   (_,GT) -> forall i <- [hr,hr-1..r+1] do swap (i,c) (i-1,c)
                 (hr,hc) := (r,c)
                 count := count+1

          swap (r,c) (r',c') = do let d = (state!r)!c
                                  (state!r)!c  := (state!r')!c'
                                  (state!r')!c' := d      

          draw = do
             canv.clear
             forall ln <- [0..4] do
               canv.line [(0,ln*sq),(4*sq,ln*sq)] []
               canv.line [(ln*sq,0),(ln*sq,4*sq)] []
             forall r <- [0..3] do
               forall c <- [0..3] do
                  canv.text (sq*c+half,sq*r+half) 
                      [Font pieceFont, Fill white, Text (show ((state!r)!c))]
             canv.rectangle (hc*sq,hr*sq) ((hc+1)*sq,(hr+1)*sq) [Fill black]
             lab.set [Text (show count++" moves")] 
          
          movePiece  pos = action
             move True pos
             draw

          drawBoard = action draw

          reset = action
                    state := initArray
                    count := 0
                    (hr,hc) := (3,3)
                    draw
 

          shuffle = action
                     rand <- natGenerator (count+2)
                     forall _ <- [1..200] do
                       r <- rand.next
                       c <- rand.next
                       move False (r `mod` 4,c `mod` 4)
                     count := 0
                     draw
      in struct ..Fifteen


main tk = do win   <- tk.window  [Title "Fifteen puzzle"]
             canv  <- win.canvas [Width(4*sq), Height(4*sq), Background blue]
             lab   <- win.label  [Font labelFont, Anchor C]
             board <- fifteen tk.bell lab canv
             quit  <- win.button [Text "Quit", Command tk.quit]
             shuf  <- win.button [Text "Shuffle", Command board.shuffle]
             res   <- win.button [Text "Reset", Command board.reset]
             pack (col[row[quit,shuf,res],canv,lab])
             board.drawBoard
             canv.bind [ButtonPress 1 (\(x,y) -> board.movePiece(y `div` sq,x `div` sq))]


