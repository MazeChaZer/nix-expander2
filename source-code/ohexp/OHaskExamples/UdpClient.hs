import Tk

port :: Port
port = Port 1984

client :: StdEnv -> (String -> Cmd ()) -> Template UDPClient
client env receiver = 
   template 
   in struct
          deliverUDP pkt = action
			    env.putStr "got packet"
			    let (host, Port port) = pkt.peer
			    env.udp.getNameOfHost host $ \hn -> action
			      env.putStr (" from " ++ hn ++ "(" ++ show port ++ ")" ++ ":" ++ pkt.contents ++ "\n")
                            receiver pkt.contents

main env = (>>= id) $ template in do
   env.setReader (\c -> env.putStr ("got "++[c]++"\n"))
   win <- env.window [Title "Udp Client"]
   lab  <- win.label  [Text "Nothing received"]
   c <- client env (\s -> lab.set [Text ("Got "++s)])
   env.udp.getHostByName "localhost" $ maybe undefined $ \h -> action
     con <- env.udp.openUDP (h, port) c
     let butt i = win.button [Text i, Command (con.send (struct contents=i))]
     b1 <- butt "Yes"
     b2 <- butt "No"
     quit <- win.button [Text "Quit", Foreground red, Command env.quit]
     pack (row [lab,b1,b2,quit])

