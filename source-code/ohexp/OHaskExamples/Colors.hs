import Tk

colorChooser tk cmd text = do 
    win  <- tk.window [Title "Color chooser"]
    let colslider col = win.slider [To 255, Background col, 
                                    Orientation Hor, Relief Ridge] 
    sr   <- colslider red
    sg   <- colslider green
    sb   <- colslider blue
    sb.set [Foreground white]
    let doCmd = do 
        r <- sr.getValue
        g <- sg.getValue
        b <- sb.getValue
        cmd (rgb r g b)
        win.destroy
    q    <- win.button [Text text, Command doCmd]
    canv <- win.canvas []
    let setCol _ = do r <- sr.getValue
                      g <- sg.getValue
                      b <- sb.getValue 
                      canv.set [Background (rgb r g b)]
    sr.set [CmdInt setCol]
    sg.set [CmdInt setCol]
    sb.set [CmdInt setCol]
    pack (rigid(col [sr,sg,sb,q]) <<< canv)
         

main tk = do
  win  <- tk.window  [Title "Colors"]
  canv <- win.canvas [BorderWidth 3, Relief Ridge]
  quit <- win.button [Text "Quit", Command tk.quit]
  txt  <- canv.text (50,50) [Anchor W, Font "Helvetica 20",
          Text "Use button to select\n background color"]
  chbut <- win.button [Text "Choose color", 
          Command (colorChooser tk (\c -> canv.set [Background c]) "Select")]
  pack (canv <<< rigid(chbut ^^^ quit))
