module Reflex where

import Tk
import Random
import SingleTimeout

data State = Idle | Holding | Counting deriving Eq

tester env seed lamp display  =
   template
      state     := Idle
      startTime := undefined
      random    <- natGenerator seed
      to        <- singletimeout

   in let

      color Idle     = black
      color Holding  = red
      color Counting = green

      timeout = action
            case state of
               Holding -> 
                  st <- currentBaseline
                  startTime := st
                  to.after (10*seconds) timeout
                  setGameState Counting "Go!"
               Counting ->
                  setGameState Idle "Timeout."
               Idle ->
                  done

      click = action
         case state of
            Idle -> 
               r <- random.next
               to.after (3*seconds + r `mod` (4*seconds)) timeout
               setGameState Holding "Ready..."

            Holding -> 
               setGameState Idle "Cheating!"
               
            Counting -> 
               stopTime <- currentBaseline
               let delay = fromTime (stopTime - startTime) / seconds
               setGameState Idle
                   ("Time: " ++ show (decimals 3 delay) ++ " s")

      setGameState s msg = do
          if s == Idle then
               to.cancel >> done
          state := s
          lamp.set [Fill (color s)]
          display.set [Text msg]

   in click

main env = do
   win  <- env.window [Title "The Reflex Game"]
   canv <- win.canvas [Width 100,Height 100, Relief Groove, BorderWidth 6]
   ov   <- canv.oval (16,16) (96,96) [Fill black]
   lab  <- win.label [Font "helvetica 20", Background white,
                      Relief Ridge, BorderWidth 4, Width 16]
   quit <- win.button [Text "Quit", Command env.quit]
   pack (col [lab, rigid canv, quit])

   tod <-env.timeOfDay
   let seed = fromInteger (tod `mod` 2^31)
   click <- tester env seed ov lab
   canv.bind [ButtonPress 1 (\_ -> click)]

decimals d t = fromInt (round (t * e)) / e
  where e = 10**d
