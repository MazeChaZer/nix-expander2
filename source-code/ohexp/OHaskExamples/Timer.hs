module Timer where

struct Timer =
   start :: Int -> Action -> Action
   tick  :: Action

timer = 
   template
      time := 0
      pend := []
   in let
      start t sig = action
         pend := insert (time+t,sig) pend
         
      insert x []         = [x]
      insert x (y:ys) 
         | fst x <= fst y = x:y:ys
         | otherwise      = y : insert x ys
      
      check = do
         case pend of
            (t,sig):p | time >= t ->
               pend := p
               sig
               check
            _ -> 
               done
            
      tick = action
         time := time + 1
         check
   
   in struct ..Timer
