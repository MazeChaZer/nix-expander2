port = Port 12345
host = HostName "localhost"

main env = do
  fix ls <- env.inet.tcp.listen port (server env ls)
  env.inet.tcp.open host port (client env)

client env peer = 
  template
  in let
    put s = env.putStr ("Client: " ++ s ++ "\n")
  in struct 
     neterror (NetError e) = action 
        put ("NetError: "++e)
     connect = action
        put "Connected"
        peer.deliver "Hello, Server"
     close = action
        put "Closed"
     deliver s = action 
        put ("Got data \""++s++"\"")
        peer.close

server env me peer = 
  template
  in let
    put s = env.putStr ("Server: " ++ s ++ "\n")
  in struct 
     neterror (NetError e) = action 
        put ("NetError: "++e)
     connect = action
        put "Connected"
     close = action
        put "Closed"
        me.close
     deliver s = action 
        put ("Got data \""++s++"\"")
        peer.deliver "Hello, Client"
