/* --------------------------------------------------------------------------
* subst.c:     Copyright (c) Mark P Jones 1991-1998.   All rights reserved.
*              See NOTICE for details and conditions of use etc...
*              Hugs version 1.3b, January 1998
*
* Part of type checker dealing with operations on current substitution.
* ------------------------------------------------------------------------*/

static  List	   accumVars;		/* holds constrained vars between  */
/* unification passes		   */
static  Int        lastTyvar;		/* last tyvar visited by postUnify()*/
static  List       lastBounds;		/* bounds of lastType              */

static  List	   skolVars;		/* Current list of skolemized vars */
static  List	   skolVars2;		/* Current list of local constr/sel vars */


static Void local checkVars();

static Void local emptySubstitution() {	/* clear current substitution	   */
	numTyvars   = 0;
#if !FIXED_SUBST
	if (maxTyvars!=NUM_TYVARS) {
		maxTyvars = 0;
		if (tyvars) {
			free(tyvars);
			tyvars = 0;
		}
	}
#endif
	nextGeneric = 0;
	genericVars = NIL;
	typeIs      = NIL;
	accumVars   = NIL;
	lastBounds  = NIL;
	lastTyvar   = 0;
}

/* add further n type variables to */
/* current substituion		   */
static Void local expandSubst(Int n)
{
#if FIXED_SUBST
	if (numTyvars+n>NUM_TYVARS) {
		ERRMSG(0) "Too many type variables in type checker"
			EEND;
	}
#else
	if (numTyvars+n>maxTyvars) {	/* need to expand substitution	   */
		Int   newMax = maxTyvars+NUM_TYVARS;
		Tyvar *newTvs;
		Int   i;

		if (numTyvars+n>newMax) {	/* safety precaution		   */
			ERRMSG(0) "Substitution expanding too quickly"
				EEND;
		}

		/* It would be better to realloc() here, but that isn't portable
		* enough for calloc()ed arrays.  The following code could cause
		* a space leak if an interrupt occurs while we're copying the
		* array ... we won't worry about this for the time being because
		* we don't expect to have to go through this process much (if at
		* all) in normal use of the type checker.
		*/

		newTvs = (Tyvar *)calloc(newMax,sizeof(Tyvar));
		if (!newTvs) {
			ERRMSG(0) "Too many variables (%d) in type checker", newMax
				EEND;
		}
		for (i=0; i<numTyvars;++i) {		/* copy substitution	   */
			newTvs[i].state = tyvars[i].state;
			newTvs[i].offs  = tyvars[i].offs;
			newTvs[i].bound = tyvars[i].bound;
			newTvs[i].kind  = tyvars[i].kind;
		}
		maxTyvars = 0;				/* protection from SIGINT? */
		if (tyvars) free(tyvars);
		tyvars    = newTvs;
		maxTyvars = newMax;
	}
#endif
}

static Void local resetState(Tyvar *tyv)
{
	tyv->state = NORMAL;
	tyv->bound = NIL;
	tyv->offs  = UNUSED_GENERIC;
}

/* allocate new type variables	   */
/* all of kind STAR		   */
static Int local newTyvars(Int n)
{
	Int beta = numTyvars;

	expandSubst(n);
	for (numTyvars+=n; n>0; n--) {
		resetState(tyvar(numTyvars-n));
		tyvars[numTyvars-n].kind  = STAR;
#ifdef DEBUG_TYPES
		printf("new type variable: _%d ::: ",numTyvars-n);
		printKind(stdout,tyvars[numTyvars-n].kind);
		putchar('\n');
#endif
	}
	return beta;
}

/* allocate new variables with	   */
/* specified kinds		   */
/* if k = k0 -> k1 -> ... -> kn	   */
/* then allocate n vars with kinds */
/* k0, k1, ..., k(n-1)		   */
/* Dealing with quantifiers, if nec*/
static Int local newKindedVars(Kind k, Bool inPat)
{
	Int beta = numTyvars;
	for (; isPair(k); k=snd(k)) {
		expandSubst(1);
		resetState(tyvar(numTyvars));
		tyvars[numTyvars].kind  = fst(k);
#ifdef DEBUG_TYPES
		printf("new type variable: _%d ::: ",numTyvars);
		printKind(stdout,tyvars[numTyvars].kind);
		putchar('\n');
#endif
		numTyvars++;
	}
	return beta;
}

#define deRef(tyv,t,o)  while ((tyv=getTypeVar(t,o)) && isBound(tyv)) { \
	t = tyv->bound;                             \
	o = tyv->offs;                              \
}

/* get number of type variable	   */
/* represented by (t,o) [if any].  */
static Tyvar *local getTypeVar(Type t, Int  o)
{
	switch (whatIs(t)) {
	case INTCELL : return tyvar(intOf(t));
	case OFFSET  : return tyvar(o+offsetOf(t));
	}
	return ((Tyvar *)0);
}

/* load type held in type variable */
/* vn into (typeIs,typeOff)	   */
static Void local tyvarType(Int vn)
{
	Tyvar *tyv;

	while ((tyv=tyvar(vn)), isBound(tyv))
		switch(whatIs(tyv->bound)) {
		case INTCELL : vn = intOf(tyv->bound);
			break;

		case OFFSET  : vn = offsetOf(tyv->bound)+(tyv->offs);
			break;

		default	 : typeIs  = tyv->bound;
			typeOff = tyv->offs;
			return;
	}
	typeIs  = var;
	typeOff = vn;
}

#define bindTyv(tyv,t,o)     tyv->bound = t; tyv->offs  = o

/* set type variable vn to (t,o)   */
static Void local bindTv(Int  vn, Type t, Int  o)
{
	Tyvar *tyv = tyvar(vn);
	if (!isNormal(tyv))
		internal("bindTv");
	bindTyv(tyv,t,o);
#ifdef DEBUG_TYPES
	printf("binding type variable: _%d to ",vn);
	printType(stdout,debugType(t,o));
	putchar('\n');
#endif
}

/* Expand type synonym with:	   */
/* head h			   */
/* ar args (NB. ar>=tycon(h).arity)*/
/* original expression (*at,*ao)   */
/* expansion returned in (*at,*ao) */
static Void local expandSyn(Tycon h, Int   ar, Type  *at, Int   *ao)
{
	ar -= tycon(h).arity;		/* calculate surplus arguments	   */
	if (ar==0)
		expandSyn1(h,at,ao);
	else {
		/* if there are more args than the */
		/* arity, we have to do a little   */
		/* bit of work to isolate args that*/
		/* will not be changed by expansion*/
		Type t    = *at;
		Int  o    = *ao;
		Type args = NIL;
		Int  i;
		/* find part to expand, and the	   */
		/* unused arguments		   */
		while (ar-- > 0) {
			Tyvar *tyv;
			args = cons(arg(t),args);
			t    = fun(t);
			deRef(tyv,t,o);
		}
		/* do the expansion		   */
		/* and embed the results back in   */
		/* (*at, *ao) as required	   */
		expandSyn1(h,&t,&o);
		bindTv((i=newTyvars(1)),t,o);
		tyvar(i)->kind = getKind(t,o);
		*at = applyToArgs(mkInt(i),args);
	}
}

/* Expand type synonym with:	   */
/* head h, tycon(h).arity args,	   */
/* original expression (*at,*ao)   */
/* expansion returned in (*at,*ao) */
static Void local expandSyn1(Tycon h, Type  *at, Int   *ao)
{
	Int   n = tycon(h).arity;
	Type  t = *at;
	Int   o = *ao;
	Tyvar *tyv;

	*at = tycon(h).defn;
	*ao = newKindedVars(tycon(h).kind,FALSE);
	for (; 0<n--; t=fun(t)) {
		deRef(tyv,t,o);
		if (tyv || !isAp(t))
			internal("expandSyn1");
		bindTv(*ao+n,arg(t),o);
	}
}

/* get value at head of type exp.  */
static Cell local getDerefHead(Type t, Int  o)
{
	Tyvar *tyv;
	argCount = 0;
	for (;;) {
		while (isAp(t)) {
			argCount++;
			t = fun(t);
		}
		if ((tyv=getTypeVar(t,o)) && isBound(tyv)) {
			t = tyv->bound;
			o = tyv->offs;
		}
		else
			break;
	}
	if (isOffset(t))
		return mkInt(offsetOf(t)+o);
	else
		return t;
}

/* --------------------------------------------------------------------------
* Skolemization:
* ------------------------------------------------------------------------*/

static Void local skolemOffsetsAbove(Int  n, Type t, Int  o)
{
	switch(whatIs(t)) {
	case AP      : skolemOffsetsAbove(n,fst(t),o);
		skolemOffsetsAbove(n,snd(t),o);
		break;
	case OFFSET  : if (offsetOf(t) >= n) {
		Tyvar *tyv = tyvar(o+offsetOf(t));
		if (isBound(tyv))
			internal("skolemOffsetsAbove");
		if (!isSkolem(tyv)) {
			tyv->state = SKOLEM;
			hd(skolVars2) = cons(mkInt(o+offsetOf(t)),hd(skolVars2));
		}
				   }
				   break;
	}
}

static Void saveSkolem2() {
	skolVars2 = cons(NIL,skolVars2);
}

static Void local restoreSkolem2() {
	List vs = hd(skolVars2);
	for (; nonNull(vs); vs=tl(vs))
		deskolemTyvar(intOf(hd(vs)));
	skolVars2 = tl(skolVars2);
}

static Void local skolemTyvar(Int vn)	/* Skolemize type variable         */
{
	Tyvar *tyv = tyvar(vn);
	if (isBound(tyv))
		skolemType(tyv->bound,tyv->offs);
	else if (isUnbound(tyv)) {
		tyv->state = SKOLEM;
		hd(skolVars) = cons(mkInt(vn),hd(skolVars));
	}
}

/* Skolemize type	           */
static Void local skolemType(Type t, Int  o)
{
	switch(whatIs(t)) {
	case AP      : skolemType(fst(t),o);
		skolemType(snd(t),o);
		break;
	case OFFSET  : skolemTyvar(o+offsetOf(t));
		break;
	case INTCELL : break;           /* Must be fresh, don't skolemize  */
	}
}

static Void local deskolemTyvar(Int vn)	/* Remove skolemization from tyvar */
{
	Tyvar *tyv = tyvar(vn);
	if (!isSkolem(tyv))
		internal("deskolemTyvar");
	resetState(tyv);
}

static Void saveSkolem() {
	skolVars = cons(NIL,skolVars);
}

static Void local restoreSkolem() {
	List vs = hd(skolVars);
	for (; nonNull(vs); vs=tl(vs))
		deskolemTyvar(intOf(hd(vs)));
	skolVars = tl(skolVars);
}

/* --------------------------------------------------------------------------
* Count tyvar references in accumulated bounds (used during unification).
* ------------------------------------------------------------------------*/

static Int delta;

#define countWith(n,vn)	{delta = n; countTyvar(vn);}
#define countUp(vn)	countWith(1,vn)
#define countDown(vn)	countWith(-1,vn)

static Void local countTyvar(Int vn)
{
	Tyvar *tyv = tyvar(vn);

	if (isBound(tyv))
		countType(tyv->bound,tyv->offs);
	else if (isCloned(tyv))
		internal("countTyvar");
	else if (!isSkolem(tyv)) {
		mkAccum(tyv);
		tyv->offs += delta;
		if (tyv->offs < 0)
			internal("refCount < 0");
	}
}

static Void local countType(Type t, Int  o)
{
	switch(whatIs(t)) {
	case AP      : countType(fst(t),o);
		countType(snd(t),o);
		break;
	case OFFSET  : countTyvar(o+offsetOf(t));
		break;
	case INTCELL : countTyvar(intOf(t));
		break;
	}
}

/* ----------------------------------------------------------------------
* Accumulating bounds
* ---------------------------------------------------------------------*/

static Void local mkAccum(Tyvar *tyv)
{
	if (!isAccum(tyv)) {
		if (!isUnbound(tyv))
			internal("mkAccum");
		tyv->bound = NIL;
		tyv->state = ACCUM;
		tyv->offs  = 0;
		accumVars  = cons(mkInt(tyvNum(tyv)),accumVars);
	}
}

static Void accumBound(Tyvar *tyv, Int   beta, Int   z)
{
	if (z != NONV) {
		mkAccum(tyv);
		countUp(beta);
		tyv->bound = cons(pair(mkInt(beta),mkInt(z)),tyv->bound);
		checkVars();
	}
}

static Void local deAccum(Tyvar *tyv, Bool decr)
{
	if (isAccum(tyv)) {
		if (decr) {
			List bs = tyv->bound;
			for (; nonNull(bs); bs=tl(bs))
				countDown(intOf(fst(hd(bs))));
		}
		resetState(tyv);
		accumVars = removeCell(mkInt(tyvNum(tyv)),accumVars);
	}
}

/* --------------------------------------------------------------------------
* Mark type expression, so that all variables are marked as unused generics
* ------------------------------------------------------------------------*/

static Void local normState() {      	/* reset state for all unbound vars*/
	Int i;			
	for (i=0; i<numTyvars; ++i) {
		Tyvar *tyv = tyvar(i);
		if (!isNormal(tyv))
			resetState(tyv);
	}
}

static Void local clearOffsTyvar(Int vn)
{
	Tyvar *tyv = tyvar(vn);

	if (isBound(tyv))
		clearOffsType(tyv->bound,tyv->offs);
	else if (!isAccum(tyv))
		tyv->offs = NONV;		/* == UNUSED_GENERIC		   */
}

static Void local clearOffsType(Type t, Int  o)
{
	switch (whatIs(t)) {
	case AP      : clearOffsType(fst(t),o);
		clearOffsType(snd(t),o);
		return;
	case OFFSET  : if (o >= 0) clearOffsTyvar(o+offsetOf(t));
		return;
	case INTCELL : clearOffsTyvar(intOf(t));
		return;
	}
}

/* set all unbound type vars to	   */
/* unused generic variables	   */
static Void local clearMarks() {
	Int i;
	for (i=0; i<numTyvars; ++i) {
		Tyvar *tyv = tyvar(i);
		if (shouldMark(tyv))
			tyv->offs = UNUSED_GENERIC;
	}
	nextGeneric = 0;
	genericVars = NIL;
}

/* reset all generic vars to unused*/
/* for generics >= n		   */
static Void local resetGenericsFrom(Int n)
{
	Int i;

	if (n==0)				/* reset generic variables list	   */
		genericVars = NIL;		/* most common case: reset to zero */
	else
		for (i=length(genericVars); i>n; i--)
			genericVars = tl(genericVars);

	for (i=0; i<numTyvars; ++i)
		if (shouldMark(tyvar(i)) && tyvar(i)->offs>=GENERIC+n)
			tyvar(i)->offs = UNUSED_GENERIC;
	nextGeneric = n;
}

/* mark fixed vars in type bound to*/
/* given type variable		   */
static Void local markTyvar(Int vn)
{
	Tyvar *tyv = tyvar(vn);

	if (isBound(tyv))
		markType(tyv->bound, tyv->offs);
	else if (isAccum(tyv))
		internal("markTyvar");
	else if (isCloned(tyv)) {
		List bs = tyv->bound;
		for (; nonNull(bs); bs=tl(bs))
			markTyvar(intOf(fst(hd(bs))));
		tyv->offs = FIXED_TYVAR;
	}
	else if (!isSkolem(tyv))
		tyv->offs = FIXED_TYVAR;
}

/* mark fixed vars in type (t,o)   */
static Void local markType(Type t, Int  o)
{
	Int w = whatIs(t);
	switch (w) {
	case TYCON   :
	case TUPLE   : return;

	case AP      : markType(fst(t),o);
		markType(snd(t),o);
		return;

	case OFFSET  : markTyvar(o+offsetOf(t));
		return;

	case INTCELL : markTyvar(intOf(t));
		return;

	default      : internal("markType");
	}
}

/* --------------------------------------------------------------------------
* Copy type expression from substitution to make a single type expression:
* ------------------------------------------------------------------------*/

/* calculate most general form of  */
/* type bound to given type var	   */
static Type local copyTyvar(Int vn)
{
	Tyvar *tyv = tyvar(vn);

	if (isBound(tyv))
		return copyType(tyv->bound,tyv->offs);

	if (!isNormal(tyv))
		return (mkInt(vn));

	switch (tyv->offs) {
	case FIXED_TYVAR    : return mkInt(vn);

	case UNUSED_GENERIC : (tyv->offs) = GENERIC + nextGeneric++;
		if (nextGeneric>=NUM_OFFSETS) {
			ERRMSG(0)
				"Too many quantified type variables"
				EEND;
		}
		genericVars = cons(mkInt(vn),genericVars);

	default 	    : return mkOffset(tyv->offs - GENERIC);
	}
}

/* calculate most general form of  */
/* type expression (t,o) 	   */
static Type local copyType(Type t, Int  o)
{
	switch (whatIs(t)) {
	case AP      : {   
		/* ensure correct */
		/* eval. order    */
		Type l = copyType(fst(t),o);
		Type r = copyType(snd(t),o);
		return ap(l,r);
				   }
	case OFFSET  : return copyTyvar(o+offsetOf(t));
	case INTCELL : return copyTyvar(intOf(t));
	}

	return t;
}

/* calculate list of generic vars  */
/* thru variable vn, prepended to  */
/* list vs			   */
static Type local genvarTyvar(Int  vn, List vs)
{
	Tyvar *tyv = tyvar(vn);

	if (isBound(tyv))
		return genvarType(tyv->bound,tyv->offs,vs);
	else if (!isNormal(tyv))
		return vs;
	else if (tyv->offs == UNUSED_GENERIC) {
		tyv->offs = GENERIC + nextGeneric++;
		return cons(mkInt(vn),vs);
	}
	else if (tyv->offs>=GENERIC && !intIsMember(vn,vs))
		return cons(mkInt(vn),vs);
	else
		return vs;
}

/* calculate list of generic vars  */
/* in type expression (t,o) 	   */
/* results are prepended to vs	   */
static List local genvarType(Type t, Int  o, List vs)
{
	switch (whatIs(t)) {
	case AP      : return genvarType(snd(t),o,genvarType(fst(t),o,vs));
	case OFFSET  : return genvarTyvar(o+offsetOf(t),vs);
	case INTCELL : return genvarTyvar(intOf(t),vs);
	}
	return vs;
}

#ifdef DEBUG_TYPES
/* expand type structure in full   */
/* detail			   */
static Type local debugTyvar(Int vn)
{
	Tyvar *tyv = tyvar(vn);

	if (isBound(tyv))
		return debugType(tyv->bound,tyv->offs);
	return mkInt(vn);
}

static Type local debugType(Type t, Int  o)
{
	switch (whatIs(t)) {
	case AP      : {   Type l = debugType(fst(t),o);
		Type r = debugType(snd(t),o);
		return ap(l,r);
				   }
	case OFFSET  : return debugTyvar(o+offsetOf(t));
	case INTCELL : return debugTyvar(intOf(t));
	}

	return t;
}
#endif /*DEBUG_TYPES*/

/* --------------------------------------------------------------------------
* Compute variance information
* -------------------------------------------------------------------------*/

static Cell local getVariance(Type h, Int  n)
{
	if (isTycon(h))
		return skipOver(length(tycon(h).arity)-n,tycon(h).variance);
	else if (isTuple(h))
		return mkInt(POSV);
	else if (isInt(h) || isOffset(h))
		return mkInt(INV);		/* higer order variance assumption */
	else {
		internal("getVariance");
		return 0;
	}
}

static Int local thisVariance(Cell z)
{
	if (isPair(z))
		return intOf(hd(z));
	else if (isInt(z))
		return intOf(z);
	else {
		internal("thisVariance");
		return 0;
	}
}

static Cell local nextVariance(Cell z)
{
	if (isPair(z))
		return tl(z);
	else if (isInt(z))
		return z;
	else {
		internal("nextVariance");
		return 0;
	}
}

/* mark variance in tyvar */
static Void local markVarianceTyvar(Int vn, Int z)
{
	Tyvar *tyv = tyvar(vn);

	if (isBound(tyv))
		markVarianceType(tyv->bound, tyv->offs, z);
	else if (!isAccum(tyv))
		tyv->offs |= z;
}

/* mark variance in type  */
static Void local markVarianceType(Type t, Int  o, Int  z)
{
	switch (whatIs(t)) {
	case AP      : {    Cell z1;
		Type h = getHead(t);

		if (isInt(h) || (isOffset(h) && o >= 0))
			h = getDerefHead(t,o);
		z1 = getVariance(h,argCount);

		while (isAp(t)) {
			Tyvar *tyv;
			markVarianceType(arg(t),o,multv(z,thisVariance(z1)));
			t = fun(t);
			if (isInt(t) || (isOffset(t) && o >= 0))
				deRef(tyv,t,o);
			z1 = nextVariance(z1);
		}
		markVarianceType(t,o,z);
				   }
				   break;

	case OFFSET  : if (o >= 0)
					   markVarianceTyvar(o+offsetOf(t),z);
		break;

	case INTCELL : markVarianceTyvar(intOf(t),z);
		break;
	}
}

/* --------------------------------------------------------------------------
* Clone type expression by replacing each tyvar occurrence with a fresh 
* copy, and append each new tyvar to the bounds of its origin.
* ------------------------------------------------------------------------*/

static Type local cloneTyvar(Int vn, Int z)
{
	Tyvar *tyv = tyvar(vn);

	if (isBound(tyv))
		return cloneType(tyv->bound,tyv->offs,z);
	else {
		Int beta;
		if (isNormal(tyv)) {
			tyv->state = CLONED;
		}
		if (!isCloned(tyv))		/* Never clone skolemized tyvars */
			return mkInt(vn);
		else {
			beta = newTyvars(1);	/* invalidates tyv */
			tyv = tyvar(vn);	
			tyvar(beta)->kind = tyv->kind;
			tyv->bound = cons(pair(mkInt(beta),mkInt(z)),tyv->bound);
			return fst(hd(tyv->bound));
		}
	}
}

static Type local cloneType(Type t, Int  o, Int  z)
{
	switch (whatIs(t)) {
	case TYCON   :
	case TUPLE   : return t;
	case AP      : {    Cell z1;
		List args = NIL;
		Type h = getHead(t);

		if (isInt(h) || (isOffset(h) && o >= 0))
			h = getDerefHead(t,o);
		z1 = getVariance(h,argCount);

		while (isAp(t)) {
			Tyvar *tyv;
			args = cons(cloneType(arg(t),o,multv(z,thisVariance(z1))),args);
			t = fun(t);
			if (isInt(t) || (isOffset(t) && o >= 0))
				deRef(tyv,t,o);
			z1 = nextVariance(z1);
		}
		t = cloneType(t,o,z);
		while (nonNull(args)) {
			t = ap(t,hd(args));
			args = tl(args);
		}
		return t;
				   }
	case OFFSET  : return (o<0) ? t : cloneTyvar(o+offsetOf(t),z);
	case INTCELL : return cloneTyvar(intOf(t),z);
	default      : internal("cloneType"); return 0;
	}
}

static Type local clone(Type t, Int o,Int z)
{
	return cloneType(t,o,z);
}

static Int cloneTv(Int vn, Int z)
{
	Type t = clone(var,vn,z);
	if (isInt(t))
		return intOf(t);
	else {
		Int beta = newTyvars(1);
		bindTv(beta,t,0);
		return beta;
	}
}

/* --------------------------------------------------------------------------
* Convert a CLONED tyvar to an ACCUM one, i.e. essentially remove the 
* tyvars from its bounds and prepare for postunify.
* ------------------------------------------------------------------------*/

static Bool local doAccum(Int vn)
{
	Tyvar *tyv = tyvar(vn);
	List bs = tyv->bound;
	Int saveLastTyvar = lastTyvar;
	List saveLastBounds = lastBounds;

	resetState(tyv);
	lastTyvar  = vn;
	lastBounds = appendOnto(bs,lastBounds);
	for (; nonNull(bs) && bs!=saveLastBounds; bs=tl(bs)) {
		Int beta = intOf(fst(hd(bs)));
		Int z    = intOf(snd(hd(bs)));
		if (!accumClonedTyvar(beta,FALSE))
			return FALSE;
		if (!preUnify(var,beta,var,vn,z))
			return FALSE;
	}
	lastTyvar  = saveLastTyvar;
	lastBounds = saveLastBounds;
	return TRUE;
}

static Bool local accumClonedTyvar(Int vn, Bool skip)
{
	Tyvar *tyv = tyvar(vn);

	if (isBound(tyv))
		return accumClonedType(tyv->bound,tyv->offs,skip);
	else if (isCloned(tyv)) {
		/* Skip these, since they are reachable   */
		/* from some other cloned tyvar that will */
		/* be resolved later.                     */
		if (skip && tyv->offs == FIXED_TYVAR)
			return TRUE;
		else
			return doAccum(vn);
	}
	return TRUE;
}

static Bool local accumClonedType(Type t, Int  o, Bool skip)
{
	switch (whatIs(t)) {
	case AP      : if (!accumClonedType(fst(t),o,skip))
					   return FALSE;
		return accumClonedType(snd(t),o,skip);
	case INTCELL : return accumClonedTyvar(intOf(t),skip);
	case OFFSET  : if (o>=0) 
					   return accumClonedTyvar(o+offsetOf(t),skip);
	}
	return TRUE;
}

static Bool local accumClonedPolyType(Type t, Bool skip)
{
	if (isPolyType(t))
		t = monoTypeOf(t);
	if (whatIs(t) == QUAL)
		t = snd(snd(t));
	return accumClonedType(t,-1,skip);
}

/* --------------------------------------------------------------------------
* Occurs check:
* ------------------------------------------------------------------------*/

static Tyvar *lookingFor;	       	/* var to look for in occurs check */

/* Return TRUE if var lookingFor   */
/* is referenced in (t,o)	   */
static Bool local occursIn(Type t, Int o)
{
	Tyvar *tyv;

	for (;;) {
		deRef(tyv,t,o);
		if (tyv)			/* type variable		   */
			return tyv==lookingFor;
		else if (isAp(t)) {		/* application			   */
			if (occursIn(snd(t),o))
				return TRUE;
			else
				t = fst(t);
		}
		else                            /* no variable found		   */
			break;
	}
	return FALSE;
}

static Bool local occurs(Tyvar *tyv, Type  t, Int  o)
{
	lookingFor = tyv;
	return occursIn(t,o);
}

static Bool local offsetsBetween(Int lo, Int hi, Type t)
{
	switch (whatIs(t)) {
	case TYCON   :
	case TUPLE   : return FALSE;
	case AP      : return offsetsBetween(lo,hi,fun(t)) || offsetsBetween(lo,hi,arg(t));
	case OFFSET  : return lo < offsetOf(t) && offsetOf(t) <= hi;
	default      : internal("offsetsBetween"); return 0;
	}
}

/* --------------------------------------------------------------------------
* Unification algorithm:
* ------------------------------------------------------------------------*/

static char *unifyFails = 0;		/* unification error message	   */
/* set to TRUE to prevent binding  */
/* during matching process	   */
static Bool matchMode	= FALSE;

static Bool local failure(String str)
{
	unifyFails = str;
	checkVars();
	return FALSE;
}

/* bind tyv to (t,o) and preUnify  */
/* with accumulated bounds         */
static Bool solveAccum(Tyvar *tyv, Type  t, Int   o)
{
	List bs = NIL;
	Int  n  = 0;
//	Type h;

	if (occurs(tyv,t,o))
		return failure("unification would give infinite type");
	if (isAccum(tyv)) {
		bs = tyv->bound;
		n = tyv->offs;
		deAccum(tyv,TRUE);
	}
	bindTyv(tyv,t,o);
	if (n > 0)
		countWith(n,tyvNum(tyv));
	for (; nonNull(bs); bs=tl(bs))
		if (!preUnify(var,intOf(fst(hd(bs))),t,o,intOf(snd(hd(bs)))))
			return FALSE;
	return TRUE;
}

/* Make binding tyv1 := tyv2 */
static Bool local varToVarBind(Tyvar *tyv1, Tyvar *tyv2, Int z)
{
	checkVars();
	if (tyv1!=tyv2) {
		if (matchMode)
			return failure("types do not match");
		if (isCloned(tyv1) || isCloned(tyv2))
			internal("clones in varToVarBind");
		if (!eqKind(tyv1->kind,tyv2->kind))
			return failure("constructor variable kinds do not match");
		if (!isSkolem(tyv1)) {
			if (!isSkolem(tyv2)) {
				/* equivalent:  return solveAccum(tyv1,var,tyvNum(tyv2));  */
				if (isAccum(tyv1)) {
					mkAccum(tyv2);
					tyv2->bound = appendOnto(tyv1->bound,tyv2->bound);
					tyv2->offs  = tyv1->offs + tyv2->offs;   /* ref-counts */
					deAccum(tyv1,FALSE);
				}
				bindTyv(tyv1,var,tyvNum(tyv2));
				checkVars();
			}
			else
				return varToTypeAccum(tyv1,var,tyvNum(tyv2),negv(z));
		}
		else {
			if (!isSkolem(tyv2))
				return varToTypeAccum(tyv2,var,tyvNum(tyv1),z);
			else {
				// Insert subtyping extension here...
				return failure("quantified variable prevents unification");
			}
		}
	}
	checkVars();
	return TRUE;
}

/* Accumulate tyv < (t,o)       */
static Bool local varToTypeAccum(Tyvar *tyv,
								 /* guaranteed not bo be a variable, or to  */ 
								 /* have synonym as outermost constr        */
								 /* Can assume t==mkInt() => t skolemized   */
								 Type t,
								 Int   o, Int   z)
{
	Type h;
	checkVars();
	if (matchMode)
		return failure("types do not match");
	if (isCloned(tyv))
		internal("clone in varToTypeAccum");
	if (!eqKind(tyv->kind,getKind(t,o)))
		return failure("kinds do not match");
	if (isSkolem(tyv)) {
		h = getDerefHead(t,o);
		// Insert subtyping extension here...
		return failure("quantified variable prevents unification");
	}
	else {					  /* var to type bind      */
		Int alpha = tyvNum(tyv);
		Int beta  = newTyvars(1);		  /* invalidates tyv       */
		tyv = tyvar(alpha);
		bindTv(beta,t,o);
		accumBound(tyv,beta,z);
		return TRUE;
	}
	return FALSE; /* not reached */
}

#define lub(ts) bestBounds(ts,0,POSV)
#define glb(ts) bestBounds(ts,0,NEGV)
#define raiseArity(tc,a,z) bestBounds(singleton(tc),a,z)

/* break down constraint into a  */
/* set of simple constraints on  */
/* variables (result is stored   */
/* in 'accumVars') 	           */
static Bool local preUnify(Type t1, Int o1, Type t2, Int o2, Int z)
{
	Tyvar *tyv1, *tyv2;

	checkVars();
	unifyFails = 0;
	if (z==NONV)
		return TRUE;

	deRef(tyv1,t1,o1);
	deRef(tyv2,t2,o2);

un: if (tyv1)
		if (tyv2)
			return varToVarBind(tyv1,tyv2,z);	    /* t1, t2 variables    */
		else {
			Cell h2 = getDerefHead(t2,o2);	    /* t1 variable, t2 not */
			if (isSynonym(h2) && argCount>=tycon(h2).arity) {
				expandSyn(h2,argCount,&t2,&o2);
				deRef(tyv2,t2,o2);
				checkVars();
				goto un;
			}
			return varToTypeAccum(tyv1,t2,o2,negv(z));
		}
	else
		if (tyv2) {
			Cell h1 = getDerefHead(t1,o1);	    /* t2 variable, t1 not */
			if (isSynonym(h1) && argCount>=tycon(h1).arity) {
				expandSyn(h1,argCount,&t1,&o1);
				deRef(tyv1,t1,o1);
				checkVars();
				goto un;
			}
			return varToTypeAccum(tyv2,t1,o1,z);
		}
		else {					    /* t1, t2 not vars	   */
			Type h1 = getDerefHead(t1,o1);
			Int  a1 = argCount;
			Type h2 = getDerefHead(t2,o2);
			Int  a2 = argCount;
			checkVars();

#ifdef DEBUG_TYPES
			printf("tt unifying types: ");
			printType(stdout,debugType(t1,o1));
			printf(" with ");
			printType(stdout,debugType(t2,o2));
			putchar('\n');
#endif

			if (isOffset(h1) || isInt(h1)) h1=NIL;  /* represent var by NIL*/
			if (isOffset(h2) || isInt(h2)) h2=NIL;

			if (nonNull(h1) && h1==h2) {
				Cell z1 = getVariance(h1,a1);

				if (a1!=a2)
					return failure("incompatible constructors");
				while (isAp(t1)) {
					if (!preUnify(arg(t1),o1,arg(t2),o2,multv(z,thisVariance(z1))))
						return FALSE;
					checkVars();
					t1 = fun(t1);
					deRef(tyv1,t1,o1);
					t2 = fun(t2);
					deRef(tyv2,t2,o2);
					z1 = nextVariance(z1);
				}
				return TRUE;
			}

			/* Types do not match -- look for type synonyms to expand */

			if (isSynonym(h1) && a1>=tycon(h1).arity) {
				expandSyn(h1,a1,&t1,&o1);
				deRef(tyv1,t1,o1);
				checkVars();
				goto un;
			}
			if (isSynonym(h2) && a2>=tycon(h2).arity) {
				expandSyn(h2,a2,&t2,&o2);
				deRef(tyv2,t2,o2);
				checkVars();
				goto un;
			}
			checkVars();

			/* See if h1 and h2 are related by subtyping */

			if (!matchMode && z != INV &&
				isTycon(h1) && isTycon(h2) && tycon(h1).what==tycon(h2).what) {
					static String ho_err = "higher order subtyping axiom cannot be derived";
					Type ax=NIL,s1,s2/*,t*/;
					Int  o, n, hi, lo;

					if ((tycon(h1).what==DATATYPE) == (z==POSV)) {
						if (nonNull(ax = findAxiom(h2,h1))) {
							instantiate(ax,&s1,&o,0);
							s2 = satTycon(h2);
							hi = tycon(h2).arity-1;
							if (tcMode!=EXPRESSION)
								skolemOffsetsAbove(tycon(h2).arity,s1,o);
						}
					}
					else {
						if (nonNull(ax = findAxiom(h1,h2))) {
							instantiate(ax,&s2,&o,0);
							s1 = satTycon(h1);
							hi = tycon(h1).arity-1;
							if (tcMode!=EXPRESSION)
								skolemOffsetsAbove(tycon(h1).arity,s2,o);
						}
					}
					if (isNull(ax) || (n=(tycon(h1).arity-a1)) != (tycon(h2).arity-a2))
						return failure(0);

					/* If n > 0, try to derive a higher-order subtyping axiom */

					lo = hi-n;
					for (; n>0; n--, s1=fun(s1), s2=fun(s2))
						if (!isOffset(arg(s1)) || arg(s1) != arg(s2))
							return failure(ho_err);
					if (offsetsBetween(lo,hi,s1) || offsetsBetween(lo,hi,s2))
						return failure(ho_err);

					/* Check axiom against original types */

					return preUnify(t1,o1,s1,o,z) &&
						preUnify(s2,o,t2,o2,z);
			}

			if ((isNull(h1) && a1<=a2) ||       /* last attempt -- maybe   */
				(isNull(h2) && a2<=a1))	{	/* one head is a variable? */
					Cell z1 = nonNull(h1) ? getVariance(h1,a1) 
						: (nonNull(h2) ? getVariance(h2,a2)
						: mkInt(INV));
					for (;;) {
						deRef(tyv1,t1,o1);
						deRef(tyv2,t2,o2);
						checkVars();

						if (tyv1)				/* unify heads!	   */
							if (tyv2)
								return varToVarBind(tyv1,tyv2,z);
							else
								return varToTypeAccum(tyv1,t2,o2,negv(z));
						else if (tyv2)
							return varToTypeAccum(tyv2,t1,o1,z);

						/* at this point, neither t1 nor t2 is a variable. In  */
						/* addition, they must both be APs unless one of the   */
						/* head variables has been bound during unification of */
						/* the arguments.					   */

						checkVars();
						if (!isAp(t1) || !isAp(t2))		/* might not be APs*/
							return (t1==t2) ? TRUE : failure(0);
						/*must be APs      */
						if (!preUnify(arg(t1),o1,arg(t2),o2,multv(z,thisVariance(z1))))
							return FALSE;
						t1 = fun(t1);
						t2 = fun(t2);
						z1 = nextVariance(z1);
					}
			}

			if (matchMode || z==INV)
				return failure(0);

			/* One final attempt: handle arity mismatches.  Check if a      */
			/* unique suptyping axiom can be determined on basis of the     */
			/* arity requirements.  One typical example is the subtyping    */
			/* problem Action < m a, which can be solved by utilizing the   */
			/* axiom Action < Cmd ()  (in preference over Action < O s ())  */

			/* Todo: handle higher kinds here? */

			if (isTycon(h1) && isNull(h2) && a2>0) {
				List tcs = bestCandidates(matchArities(rootedAt(h1,z),a2),z);
				if (nonNull(tcs) && isNull(tl(tcs))) {
					Type t;
					Int  o;
					instantiate(findAxiom(h1,hd(tcs)),&t,&o,0);
					return preUnify(t1,o1,t,o,z) && preUnify(t,o,t2,o2,z);
				}
			}
			if (isTycon(h2) && isNull(h1) && a1>0) {
				List tcs = bestCandidates(matchArities(rootedAt(h2,z),a1),z);
				if (nonNull(tcs) && isNull(tl(tcs))) {
					Type t;
					Int  o;
					instantiate(findAxiom(h2,hd(tcs)),&t,&o,0);
					return preUnify(t1,o1,t,o,z) && preUnify(t,o,t2,o2,z);
				}
			}
		}
		return failure(0);
}

/* variance of tyv in current goal */
static Int local varianceInGoal(Tyvar *tyv)
{
	Tyvar saved = *tyv;
	Int  z;
	List gs;
	resetState(tyv);
	if (isNull(goalStack))
		internal("varianceInGoal");
	for (gs=hd(goalStack); nonNull(gs); gs=tl(gs))
		markVarianceTyvar(intOf(hd(gs)),POSV);
	z = tyv->offs;
	*tyv = saved;
	return z;
}

static Bool local isSubtype(Type t1, Type t2, Int  z)
{
	if (t1 == t2)
		return TRUE;
	if (z == INV)
		return FALSE;
	switch (whatIs(t1)) {
	case TUPLE   : return FALSE;

	case INTCELL : /* insert skolem constraint check here */ return FALSE;

	case TYCON   : if (isInt(t2)) {
		/* insert skolem constraint check here */ return FALSE;
				   }
				   else if ((tycon(t1).what==DATATYPE) == (z==POSV))
					   return nonNull(findAxiom(t2,t1));
				   else
					   return nonNull(findAxiom(t1,t2));
	}
	internal("isSubtype");
	return 0;
}

static Bool local allSubtypes(Tycon tc, List  tcs, Int   z)
{
	for (; nonNull(tcs); tcs=tl(tcs))
		if (!isSubtype(tc,hd(tcs),z))
			break;
	return isNull(tcs);
}

static List bestCandidates(List tcs, Int  z)
{
	List hs;
	if (nonNull(tcs) && nonNull(tl(tcs))) {
		for (hs = tcs; nonNull(hs); hs = tl(hs))
			if (allSubtypes(hd(hs),tcs,z))
				return singleton(hd(hs));
	}
	return tcs;
}

/* z==POSV gives supertypes, z==NEGV gives subtypes */
static List rootedAt(Tycon h, Int   z)
{
	List tcs = NIL;

	if (isTycon(h) && z!= INV) {
		Int what = tycon(h).what;

		if ((z==POSV) == (what==DATATYPE)) {
			/* Search all tycons, and add those that extend h */
			Tycon tc = TYCMIN, last = lastTycon();
			for (; tc <= last; tc++)
				if (tycon(tc).what==what && findAxiom(tc,h))
					tcs = cons(tc,tcs);
		} else {
			/* Add those tycons that h extends */
			List hs = tycon(h).axioms;
			for (; nonNull(hs); hs=tl(hs))
				tcs = cons(getHead(monoType(hd(hs))),tcs);
		}
	}
	return tcs;
}

static List matchArities(List tcs, Int  arity)
{
	List hs;
	if (nonNull(tcs)) {
		switch (whatIs(hd(tcs))) {
		case TUPLE:
			if (tupleOf(hd(tcs)) < arity)
				tcs = NIL;
			break;
		case INTCELL:
			if (length(tyvar(intOf(hd(tcs)))->kind) < arity)
				tcs = NIL;
			break;
		default:
			/* Some kind of tycon, multiple candidates may exist */
			if (isNull(tl(tcs)))
				break;
			for (hs = tcs, tcs = NIL; nonNull(hs); hs = tl(hs))
				if (tycon(hd(hs)).arity >= arity)
					tcs = cons(hd(hs),tcs);
		}
	}
	return tcs;
}

static Bool mergeBounds(Tyvar *tyv, Type  *t, Int   *o)
{
	Cell what  = NIL;
	Int  arity = 0;
	Kind kind  = tyv->kind;
	Bool skol  = FALSE;
	List tcs   = NIL;
	List bs    = tyv->bound;
	List prevs = NIL;
	Int  ztot  = NONV;
	List hs;
//	Int  n,k;

	/* Scan through all bounds for tyv */
	for (; nonNull(bs); bs=tl(bs)) {
		Type h = getDerefHead(fst(hd(bs)),0);
		Int  z = intOf(snd(hd(bs)));
		if (isInt(h)) skol = TRUE;  /* Mark for the purpose of a potential error message */
		prevs = cons(h, prevs);
		ztot |= z;

		/* First check whether bound is an arity constraint (i.e., a variable with args) */
		if (isInt(h) && !isSkolem(tyvar(intOf(h)))) {
			if (argCount > arity) {
				arity = argCount;
				kind = tyvar(intOf(h))->kind;
			}
			continue;
		}

		/* Now we know h is a constant (in the wider sense).  Is it the first one? */
		if (what == NIL ) {
			/* Yes, generate set of candidates: h + all its subs/sups */
			tcs = singleton(h);
			what = whatIs(h);
			if (what == TYCON) {
				what = tycon(h).what;
				if (z != INV && nonNull(tl(bs))) {
					tl(tcs) = rootedAt(h,z);
				}
			}
		} else {
			/* No, filter out non-viable candidates, i.e., those not covered by this h */
			if (z != INV && isTycon(h) && tycon(h).what == what) {
				for (hs = tcs, tcs = NIL; nonNull(hs); hs = tl(hs))
					if (isSubtype(h,hd(hs),z))
						tcs = cons(hd(hs),tcs);
			} else if (cellIsMember(h,tcs)) {
				/* In case of non-tycons or an equality constraint, there's only one solution */
				tcs = singleton(h);
			} else {
				/* Distinct tuples, skolems, or incompatible tycons -- just fail */
				tcs = NIL;
				break;
			}
		}
	}
	/* Done searching the bounds */
	if (what == NIL) {
		/* No tycons in the bounds, just create a tyvar of correct kind */
		Int vn = newTyvars(1);
		tyvar(vn)->kind = kind;
		tcs = singleton(mkInt(vn));
	} else {
		if (nonNull(tcs)) {
			/* Got some candidates.  Filter out those with insufficient arities */
			tcs = matchArities(tcs, arity);
		}

		if (nonNull(tcs) && nonNull(tl(tcs))) {
			/* Still several candidates, find best candidate depending on goal */
			Int bias = ztot!=INV ? ztot : (varianceInGoal(tyv) == NEGV ? NEGV : POSV);
			tcs = bestCandidates(tcs, bias);
		}
		/* Report possible errors */
		if (isNull(tcs) && skol)
			return failure("quantified variable prevents unification");
		else if (isNull(tcs))
			return failure("variable has conflicting bounds");
		else if (nonNull(tl(tcs)))
			return failure("variable has ambiguous bounds");
	}
	/* Got a solution!  Now make kinds match. */
	*t = hd(tcs);
	*o = newTyvars(0);
	{
		Kind k, k0 = tyv->kind;
		Int  n, i;

		switch (whatIs(*t)) {
		case TYCON   : k = tycon(*t).kind;
			break;
		case TUPLE   : k = simpleKind(tupleOf(*t));
			break;
		case INTCELL : k = tyvar(intOf(*t))->kind;
			break;
		default      : internal("mergeBounds");
		}
		for (n=0; isPair(k); k=snd(k),n++) {
			if (eqKind(k,k0))
				break;
			tyvar(newTyvars(1))->kind = fst(k);
		}
		if (k==STAR && k0!=STAR)
			return failure("kind error in higher-order bound");
		for (i=0; i<n; ++i)
			*t = ap(*t,mkOffset(i));
	}
	return TRUE;
}

static Bool local postUnify() {		/* solve accumulated constraints   */

	while (nonNull(accumVars)) {
		Tyvar *tyv;
		List  vs,bs;
		Type  t;
		Int   o;

		checkVars();
		for (vs=accumVars; nonNull(vs); vs=tl(vs)) {/* find unreferenced var */
			if (tyvar(intOf(hd(vs)))->offs == 0)
				break;
		}

		lastTyvar = intOf(hd(nonNull(vs) ? vs : accumVars));
		tyv = tyvar(lastTyvar);
		if (!isAccum(tyv))
			internal("postUnify");
		bs = lastBounds = tyv->bound;

		/* If vs==NIL we have a pending infinite type error, but we */
		/* continue until we have a directly recursive constraint   */
		/* for the sake of a more readable error message.           */

		if (vs==NIL) {
			for (; nonNull(bs); bs=tl(bs))
				if (occurs(tyv,var,intOf(fst(hd(bs)))))
					return failure("constraints are recursive");
			bs = lastBounds;
		}

		if (isNull(bs)) {	   /* short-circuit empty or single bounds */
			if (nonNull(vs))
				deAccum(tyv,TRUE);
			else
				accumVars = tl(accumVars);
			continue;
		} else if (isNull(tl(bs))) {
			countDown(intOf(fst(hd(bs))));
			tyv->bound = NIL;
			if (!solveAccum(tyv,var,intOf(fst(hd(bs)))))
				return FALSE;
			continue;
		}

		if (!mergeBounds(tyv,&t,&o))	  /* general case, result is (t,o) */
			return FALSE;
		if (!solveAccum(tyvar(lastTyvar),t,o))
			return FALSE;
		checkVars();
	}
	return TRUE;
}

/* Main unification routine	   */
/* unify (t1,o1) with (t2,o2)	   */
static Bool local unify(Type t1, Int o1, Type t2, Int o2)
{
	lastTyvar  = 0;
	lastBounds = NIL;
	if (!preUnify(t1,o1,t2,o2,POSV))
		return FALSE;
	return postUnify();
}

/* Compare types without binding */
static Bool local sameType(Type t1, Int o1, Type t2, Int o2)
{
	Bool result;
	matchMode = TRUE;
	result    = unify(t1,o1,t2,o2);
	matchMode = FALSE;
	return result;
}

/* --------------------------------------------------------------------------
* Unify kind expressions:
* ------------------------------------------------------------------------*/

/* Make binding tyv1 := tyv2	   */
/* for kind variable bindings	   */
static Bool local kvarToVarBind(Tyvar *tyv1, Tyvar *tyv2)
{
	if (tyv1!=tyv2) {
		tyv1->bound = var;
		tyv1->offs  = tyvNum(tyv2);
	}
	return TRUE;
}

/* Make binding tyv := (t,o)	   */
/* for kind variable bindings	   */
/* guaranteed not to be a v'ble or */
/* have synonym as outermost constr*/
static Bool local kvarToTypeBind(Tyvar *tyv, Type  t, Int   o)
{
	if (occurs(tyv,t,o))
		return failure("unification would give infinite kind");
	tyv->bound = t;
	tyv->offs  = o;
	return TRUE;
}

/* Unify kind expr (k1,o1) with	   */
/* (k2,o2)			   */
static Bool local kunify(Kind k1, Int o1, Kind k2, Int o2)
{
	Tyvar *kyv1, *kyv2;

	deRef(kyv1,k1,o1);
	deRef(kyv2,k2,o2);

	if (kyv1)
		if (kyv2)
			return kvarToVarBind(kyv1,kyv2);	    /* k1, k2 variables    */
		else
			return kvarToTypeBind(kyv1,k2,o2);	    /* k1 variable, k2 not */
	else
		if (kyv2)
			return kvarToTypeBind(kyv2,k1,o1);	    /* k2 variable, k1 not */
		else
			if (k1==STAR && k2==STAR)		    /* k1, k2 not vars	   */
				return TRUE;
			else if (isAp(k1) && isAp(k2))
				return kunify(fst(k1),o1,fst(k2),o2) &&
				kunify(snd(k1),o1,snd(k2),o2);
	return failure(0);
}

/*-------------------------------------------------------------------------*/

static Void local checkVars() {
#if 0
	List vs, bs;
	Tyvar *tyv, *tyv1, *tyv2;
	Int lenBs, beta, beta1, p, z, len = length(accumVars);

	for (vs=accumVars; nonNull(vs); vs=tl(vs)) {
		beta = intOf(hd(vs));
		tyv  = tyvar(beta);
		if (cellIsMember(hd(vs),tl(vs)))
			internal("duplicates in accumVars");
		if (!isAccum(tyv))
			internal("no accum");
		lenBs = length(tyv->bound);
		if (tyv->offs < 0)
			internal("negative refcount");
		for (bs=tyv->bound; nonNull(bs); bs=tl(bs)) {
			p = hd(bs);
			z = intOf(snd(p));
			beta1 = intOf(fst(p));
			tyv1 = tyvar(beta1);
			if (isAccum(tyv1))
				internal("bound has accum");
			if ((tyv2=getTypeVar(tyv1->bound,tyv1->offs)) && !isSkolem(tyv2))
				internal("var at head");
			countDown(beta1);
		}
	}
	for (vs=accumVars; nonNull(vs); vs=tl(vs)) {
		beta = intOf(hd(vs));
		tyv = tyvar(beta);
		if (tyv->offs != 0)
			internal("refCount mismatch");
	}
	for (vs=accumVars; nonNull(vs); vs=tl(vs)) {
		beta = intOf(hd(vs));
		tyv = tyvar(beta);
		for (bs=tyv->bound; nonNull(bs); bs=tl(bs)) {
			p = hd(bs);
			z = intOf(snd(p));
			beta1 = intOf(fst(p));
			tyv1 = tyvar(beta1);
			countUp(beta1);
		}
	}
	for (vs=skolVars; nonNull(vs); vs=tl(vs)) {
		List vs1;
		for (vs1=hd(vs); nonNull(vs1); vs1=tl(vs1)) {
			beta = intOf(hd(vs1));
			tyv = tyvar(beta);
			if (!isSkolem(tyv))
				internal("no skolem");
		}
	}
	for (vs=skolVars2; nonNull(vs); vs=tl(vs)) {
		List vs1;
		for (vs1=hd(vs); nonNull(vs1); vs1=tl(vs1)) {
			beta = intOf(hd(vs1));
			tyv = tyvar(beta);
			if (!isSkolem(tyv))
				internal("no skolem");
		}
	}
#endif
}

