/* --------------------------------------------------------------------------
* omonad.c:
*
* Implementation of the O'Haskell O monad.
*
* ------------------------------------------------------------------------*/

/*

type O s a      = (Error -> ()) -> (a -> ()) -> ()

return x        = \err -> \cont -> cont x

a >>= b         = \err -> \cont -> a err (\x -> b x err cont)


raise x         = \err -> \cont -> err x

a `catch` b     = \err -> \cont -> a (\x -> b x err cont) cont


get             = \err -> \cont -> cont self.state

set x           = \err -> \cont -> self.state := x;
cont ()

templ2 a c      = \err -> \cont -> let n = mkObj()
n.handler := \e k x -> c x e k -- pass c
n.state := a
cont n

act n a         = \err -> \cont -> if n /= self && n.msgs == NIL then
ready := ready ++ [n];
n.msgs := n.msgs ++ 
[a (n.ahandler doneO doneO) doneO];
cont ()

req n a = n.msgs := n.msg ++ 
[do (do a >>= reply self
handle \x -> n.handler x
raise x)
handle release self]

req n a         = \err -> \cont -> 
w := n;
while w.waitsFor /= NIL do
w := w.waitsFor;
if w == self then
err Deadlock;
if n.msgs == NIL then
ready = ready ++ [n];
n.msgs := n.msgs ++ 
[a (req2 n.handler (reply currentMsg self err)) 
(reply currentMsg self cont)]
()

req2 h rep x = h rep (const (rep x)) x

reply cur m cont x  = m.msgs := cont x : m.msgs;
m.waitsFor := NIL;
ready := ready ++ [m];
doneO ()

fix f err cont     =    x := error "fixM: black hole";
f x err (fix2 x err cont)

fix2 x err cont a  =    x := a;
cont a;

doneO x         = if self.msgs /= NIL then
ready := ready ++ [self];
()

uncaughtO x     = if failOnError
abandon "Process" x
else
()

run main        = let root = mkObj()
root.handler := uncaughtO
root.state := ()
root.msgs := (mkEnv >>= main) uncaughtO doneO
while not terminate do
while not terminate && not (null ready) do
self := hd ready
ready := tl ready
let msg = hd self.msgs
self.msgs := tl self.msgs
eval msg
if not terminate then
let ch = readTerminalChar() 
self := root
eval (charReader ch uncaughtO doneO)
*/
#if !VC32
extern char *strerror(int);
#endif

#include <errno.h>

static Void local pushStringN(String,Int);
static Void local pushString(String);

static Void local pushString(String s)
{
	pushStringN(s,strlen(s));
}

/* push pointer to string onto stack */
static Void local pushStringN(String s, Int l)
{
	push(nameNil);
	while (--l >= 0) {
		topfun(consChar(s[l]));
	}
}

#define msgsOf(o)      fst(snd(o))
#define waitsFor(o)    fst(snd(snd(o)))
#define stateOf(o)     fst(snd(snd(snd(o))))
#define handlerOf(o)   snd(snd(snd(snd(o))))

#define mkObj()        pair(OID,pair(NIL,pair(NIL,pair(NIL,NIL))))

#define objOf(m)       fst(snd(m))
#define codeOf(m)      fst(snd(snd(m)))
#define baselineOf(m)  fst(snd(snd(snd(m))))  /* absolute time */
#define deadlineOf(m)  snd(snd(snd(snd(m))))  /* absolute time */
#define mkMsg(o,c,b,d) pair(MSGTAG,pair(o,pair(c,pair(b,d))))

#define objDeadline(o) intOf(deadlineOf(hd(msgsOf(o))))

static Cell currentMsg;
static Cell rootMsg;

static Cell infTime;
static Cell zeroTime;

static Void insertTimeout(Cell);
static Void appendMessage(Cell);
static Void prependMessage(Cell,Cell);
static Bool killMessage(Cell);
static Void makeReady(Cell);
static Void checkPendingMsgs(Void);

static Void local insertTimeout(Cell msg)
{
	Int baseline = intOf(baselineOf(msg));
	List p       = NIL;
	List l       = pending;

	while (nonNull(l) && intOf(baselineOf(hd(l))) <= baseline) {
		p = l;
		l = tl(l);
	}

	BEGINPROTECT(pending);
	BEGINPROTECT(msg);
	if (isNull(p))
		pending = cons(msg,l);
	else
		tl(p) = cons(msg,l);
	ENDPROTECT();
	ENDPROTECT();
}

static Void local appendMessage(Cell msg)
{
	Cell obj     = objOf(msg);
	Int deadline = intOf(deadlineOf(msg));
	List p       = NIL;
	List l       = msgsOf(obj);

	BEGINPROTECT(msg);
	while (nonNull(l)) {
		if (intOf(deadlineOf(hd(l))) > deadline)
			deadlineOf(hd(l)) = deadlineOf(msg);
		p = l;
		l = tl(l);
	}
	if (isNull(p))
		msgsOf(obj) = singleton(msg);
	else
		tl(p) = singleton(msg);
	ENDPROTECT();
}

static Void local prependMessage(Cell obj, Cell msg)
{
	BEGINPROTECT(obj);
	BEGINPROTECT(msg);
	if (nonNull(msgsOf(obj))) {
		Cell d0 = deadlineOf(hd(msgsOf(obj)));
		if (intOf(d0) < intOf(deadlineOf(msg)))
			deadlineOf(msg) = d0;
	}
	msgsOf(obj) = cons(msg,msgsOf(obj));
	ENDPROTECT();
	ENDPROTECT();
}

static Bool local killMessage(Cell msg)
{
	if (nonNull(objOf(msg))) {
		Cell obj = objOf(msg), p, l;
		for (p=NIL, l=msgsOf(obj); nonNull(l) && hd(l)!=msg; p=l, l=tl(l)) ;
		if (nonNull(l)) {
			if (isNull(p)) msgsOf(obj) = tl(l); else tl(p) = tl(l);
			return TRUE;
		}
		for (p=NIL, l=pending; nonNull(l) && hd(l)!=msg; p=l, l=tl(l)) ;
		if (nonNull(l)) {
			if (isNull(p)) pending = tl(l); else tl(p) = tl(l);
			return TRUE;
		}
	}
	return FALSE;
}

static Void local makeReady(Cell obj)
{
	Int  d = objDeadline(obj);
	List p = NIL;
	List r = ready;
	while (nonNull(r) && (objDeadline(hd(r)) <= d)) {
		/* Since deadlines never increase, obj   */
		/* must already be at the right position */
		if (hd(r) == obj)
			return;
		p = r;
		r = tl(r);
	}

	BEGINPROTECT(ready);
	BEGINPROTECT(obj);

	/* nonNull(r) => objDeadline(hd(r)) > objDeadline(obj) */
	/* i.e. hd(r) can't be equal to obj here.              */
	if (isNull(p))
		ready = p = cons(obj,r);
	else {
		tl(p) = cons(obj,r);
		p = tl(p);
	}
	/* Now p is guaranteed to be nonNull */
	while (nonNull(r) && hd(r) != obj) {
		p = r;
		r = tl(r);
	}
	if (nonNull(r))
		tl(p) = tl(r);

	ENDPROTECT();
	ENDPROTECT();
}

Void checkPendingMsgs(Void) {
	BEGINPROTECT(ready);
	getCurrentTime();
	while (nonNull(pending)) {
		Cell msg = hd(pending);
		Cell obj = objOf(msg);
		if ((unsigned long)intOf(baselineOf(msg)) > currentTime)
			break;
		appendMessage(msg);
		if (isNull(waitsFor(obj)))
			makeReady(obj);
		pending = tl(pending);
	}

	ENDPROTECT();
}

primFun(primBaseline) {
	long bl;
	eval(primArg(1));
	bl = intOf(baselineOf(whnfHead));
	push(mkTuple(2));
	toparg(mkInt(bl / 1000 + startupTime));
	updapRoot(top(),mkInt((bl % 1000) * 1000));
}

primFun(primDeadline) {
	unsigned long deadline;
	eval(primArg(1));
	deadline = intOf(deadlineOf(whnfHead));
	if (deadline < MAXPOSINT)
		deadline = deadline - intOf(baselineOf(whnfHead));
	updateRoot(mkInt(deadline));
}

primFun(primDelivered) {
	eval(primArg(3));
	updapRoot(primArg(1), isNull(objOf(whnfHead)) ? nameTrue : nameFalse);
}

primFun(primAbort) {
	eval(primArg(3));
	updapRoot(primArg(1), killMessage(whnfHead) ? nameTrue : nameFalse);
}

primFun(primCurrentTag) {
	updapRoot(primArg(1), currentMsg);
}

primFun(primGet) {                      /* O monad state read              */
	updapRoot(primArg(1),stateOf(self));
}

primFun(primSet) {                      /* O monad state write             */
	stateOf(self) = primArg(3);
	updapRoot(primArg(1),nameUnit);
}

primFun(primTempl) {                    /* O monad object create           */
	Cell n = mkObj();
	push(namePass);
	toparg(primArg(3));
	handlerOf(n) = pop();               /* = pass 3 uncaughtO doneO       */
	stateOf(n) = primArg(4);            /* = 4                             */
	updapRoot(primArg(1),n);
}

primFun(primTag) {                      /* tag a err cont = a err (tagCont cont) */
	updapRoot(ap(primArg(3),primArg(2)),ap(nameTagCont,primArg(1))); // XXX
}

primFun(primTagCont) {                  /* tagCont cont x = cont lastMsg  */
	updapRoot(primArg(2),lastMsg);
}

primFun(primAfter) {
	eval(primArg(4));
	alpha = whnfInt;
	updapRoot(ap(primArg(3),primArg(2)),primArg(1));
}

primFun(primBefore) {
	eval(primArg(4));
	delta = whnfInt;
	updapRoot(ap(primArg(3),primArg(2)),primArg(1));
}

/* reset f err cont = do
let x = f currentBaseline currentDeadline err cont
clearTimeConstraints();             
x */
primFun(primReset) {
	push(ap(primArg(3),mkInt(alpha)));
	toparg(mkInt(delta));
	toparg(primArg(2));
	updapRoot(top(),primArg(1));
	clearTimeConstraints();
}

primFun(primSetBaseline) { /* Int -> Int -> Cmd () */
	unsigned long sec, usec;
	eval(primArg(4));
	sec = whnfInt;
	eval(primArg(3));
	usec = whnfInt;
	baselineOf(currentMsg) = mkInt((sec - startupTime) * 1000 + usec / 1000);
	updapRoot(primArg(1),nameUnit);
}

primFun(primAct) {                      /* O monad asynchronous send       */
	Int thisBaseline = intOf(baselineOf(currentMsg));
	Int thisDeadline = intOf(deadlineOf(currentMsg));
	Cell msg;
	getCurrentTime();
	alpha += thisBaseline;
	if (delta == 0) /* No deadline set, inherit current relative deadline  */
		if(thisDeadline < MAXPOSINT)
			delta = thisDeadline - thisBaseline + alpha;
		else 
			delta = MAXPOSINT;
	else if (delta < MAXPOSINT)
		delta += alpha;
	eval(primArg(4));
	push(ap(ap(handlerOf(whnfHead),nameDoneO),nameDoneO));
	topfun(primArg(3));
	toparg(nameDoneO);
	msg = mkMsg(whnfHead,pop(),mkInt(alpha),mkInt(delta));
	if ((unsigned long)alpha <= currentTime) {
		appendMessage(msg);
		if (whnfHead!=self && isNull(waitsFor(whnfHead)))
			makeReady(whnfHead);
	} else {
		insertTimeout(msg);
	}
	lastMsg = msg;
	updapRoot(primArg(1),nameUnit);
	clearTimeConstraints();
}

static local Bool missedDeadline() {
	getCurrentTime();
	if (currentTime > (unsigned long)intOf(deadlineOf(currentMsg))) {
		fprintf(stderr, "   **** Action ?: baseline %d, deadline %d, missed at %lu\n",
			intOf(baselineOf(currentMsg)),
			intOf(deadlineOf(currentMsg)) - intOf(baselineOf(currentMsg)),
			currentTime);
#if 0
		deadlineOf(currentMsg) = infTime;
#endif
		return TRUE;
	}
	return FALSE;
}

primFun(primReq) {                      /* O monad synchronous request     */
	Cell n, n1, msg;
	eval(primArg(4));
	n = whnfHead;
	n1 = waitsFor(n);
	while (nonNull(n1)) {
		n = n1;
		n1 = waitsFor(n1);
	}
	if (n == self) {
		updapRoot(primArg(2),nameDeadlock);
		return;
	}
	waitsFor(self) = whnfHead;
	push(ap(ap(ap(nameReply,currentMsg),self),primArg(2)));
	topfun(ap(nameReq2,handlerOf(whnfHead)));
	topfun(primArg(3));
	toparg(ap(ap(ap(nameReply,currentMsg),self),primArg(1)));
	msg = mkMsg(whnfHead,pop(),baselineOf(currentMsg),deadlineOf(currentMsg));
	appendMessage(msg);
	if (isNull(waitsFor(whnfHead)))
		makeReady(whnfHead);
	updateRoot(nameUnit);
}

primFun(primReq2) {
	push(ap(primArg(3),primArg(2)));
	toparg(ap(nameConst,ap(primArg(2),primArg(1))));
	updapRoot(top(),primArg(1));
}

primFun(primReply) {
	codeOf(primArg(4)) = ap(primArg(2),primArg(1));
	prependMessage(primArg(3), primArg(4));
	waitsFor(primArg(3)) = NIL;
	makeReady(primArg(3));
	updapRoot(nameDoneO,nameUnit);
}

primFun(primDoneO) {
	if (missedDeadline()) {
		//        updapRoot(primArg(2), nameMissedDeadline);
		//        return;
	}
	if (nonNull(msgsOf(self)))
		makeReady(self);
	updateRoot(nameUnit);
}

primFun(primUncaughtO) {
	if (failOnError)
		abandon("Process",top());
	if (nonNull(msgsOf(self)))
		makeReady(self);
	updateRoot(nameUnit);
}

primFun(primFix) { /* (a -> O s a) -> O s a */
	Cell x;
	pushString("fixM: black hole");
	x = ap(FIXDO, ap(nameError, top())); /* XXX eliminate FIXDO */
	push(ap(nameDeref, x));
	topfun(primArg(3));
	toparg(primArg(2));
	updapRoot(top(),ap(ap(ap(nameFix2, x), primArg(2)), primArg(1)));
}

primFun(primDeref) { /* Ref a -> a */
	eval(primArg(1));
	updateRoot(snd(whnfHead));
}

primFun(primFix2) { /* Ref a -> (Err -> ()) -> (a  -> ()) -> a -> () */
	eval(primArg(4));
	snd(whnfHead) = primArg(1);
	updapRoot(primArg(2), primArg(1));
}

primFun(primSetReader) {
#if O_IP
	extern void setReader(int, Cell);
	setReader(0, primArg(3));
#else
	charReader = primArg(3);
#endif
	updapRoot(primArg(1),nameUnit);
}

static void raiseErr(StackPtr root, Name n) {
	pushString(strerror(errno));
	updapRoot(primArg(2),ap(n,top()));
}


primFun(primWriteFileO) {
	fwritePrim(root,FALSE);
	if (fst(stack(root)) == primArg(2))
		raiseErr(root, nameFileError);
}

primFun(primAppendFileO) {
	fwritePrim(root,TRUE);
	if (fst(stack(root)) == primArg(2))
		raiseErr(root, nameFileError);
}

primFun(primReadFileO) {
	primReadFile(root);
	if (fst(stack(root)) == primArg(2))
		raiseErr(root, nameFileError);
}

static Bool terminate = FALSE;

primFun(primTerminate) {
	terminate = TRUE;
	updapRoot(primArg(1),nameUnit);
}

#define FINDTEXT(v,f,s)                      \
	if (isNull(v = f(findText(s)))) {        \
	fprintf(stderr,"%s not found\n",s);  \
	fflush(stderr);                      \
	abandon("Program execution", top()); \
	}

primFun(nullAct) {
	updapRoot(primArg(1),nameUnit);
}

static
	enum Etype { noMatch, 
	OProg, 
#if O_TK
	TkProg,
#if O_TIX
	TixProg,
#endif
#endif
} etype = noMatch;


#if O_IP
Bool checkSockets();
void initIP();
#  if O_TK
static Bool doCheckSockets = FALSE;
#  endif
#endif

/* Execute reactive program if it has type      */
/*   StdEnv -> Cmd ()  or                       */
/*   TkEnv -> Cmd () or TixEnv -> Cmd ()        */
/* return True if matching type found           */
Bool tryOExecute(Type type)
{
	Cell   temp, cb;
#if O_TK
	Name   nameTkEnvironment; 
	Type   typeTkEnvironment;
#if O_TIX 
	Name   nameTixEnvironment;
	Type   typeTixEnvironment;
#endif
#endif
	Name   nameEnv, nameBindCmd;

	String cmdAndArgs; 
	int   actNo;

	etype = noMatch;
	FINDTEXT(nameBindCmd,findName,"bindCmd");
#if O_TK
#if O_TIX
	if (!isNull(typeTixEnvironment=findTycon(findText("TixEnv"))) &&
		typeMatches(type,fn(typeTixEnvironment,typeProgO))) {
			etype = TixProg;
			FINDTEXT(nameTixEnvironment,findName,"primTixEnv");
			c_primInitTcl ();
			nameEnv = nameTixEnvironment;
	} else
#endif
		if (!isNull(typeTkEnvironment=findTycon(findText("TkEnv"))) &&
			typeMatches(type,fn(typeTkEnvironment,typeProgO))) {
				etype = TkProg;
				FINDTEXT(nameTkEnvironment,findName,"primTkEnv");
				c_primInitTcl ();
				nameEnv = nameTkEnvironment;
		} else
#endif
		{
			if (typeMatches(type,typeOProg)) {
				etype = OProg;
				FINDTEXT(nameStdEnvT,findName,"primStdEnvT");
				nameEnv = nameStdEnvT;
			}
		}

		if (etype==noMatch)
			return FALSE;

#if O_IP
		initIP();
#endif
		noechoTerminal();
		getStartupTime();

		ready = pending = NIL;
		rootObj = mkObj();
		stateOf(rootObj) = nameUnit;
		handlerOf(rootObj) = nameUncaughtO;
		topfun(ap(nameBindCmd,nameEnv));
		toparg(nameUncaughtO);
		toparg(nameDoneO);
		rootMsg = mkMsg(NIL,pop(),zeroTime,infTime);
		prependMessage(rootObj,rootMsg);
		makeReady(rootObj);
		clearTimeConstraints();
		for (terminate=FALSE; !terminate;) {
			while (!terminate && nonNull(ready)) {
				self = hd(ready);
				ready = tl(ready);
				currentMsg = hd(msgsOf(self));
				msgsOf(self) = tl(msgsOf(self));
				objOf(currentMsg) = NIL;
				temp = evalWithNoError(codeOf(currentMsg));
				checkPendingMsgs();
			}
			if (terminate)
				break;
			self = rootObj;
			currentMsg = rootMsg;
			switch (etype) {
#if O_TK
#if O_TIX
			case TixProg:
#endif
			case TkProg:
#define TIMEOUTEVENT -1
				if (nonNull(pending)) {
					char str[32];
					Int delta;
					getCurrentTime();
					delta = intOf(baselineOf(hd(pending)))-currentTime;
					sprintf(str, "after %d {doEvent %d}", delta,TIMEOUTEVENT);
					c_primExecuteTcl_(str);
				}
				terminate = !c_primRunTcl();
				if (!terminate){
#if O_IP
					if (doCheckSockets) {
						doCheckSockets = FALSE;
						checkSockets(); /* XXX open sockets are ignored */
						continue;
					} else
#endif
					{
						cmdAndArgs = c_primGetTcl();
						sscanf(cmdAndArgs,"%i",&actNo);
						if (actNo == TIMEOUTEVENT) {
							checkPendingMsgs();
							continue;
						} else if (nonNull(pending)) {
							char str[32];
							sprintf(str, "after cancel {doEvent %d}",TIMEOUTEVENT);
							c_primExecuteTcl_(str);
						}
						getCurrentTime();
						baselineOf(currentMsg) = mkInt(currentTime); /* XXX */
						cb = hd(skipOver(nrcallbacks-actNo,callbacks));
						push(cb);
						toparg(buildString(cmdAndArgs));
					}
				}
				break;
#endif
			default:
#if O_IP
				terminate = checkSockets();
				continue;
#else
				terminate = isNull(charReader);
				if (!terminate) {
					Int c;
					push(charReader);
					c = readTerminalChar();
					getCurrentTime();
					baselineOf(currentMsg) = mkInt(currentTime); /* XXX */
					toparg(c);
				}
#endif
				break;
			}
			if (!terminate) {
				toparg(nameUncaughtO);
				toparg(nameDoneO);
				temp = evalWithNoError(pop());
				if (nonNull(temp)) {
					push(temp);
					abandon("Program execution",top());
				}
			}
		}
#if O_TK
		if(etype==TkProg)c_deleteTcl();
#if O_TIX
		if(etype==TixProg)c_deleteTcl();
#endif
#endif
#if O_IP
		initIP();
#endif
		return TRUE;
}

int gArgc;
char **gArgv;

primFun(primProgArgs) {
	int i;
	push(nameNil);
	for(i=gArgc-1;i>=0;i--) {
		pushString(gArgv[i]);
		topfun(nameCons);
		pushed(1) = ap(top(),pushed(1));
		drop();
	}
}

primFun(primGetEnv) {
	char *s = getenv(evalName(primArg(3)));
	if(s) {
		pushString(s);
		topfun(nameJust);
	} else {
		push(nameNothing);
	}
	updapRoot(primArg(1),top());
}

#if UNIX
#include <sys/time.h>

Void getStartupTime(Void) {
	struct timeval t;
	gettimeofday(&t,NULL);
	startupTime = t.tv_sec;
	currentTime = 0;
}

Void getCurrentTime(Void) {
	struct timeval t;
	unsigned long newtime;
	gettimeofday(&t,NULL);
	newtime = (t.tv_sec - startupTime) * 1000 + t.tv_usec / 1000;
	/* Compensate for potential non-monotonicity bug in gettimeofday */
	if(newtime > currentTime)
		currentTime = newtime;
}

primFun(primTimeOfDay) {
	struct timeval t;
	gettimeofday(&t,NULL);
	updapRoot(primArg(1),ap(ap(mkTuple(2),mkInt(t.tv_sec)),mkInt(t.tv_usec)));
}

#else
#include <time.h>

Void getStartupTime(Void) {
	clock_t uptime = clock();
	startupTime = uptime;
}

Void getCurrentTime(Void) {
	clock_t uptime = clock();
	currentTime = ((uptime-startupTime) * 1000) / CLOCKS_PER_SEC;
}

primFun(primTimeOfDay) {
	clock_t uptime = clock();
	time_t t = time(NULL);
	long usec = (1000000/CLOCKS_PER_SEC) * (uptime % CLOCKS_PER_SEC);
	updapRoot(primArg(1),ap(ap(mkTuple(2),mkInt((Int)t)),mkInt(usec)));
}

#endif

#if O_IP

#if 0
# define DPRINTF(s) printf s
# if 0
#  define XDPRINTF(s) printf s
# else
#  define XDPRINTF(s)
# endif
#else
# define DPRINTF(s)
# define XDPRINTF(s)
#endif

#if VC32 | CYGWIN
typedef int socklen_t;
#endif
#if CYGWIN
#define SHUT_RDWR 2
#endif
#if VC32
#include <winsock2.h>
// conflicts with our definitions
#undef FD_READ
#undef FD_WRITE
#else
#include <netdb.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif
#include <fcntl.h>

#define FD_READCHAR 1
#define FD_READSOCK 2
#define FD_RECVSOCK 3
#define FD_ACCPSOCK 4
#define FD_READ     5
#define FD_WRITE    6
#define FD_MKTAG(t, fd) (((t) << 16) | fd)
#define FD_GETTAG(x) ((x) >> 16)
#define FD_GETFD(x) ((x) & 0xffff)

static int maxfd = -1;

local Void setReader(Int,Cell);
static local Void addSocket(Int, Int, Cell);
static local Void closeSocket(Int, Cell);
static local Void closeDeleteSocketArgs(Int);

#if O_TK

static local Void handleSockets(ClientData, Int);

static Void handleSockets(ClientData cd, Int mask) {
	DPRINTF(("handleSockets: sock=%d\n",(int)cd));
	stopTkMain = TRUE;
	doCheckSockets = TRUE;
}
#endif

static Void addSocket(Int tag, Int sock, Cell client) {
	Cell pr;
#if O_TK
	Bool isTkEnv;
	Tcl_Channel tc = NULL;
#if O_TIX
	isTkEnv = etype == TkProg || etype == TixProg;
#else  
	isTkEnv = etype == TkProg;
#endif
#endif
	DPRINTF(("addSocket: tag=%d, sock=%d\n",tag,sock));
	BEGINPROTECT(client);

#if O_TK
	if (isTkEnv && tag != FD_WRITE) {
		tc = Tcl_MakeFileChannel((ClientData)sock, TCL_READABLE);
		if (!tc) {
			fprintf(stderr, "addSocket: Tcl_MakeFileChannel returns 0\n");
			ENDPROTECT();
			abandon("Program execution", top());
		}
		DPRINTF(("Channel name: '%s'\n", Tcl_GetChannelName(tc)));
		Tcl_CreateChannelHandler(tc, TCL_READABLE|TCL_EXCEPTION,
			handleSockets, NULL);
	}
#endif
	pr = pair(mkInt(FD_MKTAG(tag, sock)), pair(client,mkInt((Int)
#if O_TK
		tc
#else
		0
#endif
		)));
	ip_clients = cons(pr, ip_clients);
	ENDPROTECT();
}

static Void closeSocket(Int sock, Cell p) {
	Int i;
#if O_TK
	i = intOf(snd(snd(p)));
	DPRINTF(("close channelhandler=%d\n",i));
	if (i && FD_GETTAG(intOf(fst(p))) != FD_WRITE) {
		Tcl_DeleteChannelHandler((Tcl_Channel)i, handleSockets, NULL);
		Tcl_Close(interp, (Tcl_Channel)i);
	} else
		close(sock);
#else           
	close(sock);
#endif
}

static Void closeDeleteSocket(Int sock) {
	Cell l, p, temp, last;
	DPRINTF(("closeDeleteSocket sock=%d\n",sock));

	last = NIL;
	for (l = ip_clients; l != NIL; l = snd(l)) {
		p = fst(l);
		if (FD_GETFD(intOf(fst(p))) == sock) {
			switch (FD_GETTAG(intOf(fst(p)))) {
			case FD_READSOCK:
			case FD_RECVSOCK:
			case FD_ACCPSOCK:
#if VC32
				shutdown(sock, SD_BOTH);
#else
				shutdown(sock, SHUT_RDWR);
#endif
				break;

			default:
				break;
			}
			closeSocket(sock, p);

			if (last == NIL) {
				ip_clients = snd(l);
			} else {
				snd(last) = snd(l);
			}

			push(fst(snd(p)));
			toparg(nameClosed);
			toparg(nameUncaughtO);
			toparg(nameDoneO);
			DPRINTF(("evaluating closed\n"));
			temp = evalWithNoError(pop());
			if (nonNull(temp)) {
				push(temp);
				abandon("Program execution",top());
			}
			return;
		} else {
			last = l;
		}
	}
	DPRINTF(("*** closeDeleteSocket: %d already closed\n", sock));
}

Void initIP() {
	Cell l, p;
	Int sock, tagfd;

#if VC32
	struct WSAData wsaData;
	static Bool winsockets_init = 0;
	Int nCode;

	if (!winsockets_init) {
		winsockets_init = 1;
		if ((nCode = WSAStartup(MAKEWORD(1, 1), &wsaData)) != 0) {
			fprintf(stderr, "Cannot initialize winsockets.\n");
			errAbort();
		}
	}
#endif
	DPRINTF(("initIP\n"));
	for (l = ip_clients; l != NIL; l = snd(l)) {
		p = fst(l);
		tagfd = intOf(fst(p));
		switch (FD_GETTAG(tagfd)) {
		case FD_READ:
		case FD_WRITE:
		case FD_READSOCK:
		case FD_RECVSOCK:
		case FD_ACCPSOCK:
			sock = FD_GETFD(tagfd);
			DPRINTF(("initIP: close sock=%d\n", sock));
			closeSocket(sock, p);
			break;
		}
	}
	ip_clients = NIL;
}

primFun(primGetHostByName) {
	String host;
	struct hostent *h;
	Int hostid = 0;

	host = evalName(primArg(1));
	if (!host)
		goto done;
	DPRINTF(("primGetHostByName: looking up='%s'\n", host));
#if VC32
	if (strcmp(host,"localhost")==0)
		host = "127.0.0.1";
	hostid = inet_addr(host);
	if (hostid != INADDR_NONE)
		goto done;
	hostid = 0;
#endif
	h = gethostbyname(host);
	if (!h)
		goto done;

	memcpy(&hostid, h->h_addr, sizeof hostid);
done:
	updateRoot(mkInt(hostid));
}

primFun(primGetNameOfHost) {
	char *addr;
	struct hostent *h;
	char *host;
	int hostid, alen;

	eval(primArg(1));
	hostid = whnfInt;
	DPRINTF(("primGetNameOfHost: hostid=%x\n", hostid));

	addr = (char *)&hostid;
	alen = sizeof addr;

	h = gethostbyaddr(addr, alen, AF_INET);

	if (h)
		host = (char *)h->h_name;
	else
		host = ""; /* inet_ntoa(*(struct in_addr *)addr); */

	pushString(host);
	updateRoot(top());
}

primFun(primInet_ntoa) {
	Int hostid;
	eval(primArg(1));
	hostid = whnfInt;
	pushString(inet_ntoa(*(struct in_addr *)&hostid));
	updateRoot(top());
}

primFun(primStrerror) {
	pushString(strerror(errno));
	updapRoot(primArg(1),top());
}

/* primitive primOpen :: Int -> Int -> (Int -> Cmd ()) -> Action */
primFun(primOpen) {
	Int port;
	Int sock, hostno;
	struct sockaddr_in saddr;

	DPRINTF(("primOpen: enter\n"));
	eval(primArg(5));
	hostno = whnfInt;
	eval(primArg(4));
	port = whnfInt;
	DPRINTF(("primOpen: host=%x port=%d\n", hostno, port));
	sock = socket(PF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		DPRINTF(("primOpen: socket() error=%d\n", errno));
		sock = 0;
	} else {
		saddr.sin_family = AF_INET;
		saddr.sin_addr.s_addr = hostno;
		saddr.sin_port = htons(port);
		/* XXX Should not block like this */
		if (connect(sock, (struct sockaddr *)&saddr, sizeof saddr) < 0) {
			DPRINTF(("primOpen: connect() error=%d\n", errno));
			sock = 0;
		}
	}
	push(ap(primArg(3), mkInt(sock)));
	toparg(nameUncaughtO);
	toparg(nameDoneO);
	DPRINTF(("primOpen, calling with sock=%d\n",sock));
	evalWithNoError(pop()); /* XXX */
	updapRoot(primArg(1), nameUnit);
}

/* Int -> Int -> PrimUDPHandler -> Request Int */
primFun(primOpenUDP) {
	Int port;
	Int sock, hostno;
	struct sockaddr_in saddr;

	DPRINTF(("primOpenUDP: enter\n"));
	eval(primArg(5));
	hostno = whnfInt;
	eval(primArg(4));
	port = whnfInt;
	DPRINTF(("primOpen: host=%x port=%d\n", hostno, port));
	sock = socket(PF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		DPRINTF(("primOpenUDP: socket() error=%d\n", errno));
		raiseErr(root,nameNetError);
		return;
	}
	if (port) {
		saddr.sin_family = AF_INET;
		saddr.sin_addr.s_addr = hostno;
		saddr.sin_port = htons(port);
		if (connect(sock, (struct sockaddr *)&saddr, sizeof saddr) < 0) {
			DPRINTF(("primOpenUDP: connect() error=%d\n", errno));
			raiseErr(root,nameNetError);
			return;
		}
	}
	addSocket(FD_RECVSOCK, sock, primArg(3));
	updapRoot(primArg(1), mkInt(sock));
}

/* primListen :: Int -> (Int -> Int -> Int -> Cmd ()) -> Request Int */
primFun(primListen) {
	Int port, sock;
	struct sockaddr_in saddr;

	DPRINTF(("primListen: enter\n"));

	eval(primArg(4));
	port = whnfInt;
	sock = socket(PF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		DPRINTF(("primListen: socket() error=%d\n", errno));
		sock = 0;
	}
	if (sock) {
		saddr.sin_family = AF_INET;
		saddr.sin_addr.s_addr = INADDR_ANY;
		saddr.sin_port = htons(port);
		if (bind(sock, (struct sockaddr *)&saddr, sizeof saddr) < 0) {
			DPRINTF(("primListen: bind() error: %s\n", strerror(errno)));
			sock = 0;
		} else {
			if (listen(sock, 3) < 0) {
				DPRINTF(("primListen: listen() error: %s\n", strerror(errno)));
				sock = 0;
			} else {
				addSocket(FD_ACCPSOCK, sock, primArg(3));
			}
		}
	}
	updapRoot(primArg(1),mkInt(sock));
}

/* Int -> PrimTCPClient -> Action */
primFun(primAddSocket) {
	Int sock;
	eval(primArg(4));
	sock = whnfInt;
	addSocket(FD_READSOCK, sock, primArg(3));
	updapRoot(primArg(1),nameUnit);
}

/* String -> PrimClient -> Request Int */
primFun(primOpenInStream) {
	Int fd;
	String path;
	path = evalName(primArg(4));
	fd = open(path,O_RDONLY);
	if (fd == -1)
		raiseErr(root,nameFileError);
	else {
		addSocket(FD_READ,fd, primArg(3));
		updapRoot(primArg(1),mkInt(fd));
	}
}

/* String -> Request Int */
primFun(primOpenOutStream) {
	Int fd;
	String path;
	path = evalName(primArg(3));
	fd = open(path,O_WRONLY);
	if (fd == -1)
		raiseErr(root,nameFileError);
	else {
		addSocket(FD_WRITE,fd, 0);
		updapRoot(primArg(1),mkInt(fd));
	}
}

Void setReader(int fd, Cell c) {
	ip_clients = cons(pair(mkInt(FD_MKTAG(FD_READCHAR, fd)), pair(c,mkInt(0))),
		ip_clients);
}

#if SOLARIS
#  ifndef _SOCKLEN_T
typedef size_t socklen_t;
#  endif
#endif

Bool checkSockets(Void) {
	Cell l, p, temp, client;
	socklen_t fromlen;
	Int sock, msock, n, addr;
	fd_set fdset;
	struct timeval zero;
	struct sockaddr_in from;
	char buffer[1024];

	XDPRINTF(("checkSockets\n"));

	getCurrentTime();
	baselineOf(currentMsg) = mkInt(currentTime); /* XXX */
	FD_ZERO(&fdset);
	msock = -1;
	for (l = ip_clients; l != NIL; l = snd(l)) {
		p = fst(l);
		if(FD_GETTAG(intOf(fst(p))) != FD_WRITE) {
			sock = FD_GETFD(intOf(fst(p)));
			XDPRINTF(("add sock=%d\n", sock));
			FD_SET(sock, &fdset);
			if (sock > msock)
				msock = sock;
		}
	}
	if (charReader) {
		FD_SET(0, &fdset);
		if (0 > msock)
			msock = 0;
	}
	if (msock < 0 && isNull(pending))
		return TRUE;
	if (msock > maxfd)
		maxfd = msock;
	if (nonNull(pending)) {
		Int baseline = intOf(baselineOf(hd(pending)));
		getCurrentTime();
		baseline -= (((unsigned long)baseline) > currentTime) ? currentTime : baseline;
		zero.tv_sec = baseline / 1000;
		zero.tv_usec = (baseline % 1000) * 1000;
	} else {
		zero.tv_sec = 0;        /* XXX remove timeout here */
		zero.tv_usec = 50000;   /* wait a little to avoid busy wait */
	}
	n = select(msock+1, &fdset, NULL, NULL, &zero);
	if (n == -1)
		DPRINTF(("select error: %s\n", strerror(errno)));
	XDPRINTF(("select()=%d, errno=%d\n", n, errno));
	if (n <= 0) {
		checkPendingMsgs();
		return FALSE;
	}
	DPRINTF(("input on %d sockets\n", n));
	for (l = ip_clients; l != NIL; l = snd(l)) {
		p = fst(l);
		sock = FD_GETFD(intOf(fst(p)));
		client = fst(snd(p));

		if (FD_ISSET(sock, &fdset)) {
			switch (FD_GETTAG(intOf(fst(p)))) {
			case FD_READCHAR:
				/* XXX should read from specified fd */
				DPRINTF(("reading input\n"));
				push(client);
				toparg(mkChar(readTerminalChar()));
				break;
			case FD_READSOCK:
			case FD_READ:
				n = read(sock, buffer, sizeof buffer);
				if (n >= 0) {
					DPRINTF(("READSOCK: sock=%d, data len=%d data[0]=0x%x\n", sock, n, buffer[0]));
					if (n == 0) {
						DPRINTF(("sock=%d zero read\n", sock));
						closeDeleteSocket(sock);
						continue;
					} else {
						pushStringN(buffer, n);
						topfun(ap(ap(ap(client, 
							nameDeliver),
							mkInt(0)),
							mkInt(0)));
					}
				} else {
					DPRINTF(("READSOCK: sock=%d, error=%s\n",sock,strerror(errno)));
					closeDeleteSocket(sock);
					continue;
					// errAbort();
				}
				break;
			case FD_RECVSOCK:
				fromlen = sizeof from;
				n = recvfrom(sock, buffer, sizeof buffer, 0, (struct sockaddr *)&from, &fromlen);
				if (n >= 0) {
					addr = from.sin_addr.s_addr;
					DPRINTF(("RECVSOCK: sock=%d, host=%x, port=%d, data len=%d data[0]=0x%x\n", 
						sock, addr, ntohs(from.sin_port), n, buffer[0]));
					pushStringN(buffer, n);
					topfun(ap(ap(ap(client, 
						nameDeliver),
						mkInt(addr)),
						mkInt(ntohs(from.sin_port))));
				} else {
					DPRINTF(("RECVSOCK: sock=%n, error=%s\n",sock,strerror(errno)));
					errAbort();
				}
				break;
			case FD_ACCPSOCK:
				fromlen = sizeof from;
				n = accept(sock, (struct sockaddr *)&from, &fromlen);
				if (n >= 0) {
					DPRINTF(("ACCPSOCK: sock=%d, n=%d\n", sock, n));
					push(ap(ap(ap(client, mkInt(from.sin_addr.s_addr)),
						mkInt(from.sin_port)),
						mkInt(n)));
				} else
					continue;
				break;
			default:
				abandon("Program execution", 0);
				break;
			}
			toparg(nameUncaughtO);
			toparg(nameDoneO);
			DPRINTF(("evaluating\n"));
			temp = evalWithNoError(pop());
			if (nonNull(temp)) {
				push(temp);
				abandon("Program execution",top());
			}
		}
	}

	return FALSE;
}

#if 0
static Int theSock;
static Void wrerror() {
	/* Note the error */
	/*printf("wrerror\n");*/
	closeDeleteSocket(theSock);
}
#endif

/* Int -> Packet -> Action */
primFun(primSend) {
	Int sock;
	char data[1024];
	Int r, pos;
#if 0
	Void (*oldsig)();
#endif
	Cell es = primArg(3);
	StackPtr saveSp = sp;

	eval(primArg(4));
	sock = whnfInt;

	DPRINTF(("primSend: sock=%d\n", sock));

	eval(es);
	for (;;) {
		pos = 0;

		while (whnfHead==nameCons && pos<sizeof data) {
			eval(pop());        
			data[pos++] = charOf(whnfHead);
			eval(pop());
		}
		DPRINTF(("primSend: datalen=%d data[0]=%x\n", pos, data[0]));
#if 0
		oldsig = signal(SIGPIPE, wrerror);
		theSock = sock;
#endif
		r = write(sock, data, pos);
#if 0
		signal(SIGPIPE, oldsig);
#endif
		if (r < 0) {
			sp = saveSp;
			raiseErr(root,nameNetError);
			closeDeleteSocket(sock);
			return;
		}

		if (whnfHead==nameNil)
			break;
	}
	sp = saveSp;

	updapRoot(primArg(1),nameUnit);
}

/* Int -> Action */
primFun(primClose) {
	Int sock;

	eval(primArg(3));
	sock = whnfInt;

	DPRINTF(("primClose: sock=%d\n", sock));

	closeDeleteSocket(sock);

	updapRoot(primArg(1),nameUnit);
}

/* Int -> Int -> Int -> [Byte] -> Action */
primFun(primTransmit) {
	Int hostno, port, sock, r, pos;
	socklen_t tolen;
	char data[8192];
	struct sockaddr_in to;
	Cell es = primArg(3);
	StackPtr saveSp = sp;

	DPRINTF(("primTransmit: enter\n"));
	eval(primArg(6));
	hostno = whnfInt;
	eval(primArg(5));
	port = whnfInt;
	eval(primArg(4));
	sock = whnfInt;

	to.sin_port = htons(port);
	to.sin_family = AF_INET;
	to.sin_addr.s_addr = hostno;
	tolen = sizeof to;

	eval(es);
	pos = 0;
	while (whnfHead==nameCons && pos<sizeof data) {
		eval(pop());        
		data[pos++] = charOf(whnfHead);
		eval(pop());
	}
	sp = saveSp;

	DPRINTF(("primTransmit: send to host=%x port=%d, len=%d\n", hostno, port, pos));
	r = sendto(sock, data, pos, 0, (struct sockaddr *)&to, tolen);

	if (r < 0) {
		raiseErr(root,nameNetError); /* XXX */
		return;
	}

	updapRoot(primArg(1),nameUnit);
}

/* Int -> PrimUDPHandler -> Request Int */
primFun(primListenUDP) {

	Int sock, port;
	struct sockaddr_in from;

	eval(primArg(4));
	port = whnfInt;

	DPRINTF(("primListenUDP: port=%d\n", port));
	sock = socket(PF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		DPRINTF(("primListenUDP: socket() error=%d\n", errno));
		raiseErr(root, nameNetError);
		return;
	}

	memset(&from, 0, sizeof from);
	from.sin_family = AF_INET;
	from.sin_port = htons(port);
	if (bind(sock, (struct sockaddr *)&from, sizeof from) < 0) {
		DPRINTF(("primListenUDP: bind() error=%d\n", errno));
		raiseErr(root, nameNetError);
		return;
	}
	addSocket(FD_RECVSOCK, sock, primArg(3));

	DPRINTF(("primListenUDP: add sock=%d\n", sock));
	updapRoot(primArg(1), mkInt(sock));
}
#endif

#if MIDI
#if VC32
#include <mmsystem.h>
HMIDIOUT midiHandle;
#endif

Bool     midiOpen = FALSE;

static void sendMIDI(int x, int y, int z) {
	int msg = x | (y<<8) | (z<<16);
	printf("Sending midi message %0x %0x %0x\n", x, y, z);
	if (!midiOpen) {
#if VC32
		midiOpen = !midiOutOpen(&midiHandle, MIDI_MAPPER, 0, 0, CALLBACK_NULL);
#endif
		if (!midiOpen) {
			return;
		}
	}
#if VC32
	midiOutShortMsg(midiHandle, msg);
#endif
}

static void closeMIDI() {
	if (midiOpen) {
		midiOpen = FALSE;
#if VC32
		midiOutClose(midiHandle);
#endif
	}
}

primFun(primSendMIDI) {                 /* send midi out message           */
	int x, y, z;
	eval(pop());
	x = whnfInt;
	eval(pop());
	y = whnfInt;
	eval(pop());
	z = whnfInt;
	sendMIDI(x, y, z);
	updapRoot(primArg(1), nameUnit);
}
#endif

