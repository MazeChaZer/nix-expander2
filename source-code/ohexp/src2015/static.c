/* --------------------------------------------------------------------------
* static.c:    Copyright (c) Mark P Jones 1991-1998.   All rights reserved.
*              See NOTICE for details and conditions of use etc...
*              Hugs version 1.3b, January 1998
*
* Static Analysis for Hugs
* ------------------------------------------------------------------------*/

#include "prelude.h"
#include "storage.h"
#include "connect.h"
#include "errors.h"

/* --------------------------------------------------------------------------
* local function prototypes:
* ------------------------------------------------------------------------*/

static Void  local checkTyconDefn(Tycon);
static Void  local depConstrs(Tycon,List,Cell);
static List  local depStructSels(Tycon,List,List);
static Type  local depTypeExp(Int,List,Type);
static Type  local depTypeVar(Int,List,Text);
static List  local selectCtxt(List,List);
static Void  local checkSynonyms(List);
static List  local visitSyn(List,Tycon,List);
static Void  local checkSubtypes(List);
static List  local visitTycon(List,List,Tycon,List);
static Void  local addAxiom(Tycon,Type);
static Int   local countTycon(Tycon,Type);
static Void  local inferVariances(List);
static Void  local deriveEval(List);
static List  local visitPossEval(Tycon,List,List);
static Void  local checkBanged(Name,Type,List);

static Type  local instantiateSyn(Type,Type);

static List  local typeVarsIn(Cell,List);
static List  local maybeAppendVar(Cell,List);

static List  local offsetTyvarsIn(Type,List);

static Type  local checkSigType(Int,String,Cell,Type);

static Void  local checkClassDefn(Class);
static Void  local depPredExp(Int,List,Cell);
static Void  local checkMems(Cell,List,Cell);
static Void  local addMembers(Class);
static Name  local newMember(Int,Int,Cell,Type);
static Int   local visitClass(Class);
static Int   local inferClassVariance(Class);


static Void  local checkInstDefn(Inst);
static Void  local checkInstSC(Inst);
static Cell  local scEvidFrom(Cell,List,Int);

static List  local classBindings(String,Class,List);
static Int   local memberNumber(Class,Text);
static List  local numInsert(Int,Cell,List);

static Void  local checkDerive(Tycon,List,List,Cell);
static Void  local addDerInst(Int,Class,List,List,Type,Int);
static Void  local deriveContexts(List);
static Void  local expandDComps(Inst);
static List  local superSimp(List);
static Void  local maybeAddPred(Cell,List);
static Cell  local instPred(Cell,Type);
static Void  local calcInstPreds(Inst);

static Void  local addDerivImp(Inst);
static List  local getDiVars(Int);
static Cell  local mkBind(String,List);
static Cell  local mkVarAlts(Int,Cell);

static List  local deriveEq(Tycon);
static Pair  local mkAltEq(Int,List);
static List  local deriveOrd(Tycon);
static Pair  local mkAltOrd(Int,List);
static List  local makeDPats2(Cell,Int);

static List  local deriveIx(Tycon);
static List  local deriveEnum(Tycon);
static Bool  local isEnumType(Tycon);
static List  local mkIxBinds(Int,Cell,Int);
static Cell  local prodRange(Int,List,Cell,Cell,Cell);
static Cell  local prodIndex(Int,List,Cell,Cell,Cell);
static Cell  local prodInRange(Int,List,Cell,Cell,Cell);

static List  local deriveShow(Tycon);
static Cell  local mkAltShow(Int,Cell,Int);
static Cell  local showsPrecRhs(Cell,Cell);

static List  local deriveRead(Tycon);

static List  local deriveBounded(Tycon);
static List  local mkBndBinds(Int,Cell,Int);

static Void  local checkPrimDefn(Triple);
static Void  local addNewPrim(Int,Text,String,Cell);

static Void  local checkDefaultDefns(Void);

static Cell  local checkPat(Int,Cell);
static Cell  local checkMaybeCnkPat(Int,Cell);
static Cell  local checkApPat(Int,Int,Cell);
static Void  local addPatVar(Int,Cell);
#if OBJ
static Cell  local checkAssignPat(Int,Cell);
static Void  local checkNoHiding(Int,Text);
#endif
static Name  local selDefined(Int,Text);
static Name  local conDefined(Int,Text);
static Void  local checkIsCfun(Int,Name);
static Void  local checkCfunArgs(Int,Cell,Int);

static Cell  local bindPat(Int,Cell);
static Void  local bindPats(Int,List);
static List  local bindGens(Int,List);

static List  local extractGens(List);
static List  local extractSigdecls(List);
static List  local extractBindings(List);
static List  local eqnsToBindings(List);
static Void  local notDefined(Int,List,Cell);
static Cell  local findBinding(Text,List);
static Void  local addSigDecl(List,Cell);
static Void  local setType(Int,Cell,Cell,List);

static List  local dependencyAnal(List);
static List  local topDependAnal(List);
static Void  local addDepField(Cell);
static Void  local remDepField(List);
static Void  local remDepField1(Cell);
static Void  local clearScope(Void);
static Void  local withinScope(List);
static Void  local leaveScope(Void);

static Void  local depBinding(Cell);
static Void  local depSelBind(Cell);
static Cell  local depStruct(Int,Cell);
static Void  local depDefaults(Class);
static Void  local depInsts(Inst);
static Void  local depClassBindings(List);
static Void  local depAlt(Cell);
static Void  local depRhs(Cell);
static Void  local depGuard(Cell);
static Cell  local depExpr(Int,Cell);
static Void  local depPair(Int,Cell);
static Void  local depTriple(Int,Cell);
static Void  local depDoComp(Int,Cell,List);
static Void  local depComp(Int,Cell,List);
static Void  local depCaseAlt(Int,Cell);
static Cell  local depVar(Int,Cell);
#if OBJ
static Void  local depTempl(Int,Cell);
#endif
static Void  local depGens(Int,List);

static Int   local sccMin(Int,Int);
static List  local tcscc(List,List);
static List  local bscc(List);

static Void  local addRSsigdecls(Pair);
static Void  local opDefined(List,Cell);
static Void  local allNoPrevDef(Cell);
static Void  local noPrevDef(Int,Cell);
static Void  local checkTypeIn(Pair);

/* --------------------------------------------------------------------------
* Static analysis of type declarations:
*
* Type declarations come in two forms:
* - data declarations - define new constructed data types
* - type declarations - define new type synonyms
*
* A certain amount of work is carried out as the declarations are
* read during parsing.  In particular, for each type constructor
* definition encountered:
* - check that there is no previous definition of constructor
* - ensure type constructor not previously used as a class name
* - make a new entry in the type constructor table
* - record line number of declaration
* - Build separate lists of newly defined constructors for later use.
* ------------------------------------------------------------------------*/

/* process new type definition	   */
Void tyconDefn(
	Int  line,				/* definition line number	   */
	Cell lhs,				/* left hand side of definition	   */
	Cell rhs,				/* right hand side of definition   */
	Cell what,				/* SYNONYM/DATATYPE/etc...	   */
	List axs				/* declared sub/supertypes	   */
	)
{
	Text t = textOf(getHead(lhs));

	if (nonNull(findTycon(t))) {
		ERRMSG(line) "Repeated definition of type constructor \"%s\"",
			textToStr(t)
			EEND;
	}
	else if (nonNull(findClass(t))) {
		ERRMSG(line) "\"%s\" used as both class and type constructor",
			textToStr(t)
			EEND;
	}
	else {
		Tycon nw	 = newTycon(t);
		tyconDefns       = cons(nw,tyconDefns);
		tycon(nw).line   = line;
		tycon(nw).arity  = argCount;
		tycon(nw).what   = what;
		tycon(nw).axioms = axs;
		if (what==RESTRICTSYN) {
			typeInDefns  = cons(pair(nw,snd(rhs)),typeInDefns);
			rhs          = fst(rhs);
		}
		tycon(nw).defn   = pair(lhs,rhs);
	}
}

/* set local synonyms for given	   */
/* binding group		   */
Void setTypeIns(List bs)
{
	List cvs = typeInDefns;
	for (; nonNull(cvs); cvs=tl(cvs)) {
		Tycon c  = fst(hd(cvs));
		List  vs = snd(hd(cvs));
		for (tycon(c).what = RESTRICTSYN; nonNull(vs); vs=tl(vs)) {
			if (nonNull(findBinding(textOf(hd(vs)),bs))) {
				tycon(c).what = SYNONYM;
				break;
			}
		}
	}
}

Void clearTypeIns() {			/* clear list of local synonyms	   */
	for (; nonNull(typeInDefns); typeInDefns=tl(typeInDefns))
		tycon(fst(hd(typeInDefns))).what = RESTRICTSYN;
}

/* --------------------------------------------------------------------------
* Further analysis of Type declarations:
*
* In order to allow the definition of mutually recursive families of
* data types, the static analysis of the right hand sides of type
* declarations cannot be performed until all of the type declarations
* have been read.
*
* Once parsing is complete, we carry out the following:
*
* - check format of lhs, extracting list of bound vars and ensuring that
*   there are no repeated variables and no Skolem variables.
* - run dependency analysis on rhs to check that only bound type vars
*   appear in type and that all constructors are defined.
*   Replace type variables by offsets, constructors by Tycons.
* - use list of dependents to sort into strongly connected components.
* - ensure that there is not more than one synonym in each group.
* - kind-check each group of type definitions.
*
* - check that there are no previous definitions for constructor
*   functions in data type definitions.
* - install synonym expansions and constructor definitions.
* ------------------------------------------------------------------------*/

static List tcDeps = NIL;		/* list of dependent tycons/classes*/
static Bool acceptWildcards = FALSE;    /* Accept wildcards in type-exprs? */

static Void local checkTyconDefn(Tycon d)	/* validate type constructor defn  */
{
	Cell lhs    = fst(tycon(d).defn);
	Cell rhs    = snd(tycon(d).defn);
	Int  line   = tycon(d).line;
	List tyvars = getArgs(lhs);
	List temp;
	/* check for repeated tyvars on lhs*/
	for (temp=tyvars; nonNull(temp); temp=tl(temp))
		if (nonNull(varIsMember(textOf(hd(temp)),tl(temp)))) {
			ERRMSG(line) "Repeated type variable \"%s\" on left hand side",
				textToStr(textOf(hd(temp)))
				EEND;
		}

		tcDeps = NIL;			/* find dependents		   */

		switch (whatIs(tycon(d).what)) {
		case PRIMTYPE    : internal("checkTyconDefn");

		case RESTRICTSYN :
		case SYNONYM	 : rhs = depTypeExp(line,tyvars,rhs);
			if (cellIsMember(d,tcDeps)) {
				ERRMSG(line) "Recursive type synonym \"%s\"",
					textToStr(tycon(d).text)
					EEND;
			}
			break;

		case DATATYPE	 :
		case NEWTYPE	 : depConstrs(d,tyvars,rhs);
			rhs = fst(rhs);
			break;

		case STRUCTTYPE  : if (nonNull(extractBindings(rhs))) {
			ERRMSG(line) 
				"Bindings not permitted in struct definition"
				EEND;
						   }
						   rhs = extractSigdecls(rhs);
						   rhs = depStructSels(d,tyvars,rhs);
						   break;

		default		 : internal("checkTyconDefn");
			break;
		}

		tycon(d).defn = rhs;
		tycon(d).kind = tcDeps;
		tcDeps	  = NIL;

		tycon(d).variance = NIL;
}

/* Define constructor functions and*/
/* do dependency analysis for data */
/* definitions (w or w/o deriving) */
static Void local depConstrs(Tycon t, List  tyvars, Cell  cd)
{
	Int  line      = tycon(t).line;
	Int  conNo     = 1;
	Type lhs	   = satTycon(t);
	List cs	   = fst(cd);
	List derivs    = snd(cd);
	List compTypes = NIL;
	List axs       = tycon(t).axioms;
	//	Int  i;

	/* slight semantic diff: now only  */
	/* newtype cons are failure-free   */
	if (tycon(t).what==NEWTYPE)
		conNo = 0;

	for (; nonNull(axs); axs=tl(axs)) {	/* for each subtype:		   */
		List sig = typeVarsIn(hd(axs),dupList(tyvars));
		Type sub = depTypeExp(line,sig,hd(axs));

		if (nonNull(sig)) {		/* Add quantifiers to type	   */
			List ts = sig;
			for (; nonNull(ts); ts=tl(ts))
				hd(ts) = NIL;
			sub = mkPolyType(sig,sub);
		}

		hd(axs) = sub;
	}

	for (; nonNull(cs); cs=tl(cs)) {	/* For each constructor function:  */
		Cell con   = hd(cs);
		List sig   = typeVarsIn(con,dupList(tyvars));
		List scs   = NIL;		/* strict components		   */
		Type type  = lhs;		/* constructor function type	   */
		Int  arity = 0;			/* arity of constructor function   */
		Name n;				/* name for constructor function   */

		Cell c = con;
		Int  compNo;
		for (; isAp(c); c=fun(c))
			arity++;
		for (compNo=arity, c=con; isAp(c); c=fun(c)) {
			Type t = arg(c);
			if (whatIs(t)==BANG) {
				scs = cons(mkInt(compNo),scs);
				t   = arg(t);
			}
			compNo--;
			arg(c) = depTypeExp(line,sig,t);
		}

		while (isAp(con)) {		/* Calculate type of constructor   */
			Type t   = fun(con);
			fun(con) = typeArrow;
			if (nonNull(derivs))	/* and build list of components	   */
				compTypes = cons(arg(con),compTypes);
			type     = ap(con,type);
			con      = t;
		}

		if (nonNull(sig)) {		/* Add quantifiers to type	   */
			List ts = sig;
			for (; nonNull(ts); ts=tl(ts))
				hd(ts) = NIL;
			type = mkPolyType(sig,type);
		}

		n = findName(textOf(con));	/* Allocate constructor fun name   */
		if (isNull(n))
			n = newName(textOf(con));
		else if (name(n).defn!=PREDEFINED) {
			ERRMSG(line) "Repeated definition for constructor function \"%s\"",
				textToStr(name(n).text)
				EEND;
		}
		name(n).arity  = arity;		/* Save constructor fun details	   */
		name(n).line   = line;
		name(n).number = cfunNo(conNo++);
		name(n).type   = type;
		if (tycon(t).what==NEWTYPE)
			name(n).defn = nameId;
		else
			implementCfun(n,scs);

		hd(cs) = n;
	}

	if (nonNull(derivs)) {		/* Generate derived instances	   */
		map3Proc(checkDerive,t,NIL,compTypes,derivs);
	}
}

/* Define struct selectors and     */
/* do dependency analysis for      */
/* struct definitions              */
static List local depStructSels(Tycon tc, List  tyvars, List  sd)
{
	Int  line  = tycon(tc).line;
	Int  selNo = 1;
	Type lhs   = satTycon(tc);
	List sels  = NIL;
	List axs   = tycon(tc).axioms;
	//	Int  i;

	for (; nonNull(axs); axs=tl(axs)) {	/* for each supertype:		   */
		List sig = typeVarsIn(hd(axs),dupList(tyvars));
		Type sup = depTypeExp(line,sig,hd(axs));

		if (nonNull(sig)) {		/* Add quantifiers to supertype	   */
			List ts = sig;
			for (; nonNull(ts); ts=tl(ts))
				hd(ts) = NIL;
			sup = mkPolyType(sig,sup);
		}

		hd(axs) = sup;
	}

	for (; nonNull(sd); sd=tl(sd)) {	/* For each selector definition:   */
		Int  line  = intOf(fst3(hd(sd)));
		List vs    = snd3(hd(sd));
		Type t     = thd3(hd(sd));
		List sig   = typeVarsIn(t,dupList(tyvars));

		if (whatIs(t) == QUAL) {
			ERRMSG(line) "Qualified type not allowed in selector definition"
				EEND;
		}
		t = depTypeExp(line,sig,t);

		for (; nonNull(vs); vs=tl(vs)) {
			List sig1    = dupList(sig);
			Type seltype = ap(ap(typeArrow,lhs),t);/* actual function type*/
			Text txt     = mkStructSel(textOf(hd(vs)));
			Name n       = findName(txt); /* Allocate selector fun name   */

			if (isNull(n))
				n = newName(txt);
			else if (name(n).defn!=PREDEFINED) {
				ERRMSG(line) "Repeated definition for struct selector \"%s\"",
					textToStr(textOf(hd(vs)))
					EEND;
			}

			if (nonNull(sig1)) {	/* Add quantifiers to type	   */
				List ts = sig1;
				for (; nonNull(ts); ts=tl(ts))
					hd(ts) = NIL;
				seltype = mkPolyType(sig1,seltype);
			}

			name(n).arity  = 0;		/* ... will be treated as nullary  */
			name(n).line   = line;      /*     by back end                 */
			name(n).number = cfunNo(selNo++);
			name(n).type   = seltype;
			name(n).defn   = n;		/* no special implementation       */

			sels = cons(n,sels);
		}
	}
	return sels;
}

static Type local depTypeExp(Int  line, List tyvars, Type type)
{
	switch (whatIs(type)) {
	case AP		: fst(type) = depTypeExp(line,tyvars,fst(type));
		snd(type) = depTypeExp(line,tyvars,snd(type));
		break;

	case VARIDCELL	: return depTypeVar(line,tyvars,textOf(type));

	case CONIDCELL	: {   Tycon tc = findTycon(textOf(type));
		if (isNull(tc)) {
			ERRMSG(line)
				"Undefined type constructor \"%s\"",
				textToStr(textOf(type))
				EEND;
		}
		if (cellIsMember(tc,tyconDefns) &&
			!cellIsMember(tc,tcDeps))
			tcDeps = cons(tc,tcDeps);
		return tc;
					  }

	case TYCON	:
	case TUPLE	: break;

	case WILDTYPE   : if (!acceptWildcards) {
		ERRMSG(line)
			"Wildcard types not allowed in this context"
			EEND;
					  }
					  break;

	default		: internal("depTypeExp");
	}
	return type;
}

static Type local depTypeVar(Int  line, List tyvars, Text tv)
{
	Int offset = 0;

	for (; nonNull(tyvars) && tv!=textOf(hd(tyvars)); offset++)
		tyvars = tl(tyvars);
	if (isNull(tyvars)) {
		ERRMSG(line) "Undefined type variable \"%s\"", textToStr(tv)
			EEND;
	}
	return mkOffset(offset);
}

static List local selectCtxt(List ctxt, List vs)	/* calculate subset of context	   */
{
	if (isNull(vs))
		return NIL;
	else {
		List ps = NIL;
		for (; nonNull(ctxt); ctxt=tl(ctxt)) {
			List us = offsetTyvarsIn(hd(ctxt),NIL);
			for (; nonNull(us) && cellIsMember(hd(us),vs); us=tl(us))
				;
			if (isNull(us))
				ps = cons(hd(ctxt),ps);
		}
		return rev(ps);
	}
}

/* Check for mutually recursive	   */
/* synonyms			   */
static Void local checkSynonyms(List ts)
{
	List syns = NIL;
	for (; nonNull(ts); ts=tl(ts)) {	/* build list of all synonyms	   */
		Tycon t = hd(ts);
		switch (whatIs(tycon(t).what)) {
		case SYNONYM     :
		case RESTRICTSYN : syns = cons(t,syns);
			break;
		}
	}
	while (nonNull(syns))		/* then visit each synonym	   */
		syns = visitSyn(NIL,hd(syns),syns);
}

/* visit synonym definition to look*/
/* for cycles			   */
static List local visitSyn(List  path, Tycon t, List  syns)
{
	if (cellIsMember(t,path)) {		/* every elt in path depends on t  */
		ERRMSG(tycon(t).line)
			"Type synonyms \"%s\" and \"%s\" are mutually recursive",
			textToStr(tycon(t).text), textToStr(tycon(hd(path)).text)
			EEND;
	}
	else {
		List ds    = tycon(t).kind;
		List path1 = NIL;
		for (; nonNull(ds); ds=tl(ds))
			if (cellIsMember(hd(ds),syns)) {
				if (isNull(path1))
					path1 = cons(t,path);
				syns = visitSyn(path1,hd(ds),syns);
			}
	}
	tycon(t).defn = fullExpand(tycon(t).defn);
	return removeCell(t,syns);
}

static Void local checkSubtypes(List ts)
{
	List ds = NIL;
	List ss = NIL;
	for (; nonNull(ts); ts=tl(ts)) {
		Tycon t = hd(ts);
		switch (whatIs(tycon(t).what)) {
		case DATATYPE   : ds = cons(t,ds);
			break;
		case STRUCTTYPE : ss = cons(t,ss);
			break;
		}
	}
	while (nonNull(ds))
		ds = visitTycon(NIL,NIL,hd(ds),ds);
	while (nonNull(ss))
		ss = visitTycon(NIL,NIL,hd(ss),ss);
}

static List local visitTycon(List  path, List  axpath, Tycon t, List  ts)
{
	if (cellIsMember(t,path)) {
		ERRMSG(tycon(t).line) "Subtype relation is cyclic" ETHEN
			for (; nonNull(path); path=tl(path),axpath=tl(axpath)) {
				Tycon h = hd(path);
				ERRTEXT "\n*** " ETHEN ERRAXIOM(hd(path),hd(axpath))
					if (hd(path)==t)
						break;
			}
			ERRTEXT "\n"
				EEND;
	}
	else {
		List axs   = tycon(t).axioms;
		List path1 = NIL;
		tycon(t).axioms = NIL;
		for (; nonNull(axs); axs=tl(axs)) {
			Type h;
			List axs1;

			hd(axs) = fullExpand(hd(axs));
			h = getHead(monoType(hd(axs)));
			if (!isTycon(h) || tycon(h).what != tycon(t).what) {
				ERRMSG(tycon(t).line) "Illegal %stype\n*** ", dir(t) ETHEN
					ERRAXIOM(t,hd(axs)) 
					ERRTEXT "\n"
					EEND;
			}
			if (cellIsMember(h,ts)) {
				if (isNull(path1))
					path1 = cons(t,path);
				ts = visitTycon(path1,cons(hd(axs),axpath),h,ts);
			}
			for (axs1=tycon(h).axioms; nonNull(axs1); axs1=tl(axs1))
				addAxiom(t,connectAxioms(t,hd(axs),hd(axs1)));
			addAxiom(t,hd(axs));
		}
		tycon(t).axioms = rev(tycon(t).axioms);
	}
	return removeCell(t,ts);
}

/* add ax to t's list of axioms   */
/* if not already present	  */
static Void local addAxiom(Tycon t, Type  ax)
{
	Type h      = getHead(monoType(ax));
	Type ax1    = findAxiom(t,h);

	if (isNull(ax1)) {
		Int axm = monoType(ax);
		Int nt = countTycon(t,axm);
		if (nt > 0 || countTycon(h,axm) > 1) {
			ERRMSG(tycon(t).line) 
				"Illegal use of \"%s\" in %stype argument",
				textToStr(tycon(nt > 0 ? t : h).text), dir(t) ETHEN
				ERRTEXT "\n*** " ETHEN ERRAXIOM(t,ax)
				ERRTEXT "\n"
				EEND;
		}
		tycon(t).axioms = cons(ax,tycon(t).axioms);
	}
	else if (!equalSchemes(ax1,ax)) {
		ERRMSG(tycon(t).line) "Ambiguous %stypes", dir(t) ETHEN
			ERRTEXT "\n*** " ETHEN ERRAXIOM(t,ax1)
			ERRTEXT "\n*** " ETHEN ERRAXIOM(t,ax)
			ERRTEXT "\n"
			EEND;
	}
}

static Int local countTycon(Tycon tc, Type  t)
{
	switch (whatIs(t)) {
	case TYCON : return (t==tc ? 1 : 0);
	case AP    : return countTycon(tc,fun(t)) + countTycon(tc,arg(t));
	default	   : return 0;
	}
}

static Void local inferVariances(List tcs)	/* Infer variance for tycons      */
{
	Bool changed;
	List tcs1;

	mapProc(initVariance,tcs);
	typeChecker(RESET);
	do {
		changed = FALSE;
		for (tcs1=tcs; nonNull(tcs1); tcs1=tl(tcs1))
			if (updateVariance(hd(tcs1)))
				changed = TRUE;
	} while (changed);
	typeChecker(RESET);
}

static Void local deriveEval(List tcs)	/* Derive instances of Eval	   */
{
	List ts1 = tcs;
	List ts  = NIL;
	/* Build list of rsyns and newtypes*/
	/* and derive instances for data   */
	for (; nonNull(ts1); ts1=tl(ts1)) {
		Tycon t = hd(ts1);
		switch (whatIs(tycon(t).what)) {
		case STRUCTTYPE  :
		case PRIMTYPE    :
		case DATATYPE    : addEvalInst(tycon(t).line,t,tycon(t).arity,NIL);
			break;
		case NEWTYPE     :
		case RESTRICTSYN : ts = cons(t,ts);
			break;
		}
	}
	while (nonNull(ts))			/* ... then visit each in turn	   */
		ts = visitPossEval(hd(ts),tl(ts),NIL);

	for (; nonNull(tcs); tcs=tl(tcs)) {	/* Check any banged components	   */
		Tycon t = hd(tcs);
		if (whatIs(tycon(t).what)==DATATYPE) {
			List cs = tycon(t).defn;
			for (; nonNull(cs); cs=tl(cs)) {
				Name c = hd(cs);
				if (isPair(name(c).defn)) {
					Type t    = name(c).type;
					List scs  = fst(name(c).defn);
					List ctxt = NIL;
					Int  n    = 1;
					if (isPolyType(t))
						t = monoTypeOf(t);
					if (whatIs(t)==QUAL) {
						ctxt = fst(snd(t));
						t    = snd(snd(t));
					}
					for (; nonNull(scs); scs=tl(scs)) {
						Int i = intOf(hd(scs));
						for (; n<i; n++)
							t = arg(t);
						checkBanged(c,arg(fun(t)),ctxt);
					}
				}
			}
		}
	}
}

/* Test to see if we can add an	   */
/* Eval instance for t		   */
static List local visitPossEval(Tycon t,
								List  ts,	/* [RESTRICTSYN | NEWTYPE]	   */
								List  ps	/* [Tycons already being visited]  */
								)
{
	Type ty = tycon(t).defn;		/* Find the `deciding type' ...	   */
	if (whatIs(tycon(t).what)==NEWTYPE) {
		ty = name(hd(tycon(t).defn)).type;
		if (isPolyType(ty))
			ty = monoTypeOf(ty);
		if (whatIs(ty)==QUAL)
			ty = snd(snd(ty));
		ty = arg(fun(ty));
	}

	for (;;) {				/* Scrutinize it ...		   */
		/* Find the constructor in the	   */
		/* head position of the type	   */
		Type h = getHead(ty);
		if (isSynonym(h))
			h = getHead(ty=fullExpand(ty));
		if (isOffset(h)) {		/* Variable in head position	   */
			if (argCount==0)		/* ... without any arguments	   */
				addEvalInst(tycon(t).line,
				t,
				tycon(t).arity,
				singleton(ap(classEval,h)));
			return ts;			/* ... with args, so no instance   */
		}
		else {				/* Tycon or tuple in head position */
			Int  a = argCount;
			Inst in;

			if (h==t || cellIsMember(h,ps)) {	/* ... already visited?	   */
				addEvalInst(tycon(t).line,t,tycon(t).arity,NIL);
				return ts;
			}

			if (cellIsMember(h,ts))		/* ... still to be visited */
				ts = visitPossEval(h,removeCell(h,ts),cons(t,ps));

			if (nonNull(in=findInst(classEval,h))) {
				if (nonNull(inst(in).specifics)) {
					Int n = offsetOf(arg(hd(inst(in).specifics)));
					while (++n < a)
						ty = fun(ty);
					ty = arg(ty);
					/* If there was a context, then we */
					/* need to go round the loop again */
					continue;
				}
				addEvalInst(tycon(t).line,t,tycon(t).arity,NIL);
			}
			return ts;
		}
	}
}

/* Check that banged component of c */
/* with type ty is an instance of   */
/* Eval under the predicates in ps. */
static Void local checkBanged(Name c, Type ty, List ps)
{
	Type ty1 = ty;			/* Save orig type, in case of err. */
	for (;;) {
		/* Find the constructor in the	   */
		/* head position of the type	   */
		Type h = getHead(ty);
		if (isSynonym(h))
			h = getHead(ty=fullExpand(ty));
		if (isOffset(h)) {		/* Variable in head position	   */
			if (argCount==0 && nonNull(scEvidFrom(pair(classEval,h),ps,0)))
				return;
			break;
		}
		else {				/* Tycon or tuple in head position */
			Int  a  = argCount;
			Inst in = findInst(classEval,h);
			if (nonNull(in)) {
				if (nonNull(inst(in).specifics)) {
					Int n = offsetOf(arg(hd(inst(in).specifics)));
					while (++n < a)
						ty = fun(ty);
					ty = arg(ty);
					/* If there was a context, then we */
					/* need to go round the loop again */
					continue;
				}
				return;			/* No context, so nothing to prove */
			}
			break;
		}
	}
	ERRMSG(name(c).line) "Illegal datatype strictness annotation:" ETHEN
		ERRTEXT "\n*** Constructor : "  ETHEN ERREXPR(c);
	ERRTEXT "\n*** Context     : "  ETHEN ERRCONTEXT(ps);
	ERRTEXT "\n*** Required    : "  ETHEN ERRPRED(ap(classEval,ty1));
	ERRTEXT "\n"
		EEND;
}

/* --------------------------------------------------------------------------
* Expanding out all type synonyms in a type expression:
* ------------------------------------------------------------------------*/

/* find full expansion of type exp */
/* assuming that all relevant      */
/* synonym defns of lower rank have*/
/* already been fully expanded	   */
Type fullExpand(Type t)
{
	Cell h = t;
	Int  n = 0;
	List args;
	for (args=NIL; isAp(h); h=fun(h), n++)
		args = cons(fullExpand(arg(h)),args);
	t = applyToArgs(h,args);
	if (isSynonym(h) && n>=tycon(h).arity)
		if (n==tycon(h).arity)
			t = instantiateSyn(tycon(h).defn,t);
		else {
			Type p = t;
			while (--n > tycon(h).arity)
				p = fun(p);
			fun(p) = instantiateSyn(tycon(h).defn,fun(p));
		}
		return t;
}

/* instantiate type according using*/
/* env to determine appropriate    */
/* values for OFFSET type vars	   */
static Type local instantiateSyn(Type t, Type env)
{
	switch (whatIs(t)) {
	case AP      : return ap(instantiateSyn(fun(t),env),
					   instantiateSyn(arg(t),env));

	case OFFSET  : return nthArg(offsetOf(t),env);

	default	     : return t;
	}
}

/* --------------------------------------------------------------------------
* Calculate set of variables appearing in a given type expression (possibly
* qualified) as a list of distinct values.  The order in which variables
* appear in the list is the same as the order in which those variables
* occur in the type expression when read from left to right.
* ------------------------------------------------------------------------*/

/* calculate list of type variables */
/* used in type expression, reading */
/* from left to right		   */
static List local typeVarsIn(Cell type, List vs)
{
	switch (whatIs(type)) {
	case AP        : return typeVarsIn(snd(type),
						 typeVarsIn(fst(type),
						 vs));
	case VARIDCELL :
	case VAROPCELL : return maybeAppendVar(type,vs);

	case QUAL      : {   List qs = fst(snd(type));
		vs = typeVarsIn(snd(snd(type)),vs);
		for (; nonNull(qs); qs=tl(qs))
			vs = typeVarsIn(hd(qs),vs);
		return vs;
					 }

	case BANG      : return typeVarsIn(snd(type),vs);
	}
	return vs;
}

/* append variable to list if not   */
/* already included		   */
static List local maybeAppendVar(Cell v, List vs)
{
	Text t = textOf(v);
	List p = NIL;
	List c = vs;

	while (nonNull(c)) {
		if (textOf(hd(c))==t)
			return vs;
		p = c;
		c = tl(c);
	}

	if (nonNull(p))
		tl(p) = cons(v,NIL);
	else
		vs    = cons(v,NIL);

	return vs;
}

/* --------------------------------------------------------------------------
* Check for ambiguous types:
* A type  Preds => type  is ambiguous if not (TV(P) `subset` TV(type))
* ------------------------------------------------------------------------*/

/* add list of offset tyvars in t  */
/* to list vs			   */
static List local offsetTyvarsIn(Type t, List vs)
{
	switch (whatIs(t)) {
	case AP	    : return offsetTyvarsIn(fun(t),offsetTyvarsIn(arg(t),vs));

	case OFFSET : if (cellIsMember(t,vs))
					  return vs;
				  else
					  return cons(t,vs);

	case QUAL   : return offsetTyvarsIn(snd(t),vs);

	default	    : return vs;
	}
}

/* Determine whether type is	   */
/* ambiguous 			   */
Bool isAmbiguous(Type type)
{
	if (isPolyType(type))
		type = monoTypeOf(type);
	if (whatIs(type)==QUAL) {		/* only qualified types can be	   */
		List tvps = offsetTyvarsIn(fst(snd(type)),NIL);	/* ambiguous	   */
		List tvts = offsetTyvarsIn(snd(snd(type)),NIL);
		while (nonNull(tvps) && cellIsMember(hd(tvps),tvts))
			tvps = tl(tvps);
		return nonNull(tvps);
	}
	return FALSE;
}

/* produce error message for	   */
/* ambiguity			   */
Void ambigError(Int    line, String where, Cell   e, Type   type)
{
	ERRMSG(line) "Ambiguous type signature in %s", where ETHEN
		ERRTEXT "\n*** ambiguous type : " ETHEN ERRTYPE(type);
	ERRTEXT "\n*** assigned to    : " ETHEN ERREXPR(e);
	ERRTEXT "\n"
		EEND;
}

/* --------------------------------------------------------------------------
* Type expressions appearing in type signature declarations and expressions
* also require static checking, but unlike type expressions in type decls,
* they may introduce arbitrary new type variables.  The static analysis
* required here is:
*   - ensure that each type constructor is defined and used with the
*     correct number of arguments.
*   - replace type variables by offsets, constructor names by Tycons.
*   - ensure that type is well-kinded.
* ------------------------------------------------------------------------*/

/* check validity of type expression*/
/* in explicit type signature	   */
static Type local checkSigType(Int    line, String where,
							   Cell   e, Type   type)
{
	List tyvars = typeVarsIn(type,NIL);
	Int  n      = length(tyvars);

	if (whatIs(type)==QUAL) {
		map2Proc(depPredExp,line,tyvars,fst(snd(type)));
		acceptWildcards = TRUE;
		snd(snd(type)) = depTypeExp(line,tyvars,snd(snd(type)));
		acceptWildcards = FALSE;

		if (isAmbiguous(type))
			ambigError(line,where,e,type);
	}
	else {
		acceptWildcards = TRUE;
		type = depTypeExp(line,tyvars,type);
		acceptWildcards = FALSE;
	}

	if (n>0)
		if (n>=NUM_OFFSETS) {
			ERRMSG(line) "Too many type variables in %s\n", where
				EEND;
		}
		else {
			List sig = dupList(tyvars);
			List ts  = sig;
			for (; nonNull(ts); ts=tl(ts))
				hd(ts) = NIL;
			type = mkPolyType(sig,type);
		}

		kindSigType(line,type);		/* check that type is well-kinded  */
		return type;
}

/* --------------------------------------------------------------------------
* Static analysis of class declarations:
*
* Performed in a similar manner to that used for type declarations.
*
* The first part of the static analysis is performed as the declarations
* are read during parsing.  The parser ensures that:
* - the class header and all superclass predicates are of the form
*   ``Class var''
*
* The classDefn() function:
* - ensures that there is no previous definition for class
* - checks that class name has not previously been used as a type constr.
* - make new entry in class table
* - record line number of declaration
* - build list of classes defined in current script for use in later
*   stages of static analysis.
* ------------------------------------------------------------------------*/

/* process new class definition	   */
Void classDefn(Int  line,	/* definition line number	   */
			   Cell head,	/* class header :: ([Supers],Class) */
			   List ms		/* class definition body		   */
			   )
{
	Text ct = textOf(fun(snd(head)));

	if (nonNull(findClass(ct))) {
		ERRMSG(line) "Repeated definition of class \"%s\"",
			textToStr(ct)
			EEND;
	}
	else if (nonNull(findTycon(ct))) {
		ERRMSG(line) "\"%s\" used as both class and type constructor",
			textToStr(ct)
			EEND;
	}
	else {
		Class nw	   = newClass(ct);
		cclass(nw).line    = line;
		cclass(nw).supers  = head;
		cclass(nw).members = ms;
		cclass(nw).level   = 0;
		classDefns	   = cons(nw,classDefns);
	}
}

/* --------------------------------------------------------------------------
* Further analysis of class declarations:
*
* Full static analysis of class definitions must be postponed until the
* complete script has been read and all static analysis on type definitions
* has been completed.
*
* Once this has been achieved, we carry out the following checks on each
* class definition:
* - check superclass declarations, replace by list of classes
* - split body of class into members and declarations
* - make new name entry for each member function
* - record member function number (eventually an offset into dictionary!)
* - no member function has a previous definition ...
* - no member function is mentioned more than once in the list of members
* - each member function type is valid, replace vars by offsets
* - qualify each member function type by class header
* - only bindings for members appear in defaults
* - only function bindings appear in defaults
* - check that extended class hierarchy does not contain any cycles
* ------------------------------------------------------------------------*/

static Void local checkClassDefn(Class c)    /* validate class definition	   */
{
	Cell head          = snd(cclass(c).supers);
	List tyvars        = singleton(arg(head));

	cclass(c).supers   = fst(cclass(c).supers);			/* supercl.*/
	tcDeps	       = NIL;
	map2Proc(depPredExp,cclass(c).line,tyvars,cclass(c).supers);
	cclass(c).numSupers= length(cclass(c).supers);
	mapOver(fst,cclass(c).supers);

	cclass(c).defaults = extractBindings(cclass(c).members);	/* defaults*/
	cclass(c).members  = extractSigdecls(cclass(c).members);
	fun(head)	       = c;
	arg(head)	       = mkOffset(0);
	map2Proc(checkMems,head,tyvars,cclass(c).members);
	cclass(c).sig      = tcDeps;
	tcDeps             = NIL;
}

static Void local depPredExp(Int  line, List tyvars, Cell pred)
{
	Class c = findClass(textOf(fun(pred)));
	if (isNull(c)) {
		ERRMSG(line) "Undefined class \"%s\"", textToStr(textOf(fun(pred)))
			EEND;
	}
	fun(pred) = c;
	arg(pred) = depTypeExp(line,tyvars,arg(pred));
	if (cellIsMember(c,classDefns) && !cellIsMember(c,tcDeps))
		tcDeps = cons(c,tcDeps);
}

static Void local checkMems(Cell h, List tyvars, Cell m)	/* check member function details   */
{
	Int  line = intOf(fst3(m));
	List vs   = snd3(m);
	Type t    = thd3(m);

	tyvars    = typeVarsIn(t,tyvars);
	if (whatIs(t)==QUAL) {		/* overloaded member signatures?  */
		List qs = fst(snd(t));
		for (; nonNull(qs); qs=tl(qs)) {
			depPredExp(line,tyvars,hd(qs));
			if (arg(hd(qs))==mkOffset(0)) {
				ERRMSG(line) "Illegal constraints on class variable \"%s\"",
					textToStr(textOf(hd(tyvars)))
					ETHEN ERRTEXT " in type of member function \"%s\"",
					textToStr(textOf(hd(vs)))
					EEND;
			}
		}
	}
	else
		t = ap(QUAL,pair(NIL,t));

	fst(snd(t)) = cons(h,fst(snd(t)));		/* Add main predicate	   */
	snd(snd(t)) = depTypeExp(line,tyvars,snd(snd(t)));

	if (isNull(tl(tyvars)))			
		t = mkPolyType(singleton(NIL),t);
	else {
		List sig   = NIL;
		List tvs   = tyvars;
		for (; nonNull(tvs); tvs=tl(tvs))
			sig = ap(NIL,sig);
		t          = mkPolyType(rev(sig),t);
		tl(tyvars) = NIL;			/* delete extra type vars  */
	}

	if (isAmbiguous(t))
		ambigError(line,"class declaration",hd(vs),t);

	thd3(m)    = t;				/* save type		   */
}

static Void local addMembers(Class c)		/* Add definitions of member funs  */
{
	Int  mno   = 1;			/* member function number	   */
	List mfuns = NIL;			/* list of member functions	   */
	List ms    = cclass(c).members;

	for (; nonNull(ms); ms=tl(ms)) {	/* cycle through each sigdecl	   */
		Int  line = intOf(fst3(hd(ms)));
		List vs   = rev(snd3(hd(ms)));
		Type t    = thd3(hd(ms));
		for (; nonNull(vs); vs=tl(vs))
			mfuns = cons(newMember(line,mno++,hd(vs),t),mfuns);
	}
	cclass(c).members    = rev(mfuns);	/* save list of members		   */
	cclass(c).numMembers = length(cclass(c).members);
	cclass(c).defaults   = classBindings("class",c,cclass(c).defaults);
}

/* Make definition for member fn   */
static Name local newMember(Int  l, Int  no, Cell v, Type t)
{
	Name m = findName(textOf(v));

	if (isNull(m))
		m = newName(textOf(v));
	else if (name(m).defn!=PREDEFINED) {
		ERRMSG(l) "Repeated definition for member function \"%s\"",
			textToStr(name(m).text)
			EEND;
	}

	name(m).line   = l;
	name(m).arity  = 1;
	name(m).number = mfunNo(no);
	name(m).type   = t;
	name(m).defn   = NIL;
	return m;
}

/* visit class defn to check that  */
/* class hierarchy is acyclic	   */
static Int local visitClass(Class c)
{
	if (cclass(c).level < 0) {		/* already visiting this class?	   */
		ERRMSG(cclass(c).line) "Class hierarchy for \"%s\" is not acyclic",
			textToStr(cclass(c).text)
			EEND;
	}
	else if (cclass(c).level == 0) {	/* visiting class for first time   */
		List scs = cclass(c).supers;
		Int  lev = 0;
		cclass(c).level = (-1);
		for (; nonNull(scs); scs=tl(scs)) {
			Int l = visitClass(hd(scs));
			if (l>lev) lev=l;
		}
		cclass(c).level = 1+lev;	/* level = 1 + max level of supers */
	}
	return cclass(c).level;
}

static Int local inferClassVariance(Class c)
{
	if (!cclass(c).variance) {
		String str = textToStr(cclass(c).text);
		Int z = 0;
		List cs = cclass(c).supers;
		List ms = cclass(c).members;

		for (; nonNull(cs); cs=tl(cs))
			z |= inferClassVariance(hd(cs));
		for (; nonNull(ms); ms=tl(ms))
			z |= getVarianceAt(name(hd(ms)).type,0);
		cclass(c).variance = z;
	}
	return cclass(c).variance;
}

/* --------------------------------------------------------------------------
* Static analysis of instance declarations:
*
* The first part of the static analysis is performed as the declarations
* are read during parsing:
* - make new entry in instance table
* - record line number of declaration
* - build list of instances defined in current script for use in later
*   stages of static analysis.
* ------------------------------------------------------------------------*/

/* process new instance definition  */
Void instDefn(Int  line,		/* definition line number	   */
			  Cell head,		/* inst header :: (context,Class)   */
			  List ms			/* instance members		   */
			  ) 
{
	Inst nw             = newInst();
	inst(nw).line       = line;
	inst(nw).specifics  = head;
	inst(nw).implements = ms;
	instDefns           = cons(nw,instDefns);
}

/* --------------------------------------------------------------------------
* Further static analysis of instance declarations:
*
* Makes the following checks:
* - Class part of header has form C (T a1 ... an) where C is a known
*   class, and T is a known datatype constructor (or restricted synonym),
*   and there is no previous C-T instance, and (T a1 ... an) has a kind
*   appropriate for the class C.
* - Each element of context is a valid class expression, with type vars
*   drawn from a1, ..., an.
* - All bindings are function bindings
* - All bindings define member functions for class C
* - Arrange bindings into appropriate order for member list
* - No top level type signature declarations
* ------------------------------------------------------------------------*/

static Void local checkInstDefn(Inst in)    /* validate instance declaration    */
{
	Int  line   = inst(in).line;
	Cell head   = snd(inst(in).specifics);
	List tyvars = getArgs(arg(head));
	Cell tmp;

	for (tmp=tyvars; nonNull(tmp); tmp=tl(tmp))	/* check for repeated var  */
		if (nonNull(varIsMember(textOf(hd(tmp)),tl(tmp)))) {
			ERRMSG(line) "Repeated type variable \"%s\" in instance predicate",
				textToStr(textOf(hd(tmp)))
				EEND;
		}
		depPredExp(line,tyvars,head);
		if (fun(head)==classEval) {
			ERRMSG(line) "Instances of class \"%s\" are generated automatically",
				textToStr(cclass(fun(head)).text)
				EEND;
		}
		inst(in).specifics = fst(inst(in).specifics);
		map2Proc(depPredExp,line,tyvars,inst(in).specifics);
		inst(in).numSpecifics = length(inst(in).specifics);

		tmp = getHead(arg(head));
		if (!isTycon(tmp) && !isTuple(tmp)) {
			ERRMSG(line) "Simple type required in instance declaration"
				EEND;
		}
		if (isSynonym(tmp)) {
			ERRMSG(line) "Type synonym \"%s\" not permitted in instance of \"%s\"",
				textToStr(tycon(tmp).text),
				textToStr(cclass(fun(head)).text)
				EEND;
		}

		inst(in).c     = fun(head);
		inst(in).t     = tmp;
		inst(in).arity = argCount;
		kindInst(in,head);

		if (nonNull(findInst(inst(in).c,inst(in).t))) {
			ERRMSG(line) "Repeated instance declaration for "
				ETHEN ERRPRED(head);
			ERRTEXT "\n"
				EEND;
		}
		cclass(inst(in).c).instances
			= appendOnto(cclass(inst(in).c).instances,singleton(in));


		if (nonNull(extractSigdecls(inst(in).implements))) {
			ERRMSG(line) "Type signature decls not permitted in instance decl"
				EEND;
		}
		inst(in).implements = classBindings("instance",
			inst(in).c,
			extractBindings(inst(in).implements));
}

/* --------------------------------------------------------------------------
* Verifying superclass constraints:
*
* Unlike Gofer, the Haskell report requires strict static checks on
* instance declarations to ensure that superclass hierarchies can be
* constructed.  The restrictions are outlined on Pages 41--42 of the
* Haskell 1.3 report.  The effect of these rules is that, for each
* pair of declarations:
*
*    class C a => D a where ...
*    instance ps => D (T a1 ... an) where ...
*
* there must also be an instance:
*
*    instance ps1 => C (T a1 ... an) where ...
*
* such that ps1 is always implied by ps.  Since Haskell and Hugs restrict
* these two contexts to predicates of the form Class var, this is equivalent
* to requiring that each pi' in ps1 is a subclass (not necessarily proper)
* of some pi in ps.
* ------------------------------------------------------------------------*/

/* check superclass constraints for*/
/* a given instance, in		   */
static Void local checkInstSC(Inst in)
{
	Class c   = inst(in).c;
	List  scs = cclass(c).supers;
	List  ps  = inst(in).specifics;
	Int   n   = dictSpecificsStart(c);

	for (inst(in).superBuild=NIL; nonNull(scs); scs=tl(scs)) {
		Class sc   = hd(scs);
		Inst  scin = findInst(sc,inst(in).t);
		List  ps1  = NIL;

		if (isNull(scin)) {				/* condition 1	   */
			Cell cpi  = makeInstPred(in);
			Cell scpi = ap(sc,arg(cpi));
			ERRMSG(inst(in).line) "Definition of "   ETHEN ERRPRED(cpi);
			ERRTEXT " requires superclass instance " ETHEN ERRPRED(scpi);
			ERRTEXT "\n"
				EEND;
		}

		for (ps1=inst(scin).specifics; nonNull(ps1); ps1=tl(ps1)) {
			Cell e = scEvidFrom(hd(ps1),ps,n);		/* condition 2	   */
			if (nonNull(e))
				scin = ap(scin,e);
			else {
				Cell cpi  = makeInstPred(in);
				Cell scpi = ap(sc,arg(cpi));
				ERRMSG(inst(in).line) "Cannot build superclass instance "
					ETHEN ERRPRED(scpi);
				ERRTEXT " of "		     ETHEN ERRPRED(cpi);
				ERRTEXT ":\n*** Context  : " ETHEN ERRCONTEXT(ps);
				ERRTEXT "\n*** Required : "  ETHEN ERRPRED(hd(ps1));
				ERRTEXT "\n"
					EEND;
			}
		}
		inst(in).superBuild = cons(scin,inst(in).superBuild);
	}
	inst(in).superBuild = rev(inst(in).superBuild);
}

/* Calculate evidence for pred	   */
/* pi from ps using superclass	   */
/* entailment			   */
static Cell local scEvidFrom(Cell pi, List ps, Int  n)
{
	for (; nonNull(ps); ps=tl(ps), n++)
		if (arg(pi)==arg(hd(ps))) {
			Cell e = superEvid(mkOffset(n),fun(hd(ps)),fun(pi));
			if (nonNull(e))
				return e;
		}
		return NIL;
}

/* --------------------------------------------------------------------------
* Process class and instance declaration binding groups:
* ------------------------------------------------------------------------*/

/* check validity of bindings bs for*/
/* class c (or an instance of c)    */
/* sort into approp. member order   */
static List local classBindings(String where, Class  c, List   bs)
{
	List nbs = NIL;

	for (; nonNull(bs); bs=tl(bs)) {
		Cell b  = hd(bs);
		Name nm = newName(inventText());   /* pick name for implementation */
		Int  mno;

		if (!isVar(fst(b))) {          /* only allows function bindings    */
			ERRMSG(rhsLine(snd(snd(snd(b)))))
				"Pattern binding illegal in %s declaration", where
				EEND;
		}

		if ((mno=memberNumber(c,textOf(fst(b))))==0) {
			ERRMSG(rhsLine(snd(hd(snd(snd(b))))))
				"No member \"%s\" in class \"%s\"",
				textToStr(textOf(fst(b))), textToStr(cclass(c).text)
				EEND;
		}

		name(nm).defn = snd(snd(b));   /* save definition of implementation*/
		nbs           = numInsert(mno-1,nm,nbs);
	}
	return nbs;
}

/* return number of member function */
/* with name t in class c           */
/* return 0 if not a member         */
static Int local memberNumber(Class c, Text  t)
{
	List ms = cclass(c).members;
	for (; nonNull(ms); ms=tl(ms))
		if (t==name(hd(ms)).text)
			return mfunOf(hd(ms));
	return 0;
}

/* insert x at nth position in xs,  */
/* filling gaps with NIL            */
static List local numInsert(Int  n, Cell x, List xs)
{
	List start = isNull(xs) ? cons(NIL,NIL) : xs;

	for (xs=start; 0<n--; xs=tl(xs))
		if (isNull(tl(xs)))
			tl(xs) = cons(NIL,NIL);
	hd(xs) = x;
	return start;
}

/* --------------------------------------------------------------------------
* Process derived instance requests:
* ------------------------------------------------------------------------*/

static List derivedInsts;		/* list of derived instances	   */
static Bool instsChanged;

/* verify derived instance request */
/* for tycon t, with explicit	   */
/* context p, component types ts   */
/* and named class ct		   */
static Void local checkDerive(Tycon t, List  p, List  ts, Cell  ct)
{
	Int   line = tycon(t).line;
	Class c    = findClass(textOf(ct));
	if (nonNull(tycon(t).axioms) && c!=classEq) {
		ERRMSG(line) "Extended types only support derived instances of class \"Eq\""
			EEND;
	}
	if (isNull(c)) {
		ERRMSG(line) "Unknown class \"%s\" in derived instance",
			textToStr(textOf(ct))
			EEND;
	}
	addDerInst(line,c,p,dupList(ts),t,tycon(t).arity);
}

/* add a derived instance	  */
static Void local addDerInst(Int   line, Class c,
							 List  p, List cts, Type  t, Int   a)
{
	Inst in;

	if (nonNull(findInst(c,t))) {
		ERRMSG(line) "Duplicate derived instance for class \"%s\"",
			textToStr(cclass(c).text)
			EEND;
	}

	/* set initial values for  */
	/* derived instance calc.  */
	p = appendOnto(dupList(p),singleton(NIL));
#define applyClass(t) ap(c,t)
	mapOver(applyClass,cts);
#undef  applyClass

	in		        = newInst();
	inst(in).c          = c;
	inst(in).t          = t;
	inst(in).arity      = a;
	inst(in).line       = line;
	inst(in).specifics  = ap(DERIVE,pair(p,cts));
	inst(in).implements = NIL;
	cclass(c).instances = appendOnto(cclass(c).instances,singleton(in));
	derivedInsts        = cons(in,derivedInsts);
}

/* Request derived instance of c   */
/* for mkTuple(n) constructor	   */
Void addTupInst(Class c, Int   n)
{
	Int  m              = n;
	List cts            = NIL;
	while (0<m--)
		cts = cons(mkOffset(m),cts);
	addDerInst(0,c,NIL,cts,mkTuple(n),n);
}

/* Add dummy instance for Eval	   */
Void addEvalInst(Int  line, Cell t, Int  arity, List ctxt)
{
	if (nonNull(findInst(classEval,t)))
		internal("addEvalInst");
	else {
		Inst in		      = newInst();
		inst(in).c	      = classEval;
		inst(in).t	      = t;
		inst(in).arity	      = arity;
		inst(in).line	      = line;
		inst(in).specifics    = ctxt;
		inst(in).numSpecifics = length(ctxt);
		cclass(classEval).instances
			= appendOnto(cclass(classEval).instances,singleton(in));
	}
}

/* Calculate contexts for derived  */
/* instances			   */
static Void local deriveContexts(List is)
{

	mapProc(addDerivImp,is);		/* First, add implementations and  */
	mapProc(expandDComps,is);		/* expand syns in component types  */

	do {				/* Main calculation of contexts	   */
		instsChanged = FALSE;
		mapProc(calcInstPreds,derivedInsts);
	} while (instsChanged);

	for (; nonNull(is); is=tl(is)) {	/* Extract and simplify results	   */
		inst(hd(is)).specifics
			= superSimp(initSeg(fst(snd(inst(hd(is)).specifics))));
		inst(hd(is)).numSpecifics = length(inst(hd(is)).specifics);
	}
}

/* Expand away synonyms appearing  */
/* in the component types	   */
static Void local expandDComps(Inst in)
{
	List cts = snd(snd(inst(in).specifics));
	for (; nonNull(cts); cts=tl(cts))
		snd(hd(cts)) = fullExpand(snd(hd(cts)));
}

/* Simplify preds in ps using super*/
/* class hierarchy ...		   */
static List local superSimp(List ps)
{
	Int n = length(ps);

	while (0<n--)
		if (nonNull(scEvidFrom(hd(ps),tl(ps),0)))
			ps = tl(ps);
		else {
			Cell tmp = tl(ps);
			tl(ps)   = NIL;
			ps       = appendOnto(tmp,ps);
		}
		return ps;
}

/* Add predicate pi to the list ps,*/
/* setting the instsChanged flag if*/
/* pi is not already a member.	   */
static Void local maybeAddPred(Cell pi, List ps)
{
	Class c = fun(pi);
	Cell  v = arg(pi);
	for (; nonNull(ps); ps=tl(ps))
		if (isNull(hd(ps))) {		/* reached the `dummy' end of list?*/
			hd(ps)       = pi;
			tl(ps)       = pair(NIL,NIL);
			instsChanged = TRUE;
			return;
		}
		else if (fun(hd(ps))==c && arg(hd(ps))==v)
			return;
}

/* Create instance of Hask pred pi */
/* under the simple substitution   */
/* represented by t		   */
static Cell local instPred(Cell pi, Type t)
{
	return ap(fun(pi),nthArg(offsetOf(arg(pi)),t));
}

/* Calculate next approximation	   */
/* of the context for a derived	   */
/* instance			   */
static Void local calcInstPreds(Inst in)
{
	List retain = NIL;
	List ps     = snd(snd(inst(in).specifics));
	List spcs   = fst(snd(inst(in).specifics));

	while (nonNull(ps)) {
		Cell pi = hd(ps);
		ps      = tl(ps);
		if (isClass(fun(pi))) {			/* Class type		   */
			if (isOffset(arg(pi)))		/* Class variable	   */
				maybeAddPred(pi,spcs);
			else {				/* Class (T t1 ... tn)	   */
				Class c   = fun(pi);
				Cell  t   = getHead(arg(pi));
				Inst  in1 = findInst(c,t);

				if (isNull(in1)) {		/* No suitable instance	   */
					Cell bpi = makeInstPred(in);
					ERRMSG(inst(in).line) "An instance of " ETHEN ERRPRED(pi);
					ERRTEXT " is required to derive "      ETHEN ERRPRED(bpi);
					ERRTEXT "\n"
						EEND;
				}				/* previously defined inst */
				else if (whatIs(inst(in1).specifics)!=DERIVE) {
					List qs = inst(in1).specifics;
					for (; nonNull(qs); qs=tl(qs))
						ps = cons(instPred(hd(qs),arg(pi)),ps);
				}
				else {				/* still being derived	   */
					List qs = fst(snd(inst(in1).specifics));
					for (; nonNull(hd(qs)); qs=tl(qs))
						ps = cons(instPred(hd(qs),arg(pi)),ps);
					retain = cons(pair(arg(pi),qs),retain);
					instsChanged = TRUE;
				}
			}
		}
		else {					/* Application of a subst  */
			List qs = snd(pi);			/* to a list of predicates,*/
			if (nonNull(hd(qs)))		/* given by a variable	   */
				instsChanged = TRUE;
			for (; nonNull(hd(qs)); qs=tl(qs))
				ps = cons(instPred(hd(qs),fst(pi)),ps);
			retain = cons(pair(fst(pi),qs),retain);
		}
	}

	snd(snd(inst(in).specifics)) = retain;
}

/* --------------------------------------------------------------------------
* Generate code for derived instances:
* ------------------------------------------------------------------------*/

static Void local addDerivImp(Inst in)
{
	List imp = NIL;
	if (inst(in).c==classEq)
		imp = deriveEq(inst(in).t);
	else if (inst(in).c==classOrd)
		imp = deriveOrd(inst(in).t);
	else if (inst(in).c==classEnum)
		imp = deriveEnum(inst(in).t);
	else if (inst(in).c==classIx)
		imp = deriveIx(inst(in).t);
	else if (inst(in).c==classShow)
		imp = deriveShow(inst(in).t);
	else if (inst(in).c==classRead)
		imp = deriveRead(inst(in).t);
	else if (inst(in).c==classBounded)
		imp = deriveBounded(inst(in).t);
	else {
		ERRMSG(inst(in).line) "Cannot derive instances of class \"%s\"",
			textToStr(cclass(inst(in).c).text)
			EEND;
	}

	inst(in).implements = classBindings("derived instance",
		inst(in).c,
		imp);
}

static List diVars = NIL;		/* Acts as a cache of invented vars*/
static Int  diNum  = 0;

/* get list of at least n vars for */
/* derived instance generation	   */
static List local getDiVars(Int n)
{
	for (; diNum<n; diNum++)
		diVars = cons(inventVar(),diVars);
	return diVars;
}

/* make a binding for a variable   */
static Cell local mkBind(String s, List   alts)
{
	return pair(mkVar(findText(s)),pair(NIL,alts));
}

/* make alts for binding a var to  */
/* a simple expression		   */
static Cell local mkVarAlts(Int  line, Cell r)
{
	return singleton(pair(NIL,pair(mkInt(line),r)));
}

/* --------------------------------------------------------------------------
* Given a datatype:   data T a b = A a b | B Int | C  deriving (Eq, Ord)
* The derived definitions of equality and ordering are given by:
*
*   A a b == A x y  =  a==x && b==y
*   B a   == B x    =  a==x
*   C     == C      =  True
*   _     == _      =  False
*
*   compare (A a b) (A x y) =  primCompAux a x (compare b y)
*   compare (B a)   (B x)   =  compare a x
*   compare C       C       =  EQ
*   compare a       x       =  cmpConstr a x
*
* In each case, the last line is only needed if there are multiple
* constructors in the datatype definition.
* ------------------------------------------------------------------------*/

#define ap2(f,x,y) ap(ap(f,x),y)

/* generate binding for derived == */
/* for some TUPLE or DATATYPE t	   */
static List local deriveEq(Type t)
{
	List alts = NIL;
	if (isTycon(t)) {			/* deal with type constrs	   */
		List ts = cons(t,tycon(t).axioms);
		for (; nonNull(ts); ts=tl(ts)) {
			Tycon h = getHead(monoType(hd(ts)));
			List cs = tycon(h).defn;
			for (; nonNull(cs); cs=tl(cs))
				alts = cons(mkAltEq(tycon(t).line,
				makeDPats2(hd(cs),name(hd(cs)).arity)),
				alts);
		}
		if (tycon(t).what!=NEWTYPE)
			alts = cons(pair(cons(WILDCARD,cons(WILDCARD,NIL)),
			pair(mkInt(tycon(t).line),nameFalse)),alts);
		alts = rev(alts);
	}
	else				/* special case for tuples	   */
		alts = singleton(mkAltEq(0,makeDPats2(t,tupleOf(t))));

	return singleton(mkBind("==",alts));
}

/* make alt for an equation for == */
/* using patterns in pats for lhs  */
/* arguments			   */
static Pair local mkAltEq(Int  line, List pats)
{
	Cell p = hd(pats);
	Cell q = hd(tl(pats));
	Cell e = nameTrue;

	if (isAp(p)) {
		e = ap2(nameEq,arg(p),arg(q));
		for (p=fun(p), q=fun(q); isAp(p); p=fun(p), q=fun(q))
			e = ap2(nameAnd,ap2(nameEq,arg(p),arg(q)),e);
	}
	return pair(pats,pair(mkInt(line),e));
}

/* make binding for derived compare*/
/* for some TUPLE or DATATYPE t	   */
static List local deriveOrd(Type t)
{
	List alts = NIL;
	if (isEnumType(t))			/* special case for enumerations   */
		alts = mkVarAlts(tycon(t).line,nameConCmp);
	else if (isTycon(t)) {		/* deal with type constrs	   */
		List cs = tycon(t).defn;
		for (; nonNull(cs); cs=tl(cs))
			alts = cons(mkAltOrd(tycon(t).line,
			makeDPats2(hd(cs),name(hd(cs)).arity)),
			alts);
		if (cfunOf(hd(tycon(t).defn))!=0) {
			Cell u = inventVar();
			Cell w = inventVar();
			alts   = cons(pair(cons(u,singleton(w)),
				pair(mkInt(tycon(t).line),
				ap2(nameConCmp,u,w))),alts);
		}
		alts = rev(alts);
	}
	else				/* special case for tuples	   */
		alts = singleton(mkAltOrd(0,makeDPats2(t,tupleOf(t))));

	return singleton(mkBind("compare",alts));
}

/* make alt for eqn for compare	   */
/* using patterns in pats for lhs  */
/* arguments			   */
static Pair local mkAltOrd(Int  line, List pats)
{
	Cell p = hd(pats);
	Cell q = hd(tl(pats));
	Cell e = nameEQ;

	if (isAp(p)) {
		e = ap2(nameCompare,arg(p),arg(q));
		for (p=fun(p), q=fun(q); isAp(p); p=fun(p), q=fun(q))
			e = ap(ap2(nameCompAux,arg(p),arg(q)),e);
	}

	return pair(pats,pair(mkInt(line),e));
}

/* generate pattern list	   */
/* by putting two new patterns with*/
/* head h and new var components   */
static List local makeDPats2(Cell h, Int  n)
{
	List us = getDiVars(2*n);
	List vs = NIL;
	Cell p;
	Int  i;

	for (i=0, p=h; i<n; ++i) {		/* make first version of pattern   */
		p  = ap(p,hd(us));
		us = tl(us);
	}
	vs = cons(p,vs);

	for (i=0, p=h; i<n; ++i) {		/* make second version of pattern  */
		p  = ap(p,hd(us));
		us = tl(us);
	}
	return cons(p,vs);
}

/* --------------------------------------------------------------------------
* Deriving Ix and Enum:
* ------------------------------------------------------------------------*/

static List local deriveEnum(Tycon t)	/* Construct definition of enumeration	   */
{
	Int l = tycon(t).line;

	if (!isEnumType(t)) {
		ERRMSG(l) "Can only derive instances of Enum for enumeration types"
			EEND;
	}

	return cons(mkBind("toEnum",mkVarAlts(l,ap(nameEnToEn,hd(tycon(t).defn)))),
		cons(mkBind("fromEnum",mkVarAlts(l,nameEnFrEn)),
		cons(mkBind("enumFrom",mkVarAlts(l,nameEnFrom)),
		cons(mkBind("enumFromTo",mkVarAlts(l,nameEnFrTo)),
		cons(mkBind("enumFromThen",mkVarAlts(l,nameEnFrTh)),NIL)))));
}

static List local deriveIx(Tycon t)	/* Construct definition of indexing	   */
{
	if (isEnumType(t))		/* Definitions for enumerations		   */
		return cons(mkBind("range",mkVarAlts(tycon(t).line,nameEnRange)),
		cons(mkBind("index",mkVarAlts(tycon(t).line,nameEnIndex)),
		cons(mkBind("inRange",mkVarAlts(tycon(t).line,nameEnInRng)),
		NIL)));
	else if (isTuple(t))	/* Definitions for product types	   */
		return mkIxBinds(0,t,tupleOf(t));
	else if (isTycon(t) && cfunOf(hd(tycon(t).defn))==0)
		return mkIxBinds(tycon(t).line,
		hd(tycon(t).defn),
		name(hd(tycon(t).defn)).arity);

	ERRMSG(tycon(t).line)
		"Can only derive instances of Ix for enumeration or product types"
		EEND;
	return NIL;/* NOTREACHED*/
}

/* Determine whether t is an enumeration   */
/* type (i.e. all constructors arity == 0) */
static Bool local isEnumType(Tycon t)
{
	if (isTycon(t) && (tycon(t).what==DATATYPE || tycon(t).what==NEWTYPE)) {
		List cs = tycon(t).defn;
		for (; nonNull(cs); cs=tl(cs))
			if (name(hd(cs)).arity!=0)
				return FALSE;
		addCfunTable(t);
		return TRUE;
	}
	return FALSE;
}

/* build bindings for derived Ix on*/
/* a product type		   */
static List local mkIxBinds(Int  line, Cell h, Int  n)
{
	List vs   = getDiVars(3*n);
	Cell ls   = h;
	Cell us   = h;
	Cell is   = h;
	Cell pr   = NIL;
	Cell pats = NIL;
	Int  i;

	for (i=0; i<n; ++i, vs=tl(vs)) {	/* build three patterns for values */
		ls = ap(ls,hd(vs));		/* of the datatype concerned	   */
		us = ap(us,hd(vs=tl(vs)));
		is = ap(is,hd(vs=tl(vs)));
	}
	pr   = ap2(mkTuple(2),ls,us);	/* Build (ls,us)		   */
	pats = cons(pr,cons(is,NIL));	/* Build [(ls,us),is]		   */

	return cons(prodRange(line,singleton(pr),ls,us,is),
		cons(prodIndex(line,pats,ls,us,is),
		cons(prodInRange(line,pats,ls,us,is),NIL)));
}

/* Make definition of range for a  */
/* product type			   */
static Cell local prodRange(Int  line, List pats, Cell ls, Cell us, Cell is)
{
	/* range :: (a,a) -> [a]
	* range (X a b c, X p q r)
	*   = [ X x y z | x <- range (a,p), y <- range (b,q), z <- range (c,r) ]
	*/
	Cell is1 = is;
	List e   = NIL;
	for (; isAp(ls); ls=fun(ls), us=fun(us), is=fun(is))
		e = cons(ap(FROMQUAL,pair(arg(is),
		ap(nameRange,ap2(mkTuple(2),
		arg(ls),
		arg(us))))),e);
	e = ap(COMP,pair(is1,e));
	e = singleton(pair(pats,pair(mkInt(line),e)));
	return mkBind("range",e);
}

/* Make definition of index for a  */
/* product type			   */
static Cell local prodIndex(Int  line, List pats, Cell ls, Cell us, Cell is)
{
	/* index :: (a,a) -> a -> Bool
	* index (X a b c, X p q r) (X x y z)
	*  = index (c,r) z + rangeSize (c,r) * (
	*     index (b,q) y + rangeSize (b,q) * (
	*      index (a,x) x))
	*/
	List xs = NIL;
	Cell e  = NIL;
	for (; isAp(ls); ls=fun(ls), us=fun(us), is=fun(is))
		xs = cons(ap2(nameIndex,ap2(mkTuple(2),arg(ls),arg(us)),arg(is)),xs);
	for (e=hd(xs); nonNull(xs=tl(xs));) {
		Cell x = hd(xs);
		e = ap2(namePlus,x,ap2(nameMult,ap(nameRangeSize,arg(fun(x))),e));
	}
	e = singleton(pair(pats,pair(mkInt(line),e)));
	return mkBind("index",e);
}

/* Make definition of inRange for a*/
/* product type			   */
static Cell local prodInRange(Int  line, List pats, Cell ls, Cell us, Cell is)
{
	/* inRange :: (a,a) -> a -> Bool
	* inRange (X a b c, X p q r) (X x y z)
	*          = inRange (a,p) x && inRange (b,q) y && inRange (c,r) z
	*/
	Cell e = ap2(nameInRange,ap2(mkTuple(2),arg(ls),arg(us)),arg(is));
	while (ls=fun(ls), us=fun(us), is=fun(is), isAp(ls))
		e = ap2(nameAnd,
		ap2(nameInRange,ap2(mkTuple(2),arg(ls),arg(us)),arg(is)),
		e);
	e = singleton(pair(pats,pair(mkInt(line),e)));
	return mkBind("inRange",e);
}

/* --------------------------------------------------------------------------
* Deriving Show:
* ------------------------------------------------------------------------*/

/* Construct definition of text conversion */
static List local deriveShow(Tycon t)
{
	List alts = NIL;
	if (isTycon(t)) {			/* deal with type constrs	   */
		List cs = tycon(t).defn;
		for (; nonNull(cs); cs=tl(cs))
			alts = cons(mkAltShow(tycon(t).line,hd(cs),name(hd(cs)).arity),
			alts);
		alts = rev(alts);
	}
	else				/* special case for tuples	   */
		alts = singleton(mkAltShow(0,t,tupleOf(t)));

	return singleton(mkBind("showsPrec",alts));
}

/* make alt for showsPrec eqn	   */
static Cell local mkAltShow(Int  line, Cell h, Int  a)
{
	List vs   = getDiVars(a+1);
	Cell d    = hd(vs);
	Cell pat  = h;
	List pats = NIL;
	while (vs=tl(vs), 0<a--)
		pat = ap(pat,hd(vs));
	pats = cons(d,cons(pat,NIL));
	return pair(pats,pair(mkInt(line),showsPrecRhs(d,pat)));
}

#define shows0   ap(nameShowsPrec,mkInt(0))
#define shows10  ap(nameShowsPrec,mkInt(10))
#define showsOP  ap(nameComp,consChar('('))
#define showsOB  ap(nameComp,consChar('{'))
#define showsCM  ap(nameComp,consChar(','))
#define showsSP  ap(nameComp,consChar(' '))
#define showsBQ  ap(nameComp,consChar('`'))
#define showsCP  consChar(')')
#define showsCB  consChar('}')

/* build a rhs for showsPrec for a */
/* given pattern, pat		   */
static Cell local showsPrecRhs(Cell d, Cell pat)
{
	Cell h   = getHead(pat);

	if (isTuple(h)) {
		/* To display a tuple:
		*    showsPrec d (a,b,c,d) = showChar '(' . showsPrec 0 a .
		*			      showChar ',' . showsPrec 0 b .
		*			      showChar ',' . showsPrec 0 c .
		*			      showChar ',' . showsPrec 0 d .
		*			      showChar ')'
		*/
		Int  i   = tupleOf(h);
		Cell rhs = showsCP;
		for (; i>1; --i) {
			rhs = ap(showsCM,ap2(nameComp,ap(shows0,arg(pat)),rhs));
			pat = fun(pat);
		}
		return ap(showsOP,ap2(nameComp,ap(shows0,arg(pat)),rhs));
	}

	if (name(h).arity==0)
		/* To display a nullary constructor:
		*    showsPrec d Foo = showString "Foo"
		*/
		return ap(nameApp,mkStr(name(h).text));
	else {
		Syntax s = syntaxOf(name(h).text);
		if (name(h).arity==2 && assocOf(s)!=APPLIC) {
			/* For a binary constructor with prec p:
			* showsPrec d (a :* b) = showParen (d > p)
			*				(showsPrec lp a . showChar ' ' .
			*				 showsString s  . showChar ' ' .
			*				 showsPrec rp b)
			*/
			Int  p   = precOf(s);
			Int  lp  = (assocOf(s)==LEFT_ASS)  ? p : (p+1);
			Int  rp  = (assocOf(s)==RIGHT_ASS) ? p : (p+1);
			Cell rhs = ap(showsSP,ap2(nameShowsPrec,mkInt(rp),arg(pat)));
			if (defaultSyntax(name(h).text)==APPLIC)
				rhs = ap(showsBQ,
				ap2(nameComp,
				ap(nameApp,mkStr(name(h).text)),
				ap(showsBQ,rhs)));
			else
				rhs = ap2(nameComp,ap(nameApp,mkStr(name(h).text)),rhs);

			rhs = ap2(nameComp,
				ap2(nameShowsPrec,mkInt(lp),arg(fun(pat))),
				ap(showsSP,rhs));
			rhs = ap2(nameShowParen,ap2(nameLe,mkInt(p+1),d),rhs);
			return rhs;
		}
		else {
			/* To display a non-nullary constructor with applicative syntax:
			*    showsPrec d (Foo x y) = showParen (d>=10)
			*				   (showString "Foo" .
			*				    showChar ' ' . showsPrec 10 x .
			*				    showChar ' ' . showsPrec 10 y)
			*/
			Cell rhs = ap(showsSP,ap(shows10,arg(pat)));
			for (pat=fun(pat); isAp(pat); pat=fun(pat))
				rhs = ap(showsSP,ap2(nameComp,ap(shows10,arg(pat)),rhs));
			rhs = ap2(nameComp,ap(nameApp,mkStr(name(h).text)),rhs);
			rhs = ap2(nameShowParen,ap2(nameLe,mkInt(10),d),rhs);
			return rhs;
		}
	}
}
#undef  shows10
#undef  shows0
#undef  showsOP
#undef  showsOB
#undef  showsCM
#undef  showsSP
#undef  showsBQ
#undef  showsCP
#undef  showsCB

/* --------------------------------------------------------------------------
* Deriving Read:
* ------------------------------------------------------------------------*/

/* construct definition of text reader	   */
static List local deriveRead(Tycon t)
{
	return NIL;			/* NOT YET IMPLEMENTED			   */
}

/* --------------------------------------------------------------------------
* Deriving Bounded:
* ------------------------------------------------------------------------*/

/* construct definition of bounds	   */
static List local deriveBounded(Tycon t)
{
	if (isEnumType(t)) {
		Cell last  = tycon(t).defn;
		Cell first = hd(last);
		while (nonNull(tl(last)))
			last = tl(last);
		return cons(mkBind("minBound",mkVarAlts(tycon(t).line,first)),
			cons(mkBind("maxBound",mkVarAlts(tycon(t).line,hd(last))),
			NIL));
	}
	else if (isTuple(t))	/* Definitions for product types	   */
		return mkBndBinds(0,t,tupleOf(t));
	else if (isTycon(t) && cfunOf(hd(tycon(t).defn))==0)
		return mkBndBinds(tycon(t).line,
		hd(tycon(t).defn),
		name(hd(tycon(t).defn)).arity);

	ERRMSG(tycon(t).line)
		"Can only derive instances of Bounded for enumeration and product types"
		EEND;
	return NIL;
}

/* build bindings for derived	   */
/* Bounded on a product type	   */
static List local mkBndBinds(Int  line, Cell h, Int  n)
{
	Cell minB = h;
	Cell maxB = h;
	while (n-- > 0) {
		minB = ap(minB,nameMinBnd);
		maxB = ap(maxB,nameMaxBnd);
	}
	return cons(mkBind("minBound",mkVarAlts(line,minB)),
		cons(mkBind("maxBound",mkVarAlts(line,maxB)),
		NIL));
}

/* --------------------------------------------------------------------------
* Primitive definitions are usually only included in the first script
* file read - the prelude.  A primitive definition associates a variable
* name with a string (which identifies a built-in primitive) and a type.
* ------------------------------------------------------------------------*/

/* Handle primitive definitions	   */
Void primDefn(Cell line, List prims, Cell type)
{
	primDefns = cons(triple(line,prims,type),primDefns);
}

/* Check primitive definition	   */
static Void local checkPrimDefn(Triple p)
{
	Int  line  = intOf(fst3(p));
	List prims = snd3(p);
	Type type  = thd3(p);
	type = checkSigType(line,"primitive definition",fst(hd(prims)),type);
	for (; nonNull(prims); prims=tl(prims)) {
		Cell   p    = hd(prims);
		Bool   same = isVar(p);
		Text   pt   = textOf(same ? p : fst(p));
		String pr   = textToStr(textOf(same ? p : snd(p)));
		addNewPrim(line,pt,pr,type);
	}
}

/* make binding of variable vn to  */
/* primitive function referred	   */
/* to by s, with given type t	   */
static Void local addNewPrim(Int    l, Text   vn, String s, Cell   t)
{
	Name n = findName(vn);

	if (isNull(n))
		n = newName(vn);
	else if (name(n).defn!=PREDEFINED) {
		ERRMSG(l) "Redeclaration of primitive \"%s\"", textToStr(vn)
			EEND;
	}

	addPrim(l,n,s,t);
}

/* --------------------------------------------------------------------------
* Default definitions; only one default definition is permitted in a
* given script file.  If no default is supplied, then a standard system
* default will be used where necessary.
* ------------------------------------------------------------------------*/

/* Handle default types definition */
Void defaultDefn(Int  line, List defs)
{
	if (defaultLine!=0) {
		ERRMSG(line) "Multiple default declarations are not permitted in" ETHEN
			ERRTEXT     "a single script file.\n"
			EEND;
	}
	defaultDefns = defs;
	defaultLine  = line;
}

/* check that default types are	   */
/* well-kinded instances of Num	   */
static Void local checkDefaultDefns() {
	List  ds = NIL;

	if (defaultLine!=0) {
		map2Over(depTypeExp,defaultLine,NIL,defaultDefns);
		kindDefaults(defaultLine,defaultDefns);
		mapOver(fullExpand,defaultDefns);
	}
	else
		defaultDefns = stdDefaults;

	if (isNull(classNum))
		classNum = findClass(findText("Num"));

	for (ds=defaultDefns; nonNull(ds); ds=tl(ds))
		if (!mtInst(classNum,hd(ds))) {
			ERRMSG(defaultLine)
				"Default types must be instances of the Num class"
				EEND;
		}
}

/* --------------------------------------------------------------------------
* Static analysis of patterns:
*
* Patterns are parsed as ordinary (atomic) expressions.  Static analysis
* makes the following checks:
*  - Patterns are well formed (according to pattern syntax), including the
*    special case of (n+k) patterns.
*  - All constructor functions have been defined and are used with the
*    correct number of arguments.
*  - No variable name is used more than once in a pattern.
*
* The list of pattern variables occuring in each pattern is accumulated in
* a global list `patVars', which must be initialised to NIL at appropriate
* points before using these routines to check for valid patterns.  This
* mechanism enables the pattern checking routine to be mapped over a list
* of patterns, ensuring that no variable occurs more than once in the
* complete pattern list (as is required on the lhs of a function defn).
* ------------------------------------------------------------------------*/

static List patVars;		       /* list of vars bound in pattern    */

#if OBJ
static Bool statePat;
static Bool stateInScope;
static Bool withinTemplate;
static List stateVars;
static Bool checkingStruct;
#endif

/* Check valid pattern syntax	   */
static Cell local checkPat(Int  line, Cell p)
{
	switch (whatIs(p)) {
	case VARIDCELL :
	case VAROPCELL : 
#if OBJ
		if (statePat) {
			if (!varIsMember(textOf(p),stateVars)) {
				ERRMSG(line) "Undefined state variable \"%s\"",
					textToStr(textOf(p))
					EEND;
			}
		}
		else
#endif
			addPatVar(line,p);
		break;

	case AP        : return checkMaybeCnkPat(line,p);

	case NAME      :
	case CONIDCELL :
	case CONOPCELL : return checkApPat(line,0,p);

#if BIGNUMS
	case ZERONUM   :
	case POSNUM    :
	case NEGNUM    :
#endif
	case WILDCARD  :
	case STRCELL   :
	case CHARCELL  :
	case INTCELL   : break;

	case ASPAT     : addPatVar(line,fst(snd(p)));
		snd(snd(p)) = checkPat(line,snd(snd(p)));
		break;

	case LAZYPAT   : snd(p) = checkPat(line,snd(p));
		break;

	case FINLIST   : map1Over(checkPat,line,snd(p));
		break;

	default        : ERRMSG(line) "Illegal pattern syntax"
						 EEND;
	}
	return p;
}

/* Check applicative pattern with   */
/* the possibility of n+k pattern   */
static Cell local checkMaybeCnkPat(Int  l, Cell p)
{
#if NPLUSK
	Cell h = getHead(p);

	if (argCount==2 && isVar(h) && textOf(h)==textPlus) {	/* n+k	   */
		Cell v = arg(fun(p));
		if (!isInt(arg(p))) {
			ERRMSG(l) "Second argument in (n+k) pattern must be an integer"
				EEND;
		}
		if (intOf(arg(p))<=0) {
			ERRMSG(l) "Integer k in (n+k) pattern must be > 0"
				EEND;
		}
		fst(fun(p))	 = ADDPAT;
		intValOf(fun(p)) = intOf(arg(p));
		arg(p)		 = checkPat(l,v);
		return p;
	}
#endif
	return checkApPat(l,0,p);
}

/* check validity of application    */
/* of constructor to arguments	   */
static Cell local checkApPat(Int  line, Int  args, Cell p)
{
	switch (whatIs(p)) {
	case AP        : fun(p) = checkApPat(line,args+1,fun(p));
		arg(p) = checkPat(line,arg(p));
		break;

	case TUPLE     : if (tupleOf(p)!=args) {
		ERRMSG(line) "Illegal tuple pattern"
			EEND;
					 }
					 break;

	case CONIDCELL :
	case CONOPCELL : p = conDefined(line,textOf(p));
		checkCfunArgs(line,p,args);
		break;

	case NAME      : checkIsCfun(line,p);
		checkCfunArgs(line,p,args);
		break;

	default        : ERRMSG(line) "Illegal pattern syntax"
						 EEND;
	}
	return p;
}

/* add variable v to list of vars   */
/* in current pattern, checking for */
/* repeated variables.		   */
static Void local addPatVar(Int  line, Cell v)
{
	Text t = textOf(v);
	List p = NIL;
	List n = patVars;
	String var = textToStr(t);

#if OBJ
	checkNoHiding(line,t);
#endif
	for (; nonNull(n); p=n, n=tl(n))
		if (textOf(hd(n))==t) {
			ERRMSG(line) "Repeated variable \"%s\" in pattern",
				textToStr(t)
				EEND;
		}

		if (isNull(p))
			patVars = cons(v,NIL);
		else
			tl(p)	 = cons(v,NIL);
}

#if OBJ
static Cell local checkAssignPat(Int  l, Cell p)
{
	Cell pat   = p;
	Cell h     = getHead(pat);
	Int  bangs = 0;

	while (argCount==2 && isVar(h) && textOf(h)==textBang) {
		arg(pat) = depExpr(l,arg(pat));
		pat      = arg(fun(pat));
		h        = getHead(pat);
		bangs++;
	}

	statePat = TRUE;
	pat      = checkPat(l,pat);
	statePat = FALSE;

	if (bangs) {
		Name n = findName(textBang);
		Name u = findName(findText("//"));
		if (isNull(n) || isNull(u)) {
			ERRMSG(l) "Array update syntax requires operators \"!\" and \"//\" in scope"
				EEND;
		}
		if (!isVar(pat)) {
			ERRMSG(l) "Illegal array update syntax"
				EEND;
		}
	}
	return p;
}

static Void local checkNoHiding(Int  line, Text t)
{
	if (checkingStruct)
		return;
	if (varIsMember(t,stateVars)) {
		ERRMSG(line) "Illegal hiding of state variable \"%s\"",
			textToStr(t)
			EEND;
	}
	if (t==textSelf) {
		ERRMSG(line) "Illegal use of variable name \"self\""
			EEND;
	}
}
#endif

/* check that t is the name of a    */
/* previously defined selector      */
/* function			   */
static Name local selDefined(Int line, Text t)
{
	Name n = findName(mkStructSel(t));
	if (isNull(n)) {
		ERRMSG(line) "Undefined selector function \"%s\"", textToStr(t)
			EEND;
	}
	return n;
}

/* check that t is the name of a    */
/* previously defined constructor   */
/* function.			   */
static Name local conDefined(Int line, Text t)
{
	Cell c=findName(t);
	if (isNull(c)) {
		ERRMSG(line) "Undefined constructor function \"%s\"", textToStr(t)
			EEND;
	}
	checkIsCfun(line,c);
	return c;
}

/* Check that c is a constructor fn */
static Void local checkIsCfun(Int  line, Name c)
{
	if (!isCfun(c)) {
		ERRMSG(line) "\"%s\" is not a constructor function",
			textToStr(name(c).text)
			EEND;
	}
}

/* Check constructor applied with   */
/* correct number of arguments	   */
static Void local checkCfunArgs(Int  line, Cell c, Int  args)
{
	if (name(c).arity!=args) {
		ERRMSG(line) "Constructor function \"%s\" needs %d args in pattern",
			textToStr(name(c).text), name(c).arity
			EEND;
	}
}

/* --------------------------------------------------------------------------
* Maintaining lists of bound variables and local definitions, for
* dependency and scope analysis.
* ------------------------------------------------------------------------*/

static List bounds;		       /* list of lists of bound vars	   */
static List bindings;		       /* list of lists of binds in scope  */
static List depends;		       /* list of lists of dependents	   */

#define saveBvars()	 hd(bounds)    /* list of bvars in current scope   */
#define restoreBvars(bs) hd(bounds)=bs /* restore list of bound variables  */

/* add new bound vars for pattern   */
static Cell local bindPat(Int  line, Cell p)
{
	patVars    = NIL;
	p	       = checkPat(line,p);
	hd(bounds) = revOnto(patVars,hd(bounds));
	return p;
}

/* add new bound vars for patterns  */
static Void local bindPats(Int  line, List ps)
{
	patVars    = NIL;
	map1Over(checkPat,line,ps);
	hd(bounds) = revOnto(patVars,hd(bounds));
}

/* add new bound vars for patterns  */
/* in list of generators            */
static List local bindGens(Int  line, List gs)
{
	patVars    = NIL;
	for (; nonNull(gs); gs = tl(gs))
		checkPat(line,fst(hd(gs)));
	hd(bounds) = revOnto(dupList(patVars),hd(bounds));
	return patVars;
}

/* --------------------------------------------------------------------------
* Before processing value and type signature declarations, all data and
* type definitions have been processed so that:
* - all valid type constructors (with their arities) are known.
* - all valid constructor functions (with their arities and types) are
*   known.
*
* The result of parsing a list of value declarations is a list of Eqns:
*	 Eqn ::= (SIGDECL,(Line,[Var],type))  |  (Expr,Rhs)
* The ordering of the equations in this list is the reverse of the original
* ordering in the script parsed.  This is a consequence of the structure of
* the parser ... but also turns out to be most convenient for the static
* analysis.
*
* As the first stage of the static analysis of value declarations, each
* list of Eqns is converted to a list of Bindings.  As part of this
* process:
* - The ordering of the list of Bindings produced is the same as in the
*   original script.
* - When a variable (function) is defined over a number of lines, all
*   of the definitions should appear together and each should give the
*   same arity to the variable being defined.
* - No variable can have more than one definition.
* - For pattern bindings:
*   - Each lhs is a valid pattern/function lhs, all constructor functions
*     have been defined and are used with the correct number of arguments.
*   - Each lhs contains no repeated pattern variables.
*   - Each equation defines at least one variable (e.g. True = False is
*     not allowed).
* - Types appearing in type signatures are well formed:
*    - Type constructors used are defined and used with correct number
*	of arguments.
*    - type variables are replaced by offsets, type constructor names
*	by Tycons.
* - Every variable named in a type signature declaration is defined by
*   one or more equations elsewhere in the script.
* - No variable has more than one type declaration.
*
* ------------------------------------------------------------------------*/

#define bindingType(b) fst(snd(b))     /* type (or types) for binding	   */
#define fbindAlts(b)   snd(snd(b))     /* alternatives for function binding*/

/* extract the generators from list */
/* of "equations" (template only)   */
static List local extractGens(List es)
{
	List gens  = NIL;	 	       /* :: [(Pat,Exp)]	           */

	for(; nonNull(es); es=tl(es))
		if (fst(hd(es))==FROMQUAL)		     /* generator?         */
			gens = cons(snd(hd(es)),gens);           /* discard tag        */

	return gens;
}

/* extract the SIGDECLS from list   */
/* of equations			   */
static List local extractSigdecls(List es)
{
	List sigDecls  = NIL;	       /* :: [(Line,[Var],Type)]	   */

	for(; nonNull(es); es=tl(es))
		if (fst(hd(es))==SIGDECL)		     /* type-declaration?  */
			sigDecls = cons(snd(hd(es)),sigDecls);   /* discard tag        */

	return sigDecls;
}

/* extract untyped bindings from    */
/* given list of equations	   */
static List local extractBindings(List es)
{
	Cell lastVar   = NIL;	       /* = var def'd in last eqn (if any) */
	Int  lastArity = 0; 	       /* = number of args in last defn    */
	List bs	   = NIL;	       /* :: [Binding]			   */

	for(; nonNull(es); es=tl(es)) {
		Cell e = hd(es);

		if (fst(e)==FROMQUAL) {			     /* generator?         */
			/* skip */				     /* (in template only) */
		} else if (fst(e)!=SIGDECL) {
			Int  line	 = rhsLine(snd(e));
			Cell lhsHead = getHead(fst(e));

			switch (whatIs(lhsHead)) {
			case VARIDCELL :
			case VAROPCELL : {		      /* function-binding? */
				Cell newAlt = pair(getArgs(fst(e)), snd(e));
				if (nonNull(lastVar) && textOf(lhsHead)==textOf(lastVar)) {
					if (argCount!=lastArity) {
						ERRMSG(line)
							"Equations give different arities for \"%s\"",
							textToStr(textOf(lhsHead))
							EEND;
					}
					fbindAlts(hd(bs)) = cons(newAlt,fbindAlts(hd(bs)));
				}
				else {
					lastVar   = lhsHead;
					lastArity = argCount;
					notDefined(line,bs,lhsHead);
#if OBJ
					checkNoHiding(line,textOf(lhsHead));
#endif
					bs	  = cons(pair(lhsHead,
						pair(NIL,
						singleton(newAlt))),
						bs);
				}
							 }
							 break;

			case CONOPCELL :
			case CONIDCELL :
			case FINLIST   :
			case TUPLE     :
			case NAME      :
			case ASPAT     : lastVar = NIL;       /* pattern-binding?  */
				patVars = NIL;
				fst(e)  = checkPat(line,fst(e));
				if (isNull(patVars)) {
					ERRMSG(line)
						"No variables defined in lhs pattern"
						EEND;
				}
				map2Proc(notDefined,line,bs,patVars);
				bs = cons(pair(patVars,pair(NIL,e)),bs);
				break;

			default        : ERRMSG(line) "Improper left hand side"
								 EEND;
			}
		}
	}
	return bs;
}

/* Convert list of equations to list*/
/* of typed bindings		   */
static List local eqnsToBindings(List es)
{
	List bs = extractBindings(es);
	map1Proc(addSigDecl,bs,extractSigdecls(es));
	return bs;
}

/* check if name already defined in */
/* list of bindings		   */
static Void local notDefined(Int  line, List bs, Cell v)
{
	if (nonNull(findBinding(textOf(v),bs))) {
		ERRMSG(line) "\"%s\" multiply defined", textToStr(textOf(v))
			EEND;
	}
}

/* look for binding for variable t  */
/* in list of bindings bs	   */
static Cell local findBinding(Text t, List bs)
{
	for (; nonNull(bs); bs=tl(bs))
		if (isVar(fst(hd(bs)))) {		      /* function-binding? */
			if (textOf(fst(hd(bs)))==t)
				return hd(bs);
		}
		else if (nonNull(varIsMember(t,fst(hd(bs))))) /* pattern-binding?  */
			return hd(bs);
	return NIL;
}

/* add type information to bindings*/
static Void local addSigDecl(List bs,		/* :: [Binding]			   */
							 Cell sigDecl 	/* :: (Line,[Var],Type)		   */
							 )
{
	Int  line = intOf(fst3(sigDecl));
	Cell vs   = snd3(sigDecl);
	Cell type = checkSigType(line,"type declaration",hd(vs),thd3(sigDecl));

	map3Proc(setType,line,type,bs,vs);
}

/* Set type of variable		   */
static Void local setType(Int  line, Cell type, List bs, Cell v)
{
	Text t = textOf(v);
	Cell b = findBinding(t,bs);

	if (isNull(b)) {
		ERRMSG(line) "Type declaration for variable \"%s\" with no body",
			textToStr(t)
			EEND;
	}

	if (isVar(fst(b))) {			      /* function-binding? */
		if (isNull(bindingType(b))) {
			bindingType(b) = type;
			return;
		}
	}
	else {					      /* pattern-binding?  */
		List vs = fst(b);
		List ts = bindingType(b);

		if (isNull(ts))
			bindingType(b) = ts = copy(length(vs),NIL);

		while (nonNull(vs) && t!=textOf(hd(vs))) {
			vs = tl(vs);
			ts = tl(ts);
		}

		if (nonNull(vs) && isNull(hd(ts))) {
			hd(ts) = type;
			return;
		}
	}

	ERRMSG(line) "Repeated type declaration for \"%s\"", textToStr(t)
		EEND;
}

/* --------------------------------------------------------------------------
* To facilitate dependency analysis, lists of bindings are temporarily
* augmented with an additional field, which is used in two ways:
* - to build the `adjacency lists' for the dependency graph. Represented by
*   a list of pointers to other bindings in the same list of bindings.
* - to hold strictly positive integer values (depth first search numbers) of
*   elements `on the stack' during the strongly connected components search
*   algorithm, or a special value mkInt(0), once the binding has been added
*   to a particular strongly connected component.
*
* Using this extra field, the type of each list of declarations during
* dependency analysis is [Binding'] where:
*
*    Binding' ::= (Var, (Dep, (Type, [Alt])))	      -- function binding
*		|  ([Var], (Dep, (Type, (Pat,Rhs))))  -- pattern binding
*
* ------------------------------------------------------------------------*/

#define depVal(d) (fst(snd(d)))        /* Access to dependency information */

/* Separate lists of bindings into  */
/* mutually recursive groups in	   */
/* order of dependency		   */
static List local dependencyAnal(List bs)
{
	mapProc(addDepField,bs);	       /* add extra field for dependents   */
	mapProc(depBinding,bs);	       /* find dependents of each binding  */
	bs = bscc(bs);		       /* sort to strongly connected comps */
	mapProc(remDepField,bs);	       /* remove dependency info field	   */
	return bs;
}

/* Like dependencyAnal(), but at    */
/* top level, reporting on progress */
static List local topDependAnal(List bs)
{
	List xs;
	Int  i = 0;

	setGoal("Dependency analysis",(Target)(length(bs)));
	mapProc(addDepField,bs);	       /* add extra field for dependents   */
	for (xs=bs; nonNull(xs); xs=tl(xs)) {
		depBinding(hd(xs));
		soFar((Target)(i++));
	}
	bs = bscc(bs);		       /* sort to strongly connected comps */
	mapProc(remDepField,bs);	       /* remove dependency info field	   */
	done();
	return bs;
}

/* add extra field to binding to    */
/* hold list of dependents	   */
static Void local addDepField(Cell b)
{
	snd(b) = pair(NIL,snd(b));
}

/* remove dependency field from	   */
/* list of bindings		   */
static Void local remDepField(List bs)
{
	mapProc(remDepField1,bs);
}

/* remove dependency field from	   */
/* single binding		   */
static Void local remDepField1(Cell b)
{
	snd(b) = snd(snd(b));
}

static Void local clearScope() {       /* initialise dependency scoping    */
	bounds   = NIL;
	bindings = NIL;
	depends  = NIL;
}

/* enter scope of bindings bs	   */
static Void local withinScope(List bs)
{
	bounds   = cons(NIL,bounds);
	bindings = cons(bs,bindings);
	depends  = cons(NIL,depends);
}

static Void local leaveScope() {       /* leave scope of last withinScope  */
	bounds   = tl(bounds);
	bindings = tl(bindings);
	depends  = tl(depends);
}

/* --------------------------------------------------------------------------
* As a side effect of the dependency analysis we also make the following
* checks:
* - Each lhs is a valid pattern/function lhs, all constructor functions
*   have been defined and are used with the correct number of arguments.
* - No lhs contains repeated pattern variables.
* - Expressions used on the rhs of an eqn should be well formed.  This
*   includes:
*   - Checking for valid patterns (including repeated vars) in lambda,
*     case, and list comprehension expressions.
*   - Recursively checking local lists of equations.
* - No free (i.e. unbound) variables are used in the declaration list.
* ------------------------------------------------------------------------*/

static Void local depBinding(Cell b)  /* find dependents of binding	   */
{
	Cell defpart = snd(snd(snd(b)));   /* definition part of binding	   */

	hd(depends) = NIL;

	if (isVar(fst(b))) {	       /* function-binding?		   */
		mapProc(depAlt,defpart);
	}
	else {			       /* pattern-binding?		   */
		depRhs(snd(defpart));
	}

	depVal(b) = hd(depends);
}

static Void local depSelBind(Cell b)	 /* find dependants of selector bind */
{
	Cell alts = snd(snd(b));
	if (!isVar(fst(b))) {              /* only allow function bindings     */
		ERRMSG(rhsLine(snd(hd(alts))))
			"Pattern binding not permitted in struct expression"
			EEND;
	}
	fst(b) = selDefined(rhsLine(snd(hd(alts))),textOf(fst(b)));
	mapProc(depAlt,snd(snd(b)));
}

static Cell local depStruct(Int l, Cell e)
{
	Int line = intOf(fst(snd(e)));
	Cell padding = hd(snd(snd(e)));

	if (isCon(padding))
		snd(snd(e)) = tl(snd(snd(e)));
	else
		padding = NIL;

	if (nonNull(extractSigdecls(snd(snd(e))))) {
		ERRMSG(line) 
			"Type signatures not permitted in struct value"
			EEND;
	}

#if OBJ
	checkingStruct = TRUE;
#endif
	snd(snd(e)) = extractBindings(snd(snd(e)));
#if OBJ
	checkingStruct = FALSE;
#endif
	mapProc(depSelBind,snd(snd(e)));

	if (padding) {
		Tycon t = findTycon(textOf(padding));
		List missing = NIL;
		List ts;

		if (isNull(t) || tycon(t).what != STRUCTTYPE) {
			ERRMSG(line)
				"Struct padding \"%s\" is not a struct type", 
				textToStr(textOf(padding))
				EEND;
		}

		for (ts=cons(t,tycon(t).axioms); nonNull(ts); ts=tl(ts)) {
			Tycon h = getHead(monoType(hd(ts)));
			List ss = tycon(h).defn;

			for (; nonNull(ss); ss=tl(ss)) {
				List eqns = snd(snd(e));
				for (; nonNull(eqns); eqns=tl(eqns)) 
					if (fst(hd(eqns)) == hd(ss))
						break;
				if (isNull(eqns)) {
					Cell selvar = depVar(line,mkVar(unStructSel(hd(ss))));

					missing = 
						cons(pair(hd(ss),
						pair(NIL,
						singleton(pair(NIL,
						pair(mkInt(line),
						selvar))))),
						missing);
				}
			}
		}
		snd(snd(e)) = appendOnto(snd(snd(e)),missing);

		if (isNull(snd(snd(e)))) {
			Type sig = t;
			Int n = tycon(t).arity;

			for (; n>0; n--)
				sig = ap(sig,ap(WILDTYPE,NIL));

			return ap(ESIGN,pair(e,checkSigType(l,"struct padding",NIL,sig)));
		}
	}
	return e;
}

/* dependency analysis on defaults  */
/* from class definition            */
static Void local depDefaults(Class c)
{
	depClassBindings(cclass(c).defaults);
}

/* dependency analysis on instance  */
/* bindings                         */
static Void local depInsts(Inst in)
{
	depClassBindings(inst(in).implements);
}

/* dependency analysis on list of   */
/* bindings, possibly containing    */
/* NIL bindings ...                 */
static Void local depClassBindings(List bs)
{
	for (; nonNull(bs); bs=tl(bs))
		/* No need to add extra field for   */
			/* dependency information.. */
				if (nonNull(hd(bs)))
					mapProc(depAlt,name(hd(bs)).defn);
}

/* find dependents of alternative   */
static Void local depAlt(Cell a)
{
	List origBvars = saveBvars();      /* save list of bound variables	   */
	bindPats(rhsLine(snd(a)),fst(a));  /* add new bound vars for patterns  */
	depRhs(snd(a));		       /* find dependents of rhs	   */
	restoreBvars(origBvars);	       /* restore original list of bvars   */
}

static Void local depRhs(Cell r)	       /* find dependents of rhs	   */
{
	switch (whatIs(r)) {
	case GUARDED : mapProc(depGuard,snd(r));
		break;

	case LETREC  : fst(snd(r)) = eqnsToBindings(fst(snd(r)));
		withinScope(fst(snd(r)));
		fst(snd(r)) = dependencyAnal(fst(snd(r)));
		hd(depends) = fst(snd(r));
		depRhs(snd(snd(r)));
		leaveScope();
		break;

	default      : snd(r) = depExpr(intOf(fst(r)),snd(r));
		break;
	}
}

/* find dependents of single guarded*/
/* expression			   */
static Void local depGuard(Cell g)
{
	depPair(intOf(fst(g)),snd(g));
}

/* find dependents of expression    */
static Cell local depExpr(Int  line, Cell e)
{
	switch (whatIs(e)) {

	case VARIDCELL	:
	case VAROPCELL	: return depVar(line,e);

	case CONIDCELL	:
	case CONOPCELL	: return conDefined(line,textOf(e));

	case AP 	: depPair(line,e);
		break;

#if BIGNUMS
	case ZERONUM	:
	case POSNUM	:
	case NEGNUM	:
#endif
	case NAME	:
	case TUPLE	:
	case STRCELL	:
	case CHARCELL	:
	case FLOATCELL  :
	case INTCELL	: break;

	case COND	: depTriple(line,snd(e));
		break;

	case FINLIST	: map1Over(depExpr,line,snd(e));
		break;

	case LETREC	: fst(snd(e)) = eqnsToBindings(fst(snd(e)));
		withinScope(fst(snd(e)));
		fst(snd(e)) = dependencyAnal(fst(snd(e)));
		hd(depends) = fst(snd(e));
		snd(snd(e)) = depExpr(line,snd(snd(e)));
		leaveScope();
		break;

	case LAMBDA	: depAlt(snd(e));
		break;

#if OBJ
	case HNDLEXP    : map1Proc(depCaseAlt,line,fst(snd(e)));
		snd(snd(e)) = depExpr(line,snd(snd(e)));
		break;

	case TEMPLEXP   : depTempl(line,e);
		break;	
	case ACTEXP     : 
	case REQEXP     : if (!withinTemplate) {
		ERRMSG(line) 
			"%s syntax not allowed outside a template",
			fst(e)==ACTEXP ? "action" : "request"
			EEND;
					  }
					  snd(e) = depExpr(line,snd(e));
					  break;
#endif
	case DOCOMP	: depDoComp(line,snd(e),snd(snd(e)));
		break;

	case COMP	: depComp(line,snd(e),snd(snd(e)));
		break;

#if LAZY_ST
	case RUNST	: snd(e) = depExpr(line,snd(e));
		break;
#endif

	case ESIGN	: fst(snd(e)) = depExpr(line,fst(snd(e)));
		snd(snd(e)) = checkSigType(line,
			"expression",
			fst(snd(e)),
			snd(snd(e)));
		break;

	case CASE	: fst(snd(e)) = depExpr(line,fst(snd(e)));
		map1Proc(depCaseAlt,line,snd(snd(e)));
		break;

	case ASPAT	: ERRMSG(line) "Illegal `@' in expression"
					  EEND;

	case LAZYPAT	: ERRMSG(line) "Illegal `~' in expression"
						  EEND;

	case WILDCARD	: ERRMSG(line) "Illegal `_' in expression"
						  EEND;

	case STRUCTVAL	: e = depStruct(line,e);
		break;

	case SELECTION	: if (isVar(snd(e)))        /* selector section? */
						  return selDefined(line,textOf(snd(e)));
					  else
						  return ap(selDefined(line,textOf(snd(snd(e)))),
						  depExpr(line,fst(snd(e))));

	default 	: printf("(%d)\n",e); internal("in depExpr");
	}
	return e;
}

/* find dependents of pair of exprs*/
static Void local depPair(Int  line, Cell e)
{
	fst(e) = depExpr(line,fst(e));
	snd(e) = depExpr(line,snd(e));
}

/* find dependents of triple exprs */
static Void local depTriple(Int  line, Cell e)
{
	fst3(e) = depExpr(line,fst3(e));
	snd3(e) = depExpr(line,snd3(e));
	thd3(e) = depExpr(line,thd3(e));
}

static Void local depDoComp(Int  l, Cell e, List qs)
{
#if OBJ
	Bool oldStateInScope = stateInScope;
	stateInScope = TRUE;
#endif
	depComp(l,e,qs);
#if OBJ
	stateInScope = oldStateInScope;
#endif
}

/* find dependents of comprehension*/
static Void local depComp(Int  l, Cell e, List qs)
{
	if (isNull(qs))
		fst(e) = depExpr(l,fst(e));
	else {
		Cell q   = hd(qs);
		List qs1 = tl(qs);
		switch (whatIs(q)) {
		case FROMQUAL   : {   List origBvars = saveBvars();
			snd(snd(q))    = depExpr(l,snd(snd(q)));
			fst(snd(q))    = bindPat(l,fst(snd(q)));
			depComp(l,e,qs1);
			restoreBvars(origBvars);
						  }
						  break;

		case QWHERE     : snd(q)      = eqnsToBindings(snd(q));
			withinScope(snd(q));
			snd(q)      = dependencyAnal(snd(q));
			hd(depends) = snd(q);
			depComp(l,e,qs1);
			leaveScope();
			break;

		case DOQUAL	    : /* fall-thru */
		case BOOLQUAL   : snd(q) = depExpr(l,snd(q));
			depComp(l,e,qs1);
			break;
#if OBJ
		case ASSIGNQ    : {   Int l       = intOf(fst(snd(snd(q))));
			fst(snd(q)) = checkAssignPat(l,fst(snd(q)));
			snd(snd(snd(q))) = depExpr(l,snd(snd(snd(q))));
						  }
						  depComp(l,e,qs1);
						  break;

		case WHILEDO    : fst(snd(q)) = depExpr(l,fst(snd(q)));
			snd(snd(q)) = depExpr(l,snd(snd(q)));
			depComp(l,e,qs1);
			break;

		case FORALLDO   : {   List origBvars = saveBvars();
			snd3(snd(q)) = depExpr(l,snd3(snd(q)));
			fst3(snd(q)) = bindPat(l,fst3(snd(q)));
			thd3(snd(q)) = depExpr(l,thd3(snd(q)));
			restoreBvars(origBvars);
			depComp(l,e,qs1);
						  }
						  break;

		case FIXDO      : {   List origBvars = saveBvars();
			List fixvars = bindGens(l,snd(q));
			depGens(l,snd(q));
			depComp(l,e,qs1);
			restoreBvars(origBvars);
			snd(q) = pair(fixvars,snd(q));
						  }
						  break;
#endif
		}
	}
}

static Void local depGens(Int line, List gs)
{
	for (; nonNull(gs); gs = tl(gs))
		snd(hd(gs)) = depExpr(line,snd(hd(gs)));
}

/* find dependents of case altern. */
static Void local depCaseAlt(Int  line, Cell a)
{
	List origBvars = saveBvars();	/* save list of bound variables	   */
	fst(a) = bindPat(line,fst(a));	/* add new bound vars for patterns */
	depRhs(snd(a));			/* find dependents of rhs	   */
	restoreBvars(origBvars);		/* restore original list of bvars  */
}

/* register occurrence of variable */
static Cell local depVar(Int line, Cell e)
{
	List bounds1   = bounds;
	List bindings1 = bindings;
	List depends1  = depends;
	Text t	   = textOf(e);
	Cell n;

#if OBJ
	if (stateInScope && (n=varIsMember(t,stateVars)))
		return n;
#endif

	while (nonNull(bindings1)) {
		n = varIsMember(t,hd(bounds1));   /* look for t in bound variables */
		if (nonNull(n))
			return n;

		n = findBinding(t,hd(bindings1)); /* look for t in var bindings    */
		if (nonNull(n)) {
			if (!cellIsMember(n,hd(depends1)))
				hd(depends1) = cons(n,hd(depends1));
			return (isVar(fst(n)) ? fst(n) : e);
		}

		bounds1   = tl(bounds1);
		bindings1 = tl(bindings1);
		depends1  = tl(depends1);
	}

	if (isNull(n=findName(t))) {	       /* check global definitions */
		ERRMSG(line) "Undefined variable \"%s\"", textToStr(t)
			EEND;
	}

	return n;
}

#if OBJ
static Void local depTempl(Int  line, Cell e)
{
	List gs = snd(fst(snd(e))) = extractGens(fst(fst(snd(e))));
	List bs = fst(fst(snd(e))) = eqnsToBindings(fst(fst(snd(e))));
	List oldBvars          = saveBvars();
	List oldStateVars      = stateVars;
	Bool oldStateInScope   = stateInScope;
	Bool oldWithinTemplate = withinTemplate;
	stateVars      = NIL;
	stateInScope   = FALSE;
	withinTemplate = TRUE;

	for (; nonNull(bs); bs=tl(bs)) {
		Cell b = hd(bs);
		if (isVar(fst(b))) {	/* function binding? turn into pattern binding */
			List alts = snd(snd(b));
			Int  l    = rhsLine(snd(hd(alts)));
			if (nonNull(tl(alts))) {
				ERRMSG(l)
					"Repeated definition of state variable \"%s\"",
					textToStr(textOf(fst(hd(bs))))
					EEND;
			}
			if (nonNull(fst(hd(alts)))) {
				ERRMSG(l)
					"Function binding not allowed in state declaration"
					EEND;
			}
			stateVars   = cons(fst(b),stateVars);
			snd(snd(b)) = pair(fst(b),snd(hd(alts)));
			fst(snd(b)) = isNull(fst(snd(b))) ? NIL 
				: singleton(fst(snd(b)));
			fst(b)      = singleton(fst(b));
		}
		else {			/* pattern binding */
			List vs = fst(hd(bs));
			for (; nonNull(vs); vs=tl(vs))
				stateVars = cons(hd(vs),stateVars);
		}
	}
	hd(bounds) = cons(varSelf,hd(bounds));
	snd(fst(snd(e))) = pair(bindGens(line,gs),gs);

	for (bs=fst(fst(snd(e))); nonNull(bs); bs=tl(bs))
		depRhs(snd(snd(snd(hd(bs)))));
	depGens(line,gs);

	fst(snd(snd(e))) = depExpr(line,fst(snd(snd(e))));
	map1Proc(depCaseAlt,line,snd(snd(snd(e))));

	restoreBvars(oldBvars);
	stateVars      = oldStateVars;
	stateInScope   = oldStateInScope;
	withinTemplate = oldWithinTemplate;
}
#endif

/* --------------------------------------------------------------------------
* Several parts of this program require an algorithm for sorting a list
* of values (with some added dependency information) into a list of strongly
* connected components in which each value appears before its dependents.
*
* Each of these algorithms is obtained by parameterising a standard
* algorithm in "scc.c" as shown below.
* ------------------------------------------------------------------------*/

#define visited(d) (isInt(DEPENDS(d)))	/* binding already visited ?	   */

static Cell daSccs = NIL;
static Int  daCount;

/* calculate minimum of x,y (unless */
/* y is zero)			   */
static Int local sccMin(Int x,Int y)
{
	return (x<=y || y==0) ? x : y;
}

#define  SCC2		 tcscc		/* make scc algorithm for Tycons   */
#define  LOWLINK	 tclowlink
#define  DEPENDS(c)      (isTycon(c) ? tycon(c).kind : cclass(c).sig)
#define  SETDEPENDS(c,v) if(isTycon(c)) tycon(c).kind=v; else cclass(c).sig=v
#include "scc.c"
#undef   SETDEPENDS
#undef	 DEPENDS
#undef 	 LOWLINK
#undef	 SCC2

#define  SCC		 bscc		/* make scc algorithm for Bindings */
#define  LOWLINK	 blowlink
#define  DEPENDS(t)	 depVal(t)
#define  SETDEPENDS(c,v) depVal(c)=v
#include "scc.c"
#undef   SETDEPENDS
#undef	 DEPENDS
#undef 	 LOWLINK
#undef	 SCC

/* --------------------------------------------------------------------------
* Main static analysis:
* ------------------------------------------------------------------------*/

Void checkExp() {			/* Top level static check on Expr  */
	staticAnalysis(RESET);
	clearScope();
	/* Analyse expression in the scope */
	/* of no local bindings		   */
	withinScope(NIL);
	inputExpr = depExpr(0,inputExpr);
	leaveScope();
	staticAnalysis(RESET);
}

Void checkDefns() {			/* Top level static analysis	   */
	staticAnalysis(RESET);

	linkPreludeTC();			/* Get prelude tycons and classes  */
	mapProc(checkTyconDefn,tyconDefns);	/* validate tycon definitions	   */
	checkSynonyms(tyconDefns);		/* check synonym definitions	   */
	mapProc(checkClassDefn,classDefns);	/* process class definitions	   */
	mapProc(kindTCGroup,tcscc(tyconDefns,classDefns)); /* attach kinds	   */
	checkSubtypes(tyconDefns);		/* close and check subtyping theory*/
	inferVariances(tyconDefns);		/* infer variance for tycons       */
	deriveEval(tyconDefns);		/* derive instances of Eval	   */
	tyconDefns = NIL;
	mapProc(addMembers,classDefns);	/* add definitions for member funs */
	mapProc(visitClass,classDefns);	/* check class hierarchy	   */
	mapProc(inferClassVariance,classDefns); /* infer variance for class var*/

	mapProc(checkPrimDefn,primDefns);	/* check primitive declarations	   */
	primDefns = NIL;

	instDefns = rev(instDefns);		/* process instance definitions	   */
	mapProc(checkInstDefn,instDefns);

	linkPreludeCM();			/* Get prelude cfuns and mfuns	   */
	deriveContexts(derivedInsts);	/* check derived instances	   */
	instDefns = appendOnto(instDefns,derivedInsts);
	mapProc(checkInstSC,instDefns);
	checkDefaultDefns();		/* validate default definitions	   */

	mapProc(addRSsigdecls,typeInDefns);	/* add sigdecls for RESTRICTSYN	   */
	valDefns = eqnsToBindings(valDefns);/* translate value equations	   */
	map1Proc(opDefined,valDefns,opDefns);/*check all declared ops bound	   */
	mapProc(allNoPrevDef,valDefns);	/* check against previous defns	   */

	mapProc(checkTypeIn,typeInDefns);	/* check restricted synonym defns  */

	clearScope();
	withinScope(valDefns);
	valDefns = topDependAnal(valDefns); /* top level dependency ordering   */
	mapProc(depDefaults,classDefns);    /* dep. analysis on class defaults */
	mapProc(depInsts,instDefns);        /* dep. analysis on inst defns	   */
	leaveScope();

	evalDefaults = defaultDefns;	/* Set defaults for evaluator	   */

	staticAnalysis(RESET);
}

/* add sigdecls from TYPE ... IN ..*/
static Void local addRSsigdecls(Pair pr)
{
	List vs = snd(pr);			/* get list of variables	   */
	for (; nonNull(vs); vs=tl(vs)) {
		if (fst(hd(vs))==SIGDECL) {	/* find a sigdecl		   */
			valDefns = cons(hd(vs),valDefns);	/* add to valDefns	   */
			hd(vs)   = hd(snd3(snd(hd(vs))));	/* and replace with var	   */
		}
	}
}

/* check that op bound in bs	   */
/* (or in current module for	   */
/* constructor functions etc...)  */
static Void local opDefined(List bs, Cell op)
{
	Name n;

	if (isNull(findBinding(textOf(op),bs))
		&& (isNull(n=findName(textOf(op))) || !nameThisModule(n))) {
			ERRMSG(0) "No top level definition for operator symbol \"%s\"",
				textToStr(textOf(op))
				EEND;
	}
}

/* ensure no previous bindings for*/
/* variables in new binding	   */
static Void local allNoPrevDef(Cell b)
{
	if (isVar(fst(b)))
		noPrevDef(rhsLine(snd(hd(snd(snd(b))))),fst(b));
	else {
		Int line = rhsLine(snd(snd(snd(b))));
		map1Proc(noPrevDef,line,fst(b));
	}
}

/* ensure no previous binding for */
/* new variable		   */
static Void local noPrevDef(Int  line, Cell v)
{
	Name n = findName(textOf(v));

	if (isNull(n)) {
		n            = newName(textOf(v));
		name(n).defn = PREDEFINED;
	}
	else if (name(n).defn!=PREDEFINED) {
		ERRMSG(line) "Attempt to redefine variable \"%s\"",
			textToStr(name(n).text)
			EEND;
	}
	name(n).line = line;
}

/* Check that vars in restricted   */
/* synonym are defined, and replace*/
/* vars with names		   */
static Void local checkTypeIn(Pair cvs)
{
	Tycon c  = fst(cvs);
	List  vs = snd(cvs);

	for (; nonNull(vs); vs=tl(vs))
		if (isNull(findName(textOf(hd(vs))))) {
			ERRMSG(tycon(c).line)
				"No top level binding of \"%s\" for restricted synonym \"%s\"",
				textToStr(textOf(hd(vs))), textToStr(tycon(c).text)
				EEND;
		}
}

/* --------------------------------------------------------------------------
* Static Analysis control:
* ------------------------------------------------------------------------*/

Void staticAnalysis(Int what)
{
	switch (what) {
	case INSTALL :
	case RESET   : daSccs	       = NIL;
		patVars	       = NIL;
		bounds	       = NIL;
		bindings	       = NIL;
		depends         = NIL;
		tcDeps	       = NIL;
		derivedInsts    = NIL;
		diVars	       = NIL;
		diNum	       = 0;
		acceptWildcards = FALSE;
#if OBJ
		statePat        = FALSE;
		stateInScope    = FALSE;
		withinTemplate  = FALSE;
		stateVars       = NIL;
		checkingStruct  = FALSE;
#endif
		break;

	case MARK    : mark(daSccs);
		mark(patVars);
		mark(bounds);
		mark(bindings);
		mark(depends);
		mark(tcDeps);
		mark(derivedInsts);
		mark(diVars);
#if OBJ
		mark(stateVars);
#endif
		break;
	}
}

/*-------------------------------------------------------------------------*/
