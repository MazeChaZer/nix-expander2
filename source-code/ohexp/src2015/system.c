#if HASKELL_SYSTEM
/* --------------------------------------------------------------------------
* system.c:   Copyright (c) Jos Kusiek 2013.   All rights reserved.
*              OHugs version 10.5, Jan 2001
*
* Implementation of Haskell System module.
* ------------------------------------------------------------------------*/

/* ----------------------------------------------------------------------
* System dependent macros.
* ----------------------------------------------------------------------*/
#if VC32
#define MKDIR(path) mkdir(path)
#define GET_DIRECTORY_CONTENT(c,e) WIN32_FIND_DATA fd;\
	HANDLE hndl;\
	char dir[_MAX_PATH];\
	\
	strcpy(dir, evalName(primArg(3)));\
	strcat(dir, "\\*.*");\
	hndl = FindFirstFile(dir, &fd);\
	if (hndl == INVALID_HANDLE_VALUE) { /* check error occurs */\
		e = TRUE;\
	} else { /* no errors */\
		List list = ap(ap(nameCons, buildString(fd.cFileName)), NIL);\
		while (FindNextFile(hndl, &fd))\
			list = ap(ap(nameCons, buildString(fd.cFileName)), list);\
		c = revOnto(list, nameNil);\
	}
#define DOES_DIRECTORY_EXIST(path,b) DWORD attr = GetFileAttributes(path);\
		b = (attr != INVALID_FILE_ATTRIBUTES) && (attr & FILE_ATTRIBUTE_DIRECTORY)
#define PATHSEP '\\'
#define ENV "APPDATA"
#define F_OK 00
#define SYS 1 /* Windows */
#endif

#if UNIX
#include <dirent.h>
#define MKDIR(path) mkdir(path, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)
#define GET_DIRECTORY_CONTENT(c,e) struct dirent **dirs;\
	int num = scandir(evalName(primArg(3)), &dirs, NULL, NULL);\
	if (num < 0) { /* check error occurs */\
		e = TRUE;\
	} else if (num == 0) { /* empty folder */\
		free(dirs);\
		c = nameNil;\
	} else { /* none empty folder */\
		List list = NIL;\
		int i;\
		for (i = 0; i < num; i++) {\
			list = ap(ap(nameCons, buildString(dirs[i]->d_name)), list);\
			free(dirs[i]);\
		}\
		free(dirs);\
		c = revOnto(list, nameNil);\
	}
#define DOES_DIRECTORY_EXIST(path,b) DIR* dir = opendir(path);\
	b = (dir && (errno == ENOENT));\
	if (b)\
		closedir(dir)
#define PATHSEP '/'
#define ENV "HOME"
#define SYS 2 /* Unix */
#endif

#if DJGPP2
#define SYS 3 /* Dos */
#endif
#if  RISCOS
#define SYS 4 /* RiscOS */
#endif

#ifndef MKDIR
#define MKDIR(path) 0
#endif
#ifndef GET_DIRECTORY_CONTENT
#define GET_DIRECTORY_CONTENT(c,e) c = nameNil
#endif
#ifndef DOES_DIRECTORY_EXIST
#define DOES_DIRECTORY_EXIST(path,b) b = FALSE
#endif
#ifndef PATHSEP
#define PATHSEP '/'
#endif
#ifndef ENV
#define ENV ""
#endif
#ifndef SYS
#define SYS 0 /* Unknown */
#endif

/* ------------------------------------------
* System module functions
* ---------------------------------------*/
primFun(primSystem) { /* :: [Char] -> IO Int but should be [Char] -> IO ExitCode */
	String str;
	int exit_code;
	str = evalName(primArg(3));
	exit_code = system(str);
	updapRoot(primArg(1),mkInt(exit_code));
}

primFun(primCreateDirectory) { /* :: String -> IO () */
	int err;
	String dir = evalName(primArg(3));
	err = MKDIR(dir);
	if ( err == 0) { /* check if no error during directory creation occure */
		updapRoot(primArg(1),nameUnit);
	} else { /* error handling */
		updapRoot(primArg(2),nameIllegal);
	}
}


primFun(primGetDirectoryContents) { /* :: String -> IO [String] */
	Cell content;
	Bool is_error = FALSE;
	GET_DIRECTORY_CONTENT(content, is_error);
	if (is_error)
		updapRoot(primArg(2),nameIllegal);
	else
		updapRoot(primArg(1), content);
}

primFun(primDoesDirectoryExist) { /* :: FilePath -> IO Bool */
	Bool is_dir;
	String path = evalName(primArg(3));
	DOES_DIRECTORY_EXIST(path, is_dir);
	updapRoot(primArg(1), is_dir ? nameTrue : nameFalse);
}

primFun(primDoesFileExist) {
    String path = evalName(primArg(3));
    Bool exist = access(path, F_OK) == 0;
    updapRoot(primArg(1), exist ? nameTrue : nameFalse);
}

/* -----------------------------------------------------
* Additional system dependent features
*------------------------------------------------------

/* System specific file seperator */
primFun(primGetFileSeparator) { /* :: Char */
	push(mkChar(PATHSEP));
}

/* System specific directory constant that should be used for data saved by */
/* the application. A subdirectory with a proper name to avoid name clashes */
/* should be used (Vendor/AppName). */
primFun(primGetAppDirectory) { /* :: String */
	push(buildString(getenv(ENV)));
}

// Int to represent the OS. Can be used with OSType:
// 0: Unknown
// 1: Windows
// 2: Unix
// 3: Dos
// 4: RiscOS
primFun(primGetOS) { /* :: Int */
	push(mkInt(SYS));
}

/* -----------------------------------------------------------
* Section to undefine system dependent macros
* ------------------------------------------------------------*/
#undef MKDIR
#undef GET_DIRECTORY_CONTENT
#undef DOES_DIRECTORY_EXIST
#undef PATHSEP
#undef ENV


#endif