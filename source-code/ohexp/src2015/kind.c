/* --------------------------------------------------------------------------
* kind.c:      Copyright (c) Mark P Jones 1991-1998.   All rights reserved.
*              See NOTICE for details and conditions of use etc...
*              Hugs version 1.3b, January 1998
*
* Part of type checker dealing with kind inference
* ------------------------------------------------------------------------*/

/* to highlight uses of type vars  */
/* as kind variables		   */
#define newKindvars(n)	newTyvars(n)

/* TRUE => display kind errors in  */
/*	   full detail		   */
Bool kindExpert = TRUE;//FALSE;

/* --------------------------------------------------------------------------
* Kind checking code:
* ------------------------------------------------------------------------*/

static Void local kindError(
	Int    l,				/* line number near constuctor exp */
	Constr c,				/* constructor			   */
	Constr in,				/* context (if any)		   */
	String wh,				/* place in which error occurs	   */
	Kind k,				/* expected kind (k,o)		   */
	Int o				/* inferred kind (typeIs,typeOff)  */
	)
{
	clearMarks();

	if (!kindExpert) {			/* for those with a fear of kinds  */
		ERRMSG(l) "Illegal type" ETHEN
			if (nonNull(in)) {
				ERRTEXT " \"" ETHEN ERRTYPE(in);
				ERRTEXT "\""  ETHEN
			}
			ERRTEXT " in %s\n", wh
				EEND;
	}

	ERRMSG(l) "Kind error in %s", wh ETHEN
		if (nonNull(in)) {
			ERRTEXT "\n*** expression     : " ETHEN ERRTYPE(in);
		}
		ERRTEXT "\n*** constructor    : " ETHEN ERRTYPE(c);
		ERRTEXT "\n*** kind           : " ETHEN ERRKIND(copyType(typeIs,typeOff));
		ERRTEXT "\n*** does not match : " ETHEN ERRKIND(copyType(k,o));
		if (unifyFails) {
			ERRTEXT "\n*** because        : %s", unifyFails ETHEN
		}
		ERRTEXT "\n"
			EEND;
}

#define shouldKind(l,c,in,wh,k,o) if (!kunify(typeIs,typeOff,k,o)) \
	kindError(l,c,in,wh,k,o)
#define checkKind(l,c,in,wh,k,o)  kindConstr(l,c); shouldKind(l,c,in,wh,k,o)
#define inferKind(k,o)		  typeIs=k; typeOff=o

static Int  locCVars;			/* offset to local variable kinds  */
static List unkindTypes;		/* types in need of kind annotation*/

static Void local kindConstr(Int  l, Cell c)	/* Determine kind of constructor   */
{
	Cell h = getHead(c);
	Int  n = argCount;

	if (isSynonym(h) && n<tycon(h).arity) {
		ERRMSG(l) "Not enough arguments for type synonym \"%s\"",
			textToStr(tycon(h).text)
			EEND;
	}

	if (n==0)				/* trivial case, no arguments	   */
		typeIs = kindAtom(c);
	else {				/* non-trivial application	   */
		static String app = "constructor application";
		Cell   a = c;
		Int    i;
		Kind   k;
		Int    beta;

		varKind(n);
		beta   = typeOff;
		k      = typeIs;

		typeIs = kindAtom(h);		/* h  :: v1 -> ... -> vn -> w	   */
		shouldKind(l,h,c,app,k,beta);

		for (i=n; i>0; --i) {		/* ci :: vi for each 1 <- 1..n	   */
			checkKind(l,arg(a),c,app,var,beta+i-1);
			a = fun(a);
		}
		tyvarType(beta+n);		/* inferred kind is w		   */
	}
}

static Kind local kindAtom(Cell c)		/* Find kind of atomic constructor */
{
	switch (whatIs(c)) {
	case TUPLE    : return simpleKind(tupleOf(c));	/*(,)::* -> * -> * */
	case OFFSET   : return mkInt(locCVars+offsetOf(c));
	case TYCON    : return tycon(c).kind;
	case WILDTYPE : snd(c) = mkInt(newKindvars(1));
		return snd(c);
	}
	internal("kindAtom");
	return STAR;/* not reached */
}

static Void local kindPred(Int  line, Cell pred)	/* Check kinds of arguments in pred*/
{
	static String predicate = "class constraint";
	checkKind(line,arg(pred),NIL,predicate,cclass(fun(pred)).sig,0);
}

/* check that (poss qualified) type*/
/* is well-kinded		   */
static Void local kindType(Int    line, String wh, Type   type)
{
	locCVars = 0;
	if (isPolyType(type)) {		/* local constructor vars reqd?	   */
		Kind k      = polySigOf(type);
		Int  n      = 0;
		for (; isPair(k); k=snd(k))
			n++;
		locCVars    = newKindvars(n);
		unkindTypes = cons(pair(mkInt(locCVars),snd(type)),unkindTypes);
		type	    = monoTypeOf(type);
	}
	if (whatIs(type)==QUAL) {		/* examine context (if any)	   */
		map1Proc(kindPred,line,fst(snd(type)));
		type = snd(snd(type));
	}
	checkKind(line,type,NIL,wh,STAR,0);	/* finally, check type part	   */
}

/* check that sub/supertype of tc  */
/* is well-kinded		   */
static Void local kindAxiom(Tycon  tc, Type   type)
{
	String wh   = tycon(tc).what==DATATYPE ? "subtype" : "supertype";
	Int    line = tycon(tc).line;

	locCVars = 0;
	if (isPolyType(type)) {		/* local constructor vars reqd?	   */
		Int  offs   = tyvar(intOf(tycon(tc).kind))->offs;
		Kind k      = polySigOf(type);
		Int  n      = 0;
		Int  i      = 0;

		for (; isPair(k); k=snd(k))
			n++;
		locCVars    = newKindvars(n);
		unkindTypes = cons(pair(mkInt(locCVars),snd(type)),unkindTypes);
		type	    = monoTypeOf(type);

		for (; i < tycon(tc).arity; i++)
			bindTv(locCVars+i,var,offs+i);
	}
	checkKind(line,type,NIL,wh,STAR,0);	/* finally, check type part	   */
}

static Void local fixKinds() {		/* add kind annotations to types   */
	for (; nonNull(unkindTypes); unkindTypes=tl(unkindTypes)) {
		Pair pr   = hd(unkindTypes);
		Int  beta = intOf(fst(pr));
		Cell qts  = fst(snd(pr));
		for (;;) {
			if (isNull(hd(qts)))
				hd(qts) = copyKindvar(beta++);
			else
				hd(qts) = ap(hd(qts),copyKindvar(beta++));
			if (nonNull(tl(qts)))
				qts = tl(qts);
			else {
				tl(qts) = STAR;
				break;
			}
		}
#ifdef DEBUG_KINDS
		printf("Type expression: ");
		printType(stdout,snd(snd(pr)));
		printf(" :: ");
		printKind(stdout,fst(snd(pr)));
		printf("\n");
#endif
	}
}

/* --------------------------------------------------------------------------
* Kind checking of groups of type constructors and classes:
* ------------------------------------------------------------------------*/

/* find kinds for mutually rec. gp */
/* of tycons and classes	   */
Void kindTCGroup(List tcs)
{
	typeChecker(RESET);
	mapProc(initTCKind,tcs);
	mapProc(kindTC,tcs);
	mapProc(genTC,tcs);
	fixKinds();
	typeChecker(RESET);
}

static Void local initTCKind(Cell c)		/* build initial kind/arity for	c  */
{
	/* Initial kind of tycon is:	   */
	/*    v1 -> ... -> vn -> vn+1	   */
	/* where n is the arity of c.	   */
	/* For data definitions, vn+1 == * */
	if (isTycon(c)) {
		Int beta = newKindvars(1);
		varKind(tycon(c).arity);
		bindTv(beta,typeIs,typeOff);
		switch (whatIs(tycon(c).what)) {
		case STRUCTTYPE :
		case PRIMTYPE :
		case NEWTYPE  :
		case DATATYPE : bindTv(typeOff+tycon(c).arity,STAR,0);
		}
		tycon(c).kind = mkInt(beta);
	}
	else
		cclass(c).sig = mkInt(newKindvars(1));
}

/* check each part of a tycon/class*/
/* is well-kinded		   */
static Void local kindTC(Cell c)
{
	if (isTycon(c)) {
		static String cfun = "constructor function";
		static String tsyn = "synonym definition";
		Int    line        = tycon(c).line;

		switch (whatIs(tycon(c).what)) {
		case PRIMTYPE    : internal("kindTC");

		case STRUCTTYPE  :
		case DATATYPE    : {   List axs = tycon(c).axioms;

			for (; nonNull(axs); axs=tl(axs))
				kindAxiom(c,hd(axs));
						   }
						   /* Intentional fall-thru */

		case NEWTYPE     : {   List cs  = tycon(c).defn;

			for (; nonNull(cs); cs=tl(cs))
				kindType(line,cfun,name(hd(cs)).type);
						   }
						   break;

		default          : 	locCVars = tyvar(intOf(tycon(c).kind))->offs;
			checkKind(line,tycon(c).defn,NIL,
				tsyn,var,locCVars+tycon(c).arity);
		}
	}
	/* scan type exprs in class defn to*/
	/* determine the class signature   */
	else {
		List ms  = cclass(c).members;
		List scs = cclass(c).supers;

		for (; nonNull(scs); scs=tl(scs))
			if (!kunify(cclass(hd(scs)).sig,0,cclass(c).sig,0)) {
				ERRMSG(cclass(c).line)
					"Kind of class \"%s\" does not match superclass \"%s\"",
					textToStr(cclass(c).text), textToStr(cclass(hd(scs)).text)
					EEND;
			}

			for (; nonNull(ms); ms=tl(ms)) {
				Int  line = intOf(fst3(hd(ms)));
				Type type = thd3(hd(ms));
				kindType(line,"member function type signature",type);
			}
	}
}

/* generalise kind inferred for	   */
/* given tycon/class		   */
static Void local genTC(Cell c)
{
	if (isTycon(c)) {
		tycon(c).kind = copyKindvar(intOf(tycon(c).kind));
#ifdef DEBUG_KINDS
		printf("%s :: ",textToStr(tycon(c).text));
		printKind(stdout,tycon(c).kind);
		putchar('\n');
#endif
	}
	else {
		cclass(c).sig = copyKindvar(intOf(cclass(c).sig));
#ifdef DEBUG_KINDS
		printf("%s :: ",textToStr(cclass(c).text));
		printKind(stdout,cclass(c).sig);
		putchar('\n');
#endif
	}
}

static Kind local copyKindvar(Int vn)      	/* build kind attatched to variable*/
{
	Tyvar *tyv = tyvar(vn);
	if (tyv->bound)
		return copyKind(tyv->bound,tyv->offs);

	/* any unbound variable defaults to*/
	/* the kind of all types	   */
	return STAR;
}

/* build kind expression from	   */
/* given skeleton		   */
static Kind local copyKind(Kind k, Int  o)
{
	switch (whatIs(k)) {
		/* ensure correct */
		/* eval. order    */
	case AP      : {   Kind l = copyKind(fst(k),o);
		Kind r = copyKind(snd(k),o);
		return ap(l,r);
				   }
	case OFFSET  : return copyKindvar(o+offsetOf(k));
	case INTCELL : return copyKindvar(intOf(k));
	}
	return k;
}

/* --------------------------------------------------------------------------
* Kind checking of instance declaration headers:
* ------------------------------------------------------------------------*/

Void kindInst(Inst in, Cell h)			/* check predicates in instance    */
{
	typeChecker(RESET);
	locCVars = newKindvars(inst(in).arity);
	kindPred(inst(in).line,h);
	map1Proc(kindPred,inst(in).line,inst(in).specifics);
	typeChecker(RESET);
}

/* --------------------------------------------------------------------------
* Add kind annotation to wildcard types:
* ------------------------------------------------------------------------*/

static Void local fixWildtypes(Type t)
{
	switch (whatIs(t)) {
	case POLYTYPE : fixWildtypes(monoTypeOf(t));
		break;
	case QUAL     : fixWildtypes(snd(snd(t)));
		break;
	case (int)NULL: 
	case OFFSET   : 
	case INTCELL  : 
	case TYCON    :
	case TUPLE    : break;;
	case AP       : fixWildtypes(fst(t));
		fixWildtypes(snd(t));
		break;;
	case WILDTYPE : snd(t) = copyKindvar(intOf(snd(t)));
		break;
	default       : internal("fixWildtypes");
	}
}

/* --------------------------------------------------------------------------
* Kind checking of individual type signatures:
* ------------------------------------------------------------------------*/

Void kindSigType(Int  line, Type type)		/* check that type is well-kinded  */
{
	typeChecker(RESET);
	kindType(line,"type expression",type);
	fixKinds();
	fixWildtypes(type);
	typeChecker(RESET);
}

/* --------------------------------------------------------------------------
* Kind checking of default types:
* ------------------------------------------------------------------------*/

/* check that list of types are	   */
/* well-kinded			   */
Void kindDefaults(Int  line, List ts)
{
	typeChecker(RESET);
	map2Proc(kindType,line,"default type",ts);
	fixKinds();
	typeChecker(RESET);
}

/* --------------------------------------------------------------------------
* Support for `kind preserving substitutions' from unification:
* ------------------------------------------------------------------------*/

/* check that two (mono)kinds are  */
/* equal			   */
static Bool local eqKind(Kind k1, Kind k2)
{
	return k1==k2
		|| (isPair(k1) && isPair(k2)
		&& eqKind(fst(k1),fst(k2))
		&& eqKind(snd(k1),snd(k2)));
}

/* Find kind of constr during type */
/* checking process		   */
static Kind local getKind(Cell c, Int  o)
{
	if (isAp(c))					/* application	   */
		return snd(getKind(fst(c),o));
	switch (whatIs(c)) {
	case TUPLE  : return simpleKind(tupleOf(c));	/*(,)::* -> * -> * */
	case OFFSET : return tyvar(o+offsetOf(c))->kind;
	case INTCELL: return tyvar(intOf(c))->kind;
	case TYCON  : return tycon(c).kind;
	}
#ifdef DEBUG_KINDS
	printf("getKind c = %d, whatIs=%d\n",c,whatIs(c));
#endif
	internal("getKind");
	return STAR;/* not reached */
}

/* --------------------------------------------------------------------------
* Two forms of kind expression are used quite frequently:
*	*  -> *  -> ... -> *  -> *	for kinds of ->, [], ->, (,) etc...
*	v1 -> v2 -> ... -> vn -> vn+1	skeletons for constructor kinds
* Expressions of these forms are produced by the following functions which
* use a cache to avoid repeated construction of commonly used values.
* A similar approach is used to store the types of tuple constructors in the
* main type checker.
* ------------------------------------------------------------------------*/

#define MAXKINDFUN 10
static  Kind simpleKindCache[MAXKINDFUN];
static  Kind varKindCache[MAXKINDFUN];

static Kind local makeSimpleKind(Int n)	/* construct * -> ... -> * (n args)*/
{
	Kind k = STAR;
	while (n-- > 0)
		k = ap(STAR,k);
	return k;
}

/* return (possibly cached) simple */
/* function kind		   */
static Kind local simpleKind(Int n)
{
	if (n>=MAXKINDFUN)
		return makeSimpleKind(n);
	else if (nonNull(simpleKindCache[n]))
		return simpleKindCache[n];
	else if (n==0)
		return simpleKindCache[0] = STAR;
	else
		return simpleKindCache[n] = ap(STAR,simpleKind(n-1));
}

static Kind local makeVarKind(Int n)	/* construct v0 -> .. -> vn	   */
{
	Kind k = mkOffset(n);
	while (n-- > 0)
		k = ap(mkOffset(n),k);
	return k;
}

/* return (possibly cached) var	   */
/* function kind		   */
static Void local varKind(Int n)
{
	typeOff = newKindvars(n+1);
	if (n>=MAXKINDFUN)
		typeIs = makeVarKind(n);
	else if (nonNull(varKindCache[n]))
		typeIs = varKindCache[n];
	else
		typeIs = varKindCache[n] = makeVarKind(n);
}

/*-------------------------------------------------------------------------*/

