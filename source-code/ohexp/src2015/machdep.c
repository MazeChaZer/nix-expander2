/* --------------------------------------------------------------------------
* machdep.c:   Copyright (c) Mark P Jones 1991-1998.   All rights reserved.
*              See NOTICE for details and conditions of use etc...
*              Hugs version 1.3b, January 1998
*
* Machine dependent code
* RISCOS specific code provided by Bryan Scatergood, JBS
* ------------------------------------------------------------------------*/

#if UNIX
#include <signal.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#endif

#if VC32
#include <dos.h>
#include <conio.h>
#include <stdlib.h>
#include <sys\stat.h>
#include <signal.h>
#include <time.h>
#endif

#if DJGPP2
#include <dos.h>
#include <stdlib.h>
#include <std.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#endif

#if RISCOS
#include <assert.h>
#include <signal.h>
#include "swis.h"
#include "os.h"
#endif

/* --------------------------------------------------------------------------
* Find information about a file:
* ------------------------------------------------------------------------*/

#if RISCOS
typedef struct { unsigned hi, lo; } Time;
#define timeChanged(now,thn)	(now.hi!=thn.hi || now.lo!=thn.lo)
#define timeSet(var,tm)		var.hi = tm.hi; var.lo = tm.lo
#else
typedef time_t Time;
#define timeChanged(now,thn)	(now!=thn)
#define timeSet(var,tm)		var = tm
#endif

static Void local getFileInfo(String, Time *, Long *);

static Void local getFileInfo(String s, Time   *tm, Long   *sz)	/* find time stamp and size of file*/
{
#if RISCOS				/* get file info for RISCOS -- JBS */
	os_regset r;			/* RISCOS PRM p.850 and p.837	   */
	r.r[0] = 17;			/* Read catalogue, no path	   */
	r.r[1] = (int)s;
	os_swi(OS_File, &r);
	if(r.r[0] == 1 && (r.r[2] & 0xFFF00000) == 0xFFF00000) {
		tm->hi = r.r[2] & 0xFF;		/* Load address (high byte)	   */
		tm->lo = r.r[3];		/* Execution address (low 4 bytes) */
	}
	else				/* Not found, or not time-stamped  */
		tm->hi = tm->lo = 0;
	*sz = (Long)(r.r[0] == 1 ? r.r[4] : 0);
#else					/* normally just use stat()	   */
	static struct stat scbuf;
	stat(s,&scbuf);
	*tm = scbuf.st_mtime;
	*sz = (Long)(scbuf.st_size);
#endif
}

#if RISCOS				/* RISCOS needs access()	   */
int access(char *s, int dummy) {	/* Give 1 iff cannot access file s */
	os_regset r;			/* RISCOS PRM p.850	-- JBS	   */
	assert(dummy == 0);
	r.r[0] = 17; /* Read catalogue, no path */
	r.r[1] = (int)s;
	os_swi(OS_File, &r);
	return r.r[0] != 1;
}

int namecmp(char *filename, char *spec){/* For filename extension hacks	   */
	while(*spec)
		if  (tolower(*filename) != *spec++)
			return 0;
		else
			++filename;
	return *filename == '.';
}
#endif

/* --------------------------------------------------------------------------
* Search for script files on the HUGS path:
* ------------------------------------------------------------------------*/

static String local normPath(String);
static Void   local searchChr(Int);
static Void   local searchStr(String);
static Bool   local tryEndings(String);

#if (DJGPP | VC32)
#define SLASH			'\\'
#define PATHSEP			';'
#else
#define SLASH			'/'
#define PATHSEP			':'
#endif
#ifndef valid
#define valid(file)		(access(file,0)==0)
#endif

static String endings[] = { "", ".hs", ".lhs", 0 };
static char   searchBuf[FILENAME_MAX+1];
static Int    searchPos;

#define searchReset(n)		searchBuf[searchPos=(n)]='\0'

/* Try, as much as possible, to normalize  */
/* a pathname in some appropriate manner.  */
static local String normPath(String s)
{
#if DJGPP2
	static char path[FILENAME_MAX+1];
	strcpy(path,s);
	strlwr(path);
	return path;
#else
	return s;
#endif
}

static Void local searchChr(Int c)	/* Add single character to search buffer   */
{
	if (searchPos<FILENAME_MAX) {
		searchBuf[searchPos++] = (char)c;
		searchBuf[searchPos]   = '\0';
	}
}

static Void local searchStr(String s)	/* Add string to search buffer		   */
{
	while (*s && searchPos<FILENAME_MAX)
		searchBuf[searchPos++] = *s++;
	searchBuf[searchPos] = '\0';
}

static Bool local tryEndings(String s)	/* Try each of the listed endings	   */
{
	Int i = 0;
	searchStr(s);
	for (; endings[i]; ++i) {
		Int save = searchPos;
		searchStr(endings[i]);
		if (valid(searchBuf))
			return TRUE;
		searchReset(save);
	}
	return FALSE;
}

/* Look for a file along specified path	   */
/* If nonzero, a path prefix from along is */
/* used as the first prefix in the search. */
String findPathname(String along, String name)
{
	String pathpt = hugsPath;

	searchReset(0);
	if (along) {		/* Was a path for an existing file given?  */
		Int last = (-1);
		Int i    = 0;
		for (; along[i]; i++) {
			searchChr(along[i]);
			if (along[i]==SLASH)
				last = i;
		}
		searchReset(last+1);
	}
	if (tryEndings(name))
		return normPath(searchBuf);

	if (pathpt && *pathpt) {	/* Otherwise, we look along the OHUGSPATH   */
		Bool more = TRUE;
		do {
			searchReset(0);
			if (*pathpt) {
				if (*pathpt!=PATHSEP) {
					do
					searchChr(*pathpt++);
					while (*pathpt && *pathpt!=PATHSEP);
					searchChr(SLASH);
				}
				if (*pathpt==PATHSEP)
					pathpt++;
				else
					more = FALSE;
			}
			else
				more = FALSE;
			if (tryEndings(name))
				return normPath(searchBuf);
		} while (more);
	}

	/* As a last resort, look for file in the  */
	/* current directory			   */
	searchReset(0);
	tryEndings(name);
	return normPath(searchBuf);
}

/* --------------------------------------------------------------------------
* Get time/date stamp for inclusion in compiled files:
* ------------------------------------------------------------------------*/

#if PROFILING
/* return time&date string	   */
/* must end with '\n' character	   */
String timeString() {
	time_t clock;
	time(&clock);
	return(ctime(&clock));
}
#endif

/* --------------------------------------------------------------------------
* Garbage collection notification:
* ------------------------------------------------------------------------*/

Bool gcMessages = FALSE;		/* TRUE => print GC messages	   */

Void gcStarted() {			/* notify garbage collector start  */
	if (gcMessages) {
		printf("{{Gc");
		fflush(stdout);
	}
}

Void gcScanning() {			/* notify garbage collector scans  */
	if (gcMessages) {
		putchar(':');
		fflush(stdout);
	}
}

Void gcRecovered(Int recovered, Int flatrec)	/* notify garbage collection done  */
{
	if (gcMessages) {
#ifdef SHOW_FLATREC
		printf("%d,%d}}",recovered,flatrec);
#else
		printf("%d}}",recovered);
#endif
		fflush(stdout);
	}
}

Cell *CStackBase;			/* Retain start of C control stack */

#if RISCOS				/* Stack traversal for RISCOS	   */

/* Warning: The following code is specific to the Acorn ARM under RISCOS
(and C4).  We must explicitly walk back through the stack frames, since
the stack is extended from the heap. (see PRM pp. 1757).  gcCStack must
not be modified, since the offset '5' assumes that only v1 is used inside
this function. Hence we do all the real work in gcARM.
*/

#define spreg 13 /* C3 has SP=R13 */

#define previousFrame(fp)	((int *)((fp)[-3]))
#define programCounter(fp)	((int *)((*(fp)-12) & ~0xFC000003))
#define isSubSPSP(w)		(((w)&dontCare) == doCare)
#define doCare			(0xE24DD000)  /* SUB r13,r13,#0 */
#define dontCare		(~0x00100FFF) /* S and # bits   */
#define immediateArg(x)		( ((x)&0xFF) << (((x)&0xF00)>>7) )

static void gcARM(int *fp) {
	int si = *programCounter(fp);	/* Save instruction indicates how */
	/* many registers in this frame   */
	int *regs = fp - 4;
	if (si & (1<<0)) markWithoutMove(*regs--);
	if (si & (1<<1)) markWithoutMove(*regs--);
	if (si & (1<<2)) markWithoutMove(*regs--);
	if (si & (1<<3)) markWithoutMove(*regs--);
	if (si & (1<<4)) markWithoutMove(*regs--);
	if (si & (1<<5)) markWithoutMove(*regs--);
	if (si & (1<<6)) markWithoutMove(*regs--);
	if (si & (1<<7)) markWithoutMove(*regs--);
	if (si & (1<<8)) markWithoutMove(*regs--);
	if (si & (1<<9)) markWithoutMove(*regs--);
	if (previousFrame(fp)) {
		/* The non-register stack space is for the previous frame is above
		this fp, and not below the previous fp, because of the way stack
		extension works. It seems the only way of discovering its size is
		finding the SUB sp, sp, #? instruction by walking through the code
		following the entry point.
		*/
		int *oldpc = programCounter(previousFrame(fp));
		int fsize = 0, i;
		for(i = 1; i < 6; ++i)
			if(isSubSPSP(oldpc[i])) fsize += immediateArg(oldpc[i]) / 4;
		for(i=1; i<=fsize; ++i)
			markWithoutMove(fp[i]);
	}
}

void gcCStack() {
	int dummy;
	int *fp = 5 + &dummy;
	while (fp) {
		gcARM(fp);
		fp = previousFrame(fp);
	}
}

#else			/* Garbage collection for standard stack machines  */

/* Garbage collect elements off    */
/* C stack			   */
Void gcCStack() {
	Cell stackTop = NIL;
	Cell *ptr = &stackTop;
	if (((long)(ptr) - (long)(CStackBase))&3)
		fatal("gcCStack");

#define StackGrowsDown	while (ptr<=CStackBase) markWithoutMove(*ptr++)
#define StackGrowsUp	while (ptr>=CStackBase) markWithoutMove(*ptr--)
#define GuessDirection	if (ptr>CStackBase) StackGrowsUp; else StackGrowsDown
#if HPUX
	GuessDirection;
#else
	StackGrowsDown;
#endif
#undef  StackGrowsDown
#undef  StackGrowsUp
#undef  GuessDirection
}
#endif

/* --------------------------------------------------------------------------
* Terminal dependent stuff:
* ------------------------------------------------------------------------*/

#if   (TERMIO_IO | SGTTY_IO | TERMIOS_IO)

#if TERMIO_IO
#include <termio.h>
typedef  struct termio   TermParams;
#define  getTerminal(tp) ioctl(fileno(stdin),TCGETA,&tp)
#define  setTerminal(tp) ioctl(fileno(stdin),TCSETAF,&tp)
#define  noEcho(tp)      tp.c_lflag    &= ~(ICANON | ECHO); \
	tp.c_cc[VMIN]  = 1;		    \
	tp.c_cc[VTIME] = 0;
#endif

#if SGTTY_IO
#include <sgtty.h>
typedef  struct sgttyb   TermParams;
#define  getTerminal(tp) ioctl(fileno(stdin),TIOCGETP,&tp)
#define  setTerminal(tp) ioctl(fileno(stdin),TIOCSETP,&tp)
#if HPUX
#define  noEcho(tp)      tp.sg_flags |= RAW; tp.sg_flags &= (~ECHO);
#else
#define  noEcho(tp)      tp.sg_flags |= CBREAK; tp.sg_flags &= (~ECHO);
#endif
#endif

#if TERMIOS_IO
#include <termios.h>
typedef  struct termios  TermParams;
#define  getTerminal(tp) tcgetattr(fileno(stdin), &tp)
#define  setTerminal(tp) tcsetattr(fileno(stdin), TCSAFLUSH, &tp)
#define  noEcho(tp)      tp.c_lflag    &= ~(ICANON | ECHO); \
	tp.c_cc[VMIN]  = 1;                \
	tp.c_cc[VTIME] = 0;
#endif

static Bool messedWithTerminal = FALSE;
static TermParams originalSettings;

Void normalTerminal() {			/* restore terminal initial state  */
	if (messedWithTerminal)
		setTerminal(originalSettings);
}

Void noechoTerminal() {			/* set terminal into noecho mode   */
	TermParams settings;

	if (!messedWithTerminal) {
		getTerminal(originalSettings);
		messedWithTerminal = TRUE;
	}
	getTerminal(settings);
	noEcho(settings);
	setTerminal(settings);
}

Int getTerminalWidth() {		/* determine width of terminal	   */
#ifdef TIOCGWINSZ
#ifdef _M_UNIX				/* SCO Unix 3.2.4 defines TIOCGWINSZ*/
#include <sys/stream.h>			/* Required by sys/ptem.h	   */
#include <sys/ptem.h>			/* Required to declare winsize	   */
#endif
	static struct winsize terminalSize;
	ioctl(fileno(stdout),TIOCGWINSZ,&terminalSize);
	return (terminalSize.ws_col==0)? 80 : terminalSize.ws_col;
#else
	return 80;
#endif
}

/* read character from terminal	   */
/* without echo, assuming that	   */
/* noechoTerminal() is active...   */
Int readTerminalChar() {
	return getchar();
}
#endif

#if DOS_IO
static Bool terminalEchoReqd = TRUE;

Int getTerminalWidth() {		/* PC screen is fixed 80 chars	   */
	return 80;
}

Void normalTerminal() {			/* restore terminal initial state  */
	terminalEchoReqd = TRUE;
}

Void noechoTerminal() {			/* turn terminal echo on/off	   */
	terminalEchoReqd = FALSE;
}

Int readTerminalChar() {		/* read character from terminal	   */
	if (terminalEchoReqd)
		return getchar();
	else {
		Int c = getch();
		return c=='\r' ? '\n' : c;
	}
}
#endif

#if RISCOS
Int getTerminalWidth() {
	int dummy, width;
	(void) os_swi3r(OS_ReadModeVariable, -1, 1, 0, &dummy, &dummy, &width);
	return width+1;
}

Void normalTerminal() {			/* restore terminal initial state  */
}					/* (not yet implemented)	   */

Void noechoTerminal() {			/* turn terminal echo on/off	   */
}					/* (not yet implemented)	   */

Int readTerminalChar() {		/* read character from terminal	   */
	return getchar();
}
#endif

/* --------------------------------------------------------------------------
* Interrupt handling:
* ------------------------------------------------------------------------*/

Bool    broken         = FALSE;
static  Bool breakReqd = FALSE;
static  sigProto(ignoreBreak);

/* set break trapping on if reqd,  */
/* or off otherwise, returning old */
Bool breakOn(Bool reqd)
{
	Bool old  = breakReqd;

	breakReqd = reqd;
	if (reqd) {
		if (broken) {			/* repond to break signal received */
			broken = FALSE;		/* whilst break trap disabled	   */
			sigRaise(breakHandler);
		}
#if VC32
		ctrlbrk(ignoreBreak);
#else
		ctrlbrk(breakHandler);
#endif
	}
	else
		ctrlbrk(ignoreBreak);

	return old;
}

static sigHandler(ignoreBreak) {	/* record but don't respond to break*/
	ctrlbrk(ignoreBreak);
	broken = TRUE;
	sigResume;
}

/* --------------------------------------------------------------------------
* Shell escapes:
* ------------------------------------------------------------------------*/

/* Start editor on file name at	   */
/* given line.  Both name and line */
/* or just line may be zero	   */
Bool startEdit(Int    line, String name)
{
	static char editorCmd[FILENAME_MAX+1];

	if (hugsEdit && *hugsEdit) {	/* Check that editor configured	   */
		Unsigned n     = FILENAME_MAX;
		String he = hugsEdit;
		String ec = editorCmd;
		String rd = NULL;		/* Set to nonnull to redo ...	   */

		for (; n>0 && *he && *he!=' '; n--)
			*ec++ = *he++;		/* Copy editor name to buffer	   */

		/* Name, line, and enough space    */
		/* save, in case we don't find name*/
		if (name && line && n>1 && *he){
			rd = ec;
			while (n>0 && *he) {
				if (*he=='%') {
					if (*++he=='d' && n>10) {
						sprintf(ec,"%d",line);
						he++;
					}
					else if (*he=='s' && n>strlen(name)) {
						strcpy(ec,name);
						rd = NULL;
						he++;
					}
					else if (*he=='%' && n>1) {
						strcpy(ec,"%");
						he++;
					}
					/* Ignore % char if not followed   */
					/* by one of d, s, or %,	   */
					else
						*ec = '\0';
					for (; *ec && n>0; n--)
						ec++;
				}
				/* ignore % followed by anything other than d, s, or % */
				/* Copy other characters across	   */
				else {
					*ec++ = *he++;
					n--;
				}
			}
		}
		else
			line = 0;

		if (rd) {			/* If file name was not included   */
			ec   = rd;
			line = 0;
		}

		if (name && line==0 && n>1) {	/* Name, but no line ...	   */
			*ec++ = ' ';
			for (; n>0 && *name; n--)	/* ... just copy file name	   */
				*ec++ = *name++;
		}

		*ec = '\0';				/* Add terminating null byte	   */
	}
	else {
		ERRMSG(0) "Hugs is not configured to use an editor"
			EEND;
	}

	if (shellEsc(editorCmd))
		printf("Warning: Editor terminated abnormally\n");
	return TRUE;
}

Int shellEsc(String s)				/* run a shell command (or shell)  */
{
#if UNIX
	if (s[0]=='\0')
		s = fromEnv("SHELL","/bin/sh");
#endif
	return system(s);
}

#if RISCOS				/* RISCOS also needs a chdir()	   */
int chdir(char *s) {			/* RISCOS PRM p. 885	-- JBS	   */
	return os_swi2(OS_FSControl + XOS_Bit, 0, (int)s) != NULL;
}
#endif

/* --------------------------------------------------------------------------
* Floating point support:
* ------------------------------------------------------------------------*/

static union {
	Float flVal;
	Cell  clVal;
} fudgeCoerce;

Cell mkFloat(FloatPro fl)
{
	fudgeCoerce.flVal = (Float)fl;
	return pair(FLOATCELL,fudgeCoerce.clVal);
}

FloatPro floatOf(Cell c)
{
	fudgeCoerce.clVal = snd(c);
	return fudgeCoerce.flVal;
}

/* Make sure that floating   */
/* point values print out in */
/* a form in which they could*/
/* also be entered as floats */
String floatToString(FloatPro fl)
{
	static char buffer1[32];
	static char buffer2[32];
	Int i=0, j=0;

	sprintf(buffer1,FloatFMT,fl);
	while (buffer1[i] && strchr("eE.",buffer1[i])==0)
		buffer2[j++] = buffer1[i++];
	if (buffer1[i]!='.') {
		buffer2[j++] = '.';
		buffer2[j++] = '0';
	}
	while (buffer2[j++]=buffer1[i++])
		;
	return buffer2;
}

FloatPro stringToFloat(String s)
{
	return atof(s);
}

/* --------------------------------------------------------------------------
* Machine dependent control:
* ------------------------------------------------------------------------*/

/* Exit as quickly as possible, but at least restore terminal */
static Void doexit(int i) {
	machdep(EXIT);
	exit(i);
}

#if UNIX
/* exit in a panic, on receipt of  */
/* an unexpected signal		   */
static sigHandler(panic) {
	everybody(EXIT);
	fprintf(stderr,"\nUnexpected signal\n");
	doexit(1);
	sigResume;/*NOTREACHED*/
}
#endif

/* Handle machine specific	   */
/* initialisation etc..		   */
Void machdep(Int what)
{
	switch (what) {
	case MARK    :
		break;
#if UNIX
	case INSTALL :
#ifdef SIGHUP
		signal(SIGHUP,panic);
#endif
#ifdef SIGQUIT
		signal(SIGQUIT,panic);
#endif
#ifdef SIGTERM
		signal(SIGTERM,panic);
#endif
#ifdef SIGSEGV
		signal(SIGSEGV,panic);
#endif
#ifdef SIGBUS
		signal(SIGBUS,panic);
#endif
		break;
#endif
	case RESET   :
	case BREAK   :
	case EXIT    : normalTerminal();
		break;
	}
}

/* Elegant fix for a needed eight bit character... */
#if VC32
# define OE "\224"
#endif
#if MACOSX
# define OE "\232"
#endif
#ifndef OE
# define OE "\366"
#endif

/*-------------------------------------------------------------------------*/
