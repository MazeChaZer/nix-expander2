/* --------------------------------------------------------------------------
* connect.h:	Copyright (c) Mark P Jones 1991-1998.   All rights reserved.
*		See NOTICE for details and conditions of use etc...
*              Hugs version 1.3b, January 1998
*
* Connections between components of the Hugs system
* ------------------------------------------------------------------------*/

/* --------------------------------------------------------------------------
* Standard data:
* ------------------------------------------------------------------------*/

extern Name  nameFalse,	  nameTrue;	/* primitive constructor functions */
extern Name  nameNil,	  nameCons;
extern Name  nameJust,	  nameNothing;
extern Name  nameUnit;
extern Name  nameLT,      nameEQ;
extern Name  nameGT;
extern Name  nameFst,	  nameSnd;	/* standard combinators		   */
extern Name  nameId,	  nameOtherwise;
extern Name  nameNegate,  nameFlip;	/* primitives reqd for parsing	   */
extern Name  nameFrom,    nameFromThen;
extern Name  nameFromTo,  nameFromThenTo;
extern Name  nameFatbar,  nameFail;	/* primitives reqd for translation */
extern Name  nameIf,	  nameSel;
extern Name  nameCompAux;
extern Name  namePmInt,	  namePmFlt;	/* primitives for pattern matching */
extern Name  namePmInteger;
#if NPLUSK
extern Name  namePmNpk,	  namePmSub;	/* primitives for (n+k) patterns   */
#endif
extern Name  nameUndefMem;	 	/* undefined member primitive	   */
extern Name  nameMakeMem;		/* makeMember primitive		   */
extern Name  nameError;			/* For runtime error messages	   */
extern Name  nameUndefined;		/* A generic undefined value	   */
extern Name  nameBlackHole;		/* for GC-detected black hole	   */
extern Name  nameAnd,	  nameOr;	/* for optimisation of && and ||   */
extern Name  nameFromInt, nameFromDouble;/*coercion of numerics		   */
extern Name  nameFromInteger;
extern Name  nameEq,	  nameCompare;	/* names used for deriving	   */
extern Name  nameMinBnd,  nameMaxBnd;
extern Name  nameIndex,	  nameInRange;
extern Name  nameRange;
extern Name  nameLe,	  nameShowsPrec;
extern Name  nameMult,	  namePlus;
extern Name  nameConCmp,  nameEnRange;
extern Name  nameEnIndex, nameEnInRng;
extern Name  nameEnToEn,  nameEnFrEn;
extern Name  nameEnFrom,  nameEnFrTh;
extern Name  nameEnFrTo;
extern Name  nameComp,	  nameApp;	/* composition and append	   */
extern Name  nameShowField;		/* display single field		   */
extern Name  nameShowParen;		/* wrap with parens		   */
extern Name  nameRangeSize;		/* calculate size of index range   */
extern Class classMonad,  classMonad0;	/* Monads and monads with a zero   */
extern Class classFixMonad, classFailureMonad;
extern Name  nameDone;                  /* = return ()                     */
extern Name  nameReturn,  nameBind;	/* for translating monad comps	   */
extern Name  nameZero;			/* for monads with a zero	   */
extern Name  nameConst;

extern Name  nameStrict,  nameSeq;	/* Members of class Eval	   */
extern Name  nameIStrict, nameISeq;	/* ... and their implementations   */

extern Name  namePrint;			/* printing primitive		   */

#if    IO_MONAD
extern Type   typeProgIO;		/* For the IO monad, IO ()	   */
extern Void   ioExecute(Void);	/* IO monad executor		   */
extern Name   nameUserErr,  nameIllegal;/* primitives required for IOError */
extern Name   nameNameErr,  nameSearchErr;
extern Name   nameWriteErr, nameEvalErr;
extern Name   nameFileError;
#endif
#if OBJ
extern int    gArgc;
extern char   **gArgv;
extern Void   oExecute(Type);
extern Type   typeEnvironment;
extern Type   typeProgO;
extern Type   typeOProg;
extern Type   typeTemplate;
extern Cell   dictMonadTempl, dictFixMonadTempl;
#if O_TK
extern Type   typeTkProg;
#endif
extern Name   nameDeadlock,      nameReqAbort,  nameMissedDeadline, nameFileError;
extern Name   namePutCharSel,    namePutStrSel;
extern Name   nameSetReaderSel,  nameWriteFileSel;
extern Name   nameAppendFileSel, nameReadFileSel;
extern Name   nameTimeOfDaySel;
extern Name   nameProgArgsSel;
extern Name   nameGetEnvSel;
extern Name   nameTerminateSel;
extern Name   namePrimTempl,     namePrimAct,   namePrimReq;
extern Name   namePrimSet,       namePrimGet;
extern Name   nameCatch,         nameRaise,     nameRaiseO;
extern Name   nameForall,        nameWhile,     nameFixM;
#if O_IP
extern Name   nameInetSel;
extern Name   nameNetError;
extern Name   nameDeliver, nameClosed;
#endif
#endif
#if    LAZY_ST
extern Type   typeST;			/* Lazy state threads		   */
extern Name   nameSTRun;		/* Encapsulator			   */
#endif
#if    NPLUSK
extern Text  textPlus;			/* Used to recognise n+k patterns  */
#endif
extern Text  textBang;

extern String repeatStr;		/* Repeat last command string	   */
extern String hugsEdit;			/* String for editor command	   */
extern String hugsPath;			/* String for file search path	   */

extern Type  typeArrow;			/* Builtin type constructors	   */
extern Type  typeList;
extern Type  typeUnit;

extern List  stdDefaults;		/* List of standard default types  */

extern Class classEq;			/* `standard' classes		   */
extern Class classOrd;
extern Class classShow;
extern Class classRead;
extern Class classIx;
extern Class classEnum;
extern Class classEval;
extern Class classBounded;

extern Class classReal;			/* `numeric' classes		   */
extern Class classIntegral;
extern Class classRealFrac;
extern Class classRealFloat;
extern Class classFractional;
extern Class classFloating;
extern Class classNum;

extern Cell  *CStackBase;		/* pointer to base of C stack	   */

extern List  tyconDefns;		/* list of type constructor defns  */
extern List  typeInDefns;		/* list of synonym restrictions	   */
extern List  valDefns;			/* list of value definitions       */
extern List  opDefns;			/* list of operator definitions    */
extern List  classDefns;		/* list of class definitions       */
extern List  instDefns;			/* list of instance definitions    */
extern List  selDefns;			/* list of selector lists	   */
extern List  overDefns;			/* list of overloaded member defns */
extern List  primDefns;			/* list of primitive definitions   */
extern List  defaultDefns;		/* default definitions (if any)	   */
extern Int   defaultLine;		/* line in which default defs occur*/
extern List  evalDefaults;		/* defaults for evaluator	   */
extern Cell  inputExpr;			/* evaluator input expression      */
extern Addr  inputCode;			/* Code for compiled input expr    */

extern Int   whnfArgs;			/* number of args of term in whnf  */
extern Cell  whnfHead;			/* head of term in whnf            */
extern Int   whnfInt;			/* integer value of term in whnf   */
extern Float whnfFloat;			/* float value of term in whnf	   */
extern Long  numReductions;		/* number of reductions used       */
extern Long  numCells;			/* number of cells allocated       */
extern Int   numberGcs;			/* number of garbage collections   */
extern Int   cellsRecovered;		/* cells recovered by last gc	   */
extern Bool  broken;			/* indicates interrupt received    */

extern Bool  gcMessages;		/* TRUE => print GC messages	   */
extern Bool  literateScripts;		/* TRUE => default lit scripts     */
extern Bool  literateErrors;		/* TRUE => report errs in lit scrs */
extern Bool  failOnError;		/* TRUE => error produces immediate*/
/*	   termination		   */
extern Bool  kindExpert;		/* TRUE => display kind errors in  */
/* 	   full detail		   */

/* --------------------------------------------------------------------------
* Function prototypes etc...
* ------------------------------------------------------------------------*/

extern Void   everybody(Int);

#define RESET   1		/* reset subsystem                         */
#define MARK    2		/* mark parts of graph in use by subsystem */
#define INSTALL 3		/* install subsystem (executed once only)  */
#define EXIT	4		/* Take action immediately before exit()   */
#define BREAK   5		/* Take action after program break	   */

typedef long   Target;
extern  Void   setGoal(String, Target);
extern  Void   soFar(Target);
extern  Void   done(Void);
extern  String fromEnv(String,String);
extern  Bool   chase(List);

extern  Void   storage(Int);
extern  Void   setLastExpr(Cell);
extern  Cell   getLastExpr(Void);
extern  List   addTyconsMatching(String,List);
extern	List   addNamesMatching(String,List);

extern  Void   input(Int);
extern  Void   consoleInput(String);
extern  Void   projInput(String);
extern  Void   parseScript(String,Long);
extern  Void   parseExp(Void);
extern  String readFilename(Void);
extern  String readLine(Void);
extern  Syntax defaultSyntax(Text);
extern  String unlexChar(Char,Char);
extern  Void   printString(String);

extern  Bool   isStructSel(Cell);
extern  Text   unStructSel(Cell);
extern  Text   mkStructSel(Text);

extern  Bool   tryOExecute(Type);

extern  Void   staticAnalysis(Int);
extern  Void   tyconDefn(Int,Cell,Cell,Cell,List);
extern  Void   setTypeIns(List);
extern  Void   clearTypeIns(Void);
extern  Type   fullExpand(Type);
extern  Bool   isAmbiguous(Type);
extern  Void   ambigError(Int,String,Cell,Type);
extern  Void   classDefn(Int,Cell,Cell);
extern  Void   instDefn(Int,Cell,Cell);
extern  Void   addTupInst(Class,Int);
extern  Void   addEvalInst(Int,Cell,Int,List);
extern  Void   primDefn(Cell,List,Cell);
extern  Void   defaultDefn(Int,List);
extern  Void   checkExp(Void);
extern  Void   checkDefns(Void);

extern  Void   typeChecker(Int);
extern  Type   typeCheckExp(Bool);
extern  Cell   getDictFor(Class,Type);
extern  Void   typeCheckDefns(Void);
extern  Bool   updateVariance(Tycon);
extern  Void   initVariance(Tycon);
extern  Int    getVarianceAt(Type,Int);
extern  Char   varianceSym(Int);
extern  Bool   equalSchemes(Type,Type);
extern  Type   connectAxioms(Tycon,Type,Type);
extern  Type   liftedType(Name,Tycon);
extern  Cell   rhsExpr(Cell);
extern  Int    rhsLine(Cell);
extern  Int    bindingsLine(List);
extern  Bool   typeMatches(Type,Type);
extern  Cell   superEvid(Cell,Class,Class);
extern  Bool   mtInst(Class,Type);
extern  Cell   makeDictFor(Cell,Cell);
extern  Void   linkPreludeTC(Void);
extern  Void   linkPreludeCM(Void);
extern  Void   kindTCGroup(List);
extern  Void   kindSigType(Int,Type);
extern  Void   kindInst(Inst,Cell);
extern  Void   kindDefaults(Int,List);

extern  Void   compiler(Cell);
extern  Void   compileDefns(Void);
extern  Void   compileExp(Void);
extern  Bool   failFree(Cell);
extern  Int    discrArity(Cell);

extern  Void   machine(Int);
extern  Addr   codeGen(Name,Int,Cell);
extern  Void   implementCfun(Name,List);;
extern  Void   implementSfun(Name);
extern  Void   externalPrim(Name,String);
extern  Void   addCfunTable(Tycon);
extern  Name   succCfun(Name);
extern  Name   nextCfun(Name,Name);
extern  Name   cfunByNum(Name,Int);
extern  Void   unwind(Cell);
extern  Void   eval(Cell);
extern  Cell   evalWithNoError(Cell);
extern  Void   evalFails(StackPtr);
extern  Void   graphForExp(Void);

extern  Void   builtIn(Int);
extern  Void   abandon(String,Cell);
extern  Void   outputString(FILE *);
extern  Void   dialogue(Cell);
extern  Cell   consChar(Char);
#if BIGNUMS
extern  Bignum bigInt(Int);
extern  Bignum bigDouble(double);
extern  Bignum bigNeg(Bignum);
extern  Cell   bigToInt(Bignum);
extern  Cell   bigToFloat(Bignum);
extern  Bignum bigStr(String);
extern  Cell   bigOut(Bignum,Cell,Bool);
extern  Bignum bigShift(Bignum,Int,Int);
extern  Int    bigCmp(Bignum,Bignum);
#endif

extern  Void   machdep(Int);
extern  String findPathname(String,String);
#if PROFILING
extern  String timeString(Void);
#endif

extern  Bool   startEdit(Int,String);
extern  Int    shellEsc(String);
extern  Int    getTerminalWidth(Void);
extern  Void   normalTerminal(Void);
extern  Void   noechoTerminal(Void);
extern  Int    readTerminalChar(Void);
extern  Void   gcStarted(Void);
extern  Void   gcScanning(Void);
extern  Void   gcRecovered(Int,Int);
extern  Void   gcCStack(Void);

extern  Void   evalExp(Void);

/*-------------------------------------------------------------------------*/
