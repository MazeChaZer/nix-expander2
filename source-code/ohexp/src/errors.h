/* --------------------------------------------------------------------------
 * errors.h:    Copyright (c) Mark P Jones 1991-1998.   All rights reserved.
 *              See NOTICE for details and conditions of use etc...
 *              Hugs version 1.3b, January 1998
 *
 * Error handling support functions
 * ------------------------------------------------------------------------*/

#define ERRMSG(l)	 errHead(l);fprintf(stdout,
#define EEND       	 ); errFail()
#define ETHEN		 );
#define ERRTEXT		 fprintf(stdout,
#define ERREXPR(e)	 printExp(stdout,e);
#define ERRTYPE(e)	 printType(stdout,e);
#define ERRCONTEXT(qs)   printContext(stdout,qs);
#define ERRPRED(pi)      printPred(stdout,pi);
#define ERRKIND(k)	 printKind(stdout,k);
#define ERRAXIOM(tc,ax)  printAxiom(stdout,tc,ax);

extern Void errHead(Int);             /* in main.c           */
extern Void errFail(Void);
extern Void errAbort(Void);

extern sigProto(breakHandler);

extern Bool breakOn(Bool);		   	   /* in machdep.c	      */

extern Void printExp(FILE *,Cell);     /* in output.c         */
extern Void printType(FILE *,Cell);
extern Void printContext(FILE *,List);
extern Void printPred(FILE *,Cell);
extern Void printKind(FILE *,Kind);
extern Void printVariance(FILE *,List);
extern Void printAxiom(FILE *,Tycon,Type);
extern Void printAxioms(FILE *,Tycon);

/*-------------------------------------------------------------------------*/
