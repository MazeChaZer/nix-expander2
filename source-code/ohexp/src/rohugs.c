

#define NO_MAIN
#include "hugs.c"
#undef NO_MAIN


int main(int    argc, char* argv[])
{
	Name n;

	Int errorNumber = setjmp(catch_error);

	gArgc = argc;
	gArgv = argv;

	if(errorNumber) {
		fflush(stderr);
		doexit(1);
	}

	if (argc < 2) {
		fprintf(stderr,"rohugs: missing file argument\n");
		fflush(stderr);
		doexit(1);
	}

	CStackBase = &argc;   
	listFiles = FALSE;
	hugsPath      = strCopy(fromEnv("OHUGSPATH",NULL));
	scriptFile    = 0;
	numScripts    = 0;
	namesUpto     = 0;
	everybody(INSTALL);

	addScriptName(STD_PRELUDE,TRUE);
	addScriptName(argv[1],TRUE);
	readScripts(numScripts);

	if (isNull(n = findName(findText("main")))) {
		fprintf(stderr,"rohugs: no main in %s\n",argv[1]);
		fflush(stderr);
		doexit(1);}
	else {
		inputExpr = n;
		push(n);
		breakOn(TRUE);	
		if(!tryOExecute(typeCheckExp(FALSE))) {
			fprintf(stderr,"rohugs: Illegal type of main in %s\n",argv[1]);
			fflush(stderr);
			doexit(1);
		}
	}
	doexit(0);
}


