/* --------------------------------------------------------------------------
* prelude.h:   Copyright (c) Mark P Jones 1991-1998.   All rights reserved.
*              See NOTICE for details and conditions of use etc...
*              Hugs version 1.3b, January 1998
*
* Basic data type definitions, prototypes and standard macros including
* machine dependent variations...
* ------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
* To select a particular machine/compiler, just place a 1 in the appropriate
* position in the following list and ensure that 0 appears in all other
* positions:
*
* The letters UN in the comment field indicate that I have not personally
* been able to test this configuration yet and I have not heard from anybody
* else that has tried it.  If you run Hugs on one of these systems and it
* works (or needs patches) please let me know so that I can fix it and
* update the source.
*-------------------------------------------------------------------------*/

#define SOLARIS  0  /* For Solaris 2.4				           */
#define DJGPP2   0  /* For DJGPP version 2				   */
#define CYGWIN   0  /* For Windows 32, CygWin                              */
#define MSYS2    0  /* For Windows Msys2                                   */
#define VC32	 0  /* For Microsoft Visual C++ 2.0 upwards		   */
#define HPUX     0  /* For HPUX using gcc				UN */
#define LINUX    0  /* For Linux using gcc				UN */
#define RISCOS   0  /* For Acorn DesktopC and RISCOS2 or 3		UN */
#define AIX	 0  /* For IBM AIX on RS/6000 using GCC		        UN */
#define NETBSD	 0  /* For NetBSD-current				UN */
#define MACOSX   0  /* For MacOS X using cc (gcc)                          */
#define FREEBSD  0  /* FreeBSD 11.2-RELEASE                                */

/*---------------------------------------------------------------------------
* To add a new machine/compiler, add a new macro line above, add the new
* to the appropriate flags below and add a `machine specific' section in the
* following section of this file.  Please send me details of any new machines
* or compilers that you try so that I can pass them onto others!
*
*   UNIX           if the machine runs fairly standard Unix.
*   JMPBUF_ARRAY   if jmpbufs can be treated like arrays.
*   DOS_IO         to use DOS style IO for terminal control.
*   TERMIO_IO      to use Unix termio for terminal control.
*   SGTTY_IO       to use Unix sgtty for terminal control.
*   TERMIOS_IO	    to use posix termios for terminal control.
*   HASKELL_ARRAYS to include support for Haskell array primitives.
*   FLAT_ARRAYS    to use a flat array representation, if possible.
*   IO_MONAD	    to include the IO monad primitives and support.
*   IO_REFS	    Ref type for IO_MONAD, and simple operations.
*   FLUSHEVERY	    to force a fflush after every char in putStr/hPutStr.
*   LAZY_ST	    to include support for lazy state threads.
*   NPLUSK	    to include support for (n+k) patterns.
*   BIGNUMS	    to include support for Integer bignums.
*   FIXED_SUBST    to force a fixed size for the current substitution.
*   DYN_COMPS	    to allocate tables dynamically, currently just a memory
*		    saving trick, but this may be extended at a later stage
*		    to allow at least some of the tables to be extended
*		    dynamically at run-time to avoid exhausted space errors.
*   PROFILING	    to include support for profiling; WARNING: this can have
*		    a significant, adverse effect on runtime performance.
*   HASKELL_SYSTEM additional primitive functions for Haskell's System module.
*-------------------------------------------------------------------------*/

#define WINDOWS		(VC32 | MSYS2) /*(| CYGWIN)*/
#define UNIX		(HPUX | LINUX | AIX | NETBSD | SOLARIS | CYGWIN | MACOSX | FREEBSD)
#define LARGE_HUGS	1
#define JMPBUF_ARRAY	(UNIX   | DJGPP2 | RISCOS)
#define DOS_IO		(DJGPP2 | WINDOWS)
#define TERMIO_IO	(LINUX  | HPUX | CYGWIN)
#define SGTTY_IO	AIX
#define TERMIOS_IO      (NETBSD | SOLARIS | MACOSX | FREEBSD)



#define OBJ		    1 /* Object-oriented extensions                    */
#define O_TK            1 /* Tcl/Tk extension (needs OBJ)                  */
#define O_TIX           0 /* Tix extension (needs O_TK)                    */
#define O_IP		1 /* TCP/UDP/IP support (needs OBJ)		   */
#define MIDI            1 /* MIDI support				   */
#define HASKELL_ARRAYS	1
#define FLAT_ARRAYS	0 /* Warning: Setting 1 is not currently supported */
#define IO_MONAD	1 | OBJ
#define IO_REFS		1 /* Experimental IO Ref type			   */
#define FLUSHEVERY	(DJGPP2)
#define LAZY_ST		(IO_MONAD)
#define NPLUSK		1 /* Warning: There are those that would prefer 0  */
#define BIGNUMS		1 /* Experimental bignum implementation		   */
#define FIXED_SUBST	0 /* Warning: This may not be appropriate for PCs  */
#define PROFILING	0 /* Simplistic, incomplete, producer heap profiler*/
#define HASKELL_SYSTEM 1 /* Experimental System module implementation */

/*--------------------------------------------------------
* Fix for incompatibilities of Visual Studio's ISO/ANSI-C
* with POSIX-C standarts.
*----------------------------------------------------------*/
#if WINDOWS
#define _CRT_NONSTDC_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <stdio.h>
#define USE_INTERP_RESULT 1


/*---------------------------------------------------------------------------
* The following flags should be set automatically according to builtin
* compiler flags, but you might want to set them manually to avoid default
* behaviour in some situations:
*-------------------------------------------------------------------------*/

#ifdef  __GNUC__			/* look for GCC 2.x extensions	   */
#if     __GNUC__ >= 2	/* NeXT cc lies and says it's 2.x  */
#define GCC_THREADED 1

/* WARNING: if you use the following optimisations to assign registers for
* particular global variables, you should be very careful to make sure that
* storage(RESET) is called after a longjump (usually resulting from an error
* condition) and before you try to access the heap.  The current version of
* main deals with this using everybody(RESET) at the head of the main read,
* eval, print loop
*/

#ifdef  m68k				/* global registers on an m68k	   */
#define GLOBALfst	asm("a4")
#define GLOBALsnd	asm("a5")
#define GLOBALsp	asm("a3")
#endif 

#ifdef  sparc				/* global registers on a sparc	   */
/* sadly, although the gcc documentation suggests that the following reg   */
/* assignments should be ok, experience shows (at least on Suns) that they */
/* are not -- it seems that atof() and friends spoil things.		   */
/*#define GLOBALfst	asm("g5")*/
/*#define GLOBALsnd	asm("g6")*/
/*#define GLOBALsp	asm("g7")*/
#endif

#endif
#endif

#ifndef GCC_THREADED
#define GCC_THREADED 0
#endif

/*---------------------------------------------------------------------------
* Machine specific sections:
* Include any machine specific declarations and define macros:
*   local              prefix for locally defined functions
*   far                prefix for far pointers
*   allowBreak()       call to allow user to interrupt computation
*   FOPEN_WRITE        fopen *text* file for writing
*   FOPEN_APPEND       fopen *text* file for append
*   FOPEN_READ         fopen *text* file for read
*
* N.B. `far' must be explicitly defined (usually to the empty string)
*-------------------------------------------------------------------------*/

#if     SOLARIS
#include <malloc.h>
#define far
#define farCalloc(n,s)	(Void *)valloc(((unsigned)n)*((unsigned)s))
#endif

#if FREEBSD
#include <sys/stat.h>
#endif

#if	WINDOWS
#include <string.h>
#include <malloc.h>
#include <io.h>
#include <stdlib.h>
#include <direct.h>
#ifndef _STDLIB_H
#define _STDLIB_H
#endif
#define USE_READLINE 0
#define local
#define far
#define ctrlbrk(bh)	signal(SIGINT,bh); signal(SIGBREAK,bh)
#define allowBreak()	if (broken) { broken=FALSE; sigRaise(breakHandler); }
#endif

#if	VC32
extern void ignoreBreak(int);
#endif

#if     (HPUX | DJGPP2 | LINUX | AIX | NETBSD | CYGWIN | MACOSX | FREEBSD)
#include <string.h>
#include <stdlib.h>
#define  far
#endif

#if CYGWIN
#define USE_READLINE 0
#endif

#if	RISCOS
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#define  far
#define  isascii(c)	(((unsigned)(c))<128)
#define  Main		int
#define  MainDone	return 0;/*NOTUSED*/
extern   int access(char *, int);
extern   int namecmp(char *, char *);
#endif

#ifndef USE_READLINE
#define USE_READLINE  1
#endif
#ifndef allowBreak
#define allowBreak()
#endif
#ifndef local
#define local
#endif
#ifndef farCalloc
#define farCalloc(n,s)	   (Void *)calloc(((unsigned)n),((unsigned)s))
#endif
#ifndef FOPEN_WRITE
#define FOPEN_WRITE	   "w"
#endif
#ifndef FOPEN_APPEND
#define FOPEN_APPEND	   "a"
#endif
#ifndef FOPEN_READ
#define FOPEN_READ	   "r"
#endif
#ifndef sigProto
#define sigProto(nm)	   Void nm(int)
#define sigRaise(nm)	   nm(1)
#define sigHandler(nm)	   Void nm(int sig_arg)
#define sigResume	   return
#endif
/* to cope with systems that don't like	   */
/* main to be declared as returning Void   */
#ifndef Main
#define Main		   int
#endif
#ifndef MainDone
#define MainDone
#endif
#ifndef strCompare
#define strCompare	   strcmp
#endif

#if (LINUX | CYGWIN)
#  define ctrlbrk(bh)	   signal(SIGINT,bh); sigsetmask(0);
#elif (UNIX | DJGPP2 | RISCOS)
#  define ctrlbrk(bh)	   signal(SIGINT,bh);
#endif

/*---------------------------------------------------------------------------
* General settings:
*-------------------------------------------------------------------------*/
typedef void Void;
typedef int Bool;
#define TRUE     1
#define FALSE    0
typedef char    *String;
typedef int      Int;
typedef long     Long;
typedef int      Char;
typedef unsigned Unsigned;

#ifndef STD_PRELUDE
#if     RISCOS
#define STD_PRELUDE	   "prelude"
#else
#define STD_PRELUDE	   "Prelude.hs"
#endif
#endif

#define NUM_SYNTAX         100
#define NUM_SELECTS        100
#if IO_MONAD
#define NUM_HANDLES	       20
#endif
#define NUM_SCRIPTS        64
#define NUM_FIXUPS         100
#define NUM_TUPLES         100
#define NUM_OFFSETS        2048
#define NUM_CHARS          256

#if PROFILING
#define DEF_PROFINTDIV	   10		/* hpsize/this cells between samples*/
#endif

#define NUM_TYCON          560
#define NUM_NAME           16000
#define NUM_CLASSES        140
#define NUM_INSTS          1000
#define NUM_DICTS          32000
#define NUM_FLAT	       32000
#define NUM_TEXT           80000
#define NUM_TEXTH	       10
#define NUM_TYVARS         64000
#define NUM_STACK          512000
#define NUM_ADDRS          1280000
#define MINIMUMHEAP	       7500
#define MAXIMUMHEAP	       0
#define DEFAULTHEAP        20000000
#define MAXPOSINT          2147483647
#define NUM_DTUPLES	       5
#define BIGBASE		       10000
#define BIGEXP		       4

#define DECTABLE(tab)	   tab[]
#define DEFTABLE(tab,sz)   tab[sz]

#define minRecovery	   1000
#define bitsPerWord	   32
#define wordShift	   5
#define wordMask	   31

#define bitArraySize(n)    ((n)/bitsPerWord + 1)
#define placeInSet(n)      ((-(n)-1)>>wordShift)
#define maskInSet(n)       (1<<((-(n)-1)&wordMask))

#ifndef __GNUC__
#if !RISCOS && !WINDOWS
extern Int      strcmp(String, String);
extern Int      strlen(String);
extern char	*strcpy(String,String);
extern char     *strcat(String,String);
#endif
#endif
#if !LINUX && !WINDOWS
extern char	*getenv(const char *);
extern int      system(const char *);
extern int 	chdir(const char *);
extern double   atof(const char *);
#endif
#if !WINDOWS
extern char * strchr(const char *,int);  /* test membership in str  */
#endif
#if !DJGPP2 && !WINDOWS
extern Void     exit(Int);
#endif
extern Void     internal(String);
extern Void     fatal(String);

#define FloatImpType	   float

/* type to use in prototypes		   */
/* strictly ansi (i.e. gcc) conforming  */
/* but breaks data hiding :-(	   */
#define FloatPro	   double
#define FloatFMT	   "%g"

#ifndef FILENAME_MAX	   /* should already be defined in an ANSI compiler*/
#define FILENAME_MAX 256
#else
#if     FILENAME_MAX < 256
#undef  FILENAME_MAX
#define FILENAME_MAX 256
#endif
#endif

/*-------------------------------------------------------------------------*/