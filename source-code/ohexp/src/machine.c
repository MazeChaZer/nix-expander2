/* --------------------------------------------------------------------------
* machine.c:   Copyright (c) Mark P Jones 1991-1998.   All rights reserved.
*              See NOTICE for details and conditions of use etc...
*              Hugs version 1.3b, January 1998
*
* Graph reduction engine, code generation and execution
* ------------------------------------------------------------------------*/

#include "prelude.h"
#include "storage.h"
#include "connect.h"
#include "errors.h"
#include <setjmp.h>

/*#define DEBUG_CODE*/
Bool   failOnError   = TRUE;		/* TRUE => abort as soon as error  */
/*	   occurs		   */

/* --------------------------------------------------------------------------
* Data structures for machine memory (program storage):
* ------------------------------------------------------------------------*/

/* This list defines the sequence of all instructions that can be used in
* the abstract machine code for Hugs.  The Ins() macro is used to
* ensure that the correct mapping of instructions to labels is used when
* compiling the GCC_THREADED version.
*/

#define INSTRLIST	Ins(iLOAD),  Ins(iCELL),   Ins(iCHAR),	  \
	Ins(iINT),   Ins(iFLOAT),  Ins(iSTRING),  \
	Ins(iMKAP),  Ins(iUPDATE), Ins(iUPDAP),	  \
	Ins(iEVAL),  Ins(iRETURN), Ins(iTEST),	  \
	Ins(iGOTO),  Ins(iSETSTK), Ins(iROOT),	  \
	Ins(iDICT),  Ins(iFAIL),   Ins(iALLOC),	  \
	Ins(iSLIDE), Ins(iSTAP),   Ins(iTABLE),   \
	Ins(iLEVAL), Ins(iRUPDAP), Ins(iRUPDATE)

#define Ins(x) x
typedef enum { INSTRLIST } Instr;
#undef  Ins

typedef Int Label;

typedef union {
	Int   mint;
	Float mfloat;
	Cell  cell;
	Text  text;
	Addr  addr;
	Instr instr;
	Label lab;
} MemCell;

typedef MemCell far *Memory;
static	Memory	    memory;
#define intAt(m)    memory[m].mint
#define floatAt(m)  memory[m].mfloat
#define cellAt(m)   memory[m].cell
#define textAt(m)   memory[m].text
#define addrAt(m)   memory[m].addr
#define instrAt(m)  memory[m].instr
#define labAt(m)    memory[m].lab

/* --------------------------------------------------------------------------
* Local function prototypes:
* ------------------------------------------------------------------------*/

static Void  local instrNone(Instr);
static Void  local instrInt(Instr,Int);
static Void  local instrFloat(Instr,FloatPro);
static Void  local instrCell(Instr,Cell);
static Void  local instrText(Instr,Text);
static Void  local instrLab(Instr,Label);
static Void  local instrCellLab(Instr,Cell,Label);

static Void  local asSTART(Void);
static Label local newLabel(Label);
static Void  local asEND(Void);
static Void  local asEVAL(Void);
static Void  local asTEST(Cell,Label);
static Void  local asDICT(Int);
static Void  local asSLIDE(Int);
static Void  local asMKAP(Int);
static Void  local asUPDATE(Int);
static Void  local asRUPDATE(Void);
static Void  local asGOTO(Label);

#ifdef DEBUG_CODE
static Void  local dissassemble(Addr,Addr);
static Void  local printCell(Cell);
static Addr  local dissNone(Addr,String);
static Addr  local dissInt(Addr,String);
static Addr  local dissFloat(Addr,String);
static Addr  local dissCell(Addr,String);
static Addr  local dissText(Addr,String);
static Addr  local dissAddr(Addr,String);
static Addr  local dissIntAddr(Addr,String);
static Addr  local dissCellAddr(Addr,String);
#endif

static Void  local build(Cell,Int);
static Void  local buildGuards(List,Int);
static Int   local buildLoc(List,Int);
static Void  local buildBignum(Bignum);

static Void  local make(Cell,Int,Label,Label);
static Void  local makeCond(Cell,Cell,Cell,Int,Label,Label);
static Void  local makeNumcase(Triple,Int,Label,Label);
static Void  local testGuard(Pair,Int,Label,Label,Label);
static Void  local testCase(Pair,Int,Label,Label,Label);

static Void  local analyseAp(Cell);
static Void  local buildAp(Cell,Int,Label,Bool);

static Void  local evalString(Cell);
static Void  local run(Addr,StackPtr);

/* --------------------------------------------------------------------------
* Assembler: (Low level, instruction code storage)
* ------------------------------------------------------------------------*/

static Addr  startInstr;		/* first instruction after START   */
static Addr  lastInstr;			/* last instr written (for peephole*/
/* optimisations etc.)		   */
static Addr  noMatch;			/* address of a single FAIL instr  */
static Int   srsp;			/* simulated runtime stack pointer */
static Int   offsPosn[NUM_OFFSETS];	/* mapping from logical to physical*/
/* offset positions		   */

static Void local instrNone(Instr opc)	/* Opcode with no operands	   */
{
	lastInstr	       = getMem(1);
	instrAt(lastInstr) = opc;
}

static Void local instrInt(	Instr opc, Int   n)	/* Opcode with integer operand	   */
{
	lastInstr	       = getMem(2);
	instrAt(lastInstr) = opc;
	intAt(lastInstr+1) = n;
}

static Void local instrFloat(Instr    opc, FloatPro fl)	/* Opcode with Float operand	   */
{
	lastInstr            = getMem(2);
	instrAt(lastInstr)   = opc;
	floatAt(lastInstr+1) = (Float)fl;
}

static Void local instrCell(Instr opc, Cell  c)	/* Opcode with Cell operand	   */
{
	lastInstr		= getMem(2);
	instrAt(lastInstr)	= opc;
	cellAt(lastInstr+1) = c;
}

static Void local instrText(Instr opc, Text  t)	/* Opcode with Text operand	   */
{
	lastInstr		= getMem(2);
	instrAt(lastInstr)	= opc;
	textAt(lastInstr+1) = t;
}

static Void local instrLab(Instr opc, Label l)	/* Opcode with label operand	   */
{
	lastInstr	       = getMem(2);
	instrAt(lastInstr) = opc;
	labAt(lastInstr+1) = l;
	if (l<0)
		internal("bad Label");
}

static Void local instrCellLab(Instr opc, Cell  c, Label l)	/* Opcode with cell, label operands*/
{
	lastInstr		= getMem(3);
	instrAt(lastInstr)	= opc;
	cellAt(lastInstr+1) = c;
	labAt(lastInstr+2)	= l;
	if (l<0)
		internal("bad Label");
}

/* --------------------------------------------------------------------------
* Main low level assembler control: (includes label assignment and fixup)
*
* Labels are used as a simple form of continuation during the code gen:
*  RUNON    => produce code which does not make jump at end of construction
*  UPDRET   => produce code which performs RUPDATE at end
*  VALRET   => produce code which performs RETURN at end
*  other(d) => produce code which branches to label d at end
* ------------------------------------------------------------------------*/

static	Label	      nextLab;	       /* next label number to allocate    */
#define SHOULDNTFAIL  (-1)
#define RUNON	      (-2)
#define UPDRET	      (-3)
#define VALRET	      (-4)
static	Addr	      fixups[NUM_FIXUPS]; /* fixup table maps Label -> Addr*/
#define atLabel(n)    fixups[n] = getMem(0)
#define endLabel(d,l) if (d==RUNON) atLabel(l)
#define fix(a)	      addrAt(a) = fixups[labAt(a)]

static Void local asSTART() {	       /* initialise assembler		   */
	fixups[0]	= noMatch;
	nextLab	= 1;
	startInstr	= getMem(0);
	lastInstr	= startInstr-1;
	srsp	= 0;
	offsPosn[0] = 0;
}

static Label local newLabel(Label d)	       /* allocate new label		   */
{
	if (d==RUNON) {
		if (nextLab>=NUM_FIXUPS) {
			ERRMSG(0) "Compiled code too complex"
				EEND;
		}
		return nextLab++;
	}
	return d;
}

static Void local asEND() {	       /* Fix addresses in assembled code  */
	Addr pc = startInstr;

	while (pc<=lastInstr)
		switch (instrAt(pc)) {
		case iEVAL	 :	       /* opcodes taking no arguments	   */
		case iFAIL	 :
		case iSTAP	 :
		case iRUPDATE:
		case iRUPDAP :
		case iRETURN : pc++;
			break;

		case iGOTO	 : fix(pc+1);  /* opcodes taking one argument	   */
		case iSETSTK :
		case iALLOC  :
		case iSLIDE  :
		case iROOT	 :
		case iDICT   :
		case iLOAD	 :
		case iLEVAL  :
		case iCELL	 :
		case iCHAR	 :
		case iINT	 :
		case iFLOAT  :
		case iSTRING :
		case iMKAP	 :
		case iUPDATE :
		case iUPDAP  : pc+=2;
			break;

		case iTEST	 : fix(pc+2);
			pc+=3;
			break;

		default	 : internal("fixAddrs");
	}
}

/* --------------------------------------------------------------------------
* Assembler Opcodes: (includes simple peephole optimisations)
* ------------------------------------------------------------------------*/

#define asINTEGER(n) instrInt(iINT,n);		srsp++
#define asFLOAT(fl)  instrFloat(iFLOAT,fl);	srsp++
#define asSTRING(t)  instrText(iSTRING,t);	srsp++
#define asCHAR(n)    instrInt(iCHAR,n);		srsp++
#define asLOAD(n)    instrInt(iLOAD,n);		srsp++
#define asALLOC(n)   instrInt(iALLOC,n);	srsp+=n
#define asROOT(n)    instrInt(iROOT,n);		srsp++
#define asSETSTK(n)  instrInt(iSETSTK,n);	srsp=n
#define asSTAP()     instrNone(iSTAP);	        srsp--
#define asRETURN()   instrNone(iRETURN)
#define asCELL(c)    instrCell(iCELL,c);	srsp++
#define asFAIL()     instrNone(iFAIL)

static Void local asEVAL() {		/* load and eval stack element	   */
	if (instrAt(lastInstr)==iLOAD)
		instrAt(lastInstr) = iLEVAL;
	else
		instrNone(iEVAL);
	srsp--;
}

static Void local asTEST(Cell  c, Label l)		/* test whnf and branch on mismatch*/
{
	switch (whatIs(c)) {
		/* typing guarantees that tags will*/
		/* match without further tests	   */
	case TUPLE   : return;
	case NAME    : if (isCfun(c) && cfunOf(c)==0)
					   return;
	}
	instrCellLab(iTEST,c,l);
}

static Void local asDICT(Int n)		/* pick element of dictionary	   */
{
	/* Sadly, the following optimisation cannot be used unless CELL references
	* in compiled code are garbage collected (and possibly modified when cell  
	* indirections are found):  CELL {dict m}; DICT n ==> CELL {dict (m+n)}
	*/
	if (instrAt(lastInstr)==iCELL) {	/* Weaker version of above opt.    */
		Cell c = dictGet(cellAt(lastInstr+1),n);
		if (!isPair(c)) {
			cellAt(lastInstr+1) = c;
			return;
		}
	}
	if (n!=0)				/* optimisation:DICT 0 has no use  */
		instrInt(iDICT,n);		/* for std dictionary construction */
}

static Void local asSLIDE(Int n)		/* Slide results down stack	   */
{


	if (instrAt(lastInstr)==iSLIDE)	/* Peephole optimisation:	   */
		intAt(lastInstr+1)+=n;		/* SLIDE n;SLIDE m ===> SLIDE (n+m)*/
	else
		instrInt(iSLIDE,n);
	srsp -= n;
}

static Void local asMKAP(Int n)		/* Make application nodes ...	   */
{
	if (instrAt(lastInstr)==iMKAP)	/* Peephole optimisation:	   */
		intAt(lastInstr+1)+=n;		/* MKAP n; MKAP m  ===> MKAP (n+m) */
	else
		instrInt(iMKAP,n);
	srsp -= n;
}

static Void local asUPDATE(Int n)		/* Update node ...		   */
{
	if (instrAt(lastInstr)==iMKAP) {	/* Peephole optimisations:	   */
		Int m = intAt(lastInstr+1);
		nextInstr(lastInstr);
		if (m==1)			/* MKAP 1; UPDATE p ===> UPDAP p   */
			instrInt(iUPDAP,n);
		else {				/* MKAP m; UPDATE p		   */
			instrInt(iMKAP,m-1);	/*	 ===> MKAP (m-1); UPDAP p  */
			instrInt(iUPDAP,n);
		}
	}
	else
		instrInt(iUPDATE,n);
	srsp--;
}

static Void local asRUPDATE() {		/* Update node and return ...	   */
	if (instrAt(lastInstr)==iMKAP) {	/* Peephole optimisations:	   */
		Int m = intAt(lastInstr+1);
		nextInstr(lastInstr);
		if (m==1)			/* MKAP 1; RUPDATE ===> RUPDAP     */
			instrNone(iRUPDAP);
		else {				/* MKAP m; RUPDATE		   */
			instrInt(iMKAP,m-1);	/*	 ===> MKAP (m-1); RUPDAP   */
			instrNone(iRUPDAP);
		}
	}
	else
		instrNone(iRUPDATE);
}

/* End evaluation of expr in manner*/
/* indicated by label l		   */
static Void local asGOTO(Label l)
{
	switch (l) {					/* inaccurate srsp */
	case UPDRET : asRUPDATE();
		break;
	case VALRET : asRETURN();
	case RUNON  : break;
	default     : instrLab(iGOTO,l);
		break;
	}
}

/* --------------------------------------------------------------------------
* Constructor function tables:
*
* Tables of constructor functions for enumerated types are needed to
* produce derived instances.
* ------------------------------------------------------------------------*/

/* Add a constructor fun table to  */
/* constructors for tycon tc	   */
Void addCfunTable(Tycon tc)
{
	if (isTycon(tc) && tycon(tc).what==DATATYPE) {
		List cs = tycon(tc).defn;
		if (nonNull(cs) && nonNull(tl(cs)) && name(hd(cs)).code<=0) {
			Int  l     = length(cs);
			Addr a     = getMem(2+l);
			instrAt(a) = iTABLE;
			intAt(a+1) = l;
			for (l=0; nonNull(cs); l++, cs=tl(cs)) {
				cellAt(a+l+2)     = hd(cs);
				name(hd(cs)).code = a;
			}
		}
	}
}

/* get next constructor	in sequence*/
/* or NIL, if none		   */
Name succCfun(Name n)
{
	if (cfunOf(n)==0)
		return NIL;
	else {
		Int  d = cfunOf(n)+1;
		Addr a = name(n).code;
		return (d>intAt(a+1)) ? NIL : cellAt(a+d+1);
	}
}

/* get next constructor	in series  */
/* or NIL, if none		   */
Name nextCfun(Name n1, Name n2)
{
	/* For product constructors, the   */
	/* only possibility is n1 == n2	   */
	if (cfunOf(n1)==0)
		return n1;
	else {
		Int  d = 2*cfunOf(n2) - cfunOf(n1);
		Addr a = name(n1).code;
		return (d<=0 || d>intAt(a+1)) ? NIL : cellAt(a+d+1);
	}
}

/* get ith constructor (0<=i<m)    */
/* for enumerated datatype with a  */
/* representative cfun n	   */
Name cfunByNum(Name n, Int  i)
{
	if (cfunOf(n)==0)
		return i==0 ? n : NIL;
	else {
		Addr a = name(n).code;
		return (i>=0 && i<intAt(a+1)) ? cellAt(a+i+2) : NIL;
	}
}

/* --------------------------------------------------------------------------
* Dissassembler:
* ------------------------------------------------------------------------*/

#ifdef DEBUG_CODE
#define printAddr(a) printf("0x%04X",a)/* printable representation of Addr */

static Void local dissassemble(Addr pc, Addr end) /* print dissassembly of code	   */
{
	while (pc<=end) {
		printAddr(pc);
		printf("\t");
		switch (instrAt(pc)) {
		case iLOAD	 : pc = dissInt(pc,"LOAD");	 break;
		case iLEVAL	 : pc = dissInt(pc,"LEVAL");	 break;
		case iCELL	 : pc = dissCell(pc,"CELL");	 break;
		case iCHAR	 : pc = dissInt(pc,"CHAR");	 break;
		case iINT	 : pc = dissInt(pc,"INT");	 break;
		case iFLOAT  : pc = dissFloat(pc,"FLOAT");   break;
		case iSTRING : pc = dissText(pc,"STRING");	 break;
		case iMKAP	 : pc = dissInt(pc,"MKAP");	 break;
		case iUPDATE : pc = dissInt(pc,"UPDATE");	 break;
		case iRUPDATE: pc = dissNone(pc,"RUPDATE");	 break;
		case iUPDAP  : pc = dissInt(pc,"UPDAP");	 break;
		case iRUPDAP : pc = dissNone(pc,"RUPDAP");	 break;
		case iEVAL	 : pc = dissNone(pc,"EVAL");	 break;
		case iSTAP   : pc = dissNone(pc,"STAP");	 break;
		case iRETURN : pc = dissNone(pc,"RETURN");	 break;
		case iTEST	 : pc = dissCellAddr(pc,"TEST"); break;
		case iGOTO	 : pc = dissAddr(pc,"GOTO");	 break;
		case iSETSTK : pc = dissInt(pc,"SETSTK");	 break;
		case iALLOC  : pc = dissInt(pc,"ALLOC");	 break;
		case iSLIDE  : pc = dissInt(pc,"SLIDE");	 break;
		case iROOT	 : pc = dissInt(pc,"ROOT");	 break;
		case iDICT   : pc = dissInt(pc,"DICT");      break;
		case iFAIL	 : pc = dissNone(pc,"FAIL");	 break;
		case iTABLE  : pc = dissNone(pc,"TABLE");
			pc+= intAt(pc)+1;
			break;
		default	 : internal("unknown instruction");
		}
	}
}

static Void local printCell(Cell c)	       /* printable representation of Cell */
{
	if (isName(c))
		printf("%s",textToStr(name(c).text));
	else
		printf("$%d",c);
}

static Addr local dissNone(Addr   pc, String s)       /* dissassemble instr no args	   */
{
	printf("%s\n",s);
	return pc+1;
}

static Addr local dissInt(Addr   pc, String s)        /* dissassemble instr with Int arg  */
{
	printf("%s\t%d\n",s,intAt(pc+1));
	return pc+2;
}

static Addr local dissFloat(Addr   pc, String s)      /* dissassemble instr with Float arg*/
{
	printf("%s\t%s\n",s,floatToString((FloatPro)floatAt(pc+1)));
	return pc+2;
}

static Addr local dissCell(Addr   pc, String s)       /* dissassemble instr with Cell arg */
{
	printf("%s\t",s);
	printCell(cellAt(pc+1));
	printf("\n");
	return pc+2;
}

static Addr local dissText(Addr   pc, String s)       /* dissassemble instr with Text arg */
{
	printf("%s\t%s\n",s,textToStr(textAt(pc+1)));
	return pc+2;
}

static Addr local dissAddr(Addr   pc, String s)       /* dissassemble instr with Addr arg */
{
	printf("%s\t",s);
	printAddr(addrAt(pc+1));
	printf("\n");
	return pc+2;
}

static Addr local dissIntAddr(Addr   pc, String s)    /* dissassemble instr with Int/Addr */
{
	printf("%s\t%d\t",s,intAt(pc+1));
	printAddr(addrAt(pc+2));
	printf("\n");
	return pc+3;
}

static Addr local dissCellAddr(Addr   pc, String s)   /* dissassemble instr with Cell/Addr*/
{
	printf("%s\t",s);
	printCell(cellAt(pc+1));
	printf("\t");
	printAddr(addrAt(pc+2));
	printf("\n");
	return pc+3;
}
#endif

/* --------------------------------------------------------------------------
* Compile expression to code which will build expression without any
* evaluation.
* ------------------------------------------------------------------------*/

/* Generate code which will build  */
/* instance of given expression but*/
/* perform no evaluation 	   */
static Void local build(Cell e, Int  co)
{
	Int n;

	switch (whatIs(e)) {

	case LETREC    : n = buildLoc(fst(snd(e)),co);
		build(snd(snd(e)),co+n);
		asSLIDE(n);
		break;

	case FATBAR    : build(snd(snd(e)),co);
		build(fst(snd(e)),co);
		asCELL(nameFatbar);
		asMKAP(2);
		break;

	case COND      : build(thd3(snd(e)),co);
		build(snd3(snd(e)),co);
		build(fst3(snd(e)),co);
		asCELL(nameIf);
		asMKAP(3);
		break;

	case GUARDED   : buildGuards(snd(e),co);
		break;

	case AP        : buildAp(e,co,SHOULDNTFAIL,FALSE);
		break;

	case TUPLE     :
	case NAME      : asCELL(e);
		break;

#if BIGNUMS
	case ZERONUM   :
	case POSNUM    :
	case NEGNUM    : buildBignum(e);
		break;
#endif

	case DICTCELL  : asCELL(e);
		break;

	case INTCELL   : asINTEGER(intOf(e));
		break;

	case FLOATCELL : asFLOAT(floatOf(e));
		break;

	case STRCELL   : asSTRING(textOf(e));
		break;

	case CHARCELL  : asCHAR(charOf(e));
		break;

	case OFFSET    : asLOAD(offsPosn[offsetOf(e)]);
		break;

	default        : internal("build");
	}
}

/* Generate code to compile list   */
/* of guards to a conditional expr */
/* without evaluation		   */
static Void local buildGuards(List gs, Int  co)
{
	if (isNull(gs)) {
		asCELL(nameFail);
	}
	else {
		buildGuards(tl(gs),co);
		build(snd(hd(gs)),co);
		build(fst(hd(gs)),co);
		asCELL(nameIf);
		asMKAP(3);
	}
}

/* Generate code to build local var*/
/* bindings on stack,  with no eval*/
static Int local buildLoc(List vs, Int  co)
{
	Int n = length(vs);
	Int i;

	for (i=1; i<=n; i++)
		offsPosn[co+i] = srsp+i;
	asALLOC(n);
	for (i=1; i<=n; i++) {
		build(hd(vs),co+n);
		asUPDATE(offsPosn[co+i]);
		vs = tl(vs);
	}
	return n;
}

#if BIGNUMS
static Void local buildBignum(Bignum b)	/* Generate code to build bignum   */
{
	if (b==ZERONUM) {
		asCELL(ZERONUM);
	}
	else {
		List rs = snd(b) = rev(snd(b));
		asCELL(NIL);
		for (; nonNull(rs); rs=tl(rs)) {
			asCELL(hd(rs));
			asMKAP(1);
		}
		snd(b) = rev(snd(b));		/* put digits back in order	   */
		asCELL(fst(b));
		asMKAP(1);
	}
}
#endif

/* --------------------------------------------------------------------------
* Compile expression to code which will build expression evaluating guards
* and testing cases to avoid building complete graph.
* ------------------------------------------------------------------------*/

#define makeTests(ct,tests,co,f,d)     {   Label l1 = newLabel(d);	    \
	List  xs = tests;		    \
	while (nonNull(tl(xs))) {	    \
	Label l2   = newLabel(RUNON);\
	Int savesp = srsp;	    \
	ct(hd(xs),co,f,l2,l1);	    \
	atLabel(l2);		    \
	srsp = savesp;		    \
	xs   = tl(xs);		    \
	}				    \
	ct(hd(xs),co,f,f,d);		    \
	endLabel(d,l1);		    \
}

/* Construct code to build e, given */
/* current offset co, and branch	   */
/* to f on failure, d on completion */
static Void local make(Cell  e, Int   co, Label f, Label d)
{
	switch (whatIs(e)) {

	case LETREC    : {   Int n = buildLoc(fst(snd(e)),co);
		if (d==UPDRET || d==VALRET)
			make(snd(snd(e)),co+n,f,d);
		else {
			make(snd(snd(e)),co+n,f,RUNON);
			asSLIDE(n);
			asGOTO(d);
		}
					 }
					 break;

	case FATBAR    : {   Label l1     = newLabel(RUNON);
		Label l2     = newLabel(d);
		Int   savesp = srsp;

		make(fst(snd(e)),co,l1,l2);

		atLabel(l1);
		srsp = savesp;
		asSETSTK(srsp);
		make(snd(snd(e)),co,f,l2);

		endLabel(d,l2);
					 }
					 break;

	case COND      : makeCond(fst3(snd(e)),
						 snd3(snd(e)),
						 thd3(snd(e)),co,f,d);
		break;

	case NUMCASE   : makeNumcase(snd(e),co,f,d);
		break;

	case CASE      : make(fst(snd(e)),co,SHOULDNTFAIL,RUNON);
		asEVAL();
		makeTests(testCase,snd(snd(e)),co,f,d);
		break;

	case GUARDED   : makeTests(testGuard,snd(e),co,f,d);
		break;

	case AP        : {   Cell h = getHead(e);
		if (h==nameAnd && argCount==2) {
			/* x && y ==> if x then y else False	   */
			makeCond(arg(fun(e)),arg(e),nameFalse,co,f,d);
			break;
		}
		else if (h==nameOr && argCount==2) {
			/* x || y ==> if x then True else y	   */
			makeCond(arg(fun(e)),nameTrue,arg(e),co,f,d);
			break;
		}
					 }
					 buildAp(e,co,f,TRUE);
					 asGOTO(d);
					 break;

	case TUPLE     :
	case NAME      : asCELL(e);
		asGOTO(d);
		break;

	case DICTCELL  : asCELL(e);
		asGOTO(d);
		break;

#if BIGNUMS
	case ZERONUM   :
	case POSNUM    :
	case NEGNUM    : buildBignum(e);
		asGOTO(d);
		break;
#endif

	case INTCELL   : asINTEGER(intOf(e));
		asGOTO(d);
		break;

	case FLOATCELL : asFLOAT(floatOf(e));
		asGOTO(d);
		break;

	case STRCELL   : asSTRING(textOf(e));
		asGOTO(d);
		break;

	case CHARCELL  : asCHAR(charOf(e));
		asGOTO(d);
		break;

	case OFFSET    : asLOAD(offsPosn[offsetOf(e)]);
		asGOTO(d);
		break;

	default        : internal("make");
	}
}

/* Build code for conditional	   */
static Void local makeCond(Cell  i, Cell t, Cell e,
						   Int   co, Label f, Label d)
{
	Label l1 = newLabel(RUNON);
	Label l2 = newLabel(d);
	Int   savesp;

	make(i,co,f,RUNON);
	asEVAL();

	savesp = srsp;
	asTEST(nameTrue,l1);
	make(t,co,f,l2);

	srsp = savesp;
	atLabel(l1);
	make(e,co,f,(d==RUNON?d:l2));

	endLabel(d,l2);
}

/* Build code for numcase	   */
static Void local makeNumcase(Triple nc, Int    co, Label  f, Label d)
{
	Cell discr = snd3(nc);
	Cell h     = getHead(discr);
	make(fst3(nc),co,SHOULDNTFAIL,RUNON);
	switch (whatIs(h)) {
	case NAME   : if (h==nameFromInt) {
		asINTEGER(intOf(arg(discr)));
		make(arg(fun(discr)),co,SHOULDNTFAIL,RUNON);
		asCELL(namePmInt);
				  }
#if BIGNUMS
				  else if (h==nameFromInteger) {
					  buildBignum(arg(discr));
					  make(arg(fun(discr)),co,SHOULDNTFAIL,RUNON);
					  asCELL(namePmInteger);
				  }
#endif
				  else if (h==nameFromDouble) {
					  asFLOAT(floatOf(arg(discr)));
					  make(arg(fun(discr)),co,SHOULDNTFAIL,RUNON);
					  asCELL(namePmFlt);
				  }
				  asMKAP(3);
				  asEVAL();
				  asTEST(nameTrue,f);
				  make(thd3(nc),co,f,d);
				  break;
#if NPLUSK
	case ADDPAT : asINTEGER(snd(h));
		make(arg(discr),co,SHOULDNTFAIL,RUNON);
		asCELL(namePmNpk);
		asMKAP(3);
		asEVAL();
		asTEST(nameJust,f);
		offsPosn[co+1] = ++srsp;
		make(thd3(nc),co+1,f,d);
		--srsp;
		break;
#endif
	}
}

/* Produce code for guard	   */
static Void local testGuard(Pair  g, Int   co, Label f, Label cf, Label d)
{
	if (fst(g)!=nameTrue) {
		make(fst(g),co,SHOULDNTFAIL,RUNON);
		asEVAL();
		asTEST(nameTrue,cf);
	}
	make(snd(g),co,f,d);
}

/* Produce code for guard	   */
static Void local testCase(Pair  c,
						   Int   co,				/* labels determine where to go if:*/
						   Label f,				/* match succeeds, but rest fails  */
						   Label cf,				/* this match fails		   */
						   Label d)
{
	Int n = discrArity(fst(c));
	Int i;
	asTEST(fst(c),cf);
	for (i=1; i<=n; i++)
		offsPosn[co+i] = ++srsp;
	make(snd(c),co+n,f,d);
}

/* --------------------------------------------------------------------------
* We frequently encounter functions which call themselves recursively with
* a number of initial arguments preserved:
* e.g.  (map f) []	= []
*	 (map f) (x:xs) = f x : (map f) xs
* Lambda lifting, in particular, is likely to introduce such functions.
* Rather than reconstructing a new instance of the recursive function and
* its arguments, we can extract the relevant portion of the root of the
* current redex.
*
* The following functions implement this optimisation.
* ------------------------------------------------------------------------*/

static Int  nonRoots;		       /* #args which can't get from root  */
static Int  rootPortion;	       /* portion of root used ...	   */
static Name definingName;	       /* name of func being defined,if any*/
static Int  definingArity;	       /* arity of definingName 	   */

/* Determine if any portion of an   */
/* application can be built using a */
/* portion of the root		   */
static Void local analyseAp(Cell e)
{
	if (isAp(e)) {
		analyseAp(fun(e));
		if (nonRoots==0 && rootPortion>1
			&& isOffset(arg(e))
			&& offsetOf(arg(e))==rootPortion-1)
			rootPortion--;
		else
			nonRoots++;
	}
	else if (e==definingName)
		rootPortion = definingArity+1;
	else
		rootPortion = 0;
}

/* Build application, using root   */
/* optimization if possible	   */
static Void local buildAp(Cell  e, Int   co, Label f, Bool  str)
{
	Int nr, rp, i;

	nonRoots = 0;
	analyseAp(e);
	nr = nonRoots;
	rp = rootPortion;

	for (i=0; i<nr; ++i) {
		build(arg(e),co);
		e = fun(e);
	}

	if (isSelect(e)) {
		if (selectOf(e)>0) {
			asDICT(selectOf(e));
		}
	}
	else {
		if (isName(e) && isMfun(e)) {
			asDICT(mfunOf(e));
			nr--;	/* AP node for member function need never be built */
		}
		else {
			if (0<rp && rp<=definingArity) {
				asROOT(rp-1);
			}
			else
				if (str)
					make(e,co,f,RUNON);
				else
					build(e,co);
		}

		if (nr>0) {
			asMKAP(nr);
		}
	}
}

/* --------------------------------------------------------------------------
* Code generator entry points:
* ------------------------------------------------------------------------*/

/* Generate code for expression e,  */
/* treating return value of CAFs    */
/* differently to functs with args  */
Addr codeGen(Name n, Int  arity, Cell e)
{
	definingName  = n;
	definingArity = arity;
	asSTART();
	if (nonNull(n)) {
		Int i;
		for (i=1; i<=arity; i++)
			offsPosn[i] = ++srsp;
		make(e,arity,noMatch,(arity>0 ? UPDRET : VALRET));
	}
	else {
		build(e,0);
		asRETURN();
	}
	asEND();
#ifdef DEBUG_CODE
	if (nonNull(n))
		printf("name=%s\n",textToStr(name(n).text));
	dissassemble(startInstr,lastInstr);
	printf("------------------\n");
#endif
	if (nonNull(n))
		name(n).defn  = NIL;
	return startInstr;
}

/* Build implementation for sel	   */
/* function s			   */
Void implementSfun(Name s)
{
	List cns = name(s).defn;
	Int  a   = name(s).arity;
	asSTART();				/* inaccurate srsp doesn't matter  */
	asLOAD(1);				/* Eval main arg (i.e. skip dicts) */
	asEVAL();
	for (;;) {
		List  next = tl(cns);
		Label l    = isNull(next) ? noMatch : newLabel(RUNON);
		Name  c    = fst(hd(cns));
		Int   i    = intOf(snd(hd(cns)));
		cns        = next;
		asTEST(c,l);
		if (i>0)
			asLOAD(a + name(c).arity + 1 - i);
		asRUPDATE();
		if (nonNull(next))
			atLabel(l);
		else
			break;
	}
	asEND();
#ifdef DEBUG_CODE
	printf("Implement selector ");
	printExp(stdout,s);
	printf(" with code:\n");
	dissassemble(startInstr,lastInstr);
	printf("------------------\n");
#endif
	name(s).code = startInstr;
}

/* Build implementation for constr */
/* fun c.  scs lists integers (1..)*/
/* in incr order of strict comps.  */
Void implementCfun(Name c, List scs)
{
	Int a = name(c).arity;
	if (a==0 || isNull(scs))
		name(c).defn = c;		/* Name ==> no special imp.	   */
	else {
		Name n		= newName(inventText());
		Int  i		= 0;
		name(c).defn    = pair(scs,n);  /* (scs,n) => strict components	   */
		name(n).arity   = a;		/* Initialize name data as approp. */
		name(n).line    = 0;
		name(n).number  = EXECNAME;
		name(n).type    = NIL;		/* could obtain from name(c).type? */
		name(n).primDef = 0;

		asSTART();			/* inaccurate srsp doesn't matter  */
		asCELL(c);
		for (i=1; i<=a; i++)
			if (nonNull(scs) && intOf(hd(scs))==i) {
				asSTAP();
				scs = tl(scs);
			}
			else
				asMKAP(1);
		asRUPDATE();
		asEND();
		name(n).code = startInstr;
#ifdef DEBUG_CODE
		printf("Implement constructor ");
		printExp(stdout,c);
		printf(" using ");
		printExp(stdout,n);
		printf(" with code:\n");
		dissassemble(startInstr,lastInstr);
		printf("------------------\n");
#endif
	}
}

/* Add name n as an external primitive;	   */
/* This is not currently implemented in	   */
/* the current version of the interpreter  */
Void externalPrim(Name   n, String s)
{
	ERRMSG(name(n).line) "Unknown primitive reference \"%s\"", s
		EEND;
}

/* --------------------------------------------------------------------------
* Evaluator:
* ------------------------------------------------------------------------*/

Int   whnfArgs;				/* number of arguments of whnf term*/
Cell  whnfHead;				/* head cell of term in whnf	   */
Int   whnfInt;				/* value of INTCELL (in whnf)	   */
Float whnfFloat;			/* value of FLOATCELL (in whnf)    */
Long  numReductions;			/* number of reductions counted	   */
#if PROFILING
#define saveProducer(n)			{ Name old = producer; producer = n
#define restoreProducer()       	producer = old;			     \
	if ((numReductions%profInterval)==0) \
	garbageCollect();		     \
}
#else
#define saveProducer(n)			/* nothing */
#define restoreProducer()		/* nothing */
#endif

static Cell    errorRedex;	       /* irreducible error expression	   */
static jmp_buf *evalError = 0;	       /* jump buffer for eval errors	   */

Void eval(Cell n)				   /* Graph reduction evaluator    */
{
	StackPtr base = sp;
	Int      ar;

unw: switch (whatIs(n)) {		   /* unwind spine of application  */

	String str;

case AP        : push(n);
	n = fun(n);
	goto unw;

case INDIRECT  : n = arg(n);
	allowBreak();
	goto unw;

case NAME      : allowBreak();
	str = textToStr(name(n).text);
	if (!isCfun(n) && (ar=name(n).arity)<=(sp-base)) {
		if (ar>0) { 		    /* fn with args*/
			StackPtr root;

			push(NIL);		    /* rearrange   */
			root = sp;
			do {
				stack(root) = arg(stack(root-1));
				--root;
			} while (--ar>0);

			saveProducer(n);
			if (name(n).primDef)	    /* reduce	   */
				(*name(n).primDef)(root);
			else
				run(name(n).code,root);
			numReductions++;
			restoreProducer();

			sp = root;		    /* continue... */
			n  = pop();
		}
		else {			    /* CAF	   */
			if (isNull(name(n).defn)) {/* build CAF   */
				StackPtr root = sp;
				push(n);		    /* save CAF    */
				saveProducer(n);
				if (name(n).primDef)
					(*name(n).primDef)(sp);
				else
					run(name(n).code,sp);
				numReductions++;
				restoreProducer();
				name(n).defn = top();
				sp           = root;   /* drop CAF    */
			}
			n = name(n).defn;	    /*already built*/
			if (sp>base)
				fun(top()) = n;
		}
		goto unw;
	}
	break;

case INTCELL   : whnfInt = intOf(n);
	break;

case FLOATCELL : whnfFloat = (Float)floatOf(n);
	break;

case STRCELL   : evalString(n);
	goto unw;
	 }

	 /* rearrange components of term on  */
	 /* stack, now in whnf ...	   */
	 whnfHead = n;
	 whnfArgs = sp - base;
	 for (ar=whnfArgs; ar>0; ar--) {
		 fun(stack(base+ar)) = n;
		 n		    = stack(base+ar);
		 stack(base+ar)	    = arg(n);
	 }
}

/* unwind spine of application;	   */
/* like eval except that we always  */
/* treat the expression n as if it  */
/* were already in whnf. 	   */
Void unwind(Cell n)
{
	whnfArgs = 0;

unw:switch (whatIs(n)) {
case AP        : push(arg(n));
	whnfArgs++;
	n = fun(n);
	goto unw;

case INDIRECT  : n = arg(n);
	allowBreak();
	goto unw;

case INTCELL   : whnfInt = intOf(n);
	break;

case FLOATCELL : whnfFloat = (Float)floatOf(n);
	break;

case STRCELL   : evalString(n);
	goto unw;
	}
	whnfHead = n;
}

static Void local evalString(Cell n)		/* expand STRCELL at node n	   */
{
	Text t = textOf(n);
	Int  c = textToStr(t)[0];
	if (c==0) {
		fst(n) = INDIRECT;
		snd(n) = nameNil;
		return;
	}
	else if (c=='\\') {
		c = textToStr(++t)[0];
		if (c!='\\')
			c = 0;
	}
	push(n);				/* protect n during mkStr	   */
	fst(n) = consChar(c);
	snd(n) = mkStr(++t);
	drop();
}

/* execute code beginning at given  */
/* address with local stack starting*/
/* at given root offset		   */
static Void local run(Addr	 start, StackPtr root)
{
	register Memory pc = memory+start;

#if     GCC_THREADED
#define Ins(x)		&&l##x
	static  void *labs[] = { INSTRLIST };
#undef  Ins
#define Case(x)		l##x
#define	Continue	goto *labs[(pc++)->instr]
#define	Dispatch	Continue;
#define EndDispatch
#else
#define Dispatch	for (;;) switch((pc++)->instr) {
#define	Case(x)		case x
#define	Continue	continue
#define EndDispatch	default : internal("illegal instruction"); \
	break;			   \
	}
#endif

	Dispatch

		Case(iLOAD)   : push(stack(root+pc->mint));	 /* load from stack*/
	pc++;
	Continue;

	Case(iCELL)   : push(pc->cell);			 /* load const Cell*/
	pc++;
	Continue;

	Case(iCHAR)   : push(mkChar(pc->mint));		 /* load char const*/
	pc++;
	Continue;

	Case(iINT)    : push(mkInt(pc->mint));		 /* load int const */
	pc++;
	Continue;

	Case(iFLOAT)  : push(mkFloat(pc->mfloat));	 /* load float cnst*/
	pc++;
	Continue;

	Case(iSTRING) : push(mkStr(pc->text));		 /* load str const */
	pc++;
	Continue;

	Case(iMKAP)   : {   Int i = pc->mint;		 /* make AP nodes  */
	while (0<i--) {
		pushed(1) = ap(pushed(0),pushed(1));
		drop();
	}
	}
	pc++;
	Continue;

	Case(iUPDATE) : {   Cell t = stack(root		/* update cell ...*/
		+ pc->mint);
	fst(t) = INDIRECT;
	snd(t) = pop();
	}
	pc++;
	Continue;

	Case(iRUPDATE): {   Cell t = stack(root);	/* update and ret  */
	fst(t) = INDIRECT;
	snd(t) = top();
	}
	return;

	Case(iUPDAP)  : {   Cell t = stack(root		 /* update AP node */
		+ pc->mint);
	fst(t) = pop();
	snd(t) = pop();
	}
	pc++;
	Continue;

	Case(iRUPDAP) : {   Cell t = stack(root);	 /* updap and ret  */
	fst(t) = pop();
	snd(t) = top();
	}
	return;

	Case(iEVAL)   : eval(pop());			 /* evaluate top() */
	Continue;

	Case(iLEVAL)  : eval(stack(root+pc->mint));	 /* eval from stack*/
	pc++;
	Continue;

	Case(iSTAP)   : eval(pushed(1));		 /* strict apply   */
	sp       -= whnfArgs;
	pushed(1) = ap(top(),pushed(1));
	drop();
	Continue;

	Case(iRETURN) : return;				 /* terminate	   */

	Case(iTEST)   : if (whnfHead==pc->cell)		 /* test for cell  */
		pc += 2;
	else
		pc = memory + (pc+1)->addr;
	Continue;

	Case(iGOTO)   : pc = memory + pc->addr;		 /* goto label	   */
	Continue;

	Case(iSETSTK) : sp=root + pc->mint;	 	 /* set stack ptr  */
	pc++;
	Continue;

	Case(iALLOC)  : {   Int i = pc->mint;		 /* alloc loc vars */
	chkStack(i);
	while (0<i--)
		onto(ap(NIL,NIL));
	}
	pc++;
	Continue;

	Case(iDICT)   : top() = dictGet(top(),pc->mint); /* dict lookup	   */
	pc++;
	Continue;

	Case(iROOT)   : {   Cell t = stack(root);	 /* partial root   */
	Int  i = pc->mint;
	while (fst(t)==INDIRECT) {
		allowBreak();
		t = arg(t);
	}
	while (0<i--) {
		t = fun(t);
		while (fst(t)==INDIRECT) {
			allowBreak();
			t = arg(t);
		}
	}
	push(t);
	}
	pc++;
	Continue;

	Case(iSLIDE)  : pushed(pc->mint) = top();	 /* remove loc vars*/
	sp -= pc->mint;
	pc++;
	Continue;

	Case(iTABLE)  :
		Case(iFAIL)   : evalFails(root);		 /* cannot reduce  */
	return;/*NOT REACHED*/

	EndDispatch

#undef Dispatch
#undef Case
#undef Continue
#undef EndDispatch
}

/* Evaluate expression, returning   */
/* NIL if successful, irreducible   */
/* expression if not...		   */
Cell evalWithNoError(Cell e)
{
	Cell badRedex;
	jmp_buf *oldCatch = evalError;

#if JMPBUF_ARRAY
	jmp_buf catcherr[1];
	evalError = catcherr;
	if (setjmp(catcherr[0])==0) {
		eval(e);
		badRedex = NIL;
	}
	else
		badRedex = errorRedex;
#else
	jmp_buf catcherr;
	evalError = &catcherr;
	if (setjmp(catcherr)==0) {
		eval(e);
		badRedex = NIL;
	}
	else
		badRedex = errorRedex;
#endif

	evalError = oldCatch;
	return badRedex;
}

Void evalFails(StackPtr root)			/* Eval of current redex fails	   */
{
	errorRedex = stack(root);		/* get error & bypass indirections */
	while (isPair(errorRedex) && fst(errorRedex)==INDIRECT)
		errorRedex = snd(errorRedex);

	if (failOnError)
		abandon("Program",errorRedex);
	else if (evalError)
		longjmp(*evalError,1);
	else
		internal("uncaught eval error");
}

/* Build graph for expression to be*/
/* reduced...			   */
Void graphForExp() {
	run(inputCode,sp);
#ifdef DEBUG_CODE
	printf("graphForExp() builds: ");
	printExp(stdout,top());
	putchar('\n');
#endif
}

/* --------------------------------------------------------------------------
* Machine control:
* ------------------------------------------------------------------------*/

Void machine(Int what)
{
	switch (what) {
	case INSTALL : memory  = (Memory)farCalloc(NUM_ADDRS,sizeof(MemCell));
		if (memory==0)
			fatal("Cannot allocate program memory");
		instrNone(iFAIL);
		noMatch = lastInstr;
		break;
	}
}

/* ------------------------------------------------------------------------*/
