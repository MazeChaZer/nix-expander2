/* --------------------------------------------------------------------------
* type.c:      Copyright (c) Mark P Jones 1991-1998.   All rights reserved.
*              See NOTICE for details and conditions of use etc...
*              Hugs version 1.3b, January 1998
*
* This is the Hugs type checker
* ------------------------------------------------------------------------*/

#include "prelude.h"
#include "storage.h"
#include "connect.h"
#include "errors.h"

/*#define DEBUG_TYPES*/
/*#define DEBUG_KINDS*/
/*#define DEBUG_DICTS*/
/*#define DEBUG_DEFAULTS*/

Bool catchAmbigs       = FALSE;		/* TRUE => functions with ambig.   */
/* 	   types produce error	   */

Type typeArrow,	typeList;		/* Important primitive types	   */
Type typeUnit,	typeArray;

static Type typeInt,     typeInteger;
static Type typeFloat,   typeDouble;
static Type typeString,  typeChar;
static Type typeBool,    typeMaybe;
static Type typeOrdering;

Class classEq,    classOrd;		/* `standard' classes		   */
Class classIx,    classEnum;
Class classShow,  classRead;
Class classEval,  classBounded;

Class classReal,       classIntegral;	/* `numeric' classes		   */
Class classRealFrac,   classRealFloat;
Class classFractional, classFloating;
Class classNum;

List stdDefaults;			/* standard default values	   */

Name nameFromInt, nameFromDouble;	/* coercion of numerics		   */
Name nameFromInteger;
Name nameEq,      nameCompare;		/* derivable names		   */
Name nameLe,	  nameShowsPrec;
Name nameMinBnd,  nameMaxBnd;
Name nameIndex,	  nameInRange;
Name nameRange;
Name nameMult,	  namePlus;
Name nameTrue,	  nameFalse;		/* primitive boolean constructors  */
Name nameNil,     nameCons;		/* primitive list constructors	   */
Name nameJust,	  nameNothing;		/* primitive Maybe constructors	   */
Name nameUnit;				/* primitive Unit type constructor */
Name nameLT,	  nameEQ;		/* Ordering constructors	   */
Name nameGT;
Class classMonad, classMonad0;		/* Monads and monads with a zero   */
Class classFailureMonad;		/* Monad with catch and raise      */
Class classFixMonad;       		/* Monad with fix                  */
Name nameStrict,  nameSeq;		/* Members of class Eval	   */

#if    IO_MONAD
Type   typeIO,	     typeProgIO;	/* For the IO monad, IO ()	   */
Type   typeHandle,   typeIOError;
Type   typeIORef;
Name   nameUserErr,  nameIllegal;	/* loosely coupled IOError cfuns   */
Name   nameNameErr,  nameSearchErr;
Name   nameWriteErr, nameEvalErr;
#endif

#if    LAZY_ST
Type   typeST;				/* Lazy state threads		   */
Type   typeMutVar,   typeMutArr;
#endif

#if OBJ
Type   typeO,        typeProgO;		/* For the O monad		   */
Type   typeOst,      typeCmd;
Type   typeAction,   typeRequest;
Type   typeTemplate, typeError;
Type   typeRef,      typeOID,      typeTag;
Type   typeOProg,    typeEnvironment;
Name   nameDeadlock, nameReqAbort, nameMissedDeadline;
Name   nameFileError;
Name   namePutCharSel,    namePutStrSel;
Name   nameSetReaderSel,  nameWriteFileSel;
Name   nameAppendFileSel, nameReadFileSel;
Name   nameTimeOfDaySel;
Name   nameProgArgsSel;
Name   nameGetEnvSel;
Name   nameTerminateSel;
#if O_IP
Name   nameInetSel;
Name   nameNetError;
Name   nameDeliver, nameClosed;
#endif
Cell dictMonadTempl, dictFixMonadTempl;
#endif


/* --------------------------------------------------------------------------
* Data structures for storing a substitution:
*
* For various reasons, this implementation uses structure sharing, instead of
* a copying approach.	In principal, this is fast and avoids the need to
* build new type expressions.	Unfortunately, this implementation will not
* be able to handle *very* large expressions.
*
* The substitution is represented by an array of type variables each of
* which is a quadruple {Type bound; Int status; Int offs; Kind kind}.  The 
* last field in this struct always represents the kind of the given type 
* variable. Depending on the value of field state, the remaining two fields
* have the following meaning:
*
* case NORMAL:
*	bound	a (skeletal) type expression, or NIL if the variable
*		is not bound.
*	offs	offset of skeleton in bound.  If bound is NIL, this field
*		is used to collect variance information, or alternatively,
*		to indicate whether that variable is generic (i.e. free in
*		the current assumption set) or fixed (i.e. bound in the
*		current assumption set).  Generic variables are assigned 
*		offset numbers whilst copying type expressions (t,o) to 
*		obtain their most general form. The default usage of this
*		field is as a container of variance information; a call to
*		clearMarks() is necessary to indicate a change of meaning.
*
* case ACCUM:
*	bound	a list of accumulated bounds of the variable. The bounds are
*		represented as pairs of type variable numbers and variance
*		information.
*	offs	a reference counter counting occurrences of the variable in 
*		other accumulated bounds.
*
* case SKOLEM:
*	bound	currently NIL. Will hold part of the subtyping theory that
*		involves this variable.
*	offs	same as case NORMAL.
*
* case CLONED:
*	bound	a list of type variables having this type variable as root.
*		The bound are represented as pairs of type variable numbers
*		and variance information.
*	offs	same as case NORMAL.
* ------------------------------------------------------------------------*/

typedef struct {			/* Each type variable contains:	   */
	Type bound;				/* A type skeleton (unbound==NIL)  */
	Int  state;				/* NORMAL | ACCUM | SKOLEM | CLONED*/
	Int  offs;				/* Offset for skeleton		   */
	Kind kind;				/* kind annotation		   */
} Tyvar;

static	Int	   numTyvars;		/* no. type vars currently in use  */
#if     FIXED_SUBST
static	Tyvar	   tyvars[NUM_TYVARS];	/* storage for type variables	   */
#else
static  Tyvar     *tyvars = 0;		/* storage for type variables	   */
static  Int        maxTyvars = 0;
#endif
static	Int	   typeOff;		/* offset of result type 	   */
static	Type	   typeIs;		/* skeleton of result type	   */

static  List	   goalStack;		/* stack of inference goals	   */

#define pushGoal(g)	goalStack = cons(g,goalStack)
#define popGoal()	goalStack = tl(goalStack)
#define addGoal(vn)	hd(goalStack) = cons(mkInt(vn),hd(goalStack))
#define pushOneGoal(vn)	pushGoal(singleton(mkInt(vn)))

static  List       forkStackT;          /* stack of forked expected types  */
static  List       forkStackO;          /* stack of forked expected types  */

static	Int	   nextGeneric; 	/* number of generics found so far */
static  List	   genericVars;		/* list of generic vars		   */

/* --------------------------------------------------------------------------
* Tyvar encodings
* ------------------------------------------------------------------------*/

#define tyvar(n)   	(tyvars+(n))	/* nth type variable		   */
#define tyvNum(t)  	((t)-tyvars)	/* and the corresp. inverse funct. */

#define NORMAL		0
#define ACCUM		1
#define	CLONED		2
#define SKOLEM		3

#define isNormal(tyv)	((tyv)->state == NORMAL)
#define isAccum(tyv)	((tyv)->state == ACCUM)
#define isCloned(tyv)	((tyv)->state == CLONED)
#define isSkolem(tyv)	((tyv)->state == SKOLEM)

#define isBound(t)	(isNormal(t) && (t)->bound)
#define isUnbound(t)	(isNormal(t) && !((t)->bound))
#define shouldMark(t)   (!isBound(t) && !isAccum(t))

/* --------------------------------------------------------------------------
* Variances: (offs field for CLONED variables)
* ------------------------------------------------------------------------*/

#define NONV		0
#define NEGV		1
#define POSV		2
#define INV		(NEGV|POSV)

#define negv(x)		(((x&POSV) >> 1) | ((x&NEGV) << 1))
#define multv(x,y)	(((y&POSV) ? x : 0) | ((y&NEGV) ? negv(x) : 0))

/* --------------------------------------------------------------------------
* Generics: (offs field after clearMarks() / markType())
* ------------------------------------------------------------------------*/

#define UNUSED_GENERIC 	0	        /* not fixed, not yet encountered  */
#define FIXED_TYVAR   	1	        /* fixed in current assumption	   */
#define GENERIC        	2	        /* GENERIC+n==nth generic var found*/

/* --------------------------------------------------------------------------
* Type checker modes:
* ------------------------------------------------------------------------*/

#define EXPRESSION  0			/* type checking expression	   */
#define NEW_PATTERN 1			/* pattern, introducing new vars   */
#define OLD_PATTERN 2			/* pattern, involving bound vars   */
static int tcMode = EXPRESSION;

/* --------------------------------------------------------------------------
* Local function prototypes:
* ------------------------------------------------------------------------*/

static Void   local emptySubstitution(Void);
static Void   local expandSubst(Int);
static Void   local resetState(Tyvar *);
static Int    local newTyvars(Int);
static Int    local newKindedVars(Kind,Bool);
static Tyvar *local getTypeVar(Type,Int);
static Void   local tyvarType(Int);
static Void   local bindTv(Int,Type,Int);
static Void   local expandSyn(Tycon, Int, Type *, Int *);
static Void   local expandSyn1(Tycon, Type *, Int *);
static Cell   local getDerefHead(Type,Int);

static Void   local clearOffsTyvar(Int);
static Void   local clearOffsType(Type,Int);
static Void   local clearMarks(Void);
static Void   local resetGenericsFrom(Int);
static Void   local markTyvar(Int);
static Void   local markType(Type,Int);

static Void   local markVarianceTyvar(Int,Int);
static Void   local markVarianceType(Type,Int,Int);

static Cell   local getVariance(Type,Int);
static Int    local thisVariance(Cell);
static Cell   local nextVariance(Cell);
static Void   local markVarianceTyvar(Int,Int);
static Void   local markVarianceType(Type,Int,Int);

static Type   local cloneTyvar(Int,Int);
static Type   local cloneType(Type,Int,Int);
static Type   local clone(Type,Int,Int);
static Int    local cloneTv(Int,Int);
static Bool   local doAccum(Int);
static Bool   local accumClonedTyvar(Int,Bool);
static Bool   local accumClonedType(Type,Int,Bool);
static Bool   local accumClonedPolyType(Type,Bool);

static Type   local copyTyvar(Int);
static Type   local copyType(Type,Int);
static List   local genvarTyvar(Int,List);
static List   local genvarType(Type,Int,List);
#ifdef DEBUG_TYPES
static Type   local debugTyvar(Int);
static Type   local debugType(Type,Int);
#endif

static Bool   local occurs(Tyvar *,Type,Int);
static Bool   local offsetsBetween(Int,Int,Type);

static Void   local mkAccum(Tyvar *);
static Void   local deAccum(Tyvar *,Bool);
static Void   local countTyvar(Int);
static Void   local countType(Type,Int);

static Bool   local solveAccum(Tyvar *,Type,Int);
static Bool   local varToVarBind(Tyvar *,Tyvar *,Int);
static Bool   local varToTypeAccum(Tyvar *,Type,Int,Int);
static Bool   local kvarToVarBind(Tyvar *,Tyvar *);
static Bool   local kvarToTypeBind(Tyvar *,Type,Int);
static Bool   local failure(String);
static Bool   local preUnify(Type,Int,Type,Int,Int);
static Int    local varianceInGoal(Tyvar *);
static Bool   local isSubtype(Type,Type,Int);
static Bool   local allSubtypes(List,Type,Int);
static List   local rootedAt(Tycon,Int);
static List   local matchArities(List,Int);
static List   local bestCandidates(List,Int);
static Bool   local mergeBounds(Tyvar*,Type*,Int*);
static Bool   local postUnify(Void);
static Bool   local unify(Type,Int,Type,Int);
static Bool   local sameType(Type,Int,Type,Int);
static Bool   local kunify(Kind,Int,Kind,Int);

static Void   local kindError(Int,Constr,Constr,String,Kind,Int);
static Void   local kindConstr(Int,Constr);
static Kind   local kindAtom(Constr);
static Void   local kindPred(Int,Cell);
static Void   local kindType(Int,String,Type);
static Void   local kindAxiom(Tycon,Type);
static Void   local fixKinds(Void);

static Void   local initTCKind(Cell);
static Void   local kindTC(Cell);
static Void   local genTC(Cell);
static Kind   local copyKindvar(Int);
static Kind   local copyKind(Kind,Int);

static Bool   local eqKind(Kind,Kind);
static Kind   local getKind(Cell,Int);

static Kind   local makeSimpleKind(Int);
static Kind   local simpleKind(Int);
static Kind   local makeVarKind(Int);
static Void   local varKind(Int);

static Void   local emptyAssumption(Void);
static Void   local enterBindings(Void);
static Void   local leaveBindings(Void);
static Int    local defType(Cell);
static Type   local useType(Cell);
static Void   local markAssumList(List);
static Cell   local findAssum(Text);
static Pair   local findInAssumList(Text,List);
static List   local intsIntersect(List,List);
static List   local genvarAllAss(List);
static List   local genvarAnyAss(List);
static Void   local newVarsBind(Cell,Int);
static Void   local newDefnBind(Cell,Type);
static Void   local newStateBind(Int,Cell,Type);
static Void   local skolemOffsetsAbove(Int,Type,Int);
static Void   local saveSkolem2(Void);
static Void   local restoreSkolem2(Void);
static Void   local skolemTyvar(Int);
static Void   local skolemType(Type,Int);
static Void   local deskolemTyvar(Int);
static Void   local saveSkolem(Void);
static Void   local restoreSkolem(Void);

static Void   local pushExpectedType(Type,Int);
static Void   local popExpectedType(Void);

static Void   local escapeError(Int,Cell,Type,Type);
static Void   local checkAndRestoreSkolem2(Int,Type,Int);
static Void   local reportConstraints(Void);
static Void   local resolveBindings(Int,String,List,Int);
static Void   local reportBindings(List);
static Void   local reportBindingsError(Int,String,List);
static Void   local resolveType(Int,Cell,String,Type,Int,Int);
static Void   local reportTypeError(Int,Cell,String,Type,Int);
static Void   local match(Int,Cell,String,Type,Int,Type,Int);
static Void   local reportMatchError(Int,Cell,String,Type,Int,Type,Int);

static Cell   local typeExpr(Int,Cell,Type,Int);
static Type   local freshSig(Type);
static Void   local instantiate(Type,Type *,Int *, List *);
static Cell   local varIntro(Int,Cell,String,Type,Type,Int);
#if LAZY_ST
static Void   local typeRunST(Int,Cell,Type,Int);
#endif
static Void   local typeEsign(Int,Cell,Type,Int);
#if OBJ
static Void   local typeHandler(List,Type,Int);
static Void   local typeTempl(Int,Cell,Type,Int);
#endif
static Void   local typeComp(Int,Type,Cell,List,Type,Int);
static Void   local typeDo(Int,Cell,Type,Int);
static Cell   local compZero(List,Int);
static Cell   local compFix(List,Int);
static Cell   local typeFreshPat(Int,Cell,Type,Int);
static Bool   local isSimplePat(Cell);

static Type   local funcType(Int);
static Type   local makeFuncType(Int);

static Cell   local typeAp(Int,Cell,Type,Int);
static Void   local typeLambda(Int,Cell,Type,Int);
static Void   local typeAlts(Cell,List,Type,Int);
static Void   local typeCase(Int,Cell,Type,Int);
static Void   local typeStruct(Cell,Type,Int);
static Void   local checkAllDefined(Int,List,Int);

static Void   local typeTuple(Int,Cell,Type,Int);
static Type   local makeTupleType(Int);

static Void   local typeBindings(List);
static Void   local removeTypeSigs(Cell);

static Void   local monorestrict(List);
static Void   local restrictedBindAss(Cell);
static Void   local restrictedAss(Int,Cell,Type);

static Void   local unrestricted(List);
static Void   local addEvidParams(List,Cell);

static Void   local typeBind(Cell);
static Cell   local typeRhs(Cell,Type,Int);
static Void   local guardedType(Int,Cell);

static Void   local genBind(List,List,Cell);
static Void   local genAss(Int,List,List,Cell,Type);
static Type   local elimWildtypes(Type);
static Type   local generalize(List,Type);
static Void   local tooGeneral(Int,Cell,Type,Type);

static Bool   local checkSchemes(Type,Type);
static Bool   local checkQuals(List,List);
static Bool   local equalTypes(Type,Type);

static Void   local typeInstDefn(Inst);
static Void   local typeClassDefn(Class);
static Void   local typeMembers(String,List,List,List,Int,List,Type);
static Void   local typeMember(String,Name,Name,List,Int,List,Type);

static Void   local typeDefnGroup(List);

static Void   local loadVariance(Tycon,Int);
static Bool   local restoreVariance(Tycon,Int);

/* --------------------------------------------------------------------------
* Frequently used type skeletons:
* ------------------------------------------------------------------------*/

static Type  var;			/* mkOffset(0)		   	   */
static Type  arrow;			/* mkOffset(0) -> mkOffset(1)      */
static Type  boundPair;			/* (mkOffset(0),mkOffset(0))	   */
static Type  listof;			/* [ mkOffset(0) ] 	    	   */
static Type  typeVarToVar;		/* mkOffset(0) -> mkOffset(0)  	   */
#if    LAZY_ST
static Type  typeSTab;			/* ST a b			   */
#endif
#if    OBJ
static Type  templof;			/* Template mkOffset(0)		   */
static Type  reqof;			/* Request mkOffset(0)             */
static Type  refof;                     /* Ref mkOffset(0)                 */
#endif

static Cell  predNum;			/* Num (mkOffset(0))		   */
static Cell  predFractional;		/* Fractional (mkOffset(0))	   */
static Cell  predIntegral;		/* Integral (mkOffset(0))	   */
static Kind  starToStar;		/* Type -> Type			   */
static Cell  predMonad;			/* Monad (mkOffset(0))		   */
static Cell  predMonad0;		/* Monad0 (mkOffset(0))		   */
static Cell  predFailureMonad;          /* FailureMonad (mkOffset(0))      */
static Cell  predFixMonad;              /* FixMonad (mkOffset(0))          */
static Cell  evalDict;			/* Dictionary for eval instances   */

#define isFun(t)     (isAp(t) && isAp(fun(t)) && fun(fun(t))==typeArrow)
#define dom(t)	     arg(fun(t))
#define rng(t)	     arg(t)


/* --------------------------------------------------------------------------
* Basic operations on current substitution:
* ------------------------------------------------------------------------*/

#include "subst.c"

/* --------------------------------------------------------------------------
* Kind expressions:
*
* In the same way that values have types, type constructors (and more
* generally, expressions built from such constructors) have kinds.
* The syntax of kinds in the current implementation is very simple:
*
*	  kind ::= STAR		-- the kind of types
*		|  kind => kind -- constructors
*		|  variables	-- either INTCELL or OFFSET
*
* ------------------------------------------------------------------------*/

#include "kind.c"

/* --------------------------------------------------------------------------
* Predicate sets:
*
* A predicate set is represented by a list of triples (C t, o, used)
* which indicates that type (t,o) must be an instance of class C, with
* evidence required at the node pointed to by used.  Note that the `used'
* node may need to be overwritten at a later stage if this evidence is
* to be derived from some other predicates by entailment.
* ------------------------------------------------------------------------*/

#include "preds.c"

/* --------------------------------------------------------------------------
* Assumptions:
*
* A basic typing statement is a pair (Var,Type) and an assumption contains
* an ordered list of basic typing statements in which the type for a given
* variable is given by the most recently added assumption about that var.
*
* In practice, the assumption set is split between a pair of lists, one
* holding assumptions for vars defined in bindings, the other for vars
* defined in patterns/binding parameters etc.	The reason for this
* separation is that vars defined in bindings may be overloaded (with the
* overloading being unknown until the whole binding is typed), whereas the
* vars defined in patterns have no overloading.  A form of dependency
* analysis (at least as far as calculating dependents within the same group
* of value bindings) is required to implement this.  Where it is known that
* no overloaded values are defined in a binding (i.e., when the `dreaded
* monomorphism restriction' strikes), the list used to record dependents
* is flagged with a NODEPENDS tag to avoid gathering dependents at that
* level.
*
* To interleave between vars for bindings and vars for patterns, we use
* a list of lists of typing statements for each.  These lists are always
* the same length.  The implementation here is very similar to that of the
* dependency analysis used in the static analysis component of this system.
*
* To deal with polymorphic recursion, variables defined in bindings can be
* assigned types of the form (POLYREC,(def,use)), where def is a type
* variable for the type of the defining occurence, and use is a type
* scheme for (recursive) calls/uses of the variable.  Because of the the
* current treatment of type classes, this form of binding should only be
* used if the declared type is polymorphic, without any class constraints.
* (It may be possible to weaken this a little in future, by forcing
* a, potentially less general type scheme that is monomorphic in
* constrained type variables, but polymorphic in other type vars.)
* ------------------------------------------------------------------------*/

static List defnBounds;			/*::[[(Var,Type)]] possibly ovrlded*/
static List varsBounds;			/*::[[(Var,Type)]] not overloaded  */
static List depends;			/*::[?[Var]] dependents/NODEPENDS  */

#if OBJ
static List stateBounds;		/*::[(Var,Type)]                   */
static Int  localState;			
static Bool stateVisible;
static Bool refState;
#endif

static Void local emptyAssumption() {  	/* set empty type assumption	   */
	defnBounds = NIL;
	varsBounds = NIL;
	depends    = NIL;
}

static Void local enterBindings() {    /* Add new level to assumption sets */
	defnBounds = cons(NIL,defnBounds);
	varsBounds = cons(NIL,varsBounds);
	depends    = cons(NIL,depends);
}

static Void local leaveBindings() {    /* Drop one level of assumptions    */
	defnBounds = tl(defnBounds);
	varsBounds = tl(varsBounds);
	depends    = tl(depends);
}


/* Return type for defining occ.   */
/* of a var from assumption pair  */
static Int local defType(Cell a)
{
	return (isPair(a) && fst(a)==POLYREC) ? fst(snd(a)) : a;
}

/* Return type for use of a var	   */
/* defined in an assumption	   */
static Type local useType(Cell a)
{
	return (isPair(a) && fst(a)==POLYREC) ? snd(snd(a)) : a;
}

/* Mark all types in assumption set*/
static Void local markAssumList(
	List as				/* :: [(Var, Type)]		   */
	)
{
	/* No need to mark generic types;  */
	/* the only free variables in those*/
	/* must have been free earlier too */
	for (; nonNull(as); as=tl(as)) {
		Type t = defType(snd(hd(as)));
		if (!isPolyType(t))
			markType(t,0);
	}
}

/* Find most recent assumption about*/
/* variable named t, if any	   */
/* return translated variable, with */
/* type in typeIs		   */
static Cell local findAssum(Text t)
{
	List defnBounds1 = defnBounds;
	List varsBounds1 = varsBounds;
	List depends1    = depends;
	Pair ass;

#if OBJ
	if (stateVisible && nonNull(ass = findInAssumList(t,stateBounds))) {
		typeIs = snd(ass);
		if (tcMode==EXPRESSION)
			refState = TRUE;
		return fst(ass);
	}
#endif

	while (nonNull(defnBounds1) || nonNull(varsBounds1)) {
		ass = findInAssumList(t,hd(varsBounds1));/* search varsBounds */
		if (nonNull(ass)) {
			typeIs = snd(ass);
			return fst(ass);
		}

		ass = findInAssumList(t,hd(defnBounds1));     /* search defnBounds */
		if (nonNull(ass)) {
			Cell v = fst(ass);
			typeIs = snd(ass);

			if (hd(depends1)!=NODEPENDS &&	      /* save dependent?   */
				isNull(v=varIsMember(t,hd(depends1))))
				/* N.B. make new copy of variable and store this on list of*/
				/* dependents, and in the assumption so that all uses of   */
				/* the variable will be at the same node, if we need to    */
				/* overwrite the call of a function with a translation...  */
				hd(depends1) = cons(v=mkVar(t),hd(depends1));

			return v;
		}

		defnBounds1 = tl(defnBounds1);		      /* look in next level*/
		varsBounds1 = tl(varsBounds1);		      /* of assumption set */
		depends1    = tl(depends1);
	}
	return NIL;
}

/* Search for assumption for var    */
/* named t in list of assumptions as*/
static Pair local findInAssumList(Text t, List as)
{
	for (; nonNull(as); as=tl(as))
		if (textOf(fst(hd(as)))==t)
			return hd(as);
	return NIL;
}

/* calculate intersection of lists */
/* of integers (as sets)	   */
/* destructively modifies as	   */
static List local intsIntersect(List as, List bs)
{
	List ts = NIL;
	while (nonNull(as))
		if (intIsMember(intOf(hd(as)),bs)) {
			List temp = tl(as);
			tl(as)    = ts;
			ts	      = as;
			as	      = temp;
		}
		else
			as = tl(as);
	return ts;
}

/* calculate generic vars that are */
/* in every type in assumptions as */
static List local genvarAllAss(List as)
{
	List vs = NIL;
	if (nonNull(as)) {
		vs = genvarTyvar(intOf(defType(snd(hd(as)))),NIL);
		for (as=tl(as); nonNull(as) && nonNull(vs); as=tl(as))
			vs = intsIntersect(vs,genvarTyvar(intOf(defType(snd(hd(as)))),NIL));
	}
	return vs;
}

/* calculate generic vars that are */
/* in any type in assumptions as   */
static List local genvarAnyAss(List as)
{
	List vs = NIL;
	if (nonNull(as)) {
		vs = genvarTyvar(intOf(defType(snd(hd(as)))),NIL);
		for (as=tl(as); nonNull(as); as=tl(as))
			vs = genvarTyvar(intOf(defType(snd(hd(as)))),vs);
	}
	return vs;
}

#define findTopBinding(v)  findInAssumList(textOf(v),hd(defnBounds))

/* make new assump for pattern var  */
static Void local newVarsBind(Cell v, Int beta)
{
	hd(varsBounds) = cons(pair(v,mkInt(beta)), hd(varsBounds));
#ifdef DEBUG_TYPES
	printf("variable, assume ");
	printExp(stdout,v);
	printf(" :: _%d\n",beta);
#endif
}

/* make new assump for defn var	   */
/* and set type if given (nonNull)  */
static Void local newDefnBind(Cell v, Type type)
{
	Int  beta	   = newTyvars(1);
	Cell ta        = mkInt(beta);
	Type t;
	Int  o;
	List ps;

	instantiate(type,&t,&o,&ps);
	skolemType(t,o);
	if (nonNull(type) && isNull(ps) && isPolyType(type))
		ta = pair(POLYREC,pair(ta,type));
	hd(defnBounds) = cons(pair(v,ta), hd(defnBounds));
#ifdef DEBUG_TYPES
	printf("definition, assume ");
	printExp(stdout,v);
	printf(" :: _%d\n",beta);
#endif
	bindTv(beta,t,o);       	/* Bind beta to new type skeleton   */
	for (; nonNull(ps); ps=tl(ps))
		assumeEvid(hd(ps),o);
}

#if OBJ
static Void local newStateBind(Int  l, Cell v, Type type)
{
	Int  beta	   = newTyvars(1);
	Cell ta        = mkInt(beta);
	Type t;
	Int  o;
	List ps;

	instantiate(type,&t,&o,&ps);
	skolemType(t,o);
	stateBounds = cons(pair(v,ta), stateBounds);
	bindTv(beta,t,o);       	/* Bind beta to new type skeleton   */
	for (; nonNull(ps); ps=tl(ps))
		assumeEvid(hd(ps),o);
}
#endif

/* --------------------------------------------------------------------------
* Fork/join expected types:
* ------------------------------------------------------------------------*/

static Void local pushExpectedType(Type t, Int o)
{
	forkStackT = cons(t,forkStackT);
	forkStackO = cons(mkInt(o),forkStackO);
}

static Void local popExpectedType() {
	forkStackT = tl(forkStackT);
	forkStackO = tl(forkStackO);
}

/* --------------------------------------------------------------------------
* Type errors:
* ------------------------------------------------------------------------*/

/* report escaping type variable   */
static Void local escapeError(Int l, Cell e, Type inft, Type expt)
{
	ERRMSG(l)  "Quantified type variable escaping its scope" ETHEN
		ERRTEXT    "\n*** term     : " ETHEN ERREXPR(e);
	ERRTEXT    "\n*** type     : " ETHEN ERRTYPE(inft);
	ERRTEXT    "\n*** expected : " ETHEN ERRTYPE(expt);
	ERRTEXT    "\n"
		EEND;
}

static Void local checkAndRestoreSkolem2(Int  l, Type t, Int  o)
{
	List vs = hd(skolVars2);
	restoreSkolem2();
	clearMarks();
	mapProc(markAssumList,defnBounds);
	mapProc(markAssumList,varsBounds);
#if OBJ
	markAssumList(stateBounds);
#endif
	mapProc(markPred,preds);
	markType(t,o);
	for (; nonNull(vs); vs=tl(vs)) {
		if (tyvar(intOf(hd(vs)))->offs == FIXED_TYVAR) {
			ERRMSG(l)  "Locally quantified type variable escaping its scope\n"
				EEND;
		}
	}
}

static Void local reportConstraints() {
	if (nonNull(lastBounds)) {
		Type t0 = copyTyvar(lastTyvar);
		List bs;
		String sep = "\n*** constraints         : ";
		for (bs=lastBounds; nonNull(bs); bs=tl(bs)) {
			Int z = intOf(snd(hd(bs)));
			if (z == POSV) {
				ERRTEXT sep ETHEN ERRTYPE(copyType(fst(hd(bs)),0));
				ERRTEXT " < " ETHEN ERRTYPE(t0);
			} else {
				ERRTEXT sep ETHEN ERRTYPE(t0);
				ERRTEXT (z==INV ? " = " : " < ") ETHEN
					ERRTYPE(copyType(fst(hd(bs)),0));
			}
			sep = ", ";
		}
	}
}

static Void local resolveBindings(Int l, String wh, List as, Int propagate)
{
	List as1 = as;
	Bool res = TRUE;

	clearMarks();
	if ((propagate & POSV) && nonNull(forkStackT)) {
		markType(hd(forkStackT),intOf(hd(forkStackO)));
	}
	if (propagate & NEGV) {
		mapProc(markAssumList,tl(varsBounds));
		mapProc(markAssumList,tl(defnBounds));
#if OBJ
		markAssumList(stateBounds);
#endif
	}
	for (; nonNull(as1); as1=tl(as1))
		if (!(res = accumClonedPolyType(defType(snd(hd(as1))),TRUE)))
			break;
	if (!res || !postUnify())
		reportBindingsError(l,wh,as);
}

static Void local reportBindings(List as)
{
	for (; nonNull(as); as=tl(as)) {
		Cell v   = fst(hd(as));
		Type t   = defType(snd(hd(as)));
		Text txt = isVar(v) ? textOf(v) : name(v).text;
		Int  len = strlen(textToStr(txt));
		Int  n   = (len>17) ? 0 : (17-len);
		ERRTEXT "\n*** \"%s\"", textToStr(txt) ETHEN
			for (; n>0; n--) {
				ERRTEXT " " ETHEN
			}
			ERRTEXT " : " ETHEN ERRTYPE(copyType(t,0));
	}
}

static Void local reportBindingsError(Int l, String wh, List as)
{
	normState();
	clearMarks();
	ERRMSG(l)   "Type error in %s", wh    ETHEN
		reportBindings(as);
	reportConstraints();
	if (unifyFails) {
		ERRTEXT "\n*** because             : %s", unifyFails ETHEN
	}
	ERRTEXT "\n"
		EEND;
}

static Void local resolveType(Int    l, Cell   e, String wh, Type   t, Int    o, Int    propagate)
{
	clearMarks();
	if ((propagate & POSV) && nonNull(forkStackT)) {
		markType(hd(forkStackT),intOf(hd(forkStackO)));
	}
	if (propagate & NEGV) {
		mapProc(markAssumList,varsBounds);
		mapProc(markAssumList,defnBounds);
#if OBJ
		markAssumList(stateBounds);
#endif
	}
	if (!accumClonedType(t,o,TRUE) || !postUnify())
		reportTypeError(l,e,wh,t,o);
}

static Void local reportTypeError(Int l, Cell e, String wh, Type t, Int o)
{
	normState();
	clearMarks();
	ERRMSG(l)   "Type error in %s", wh    ETHEN
		if (nonNull(e)) {
			ERRTEXT "\n*** term                : " ETHEN ERREXPR(e);
		}
		ERRTEXT     "\n*** type                : " ETHEN ERRTYPE(copyType(t,o));
		reportConstraints();
		if (unifyFails) {
			ERRTEXT "\n*** because             : %s", unifyFails ETHEN
		}
		ERRTEXT "\n"
			EEND;
}

static Void local match(
	Int    l,			      /* line number near type error	   */
	Cell   e,			      /* source of error		   */
	String wh,			      /* place in which error occurs	   */
	Type   t,			      /* inferred type 			   */
	Int    o,			      /*     -"-			   */
	Type   typ,			      /* expected type			   */
	Int    off			      /*     -"-			   */
	)
{
	if (!unify(t,o,typ,off))
		reportMatchError(l,e,wh,t,o,typ,off);
}

/* report general type error       */
static Void local reportMatchError(Int l, Cell e, String wh,
								   Type t, Int o, Type typ, Int off)
{
	normState();
	clearMarks();
	ERRMSG(l)   "Type error in %s", wh    ETHEN
		ERRTEXT     "\n*** term                : " ETHEN ERREXPR(e);
	ERRTEXT     "\n*** type                : " ETHEN ERRTYPE(copyType(t,o));
	reportConstraints();
	ERRTEXT     "\n*** does not match      : " ETHEN ERRTYPE(copyType(typ,off));
	if (unifyFails) {
		ERRTEXT "\n*** because             : %s", unifyFails ETHEN
	}
	ERRTEXT "\n"
		EEND;
}

/* --------------------------------------------------------------------------
* Typing of expressions:
* ------------------------------------------------------------------------*/

#ifdef DEBUG_TYPES
static Cell local mytypeExpr(Int,Cell);
static Cell local typeExpr(Int l, Cell e, Type typ, Int  off)
{
	static int number = 0;
	Cell retv;
	int  mynumber = number++;
	printf("%d) to check: ",mynumber);
	printExp(stdout,e);
	printf(" :: ");
	printType(stdout,debugType(typ,off));
	putchar('\n');
	retv = mytypeExpr(l,e,typ,off);
	printf("%d) result: ",mynumber);
	printType(stdout,debugType(typeIs,typeOff));
	putchar('\n');
	return retv;
}
/* Determine type of expr/pattern  */
static Cell local mytypeExpr(Int  l, Cell e, Type typ, Int  off)
#else
/* Determine type of expr/pattern  */
static Cell local typeExpr(Int  l, Cell e, Type typ, Int  off)
#endif
{
	switch (whatIs(e)) {

		/* The following cases can occur in either pattern or expr. mode   */

	case AP 	: return typeAp(l,e,typ,off);

	case NAME	: if (isNull(name(e).type))
					  internal("typeExpr1");
				  else {
					  Cell tt = varIntro(l,e,"constant",
						  name(e).type,typ,off);
					  return isCfun(e) ? e : tt;
				  }

	case TUPLE	: typeTuple(l,e,typ,off);
		break;

#if BIGNUMS
	case POSNUM	:
	case ZERONUM	:
	case NEGNUM	: {   Int alpha = newTyvars(1);
		bindTv(alpha,typ,off);
		return ap(ap(nameFromInteger,
			assumeEvid(predNum,alpha)),
			e);
				  }
#endif
	case INTCELL	: {   Int alpha = newTyvars(1);
		bindTv(alpha,typ,off);
		return ap(ap(nameFromInt,
			assumeEvid(predNum,alpha)),
			e);
					  }

	case FLOATCELL	: {   Int alpha = newTyvars(1);
		bindTv(alpha,typ,off);
		return ap(ap(nameFromDouble,
			assumeEvid(predFractional,alpha)),
			e);
					  }

	case STRCELL	: match(l,e,"string literal",typeString,0,typ,off);
		break;

	case CHARCELL	: match(l,e,"character literal",typeChar,0,typ,off);
		break;

	case VAROPCELL	:
	case VARIDCELL	: if (tcMode!=NEW_PATTERN) {
		Cell a = findAssum(textOf(e));
		if (nonNull(a)) {
			Type t = (tcMode==OLD_PATTERN)
				? defType(typeIs)
				: useType(typeIs);
			return varIntro(l,a,"variable",t,typ,off);
		}
		else {
			a = findName(textOf(e));
			if (isNull(a) || isNull(name(a).type))
				internal("typeExpr2");
			return varIntro(l,a,"constant",
				name(a).type,typ,off);
		}
					  }
					  else {
						  Int beta = newTyvars(1);
						  bindTv(beta,typ,off);
						  newVarsBind(e,beta);
					  }
					  break;

					  /* The following cases can only occur in expr mode		   */

	case COND	: {   pushExpectedType(typ,off);
		fst3(snd(e)) = typeExpr(l,fst3(snd(e)),typeBool,0);
		snd3(snd(e)) = typeExpr(l,snd3(snd(e)),clone(typ,off,POSV),0);
		thd3(snd(e)) = typeExpr(l,thd3(snd(e)),clone(typ,off,POSV),0);
		popExpectedType();
		resolveType(l,e,"conditional",typ,off,POSV);
				  }
				  break;

	case LETREC	: saveSkolem2();
		enterBindings();
		mapProc(typeBindings,fst(snd(e)));
		snd(snd(e)) = typeExpr(l,snd(snd(e)),typ,off);
		resolveBindings(l,"let binding",hd(varsBounds),NEGV); /* yes */
		leaveBindings();
		checkAndRestoreSkolem2(l,typ,off);
		break;

	case FINLIST	: {   Int  beta = newTyvars(1);
		List xs;
		match(l,e,"list",listof,beta,typ,off);
		pushExpectedType(listof,beta);
		for (xs=snd(e); nonNull(xs); xs=tl(xs)) {
			hd(xs) = typeExpr(l,hd(xs),var,cloneTv(beta,POSV));
		}
		popExpectedType();
		resolveType(l,e,"list expression",listof,beta,POSV);
					  }
					  break;

#if OBJ
	case HNDLEXP    : {   Int alpha = newTyvars(1);
		Int beta  = newTyvars(1);
		Cell mon,dict;
		tyvar(beta)->kind = starToStar;
		mon  = ap(mkInt(beta),var);
		dict = assumeEvid(predFailureMonad,beta);
		match(l,1,"exception handler",mon,alpha,typ,off);
		pushExpectedType(mon,alpha);
		typeHandler(fst(snd(e)),mon,alpha);
		snd(snd(e)) = typeExpr(l,snd(snd(e)),clone(mon,alpha,POSV),0);
		popExpectedType();
		resolveType(l,e,"exception handler",mon,alpha,POSV);
		fst(snd(e)) = pair(dict,fst(snd(e)));
					  }
					  break;

	case TEMPLEXP   : typeTempl(l,e,typ,off);
		break;

	case ACTEXP     : {   Int beta = newTyvars(2);
		bindTv(beta,clone(var,localState,INV),0);
		bindTv(beta+1,typeUnit,0);
		match(l,e,"action",typeAction,0,typ,off);
		snd(e) = typeExpr(l,snd(e),typeOst,beta);
					  }
					  break;

	case REQEXP     : {   Int beta = newTyvars(2);
		bindTv(beta,clone(var,localState,INV),0);
		match(l,e,"request",reqof,beta+1,typ,off);
		snd(e) = typeExpr(l,snd(e),typeOst,beta);
					  }
					  break;
#endif
	case DOCOMP	: typeDo(l,e,typ,off);
		break;

	case COMP	: {   Int beta = newTyvars(1);
		match(l,e,"list",listof,beta,typ,off);
		typeComp(l,listof,snd(e),snd(snd(e)),var,beta);
				  }
				  break;

#if LAZY_ST
	case RUNST	: typeRunST(l,e,typ,off);
		break;
#endif

	case ESIGN	: typeEsign(l,e,typ,off);
		return fst(snd(e));

	case CASE	: typeCase(l,e,typ,off);
		break;

	case LAMBDA	: typeLambda(l,e,typ,off);
		break;

	case STRUCTVAL	: typeStruct(e,typ,off);
		break;
		/* The remaining cases can only occur in pattern mode: */

	case WILDCARD	: break;

	case ASPAT	: {   Int beta = newTyvars(1);
		fst(snd(e)) = typeExpr(l,fst(snd(e)),var,beta);
		snd(snd(e)) = typeExpr(l,snd(snd(e)),var,beta);
		match(l,e,"as-pattern",var,beta,typ,off);
				  }
				  break;

	case LAZYPAT	: snd(e) = typeExpr(l,snd(e),typ,off);
		break;

#if NPLUSK
	case ADDPAT	: {   Int alpha = newTyvars(1);
		match(l,e,"n+k pattern",typeVarToVar,alpha,typ,off);
		return ap(e,assumeEvid(predIntegral,alpha));
				  }
#endif

	default 	: {Int code_ = whatIs(e); internal("typeExpr3");}
	}

	return e;
}

static Type local freshSig(Type t)
{
	switch (whatIs(t)) {
	case POLYTYPE : monoTypeOf(t) = freshSig(monoTypeOf(t));
		break;
	case QUAL     : snd(snd(t)) = freshSig(snd(snd(t)));
		break;
	case (int)NULL: 
	case OFFSET   : 
	case INTCELL  : 
	case TYCON    :
	case TUPLE    : break;;
	case AP       : fst(t) = freshSig(fst(t));
		snd(t) = freshSig(snd(t));
		break;;
	case WILDTYPE : { Int beta = newTyvars(1);
		tyvar(beta)->kind = snd(t);
		return mkInt(beta);
					}
	default       : internal("freshSig");
	}
	return t;
}

/* instantiate type expr, if nonNull*/
static Void local instantiate(Type type, Type *t, Int  *o, List *p)
{
	Int  off = 0;
	List ps  = NIL;

	if (nonNull(type)) {		/* instantiate type expression ?  */

		if (isPolyType(type)) { 	/* Polymorphic type scheme ?	  */
			off  = newKindedVars(polySigOf(type),tcMode!=EXPRESSION);
			type = monoTypeOf(type);
		}

		if (whatIs(type)==QUAL) {	/* Qualified type?		  */
			ps   = fst(snd(type));
			type = snd(snd(type));
		}
	}
	*t = type;
	*o = off;
	if (p) *p = ps;
}

/* make translation of var v*/
/* with given type adding any */
/* extra dict params required */
static Cell local varIntro(Int  l, Cell v, String wh,
						   Type type, Type typ, Int  off)
{
	/* N.B. In practice, v will either be a NAME or a VARID/OPCELL	   */
	Type t;
	Int  o;
	List ps;

	instantiate(type,&t,&o,&ps);
	if (!isName(v))     /* Only clone vars, since global names have no ftv */
		t = clone(t,-1,tcMode==OLD_PATTERN ? POSV : NEGV);
	else if (isCfun(v) && tcMode!=EXPRESSION) {   /* Handle existential quantification */
		Type h = t;
		while (isFun(h)) h = rng(h);
		skolemOffsetsAbove(tycon(getHead(h)).arity,t,o);
	}

	for (; nonNull(ps); ps=tl(ps))
		v = ap(v,assumeEvid(hd(ps),o));
	match(l,v,wh,t,o,typ,off);
	return v;
}

#if LAZY_ST
/* Type check encapsulation	   */
static Void local typeRunST(Int  line, Cell e, Type typ, Int  off)
{
	static String enc = "state encapsulation";
	Int    beta       = newTyvars(2);
	List   savePreds  = preds;

	saveSkolem();

	preds = NIL;
	skolemTyvar(beta);
	bindTv(beta+1,typ,off);
	snd(e) = typeExpr(line,snd(e),typeSTab,beta);

	restoreSkolem();

	clearMarks();
	mapProc(markAssumList,defnBounds);
	mapProc(markAssumList,varsBounds);
#if OBJ
	markAssumList(stateBounds);
#endif
	mapProc(markPred,savePreds);

	savePreds = elimConstPreds(line,savePreds);
	if (nonNull(hpreds) && resolveDefs(genvarType(typeSTab,beta,NIL),hpreds))
		savePreds = elimConstPreds(line,savePreds);
	markTyvar(beta+1);

	if (tyvar(beta)->offs==FIXED_TYVAR) {
		Type actual = copyType(typeSTab,beta);
		if (nonNull(hpreds))
			actual = ap(QUAL,pair(copyPreds(hpreds),actual));
		tyvar(beta)->offs = UNUSED_GENERIC;
		escapeError(line,snd(e),actual,copyType(typeSTab,beta));
	}
	preds = savePreds;
}
#endif

/* Type check expression type sig  */
static Void local typeEsign(Int  l, Cell e, Type typ, Int  off)
{
	List ps, savePreds = preds;
	Int  o, alpha   = newTyvars(1);
	Type t, nt;			/* nt : complete inferred type	   */

	saveSkolem();

	snd(snd(e)) = freshSig(snd(snd(e)));
	instantiate(snd(snd(e)),&t,&o,&ps);
	skolemType(t,o);
	bindTv(alpha,t,o);
	preds = makeEvidArgs(ps,o);

	pushOneGoal(alpha);
	fst(snd(e)) = typeExpr(l,fst(snd(e)),var,alpha);
	popGoal();

	restoreSkolem();

	clearMarks();
	mapProc(markAssumList,defnBounds);
	mapProc(markAssumList,varsBounds);
#if OBJ
	markAssumList(stateBounds);
#endif
	mapProc(markPred,savePreds);

	savePreds = elimConstPreds(l,savePreds);
	if (nonNull(hpreds) && resolveDefs(genvarTyvar(alpha,NIL),hpreds))
		savePreds = elimConstPreds(l,savePreds);
	snd(snd(e)) = elimWildtypes(snd(snd(e)));
	resetGenericsFrom(0);
	nt = copyTyvar(alpha);		/* order of copying is *important* */
	nt = generalize(copyPreds(hpreds),nt);

	if (!checkSchemes(snd(snd(e)),nt))
		tooGeneral(l,fst(snd(e)),snd(snd(e)),nt);

	match(l,fst(snd(e)),"annotated expression",var,alpha,typ,off);
	preds = revOnto(preds,savePreds);
}

#if OBJ
static Void local typeHandler(List as, Type typ, Int  off)
{
	List as1 = as;

	for (; nonNull(as1); as1=tl(as1)) {
		Int l        = rhsLine(snd(hd(as1)));
		saveSkolem2();
		enterBindings();
		fst(hd(as1)) = typeFreshPat(l,fst(hd(as1)),typeError,0);
		snd(hd(as1)) = typeRhs(snd(hd(as1)),clone(typ,off,POSV),0);
		leaveBindings();
		checkAndRestoreSkolem2(l,typ,off);
	}
}

static Void local typeTempl(
	Int  l,
	Cell e,       /* snd :: (([Decl],([Var],[Gen])),(Exp,[Alt])) */
	Type typ,
	Int  off)
{
	Int beta = newTyvars(1);
	List bs  = fst(fst(snd(e)));
	List vs  = fst(snd(fst(snd(e))));
	List gs  = snd(snd(fst(snd(e))));
	List oldStateBounds  = stateBounds;
	Int  oldLocalState   = localState;
	Bool oldStateVisible = stateVisible;
	List savePreds       = preds;
	stateBounds          = NIL;
	stateVisible         = FALSE;

	/* Check that a template is expected */
	match(l,e,"template",templof,beta,typ,off);
	saveSkolem();
	saveSkolem2();
	enterBindings();
	preds = NIL;

	/* Assume fresh tyvar for each state variable */
	for (; nonNull(bs); bs=tl(bs)) {	
		List vs = fst(hd(bs));
		List ts = fst(snd(hd(bs)));
		for (; nonNull(vs); vs=tl(vs))
			if (nonNull(ts)) {
				hd(ts) = freshSig(hd(ts));
				newStateBind(l,hd(vs),hd(ts));
				ts = tl(ts);
			}
			else
				newStateBind(l,hd(vs),NIL);
	}

	/* Assume fresh Ref type for "self" */
	localState = newTyvars(1);
	typeFreshPat(l,varSelf,refof,localState);

	/* Assume fresh type for each local instance var */      
	for (; nonNull(vs); vs=tl(vs))
		newVarsBind(hd(vs),newTyvars(1));

	/* Treat each state decl as a pattern binding */
	for (bs=fst(fst(snd(e))); nonNull(bs); bs=tl(bs)) {
		Cell eqn     = snd(snd(hd(bs)));
		Int  line    = rhsLine(snd(eqn));
		Int  alpha   = newTyvars(1);

		pushOneGoal(alpha);
		stateVisible = TRUE;
		tcMode       = OLD_PATTERN;
		fst(eqn)     = typeExpr(line,fst(eqn),var,alpha);
		tcMode       = EXPRESSION;
		stateVisible = FALSE;
		snd(eqn)     = typeRhs(snd(eqn),var,alpha);
		popGoal();
	}

	/* Typecheck each local instance generator */
	for (; nonNull(gs); gs=tl(gs)) {
		Int alpha = newTyvars(1);
		tcMode = OLD_PATTERN;
		fst(hd(gs)) = typeExpr(l,fst(hd(gs)),var,alpha);
		tcMode = EXPRESSION;
		snd(hd(gs)) = typeExpr(l,snd(hd(gs)),templof,alpha);
		if (!failFree(fst(hd(gs)))) {
			ERRMSG(l) "Local instance patterns must be failure-free"
				EEND;
		}
	}

	/* Typecheck comm interface w.r.t. expected type */
	fst(snd(snd(e)))  = typeExpr(l,fst(snd(snd(e))),var,beta);
	/* Typecheck default handler w.r.t. inferred type for "self" */
	typeHandler(snd(snd(snd(e))),ap(ap(typeO,mkInt(localState)),typeUnit),0);
	/* Resolve all inferred requirements on state vars */
	resolveBindings(l,"state definition",stateBounds,NONV);

	/* Compute type of the local state tuple */
	{   
		Int  n  = 0;
		Int  as = stateBounds;
		Type state;

		for (stateBounds=NIL; nonNull(as); as=tl(as), n++)
			stateBounds = cons(hd(as),stateBounds);

		if (n==0)
			state = typeUnit;
		else if (n==1)
			state = snd(hd(stateBounds));
		else {
			state = mkTuple(n);
			for (as=stateBounds; nonNull(as); as=tl(as))
				state = ap(state,snd(hd(as)));
		}
		/* Relate type of "self" to local state tuple */
		bindTv(cloneTv(localState,INV),state,0);
	}
	/* Resolve all requirements on "self", including relation to state tuple, */
	/* in addition to the inferred constraints on all local instance vars.    */
	resolveBindings(l,"local template bindings",hd(varsBounds),NEGV); /* yes */

	/* Simplify state var preds */
	leaveBindings();
	checkAndRestoreSkolem2(l,typ,off);
	restoreSkolem();
	clearMarks();		       	/* mark fixed variables */
	mapProc(markAssumList,defnBounds);
	mapProc(markAssumList,varsBounds);
	mapProc(markPred,savePreds);

	savePreds = elimConstPreds(l,savePreds);
	if (nonNull(hpreds) && resolveDefs(genvarAnyAss(stateBounds),hpreds))
		savePreds = elimConstPreds(l,savePreds);

	/* Check type signatures in state decls */
	//    resetGenericsFrom(0);
	//    for (bs=fst(fst(snd(e))); nonNull(bs); bs=tl(bs)) {
	//        List vs = fst(hd(bs));
	//        List ts = fst(snd(hd(bs)));
	//        for (; nonNull(vs); vs=tl(vs))
	//            if (nonNull(ts)) {
	//		Cell ass = findInAssumList(textOf(hd(vs)),stateBounds);
	//		Type t   = generalize(copyPreds(hpreds),copyTyvar(intOf(snd(ass))));
	//                  hd(ts) = elimWildtypes(hd(ts));
	//		if (!checkSchemes(t,hd(ts)))
	//		    tooGeneral(l,hd(vs),hd(ts),t);
	//		ts = tl(ts);
	//            }
	//    }

	/* Clean up */
	preds          = revOnto(preds,savePreds);
	stateVisible   = oldStateVisible;
	stateBounds    = oldStateBounds;
	localState     = oldLocalState;

	mapProc(removeTypeSigs,fst(fst(snd(e))));       /* Remove binding type info */

	/* Make sure we have the required dictionaries for translation (hackish...) */
	if (isNull(dictMonadTempl)) {
		Int beta = newTyvars(1);
		tyvar(beta)->kind = starToStar;
		bindTv(beta,typeTemplate,0);
		dictMonadTempl    = assumeEvid(predMonad,beta);
		dictFixMonadTempl = assumeEvid(predFixMonad,beta);
		if (isNull(dictMonadTempl) || isNull(dictFixMonadTempl)) {
			ERRMSG(l) "Prelude does not define required Template instances"
				EEND;
		}
	}

}
#endif	

/* type check comprehension	   */
static Void local typeComp(
	Int  l,
	Type m,					/* monad (mkOffset(0))		   */
	Cell e,
	List qs,
	Type typ,
	Int  off)
{
	static String boolQual = "boolean qualifier";
	static String genQual  = "generator";

	if (isNull(qs))			/* no qualifiers left		   */
		fst(e) = typeExpr(l,fst(e),typ,off);
	else {
		Cell q   = hd(qs);
		List qs1 = tl(qs);
		switch (whatIs(q)) {
		case BOOLQUAL   : snd(q) = typeExpr(l,snd(q),typeBool,0);
			typeComp(l,m,e,qs1,typ,off);
			break;

		case QWHERE     : saveSkolem2();
			enterBindings();
			mapProc(typeBindings,snd(q));
			typeComp(l,m,e,qs1,typ,off);
			resolveBindings(l,"let binding",hd(varsBounds),NEGV); /* yes */
			leaveBindings();
			checkAndRestoreSkolem2(l,typ,off);
			break;

		case FROMQUAL   : {   Int beta = newTyvars(1);
			List bs;
			saveSkolem2();
			enterBindings();
			fst(snd(q)) = typeFreshPat(l,fst(snd(q)),var,beta);
			bs = hd(varsBounds);
			leaveBindings();
			snd(snd(q)) = typeExpr(l,snd(snd(q)),clone(m,-1,POSV),beta);
			enterBindings();
			hd(varsBounds) = bs;
			typeComp(l,m,e,qs1,typ,off);
			leaveBindings();
			resolveType(l,fst(snd(q)),"generator pattern",var,beta,NEGV); /* yes */
			checkAndRestoreSkolem2(l,typ,off);
						  }
						  break;

		case DOQUAL     : snd(q) = typeExpr(l,snd(q),clone(m,-1,POSV),newTyvars(1));
			typeComp(l,m,e,qs1,typ,off);
			break;
#if OBJ
		case ASSIGNQ    : {   Int alpha = newTyvars(1);
			Int line  = intOf(fst(snd(snd(q))));
			pushOneGoal(alpha);
			tcMode = OLD_PATTERN;
			fst(snd(q)) = typeExpr(line,fst(snd(q)),var,alpha);
			tcMode = EXPRESSION;
			snd(snd(q)) = typeRhs(snd(snd(q)),var,alpha);
			popGoal();
			refState = TRUE;
			typeComp(l,m,e,qs1,typ,off);
						  }
						  break;

		case WHILEDO    : fst(snd(q)) = typeExpr(l,fst(snd(q)),typeBool,0);
			snd(snd(q)) = typeExpr(l,snd(snd(q)),clone(m,-1,POSV),newTyvars(1));
			typeComp(l,m,e,qs1,typ,off);
			break;

		case FORALLDO   : {   Int beta = newTyvars(1);
			List bs;
			saveSkolem2();
			enterBindings();
			fst3(snd(q)) = typeFreshPat(l,fst3(snd(q)),var,beta);
			bs = hd(varsBounds);
			leaveBindings();
			snd3(snd(q)) = typeExpr(l,snd3(snd(q)),listof,beta);
			enterBindings();
			hd(varsBounds) = bs;
			thd3(snd(q)) = typeExpr(l,thd3(snd(q)),
				clone(m,-1,POSV),newTyvars(1));
			leaveBindings();
			resolveType(l,fst3(snd(q)),"forall pattern",var,beta,NEGV); /* yes */
			checkAndRestoreSkolem2(l,typ,off);
			typeComp(l,m,e,qs1,typ,off);
						  }
						  break;

		case FIXDO      : {   List vs = fst(snd(q));
			List gs = snd(snd(q));
			saveSkolem2();
			enterBindings();
			for (; nonNull(vs); vs=tl(vs))
				newVarsBind(hd(vs),newTyvars(1));
			for (; nonNull(gs); gs=tl(gs)) {
				Int alpha = newTyvars(1);
				tcMode = OLD_PATTERN;
				fst(hd(gs)) = typeExpr(l,fst(hd(gs)),var,alpha);
				tcMode = EXPRESSION;
				snd(hd(gs)) = typeExpr(l,snd(hd(gs)),clone(m,-1,POSV),alpha);
			}
			typeComp(l,m,e,qs1,typ,off);
			resolveBindings(l,"fix binding",hd(varsBounds),NEGV); /* yes */
			leaveBindings();
			checkAndRestoreSkolem2(l,typ,off);
						  }
						  break;
#endif
		}
	}
}

/* type check do-notation	   */
static Void local typeDo(Int  l, Cell e, Type typ, Int  off)
{
	static String stmts  = "statement sequence";
	Int  alpha		 = newTyvars(1);
	Int  beta		 = newTyvars(1);
	Cell mon, m;
#if OBJ
	Bool oldStateVisible = stateVisible;
	Bool oldRefState     = refState;
	stateVisible         = TRUE;
	refState             = FALSE;
#endif
	tyvar(beta)->kind    = starToStar;
	mon                  = ap(mkInt(beta),var);
	m                    = assumeEvid(predMonad,beta);

	match(l,e,stmts,mon,alpha,typ,off);
	pushExpectedType(mon,alpha);
	typeComp(l,mon,snd(e),snd(snd(e)),clone(mon,-1,POSV),alpha);
	snd(e) = pair(triple(m,compZero(snd(snd(e)),beta),compFix(snd(snd(e)),beta)),snd(e));
#if OBJ
	if (refState) {
		match(l,e,stmts,clone(ap(ap(typeO,mkInt(localState)),var),-1,INV), alpha,
			clone(mon,-1,POSV),                                alpha);
	}
	popExpectedType();
	resolveType(l,e,stmts,mon,alpha,POSV);
	refState     = oldRefState;
	stateVisible = oldStateVisible;
#endif
}

/* return evidence for Monad0 beta */
/* if needed for qualifiers qs	   */
static Cell local compZero(List qs, Int  beta)
{
	for (; nonNull(qs); qs=tl(qs))
		switch (whatIs(hd(qs))) {
		case FROMQUAL : if (failFree(fst(snd(hd(qs)))))
							break;
			/* intentional fall-thru */
		case BOOLQUAL : return assumeEvid(predMonad0,beta);
	}
	return NIL;
}

/* return evidence for MonadFix beta */
/* if needed by qualifiers qs        */
static Cell local compFix(List qs, Int beta)
{
	for (; nonNull(qs); qs=tl(qs))
		if (whatIs(hd(qs))==FIXDO)
			return assumeEvid(predFixMonad,beta);
	return NIL;
}

/* find type of pattern,       */
/* assigning fresh type variables   */
/* to each var bound in the pattern */
static Cell local typeFreshPat(Int  l, Cell p, Type typ, Int  off)
{
	tcMode = NEW_PATTERN;
	p	   = typeExpr(l,p,typ,off);
	tcMode = EXPRESSION;
	return p;
}

static Bool local isSimplePat(Cell c)
{
	switch (whatIs(c)) {
	case VARIDCELL : 
	case VAROPCELL : 
	case WILDCARD  : return TRUE;
	case ASPAT     : return isSimplePat(snd(snd(c)));
	case LAZYPAT   : return isSimplePat(snd(c));
	}
	return FALSE;
}

/* --------------------------------------------------------------------------
* Function types are generated as necessary.  The most common n-ary types
* (n<MAXTUPCON) are held in a cache to avoid repeated generation of the 
* function types. Note that the offsets for an n-ary function type are
* n -> n-1 -> ... -> 1 -> 0
* ------------------------------------------------------------------------*/

#define MAXFUNC 10
static Type funcTypeCache[MAXFUNC];

static Type funcType(Int n)
{
	if (n>=MAXFUNC)
		return makeFuncType(n);
	else if (nonNull(funcTypeCache[n]))
		return funcTypeCache[n];
	else
		return funcTypeCache[n] = makeFuncType(n);
}

static Type makeFuncType(Int n)
{
	if (n==0)
		return var;
	else
		return ap(ap(typeArrow,mkOffset(n)),funcType(n-1));
}

/* --------------------------------------------------------------------------
* Note the pleasing duality in the typing of application and abstraction:-)
* ------------------------------------------------------------------------*/

/* Type check application	   */
static Cell local typeAp(Int  l, Cell e, Type typ, Int  off)
{
	Cell h    = getHead(e);		/* e = h e1 e2 ... en		   */
	Int  n    = argCount;		/* save no. of arguments	   */
	Int  beta = newTyvars(n+1);
	Type t    = funcType(n);
	Cell p    = NIL;			/* points to previous AP node	   */
	Cell a    = e;			/* points to current AP node	   */
	Int  i;

#if 1
	bindTv(beta,typ,off);		/* expected type is t_0		   */
	h = typeExpr(l,h,t,beta);
	pushExpectedType(t,beta);
	for (i=1; i<=n; i++) {		/* check e_i::t_i for each i > 0   */
		pushOneGoal(beta+i);
		arg(a) = typeExpr(l,arg(a),var,cloneTv(beta+i,POSV));
		popGoal();
		p = a;
		a = fun(a);
	}
	popExpectedType();
	resolveType(l,h,"function application",t,beta,NONV);
	fun(p) = h;				/* replace head with translation   */
	return e;
#else

	bindTv(beta,typ,off);		/* expected type is t_0		   */
	for (i=1; i<=n; i++) {		/* check e_i::t_i for each i > 0   */
		pushOneGoal(beta+i);
		arg(a) = typeExpr(l,arg(a),var,beta+i);
		popGoal();
		p = a;
		a = fun(a);
	}
	h = typeExpr(l,h,t,beta);
	fun(p) = h;				/* replace head with translation   */
	return e;
#endif
}

static Void local typeLambda(Int  l, Cell e, Type typ, Int  off)
{
	List ps   = fst(snd(e));
	Int  n    = length(ps);
	Type t    = funcType(n);
	Int  beta = newTyvars(n+1);
	Int  i;

	match(l,e,"abstraction",t,beta,typ,off);
	saveSkolem2();
	enterBindings();
	for (i=n; i>0; --i) {
		hd(ps) = typeFreshPat(l,hd(ps),var,beta+i);
		ps = tl(ps);
	}
	snd(snd(e)) = typeRhs(snd(snd(e)),var,beta);
	leaveBindings();
	resolveType(l,e,"lambda abstraction",t,beta,NONV);
	checkAndRestoreSkolem2(l,typ,off);
}

static Void local typeAlts(Cell f, List as, Type typ, Int  off)
{
	Int  line  = rhsLine(snd(hd(as)));
	Int  n     = length(fst(hd(as)));
	Int  alpha = newTyvars(n+1);
	Int  beta  = newTyvars(n+1);
	Type t     = funcType(n);
	List bs    = NIL;
	List as1;
	Int  i;

	match(line,f,"function definition",t,alpha,typ,off);
	bindTv(beta,var,alpha);
	for (i=n; i>0; i--)
		unify(var,alpha+i,var,cloneTv(beta+i,POSV));

	saveSkolem2();
	for (as1=as; nonNull(as1); as1=tl(as1)) {
		List ps = fst(hd(as1));
		Int  l  = rhsLine(snd(hd(as1)));
		enterBindings();
		for (i=n; i>0; --i,ps=tl(ps)) {
			hd(ps) = typeFreshPat(l,hd(ps),var,cloneTv(beta+i,POSV));    /* POSV! */
		}
		bs = cons(hd(varsBounds),bs);
		leaveBindings();
	}
	tcMode = OLD_PATTERN;
	resolveType(line,f,"function patterns",t,beta,NONV);
	tcMode = EXPRESSION;

	pushExpectedType(var,beta);
	for (as1=as, bs=rev(bs); nonNull(as1); as1=tl(as1), bs=tl(bs)) {
		Int l = rhsLine(snd(hd(as1)));
		enterBindings();
		hd(varsBounds) = hd(bs);
		snd(hd(as1)) = typeRhs(snd(hd(as1)),var,cloneTv(beta,POSV));
		leaveBindings();
	}
	popExpectedType();
	resolveType(line,f,"function alternatives",var,beta,POSV);
	resolveType(line,f,"function patterns",t,beta,NONV);
	checkAndRestoreSkolem2(line,typ,off);
}

static Void typeCase(Int  l, Cell e, Type typ, Int  off)
{
	List as    = snd(snd(e));
	Int  line  = rhsLine(snd(hd(as)));
	Int  beta  = newTyvars(2);    		/* result, discr */
	Type t     = funcType(1);
	List bs    = NIL;
	List as1;

	bindTv(beta,typ,off);
	saveSkolem2();

	for (as1=as; nonNull(as1); as1=tl(as1)) {
		Int l = rhsLine(snd(hd(as1)));
		enterBindings();
		fst(hd(as1)) = typeFreshPat(l,fst(hd(as1)),var,cloneTv(beta+1,POSV));
		bs = cons(hd(varsBounds),bs);
		leaveBindings();
	}
	tcMode = OLD_PATTERN;
	resolveType(line,e,"case patterns",var,beta+1,NONV);
	tcMode = EXPRESSION;

	fst(snd(e)) = typeExpr(l,fst(snd(e)),var,beta+1);

	pushExpectedType(var,beta);
	for (as1=as, bs=rev(bs); nonNull(as1); as1=tl(as1), bs=tl(bs)) {
		Int l    = rhsLine(snd(hd(as1)));
		enterBindings();
		hd(varsBounds) = hd(bs);
		snd(hd(as1)) = typeRhs(snd(hd(as1)),var,cloneTv(beta,POSV));
		leaveBindings();
	}
	popExpectedType();
	resolveType(line,e,"case alternatives",var,beta,POSV);
	resolveType(line,e,"case patterns",var,beta+1,NEGV);
	checkAndRestoreSkolem2(l,typ,off);
}

/* type check struct expression   */
static Void local typeStruct(Cell e, Type typ, Int  off)
{
	Int  line = intOf(fst(snd(e)));
	Int  beta = newTyvars(1);
	List bs   = snd(snd(e));
	List ts   = NIL;
	List bs1;

	saveSkolem2();

	for (bs1=bs; nonNull(bs1); bs1=tl(bs1)) {
		Int  alpha = newTyvars(1);
		Type t;
		Int  o, arity;

		instantiate(name(fst(hd(bs1))).type,&t,&o,0);
		bindTv(cloneTv(beta,NEGV),dom(t),o);	/* can assume function type*/
		bindTv(alpha,rng(t),o);
		ts = cons(mkInt(alpha),ts);

		arity = tycon(getHead(dom(t))).arity;
		skolemOffsetsAbove(arity,rng(t),o);
	}
	bindTv(cloneTv(beta,NEGV),typ,off);
	tcMode = OLD_PATTERN;
	resolveType(line,e,"struct",var,beta,NONV);
	tcMode = EXPRESSION;

	pushExpectedType(var,beta);
	for (bs1=bs, ts=rev(ts); nonNull(bs1); bs1=tl(bs1), ts=tl(ts)) {
		Int l     = rhsLine(snd(hd(snd(snd(hd(bs1))))));
		Int alpha = cloneTv(intOf(hd(ts)),POSV);

		pushOneGoal(alpha);
		typeAlts(fst(hd(bs1)),snd(snd(hd(bs1))),var,alpha);
		popGoal();
	}
	popExpectedType();
	resolveType(line,e,"struct",var,beta,POSV);
	checkAndRestoreSkolem2(line,typ,off);

	checkAllDefined(line,bs,beta);
	mapProc(removeTypeSigs,bs);
}

/* check that bs defines  */
/* all selectors for beta */
static Void local checkAllDefined(Int  line, List bs, Int  beta)
{
	List ts,bs1;
	Type t = getDerefHead(var,beta);
	if (!isTycon(t)) {
		ERRMSG(line) "Ambiguous struct expression" EEND;
	}
	for (ts=cons(t,tycon(t).axioms); nonNull(ts); ts=tl(ts)) {
		Tycon h = getHead(monoType(hd(ts)));
		List sels = tycon(h).defn;
		for (; nonNull(sels); sels=tl(sels)) {
			for (bs1=bs; nonNull(bs1); bs1=tl(bs1)) {
				if (hd(sels)==fst(hd(bs1)))
					break;
			}
			if (isNull(bs1)) {
				ERRMSG(line) 
					"Selector \"%s\" missing in struct", 
					(textToStr(name(hd(sels)).text)+1)
					EEND;
			}
		}
	}
}

/* --------------------------------------------------------------------------
* Tuple type constructors: are generated as necessary.  The most common
* n-tuple constructors (n<MAXTUPCON) are held in a cache to avoid
* repeated generation of the constructor types.
* ------------------------------------------------------------------------*/

#define MAXTUPCON 10
static Type tupleConTypes[MAXTUPCON];

/* find type for tuple con, using */
/* tupleConTypes to cache previously*/
static Void local typeTuple(Int  l, Cell e, Type typ, Int  off)
{
	Int  n = tupleOf(e);	       /* calculated tuple constr. types.  */
	Type t;
	Int  o = newTyvars(n);

	if (n>=MAXTUPCON)
		t = makeTupleType(n);
	else if (tupleConTypes[n])
		t = tupleConTypes[n];
	else
		t = tupleConTypes[n] = makeTupleType(n);
	match(l,e,"tuple",t,o,typ,off);
}

/* construct type for tuple constr. */
/* t1 -> ... -> tn -> (t1,...,tn)   */
static Type local makeTupleType(Int n)
{
	Type h = mkTuple(n);
	Int  i;

	for (i=0; i<n; ++i)
		h = ap(h,mkOffset(i));
	while (0<n--)
		h = fn(mkOffset(n),h);
	return h;
}

/* --------------------------------------------------------------------------
* Type check group of bindings:
* ------------------------------------------------------------------------*/

static Void local typeBindings(List bs)	/* type check a binding group	   */
{
	Bool usesPatBindings = FALSE;	/* TRUE => pattern binding in bs   */
	Bool usesUntypedVar  = FALSE;	/* TRUE => var bind w/o type decl  */
	List bs1;

	/* The following loop is used to determine whether the monomorphism	   */
	/* restriction should be applied.  It could be written marginally more */
	/* efficiently by using breaks, but clarity is more important here ... */

	for (bs1=bs; nonNull(bs1); bs1=tl(bs1)) {  /* Analyse binding group    */
		Cell b = hd(bs1);
		if (!isVar(fst(b)))
			usesPatBindings = TRUE;
		else if (isNull(fst(hd(snd(snd(b))))) && isNull(fst(snd(b))))
			usesUntypedVar  = TRUE;
	}

	hd(defnBounds) = NIL;
	hd(depends)	   = NIL;

	if (usesPatBindings || usesUntypedVar)
		monorestrict(bs);
	else
		unrestricted(bs);

	mapProc(removeTypeSigs,bs);		       /* Remove binding type info */
	hd(varsBounds) = revOnto(hd(defnBounds),   /* transfer completed assmps*/
		hd(varsBounds));  /* out of defnBounds        */
	hd(defnBounds) = NIL;
	hd(depends)    = NIL;
}

static Void local removeTypeSigs(Cell b)    /* Remove type info from a binding  */
{
	snd(b) = snd(snd(b));
}

/* --------------------------------------------------------------------------
* Type check a restricted binding group:
* ------------------------------------------------------------------------*/

static Void local monorestrict(List bs)	/* Type restricted binding group   */
{
	List savePreds = preds;
	Int  line 	   = isVar(fst(hd(bs))) ? rhsLine(snd(hd(snd(snd(hd(bs))))))
		: rhsLine(snd(snd(snd(hd(bs)))));

	saveSkolem();

	hd(depends) = NODEPENDS;	       /* No need for dependents here	   */
	preds       = NIL;

	mapProc(restrictedBindAss,bs);     /* add assumptions for vars in bs   */

	pushGoal(NIL);		       /* Use defnBounds as goal	   */
	mapProc(typeBind,bs);	       /* type check each binding	   */
	resolveBindings(bindingsLine(bs),"recursive binding",hd(defnBounds),NONV); /* ? */
	popGoal();

	restoreSkolem();
	clearMarks();		       /* mark fixed variables		   */
	mapProc(markAssumList,tl(defnBounds));
	mapProc(markAssumList,tl(varsBounds));
#if OBJ
	markAssumList(stateBounds);
#endif
	mapProc(markPred,savePreds);
	if (nonNull(tl(defnBounds)))
		mapProc(markPred,preds);

	savePreds = elimConstPreds(line,savePreds);

	if (isNull(tl(defnBounds))) {	/* top-level may need defaulting   */
		if (nonNull(hpreds) &&
			resolveDefs(genvarAnyAss(hd(defnBounds)),hpreds))
			savePreds = elimConstPreds(line,savePreds);

		if (nonNull(preds) && resolveDefs(NIL,hpreds))   /* Nearly Hask 1.4*/
			savePreds = elimConstPreds(line,savePreds);

		if (nonNull(preds)) {		/* look for unresolved overloading */
			Cell v   = isVar(fst(hd(bs))) ? fst(hd(bs)) : hd(fst(hd(bs)));
			Cell ass = findInAssumList(textOf(v),hd(varsBounds));

			ERRMSG(line) "Unresolved top-level overloading" ETHEN
				reportBindings(hd(defnBounds));
			ERRTEXT     "\n*** Outstanding context : " ETHEN
				ERRCONTEXT(copyPreds(hpreds));
			ERRTEXT     "\n"
				EEND;
		}
	}
	preds     = appendOnto(preds,savePreds);
	hpreds    = NIL;

	map2Proc(genBind,NIL,NIL,bs);	/* Generalize types of def'd vars  */
}

/* make assums for vars in binding  */
/* gp with restricted overloading   */
static Void local restrictedBindAss(Cell b)
{

	if (isVar(fst(b))) {	       /* function-binding?		   */
		fst(snd(b)) = freshSig(fst(snd(b)));
		restrictedAss(rhsLine(snd(hd(snd(snd(b))))),
			fst(b),
			fst(snd(b)));
	}
	else {			       /* pattern-binding?		   */
		List vs   = fst(b);
		List ts   = fst(snd(b));
		Int  line = rhsLine(snd(snd(snd(b))));

		for (; nonNull(vs); vs=tl(vs))
			if (nonNull(ts)) {
				hd(ts) = freshSig(hd(ts));
				restrictedAss(line,hd(vs),hd(ts));
				ts = tl(ts);
			}
			else
				restrictedAss(line,hd(vs),NIL);
	}
}

/* Assume that type of binding var v*/
/* is t (if nonNull) in restricted  */
/* binding group 		   */
static Void local restrictedAss(Int  l, Cell v, Type t)
{
	List savePreds = preds;
	newDefnBind(v,t);
	if (preds != savePreds) {
		ERRMSG(l) "Explicit overloaded type for \"%s\"",textToStr(textOf(v))
			ETHEN
			ERRTEXT   " not permitted in restricted binding"
			EEND;
	}
}

/* --------------------------------------------------------------------------
* Unrestricted binding group:
* ------------------------------------------------------------------------*/

static Void local unrestricted(List bs)	/* Type unrestricted binding group */
{
	Int  line      = rhsLine(snd(hd(snd(snd(hd(bs))))));
	List savePreds = preds;
	List bs1;    

	saveSkolem();
	preds = NIL;


	/* Add assumptions about   */
	/* each bound var -- can   */
	for (bs1=bs; nonNull(bs1); bs1=tl(bs1)) {
		Cell b = hd(bs1);

		fst(snd(b)) = freshSig(fst(snd(b)));
		newDefnBind(fst(b),fst(snd(b)));	/* assume function binding */
	}

	pushGoal(NIL);				/* Use defnBounds as goal  */
	mapProc(typeBind,bs);			/* type check each binding */
	resolveBindings(bindingsLine(bs),"recursive binding",hd(defnBounds),NONV); /* ? */
	popGoal();

	restoreSkolem();
	clearMarks();				/* Mark fixed variables	   */
	mapProc(markAssumList,tl(defnBounds));
	mapProc(markAssumList,tl(varsBounds));
#if OBJ
	markAssumList(stateBounds);
#endif
	mapProc(markPred,savePreds);

	savePreds = elimConstPreds(line,savePreds);
	if (nonNull(hpreds) && resolveDefs(genvarAllAss(hd(defnBounds)),hpreds))
		savePreds = elimConstPreds(line,savePreds);

	map2Proc(genBind,preds,hpreds,bs);	/* Generalize types of def'd vars  */

	if (nonNull(preds)) {		/* Add dictionary params, if nec.  */
		map1Proc(addEvidParams,preds,hd(depends));
		map1Proc(qualifyBinding,preds,bs);
	}

	preds    = savePreds;		/* restore predicates		   */
	hpreds   = NIL;
}

/* overwrite VARID/OPCELL v with	   */
/* application of variable to evid. */
/* parameters given by qs	   */
static Void local addEvidParams(List qs, Cell v)
{
	if (nonNull(qs)) {
		Cell nv;

		if (!isVar(v))
			internal("addEvidParams");

		for (nv=mkVar(textOf(v)); nonNull(tl(qs)); qs=tl(qs))
			nv = ap(nv,thd3(hd(qs)));
		fst(v) = nv;
		snd(v) = thd3(hd(qs));
	}
}

/* --------------------------------------------------------------------------
* Type check bodies of bindings:
* ------------------------------------------------------------------------*/

static Void local typeBind(Cell b)	       /* Type check binding		   */
{
	if (isVar(fst(b))) {			       /* function binding */
		Cell ass = findTopBinding(fst(b));
		Int  alpha;

		if (isNull(ass))
			internal("typeBind");

		alpha = cloneTv(intOf(defType(snd(ass))),POSV);
		addGoal(alpha);
		typeAlts(fst(b),snd(snd(b)),var,alpha);
	}
	else {					       /* pattern binding  */
		Pair pb		     = snd(snd(b));
		Int  l		     = rhsLine(snd(pb));
		Int  alpha	     = newTyvars(1);

		addGoal(alpha);
		tcMode  = OLD_PATTERN;
		fst(pb) = typeExpr(l,fst(pb),var,alpha);    /* expecting alpha     */
		tcMode  = EXPRESSION;
		snd(pb) = typeRhs(snd(pb),var,alpha);       /* expecting Phi alpha */
	}
}

/* check type of rhs of definition  */
static Cell local typeRhs(Cell e, Type typ, Int  off)
{
	switch (whatIs(e)) {
	case GUARDED : {   Int beta = newTyvars(1);
		bindTv(beta,typ,off);
		pushExpectedType(var,beta);
		map1Proc(guardedType,beta,snd(e));
		popExpectedType();
		resolveType(rhsLine(e),NIL,"guarded alternatives",var,beta,POSV);
				   }
				   break;

	case LETREC  : {   Int line = bindingsLine(hd(fst(snd(e))));
		saveSkolem2();
		enterBindings();
		mapProc(typeBindings,fst(snd(e)));
		snd(snd(e)) = typeRhs(snd(snd(e)),typ,off);
		resolveBindings(line,"let binding",hd(varsBounds),NEGV); /* yes */
		leaveBindings();
		checkAndRestoreSkolem2(line,typ,off);
				   }
				   break;

	default      : snd(e) = typeExpr(intOf(fst(e)),snd(e),typ,off);
		break;
	}
	return e;
}

/* check type of guard (li,(gd,ex))*/
static Void local guardedType(
	Int  beta,		/* should have gd :: Bool,	   */
	Cell gded		/*	      ex :: (var,beta)	   */
	)
{
	static String guarded = "guarded expression";
	static String guard   = "guard";
	Int line = intOf(fst(gded));

	gded     = snd(gded);
	fst(gded) = typeExpr(line,fst(gded),typeBool,0);
	snd(gded) = typeExpr(line,snd(gded),var,cloneTv(beta,POSV));
}

/* find first expression on a rhs   */
Cell rhsExpr(Cell rhs)
{
	switch (whatIs(rhs)) {
	case GUARDED : return snd(snd(hd(snd(rhs))));
	case LETREC  : return rhsExpr(snd(snd(rhs)));
	default      : return snd(rhs);
	}
}

/* find line number associated with */
/* a right hand side		   */
Int rhsLine(Cell rhs)
{
	switch (whatIs(rhs)) {
	case GUARDED : return intOf(fst(hd(snd(rhs))));
	case LETREC  : return rhsLine(snd(snd(rhs)));
	default      : return intOf(fst(rhs));
	}
}

/* find line number associated with */
/* first binding in a binding list  */
Int bindingsLine(List bs)
{
	Cell b = hd(bs);
	if (isVar(fst(b)))
		return rhsLine(snd(hd(snd(snd(b)))));
	else
		return rhsLine(snd(snd(snd(b))));
}

/* --------------------------------------------------------------------------
* Calculate generalization of types and compare with declared type schemes:
* ------------------------------------------------------------------------*/

/* Generalize the type of each var */
/* defined in binding b, qualifying*/
/* each with the predicates in ps  */
/* and using Haskell predicates hps*/
static Void local genBind(List ps, List hps, Cell b)
{
	Cell v = fst(b);
	Cell t = fst(snd(b));

	if (isVar(fst(b)))
		genAss(rhsLine(snd(hd(snd(snd(b))))),ps,hps,v,t);
	else {
		Int line = rhsLine(snd(snd(snd(b))));
		for (; nonNull(v); v=tl(v)) {
			Type ty = NIL;
			if (nonNull(t)) {
				ty = hd(t);
				t  = tl(t);
			}
			genAss(line,ps,hps,hd(v),ty);
		}
	}
}

/* Calculate inferred type of v and*/
/* compare with declared type, t,  */
/* if given.  Use Haskell preds hps*/
/* to check correct unambig typing */
/* and ps to calculate GTC type    */
static Void local genAss(Int  l, List ps, List hps, Cell v, Type t)
{
	Cell ass = findTopBinding(v);
	Type it;
	Int  ng;
	Type ht;

	if (isNull(ass))
		internal("genAss");

	if (nonNull(t))
		t = elimWildtypes(t);

	resetGenericsFrom(0);		/* Calculate Haskell typing	   */
	it  = copyTyvar(intOf(defType(snd(ass))));
	ng  = nextGeneric;
	hps = copyPreds(hps);
	ht  = generalize(hps,it);

	/* If a new generic variable was   */
	/* introduced by copyHPreds, then  */
	/* the inferred type is ambiguous  */
	if (nextGeneric!=ng)
		ambigError(l,
		"inferred type",
		v,
		ht);

	if (nonNull(t) && !checkSchemes(t,ht))
		tooGeneral(l,v,t,ht);		/* Compare with declared type	   */

	snd(ass) = generalize(copyPreds(ps),it);
#ifdef DEBUG_TYPES
	printExp(stdout,v);
	printf(" :: ");
	printType(stdout,snd(ass));
	printf("\n");
#endif
}

static Type local elimWildtypes(Type ty)
{
	Type t;
	Int  o;
	List ps, mypreds;
	instantiate(ty,&t,&o,&ps);
	mypreds = makeEvidArgs(ps,o);
	resetGenericsFrom(0);
	t = copyType(t,o);
	ty = generalize(copyPreds(mypreds),t);
	return ty;
}

/* calculate generalization of t   */
/* having already marked fixed vars*/
/* with qualifying preds qs	   */
static Type local generalize(List qs, Type t)
{
	if (nonNull(qs))
		t = ap(QUAL,pair(qs,t));
	if (nonNull(genericVars)) {
		Kind k  = STAR;
		List vs = genericVars;
		for (; nonNull(vs); vs=tl(vs)) {
			Tyvar *tyv = tyvar(intOf(hd(vs)));
			Kind   ka  = tyv->kind;
			k = ap(ka,k);
		}
		t = mkPolyType(k,t);
#ifdef DEBUG_KINDS
		printf("Generalised type: ");
		printType(stdout,t);
		printf(" ::: ");
		printKind(stdout,k);
		printf("\n");
#endif
	}
	return t;
}

/* explicit type sig. too general  */
static Void local tooGeneral(Int  l, Cell e, Type dt, Type it)
{
	ERRMSG(l) "Declared type too general" ETHEN
		ERRTEXT   "\n*** Expression    : "	  ETHEN ERREXPR(e);
	ERRTEXT   "\n*** Declared type : "	  ETHEN ERRTYPE(dt);
	ERRTEXT   "\n*** Inferred type : "	  ETHEN ERRTYPE(it);
	ERRTEXT   "\n"
		EEND;
}

/* --------------------------------------------------------------------------
* Compare type schemes:
*
* In comparing declared and inferred type schemes, we require that the type
* parts of the two type schemes are identical.  However, for the predicate
* parts of the two type schemes, we require only that each inferred
* predicate is included in the list of declared predicates:
*
* e.g. Declared        Inferred
*      (Eq a, Eq a)    Eq a		OK
*      (Ord a, Eq a)   Ord a		OK
*      ()              Ord a		NOT ACCEPTED
*	Ord a		()		IMPOSSIBLE, by construction, the
*					inferred context will be at least as
*					restricted as the declared context.
* ------------------------------------------------------------------------*/

/* Compare type schemes		   */
static Bool local checkSchemes(
	Type sd,				/* declared scheme		   */
	Type si				/* inferred scheme		   */
	)
{
	Bool bd = isPolyType(sd);
	Bool bi = isPolyType(si);
	if (bd || bi) {
		if (bd && bi && eqKind(polySigOf(sd),polySigOf(si))) {
			sd = monoTypeOf(sd);
			si = monoTypeOf(si);
		}
		else
			return FALSE;
	}

	bd = (whatIs(sd)==QUAL);
	bi = (whatIs(si)==QUAL);
	if (bd && bi && checkQuals(fst(snd(sd)),fst(snd(si)))) {
		sd = snd(snd(sd));
		si = snd(snd(si));
	}
	else
		/* maybe somebody gave an   */
		/* explicitly null context? */
		if (bd && !bi && isNull(fst(snd(sd))))
			sd = snd(snd(sd));
		else if (!bd && bi && isNull(fst(snd(si))))
			si = snd(snd(si));
		else if (bd || bi)
			return FALSE;

		return equalTypes(sd,si);
}

/* Compare lists of qualifying preds*/
static Bool local checkQuals(List qsd, List qsi)
{
	for (; nonNull(qsi); qsi=tl(qsi)) {			/* check qsi < qsd */
		Cell c  = fun(hd(qsi));
		Type o  = arg(hd(qsi));
		List qs = qsd;
		for (; nonNull(qs); qs=tl(qs))
			if (c==fun(hd(qs)) && o==arg(hd(qs)))
				break;
		if (isNull(qs))
			return FALSE;
	}
	return TRUE;
}

/* Compare simple types for equality*/
static Bool local equalTypes(Type t1, Type t2)
{

et: if (whatIs(t1)!=whatIs(t2))
		return FALSE;

	switch (whatIs(t1)) {
	case TYCON    :
	case OFFSET   :
	case TUPLE    : return t1==t2;

	case INTCELL  : return intOf(t1)==intOf(t2);   //  was !=   !!!!!!!!!!!!!!!

	case AP       : if (equalTypes(fun(t1),fun(t2))) {
		t1 = arg(t1);
		t2 = arg(t2);
		goto et;
					}
					return FALSE;

	default       : internal("equalTypes");
	}

	return TRUE;/*NOTREACHED*/
}

/* --------------------------------------------------------------------------
* Type check bodies of class and instance declarations:
* ------------------------------------------------------------------------*/

/* type check implementations of   */
/* member functions for instance in*/
static Void local typeInstDefn(Inst in)
{
	Int i;
	Cell head = inst(in).t;
	List sig  = NIL;
	List k    = kindAtom(inst(in).t);

	for (i=0; i<inst(in).arity; ++i)
		head = ap(head,mkOffset(i));
	for (i=0; i<inst(in).arity; ++i, k=snd(k))
		sig  = cons(fst(k),sig);
	sig = rev(sig);

	typeMembers("instance member binding",
		cclass(inst(in).c).members,
		inst(in).implements,
		inst(in).specifics,
		dictSpecificsStart(inst(in).c),
		sig,
		head);
}

/* type check implementations of   */
/* defaults for class c		   */
static Void local typeClassDefn(Class c)
{
	typeMembers("default member binding",
		cclass(c).members,
		cclass(c).defaults,
		singleton(ap(c,var)),
		0,
		singleton(cclass(c).sig),
		var);
}

/* type check implementations `is' */
/* of members `ms' for a specific  */
/* class instance		   */
static Void local typeMembers(String wh, List ms, List is, List specifics,
							  Int spoff, List sig, Type head)
{
	while (nonNull(is)) {
		if (isName(hd(is)))
			typeMember(wh,hd(ms),hd(is),specifics,spoff,sig,head);
		is = tl(is);
		ms = tl(ms);
	}
}

/* type check implementation i of  */
/* member m for instance type head */
/* (with kinds given by sig) and   */
/* using the given specifics	   */
/* at the specified offset spoff   */
static Void local typeMember(String wh, Name   m, Name   i, List   specifics,
							 Int    spoff, List   sig, Type   head)
{
	Int  line = rhsLine(snd(hd(name(i).defn)));
	Int  o, alpha, beta;
	Type t, required, inferred;
	List ps, extras;

	saveSkolem();

#ifdef DEBUG_TYPES
	printf("Line %d, instance type: ",line);
	printType(stdout,head);
	putchar('\n');
#endif

	emptySubstitution();
	hd(defnBounds) = NIL;
	hd(depends)    = NODEPENDS;
	preds          = NIL;

	alpha    = newTyvars(1);		/* Set required instance of m	   */
	beta     = newKindedVars(sig,FALSE);
	instantiate(name(m).type,&t,&o,&ps);
	bindTv(alpha,t,o);
	bindTv(o,head,beta);
	extras   = makeEvidArgs(tl(ps),o);
	required = copyTyvar(alpha);
	skolemTyvar(alpha);

#ifdef DEBUG_TYPES
	printf("Checking implementation of: ");
	printExp(stdout,m);
	printf(" :: ");
	printType(stdout,required);
	printf("\n");
#endif

	pushOneGoal(alpha);
	typeAlts(m,name(i).defn,var,alpha);
	popGoal();

	restoreSkolem();

	if (nonNull(extras)) {		/* Now deal with predicates ...    */
		List ps = NIL;
		while (nonNull(preds)) {        /* discharge preds entailed by	   */
			List nx = tl(preds);	/* the `extras'			   */
			Cell pi = hd(preds);
			Cell ev = simpleEntails(extras,fst3(pi),intOf(snd3(pi)));
			if (nonNull(ev))
				overEvid(thd3(pi),ev);
			else {
				tl(preds) = ps;
				ps        = preds;
			}
			preds = nx;
		}
		preds = rev(ps);
		map1Proc(qualify,extras,name(i).defn);
	}

	clearMarks();
	if (nonNull(elimConstPreds(line,NIL)) ||	/* discharge const preds   */
		(resolveDefs(genvarTyvar(alpha,NIL),hpreds) &&
		nonNull(elimConstPreds(line,NIL))))
		internal("typeMember");

	resetGenericsFrom(0);
	inferred = copyTyvar(alpha);	/* Compare with inferred type	   */
	if (!equalTypes(required,inferred))
		tooGeneral(line,m,required,inferred);

#ifdef DEBUG_TYPES
	printf("preds = ");
	printContext(stdout,copyPreds(preds));
	putchar('\n');
	printf("hpreds= ");
	printContext(stdout,copyPreds(hpreds));
	putchar('\n');
#endif

	for (; nonNull(hpreds); hpreds=tl(hpreds)) {
		List ps = specifics;
		Cell pi = hd(hpreds);
		Int  i  = spoff;
		Cell ev = NIL;
		for (; isNull(ev) && nonNull(ps); ps=tl(ps), ++i)
			if (sameType(arg(fst3(pi)),intOf(snd3(pi)),arg(hd(ps)),beta))
				ev = superEvid(mkOffset(i),fun(hd(ps)),fun(fst3(pi)));
		if (nonNull(ev))
			overEvid(thd3(pi),ev);
		else {
			ERRMSG(line) "Insufficient class constraints in %s", wh ETHEN
				ERRTEXT "\n*** Context  : " ETHEN ERRCONTEXT(specifics);
			ERRTEXT "\n*** Required : " ETHEN
				ERRPRED(copyPred(fst3(pi),intOf(snd3(pi))));
			ERRTEXT "\n"
				EEND;
		}
	}

	mapOver(tidyEvid,evids);			/* avoid unnec. indirects. */

#ifdef DEBUG_TYPES
	printf("evids = "); printExp(stdout,evids); putchar('\n');
#endif

	map1Proc(qualify,preds,name(i).defn);	/* add extra dict params   */
	name(i).type = evids;			/* save evidence	   */
	overDefns    = cons(i,overDefns);		/* add to list of impls.   */
}

/* --------------------------------------------------------------------------
* Entry points to type checker:
* ------------------------------------------------------------------------*/

/* Type check top level expression */
/* using defaults if reqd	   */
Type typeCheckExp(Bool useDefs)
{
	Type type;
	Int alpha;

	typeChecker(RESET);
	alpha = newTyvars(1);
	pushOneGoal(alpha);
	enterBindings();
	inputExpr = typeExpr(0,inputExpr,var,alpha);
	clearMarks();
	if (nonNull(elimConstPreds(0,NIL)) ||
		(useDefs && resolveDefs(NIL,hpreds) &&
		nonNull(elimConstPreds(0,NIL))))
		internal("typeCheckExp");
	resetGenericsFrom(0);
	type = copyType(var,alpha);
	type = generalize(copyPreds(hpreds),type);
	if (nonNull(preds)) {		/* qualify input expression with   */
		if (whatIs(inputExpr)!=LAMBDA)	/* additional dictionary params	   */
			inputExpr = ap(LAMBDA,pair(NIL,pair(mkInt(0),inputExpr)));
		qualify(preds,snd(inputExpr));
	}
	typeChecker(RESET);
	return type;
}

/* Find dictionary (or NIL) for a   */
/* monotype instance of typ to be   */
/* in the class c		   */
Cell getDictFor(Class c, Type  typ)
{
	Cell mt = fullExpand(isPolyType(typ) ? monoTypeOf(typ) : typ);
	Cell d  = NIL;
	if (whatIs(mt)!=QUAL && mtInst(c,mt)) {
		Type t;
		Int  o;
		typeChecker(RESET);
		instantiate(typ,&t,&o,0);
		clearMarks();
		d = makeDictFor(getEvid(0,c,t,o),NIL);
		typeChecker(RESET);
	}
	return d;
}

Void typeCheckDefns() { 	       /* Type check top level bindings    */
	Target t  = length(valDefns) +
		length(instDefns) + length(classDefns);
	Target i  = 0;
	List   gs;

	typeChecker(RESET);
	saveSkolem2();
	enterBindings();
	dictsPending = NIL;
	setGoal("Type checking",t);

	for (gs=valDefns; nonNull(gs); gs=tl(gs)) {
		typeDefnGroup(hd(gs));
		soFar(i++);
	}
	clearTypeIns();
	for (gs=instDefns; nonNull(gs); gs=tl(gs)) {
		typeInstDefn(hd(gs));
		soFar(i++);
	}
	for (gs=classDefns; nonNull(gs); gs=tl(gs)) {
		typeClassDefn(hd(gs));
		soFar(i++);
	}

	if (nonNull(hd(skolVars2))) {
		ERRMSG(0) "Matching against existential type not allowed on top level\n"
			EEND;
	}
	makePendingDicts();
	typeChecker(RESET);
	done();
}

/* type check group of value defns */
/* (one top level scc)		   */
static Void local typeDefnGroup(List bs)
{
	List as;

	emptySubstitution();
	hd(defnBounds) = NIL;
	preds	   = NIL;
	setTypeIns(bs);
	typeBindings(bs);			/* find types for vars in bindings */

	if (nonNull(preds))
		internal("typeDefnGroup");

	for (as=hd(varsBounds); nonNull(as); as=tl(as)) {
		Cell a = hd(as);		/* add infered types to environment*/
		Name n = findName(textOf(fst(a)));
		if (isNull(n))
			internal("typeDefnGroup");
		name(n).type = snd(a);
	}
	hd(varsBounds) = NIL;
}

/* test if type matches monotype mt*/
Bool typeMatches(Type type, Type mt)
{
	Bool result = FALSE;
	Type t;
	Int  o;
	List ps;

	typeChecker(RESET);
	instantiate(type,&t,&o,&ps);
	if (isNull(ps)) {
		Int alpha = newTyvars(1);
		bindTv(alpha,t,o);
		pushOneGoal(alpha);
		result = unify(t,o,mt,0);
		popGoal();
	}
	typeChecker(RESET);
	return result;
}

/* Load tyvars at o with tc's variance */
static Void local loadVariance(Tycon tc, Int   o)
{
	Int  n  = tycon(tc).arity;
	List zs = tycon(tc).variance;
	for (; n>0; n--,zs=tl(zs)) {
		Tyvar *tyv = tyvar(o+n-1);
		tyv->offs = intOf(hd(zs));
	}
}

/* Restore tc's variance from tyvars */
/* at o, return TRUE if changed      */
static Bool local restoreVariance(Tycon tc, Int   o)
{
	Int  n       = tycon(tc).arity;
	List zs      = tycon(tc).variance;
	Bool changed = FALSE;
	for (; n>0; n--,zs=tl(zs)) {
		Int z0 = intOf(hd(zs));
		Int z1 = z0 | (tyvar(o+n-1)->offs);
		hd(zs) = mkInt(z1);
		if (z0 != z1)
			changed = TRUE;
	}
	return changed;
}

Bool updateVariance(Tycon tc)
{
	Bool changed = FALSE;
	List axs     = tycon(tc).axioms;
	Type t;
	Int  o;

	emptySubstitution();

	for (; nonNull(axs); axs=tl(axs)) {
		instantiate(hd(axs),&t,&o,0);
		loadVariance(tc,o);
		markVarianceType(t,o,POSV);
		if (restoreVariance(tc,o))
			changed = TRUE;
	}

	switch (whatIs(tycon(tc).what)) {
	case PRIMTYPE    : internal("updateVariance 1");

	case RESTRICTSYN : 
	case SYNONYM	 : o = newKindedVars(tycon(tc).kind,FALSE);
		loadVariance(tc,o);
		markVarianceType(tycon(tc).defn,o,POSV);
		if (restoreVariance(tc,o))
			changed = TRUE;
		break;

	case DATATYPE	 :
	case NEWTYPE	 : {   List cs = tycon(tc).defn;
		for (; nonNull(cs); cs=tl(cs)) {
			instantiate(name(hd(cs)).type,&t,&o,0);
			loadVariance(tc,o);
			for (; isFun(t); t=rng(t))
				markVarianceType(dom(t),o,POSV);
			if (restoreVariance(tc,o))
				changed = TRUE;
		}
					   }
					   break;

	case STRUCTTYPE  : {   List ss = tycon(tc).defn;
		for (; nonNull(ss); ss=tl(ss)) {
			instantiate(name(hd(ss)).type,&t,&o,0);
			loadVariance(tc,o);
			if (!isFun(t))
				internal("updateVariance 2");
			markVarianceType(rng(t),o,POSV);
			if (restoreVariance(tc,o))
				changed = TRUE;
		}
					   }
					   break;
	}
	return changed;
}

Void initVariance(Tycon tc)
{
	tycon(tc).variance = copy(length(tycon(tc).kind),mkInt(NONV));;
}

Int getVarianceAt(Type typ, Int at)
{
	Type t;
	Int  o;

	emptySubstitution();
	instantiate(typ,&t,&o,NIL);
	markVarianceType(t,o,POSV);
	return tyvar(o+at)->offs;
}

Char varianceSym(Int z)
{
	switch (z) {
	case NONV : return '.';
	case POSV : return '+';
	case NEGV : return '-';
	case INV  : return '0';
	default   : printf("z=%d ", z); internal("varianceSym"); return 0;
	}
}

Bool equalSchemes(Type t1, Type t2)
{
	return checkSchemes(t1,t2);
}

Type connectAxioms(Tycon tc, Type ax, Type ax1)
{
	Type t, t1;
	Int  o, o1;
	Int  n;

	typeChecker(RESET);
	instantiate(ax,&t,&o,0);
	instantiate(ax1,&t1,&o1,0);
	for (n=tycon(getHead(t)).arity; n>0; n--,t=fun(t))
		bindTv(o1+n-1,arg(t),o);
	copyType(satTycon(tc),o);
	return generalize(NIL,copyType(t1,o1));
}

Type liftedType(Name  n, Tycon tc)
{
	Type t, t1;
	Int  o, o1, a;

	typeChecker(RESET);
	instantiate(name(n).type,&t,&o,0);
	t1 = satTycon(tc);
	o1 = newKindedVars(tycon(tc).kind,FALSE);
	a  = newTyvars(0) - o1;
	if (isStructSel(n)) {
		t1 = fn(t1,mkOffset(a));
		newTyvars(1);
	}
	else if (isCfun(n)) {
		Type t0 = t;
		Int  i  = 0;
		for (; isFun(t0); t0=rng(t0))
			t1 = fn(mkOffset(a + i++),t1);
		newTyvars(i);
	}
	else
		internal("liftedType 1");
	if (!unify(t,o,t1,o1))
		internal("liftedType 2");
	clearMarks();
	t1 = copyType(t1,o1);
	typeChecker(RESET);
	return t1;
}

/* --------------------------------------------------------------------------
* Type checker control:
* ------------------------------------------------------------------------*/

Void typeChecker(Int what)
{
	Int  i;

	switch (what) {
	case RESET   : tcMode	    = EXPRESSION;
		matchMode    = FALSE;
		unkindTypes  = NIL;
		emptySubstitution();
		emptyAssumption();
#if OBJ
		stateBounds  = NIL;
		stateVisible = FALSE;
		refState     = FALSE;
#endif
		skolVars     = NIL;
		skolVars2    = NIL;
		goalStack    = NIL;
		forkStackT   = NIL;
		forkStackO   = NIL;
		preds        = NIL;
		evids	    = NIL;
		hpreds	    = NIL;
		dictsPending = NONE;
		break;

	case MARK    : for (i=0; i<MAXTUPCON; ++i)
					   mark(tupleConTypes[i]);
		for (i=0; i<MAXFUNC; ++i)
			mark(funcTypeCache[i]);
		for (i=0; i<MAXKINDFUN; ++i) {
			mark(simpleKindCache[i]);
			mark(varKindCache[i]);
		}
		for (i=0; i<numTyvars; ++i)
			mark(tyvars[i].bound);
		mark(typeIs);
		mark(defnBounds);
		mark(varsBounds);
		mark(depends);
		mark(skolVars);
		mark(skolVars2);
		mark(goalStack);
		mark(forkStackT);
		mark(forkStackO);
		mark(accumVars);		/* in subst.c */
		mark(lastBounds);	/* in subst.c */
		mark(preds);
		mark(evids);
		mark(hpreds);
		mark(dictsPending);
		mark(evalDict);
		mark(stdDefaults);
		mark(unkindTypes);
		mark(genericVars);
		mark(arrow);
		mark(boundPair);
		mark(listof);
		mark(typeVarToVar);
		mark(predNum);
		mark(predFractional);
		mark(predIntegral);
		mark(starToStar);
		mark(predMonad);
		mark(predMonad0);
		mark(predFailureMonad);
		mark(predFixMonad);
#if IO_MONAD
		mark(typeProgIO);
#endif
#if OBJ
		mark(stateBounds);
		mark(templof);
		mark(reqof);
		mark(refof);
		mark(typeProgO);
		mark(typeOst);
		mark(typeOProg);
		mark(typeEnvironment);
		mark(dictMonadTempl);
		mark(dictFixMonadTempl);
#endif
#if LAZY_ST
		mark(typeSTab);
#endif
		break;

	case INSTALL : typeChecker(RESET);

		for (i=0; i<MAXTUPCON; ++i)
			tupleConTypes[i] = NIL;
		for (i=0; i<MAXFUNC; ++i)
			funcTypeCache[i] = NIL;
		for (i=0; i<MAXKINDFUN; ++i) {
			simpleKindCache[i] = NIL;
			varKindCache[i]    = NIL;
		}

		starToStar   = simpleKind(1);
		typeUnit     = addPrimTycon(
			"()",STAR,0,PRIMTYPE,NIL,NIL,NIL);
		typeArrow    = addPrimTycon(
			"(->)",simpleKind(2),2,PRIMTYPE,NIL,NIL,
			cons(mkInt(POSV),cons(mkInt(NEGV),NIL)));
		typeList	    = addPrimTycon(
			"[]",starToStar,1,DATATYPE,NIL,NIL,
			cons(mkInt(POSV),NIL));
		typeArray    = addPrimTycon(
			"Array",simpleKind(2),2,PRIMTYPE,NIL,NIL,
			cons(mkInt(POSV),cons(mkInt(POSV),NIL)));

		typeChar     = addPrimTycon(
			"Char",STAR,0,PRIMTYPE,NIL,NIL,NIL);
		typeInt      = addPrimTycon(
			"Int",STAR,0,PRIMTYPE,NIL,NIL,NIL);
		typeInteger  = addPrimTycon(
			"Integer",STAR,0,PRIMTYPE,NIL,NIL,NIL);
		typeFloat    = addPrimTycon(
			"Float",STAR,0,PRIMTYPE,NIL,NIL,NIL);
		typeDouble   = addPrimTycon(
			"Double",STAR,0,PRIMTYPE,NIL,NIL,NIL);

		var	    = mkOffset(0);
		arrow	    = fn(var,mkOffset(1));
		listof	    = ap(typeList,var);
		boundPair    = ap(ap(mkTuple(2),var),var);

		nameUnit	    = addPrimCfun(findText("()"),0,0,typeUnit);
		tycon(typeUnit).defn
			= singleton(nameUnit);

		nameNil	    = addPrimCfun(findText("[]"),0,1,
			mkPolyType(starToStar,
			listof));
		nameCons     = addPrimCfun(findText(":"),2,2,
			mkPolyType(starToStar,
			fn(var,
			fn(listof,
			listof))));
		tycon(typeList).defn
			= cons(nameNil,cons(nameCons,NIL));

		typeVarToVar = fn(var,var);
#if IO_MONAD
		typeIO 	    = addPrimTycon("IO",starToStar,1,PRIMTYPE,NIL,NIL,
			cons(mkInt(POSV),NIL));
		typeProgIO   = ap(typeIO,typeUnit);
		typeIOError  = addPrimTycon("IOError",STAR,0,PRIMTYPE,NIL,NIL,NIL);
		typeHandle   = addPrimTycon("Handle",STAR,0,PRIMTYPE,NIL,NIL,NIL);
		typeIORef    = addPrimTycon("IORef",starToStar,1,PRIMTYPE,NIL,NIL,
			cons(mkInt(INV),NIL));

		nameUserErr  = addPrimCfun(inventText(),1,1,NIL);
		nameIllegal  = addPrimCfun(inventText(),0,2,NIL);
		nameNameErr  = addPrimCfun(inventText(),1,3,NIL);
		nameSearchErr= addPrimCfun(inventText(),1,4,NIL);
		nameWriteErr = addPrimCfun(inventText(),1,5,NIL);
		nameEvalErr  = addPrimCfun(inventText(),1,6,NIL);
#endif
#if LAZY_ST
		typeST	    = addPrimTycon(
			"ST",simpleKind(2),2,PRIMTYPE,NIL,NIL,
			cons(mkInt(POSV),cons(mkInt(INV),NIL)));
		typeSTab	    = ap(ap(typeST,mkOffset(0)),mkOffset(1));

		typeMutVar   = addPrimTycon(
			"MutVar",simpleKind(2),2,PRIMTYPE,NIL,NIL,
			cons(mkInt(INV),cons(mkInt(INV),NIL)));
		typeMutArr   = addPrimTycon(
			"MutArr",simpleKind(3),3,PRIMTYPE,NIL,NIL,
			cons(mkInt(INV),cons(mkInt(INV),cons(mkInt(INV),NIL))));
#endif
#if OBJ
		typeO        = addPrimTycon(
			"O",simpleKind(2),2,PRIMTYPE,NIL,NIL,
			cons(mkInt(POSV),cons(mkInt(INV),NIL)));
		typeCmd      = addPrimTycon(
			"Cmd",starToStar,1,PRIMTYPE,NIL,
			cons(mkPolyType(simpleKind(2),
			ap(ap(typeO,mkOffset(1)),mkOffset(0))),
			NIL),
			cons(mkInt(POSV),NIL));
		typeTag      = addPrimTycon("Tag",STAR,0,PRIMTYPE,NIL,NIL,NIL);
		typeAction   = addPrimTycon(
			"Action",STAR,0,PRIMTYPE,NIL,
			cons(ap(typeCmd,typeUnit),
			cons(mkPolyType(starToStar,
			ap(ap(typeO,mkOffset(0)),typeUnit)),
			NIL)),
			NIL);
		typeRequest  = addPrimTycon(
			"Request",starToStar,1,PRIMTYPE,NIL,
			cons(mkPolyType(starToStar,
			ap(typeCmd,mkOffset(0))),
			cons(mkPolyType(simpleKind(2),
			ap(ap(typeO,mkOffset(1)),mkOffset(0))),
			NIL)),
			cons(mkInt(POSV),NIL));
		typeTemplate = addPrimTycon(
			"Template",starToStar,1,PRIMTYPE,NIL,
			cons(mkPolyType(starToStar,
			ap(typeCmd,mkOffset(0))),
			cons(mkPolyType(simpleKind(2),
			ap(ap(typeO,mkOffset(1)),mkOffset(0))),
			NIL)),
			cons(mkInt(POSV),NIL));
		typeOID      = addPrimTycon("OID",STAR,0,PRIMTYPE,NIL,NIL,NIL);
		typeRef      = addPrimTycon(
			"Ref",starToStar,1,PRIMTYPE,NIL,
			cons(mkPolyType(starToStar,typeOID),NIL),
			cons(mkInt(INV),NIL));
		typeProgO    = ap(typeCmd,typeUnit);
		typeOst      = ap(ap(typeO,mkOffset(0)),mkOffset(1));
		templof      = ap(typeTemplate,var);
		reqof        = ap(typeRequest,var);
		refof        = ap(typeRef,var);
#endif
		/* Dummy dictionary for (all) instances of Eval has
		* no member, superclass, or specific slots...
		*/
		evalDict     = newDict(1);
		break;
	}
}

Void linkPreludeTC() {			/* Hook to tycons and classes in   */
	if (isNull(typeBool)) {		/* prelude when first loaded	   */
		Int i;

		typeBool     = findTycon(findText("Bool"));
		typeString   = findTycon(findText("String"));
#if OBJ
		typeError   = findTycon(findText("Error"));
		typeEnvironment = findTycon(findText("StdEnv"));
		typeOProg   = fn(typeEnvironment,typeProgO);
#endif
		typeMaybe    = findTycon(findText("Maybe"));
		typeOrdering = findTycon(findText("Ordering"));
		if (isNull(typeBool) || isNull(typeChar)   || isNull(typeString) ||
#if OBJ
			isNull(typeError) ||
#endif	
			isNull(typeMaybe)  || isNull(typeOrdering)) {
				ERRMSG(0) "Prelude does not define standard types"
					EEND;
		}
		stdDefaults  = cons(typeInt,cons(typeDouble,NIL));

		classEq      = findClass(findText("Eq"));
		classOrd     = findClass(findText("Ord"));
		classIx      = findClass(findText("Ix"));
		classEnum    = findClass(findText("Enum"));
		classShow    = findClass(findText("Show"));
		classRead    = findClass(findText("Read"));
		classEval    = findClass(findText("Eval"));
		classBounded = findClass(findText("Bounded"));
		if (isNull(classEq)   || isNull(classOrd) || isNull(classRead) ||
			isNull(classShow) || isNull(classIx)  || isNull(classEnum) ||
			isNull(classEval) || isNull(classBounded)) {
				ERRMSG(0) "Prelude does not define standard classes"
					EEND;
		}

		classReal       = findClass(findText("Real"));
		classIntegral   = findClass(findText("Integral"));
		classRealFrac   = findClass(findText("RealFrac"));
		classRealFloat  = findClass(findText("RealFloat"));
		classFractional = findClass(findText("Fractional"));
		classFloating   = findClass(findText("Floating"));
		classNum        = findClass(findText("Num"));
		if (isNull(classReal)       || isNull(classIntegral)  ||
			isNull(classRealFrac)   || isNull(classRealFloat) ||
			isNull(classFractional) || isNull(classFloating)  ||
			isNull(classNum)) {
				ERRMSG(0) "Prelude does not define numeric classes"
					EEND;
		}
		predNum	        = ap(classNum,var);
		predFractional  = ap(classFractional,var);
		predIntegral    = ap(classIntegral,var);

		classMonad        = findClass(findText("Monad"));
		classMonad0       = findClass(findText("MonadZero"));
		classFailureMonad = findClass(findText("FailureMonad"));
		classFixMonad     = findClass(findText("FixMonad"));
		if (isNull(classMonad) || isNull(classMonad0) ||
			isNull(classFailureMonad) || isNull(classFixMonad)) {
				ERRMSG(0) "Prelude does not define monad classes"
					EEND;
		}
		predMonad        = ap(classMonad,var);
		predMonad0       = ap(classMonad0,var);
		predFailureMonad = ap(classFailureMonad,var);
		predFixMonad     = ap(classFixMonad,var);

		/* The following primitives are referred to in derived instances and
		* hence require types; the following types are a little more general
		* than we might like, but they are the closest we can get without a
		* special datatype class.
		*/
		name(nameConCmp).type
			= mkPolyType(starToStar,fn(var,fn(var,typeOrdering)));
		name(nameEnRange).type
			= mkPolyType(starToStar,fn(boundPair,listof));
		name(nameEnIndex).type
			= mkPolyType(starToStar,fn(boundPair,fn(var,typeInt)));
		name(nameEnInRng).type
			= mkPolyType(starToStar,fn(boundPair,fn(var,typeBool)));
		name(nameEnToEn).type
			= mkPolyType(starToStar,fn(var,fn(typeInt,var)));
		name(nameEnFrEn).type
			= mkPolyType(starToStar,fn(var,typeInt));
		name(nameEnFrom).type
			= mkPolyType(starToStar,fn(var,listof));
		name(nameEnFrTo).type
			= name(nameEnFrTh).type
			= mkPolyType(starToStar,fn(var,fn(var,listof)));

		addEvalInst(0,typeArrow,2,NIL);	/* Add Eval instances for builtins */
		addEvalInst(0,typeList,1,NIL);
		addEvalInst(0,typeUnit,0,NIL);
		addEvalInst(0,typeArray,2,NIL);
		addEvalInst(0,typeChar,0,NIL);
		addEvalInst(0,typeInt,0,NIL);
		addEvalInst(0,typeInteger,0,NIL);
		addEvalInst(0,typeFloat,0,NIL);
		addEvalInst(0,typeDouble,0,NIL);

#if IO_MONAD
		addEvalInst(0,typeIO,1,NIL);
		addEvalInst(0,typeIOError,0,NIL);
		addEvalInst(0,typeHandle,0,NIL);
		addEvalInst(0,typeIORef,1,NIL);
#endif
#if OBJ
		addEvalInst(0,typeO,2,NIL);
		addEvalInst(0,typeCmd,1,NIL);
		addEvalInst(0,typeAction,0,NIL);
		addEvalInst(0,typeRequest,1,NIL);
		addEvalInst(0,typeTemplate,1,NIL);
		addEvalInst(0,typeRef,1,NIL);
		addEvalInst(0,typeOID,0,NIL);
		addEvalInst(0,typeTag,0,NIL);
#endif
#if LAZY_ST
		addEvalInst(0,typeST,2,NIL);
		addEvalInst(0,typeMutVar,2,NIL);
		addEvalInst(0,typeMutArr,3,NIL);
#endif

		for (i=2; i<=NUM_DTUPLES; i++) {/* Add derived instances of tuples */
			addEvalInst(0,mkTuple(i),i,NIL);
			addTupInst(classEq,i);
			addTupInst(classOrd,i);
			addTupInst(classShow,i);
			addTupInst(classRead,i);
			addTupInst(classIx,i);
		}
	}
}

Void linkPreludeCM() {			/* Hook to cfuns and mfuns in	   */
	if (isNull(nameFalse)) {		/* prelude when first loaded	   */
		nameFalse   = findName(findText("False"));
		nameTrue    = findName(findText("True"));
#if OBJ
		nameDeadlock      = findName(findText("Deadlock"));
		nameReqAbort      = findName(findText("ReqAbort"));
		nameMissedDeadline = findName(findText("MissedDeadline"));
		nameFileError     = findName(findText("FileError"));
#if O_IP
		nameInetSel  = findName(findText(".inet"));
		nameNetError  = findName(findText("NetError"));
		nameDeliver       = findName(findText(".primDeliver"));
		nameClosed        = findName(findText(".primClosed"));
#endif
		namePutCharSel    = findName(findText(".putChar"));
		namePutStrSel     = findName(findText(".putStr"));
		nameSetReaderSel  = findName(findText(".setReader"));
		nameWriteFileSel  = findName(findText(".writeFile"));
		nameAppendFileSel = findName(findText(".appendFile"));
		nameReadFileSel   = findName(findText(".readFile"));
		nameTimeOfDaySel  = findName(findText(".timeOfDay"));
		nameProgArgsSel   = findName(findText(".progArgs"));
		nameGetEnvSel     = findName(findText(".getEnv"));
		nameTerminateSel  = findName(findText(".quit"));
#endif
		nameJust    = findName(findText("Just"));
		nameNothing = findName(findText("Nothing"));
		nameLT	    = findName(findText("LT"));
		nameEQ	    = findName(findText("EQ"));
		nameGT	    = findName(findText("GT"));
		if (isNull(nameFalse) || isNull(nameTrue)    ||
#if OBJ
			isNull(nameDeadlock) || isNull(nameReqAbort) || isNull(nameFileError) ||
			isNull(namePutCharSel) || isNull(namePutStrSel) || isNull(nameSetReaderSel) ||
			isNull(nameWriteFileSel) || isNull(nameAppendFileSel) || isNull(nameReadFileSel) ||
			isNull(nameTimeOfDaySel) ||
			isNull(nameProgArgsSel)  ||
			isNull(nameGetEnvSel)    ||
			isNull(nameTerminateSel) ||
			isNull(nameMissedDeadline) ||
#if O_IP
			isNull(nameInetSel) ||
			isNull(nameNetError) ||
			isNull(nameDeliver) || isNull(nameClosed) ||
#endif
#endif
			isNull(nameJust)  || isNull(nameNothing) ||
			isNull(nameLT)    || isNull(nameEQ)      || isNull(nameGT)) {
				ERRMSG(0) "Prelude does not define standard constructors"
					EEND;
		}

		nameFromInt     = findName(findText("fromInt"));
		nameFromInteger = findName(findText("fromInteger"));
		nameFromDouble  = findName(findText("fromDouble"));
		nameEq	        = findName(findText("=="));
		nameCompare     = findName(findText("compare"));
		nameShowsPrec   = findName(findText("showsPrec"));
		nameLe	        = findName(findText("<="));
		nameIndex       = findName(findText("index"));
		nameInRange     = findName(findText("inRange"));
		nameRange       = findName(findText("range"));
		nameMult        = findName(findText("*"));
		namePlus        = findName(findText("+"));
		nameMinBnd	= findName(findText("minBound"));
		nameMaxBnd	= findName(findText("maxBound"));
		nameStrict	= findName(findText("strict"));
		nameSeq		= findName(findText("seq"));
		if (isNull(nameFromInt)   || isNull(nameFromDouble)  ||
			isNull(nameEq)        || isNull(nameCompare)     ||
			isNull(nameShowsPrec) || isNull(nameLe)          ||
			isNull(nameIndex)     || isNull(nameInRange)     ||
			isNull(nameRange)	  || isNull(nameMult)	     ||
			isNull(namePlus)      || isNull(nameFromInteger) ||
			isNull(nameMinBnd)    || isNull(nameMaxBnd)      ||
			isNull(nameStrict)    || isNull(nameSeq))         {
				ERRMSG(0) "Prelude does not define standard members"
					EEND;
		}

	}
}

/*-------------------------------------------------------------------------*/
