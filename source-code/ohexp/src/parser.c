
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 13 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"

#ifndef lint
#define lint
#endif
#define defTycon(n,l,lhs,rhs,w,ax)  tyconDefn(intOf(l),lhs,rhs,w,ax); sp-=n
#define sigdecl(l,vs,t)          ap(SIGDECL,triple(l,vs,t))
#define grded(gs)                ap(GUARDED,gs)
#define bang(t)                  ap(BANG,t)
#define only(t)                  ap(ONLY,t)
#define letrec(bs,e)             (nonNull(bs) ? ap(LETREC,pair(bs,e)) : e)
#define yyerror(s)               /* errors handled elsewhere */
#define YYSTYPE                  Cell

static Cell   local gcShadow(Int,Cell);
static Void   local syntaxError(String);
static String local unexpected(Void);
static Cell   local checkPrec(Cell);
static Void   local fixDefn(Syntax,Cell,Cell,List);
static Void   local setSyntax(Int,Syntax,Cell);
static Cell   local buildTuple(List);
static List   local checkContext(List);
static Cell   local checkClass(Cell);
static Cell   local checkInst(Cell);
static Pair   local checkDo(List);
static Cell   local checkTyLhs(Cell);
static Cell   local tidyInfix(Cell);

/* For the purposes of reasonably portable garbage collection, it is
 * necessary to simulate the YACC stack on the Hugs stack to keep
 * track of all intermediate constructs.  The lexical analyser
 * pushes a token onto the stack for each token that is found, with
 * these elements being removed as reduce actions are performed,
 * taking account of look-ahead tokens as described by gcShadow()
 * below.
 *
 * Of the non-terminals used below, only start, topDecl, fixDecl & begin
 * do not leave any values on the Hugs stack.  The same is true for the
 * terminals EXPR and SCRIPT.  At the end of a successful parse, there
 * should only be one element left on the stack, containing the result
 * of the parse.
 */

#define gc0(e)                   gcShadow(0,e)
#define gc1(e)                   gcShadow(1,e)
#define gc2(e)                   gcShadow(2,e)
#define gc3(e)                   gcShadow(3,e)
#define gc4(e)                   gcShadow(4,e)
#define gc5(e)                   gcShadow(5,e)
#define gc6(e)                   gcShadow(6,e)
#define gc7(e)                   gcShadow(7,e)
#define gc8(e)                   gcShadow(8,e)
#define gc9(e)                   gcShadow(9,e)



/* Line 189 of yacc.c  */
#line 129 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     EXPR = 258,
     SCRIPT = 259,
     CASEXP = 260,
     OF = 261,
     DATA = 262,
     TYPE = 263,
     IF = 264,
     THEN = 265,
     ELSE = 266,
     WHERE = 267,
     LET = 268,
     IN = 269,
     INFIX = 270,
     INFIXL = 271,
     INFIXR = 272,
     PRIMITIVE = 273,
     TNEWTYPE = 274,
     DEFAULT = 275,
     DERIVING = 276,
     DO = 277,
     TCLASS = 278,
     TINSTANCE = 279,
     TRUNST = 280,
     REPEAT = 281,
     VAROP = 282,
     VARID = 283,
     NUMLIT = 284,
     CHARLIT = 285,
     STRINGLIT = 286,
     CONOP = 287,
     CONID = 288,
     STRUCT = 289,
     SELDOT = 290,
     TEMPLATE = 291,
     ACTION = 292,
     REQUEST = 293,
     ASSIGN = 294,
     HANDLE = 295,
     FORALL = 296,
     WHILE = 297,
     ELSIF = 298,
     FIX = 299,
     COCO = 300,
     UPTO = 301,
     FROM = 302,
     ARROW = 303,
     IMPLIES = 304,
     MODULE = 305,
     IMPORT = 306,
     HIDING = 307,
     QUALIFIED = 308,
     ASMOD = 309
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 225 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  50
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   2300

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  74
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  125
/* YYNRULES -- Number of rules.  */
#define YYNRULES  352
/* YYNRULES -- Number of states.  */
#define YYNSTATES  612

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   309

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    57,     2,     2,     2,     2,     2,     2,
      59,    61,     2,     2,    60,    53,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    63,
      36,    48,    37,     2,    50,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    62,    51,    64,     2,    71,    65,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    72,    52,    73,    56,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    49,    54,    55,    58,    66,    67,    68,    69,    70
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     7,    10,    12,    16,    18,    21,    23,
      31,    34,    36,    38,    40,    42,    46,    49,    54,    61,
      62,    66,    71,    75,    77,    79,    82,    86,    88,    89,
      93,   100,   105,   108,   109,   114,   118,   119,   121,   123,
     126,   130,   132,   134,   136,   141,   146,   147,   149,   151,
     154,   158,   160,   162,   164,   168,   170,   174,   178,   182,
     184,   185,   189,   191,   193,   195,   197,   199,   201,   203,
     205,   209,   211,   215,   216,   218,   220,   223,   227,   231,
     233,   235,   240,   247,   254,   258,   264,   270,   274,   275,
     278,   279,   282,   286,   288,   290,   294,   297,   299,   301,
     305,   307,   311,   313,   317,   319,   324,   328,   332,   334,
     336,   338,   342,   346,   349,   352,   354,   357,   358,   361,
     366,   367,   369,   373,   375,   380,   384,   386,   388,   391,
     393,   397,   401,   406,   410,   412,   416,   418,   419,   421,
     425,   427,   431,   433,   436,   438,   442,   446,   448,   450,
     452,   456,   460,   462,   464,   466,   469,   471,   474,   476,
     478,   480,   482,   485,   489,   493,   497,   501,   505,   509,
     513,   516,   518,   521,   523,   527,   531,   535,   539,   543,
     547,   551,   552,   554,   556,   559,   563,   565,   569,   572,
     575,   577,   580,   582,   585,   586,   589,   591,   596,   600,
     602,   604,   608,   610,   614,   618,   622,   626,   628,   630,
     632,   634,   636,   640,   642,   644,   648,   650,   652,   654,
     659,   663,   666,   670,   675,   682,   689,   691,   696,   701,
     704,   707,   710,   716,   719,   721,   727,   731,   734,   736,
     739,   742,   744,   746,   750,   753,   755,   757,   760,   762,
     764,   766,   768,   772,   776,   780,   784,   789,   793,   798,
     803,   808,   812,   816,   820,   822,   825,   829,   831,   835,
     837,   840,   842,   845,   847,   852,   854,   857,   861,   866,
     870,   872,   876,   879,   886,   891,   896,   899,   906,   913,
     918,   921,   923,   925,   929,   933,   934,   936,   938,   941,
     945,   947,   949,   953,   957,   958,   960,   962,   965,   969,
     971,   975,   980,   982,   983,   987,   989,   991,   993,   998,
    1002,  1005,  1009,  1014,  1016,  1019,  1023,  1025,  1029,  1031,
    1034,  1036,  1039,  1041,  1046,  1047,  1049,  1051,  1055,  1059,
    1064,  1067,  1073,  1077,  1079,  1083,  1085,  1088,  1090,  1092,
    1094,  1096,  1097
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
      75,     0,    -1,     3,   151,   143,    -1,     4,    76,    -1,
       1,    -1,   195,    80,   196,    -1,    77,    -1,    77,    78,
      -1,    78,    -1,    66,    79,    81,    12,   197,    80,   196,
      -1,    66,     1,    -1,    33,    -1,    28,    -1,    31,    -1,
     101,    -1,    94,    63,   101,    -1,    84,    85,    -1,    84,
      63,    85,   101,    -1,    84,    63,    85,    94,    63,   101,
      -1,    -1,    59,    82,    61,    -1,    59,    82,    60,    61,
      -1,    82,    60,    83,    -1,    83,    -1,    90,    -1,    66,
      79,    -1,    84,    63,    86,    -1,    86,    -1,    -1,    67,
      79,    87,    -1,    67,    69,    79,    70,    79,    87,    -1,
      67,    69,    79,    87,    -1,    67,     1,    -1,    -1,    68,
      59,    88,    61,    -1,    59,    88,    61,    -1,    -1,    60,
      -1,    89,    -1,    89,    60,    -1,    89,    60,    90,    -1,
      90,    -1,   147,    -1,    33,    -1,    33,    59,    49,    61,
      -1,    33,    59,    91,    61,    -1,    -1,    60,    -1,    92,
      -1,    92,    60,    -1,    92,    60,    93,    -1,    93,    -1,
     147,    -1,   150,    -1,    94,    63,    95,    -1,    95,    -1,
      16,    96,    97,    -1,    17,    96,    97,    -1,    15,    96,
      97,    -1,    29,    -1,    -1,    97,    60,    98,    -1,    98,
      -1,    99,    -1,   100,    -1,    53,    -1,    27,    -1,    57,
      -1,    36,    -1,    37,    -1,    65,   149,    65,    -1,    32,
      -1,    65,    33,    65,    -1,    -1,    63,    -1,   102,    -1,
     102,    63,    -1,   102,    63,   103,    -1,   102,    63,   140,
      -1,   103,    -1,   140,    -1,     8,   108,    48,   127,    -1,
       8,   108,    48,   127,    14,   109,    -1,     7,   108,   104,
      48,   111,   116,    -1,     7,   108,   104,    -1,    19,   108,
      48,   115,   116,    -1,    34,   108,   105,    48,   137,    -1,
      34,   108,   105,    -1,    -1,    37,   106,    -1,    -1,    36,
     106,    -1,   107,    60,   106,    -1,   107,    -1,   131,    -1,
      62,   127,    64,    -1,   108,   149,    -1,    33,    -1,     1,
      -1,   109,    60,   110,    -1,   110,    -1,   147,    47,   125,
      -1,   147,    -1,   111,    52,   112,    -1,   112,    -1,    57,
     129,   100,   114,    -1,   130,   100,   114,    -1,   131,   100,
     114,    -1,   131,    -1,   113,    -1,     1,    -1,   131,    57,
     132,    -1,   113,    57,   132,    -1,   113,   132,    -1,    57,
     129,    -1,   129,    -1,   150,   132,    -1,    -1,    21,    33,
      -1,    21,    59,   117,    61,    -1,    -1,   118,    -1,   118,
      60,    33,    -1,    33,    -1,    18,   119,    47,   125,    -1,
     119,    60,   120,    -1,   120,    -1,     1,    -1,   147,    31,
      -1,   147,    -1,    23,   121,   143,    -1,    24,   122,   143,
      -1,    20,    59,   123,    61,    -1,   126,    58,   131,    -1,
     131,    -1,   126,    58,   131,    -1,   131,    -1,    -1,   124,
      -1,   124,    60,   127,    -1,   127,    -1,   126,    58,   127,
      -1,   127,    -1,    59,    61,    -1,   131,    -1,    59,   131,
      61,    -1,    59,   135,    61,    -1,   128,    -1,   131,    -1,
     130,    -1,   130,    55,   127,    -1,   131,    55,   127,    -1,
       1,    -1,   130,    -1,   131,    -1,   130,   132,    -1,   133,
      -1,   131,   132,    -1,    33,    -1,   133,    -1,    33,    -1,
     149,    -1,    59,    61,    -1,    59,    55,    61,    -1,    59,
     128,    61,    -1,    59,   131,    61,    -1,    59,   134,    61,
      -1,    59,   135,    61,    -1,    59,   136,    61,    -1,    62,
     127,    64,    -1,    62,    64,    -1,    71,    -1,   134,    60,
      -1,    60,    -1,   135,    60,   131,    -1,   131,    60,   131,
      -1,   128,    60,   127,    -1,   131,    60,   128,    -1,   135,
      60,   128,    -1,   136,    60,   127,    -1,   197,   138,   196,
      -1,    -1,    63,    -1,   139,    -1,   139,    63,    -1,   139,
      63,   140,    -1,   140,    -1,   146,    47,   125,    -1,   153,
     141,    -1,   142,   143,    -1,     1,    -1,    48,   151,    -1,
     144,    -1,    12,   137,    -1,    -1,   144,   145,    -1,   145,
      -1,    52,   151,    48,   151,    -1,   146,    60,   147,    -1,
     147,    -1,   148,    -1,    59,    53,    61,    -1,   149,    -1,
      59,    27,    61,    -1,    59,    57,    61,    -1,    59,    36,
      61,    -1,    59,    37,    61,    -1,    28,    -1,    68,    -1,
      69,    -1,    70,    -1,    33,    -1,    59,    32,    61,    -1,
     152,    -1,     1,    -1,   153,    47,   125,    -1,   153,    -1,
     154,    -1,   155,    -1,   154,    98,    53,   155,    -1,   154,
      98,   155,    -1,    53,   155,    -1,   155,    98,   155,    -1,
     155,    98,    53,   155,    -1,     9,   151,    10,   151,    11,
     151,    -1,     5,   151,     6,   197,   162,   196,    -1,   156,
      -1,    13,   137,    14,   151,    -1,    51,   158,    55,   151,
      -1,    22,   168,    -1,    39,   168,    -1,    40,   168,    -1,
      38,   173,    14,   151,   182,    -1,    34,   157,    -1,   159,
      -1,   197,   138,   196,    49,   150,    -1,   197,   138,   196,
      -1,   158,   160,    -1,   160,    -1,   159,   160,    -1,    25,
     160,    -1,   160,    -1,   147,    -1,   147,    50,   160,    -1,
      56,   160,    -1,    71,    -1,   150,    -1,    59,    61,    -1,
      29,    -1,    30,    -1,    31,    -1,    26,    -1,    59,   151,
      61,    -1,    59,   161,    61,    -1,    72,   138,   196,    -1,
     160,    35,   149,    -1,    59,    35,   149,    61,    -1,    62,
     192,    64,    -1,    59,   155,    98,    61,    -1,    59,    99,
     160,    61,    -1,    59,   100,   160,    61,    -1,    59,   134,
      61,    -1,   161,    60,   151,    -1,   151,    60,   151,    -1,
     163,    -1,   163,    63,    -1,   163,    63,   164,    -1,   164,
      -1,   153,   165,   143,    -1,   166,    -1,    55,   151,    -1,
       1,    -1,   166,   167,    -1,   167,    -1,    52,   153,    55,
     151,    -1,   169,    -1,   169,   181,    -1,   197,   170,   196,
      -1,   197,   170,    63,   196,    -1,   170,    63,   171,    -1,
     171,    -1,   183,    54,   151,    -1,    13,   137,    -1,     9,
     151,    10,   169,    11,   169,    -1,     9,   151,    10,   169,
      -1,    45,   151,    10,   169,    -1,    11,   169,    -1,     5,
     151,     6,   197,   186,   196,    -1,    43,   151,    54,   151,
      22,   169,    -1,    44,   151,    22,   169,    -1,    46,   177,
      -1,   172,    -1,   183,    -1,   184,    41,   151,    -1,   197,
     174,   196,    -1,    -1,    63,    -1,   175,    -1,   175,    63,
      -1,   175,    63,   176,    -1,   176,    -1,   172,    -1,   183,
      54,   151,    -1,   197,   178,   196,    -1,    -1,    63,    -1,
     179,    -1,   179,    63,    -1,   179,    63,   180,    -1,   180,
      -1,   183,    54,   151,    -1,    42,   197,   186,   196,    -1,
     181,    -1,    -1,   184,    47,   125,    -1,   184,    -1,   185,
      -1,   156,    -1,   185,    98,    53,   155,    -1,   185,    98,
     155,    -1,    53,   155,    -1,   156,    98,   155,    -1,   156,
      98,    53,   155,    -1,   187,    -1,   187,    63,    -1,   187,
      63,   188,    -1,   188,    -1,   153,   189,   143,    -1,   190,
      -1,    55,   169,    -1,     1,    -1,   190,   191,    -1,   191,
      -1,    52,   153,    55,   169,    -1,    -1,   151,    -1,   161,
      -1,   151,    52,   193,    -1,   151,    49,   151,    -1,   151,
      60,   151,    49,    -1,   151,    49,    -1,   151,    60,   151,
      49,   151,    -1,   193,    60,   194,    -1,   194,    -1,   151,
      54,   151,    -1,   151,    -1,    13,   137,    -1,    72,    -1,
       1,    -1,    73,    -1,     1,    -1,    -1,   198,    72,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    88,    88,    89,    90,    99,   100,   102,   103,   105,
     107,   109,   110,   111,   113,   114,   115,   116,   117,   123,
     124,   125,   127,   128,   130,   131,   136,   137,   139,   149,
     150,   152,   154,   156,   157,   158,   160,   161,   162,   163,
     165,   166,   168,   169,   170,   171,   173,   174,   175,   176,
     178,   179,   181,   182,   187,   188,   190,   191,   192,   194,
     195,   197,   198,   200,   201,   202,   204,   205,   206,   207,
     208,   210,   211,   216,   217,   218,   219,   221,   222,   223,
     224,   229,   230,   232,   234,   235,   237,   239,   241,   242,
     244,   245,   247,   248,   250,   251,   253,   254,   255,   257,
     258,   260,   262,   264,   265,   267,   268,   269,   270,   271,
     272,   274,   275,   276,   278,   279,   281,   283,   284,   285,
     287,   288,   290,   291,   296,   298,   299,   300,   302,   303,
     308,   309,   310,   312,   313,   315,   316,   318,   319,   321,
     322,   327,   328,   330,   331,   332,   333,   335,   336,   338,
     339,   340,   341,   343,   344,   346,   347,   349,   350,   352,
     353,   355,   356,   357,   358,   359,   360,   361,   362,   363,
     364,   365,   367,   368,   370,   371,   373,   374,   375,   376,
     381,   383,   384,   385,   386,   388,   389,   391,   392,   394,
     395,   397,   398,   400,   401,   403,   404,   406,   408,   409,
     411,   412,   414,   415,   416,   417,   418,   420,   421,   422,
     423,   425,   426,   431,   432,   434,   435,   437,   438,   440,
     441,   442,   443,   444,   447,   448,   449,   451,   452,   455,
     456,   457,   458,   460,   461,   463,   464,   466,   467,   469,
     470,   471,   473,   474,   475,   476,   477,   478,   479,   480,
     481,   482,   483,   484,   485,   486,   487,   488,   489,   490,
     491,   492,   494,   495,   497,   498,   500,   501,   503,   505,
     506,   507,   509,   510,   512,   514,   515,   517,   518,   520,
     521,   523,   524,   525,   526,   527,   528,   529,   530,   531,
     532,   533,   534,   536,   538,   540,   541,   542,   543,   545,
     546,   548,   550,   552,   554,   555,   556,   557,   559,   560,
     562,   564,   566,   567,   568,   569,   571,   572,   574,   575,
     576,   577,   578,   581,   582,   584,   585,   587,   589,   590,
     591,   593,   594,   596,   601,   602,   603,   604,   605,   606,
     607,   608,   611,   612,   614,   615,   616,   621,   622,   625,
     626,   638,   638
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "EXPR", "SCRIPT", "CASEXP", "OF", "DATA",
  "TYPE", "IF", "THEN", "ELSE", "WHERE", "LET", "IN", "INFIX", "INFIXL",
  "INFIXR", "PRIMITIVE", "TNEWTYPE", "DEFAULT", "DERIVING", "DO", "TCLASS",
  "TINSTANCE", "TRUNST", "REPEAT", "VAROP", "VARID", "NUMLIT", "CHARLIT",
  "STRINGLIT", "CONOP", "CONID", "STRUCT", "SELDOT", "'<'", "'>'",
  "TEMPLATE", "ACTION", "REQUEST", "ASSIGN", "HANDLE", "FORALL", "WHILE",
  "ELSIF", "FIX", "COCO", "'='", "UPTO", "'@'", "'\\\\'", "'|'", "'-'",
  "FROM", "ARROW", "'~'", "'!'", "IMPLIES", "'('", "','", "')'", "'['",
  "';'", "']'", "'`'", "MODULE", "IMPORT", "HIDING", "QUALIFIED", "ASMOD",
  "'_'", "'{'", "'}'", "$accept", "start", "topModule", "modules",
  "module", "modid", "modBody", "expspec", "exports", "export", "impDecls",
  "chase", "impDecl", "impspec", "imports", "imports1", "import", "cnames",
  "cnames1", "cname", "fixDecls", "fixDecl", "optdigit", "ops", "op",
  "varop", "conop", "topDecls", "topDecls1", "topDecl", "subs", "sups",
  "stypes", "stype", "tyLhs", "invars", "invar", "constrs", "constr",
  "btype3", "bbtype", "nconstr", "deriving", "derivs0", "derivs", "prims",
  "prim", "crule", "irule", "dtypes", "dtypes1", "sigType", "context",
  "type", "type1", "btype", "btype1", "btype2", "atype", "atype1",
  "tupCommas", "btypes2", "typeTuple", "decllist", "decls", "decls1",
  "decl", "rhs", "rhs1", "wherePart", "gdefs", "gdef", "vars", "var",
  "varid", "varid1", "conid", "exp", "exp1", "opExp", "opExp0", "pfxExp",
  "pfxExp1", "sdecllist", "pats", "appExp", "atomic", "exps2", "alts",
  "alts1", "alt", "altRhs", "guardAlts", "guardAlt", "do", "stmts",
  "stmts1", "stmt", "assign", "tdecllist", "tdecls", "tdecls1", "tdecl",
  "genlist", "gens", "gens1", "gen", "handle", "ohandle", "mexp", "mopExp",
  "mopExp0", "malts", "malts1", "malt", "maltRhs", "mguardAlts",
  "mguardAlt", "list", "quals", "qual", "begin", "end", "beg", "@1", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,    60,    62,   291,   292,
     293,   294,   295,   296,   297,   298,   299,   300,    61,   301,
      64,    92,   124,    45,   302,   303,   126,    33,   304,    40,
      44,    41,    91,    59,    93,    96,   305,   306,   307,   308,
     309,    95,   123,   125
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    74,    75,    75,    75,    76,    76,    77,    77,    78,
      78,    79,    79,    79,    80,    80,    80,    80,    80,    81,
      81,    81,    82,    82,    83,    83,    84,    84,    85,    86,
      86,    86,    86,    87,    87,    87,    88,    88,    88,    88,
      89,    89,    90,    90,    90,    90,    91,    91,    91,    91,
      92,    92,    93,    93,    94,    94,    95,    95,    95,    96,
      96,    97,    97,    98,    98,    98,    99,    99,    99,    99,
      99,   100,   100,   101,   101,   101,   101,   102,   102,   102,
     102,   103,   103,   103,   103,   103,   103,   103,   104,   104,
     105,   105,   106,   106,   107,   107,   108,   108,   108,   109,
     109,   110,   110,   111,   111,   112,   112,   112,   112,   112,
     112,   113,   113,   113,   114,   114,   115,   116,   116,   116,
     117,   117,   118,   118,   103,   119,   119,   119,   120,   120,
     103,   103,   103,   121,   121,   122,   122,   123,   123,   124,
     124,   125,   125,   126,   126,   126,   126,   127,   127,   128,
     128,   128,   128,   129,   129,   130,   130,   131,   131,   132,
     132,   133,   133,   133,   133,   133,   133,   133,   133,   133,
     133,   133,   134,   134,   135,   135,   136,   136,   136,   136,
     137,   138,   138,   138,   138,   139,   139,   140,   140,   141,
     141,   142,   142,   143,   143,   144,   144,   145,   146,   146,
     147,   147,   148,   148,   148,   148,   148,   149,   149,   149,
     149,   150,   150,   151,   151,   152,   152,   153,   153,   154,
     154,   154,   154,   154,   155,   155,   155,   156,   156,   156,
     156,   156,   156,   156,   156,   157,   157,   158,   158,   159,
     159,   159,   160,   160,   160,   160,   160,   160,   160,   160,
     160,   160,   160,   160,   160,   160,   160,   160,   160,   160,
     160,   160,   161,   161,   162,   162,   163,   163,   164,   165,
     165,   165,   166,   166,   167,   168,   168,   169,   169,   170,
     170,   171,   171,   171,   171,   171,   171,   171,   171,   171,
     171,   171,   171,   172,   173,   174,   174,   174,   174,   175,
     175,   176,   176,   177,   178,   178,   178,   178,   179,   179,
     180,   181,   182,   182,   183,   183,   184,   184,   185,   185,
     185,   185,   185,   186,   186,   187,   187,   188,   189,   189,
     189,   190,   190,   191,   192,   192,   192,   192,   192,   192,
     192,   192,   193,   193,   194,   194,   194,   195,   195,   196,
     196,   198,   197
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     3,     2,     1,     3,     1,     2,     1,     7,
       2,     1,     1,     1,     1,     3,     2,     4,     6,     0,
       3,     4,     3,     1,     1,     2,     3,     1,     0,     3,
       6,     4,     2,     0,     4,     3,     0,     1,     1,     2,
       3,     1,     1,     1,     4,     4,     0,     1,     1,     2,
       3,     1,     1,     1,     3,     1,     3,     3,     3,     1,
       0,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     1,     3,     0,     1,     1,     2,     3,     3,     1,
       1,     4,     6,     6,     3,     5,     5,     3,     0,     2,
       0,     2,     3,     1,     1,     3,     2,     1,     1,     3,
       1,     3,     1,     3,     1,     4,     3,     3,     1,     1,
       1,     3,     3,     2,     2,     1,     2,     0,     2,     4,
       0,     1,     3,     1,     4,     3,     1,     1,     2,     1,
       3,     3,     4,     3,     1,     3,     1,     0,     1,     3,
       1,     3,     1,     2,     1,     3,     3,     1,     1,     1,
       3,     3,     1,     1,     1,     2,     1,     2,     1,     1,
       1,     1,     2,     3,     3,     3,     3,     3,     3,     3,
       2,     1,     2,     1,     3,     3,     3,     3,     3,     3,
       3,     0,     1,     1,     2,     3,     1,     3,     2,     2,
       1,     2,     1,     2,     0,     2,     1,     4,     3,     1,
       1,     3,     1,     3,     3,     3,     3,     1,     1,     1,
       1,     1,     3,     1,     1,     3,     1,     1,     1,     4,
       3,     2,     3,     4,     6,     6,     1,     4,     4,     2,
       2,     2,     5,     2,     1,     5,     3,     2,     1,     2,
       2,     1,     1,     3,     2,     1,     1,     2,     1,     1,
       1,     1,     3,     3,     3,     3,     4,     3,     4,     4,
       4,     3,     3,     3,     1,     2,     3,     1,     3,     1,
       2,     1,     2,     1,     4,     1,     2,     3,     4,     3,
       1,     3,     2,     6,     4,     4,     2,     6,     6,     4,
       2,     1,     1,     3,     3,     0,     1,     1,     2,     3,
       1,     1,     3,     3,     0,     1,     1,     2,     3,     1,
       3,     4,     1,     0,     3,     1,     1,     1,     4,     3,
       2,     3,     4,     1,     2,     3,     1,     3,     1,     2,
       1,     2,     1,     4,     0,     1,     1,     3,     3,     4,
       2,     5,     3,     1,     3,     1,     2,     1,     1,     1,
       1,     0,     2
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       0,     4,     0,     0,     0,   214,     0,     0,   351,   351,
       0,   251,   207,   248,   249,   250,   211,   351,   351,   351,
     351,     0,     0,     0,     0,     0,   208,   209,   210,   245,
     181,   242,   200,   202,   246,   194,   213,   216,   217,   218,
     226,   234,   241,   348,     0,   347,     3,     6,     8,    73,
       1,     0,     0,     0,   181,     0,   229,   275,     0,   240,
     233,   181,     0,   295,   230,   231,     0,   238,   221,   244,
      66,    71,     0,    68,    69,     0,    67,   173,   247,     0,
       0,     0,     0,     0,   218,     0,   335,   336,     0,   182,
       0,   183,   186,     0,   242,     0,     0,   351,     2,     0,
      66,    71,    68,    69,    65,    67,     0,    63,    64,     0,
     239,     0,    10,    12,    13,    11,    19,     7,     0,     0,
      60,    60,    60,     0,     0,     0,     0,     0,     0,    74,
       0,     0,    28,    27,     0,    55,    14,    75,    79,    80,
     351,     0,     0,     0,   352,   351,   276,     0,     0,   351,
     351,     0,     0,     0,   351,     0,   317,     0,   280,   291,
     292,   315,   316,     0,     0,   296,   301,     0,   297,   300,
       0,     0,   237,   203,   212,     0,   205,   206,   201,   204,
       0,     0,     0,     0,   172,   261,     0,   252,     0,     0,
     253,     0,     0,     0,   257,   350,   349,   254,   184,     0,
       0,   190,     0,     0,   188,   194,   192,   196,   243,   193,
     152,   158,     0,     0,   171,   215,     0,   142,   147,   149,
     148,   156,   161,     0,   220,     0,   222,   255,     0,     0,
      98,    97,    88,     0,    59,     0,     0,     0,   127,     0,
       0,   126,   129,     0,     0,     0,   194,     0,   134,   194,
       0,   136,    90,    32,     0,    33,     5,    28,    16,    73,
      76,     0,     0,   227,   180,     0,     0,     0,   286,   282,
       0,     0,     0,   290,   304,   320,     0,     0,   277,     0,
       0,     0,     0,   236,   313,   294,   298,     0,   228,   256,
      72,    70,   259,   260,   263,   258,   262,   338,   351,   345,
     337,   343,   263,   185,   187,   198,   191,     0,   189,   195,
       0,     0,   162,     0,     0,     0,     0,     0,   170,     0,
     148,     0,   160,     0,   155,   159,     0,   157,   219,   223,
      43,     0,     0,    23,    24,    42,   351,     0,    84,    96,
       0,    58,    62,    56,    57,     0,     0,     0,     0,     0,
       0,     0,   128,     0,     0,   138,   140,   143,     0,     0,
     130,     0,   131,     0,     0,    87,    33,    36,     0,    29,
      73,    26,    54,    15,    77,    78,     0,     0,   264,   267,
       0,     0,     0,   323,   326,   351,   351,     0,   351,   351,
     305,     0,   306,   309,     0,   315,     0,   321,   279,   278,
     281,   293,   314,     0,   319,     0,   312,   232,   299,   302,
     346,     0,     0,     0,     0,   163,   162,     0,     0,     0,
     164,     0,   165,   166,     0,   167,     0,   168,   169,   141,
     150,   151,    46,    25,     0,    20,    73,     0,    89,    93,
      94,     0,    81,     0,   124,   125,     0,   117,     0,   132,
       0,     0,   145,     0,   146,   133,   135,    91,   351,     0,
      31,    37,     0,    38,    41,    36,     0,    17,   271,     0,
       0,   194,   269,   273,   225,   265,   224,   330,     0,   351,
     194,   328,   332,   311,   324,     0,   284,     0,   289,   285,
     303,   307,     0,   322,   318,   235,   344,   342,   341,   197,
     165,   167,   176,   177,   175,   178,   174,   179,     0,     0,
      47,     0,    48,    51,    52,    53,    21,    22,     0,     0,
       0,   110,     0,   117,   104,   109,     0,   108,     0,    61,
       0,     0,    85,   116,   139,   175,   174,    86,    33,    35,
      39,     0,    73,     0,   270,   268,   272,   266,     0,   329,
     327,   331,   325,     0,   351,   351,   308,   310,    44,    45,
      49,     9,    95,    92,     0,   153,   154,     0,    83,     0,
     113,     0,     0,     0,     0,    82,   100,   102,   118,   120,
      30,    40,    34,    18,     0,   351,   287,   283,   288,    50,
       0,   103,   112,     0,   106,   115,   111,   107,     0,     0,
     123,     0,   121,   274,   333,   105,   114,    99,   101,   119,
       0,   122
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     4,    46,    47,    48,   116,   131,   229,   332,   333,
     132,   258,   133,   369,   462,   463,   334,   511,   512,   513,
     134,   135,   235,   341,   342,   107,   108,   136,   137,   138,
     338,   365,   438,   439,   232,   575,   576,   523,   524,   525,
     594,   447,   532,   601,   602,   240,   241,   246,   249,   354,
     355,   215,   216,   217,   218,   595,   219,   320,   327,   221,
     315,   316,   317,    53,    90,    91,   139,   204,   205,    98,
     206,   207,    93,    31,    32,    33,    34,   299,    36,    37,
      38,    39,    40,    60,    66,    41,    42,    85,   377,   378,
     379,   471,   472,   473,    56,    57,   157,   158,   159,    62,
     167,   168,   169,   273,   391,   392,   393,   146,   407,   160,
     161,   162,   382,   383,   384,   480,   481,   482,    88,   300,
     301,    49,   197,    58,    55
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -486
static const yytype_int16 yypact[] =
{
     308,  -486,  1095,    31,    58,  -486,  1095,  1095,  -486,  -486,
    2207,  -486,  -486,  -486,  -486,  -486,  -486,  -486,  -486,  -486,
    -486,  2207,  1988,  2207,   807,   879,  -486,  -486,  -486,  -486,
    1540,    25,  -486,  -486,  -486,    67,  -486,    47,   520,   520,
    -486,  2207,   112,  -486,    65,  -486,  -486,    33,  -486,  1300,
    -486,   113,   150,   157,  1540,   114,  -486,   153,  1488,   112,
    -486,  1540,   199,  2039,  -486,  -486,  2160,   112,  -486,   112,
     141,   167,   164,   196,   215,  1660,   223,  -486,  -486,   226,
    2207,  2207,   208,   212,   520,   316,   200,   233,   243,  -486,
      28,   250,  -486,     9,   161,    64,  2207,  -486,  -486,   466,
    -486,  -486,  -486,  -486,  -486,  -486,  1728,  -486,  -486,  1780,
     112,   164,  -486,  -486,  -486,  -486,   257,  -486,    77,    77,
     296,   296,   296,    56,    77,   274,    21,    21,    35,  -486,
     230,    28,   279,  -486,   282,  -486,  -486,   284,  -486,  -486,
    -486,  1095,  1095,    28,  -486,  -486,  -486,  1095,  1095,  -486,
    -486,  1095,  1095,  1095,  -486,  1988,   520,    27,  -486,  -486,
     301,   176,   520,    28,  1095,  -486,  -486,    28,   294,  -486,
     315,  1095,   112,  -486,  -486,   311,  -486,  -486,  -486,  -486,
     297,   309,    92,    97,  -486,  -486,  1095,  -486,  1592,  1095,
    -486,   951,  1167,  1095,  -486,  -486,  -486,  -486,  1832,   466,
      18,  -486,  1095,  1095,  -486,    67,   328,  -486,   112,  -486,
    -486,  -486,   590,   325,  -486,  -486,   362,  -486,  -486,   900,
     828,  -486,  -486,  1988,  -486,  1988,  -486,  -486,   270,   410,
    -486,  -486,   382,   107,  -486,   520,   520,   520,  -486,   329,
      82,  -486,   397,   252,  1181,    11,    67,   387,   135,    67,
     395,   135,   146,  -486,   277,    84,  -486,   368,  -486,  1368,
    1436,  1832,   446,  -486,  -486,  1832,   453,   462,  -486,   157,
     430,   470,   478,  -486,  2090,  -486,  1884,   734,  -486,  1095,
    1095,   466,  1936,   440,   153,  -486,  2141,  1095,  -486,  -486,
    -486,  -486,  -486,  -486,  -486,  -486,  -486,  -486,  -486,   439,
     435,  -486,   447,  -486,  -486,  -486,  -486,   450,  -486,  -486,
     452,   607,   445,   330,  2190,   337,   344,   366,  -486,   451,
     972,  1233,  -486,  1233,  -486,  -486,  1233,  -486,  -486,  -486,
     455,   277,   371,  -486,  -486,  -486,  -486,    20,   469,  -486,
    1233,   459,  -486,   459,   459,   141,   196,   215,   461,   223,
     466,    18,  -486,   100,   463,   460,  -486,  -486,  1644,   373,
    -486,   493,  -486,   493,    20,   482,   194,   578,   472,  -486,
    1368,  -486,  -486,  -486,  -486,  -486,    37,    28,   475,  -486,
    1095,    59,    28,   476,  -486,  -486,  -486,  1095,  -486,  -486,
    -486,    28,   481,  -486,   479,   485,  1988,  -486,  -486,  -486,
    -486,  -486,  -486,  1988,  -486,   100,  -486,  -486,  -486,  -486,
     157,  1095,  1167,  1023,  1095,  -486,  -486,  2229,   377,  1233,
    -486,  1233,   495,  -486,  1233,   496,  1233,  -486,  -486,  -486,
    -486,  -486,   534,  -486,   355,  -486,  1300,  1233,  -486,   491,
     657,  1220,   541,   520,  -486,  -486,   529,   542,   657,  -486,
    1233,   493,  -486,   493,  -486,   657,   657,  -486,  -486,   277,
    -486,  -486,   504,   506,  -486,   578,   505,  -486,  -486,  1832,
    1095,    67,   519,  -486,  -486,  1832,  -486,  -486,  1832,  -486,
      67,   522,  -486,  -486,  1832,  1832,   565,   556,  -486,  -486,
    -486,  2141,  1095,  -486,  -486,  -486,  -486,  -486,  -486,  -486,
    -486,  -486,  -486,  -486,   972,  -486,   972,  -486,   523,   647,
    -486,   525,   521,  -486,  -486,  -486,  -486,  -486,    28,   516,
      20,  -486,  2012,    53,  -486,  1044,  1200,   756,    18,  -486,
     167,   108,  -486,  -486,  -486,   657,   657,  -486,    84,  -486,
     291,   526,  1368,   543,  -486,  -486,  -486,  -486,   544,  -486,
    -486,  -486,  -486,    28,  -486,  -486,  -486,  -486,  -486,  -486,
     442,  -486,  -486,  -486,     8,   657,   657,  1220,  -486,   657,
    -486,   567,  1116,   657,  1116,   547,  -486,   554,  -486,   576,
    -486,  -486,  -486,  -486,  1095,  -486,  -486,  -486,  -486,  -486,
    1116,  -486,  -486,  2012,  -486,  -486,  -486,  -486,    18,   466,
    -486,   549,   552,  -486,  -486,  -486,  -486,  -486,  -486,  -486,
     581,  -486
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -486,  -486,  -486,  -486,   568,  -124,   181,  -486,  -486,   185,
    -486,   363,   364,  -348,   159,  -486,  -342,  -486,  -486,    66,
     255,  -251,   340,   229,   -25,   603,   -22,  -252,  -486,   369,
    -486,  -486,  -337,  -486,   178,  -486,    32,  -486,    75,  -486,
    -402,  -486,   109,  -486,  -486,  -486,   280,  -486,  -486,  -486,
    -486,  -196,   350,   -96,  -173,  -485,  -394,   119,  -198,   188,
     609,  -202,  -486,   -88,   103,  -486,   -13,  -486,  -486,  -179,
    -486,   428,  -486,   -19,  -486,    23,  -341,    -2,  -486,    22,
    -486,   293,   -48,  -486,  -486,  -486,    40,   628,  -486,  -486,
     179,  -486,  -486,   183,   467,  -100,  -486,   379,   -47,  -486,
    -486,  -486,   378,  -486,  -486,  -486,   166,   381,  -486,   -62,
    -254,  -486,   186,  -486,   189,  -486,  -486,   182,  -486,  -486,
     258,  -486,  -112,    16,  -486
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -352
static const yytype_int16 yytable[] =
{
      35,   170,    81,   304,    51,    52,   255,   373,   372,   209,
     156,    94,   448,   106,   109,   156,   166,    92,   460,   256,
     395,   324,    83,    86,    54,   464,   308,   457,   195,   195,
      94,   264,    43,    61,    63,    94,   230,   564,   468,   313,
     101,    92,    94,   359,   211,   278,    12,   526,    92,   268,
      59,   283,    95,   211,   211,   285,   199,   238,    50,   188,
     477,    67,   269,    69,   495,   201,   112,   360,   231,   200,
     362,    95,   357,   571,   531,    96,    95,   239,   230,    97,
     245,   110,   437,    95,    12,   402,    26,    27,    28,   469,
     277,   515,   470,   113,    99,   175,   114,    44,   115,    44,
     196,   196,   181,    45,   242,   567,   172,  -351,   606,   418,
     231,   478,   202,    54,   479,   239,   203,   319,   467,   140,
     182,   183,   222,   464,    26,    27,    28,   111,   565,   350,
     366,   276,   111,    16,   227,    12,   208,   282,   313,   262,
     263,   578,   351,   367,    61,   266,   267,   111,   356,   270,
     271,   272,   368,   292,   444,   340,   261,   143,   293,   446,
     141,   265,   284,    12,   163,   399,    54,   579,   322,   288,
     274,   142,   597,   526,    12,    26,    27,    28,   565,    94,
     565,   305,   364,   563,   294,   303,   144,   296,   605,   297,
     580,   302,    12,  -144,   311,   145,   565,   213,   581,   565,
     306,   307,   173,    26,    27,    28,   214,   433,  -199,   335,
     410,    96,   394,   164,    26,    27,    28,   280,   220,   515,
      95,  -199,   222,   281,   170,   429,   156,   430,   174,   156,
     431,   253,    26,    27,    28,   222,   222,   395,   156,   166,
      94,    94,   222,   222,   442,   248,   251,   375,   503,   191,
     533,   505,   192,   367,    12,   339,   339,   176,   113,   180,
     193,   114,   368,   115,   459,   474,   339,   222,   184,   185,
     483,   222,   186,   187,   222,   339,   177,   400,   401,   490,
      12,    95,    95,   376,   179,   409,   486,   381,   488,   489,
     583,   372,   545,   189,    26,    27,    28,   233,    12,   254,
     353,   550,   243,   330,   222,   113,   252,   194,   114,     1,
     115,     2,     3,   198,    54,    68,   228,    84,   220,    12,
      26,    27,    28,   502,   330,   234,   210,   570,   324,   239,
     507,   314,   242,   244,   222,   538,   331,   222,    26,    27,
      28,   519,   257,   222,   222,   259,   222,   260,   335,   222,
     239,    94,   436,    12,   534,   279,   345,   286,   211,    26,
      27,    28,   290,   222,   358,   346,   347,   324,    68,   287,
     537,   592,   289,   222,   291,   596,   189,   190,   476,   549,
     203,   222,   348,    12,   311,   487,   349,   213,   330,   318,
     419,   420,    95,    26,    27,    28,   214,   184,   423,   224,
     220,   485,   226,   608,   424,   425,   561,   325,   325,   496,
      12,   498,   499,   514,   239,   335,   516,    94,   529,   337,
     321,   331,   336,    26,    27,    28,   426,   427,   352,   394,
     417,   434,   435,   453,   454,   130,   325,   424,   501,   325,
     222,   586,   222,   156,   222,   361,   335,   222,   275,   222,
      26,    27,    28,   363,   587,   588,   440,   380,    95,   385,
     222,   236,   237,   222,   222,   343,   344,   210,   544,   220,
      12,   222,   386,   222,    54,    16,   247,   250,   222,   222,
     455,   226,   456,   440,   387,   604,    64,    65,   389,   405,
     557,   543,   388,   411,    12,   412,   413,   376,   414,   211,
     548,   509,   325,  -143,   572,   574,   381,   381,   325,   577,
      26,    27,    28,   415,   432,   428,   328,   441,   329,   443,
     450,   335,   178,    94,   449,   212,   211,   222,   213,   222,
     458,   465,   281,   492,    26,    27,    28,   214,   475,   484,
     504,   514,   590,   506,   491,   222,   325,   100,   222,   222,
     222,   520,   101,  -145,  -146,   528,   102,   103,   222,   222,
     527,   530,    12,   531,    95,   539,   540,    16,   542,   397,
     535,   469,   536,   104,   478,   404,   554,   105,   555,   577,
     562,   560,   603,   508,   558,    79,   559,   582,   222,   222,
     222,   210,   222,   509,   510,   222,   222,   222,   584,   585,
     180,   599,    26,    27,    28,   325,    12,   598,   210,   600,
     609,   330,   610,   222,   611,   117,   222,   518,    12,   517,
     370,   371,   222,   211,   541,   466,   589,    80,   325,   374,
     607,   445,   568,    82,   309,    12,   325,   239,   461,   440,
     211,   566,   591,   325,   325,   310,    26,    27,    28,   311,
      77,   312,   213,    87,   547,   546,   398,   556,    26,    27,
      28,   214,   310,   551,   408,   406,   311,    77,   416,   213,
     497,   553,     0,   552,   345,    26,    27,    28,   214,   530,
       0,     0,     0,   346,   347,    12,   527,     0,     0,   493,
     322,   566,   325,   566,   325,     0,   494,     0,     0,     0,
     348,     0,     0,     0,   349,     0,     0,     0,     0,   566,
       0,     0,   566,   325,   325,   325,   311,     0,   220,   213,
       0,     0,     0,   325,   325,    26,    27,    28,   214,     0,
       0,     0,     0,     0,     0,   195,     0,     0,     0,   147,
       0,     0,     0,   148,     0,   149,     0,   150,     0,     0,
       0,     0,     0,   325,   325,     0,     9,   325,     0,    10,
      11,   325,    12,    13,    14,    15,     0,    16,    17,     0,
       0,     0,    18,    19,    20,     0,     0,   151,   152,   153,
     154,     0,     0,     0,    12,    21,     0,   155,   101,   322,
      23,     0,     0,    24,     0,     0,    25,     0,     0,     0,
       0,     0,    26,    27,    28,    29,    30,   196,     5,     0,
       0,     0,     6,   573,     0,   311,     7,     0,   213,     0,
       8,   571,     0,     0,    26,    27,    28,   214,     0,     9,
       0,     0,    10,    11,    70,    12,    13,    14,    15,    71,
      16,    17,    72,    73,    74,    18,    19,    20,     0,     0,
       0,     0,     0,     0,     0,     0,    12,     0,    21,     0,
      75,   322,     0,    23,    76,     0,    24,    77,    78,    25,
       0,     0,    79,     0,     0,    26,    27,    28,    29,    30,
       5,     0,     0,   326,     6,     0,  -144,   311,     7,     0,
     213,     0,     8,     0,     0,     0,    26,    27,    28,   214,
       0,     9,     0,     0,    10,    11,     0,    12,    13,    14,
      15,     0,    16,    17,     0,     0,     0,    18,    19,    20,
       0,     0,     0,     0,     0,     0,     0,     0,    12,     0,
      21,     0,    22,   322,     0,    23,     0,     0,    24,     0,
       0,    25,     0,  -334,     0,     0,     0,    26,    27,    28,
      29,    30,     5,     0,     0,   323,     6,     0,     0,   311,
       7,     0,   213,     0,     8,     0,     0,     0,    26,    27,
      28,   214,     0,     9,     0,     0,    10,    11,     0,    12,
      13,    14,    15,     0,    16,    17,     0,     0,     0,    18,
      19,    20,     0,     0,     0,     0,     0,     0,     0,     0,
      12,     0,    21,     0,    22,   322,     0,    23,     0,     0,
      24,     0,     0,    25,     0,  -340,     0,     0,     0,    26,
      27,    28,    29,    30,     5,     0,     0,   326,     6,     0,
       0,   311,     7,     0,   213,     0,     8,     0,     0,     0,
      26,    27,    28,   214,     0,     9,     0,     0,    10,    11,
       0,    12,    13,    14,    15,     0,    16,    17,     0,     0,
       0,    18,    19,    20,     0,     0,     0,     0,     0,     0,
       0,     0,    12,     0,    21,     0,    22,   322,     0,    23,
       0,     0,    24,     0,     0,    25,     0,  -339,     0,     0,
       0,    26,    27,    28,    29,    30,     5,     0,     0,     0,
       6,   569,     0,   311,     7,     0,   213,     0,     8,     0,
       0,     0,    26,    27,    28,   214,     0,     9,     0,     0,
      10,    11,     0,    12,    13,    14,    15,     0,    16,    17,
       0,     0,     0,    18,    19,    20,     0,     0,     0,     0,
       0,     0,     0,     0,    12,     0,    21,     0,    22,   211,
       0,    23,     0,     0,    24,     0,     0,    25,     0,     0,
       0,     0,     0,    26,    27,    28,    29,    30,     5,     0,
       0,     0,     6,   593,     0,   311,     7,     0,   213,     0,
     298,     0,   210,     0,    26,    27,    28,   214,     0,     9,
       0,     0,    10,    11,     0,    12,    13,    14,    15,     0,
      16,    17,     0,     0,     0,    18,    19,    20,     0,    12,
       0,     0,     0,     0,   211,     0,     0,     0,    21,     0,
      22,   521,     0,    23,     0,     0,    24,     0,    12,    25,
       0,     0,   101,   322,   210,    26,    27,    28,    29,    30,
     311,     0,  -137,   213,     0,     0,     0,     0,    12,    26,
      27,    28,   214,   211,     0,     0,     0,     0,     0,   311,
       0,    12,   213,     0,     0,   571,   211,     0,    26,    27,
      28,   214,     0,     0,     0,     0,     0,   522,     0,   311,
       0,     0,   213,     0,     0,     0,     0,     0,    26,    27,
      28,   214,   311,     0,     0,   213,     0,     0,     0,     0,
       0,    26,    27,    28,   214,     6,     0,   118,   119,     7,
       0,     0,     0,     8,     0,   120,   121,   122,   123,   124,
     125,     0,     9,   126,   127,    10,    11,     0,    12,    13,
      14,    15,     0,    16,   128,     0,     0,     0,    18,    19,
      20,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    21,     0,    22,     0,     0,    23,     0,     0,    24,
       0,     0,    25,   129,     0,     0,     0,   130,    26,    27,
      28,    29,    30,     6,     0,   118,   119,     7,     0,     0,
       0,     8,     0,   120,   121,   122,   123,   124,   125,     0,
       9,   126,   127,    10,    11,     0,    12,    13,    14,    15,
       0,    16,   128,     0,     0,     0,    18,    19,    20,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    21,
       0,    22,     0,     0,    23,     0,     0,    24,     0,     0,
      25,   129,     0,     0,     0,     0,    26,    27,    28,    29,
      30,     6,     0,   118,   119,     7,     0,     0,     0,     8,
       0,     0,     0,     0,   123,   124,   125,     0,     9,   126,
     127,    10,    11,     0,    12,    13,    14,    15,     0,    16,
     128,     0,     0,     0,    18,    19,    20,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    21,     0,    22,
       0,     0,    23,   147,     0,    24,     0,   148,    25,   149,
       0,   150,     0,     0,    26,    27,    28,    29,    30,     0,
       9,     0,     0,    10,    11,     0,    12,    13,    14,    15,
       0,    16,    17,     0,     0,     0,    18,    19,    20,     0,
       0,   151,   152,   153,   154,     0,     0,     0,     0,    21,
       0,   155,     0,     0,    23,     6,     0,    24,     0,     7,
      25,     0,     0,     8,     0,     0,    26,    27,    28,    29,
      30,     0,     9,     0,     0,    10,    11,     0,    12,    13,
      14,    15,     0,    16,    17,     0,     0,     0,    18,    19,
      20,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    21,     0,    22,     0,     0,    23,     6,     0,    24,
       0,     7,    25,    89,     0,     8,     0,     0,    26,    27,
      28,    29,    30,     0,     9,     0,     0,    10,    11,     0,
      12,    13,    14,    15,     0,    16,    17,     0,     0,     0,
      18,    19,    20,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    21,     0,   225,     0,     0,    23,     0,
       0,    24,     0,   295,    25,     0,     0,     0,     0,     0,
      26,    27,    28,    29,    30,     6,     0,     0,     0,     7,
       0,     0,    12,     8,     0,     0,     0,   322,     0,     0,
       0,     0,     9,     0,     0,    10,    11,     0,    12,    13,
      14,    15,     0,    16,    17,     0,     0,     0,    18,    19,
      20,     0,     0,   311,   451,   452,   213,     0,     0,     0,
       0,    21,    26,    27,    28,   214,    23,     0,     0,    24,
       0,   178,    25,     0,     0,     0,     0,     0,    26,    27,
      28,    29,    30,     6,     0,     0,     0,     7,     0,     0,
       0,     8,     0,     0,     0,     0,     0,     0,     0,     0,
       9,     0,     0,    10,    11,     0,    12,    13,    14,    15,
       0,    16,    17,     0,     0,     0,    18,    19,    20,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    21,
       0,   223,     0,     0,    23,     6,     0,    24,     0,     7,
      25,     0,     0,     8,     0,     0,    26,    27,    28,    29,
      30,     0,     9,     0,     0,    10,    11,     0,    12,    13,
      14,    15,     0,    16,    17,     0,     0,     0,    18,    19,
      20,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    21,     0,   225,     0,     0,    23,     6,     0,    24,
       0,     7,    25,     0,     0,     8,     0,     0,    26,    27,
      28,    29,    30,     0,     9,     0,     0,    10,    11,     0,
      12,    13,    14,    15,     0,    16,    17,     0,     0,     0,
      18,    19,    20,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    21,     0,    22,     0,     0,    23,     6,
       0,    24,     0,     7,    25,     0,     0,     8,     0,     0,
      26,    27,    28,    29,    30,     0,     9,     0,     0,    10,
      11,     0,    12,    13,    14,    15,     0,    16,    17,     0,
       0,     0,    18,    19,    20,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    21,     0,   396,     0,     0,
      23,     6,     0,    24,     0,     7,    25,     0,     0,     8,
       0,     0,    26,    27,    28,    29,    30,     0,     9,     0,
       0,    10,    11,     0,    12,    13,    14,    15,     0,    16,
      17,     0,     0,     0,    18,    19,    20,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    21,     0,   403,
       0,     0,    23,     6,     0,    24,     0,     7,    25,     0,
       0,     8,     0,     0,    26,    27,    28,    29,    30,     0,
       9,     0,     0,    10,    11,     0,    12,    13,    14,    15,
       0,    16,    17,     0,     0,     0,    18,    19,    20,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    21,
      12,     0,     0,     0,    23,   211,     0,    24,     0,     0,
      25,     0,     8,     0,     0,     0,    26,    27,    28,    29,
      30,     9,     0,     0,    10,    11,     0,    12,    13,    14,
      15,   311,    16,    17,   213,     0,     0,    18,    19,    20,
      26,    27,    28,   214,     0,     0,     0,     0,     0,     0,
      21,     0,   155,     0,     0,    23,     0,     0,    24,     0,
       0,    25,   165,     8,     0,     0,     0,    26,    27,    28,
      29,    30,     9,     0,     0,    10,    11,     0,    12,    13,
      14,    15,     0,    16,    17,     0,     0,     0,    18,    19,
      20,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    21,     0,   155,     0,     0,    23,     0,     0,    24,
       0,     0,    25,   390,     8,     0,     0,     0,    26,    27,
      28,    29,    30,     9,     0,     0,    10,    11,     0,    12,
      13,    14,    15,     0,    16,    17,     0,     0,     0,    18,
      19,    20,     0,     0,     0,     0,    11,     0,    12,    13,
      14,    15,    21,    16,   155,     0,     0,    23,     0,     0,
      24,     0,     0,    25,     0,     0,     0,     0,     0,    26,
      27,    28,    29,    30,     0,   171,    23,     0,    12,    24,
       0,     0,    25,   322,     0,     0,     0,     0,    26,    27,
      28,    29,    30,    11,     0,    12,    13,    14,    15,     0,
      16,     0,     0,     0,     0,   326,     0,     0,     0,   311,
     421,   422,   213,     0,     0,     0,     0,    12,    26,    27,
      28,   214,   322,    23,     0,     0,    24,     0,     0,    25,
       0,     0,     0,     0,     0,    26,    27,    28,    29,    30,
       0,     0,     0,     0,   326,     0,     0,     0,   311,   421,
     500,   213,     0,     0,     0,     0,     0,    26,    27,    28,
     214
};

static const yytype_int16 yycheck[] =
{
       2,    63,    24,   199,     6,     7,   130,   259,   259,    97,
      58,    30,   353,    38,    39,    63,    63,    30,   366,   131,
     274,   219,    24,    25,     8,   367,   205,   364,     1,     1,
      49,   143,     1,    17,    18,    54,     1,   522,     1,   212,
      32,    54,    61,   245,    33,   157,    28,   441,    61,   149,
      10,   163,    30,    33,    33,   167,    47,     1,     0,    84,
       1,    21,   150,    23,   405,     1,     1,   246,    33,    60,
     249,    49,    61,    65,    21,    50,    54,    59,     1,    12,
      59,    41,    62,    61,    28,   281,    68,    69,    70,    52,
      63,   432,    55,    28,    47,    72,    31,    66,    33,    66,
      73,    73,    79,    72,   123,    52,    66,    72,   593,   311,
      33,    52,    48,    97,    55,    59,    52,   213,   370,     6,
      80,    81,    99,   465,    68,    69,    70,    35,   522,    47,
     254,   156,    35,    33,   111,    28,    96,   162,   311,   141,
     142,    33,    60,    59,   128,   147,   148,    35,   244,   151,
     152,   153,    68,    61,   350,    48,   140,    54,    61,    59,
      10,   145,   164,    28,    61,   277,   150,    59,    33,   171,
     154,    14,   574,   567,    28,    68,    69,    70,   572,   198,
     574,   200,    36,   520,   186,   198,    72,   189,   590,   191,
     538,   193,    28,    58,    59,    42,   590,    62,   540,   593,
     202,   203,    61,    68,    69,    70,    71,   331,    47,   228,
     298,    50,   274,    14,    68,    69,    70,    41,    99,   560,
     198,    60,   199,    47,   286,   321,   274,   323,    61,   277,
     326,     1,    68,    69,    70,   212,   213,   491,   286,   286,
     259,   260,   219,   220,   340,   126,   127,   260,   421,    49,
     448,   424,    52,    59,    28,   232,   233,    61,    28,    33,
      60,    31,    68,    33,    70,   377,   243,   244,    60,    61,
     382,   248,    60,    61,   251,   252,    61,   279,   280,   391,
      28,   259,   260,   261,    61,   287,   386,   265,   388,   389,
     542,   542,   471,    60,    68,    69,    70,   119,    28,    69,
      48,   480,   124,    33,   281,    28,   128,    64,    31,     1,
      33,     3,     4,    63,   298,    22,    59,    24,   199,    28,
      68,    69,    70,   419,    33,    29,     1,   525,   526,    59,
     426,   212,   351,    59,   311,   459,    66,   314,    68,    69,
      70,   437,    63,   320,   321,    63,   323,    63,   367,   326,
      59,   370,   336,    28,   450,    54,    27,    63,    33,    68,
      69,    70,    65,   340,   245,    36,    37,   565,    75,    54,
     458,   569,    61,   350,    65,   573,    60,    61,   380,   479,
      52,   358,    53,    28,    59,   387,    57,    62,    33,    64,
      60,    61,   370,    68,    69,    70,    71,    60,    61,   106,
     281,   385,   109,   599,    60,    61,   518,   219,   220,   411,
      28,   413,   414,   432,    59,   434,    61,   436,   443,    37,
      58,    66,    12,    68,    69,    70,    60,    61,    31,   491,
     311,    60,    61,    60,    61,    67,   248,    60,    61,   251,
     417,   553,   419,   491,   421,    58,   465,   424,   155,   426,
      68,    69,    70,    58,   554,   555,   337,    11,   436,     6,
     437,   121,   122,   440,   441,   236,   237,     1,   470,   350,
      28,   448,    10,   450,   458,    33,   126,   127,   455,   456,
     361,   188,   363,   364,    54,   585,    19,    20,    10,    49,
     492,   469,    22,    54,    28,    60,    49,   475,    48,    33,
     478,    59,   314,    58,   526,   527,   484,   485,   320,   528,
      68,    69,    70,    61,    59,    64,   223,    48,   225,    60,
      60,   540,    61,   542,    61,    59,    33,   504,    62,   506,
      48,    59,    47,    54,    68,    69,    70,    71,    63,    63,
     421,   560,   564,   424,    63,   522,   358,    27,   525,   526,
     527,    60,    32,    58,    58,    14,    36,    37,   535,   536,
     441,    32,    28,    21,   542,    61,    60,    33,    63,   276,
     451,    52,   453,    53,    52,   282,    11,    57,    22,   598,
      64,    60,   584,    49,    61,    65,    61,    61,   565,   566,
     567,     1,   569,    59,    60,   572,   573,   574,    55,    55,
      33,    47,    68,    69,    70,   417,    28,    60,     1,    33,
      61,    33,    60,   590,    33,    47,   593,   436,    28,   434,
     257,   257,   599,    33,   465,   370,   560,    24,   440,   260,
     598,   351,   523,    24,   206,    28,   448,    59,    60,   520,
      33,   522,   567,   455,   456,    55,    68,    69,    70,    59,
      60,    61,    62,    25,   475,   472,   277,   491,    68,    69,
      70,    71,    55,   481,   286,   284,    59,    60,    61,    62,
     412,   485,    -1,   484,    27,    68,    69,    70,    71,    32,
      -1,    -1,    -1,    36,    37,    28,   567,    -1,    -1,   396,
      33,   572,   504,   574,   506,    -1,   403,    -1,    -1,    -1,
      53,    -1,    -1,    -1,    57,    -1,    -1,    -1,    -1,   590,
      -1,    -1,   593,   525,   526,   527,    59,    -1,   599,    62,
      -1,    -1,    -1,   535,   536,    68,    69,    70,    71,    -1,
      -1,    -1,    -1,    -1,    -1,     1,    -1,    -1,    -1,     5,
      -1,    -1,    -1,     9,    -1,    11,    -1,    13,    -1,    -1,
      -1,    -1,    -1,   565,   566,    -1,    22,   569,    -1,    25,
      26,   573,    28,    29,    30,    31,    -1,    33,    34,    -1,
      -1,    -1,    38,    39,    40,    -1,    -1,    43,    44,    45,
      46,    -1,    -1,    -1,    28,    51,    -1,    53,    32,    33,
      56,    -1,    -1,    59,    -1,    -1,    62,    -1,    -1,    -1,
      -1,    -1,    68,    69,    70,    71,    72,    73,     1,    -1,
      -1,    -1,     5,    57,    -1,    59,     9,    -1,    62,    -1,
      13,    65,    -1,    -1,    68,    69,    70,    71,    -1,    22,
      -1,    -1,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    28,    -1,    51,    -1,
      53,    33,    -1,    56,    57,    -1,    59,    60,    61,    62,
      -1,    -1,    65,    -1,    -1,    68,    69,    70,    71,    72,
       1,    -1,    -1,    55,     5,    -1,    58,    59,     9,    -1,
      62,    -1,    13,    -1,    -1,    -1,    68,    69,    70,    71,
      -1,    22,    -1,    -1,    25,    26,    -1,    28,    29,    30,
      31,    -1,    33,    34,    -1,    -1,    -1,    38,    39,    40,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    28,    -1,
      51,    -1,    53,    33,    -1,    56,    -1,    -1,    59,    -1,
      -1,    62,    -1,    64,    -1,    -1,    -1,    68,    69,    70,
      71,    72,     1,    -1,    -1,    55,     5,    -1,    -1,    59,
       9,    -1,    62,    -1,    13,    -1,    -1,    -1,    68,    69,
      70,    71,    -1,    22,    -1,    -1,    25,    26,    -1,    28,
      29,    30,    31,    -1,    33,    34,    -1,    -1,    -1,    38,
      39,    40,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      28,    -1,    51,    -1,    53,    33,    -1,    56,    -1,    -1,
      59,    -1,    -1,    62,    -1,    64,    -1,    -1,    -1,    68,
      69,    70,    71,    72,     1,    -1,    -1,    55,     5,    -1,
      -1,    59,     9,    -1,    62,    -1,    13,    -1,    -1,    -1,
      68,    69,    70,    71,    -1,    22,    -1,    -1,    25,    26,
      -1,    28,    29,    30,    31,    -1,    33,    34,    -1,    -1,
      -1,    38,    39,    40,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    28,    -1,    51,    -1,    53,    33,    -1,    56,
      -1,    -1,    59,    -1,    -1,    62,    -1,    64,    -1,    -1,
      -1,    68,    69,    70,    71,    72,     1,    -1,    -1,    -1,
       5,    57,    -1,    59,     9,    -1,    62,    -1,    13,    -1,
      -1,    -1,    68,    69,    70,    71,    -1,    22,    -1,    -1,
      25,    26,    -1,    28,    29,    30,    31,    -1,    33,    34,
      -1,    -1,    -1,    38,    39,    40,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    28,    -1,    51,    -1,    53,    33,
      -1,    56,    -1,    -1,    59,    -1,    -1,    62,    -1,    -1,
      -1,    -1,    -1,    68,    69,    70,    71,    72,     1,    -1,
      -1,    -1,     5,    57,    -1,    59,     9,    -1,    62,    -1,
      13,    -1,     1,    -1,    68,    69,    70,    71,    -1,    22,
      -1,    -1,    25,    26,    -1,    28,    29,    30,    31,    -1,
      33,    34,    -1,    -1,    -1,    38,    39,    40,    -1,    28,
      -1,    -1,    -1,    -1,    33,    -1,    -1,    -1,    51,    -1,
      53,     1,    -1,    56,    -1,    -1,    59,    -1,    28,    62,
      -1,    -1,    32,    33,     1,    68,    69,    70,    71,    72,
      59,    -1,    61,    62,    -1,    -1,    -1,    -1,    28,    68,
      69,    70,    71,    33,    -1,    -1,    -1,    -1,    -1,    59,
      -1,    28,    62,    -1,    -1,    65,    33,    -1,    68,    69,
      70,    71,    -1,    -1,    -1,    -1,    -1,    57,    -1,    59,
      -1,    -1,    62,    -1,    -1,    -1,    -1,    -1,    68,    69,
      70,    71,    59,    -1,    -1,    62,    -1,    -1,    -1,    -1,
      -1,    68,    69,    70,    71,     5,    -1,     7,     8,     9,
      -1,    -1,    -1,    13,    -1,    15,    16,    17,    18,    19,
      20,    -1,    22,    23,    24,    25,    26,    -1,    28,    29,
      30,    31,    -1,    33,    34,    -1,    -1,    -1,    38,    39,
      40,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    51,    -1,    53,    -1,    -1,    56,    -1,    -1,    59,
      -1,    -1,    62,    63,    -1,    -1,    -1,    67,    68,    69,
      70,    71,    72,     5,    -1,     7,     8,     9,    -1,    -1,
      -1,    13,    -1,    15,    16,    17,    18,    19,    20,    -1,
      22,    23,    24,    25,    26,    -1,    28,    29,    30,    31,
      -1,    33,    34,    -1,    -1,    -1,    38,    39,    40,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    51,
      -1,    53,    -1,    -1,    56,    -1,    -1,    59,    -1,    -1,
      62,    63,    -1,    -1,    -1,    -1,    68,    69,    70,    71,
      72,     5,    -1,     7,     8,     9,    -1,    -1,    -1,    13,
      -1,    -1,    -1,    -1,    18,    19,    20,    -1,    22,    23,
      24,    25,    26,    -1,    28,    29,    30,    31,    -1,    33,
      34,    -1,    -1,    -1,    38,    39,    40,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    51,    -1,    53,
      -1,    -1,    56,     5,    -1,    59,    -1,     9,    62,    11,
      -1,    13,    -1,    -1,    68,    69,    70,    71,    72,    -1,
      22,    -1,    -1,    25,    26,    -1,    28,    29,    30,    31,
      -1,    33,    34,    -1,    -1,    -1,    38,    39,    40,    -1,
      -1,    43,    44,    45,    46,    -1,    -1,    -1,    -1,    51,
      -1,    53,    -1,    -1,    56,     5,    -1,    59,    -1,     9,
      62,    -1,    -1,    13,    -1,    -1,    68,    69,    70,    71,
      72,    -1,    22,    -1,    -1,    25,    26,    -1,    28,    29,
      30,    31,    -1,    33,    34,    -1,    -1,    -1,    38,    39,
      40,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    51,    -1,    53,    -1,    -1,    56,     5,    -1,    59,
      -1,     9,    62,    63,    -1,    13,    -1,    -1,    68,    69,
      70,    71,    72,    -1,    22,    -1,    -1,    25,    26,    -1,
      28,    29,    30,    31,    -1,    33,    34,    -1,    -1,    -1,
      38,    39,    40,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    51,    -1,    53,    -1,    -1,    56,    -1,
      -1,    59,    -1,    61,    62,    -1,    -1,    -1,    -1,    -1,
      68,    69,    70,    71,    72,     5,    -1,    -1,    -1,     9,
      -1,    -1,    28,    13,    -1,    -1,    -1,    33,    -1,    -1,
      -1,    -1,    22,    -1,    -1,    25,    26,    -1,    28,    29,
      30,    31,    -1,    33,    34,    -1,    -1,    -1,    38,    39,
      40,    -1,    -1,    59,    60,    61,    62,    -1,    -1,    -1,
      -1,    51,    68,    69,    70,    71,    56,    -1,    -1,    59,
      -1,    61,    62,    -1,    -1,    -1,    -1,    -1,    68,    69,
      70,    71,    72,     5,    -1,    -1,    -1,     9,    -1,    -1,
      -1,    13,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      22,    -1,    -1,    25,    26,    -1,    28,    29,    30,    31,
      -1,    33,    34,    -1,    -1,    -1,    38,    39,    40,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    51,
      -1,    53,    -1,    -1,    56,     5,    -1,    59,    -1,     9,
      62,    -1,    -1,    13,    -1,    -1,    68,    69,    70,    71,
      72,    -1,    22,    -1,    -1,    25,    26,    -1,    28,    29,
      30,    31,    -1,    33,    34,    -1,    -1,    -1,    38,    39,
      40,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    51,    -1,    53,    -1,    -1,    56,     5,    -1,    59,
      -1,     9,    62,    -1,    -1,    13,    -1,    -1,    68,    69,
      70,    71,    72,    -1,    22,    -1,    -1,    25,    26,    -1,
      28,    29,    30,    31,    -1,    33,    34,    -1,    -1,    -1,
      38,    39,    40,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    51,    -1,    53,    -1,    -1,    56,     5,
      -1,    59,    -1,     9,    62,    -1,    -1,    13,    -1,    -1,
      68,    69,    70,    71,    72,    -1,    22,    -1,    -1,    25,
      26,    -1,    28,    29,    30,    31,    -1,    33,    34,    -1,
      -1,    -1,    38,    39,    40,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    51,    -1,    53,    -1,    -1,
      56,     5,    -1,    59,    -1,     9,    62,    -1,    -1,    13,
      -1,    -1,    68,    69,    70,    71,    72,    -1,    22,    -1,
      -1,    25,    26,    -1,    28,    29,    30,    31,    -1,    33,
      34,    -1,    -1,    -1,    38,    39,    40,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    51,    -1,    53,
      -1,    -1,    56,     5,    -1,    59,    -1,     9,    62,    -1,
      -1,    13,    -1,    -1,    68,    69,    70,    71,    72,    -1,
      22,    -1,    -1,    25,    26,    -1,    28,    29,    30,    31,
      -1,    33,    34,    -1,    -1,    -1,    38,    39,    40,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    51,
      28,    -1,    -1,    -1,    56,    33,    -1,    59,    -1,    -1,
      62,    -1,    13,    -1,    -1,    -1,    68,    69,    70,    71,
      72,    22,    -1,    -1,    25,    26,    -1,    28,    29,    30,
      31,    59,    33,    34,    62,    -1,    -1,    38,    39,    40,
      68,    69,    70,    71,    -1,    -1,    -1,    -1,    -1,    -1,
      51,    -1,    53,    -1,    -1,    56,    -1,    -1,    59,    -1,
      -1,    62,    63,    13,    -1,    -1,    -1,    68,    69,    70,
      71,    72,    22,    -1,    -1,    25,    26,    -1,    28,    29,
      30,    31,    -1,    33,    34,    -1,    -1,    -1,    38,    39,
      40,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    51,    -1,    53,    -1,    -1,    56,    -1,    -1,    59,
      -1,    -1,    62,    63,    13,    -1,    -1,    -1,    68,    69,
      70,    71,    72,    22,    -1,    -1,    25,    26,    -1,    28,
      29,    30,    31,    -1,    33,    34,    -1,    -1,    -1,    38,
      39,    40,    -1,    -1,    -1,    -1,    26,    -1,    28,    29,
      30,    31,    51,    33,    53,    -1,    -1,    56,    -1,    -1,
      59,    -1,    -1,    62,    -1,    -1,    -1,    -1,    -1,    68,
      69,    70,    71,    72,    -1,    55,    56,    -1,    28,    59,
      -1,    -1,    62,    33,    -1,    -1,    -1,    -1,    68,    69,
      70,    71,    72,    26,    -1,    28,    29,    30,    31,    -1,
      33,    -1,    -1,    -1,    -1,    55,    -1,    -1,    -1,    59,
      60,    61,    62,    -1,    -1,    -1,    -1,    28,    68,    69,
      70,    71,    33,    56,    -1,    -1,    59,    -1,    -1,    62,
      -1,    -1,    -1,    -1,    -1,    68,    69,    70,    71,    72,
      -1,    -1,    -1,    -1,    55,    -1,    -1,    -1,    59,    60,
      61,    62,    -1,    -1,    -1,    -1,    -1,    68,    69,    70,
      71
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     1,     3,     4,    75,     1,     5,     9,    13,    22,
      25,    26,    28,    29,    30,    31,    33,    34,    38,    39,
      40,    51,    53,    56,    59,    62,    68,    69,    70,    71,
      72,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   159,   160,     1,    66,    72,    76,    77,    78,   195,
       0,   151,   151,   137,   197,   198,   168,   169,   197,   160,
     157,   197,   173,   197,   168,   168,   158,   160,   155,   160,
      27,    32,    35,    36,    37,    53,    57,    60,    61,    65,
      99,   100,   134,   151,   155,   161,   151,   161,   192,    63,
     138,   139,   140,   146,   147,   153,    50,    12,   143,    47,
      27,    32,    36,    37,    53,    57,    98,    99,   100,    98,
     160,    35,     1,    28,    31,    33,    79,    78,     7,     8,
      15,    16,    17,    18,    19,    20,    23,    24,    34,    63,
      67,    80,    84,    86,    94,    95,   101,   102,   103,   140,
       6,    10,    14,   138,    72,    42,   181,     5,     9,    11,
      13,    43,    44,    45,    46,    53,   156,   170,   171,   172,
     183,   184,   185,   138,    14,    63,   172,   174,   175,   176,
     183,    55,   160,    61,    61,   149,    61,    61,    61,    61,
      33,   149,   160,   160,    60,    61,    60,    61,    98,    60,
      61,    49,    52,    60,    64,     1,    73,   196,    63,    47,
      60,     1,    48,    52,   141,   142,   144,   145,   160,   137,
       1,    33,    59,    62,    71,   125,   126,   127,   128,   130,
     131,   133,   149,    53,   155,    53,   155,   149,    59,    81,
       1,    33,   108,   108,    29,    96,    96,    96,     1,    59,
     119,   120,   147,   108,    59,    59,   121,   126,   131,   122,
     126,   131,   108,     1,    69,    79,   196,    63,    85,    63,
      63,   197,   151,   151,   196,   197,   151,   151,   169,   137,
     151,   151,   151,   177,   197,   155,    98,    63,   196,    54,
      41,    47,    98,   196,   151,   196,    63,    54,   151,    61,
      65,    65,    61,    61,   151,    61,   151,   151,    13,   151,
     193,   194,   151,   140,   125,   147,   151,   151,   143,   145,
      55,    59,    61,   128,   131,   134,   135,   136,    64,   127,
     131,    58,    33,    55,   132,   133,    55,   132,   155,   155,
      33,    66,    82,    83,    90,   147,    12,    37,   104,   149,
      48,    97,    98,    97,    97,    27,    36,    37,    53,    57,
      47,    60,    31,    48,   123,   124,   127,    61,   131,   135,
     143,    58,   143,    58,    36,   105,    79,    59,    68,    87,
      85,    86,    95,   101,   103,   140,   153,   162,   163,   164,
      11,   153,   186,   187,   188,     6,    10,    54,    22,    10,
      63,   178,   179,   180,   183,   184,    53,   155,   171,   196,
     151,   151,   125,    53,   155,    49,   181,   182,   176,   151,
     137,    54,    60,    49,    48,    61,    61,   131,   135,    60,
      61,    60,    61,    61,    60,    61,    60,    61,    64,   127,
     127,   127,    59,    79,    60,    61,   197,    62,   106,   107,
     131,    48,   127,    60,   125,   120,    59,   115,   150,    61,
      60,    60,    61,    60,    61,   131,   131,   106,    48,    70,
      87,    60,    88,    89,    90,    59,    94,   101,     1,    52,
      55,   165,   166,   167,   196,    63,   151,     1,    52,    55,
     189,   190,   191,   196,    63,   197,   169,   151,   169,   169,
     196,    63,    54,   155,   155,   150,   151,   194,   151,   151,
      61,    61,   127,   128,   131,   128,   131,   127,    49,    59,
      60,    91,    92,    93,   147,   150,    61,    83,    80,   127,
      60,     1,    57,   111,   112,   113,   130,   131,    14,    98,
      32,    21,   116,   132,   127,   131,   131,   137,    79,    61,
      60,    88,    63,   153,   151,   143,   167,   164,   153,   169,
     143,   191,   188,   186,    11,    22,   180,   151,    61,    61,
      60,   196,    64,   106,   129,   130,   131,    52,   116,    57,
     132,    65,   100,    57,   100,   109,   110,   147,    33,    59,
      87,    90,    61,   101,    55,    55,   196,   169,   169,    93,
     100,   112,   132,    57,   114,   129,   132,   114,    60,    47,
      33,   117,   118,   151,   169,   114,   129,   110,   125,    61,
      60,    33
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1455 of yacc.c  */
#line 88 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {inputExpr = letrec((yyvsp[(3) - (3)]),(yyvsp[(2) - (3)])); sp-=2;;}
    break;

  case 3:

/* Line 1455 of yacc.c  */
#line 89 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {valDefns  = (yyvsp[(2) - (2)]);            sp-=1;;}
    break;

  case 4:

/* Line 1455 of yacc.c  */
#line 90 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {syntaxError("input");;}
    break;

  case 5:

/* Line 1455 of yacc.c  */
#line 99 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(2) - (3)]));;}
    break;

  case 6:

/* Line 1455 of yacc.c  */
#line 100 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 7:

/* Line 1455 of yacc.c  */
#line 102 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(appendOnto((yyvsp[(2) - (2)]),(yyvsp[(1) - (2)])));;}
    break;

  case 8:

/* Line 1455 of yacc.c  */
#line 103 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 9:

/* Line 1455 of yacc.c  */
#line 106 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc7((yyvsp[(6) - (7)]));;}
    break;

  case 10:

/* Line 1455 of yacc.c  */
#line 107 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {syntaxError("module definition");;}
    break;

  case 11:

/* Line 1455 of yacc.c  */
#line 109 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 12:

/* Line 1455 of yacc.c  */
#line 110 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 13:

/* Line 1455 of yacc.c  */
#line 111 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 14:

/* Line 1455 of yacc.c  */
#line 113 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 15:

/* Line 1455 of yacc.c  */
#line 114 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(3) - (3)]));;}
    break;

  case 16:

/* Line 1455 of yacc.c  */
#line 115 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(NIL);;}
    break;

  case 17:

/* Line 1455 of yacc.c  */
#line 116 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4((yyvsp[(4) - (4)]));;}
    break;

  case 18:

/* Line 1455 of yacc.c  */
#line 118 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc6((yyvsp[(6) - (6)]));;}
    break;

  case 19:

/* Line 1455 of yacc.c  */
#line 123 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 20:

/* Line 1455 of yacc.c  */
#line 124 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(NIL);;}
    break;

  case 21:

/* Line 1455 of yacc.c  */
#line 125 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(NIL);;}
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 127 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(NIL);;}
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 128 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 24:

/* Line 1455 of yacc.c  */
#line 130 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 25:

/* Line 1455 of yacc.c  */
#line 131 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(NIL);;}
    break;

  case 26:

/* Line 1455 of yacc.c  */
#line 136 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {imps = cons((yyvsp[(3) - (3)]),imps); (yyval)=gc3(NIL);;}
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 137 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {imps = singleton((yyvsp[(1) - (1)])); (yyval)=gc1(NIL);;}
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 139 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {if (chase(imps)) {
					     clearStack();
					     onto(imps);
                                             done();
					     closeAnyInput();
					     return 0;
					 }
					 (yyval) = gc0(NIL);
					;}
    break;

  case 29:

/* Line 1455 of yacc.c  */
#line 149 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 151 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc6((yyvsp[(3) - (6)]));;}
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 153 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4((yyvsp[(3) - (4)]));;}
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 154 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {syntaxError("import declaration");;}
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 156 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 34:

/* Line 1455 of yacc.c  */
#line 157 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(NIL);;}
    break;

  case 35:

/* Line 1455 of yacc.c  */
#line 158 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(NIL);;}
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 160 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 37:

/* Line 1455 of yacc.c  */
#line 161 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(NIL);;}
    break;

  case 38:

/* Line 1455 of yacc.c  */
#line 162 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 39:

/* Line 1455 of yacc.c  */
#line 163 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(1) - (2)]));;}
    break;

  case 40:

/* Line 1455 of yacc.c  */
#line 165 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(NIL);;}
    break;

  case 41:

/* Line 1455 of yacc.c  */
#line 166 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 168 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 43:

/* Line 1455 of yacc.c  */
#line 169 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 44:

/* Line 1455 of yacc.c  */
#line 170 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(NIL);;}
    break;

  case 45:

/* Line 1455 of yacc.c  */
#line 171 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(NIL);;}
    break;

  case 46:

/* Line 1455 of yacc.c  */
#line 173 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 47:

/* Line 1455 of yacc.c  */
#line 174 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(NIL);;}
    break;

  case 48:

/* Line 1455 of yacc.c  */
#line 175 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 49:

/* Line 1455 of yacc.c  */
#line 176 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(1) - (2)]));;}
    break;

  case 50:

/* Line 1455 of yacc.c  */
#line 178 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(NIL);;}
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 179 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(NIL);;}
    break;

  case 52:

/* Line 1455 of yacc.c  */
#line 181 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 53:

/* Line 1455 of yacc.c  */
#line 182 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 187 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(NIL);;}
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 188 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 56:

/* Line 1455 of yacc.c  */
#line 190 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {fixDefn(LEFT_ASS,(yyvsp[(1) - (3)]),(yyvsp[(2) - (3)]),(yyvsp[(3) - (3)])); sp-=3;;}
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 191 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {fixDefn(RIGHT_ASS,(yyvsp[(1) - (3)]),(yyvsp[(2) - (3)]),(yyvsp[(3) - (3)]));sp-=3;;}
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 192 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {fixDefn(NON_ASS,(yyvsp[(1) - (3)]),(yyvsp[(2) - (3)]),(yyvsp[(3) - (3)]));  sp-=3;;}
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 194 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(checkPrec((yyvsp[(1) - (1)])));;}
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 195 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(mkInt(DEF_PREC));;}
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 197 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 198 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 200 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 201 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 202 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(varMinus);;}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 204 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 205 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(varBang);;}
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 206 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(varLT);;}
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 207 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(varGT);;}
    break;

  case 70:

/* Line 1455 of yacc.c  */
#line 208 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 210 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 72:

/* Line 1455 of yacc.c  */
#line 211 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 216 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 217 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(NIL);;}
    break;

  case 75:

/* Line 1455 of yacc.c  */
#line 218 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 76:

/* Line 1455 of yacc.c  */
#line 219 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(1) - (2)]));;}
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 221 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(1) - (3)]));;}
    break;

  case 78:

/* Line 1455 of yacc.c  */
#line 222 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 223 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 80:

/* Line 1455 of yacc.c  */
#line 224 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 229 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {defTycon(4,(yyvsp[(3) - (4)]),(yyvsp[(2) - (4)]),(yyvsp[(4) - (4)]),SYNONYM,NIL);;}
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 231 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {defTycon(6,(yyvsp[(3) - (6)]),(yyvsp[(2) - (6)]),ap((yyvsp[(4) - (6)]),(yyvsp[(6) - (6)])),RESTRICTSYN,NIL);;}
    break;

  case 83:

/* Line 1455 of yacc.c  */
#line 233 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {defTycon(6,(yyvsp[(4) - (6)]),(yyvsp[(2) - (6)]),ap(rev((yyvsp[(5) - (6)])),(yyvsp[(6) - (6)])),DATATYPE,(yyvsp[(3) - (6)]));;}
    break;

  case 84:

/* Line 1455 of yacc.c  */
#line 234 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {defTycon(3,(yyvsp[(1) - (3)]),(yyvsp[(2) - (3)]),ap(NIL,NIL),DATATYPE,(yyvsp[(3) - (3)]));;}
    break;

  case 85:

/* Line 1455 of yacc.c  */
#line 236 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {defTycon(5,(yyvsp[(3) - (5)]),(yyvsp[(2) - (5)]),ap((yyvsp[(4) - (5)]),(yyvsp[(5) - (5)])),NEWTYPE,NIL);;}
    break;

  case 86:

/* Line 1455 of yacc.c  */
#line 238 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {defTycon(5,(yyvsp[(1) - (5)]),(yyvsp[(2) - (5)]),rev((yyvsp[(5) - (5)])),STRUCTTYPE,(yyvsp[(3) - (5)]));;}
    break;

  case 87:

/* Line 1455 of yacc.c  */
#line 239 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {defTycon(3,(yyvsp[(1) - (3)]),(yyvsp[(2) - (3)]),NIL,STRUCTTYPE,(yyvsp[(3) - (3)]));;}
    break;

  case 88:

/* Line 1455 of yacc.c  */
#line 241 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 89:

/* Line 1455 of yacc.c  */
#line 242 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(2) - (2)]));;}
    break;

  case 90:

/* Line 1455 of yacc.c  */
#line 244 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 91:

/* Line 1455 of yacc.c  */
#line 245 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(2) - (2)]));;}
    break;

  case 92:

/* Line 1455 of yacc.c  */
#line 247 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(1) - (3)]),(yyvsp[(3) - (3)])));;}
    break;

  case 93:

/* Line 1455 of yacc.c  */
#line 248 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 94:

/* Line 1455 of yacc.c  */
#line 250 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 95:

/* Line 1455 of yacc.c  */
#line 251 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(typeList,(yyvsp[(2) - (3)])));;}
    break;

  case 96:

/* Line 1455 of yacc.c  */
#line 253 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)])));;}
    break;

  case 97:

/* Line 1455 of yacc.c  */
#line 254 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 98:

/* Line 1455 of yacc.c  */
#line 255 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {syntaxError("type defn lhs");;}
    break;

  case 99:

/* Line 1455 of yacc.c  */
#line 257 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 100:

/* Line 1455 of yacc.c  */
#line 258 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 101:

/* Line 1455 of yacc.c  */
#line 260 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(sigdecl((yyvsp[(2) - (3)]),singleton((yyvsp[(1) - (3)])),
							     (yyvsp[(3) - (3)])));;}
    break;

  case 102:

/* Line 1455 of yacc.c  */
#line 262 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 103:

/* Line 1455 of yacc.c  */
#line 264 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 104:

/* Line 1455 of yacc.c  */
#line 265 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 105:

/* Line 1455 of yacc.c  */
#line 267 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(ap((yyvsp[(3) - (4)]),bang((yyvsp[(2) - (4)]))),(yyvsp[(4) - (4)])));;}
    break;

  case 106:

/* Line 1455 of yacc.c  */
#line 268 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ap((yyvsp[(2) - (3)]),(yyvsp[(1) - (3)])),(yyvsp[(3) - (3)])));;}
    break;

  case 107:

/* Line 1455 of yacc.c  */
#line 269 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ap((yyvsp[(2) - (3)]),(yyvsp[(1) - (3)])),(yyvsp[(3) - (3)])));;}
    break;

  case 108:

/* Line 1455 of yacc.c  */
#line 270 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 109:

/* Line 1455 of yacc.c  */
#line 271 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 110:

/* Line 1455 of yacc.c  */
#line 272 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {syntaxError("data type definition");;}
    break;

  case 111:

/* Line 1455 of yacc.c  */
#line 274 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap((yyvsp[(1) - (3)]),bang((yyvsp[(3) - (3)]))));;}
    break;

  case 112:

/* Line 1455 of yacc.c  */
#line 275 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap((yyvsp[(1) - (3)]),bang((yyvsp[(3) - (3)]))));;}
    break;

  case 113:

/* Line 1455 of yacc.c  */
#line 276 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)])));;}
    break;

  case 114:

/* Line 1455 of yacc.c  */
#line 278 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(bang((yyvsp[(2) - (2)])));;}
    break;

  case 115:

/* Line 1455 of yacc.c  */
#line 279 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 116:

/* Line 1455 of yacc.c  */
#line 281 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(singleton(ap((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)]))));;}
    break;

  case 117:

/* Line 1455 of yacc.c  */
#line 283 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 118:

/* Line 1455 of yacc.c  */
#line 284 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(singleton((yyvsp[(2) - (2)])));;}
    break;

  case 119:

/* Line 1455 of yacc.c  */
#line 285 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4((yyvsp[(3) - (4)]));;}
    break;

  case 120:

/* Line 1455 of yacc.c  */
#line 287 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 121:

/* Line 1455 of yacc.c  */
#line 288 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(rev((yyvsp[(1) - (1)])));;}
    break;

  case 122:

/* Line 1455 of yacc.c  */
#line 290 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 123:

/* Line 1455 of yacc.c  */
#line 291 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(singleton((yyvsp[(1) - (1)])));;}
    break;

  case 124:

/* Line 1455 of yacc.c  */
#line 296 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {primDefn((yyvsp[(1) - (4)]),(yyvsp[(2) - (4)]),(yyvsp[(4) - (4)])); sp-=4;;}
    break;

  case 125:

/* Line 1455 of yacc.c  */
#line 298 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 126:

/* Line 1455 of yacc.c  */
#line 299 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 127:

/* Line 1455 of yacc.c  */
#line 300 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {syntaxError("primitive defn");;}
    break;

  case 128:

/* Line 1455 of yacc.c  */
#line 302 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(pair((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)])));;}
    break;

  case 129:

/* Line 1455 of yacc.c  */
#line 303 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 130:

/* Line 1455 of yacc.c  */
#line 308 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {classDefn(intOf((yyvsp[(1) - (3)])),(yyvsp[(2) - (3)]),(yyvsp[(3) - (3)])); sp-=3;;}
    break;

  case 131:

/* Line 1455 of yacc.c  */
#line 309 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {instDefn(intOf((yyvsp[(1) - (3)])),(yyvsp[(2) - (3)]),(yyvsp[(3) - (3)]));  sp-=3;;}
    break;

  case 132:

/* Line 1455 of yacc.c  */
#line 310 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {defaultDefn(intOf((yyvsp[(1) - (4)])),(yyvsp[(3) - (4)]));  sp-=4;;}
    break;

  case 133:

/* Line 1455 of yacc.c  */
#line 312 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(pair((yyvsp[(1) - (3)]),checkClass((yyvsp[(3) - (3)]))));;}
    break;

  case 134:

/* Line 1455 of yacc.c  */
#line 313 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(pair(NIL,checkClass((yyvsp[(1) - (1)]))));;}
    break;

  case 135:

/* Line 1455 of yacc.c  */
#line 315 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(pair((yyvsp[(1) - (3)]),checkInst((yyvsp[(3) - (3)]))));;}
    break;

  case 136:

/* Line 1455 of yacc.c  */
#line 316 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(pair(NIL,checkInst((yyvsp[(1) - (1)]))));;}
    break;

  case 137:

/* Line 1455 of yacc.c  */
#line 318 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 138:

/* Line 1455 of yacc.c  */
#line 319 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(rev((yyvsp[(1) - (1)])));;}
    break;

  case 139:

/* Line 1455 of yacc.c  */
#line 321 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 140:

/* Line 1455 of yacc.c  */
#line 322 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 141:

/* Line 1455 of yacc.c  */
#line 327 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(QUAL,pair((yyvsp[(1) - (3)]),(yyvsp[(3) - (3)]))));;}
    break;

  case 142:

/* Line 1455 of yacc.c  */
#line 328 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 143:

/* Line 1455 of yacc.c  */
#line 330 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(NIL);;}
    break;

  case 144:

/* Line 1455 of yacc.c  */
#line 331 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(singleton(checkClass((yyvsp[(1) - (1)]))));;}
    break;

  case 145:

/* Line 1455 of yacc.c  */
#line 332 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(singleton(checkClass((yyvsp[(2) - (3)]))));;}
    break;

  case 146:

/* Line 1455 of yacc.c  */
#line 333 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(checkContext((yyvsp[(2) - (3)])));;}
    break;

  case 147:

/* Line 1455 of yacc.c  */
#line 335 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 148:

/* Line 1455 of yacc.c  */
#line 336 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 149:

/* Line 1455 of yacc.c  */
#line 338 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 150:

/* Line 1455 of yacc.c  */
#line 339 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ap(typeArrow,(yyvsp[(1) - (3)])),(yyvsp[(3) - (3)])));;}
    break;

  case 151:

/* Line 1455 of yacc.c  */
#line 340 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ap(typeArrow,(yyvsp[(1) - (3)])),(yyvsp[(3) - (3)])));;}
    break;

  case 152:

/* Line 1455 of yacc.c  */
#line 341 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {syntaxError("type expression");;}
    break;

  case 153:

/* Line 1455 of yacc.c  */
#line 343 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 154:

/* Line 1455 of yacc.c  */
#line 344 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 155:

/* Line 1455 of yacc.c  */
#line 346 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)])));;}
    break;

  case 156:

/* Line 1455 of yacc.c  */
#line 347 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 157:

/* Line 1455 of yacc.c  */
#line 349 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)])));;}
    break;

  case 158:

/* Line 1455 of yacc.c  */
#line 350 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 159:

/* Line 1455 of yacc.c  */
#line 352 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 160:

/* Line 1455 of yacc.c  */
#line 353 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 161:

/* Line 1455 of yacc.c  */
#line 355 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 162:

/* Line 1455 of yacc.c  */
#line 356 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(typeUnit);;}
    break;

  case 163:

/* Line 1455 of yacc.c  */
#line 357 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(typeArrow);;}
    break;

  case 164:

/* Line 1455 of yacc.c  */
#line 358 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 165:

/* Line 1455 of yacc.c  */
#line 359 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 166:

/* Line 1455 of yacc.c  */
#line 360 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 167:

/* Line 1455 of yacc.c  */
#line 361 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(buildTuple((yyvsp[(2) - (3)])));;}
    break;

  case 168:

/* Line 1455 of yacc.c  */
#line 362 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(buildTuple((yyvsp[(2) - (3)])));;}
    break;

  case 169:

/* Line 1455 of yacc.c  */
#line 363 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(typeList,(yyvsp[(2) - (3)])));;}
    break;

  case 170:

/* Line 1455 of yacc.c  */
#line 364 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(typeList);;}
    break;

  case 171:

/* Line 1455 of yacc.c  */
#line 365 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(ap(WILDTYPE,NIL));;}
    break;

  case 172:

/* Line 1455 of yacc.c  */
#line 367 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(mkTuple(tupleOf((yyvsp[(1) - (2)]))+1));;}
    break;

  case 173:

/* Line 1455 of yacc.c  */
#line 368 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(mkTuple(2));;}
    break;

  case 174:

/* Line 1455 of yacc.c  */
#line 370 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 175:

/* Line 1455 of yacc.c  */
#line 371 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),cons((yyvsp[(1) - (3)]),NIL)));;}
    break;

  case 176:

/* Line 1455 of yacc.c  */
#line 373 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),cons((yyvsp[(1) - (3)]),NIL)));;}
    break;

  case 177:

/* Line 1455 of yacc.c  */
#line 374 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),cons((yyvsp[(1) - (3)]),NIL)));;}
    break;

  case 178:

/* Line 1455 of yacc.c  */
#line 375 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 179:

/* Line 1455 of yacc.c  */
#line 376 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 180:

/* Line 1455 of yacc.c  */
#line 381 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 181:

/* Line 1455 of yacc.c  */
#line 383 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 182:

/* Line 1455 of yacc.c  */
#line 384 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(NIL);;}
    break;

  case 183:

/* Line 1455 of yacc.c  */
#line 385 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 184:

/* Line 1455 of yacc.c  */
#line 386 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(1) - (2)]));;}
    break;

  case 185:

/* Line 1455 of yacc.c  */
#line 388 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 186:

/* Line 1455 of yacc.c  */
#line 389 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 187:

/* Line 1455 of yacc.c  */
#line 391 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(sigdecl((yyvsp[(2) - (3)]),(yyvsp[(1) - (3)]),(yyvsp[(3) - (3)])));;}
    break;

  case 188:

/* Line 1455 of yacc.c  */
#line 392 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(pair((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)])));;}
    break;

  case 189:

/* Line 1455 of yacc.c  */
#line 394 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(letrec((yyvsp[(2) - (2)]),(yyvsp[(1) - (2)])));;}
    break;

  case 190:

/* Line 1455 of yacc.c  */
#line 395 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {syntaxError("declaration");;}
    break;

  case 191:

/* Line 1455 of yacc.c  */
#line 397 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(pair((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)])));;}
    break;

  case 192:

/* Line 1455 of yacc.c  */
#line 398 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(grded(rev((yyvsp[(1) - (1)]))));;}
    break;

  case 193:

/* Line 1455 of yacc.c  */
#line 400 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(2) - (2)]));;}
    break;

  case 194:

/* Line 1455 of yacc.c  */
#line 401 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 195:

/* Line 1455 of yacc.c  */
#line 403 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(cons((yyvsp[(2) - (2)]),(yyvsp[(1) - (2)])));;}
    break;

  case 196:

/* Line 1455 of yacc.c  */
#line 404 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 197:

/* Line 1455 of yacc.c  */
#line 406 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(pair((yyvsp[(3) - (4)]),pair((yyvsp[(2) - (4)]),(yyvsp[(4) - (4)]))));;}
    break;

  case 198:

/* Line 1455 of yacc.c  */
#line 408 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 199:

/* Line 1455 of yacc.c  */
#line 409 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 200:

/* Line 1455 of yacc.c  */
#line 411 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 201:

/* Line 1455 of yacc.c  */
#line 412 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(varMinus);;}
    break;

  case 202:

/* Line 1455 of yacc.c  */
#line 414 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 203:

/* Line 1455 of yacc.c  */
#line 415 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 204:

/* Line 1455 of yacc.c  */
#line 416 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(varBang);;}
    break;

  case 205:

/* Line 1455 of yacc.c  */
#line 417 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(varLT);;}
    break;

  case 206:

/* Line 1455 of yacc.c  */
#line 418 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(varGT);;}
    break;

  case 207:

/* Line 1455 of yacc.c  */
#line 420 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 208:

/* Line 1455 of yacc.c  */
#line 421 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(varHiding);;}
    break;

  case 209:

/* Line 1455 of yacc.c  */
#line 422 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(varQualified);;}
    break;

  case 210:

/* Line 1455 of yacc.c  */
#line 423 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(varAsMod);;}
    break;

  case 211:

/* Line 1455 of yacc.c  */
#line 425 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 212:

/* Line 1455 of yacc.c  */
#line 426 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 213:

/* Line 1455 of yacc.c  */
#line 431 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 214:

/* Line 1455 of yacc.c  */
#line 432 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {syntaxError("expression");;}
    break;

  case 215:

/* Line 1455 of yacc.c  */
#line 434 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ESIGN,pair((yyvsp[(1) - (3)]),(yyvsp[(3) - (3)]))));;}
    break;

  case 216:

/* Line 1455 of yacc.c  */
#line 435 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 217:

/* Line 1455 of yacc.c  */
#line 437 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(tidyInfix((yyvsp[(1) - (1)])));;}
    break;

  case 218:

/* Line 1455 of yacc.c  */
#line 438 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 219:

/* Line 1455 of yacc.c  */
#line 440 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(NEG,ap(ap((yyvsp[(2) - (4)]),(yyvsp[(1) - (4)])),(yyvsp[(4) - (4)]))));;}
    break;

  case 220:

/* Line 1455 of yacc.c  */
#line 441 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ap((yyvsp[(2) - (3)]),(yyvsp[(1) - (3)])),(yyvsp[(3) - (3)])));;}
    break;

  case 221:

/* Line 1455 of yacc.c  */
#line 442 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(NEG,only((yyvsp[(2) - (2)]))));;}
    break;

  case 222:

/* Line 1455 of yacc.c  */
#line 443 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ap((yyvsp[(2) - (3)]),only((yyvsp[(1) - (3)]))),(yyvsp[(3) - (3)])));;}
    break;

  case 223:

/* Line 1455 of yacc.c  */
#line 444 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(NEG,
						     ap(ap((yyvsp[(2) - (4)]),only((yyvsp[(1) - (4)]))),(yyvsp[(4) - (4)]))));;}
    break;

  case 224:

/* Line 1455 of yacc.c  */
#line 447 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc6(ap(COND,triple((yyvsp[(2) - (6)]),(yyvsp[(4) - (6)]),(yyvsp[(6) - (6)]))));;}
    break;

  case 225:

/* Line 1455 of yacc.c  */
#line 448 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc6(ap(CASE,pair((yyvsp[(2) - (6)]),rev((yyvsp[(5) - (6)])))));;}
    break;

  case 226:

/* Line 1455 of yacc.c  */
#line 449 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 227:

/* Line 1455 of yacc.c  */
#line 451 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(letrec((yyvsp[(2) - (4)]),(yyvsp[(4) - (4)])));;}
    break;

  case 228:

/* Line 1455 of yacc.c  */
#line 452 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(LAMBDA,
						     pair(rev((yyvsp[(2) - (4)])),
							  pair((yyvsp[(3) - (4)]),(yyvsp[(4) - (4)])))));;}
    break;

  case 229:

/* Line 1455 of yacc.c  */
#line 455 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(2) - (2)]));;}
    break;

  case 230:

/* Line 1455 of yacc.c  */
#line 456 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(ACTEXP,(yyvsp[(2) - (2)])));;}
    break;

  case 231:

/* Line 1455 of yacc.c  */
#line 457 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(REQEXP,(yyvsp[(2) - (2)])));;}
    break;

  case 232:

/* Line 1455 of yacc.c  */
#line 459 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc5(ap(TEMPLEXP,pair(pair((yyvsp[(2) - (5)]),NIL),pair((yyvsp[(4) - (5)]),(yyvsp[(5) - (5)])))));;}
    break;

  case 233:

/* Line 1455 of yacc.c  */
#line 460 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(STRUCTVAL,pair((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)]))));;}
    break;

  case 234:

/* Line 1455 of yacc.c  */
#line 461 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 235:

/* Line 1455 of yacc.c  */
#line 463 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc5(cons((yyvsp[(5) - (5)]),(yyvsp[(2) - (5)])));;}
    break;

  case 236:

/* Line 1455 of yacc.c  */
#line 464 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 237:

/* Line 1455 of yacc.c  */
#line 466 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(cons((yyvsp[(2) - (2)]),(yyvsp[(1) - (2)])));;}
    break;

  case 238:

/* Line 1455 of yacc.c  */
#line 467 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 239:

/* Line 1455 of yacc.c  */
#line 469 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)])));;}
    break;

  case 240:

/* Line 1455 of yacc.c  */
#line 470 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(RUNST,(yyvsp[(2) - (2)])));;}
    break;

  case 241:

/* Line 1455 of yacc.c  */
#line 471 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 242:

/* Line 1455 of yacc.c  */
#line 473 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 243:

/* Line 1455 of yacc.c  */
#line 474 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ASPAT,pair((yyvsp[(1) - (3)]),(yyvsp[(3) - (3)]))));;}
    break;

  case 244:

/* Line 1455 of yacc.c  */
#line 475 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(LAZYPAT,(yyvsp[(2) - (2)])));;}
    break;

  case 245:

/* Line 1455 of yacc.c  */
#line 476 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(WILDCARD);;}
    break;

  case 246:

/* Line 1455 of yacc.c  */
#line 477 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 247:

/* Line 1455 of yacc.c  */
#line 478 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(nameUnit);;}
    break;

  case 248:

/* Line 1455 of yacc.c  */
#line 479 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 249:

/* Line 1455 of yacc.c  */
#line 480 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 250:

/* Line 1455 of yacc.c  */
#line 481 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 251:

/* Line 1455 of yacc.c  */
#line 482 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 252:

/* Line 1455 of yacc.c  */
#line 483 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 253:

/* Line 1455 of yacc.c  */
#line 484 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(buildTuple((yyvsp[(2) - (3)])));;}
    break;

  case 254:

/* Line 1455 of yacc.c  */
#line 485 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(STRUCTVAL,pair((yyvsp[(1) - (3)]),(yyvsp[(2) - (3)]))));;}
    break;

  case 255:

/* Line 1455 of yacc.c  */
#line 486 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(SELECTION,pair((yyvsp[(1) - (3)]),(yyvsp[(3) - (3)]))));;}
    break;

  case 256:

/* Line 1455 of yacc.c  */
#line 487 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(SELECTION,(yyvsp[(3) - (4)])));;}
    break;

  case 257:

/* Line 1455 of yacc.c  */
#line 488 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 258:

/* Line 1455 of yacc.c  */
#line 489 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap((yyvsp[(3) - (4)]),(yyvsp[(2) - (4)])));;}
    break;

  case 259:

/* Line 1455 of yacc.c  */
#line 490 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(ap(nameFlip,(yyvsp[(2) - (4)])),(yyvsp[(3) - (4)])));;}
    break;

  case 260:

/* Line 1455 of yacc.c  */
#line 491 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(ap(nameFlip,(yyvsp[(2) - (4)])),(yyvsp[(3) - (4)])));;}
    break;

  case 261:

/* Line 1455 of yacc.c  */
#line 492 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 262:

/* Line 1455 of yacc.c  */
#line 494 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 263:

/* Line 1455 of yacc.c  */
#line 495 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),cons((yyvsp[(1) - (3)]),NIL)));;}
    break;

  case 264:

/* Line 1455 of yacc.c  */
#line 497 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 265:

/* Line 1455 of yacc.c  */
#line 498 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(1) - (2)]));;}
    break;

  case 266:

/* Line 1455 of yacc.c  */
#line 500 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 267:

/* Line 1455 of yacc.c  */
#line 501 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 268:

/* Line 1455 of yacc.c  */
#line 503 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(pair((yyvsp[(1) - (3)]),letrec((yyvsp[(3) - (3)]),(yyvsp[(2) - (3)]))));;}
    break;

  case 269:

/* Line 1455 of yacc.c  */
#line 505 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(grded(rev((yyvsp[(1) - (1)]))));;}
    break;

  case 270:

/* Line 1455 of yacc.c  */
#line 506 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(pair((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)])));;}
    break;

  case 271:

/* Line 1455 of yacc.c  */
#line 507 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {syntaxError("case expression");;}
    break;

  case 272:

/* Line 1455 of yacc.c  */
#line 509 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(cons((yyvsp[(2) - (2)]),(yyvsp[(1) - (2)])));;}
    break;

  case 273:

/* Line 1455 of yacc.c  */
#line 510 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 274:

/* Line 1455 of yacc.c  */
#line 512 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(pair((yyvsp[(3) - (4)]),pair((yyvsp[(2) - (4)]),(yyvsp[(4) - (4)]))));;}
    break;

  case 275:

/* Line 1455 of yacc.c  */
#line 514 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1((yyvsp[(1) - (1)]));;}
    break;

  case 276:

/* Line 1455 of yacc.c  */
#line 515 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(HNDLEXP,pair((yyvsp[(2) - (2)]),(yyvsp[(1) - (2)]))));;}
    break;

  case 277:

/* Line 1455 of yacc.c  */
#line 517 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(DOCOMP,checkDo((yyvsp[(2) - (3)]))));;}
    break;

  case 278:

/* Line 1455 of yacc.c  */
#line 518 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(DOCOMP,checkDo((yyvsp[(2) - (4)]))));;}
    break;

  case 279:

/* Line 1455 of yacc.c  */
#line 520 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 280:

/* Line 1455 of yacc.c  */
#line 521 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 281:

/* Line 1455 of yacc.c  */
#line 523 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(FROMQUAL,pair((yyvsp[(1) - (3)]),(yyvsp[(3) - (3)]))));;}
    break;

  case 282:

/* Line 1455 of yacc.c  */
#line 524 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(QWHERE,(yyvsp[(2) - (2)])));;}
    break;

  case 283:

/* Line 1455 of yacc.c  */
#line 525 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc6(ap(DOQUAL,ap(COND,triple((yyvsp[(2) - (6)]),(yyvsp[(4) - (6)]),(yyvsp[(6) - (6)])))));;}
    break;

  case 284:

/* Line 1455 of yacc.c  */
#line 526 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(IFHACK,pair((yyvsp[(2) - (4)]),(yyvsp[(4) - (4)]))));;}
    break;

  case 285:

/* Line 1455 of yacc.c  */
#line 527 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(ELSIFHACK,pair((yyvsp[(2) - (4)]),(yyvsp[(4) - (4)]))));;}
    break;

  case 286:

/* Line 1455 of yacc.c  */
#line 528 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(ELSEHACK,(yyvsp[(2) - (2)])));;}
    break;

  case 287:

/* Line 1455 of yacc.c  */
#line 529 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc6(ap(DOQUAL,ap(CASE,pair((yyvsp[(2) - (6)]),rev((yyvsp[(5) - (6)]))))));;}
    break;

  case 288:

/* Line 1455 of yacc.c  */
#line 530 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc6(ap(FORALLDO,triple((yyvsp[(2) - (6)]),(yyvsp[(4) - (6)]),(yyvsp[(6) - (6)]))));;}
    break;

  case 289:

/* Line 1455 of yacc.c  */
#line 531 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(WHILEDO,pair((yyvsp[(2) - (4)]),(yyvsp[(4) - (4)]))));;}
    break;

  case 290:

/* Line 1455 of yacc.c  */
#line 532 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(FIXDO,(yyvsp[(2) - (2)])));;}
    break;

  case 291:

/* Line 1455 of yacc.c  */
#line 533 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(ap(ASSIGNQ,(yyvsp[(1) - (1)])));;}
    break;

  case 292:

/* Line 1455 of yacc.c  */
#line 534 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(ap(DOQUAL,(yyvsp[(1) - (1)])));;}
    break;

  case 293:

/* Line 1455 of yacc.c  */
#line 536 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(pair((yyvsp[(1) - (3)]),pair((yyvsp[(2) - (3)]),(yyvsp[(3) - (3)]))));;}
    break;

  case 294:

/* Line 1455 of yacc.c  */
#line 538 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 295:

/* Line 1455 of yacc.c  */
#line 540 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 296:

/* Line 1455 of yacc.c  */
#line 541 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(NIL);;}
    break;

  case 297:

/* Line 1455 of yacc.c  */
#line 542 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 298:

/* Line 1455 of yacc.c  */
#line 543 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(1) - (2)]));;}
    break;

  case 299:

/* Line 1455 of yacc.c  */
#line 545 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 300:

/* Line 1455 of yacc.c  */
#line 546 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 301:

/* Line 1455 of yacc.c  */
#line 548 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 302:

/* Line 1455 of yacc.c  */
#line 550 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(FROMQUAL,pair((yyvsp[(1) - (3)]),(yyvsp[(3) - (3)]))));;}
    break;

  case 303:

/* Line 1455 of yacc.c  */
#line 552 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3((yyvsp[(2) - (3)]));;}
    break;

  case 304:

/* Line 1455 of yacc.c  */
#line 554 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 305:

/* Line 1455 of yacc.c  */
#line 555 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(NIL);;}
    break;

  case 306:

/* Line 1455 of yacc.c  */
#line 556 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 307:

/* Line 1455 of yacc.c  */
#line 557 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(1) - (2)]));;}
    break;

  case 308:

/* Line 1455 of yacc.c  */
#line 559 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 309:

/* Line 1455 of yacc.c  */
#line 560 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 310:

/* Line 1455 of yacc.c  */
#line 562 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(pair((yyvsp[(1) - (3)]),(yyvsp[(3) - (3)])));;}
    break;

  case 311:

/* Line 1455 of yacc.c  */
#line 564 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(rev((yyvsp[(3) - (4)])));;}
    break;

  case 312:

/* Line 1455 of yacc.c  */
#line 566 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 313:

/* Line 1455 of yacc.c  */
#line 567 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(NIL);;}
    break;

  case 314:

/* Line 1455 of yacc.c  */
#line 568 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ESIGN,pair((yyvsp[(1) - (3)]),(yyvsp[(3) - (3)]))));;}
    break;

  case 315:

/* Line 1455 of yacc.c  */
#line 569 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 316:

/* Line 1455 of yacc.c  */
#line 571 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(tidyInfix((yyvsp[(1) - (1)])));;}
    break;

  case 317:

/* Line 1455 of yacc.c  */
#line 572 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 318:

/* Line 1455 of yacc.c  */
#line 574 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(NEG,ap(ap((yyvsp[(2) - (4)]),(yyvsp[(1) - (4)])),(yyvsp[(4) - (4)]))));;}
    break;

  case 319:

/* Line 1455 of yacc.c  */
#line 575 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ap((yyvsp[(2) - (3)]),(yyvsp[(1) - (3)])),(yyvsp[(3) - (3)])));;}
    break;

  case 320:

/* Line 1455 of yacc.c  */
#line 576 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(NEG,only((yyvsp[(2) - (2)]))));;}
    break;

  case 321:

/* Line 1455 of yacc.c  */
#line 577 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ap((yyvsp[(2) - (3)]),only((yyvsp[(1) - (3)]))),(yyvsp[(3) - (3)])));;}
    break;

  case 322:

/* Line 1455 of yacc.c  */
#line 578 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(NEG,
						     ap(ap((yyvsp[(2) - (4)]),only((yyvsp[(1) - (4)]))),(yyvsp[(4) - (4)]))));;}
    break;

  case 323:

/* Line 1455 of yacc.c  */
#line 581 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 324:

/* Line 1455 of yacc.c  */
#line 582 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2((yyvsp[(1) - (2)]));;}
    break;

  case 325:

/* Line 1455 of yacc.c  */
#line 584 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 326:

/* Line 1455 of yacc.c  */
#line 585 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 327:

/* Line 1455 of yacc.c  */
#line 587 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(pair((yyvsp[(1) - (3)]),letrec((yyvsp[(3) - (3)]),(yyvsp[(2) - (3)]))));;}
    break;

  case 328:

/* Line 1455 of yacc.c  */
#line 589 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(grded(rev((yyvsp[(1) - (1)]))));;}
    break;

  case 329:

/* Line 1455 of yacc.c  */
#line 590 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(pair((yyvsp[(1) - (2)]),(yyvsp[(2) - (2)])));;}
    break;

  case 330:

/* Line 1455 of yacc.c  */
#line 591 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {syntaxError("case command");;}
    break;

  case 331:

/* Line 1455 of yacc.c  */
#line 593 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(cons((yyvsp[(2) - (2)]),(yyvsp[(1) - (2)])));;}
    break;

  case 332:

/* Line 1455 of yacc.c  */
#line 594 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 333:

/* Line 1455 of yacc.c  */
#line 596 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(pair((yyvsp[(3) - (4)]),pair((yyvsp[(2) - (4)]),(yyvsp[(4) - (4)]))));;}
    break;

  case 334:

/* Line 1455 of yacc.c  */
#line 601 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc0(nameNil);;}
    break;

  case 335:

/* Line 1455 of yacc.c  */
#line 602 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(ap(FINLIST,cons((yyvsp[(1) - (1)]),NIL)));;}
    break;

  case 336:

/* Line 1455 of yacc.c  */
#line 603 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(ap(FINLIST,rev((yyvsp[(1) - (1)]))));;}
    break;

  case 337:

/* Line 1455 of yacc.c  */
#line 604 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(COMP,pair((yyvsp[(1) - (3)]),rev((yyvsp[(3) - (3)])))));;}
    break;

  case 338:

/* Line 1455 of yacc.c  */
#line 605 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(ap(nameFromTo,(yyvsp[(1) - (3)])),(yyvsp[(3) - (3)])));;}
    break;

  case 339:

/* Line 1455 of yacc.c  */
#line 606 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc4(ap(ap(nameFromThen,(yyvsp[(1) - (4)])),(yyvsp[(3) - (4)])));;}
    break;

  case 340:

/* Line 1455 of yacc.c  */
#line 607 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(nameFrom,(yyvsp[(1) - (2)])));;}
    break;

  case 341:

/* Line 1455 of yacc.c  */
#line 608 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc5(ap(ap(ap(nameFromThenTo,
							       (yyvsp[(1) - (5)])),(yyvsp[(3) - (5)])),(yyvsp[(5) - (5)])));;}
    break;

  case 342:

/* Line 1455 of yacc.c  */
#line 611 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(cons((yyvsp[(3) - (3)]),(yyvsp[(1) - (3)])));;}
    break;

  case 343:

/* Line 1455 of yacc.c  */
#line 612 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(cons((yyvsp[(1) - (1)]),NIL));;}
    break;

  case 344:

/* Line 1455 of yacc.c  */
#line 614 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc3(ap(FROMQUAL,pair((yyvsp[(1) - (3)]),(yyvsp[(3) - (3)]))));;}
    break;

  case 345:

/* Line 1455 of yacc.c  */
#line 615 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc1(ap(BOOLQUAL,(yyvsp[(1) - (1)])));;}
    break;

  case 346:

/* Line 1455 of yacc.c  */
#line 616 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = gc2(ap(QWHERE,(yyvsp[(2) - (2)])));;}
    break;

  case 347:

/* Line 1455 of yacc.c  */
#line 621 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 348:

/* Line 1455 of yacc.c  */
#line 622 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {yyerrok; goOffside(startColumn);;}
    break;

  case 349:

/* Line 1455 of yacc.c  */
#line 625 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (1)]);;}
    break;

  case 350:

/* Line 1455 of yacc.c  */
#line 626 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {yyerrok;
					 if (canUnOffside()) {
					     unOffside();
					     /* insert extra token on stack*/
					     push(NIL);
					     pushed(0) = pushed(1);
					     pushed(1) = mkInt(column);
					 }
					 else
					     syntaxError("definition");
					;}
    break;

  case 351:

/* Line 1455 of yacc.c  */
#line 638 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {useLayout();;}
    break;

  case 352:

/* Line 1455 of yacc.c  */
#line 638 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"
    {(yyval) = (yyvsp[(1) - (2)]);;}
    break;



/* Line 1455 of yacc.c  */
#line 4836 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 643 "C:\\Users\\nebel\\Documents\\visual studio 2012\\Projects\\OHugs\\ohugs\\parser.y"


/* keep parsed fragments on stack  */
static Cell local gcShadow(Int  n, Cell e)
{
    /* If a look ahead token is held then the required stack transformation
     * is:
     *   pushed: n               1     0          1     0
     *           x1  |  ...  |  xn  |  la   ===>  e  |  la
     *                                top()            top()
     *
     * Othwerwise, the transformation is:
     *   pushed: n-1             0        0
     *           x1  |  ...  |  xn  ===>  e
     *                         top()     top()
     */
	if (yychar>=0) {
		pushed(n-1) = top();
		pushed(n)   = e;
	}
	else
		pushed(n-1) = e;
	sp -= (n-1);
	return e;
}

/* report on syntax error           */
static Void local syntaxError(String s)
{
    ERRMSG(row) "Syntax error in %s (unexpected %s)", s, unexpected()
    EEND;
}

static String local unexpected() {      /* find name for unexpected token  */
	static char buffer[100];
	static char *fmt = "%s \"%s\"";
	static char *kwd = "keyword";

	switch (yychar) {
	case 0         : return "end of input";

#define keyword(kw) sprintf(buffer,fmt,kwd,kw); return buffer;
	case INFIXL    : keyword("infixl");
	case INFIXR    : keyword("infixr");
	case INFIX     : keyword("infix");
	case TINSTANCE : keyword("instance");
	case TCLASS    : keyword("class");
	case PRIMITIVE : keyword("primitive");
	case CASEXP    : keyword("case");
	case OF        : keyword("of");
	case IF        : keyword("if");
	case TRUNST    : keyword("runST");
	case THEN      : keyword("then");
	case ELSE      : keyword("else");
	case WHERE     : keyword("where");
	case TYPE      : keyword("type");
	case DATA      : keyword("data");
	case TNEWTYPE  : keyword("newtype");
	case LET       : keyword("let");
	case IN        : keyword("in");
	case DERIVING  : keyword("deriving");
	case DEFAULT   : keyword("default");
	case IMPORT    : keyword("import");
	case MODULE    : keyword("module");
	case STRUCT    : keyword("struct");
	case TEMPLATE  : keyword("template");
	case ACTION    : keyword("action");
	case REQUEST   : keyword("request");
	case HANDLE    : keyword("handle");
	case FORALL    : keyword("forall");
	case WHILE     : keyword("while");
	case ELSIF     : keyword("elsif");
#undef keyword

	case ASSIGN    : return "`:='";
	case '<'       : return "`<'";
	case '>'       : return "`>'";
	case ARROW     : return "`->'";
	case '='       : return "`='";
	case COCO      : return "`::'";
	case '-'       : return "`-'";
	case '!'       : return "`!'";
	case ','       : return "comma";
	case '@'       : return "`@'";
	case '('       : return "`('";
	case ')'       : return "`)'";
	case '{'       : return "`{'";
	case '}'       : return "`}'";
	case '|'       : return "`|'";
	case ';'       : return "`;'";
	case UPTO      : return "`..'";
	case '['       : return "`['";
	case ']'       : return "`]'";
	case '_'       : return "`_'";
	case FROM      : return "`<-'";
	case '\\'      : return "backslash (lambda)";
	case '~'       : return "tilde";
	case '`'       : return "backquote";
	case SELDOT    : return "`.'";
	case VAROP     :
	case VARID     :
	case CONOP     :
	case CONID     : sprintf(buffer,"symbol \"%s\"",
						 textToStr(textOf(yylval)));
		return buffer;
	case HIDING    : return "symbol \"hiding\"";
	case QUALIFIED : return "symbol \"qualified\"";
	case ASMOD     : return "symbol \"as\"";
	case NUMLIT    : return "numeric literal";
	case CHARLIT   : return "character literal";
	case STRINGLIT : return "string literal";
	case IMPLIES   : return "`=>'";
	default        : return "token";
	}
}

/* Check for valid precedence value */
static Cell local checkPrec(Cell p)
{
	if (!isInt(p) || intOf(p)<MIN_PREC || intOf(p)>MAX_PREC) {
		ERRMSG(row) "Precedence value must be an integer in the range [%d..%d]",
			MIN_PREC, MAX_PREC
			EEND;
	}
	return p;
}

/* Declare syntax of operators      */
static Void local fixDefn(Syntax a, Cell   line, Cell   p, List   ops)
{
    Int l = intOf(line);
    a     = mkSyntax(a,intOf(p));
    map2Proc(setSyntax,l,a,ops);
}

/* set syntax of individ. operator  */
static Void local setSyntax(Int    line, Syntax sy, Cell   op)
{
    addSyntax(line,textOf(op),sy);
    opDefns = cons(op,opDefns);
}

/* build tuple (x1,...,xn) from list*/
/* [xn,...,x1]                      */
static Cell local buildTuple(List tup)
{
	Int  n = 0;
	Cell t = tup;
	Cell x;

	/*     .                    .       */
	/*    / \                  / \      */
	/*   xn  .                .   xn    */
	/*        .    ===>      .          */
	/*         .            .           */
	/*          .          .            */
	/*         / \        / \           */
	/*        x1  NIL   (n)  x1         */
	do {
		x      = fst(t);
		fst(t) = snd(t);
		snd(t) = x;
		x      = t;
		t      = fun(x);
		n++;
	} while (nonNull(t));
	fst(x) = mkTuple(n);
	return tup;
}

/* validate type class context     */
static List local checkContext(Type con)
{
    mapOver(checkClass,con);
    return con;
}

/* check that type expr is a class */
/* constrnt of the form Class var  */
static Cell local checkClass(Cell c)
{
	Cell cn = getHead(c);

	if (!isCon(cn))
		syntaxError("class expression");
	else if (argCount!=1) {
		ERRMSG(row) "Class \"%s\" must have exactly one argument",
			textToStr(textOf(cn))
			EEND;
	}
	else if (whatIs(arg(c))!=VARIDCELL) {
		ERRMSG(row) "Argument of class \"%s\" must be a variable",
			/* Ha!  What do you think this is?  Gofer!? :-) */
			textToStr(textOf(cn))
			EEND;
	}
	return c;
}

/* check that type expr is a class */
/* constr of the form Class simple */
static Cell local checkInst(Cell c)
{
	Cell cn = getHead(c);

	if (!isCon(cn))
		syntaxError("class expression");
	else if (argCount!=1) {
		ERRMSG(row) "Class \"%s\" must have exactly one argument",
			textToStr(textOf(cn))
			EEND;
	}
	else {
		Cell a  = arg(c);
		Cell tn = getHead(a);
		if (isCon(tn) || isTycon(tn) || isTuple(tn)) {
			for (; isAp(a); a=fun(a))
				if (whatIs(arg(a))!=VARIDCELL) {
					ERRMSG(row) "Type variable expected in instance type"
						EEND;
				}
		}
		else {
			ERRMSG(row) "Illegal type expression in instance declaration"
				EEND;
		}
	}
	return c;
}

/* convert reversed list of dquals */
/* to an (expr,quals) pair         */
static Pair local checkDo(List dqs)
{
	static String ElseErr = "Badly formed \"if/elsif/else\" sequence";
	static String LastErr = "Last statement in a sequence must be an expression or an assignment";
	List qs = dqs, revqs = NIL;
	Cell exp = NIL;
	while (nonNull(qs)) {
		Cell ifexp = nameDone;

		if (whatIs(hd(qs))==ELSEHACK) {
			ifexp = snd(hd(qs));
			qs = tl(qs);
		}
		while (nonNull(qs) && whatIs(hd(qs))==ELSIFHACK) {
			ifexp = ap(COND,triple(fst(snd(hd(qs))),snd(snd(hd(qs))),ifexp));
			qs = tl(qs);
		}
		if (nonNull(qs) && whatIs(hd(qs))==IFHACK) {
			ifexp = ap(COND,triple(fst(snd(hd(qs))),snd(snd(hd(qs))),ifexp));
			hd(qs) = ap(DOQUAL,ifexp);
			ifexp = nameDone;
		}
		if (isNull(qs) || ifexp != nameDone) {
			ERRMSG(row) ElseErr EEND;
		}
		switch (whatIs(hd(qs))) {
		case IFHACK    : 
		case ELSEHACK  : 
		case ELSIFHACK : ERRMSG(row) ElseErr EEND;
			break;
		case FORALLDO  :
		case WHILEDO   :
		case ASSIGNQ   : if (exp==NIL)
							 exp = nameDone;
			break;
		case DOQUAL    : if (exp==NIL) {
			exp = snd(hd(qs));
			qs = tl(qs);
			continue;
						 }
						 break;
		default        : if (exp==NIL) {
			ERRMSG(row) LastErr EEND;
						 }
						 break;
		}
		revqs = cons(hd(qs),revqs);
		qs = tl(qs);
	}
	fst(dqs) = exp; 		         /* put expression in fst of pair   */
	snd(dqs) = revqs;            	 /* & reversed list of quals in snd */
	return dqs;
}

/*
if a then
   xxx
elsif b then
   yyy
elsif c then
   zzz
else
   www

==>
   
if a then xxx
     else if b then yyy
               else if c then zzz
                         else www
*/
                         
/* check that lhs is of the form   */
/* T a1 ... a                      */
static Cell local checkTyLhs(Cell c)
{
	Cell tlhs = c;
	while (isAp(tlhs) && whatIs(arg(tlhs))==VARIDCELL)
		tlhs = fun(tlhs);
	if (whatIs(tlhs)!=CONIDCELL) {
		ERRMSG(row) "Illegal left hand side in type definition"
			EEND;
	}
	return c;
}

/* Expressions involving infix operators or unary minus are parsed as elements
 * of the following type:
 *
 *     data OpExp = Only Exp | Neg OpExp | Infix OpExp Op Exp
 *
 * (The algorithms here do not assume that negation can be applied only once,
 * i.e., that - - x is a syntax error, as required by the Haskell report.
 * Instead, that restriction is captured by the grammar itself, given above.)
 *
 * There are rules of precedence and grouping, expressed by two functions:
 *
 *     prec :: Op -> Int;   assoc :: Op -> Assoc    (Assoc = {L, N, R})
 *
 * OpExp values are rearranged accordingly when a complete expression has
 * been read using a simple shift-reduce parser whose result may be taken
 * to be a value of the following type:
 *
 *     data Exp = Atom Int | Negate Exp | Apply Op Exp Exp | Error String
 *
 * The machine on which this parser is based can be defined as follows:
 *
 *     tidy                         :: OpExp -> [(Op,Exp)] -> Exp
 *     tidy (Only a)      []         = a
 *     tidy (Only a)      ((o,b):ss) = tidy (Only (Apply o a b)) ss
 *     tidy (Infix a o b) []         = tidy a [(o,b)]
 *     tidy (Infix a o b) ((p,c):ss)
 *                      | shift  o p = tidy a ((o,b):(p,c):ss)
 *                      | red    o p = tidy (Infix a o (Apply p b c)) ss
 *                      | ambig  o p = Error "ambiguous use of operators"
 *     tidy (Neg e)       []         = tidy (tidyNeg e) []
 *     tidy (Neg e)       ((o,b):ss)
 *                      | nshift o   = tidy (Neg (underNeg o b e)) ss
 *                      | nred   o   = tidy (tidyNeg e) ((o,b):ss)
 *                      | nambig o   = Error "illegal use of negation"
 *
 * At each stage, the parser can either shift, reduce, accept, or error.
 * The transitions when dealing with juxtaposed operators o and p are
 * determined by the following rules:
 *
 *     shift o p  = (prec o > prec p)
 *               || (prec o == prec p && assoc o == L && assoc p == L)
 *
 *     red o p    = (prec o < prec p)
 *               || (prec o == prec p && assoc o == R && assoc p == R)
 *
 *     ambig o p  = (prec o == prec p)
 *               && (assoc o == N || assoc p == N || assoc o /= assoc p)
 *
 * The transitions when dealing with juxtaposed unary minus and infix operators
 * are as follows.  The precedence of unary minus (infixl 6) is hardwired in
 * to these definitions, as it is to the definitions of the Haskell grammar
 * in the official report.
 *
 *     nshift o   = (prec o > 6)
 *     nred   o   = (prec o < 6) || (prec o == 6 && assoc o == L)
 *     nambig o   = prec o == 6 && (assoc o == R || assoc o == N)
 *
 * An OpExp of the form (Neg e) means negate the last thing in the OpExp e;
 * we can force this negation using:
 *
 *     tidyNeg              :: OpExp -> OpExp
 *     tidyNeg (Only e)      = Only (Negate e)
 *     tidyNeg (Infix a o b) = Infix a o (Negate b)
 *     tidyNeg (Neg e)       = tidyNeg (tidyNeg e)
 * 
 * On the other hand, if we want to sneak application of an infix operator
 * under a negation, then we use:
 *
 *     underNeg                  :: Op -> Exp -> OpExp -> OpExp
 *     underNeg o b (Only e)      = Only (Apply o e b)
 *     underNeg o b (Neg e)       = Neg (underNeg o b e)
 *     underNeg o b (Infix e p f) = Infix e p (Apply o f b)
 *
 * As a concession to efficiency, we lower the number of calls to syntaxOf
 * by keeping track of the values of sye, sys throughout the process.  The
 * value APPLIC is used to indicate that the syntax value is unknown.
 */

/* convert OpExp to Expr	   */
static Cell local tidyInfix(Cell e		/* :: OpExp			   */
							)
{
	Cell s     = NIL;			/* :: [(Op,Exp)]		   */
	Syntax sye = APPLIC;		/* Syntax of op in e (init unknown)*/
	Syntax sys = APPLIC;		/* Syntax of op in s (init unknown)*/

	for (;;) {
		switch (whatIs(e)) {
		case ONLY : e = snd(e);
			while (nonNull(s)) {
				Cell next   = arg(fun(s));
				arg(fun(s)) = e;
				e           = s;
				s           = next;
			}
			return e;

		case NEG  : if (nonNull(s)) {

			if (sys==APPLIC) {	/* calculate sys	   */
				sys = syntaxOf(textOf(fun(fun(s))));
				if (sys==APPLIC) sys=DEF_OPSYNTAX;
			}

			if (precOf(sys)==UMINUS_PREC &&	/* nambig  */
				assocOf(sys)!=UMINUS_ASSOC) {
					ERRMSG(row)
						"Ambiguous use of unary minus with \"%s\"",
						textToStr(textOf(fun(fun(s))))
						EEND;
			}

			if (precOf(sys)>UMINUS_PREC) {	/* nshift  */
				Cell e1    = snd(e);
				Cell t     = s;
				s          = arg(fun(s));
				while (whatIs(e1)==NEG)
					e1 = snd(e1);
				arg(fun(t)) = arg(e1);
				arg(e1)     = t;
				sys         = APPLIC;
				continue;
			}

					}

					/* Intentional fall-thru for nreduce and isNull(s) */

					{   Cell prev = e;		/* e := tidyNeg e  */
					Cell temp = arg(prev);
					Int  nneg = 1;
					for (; whatIs(temp)==NEG; nneg++) {
						fun(prev) = nameNegate;
						prev	  = temp;
						temp	  = arg(prev);
					}
					if (isInt(arg(temp))) {	/* special cases   */
						if (nneg&1)		/* for literals    */
							arg(temp) = mkInt(-intOf(arg(temp)));
					}
#if BIGNUMS
					else if (isBignum(arg(temp))) {
						if (nneg&1)
							arg(temp) = bigNeg(arg(temp));
					}
#endif
					else if (isFloat(arg(temp))) {
						if (nneg&1)
							arg(temp) = mkFloat(-floatOf(arg(temp)));
					}
					else {
						fun(prev) = nameNegate;
						arg(prev) = arg(temp);
						arg(temp) = e;
					}
					e = temp;
					}
					continue;

		default   : if (isNull(s)) {/* Move operation onto empty stack */
			Cell next   = arg(fun(e));
			s           = e;
			arg(fun(s)) = NIL;
			e           = next;
			sys         = sye;
			sye         = APPLIC;
					}
					else {		/* deal with pair of operators	   */

						if (sye==APPLIC) {	/* calculate sys and sye   */
							sye = syntaxOf(textOf(fun(fun(e))));
							if (sye==APPLIC) sye=DEF_OPSYNTAX;
						}
						if (sys==APPLIC) {
							sys = syntaxOf(textOf(fun(fun(s))));
							if (sys==APPLIC) sys=DEF_OPSYNTAX;
						}

						if (precOf(sye)==precOf(sys) &&	/* ambig   */
							(assocOf(sye)!=assocOf(sys) ||
							assocOf(sye)==NON_ASS)) {
								ERRMSG(row)
									"Ambiguous use of operator \"%s\" with \"%s\"",
									textToStr(textOf(fun(fun(e)))),
									textToStr(textOf(fun(fun(s))))
									EEND;
						}

						if (precOf(sye)>precOf(sys) ||	/* shift   */
							(precOf(sye)==precOf(sys) &&
							assocOf(sye)==LEFT_ASS &&
							assocOf(sys)==LEFT_ASS)) {
								Cell next   = arg(fun(e));
								arg(fun(e)) = s;
								s           = e;
								e	    = next;
								sys         = sye;
								sye         = APPLIC;
						}
						else {				/* reduce */
							Cell next   = arg(fun(s));
							arg(fun(s)) = arg(e);
							arg(e)      = s;
							s	    = next;
							sys         = APPLIC;
							/* sye unchanged */
						}
					}
					continue;
		}
	}
}

/*-------------------------------------------------------------------------*/

