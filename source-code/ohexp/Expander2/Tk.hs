module Tk where

-- 26.12.2011

infixr 6 ^^^ 
infixr 7 <<< 

struct Tk =
   window    :: [WindowOpt] -> Request Window
   bitmap    :: [BitmapOpt] -> Request ConfBitmap
   photo     :: [PhotoOpt]  -> Request Photo
   delay     :: Int -> (String -> Cmd ()) -> Request String
   periodic  :: Int -> Cmd ()             -> Request Runnable
   bell      :: Action 
   font      :: String -> Request TkFont
   runTclCmd :: String -> Request String
   runTclCmd_:: String -> Action
    
struct Runnable = 
   start :: Action
   stop  :: Action

struct TkEnv < Tk, StdEnv

primExTcl :: [String] -> Request String
primExTcl  = primExecuteTcl . unwords

primExTcl_ :: [String] -> Action
primExTcl_ = primExecuteTcl_ . unwords

primitive primExecuteTcl "primExecuteTcl"   :: String -> Request String
primitive primExecuteTcl_ "primExecuteTcl_" :: String -> Action
	   
addCallBack :: String -> (String -> Cmd ()) -> Cmd String
addCallBack str a = do n <- primAddCallBack a
                       return $ str ++ " {doEvent " ++ show n ++ "}"

primitive primGetPath "primGetPath"         :: Request String
primitive primAddCallBack "primAddCallBack" :: (String -> Cmd ()) -> Request Int
primitive primNextCallBack "primNextCallBack" :: Request Int

-- Windows

struct BasicWindow a < ConfWidget a =
   button      :: [ButtonOpt]      -> Request Button
   canvas      :: [CanvasOpt]      -> Request Canvas
   checkButton :: [CheckButtonOpt] -> Request CheckButton
   entry       :: [EntryOpt]       -> Request Entry
   frame       :: [StdOpt]         -> Request Frame
   label       :: [LabelOpt]       -> Request Label
   listBox     :: [ListBoxOpt]     -> Request ListBox
   menuButton  :: [MenuButtonOpt]  -> Request MenuButton
   radioButton :: [CheckButtonOpt] -> Request RadioButton
   scrollBar   :: [StdOpt]         -> Request ScrollBar
   slider      :: [SliderOpt]      -> Request Slider
   textEditor  :: [TextEditorOpt]  -> Request TextEditor
 
type Pos = (Int,Int)

struct ManagedWindow = 
   getGeometry :: Request (Pos,Pos)   -- size,position
   setSize     :: Pos -> Action
   setPosition :: Pos -> Action
   iconify     :: Action
   deiconify   :: Action

struct Window < BasicWindow WindowOpt, ManagedWindow 

-- Images and fonts

struct Image = imageName :: String

stop, hourglass, info, questhead, question, warning :: Image
stop      = struct imageName = "error"
hourglass = struct imageName = "hourglass"
info      = struct imageName = "info"
questhead = struct imageName = "questhead"
question  = struct imageName = "question"
warning   = struct imageName = "warning"

struct ConfBitmap < Image, Configurable BitmapOpt

struct Photo < Image, Configurable PhotoOpt =
   blank    :: Action
   putPixel :: Pos -> Color -> Action
   getPixel :: Pos          -> Request Color
   copyFrom :: Photo        -> Action  -- to be refined
   saveAs   :: FilePath     -> Action 
  
struct TkFont = 
   fontName     :: String
   ascent       :: Int
   descent      :: Int
   linespace    :: Int
   fixed        :: Bool
   getTextWidth :: String -> Request Int

-- Widgets

struct Widget = 
   ident               :: String
   destroy             :: Action
   exists              :: Request Bool 
   focus, raise, lower :: Action
   bind                :: [Event] -> Action
    
struct Configurable a = 
   set :: [a] -> Action

struct ConfWidget a < Widget, Configurable a

-- Structures for subtyping by WWidgets

struct Cell a =
   setValue :: a -> Action
   getValue :: Request a

struct LineEditable =
   lines       :: Request Int
   getLine     :: Int -> Request String
   deleteLine  :: Int -> Action
   insertLines :: Int -> [String] -> Action

struct Invokable = 
   invoke :: Action

data Dir = Hor | Ver 
    
data Stretch > None = XFill | YFill | XYFill

struct Packable =
   packIn :: String -> Dir -> Stretch -> Bool -> Cmd ()
   wname  :: String

struct Scannable a =
   mark :: a -> Action
   drag :: a -> Action

struct WWidget a < ConfWidget a, Packable

struct ScrollWidget a < WWidget a = 
   xview :: Request (Double,Double)
   yview :: Request (Double,Double)

-- Window widgets

struct Frame < BasicWindow StdOpt, WWidget StdOpt

struct Slider < WWidget SliderOpt, Cell Int

struct Button < WWidget ButtonOpt, Invokable =
   flash   :: Action

struct CheckButton < Button = 
   toggle   :: Action
   checked  :: Request Bool

struct RadioButton < Button = 
   select   :: Action
   deselect :: Action

struct MenuButton < WWidget MenuButtonOpt =
   menu :: [MenuOpt] -> Request Menu

struct Label < WWidget LabelOpt

struct ListBox < ScrollWidget ListBoxOpt, LineEditable, Cell [Int],  
                 Scannable Pos =
   view :: Int -> Action

struct TextEditor < ScrollWidget TextEditorOpt, LineEditable, Scannable Pos

struct Entry < ScrollWidget EntryOpt, Cell String, Scannable Int =
   cursorPos :: Request Int

struct Canvas < ScrollWidget CanvasOpt, Scannable Pos = 
   oval      :: Pos -> Pos -> [OvalOpt]    -> Request (CWidget OvalOpt)
   arc       :: Pos -> Pos -> [ArcOpt]     -> Request (CWidget ArcOpt)
   rectangle :: Pos -> Pos -> [OvalOpt]    -> Request (CWidget OvalOpt)
   line      :: [Pos]      -> [LineOpt]    -> Request (CWidget LineOpt)
   polygon   :: [Pos]      -> [PolygonOpt] -> Request (CWidget PolygonOpt)
   text      :: Pos        -> [CTextOpt]   -> Request (CWidget CTextOpt)
   image     :: Pos        -> [CImageOpt]  -> Request (CWidget CImageOpt)
   cwindow   :: Pos        -> [CWindowOpt] -> Request CWindow
   clear     :: Action
   save      :: FilePath -> Action

struct ScrollBar < WWidget StdOpt = 
   attach :: ScrollWidget BasicWOpt -> Dir -> Action

-- Canvas widgets

struct CWidget a < ConfWidget a = 
   getCoords :: Request [Pos]
   setCoords :: [Pos] -> Action
   move      :: Pos -> Action

struct CWindow < CWidget WindowOpt, BasicWindow WindowOpt

-- Menus

struct Menu < ConfWidget MenuOpt =
   mButton :: [MButtonOpt] -> Request MButton
   cascade :: [MButtonOpt] -> Request Menu

struct MButton < Configurable MButtonOpt, Invokable

-- Colors

data Color = RGB Int Int Int deriving Eq

black     = RGB 0 0 0
white     = RGB 255 255 255
red       = RGB 255 0 0
green     = RGB 0 255 0
blue      = RGB 0 0 255
yellow    = RGB 255 255 0
grey      = RGB 200 200 200
magenta   = RGB 255 0 255
cyan      = RGB 0 255 255
orange    = RGB 255 180 0
brown     = RGB 0 160 255
darkGreen = RGB 0 150 0

-- Auxiliary types for options

data None                  = None
data AnchorType		   = NW | N | NE | W | C | E | SW | S | SE 
data ReliefType    	   = Raised | Sunken | Flat | Ridge | Solid | Groove
data VertSide      	   = Top | Bottom 
data WrapType      	   = NoWrap | CharWrap | WordWrap
data SelectType    	   = Single | Multiple
data Align         	   = LeftAlign | CenterAlign | RightAlign
data Round         	   = Round
data ArcStyleType  	   = Pie | Chord | Perimeter
data CapStyleType  > Round = Butt | Proj 
data JoinStyleType > Round = Bevel | Miter 
data ArrowType     > None  = First | Last | Both

-- Options

data Anchor       = Anchor AnchorType
data Angles       = Angles Pos
data ArcStyle     = ArcStyle ArcStyleType
data Arrow        = Arrow ArrowType
data Background   = Background Color
data BitmapData   = BitmapData String
data BorderWidth  = BorderWidth Int
data Btmp         = Btmp Image
data CapStyle     = CapStyle CapStyleType
data CLabel       = CLabel String
data CmdInt       = CmdInt (Int -> Cmd ())
data Command      = Command (Cmd ())
data Cursor       = Cursor String
data Enabled      = Enabled Bool
data File         = File FilePath
data Fill         = Fill Color
data Font         = Font String | NamedFont TkFont
data Foreground   = Foreground Color
data From         = From Int
data Height       = Height Int
data Img          = Img Image
data Indicatoron  = Indicatoron Bool
data JoinStyle    = JoinStyle JoinStyleType
data Justify      = Justify Align
data Length       = Length Int
data Orientation  = Orientation Dir
data Outline      = Outline Color
data Padx         = Padx Int
data Pady         = Pady Int
data Relief       = Relief ReliefType
data ScrollRegion = ScrollRegion (Int,Int) (Int,Int)
data SelectMode   = SelectMode SelectType
data SelectColor  = SelectColor (Maybe Color)
data Smooth       = Smooth Bool
data Stipple      = Stipple String
data Text         = Text String
data Title        = Title String          
data To           = To Int
data Underline    = Underline Int
data Width        = Width Int
data Wrap         = Wrap WrapType

-- Widget option types

data BasicOpt       > Background, BorderWidth, Cursor, Relief
data BasicWOpt      > BasicOpt, Width
data DimOpt         > Height, Width
data StdOpt         > BasicWOpt, DimOpt
data FontOpt        > Font, Foreground, Anchor, Justify
data PadOpt         > Padx, Pady
data WindowOpt      > BasicOpt, Title
data PhotoOpt       > DimOpt, File
data BitmapOpt      > Background, Foreground, File, BitmapData
data ButtonOpt      > MenuButtonOpt, Command
data CanvasOpt      > StdOpt, ScrollRegion 
data CheckButtonOpt > ButtonOpt, Indicatoron, SelectColor
data EntryOpt       > BasicWOpt, Justify, Font, Foreground, Enabled
data LabelOpt       > StdOpt, FontOpt, PadOpt, Img, Btmp, Underline, Text
data ListBoxOpt     > StdOpt, Font, Foreground, SelectMode
data MenuButtonOpt  > LabelOpt, Enabled
data SliderOpt      > BasicWOpt, From, To, Orientation, Length, 
                      Font, Foreground, CmdInt, Enabled
data TextEditorOpt  > StdOpt, Font, Foreground, PadOpt, Wrap, Enabled

data CBasicOpt      > Fill, Width, Stipple
data CImageOpt      > Anchor, Img, Btmp
data CTextOpt       > Font, Justify, Text, Anchor, Fill
data CWindowOpt     > DimOpt, Anchor
data LineOpt        > CBasicOpt, Arrow, Smooth, CapStyle, JoinStyle
data PolygonOpt     > OvalOpt, Smooth
data ArcOpt         > OvalOpt, ArcStyle, Angles
data OvalOpt        > CBasicOpt, Outline
data MenuOpt        > WindowOpt, Enabled
data MButtonOpt     > StdOpt, FontOpt, PadOpt, Img, Btmp, Underline, 
                      CLabel, Enabled, Command
data AllOpt         > MenuOpt, CheckButtonOpt, TextEditorOpt, StdOpt, LineOpt, 
		      WindowOpt, ArcOpt, PolygonOpt, OvalOpt, CTextOpt, 
		      SliderOpt, MButtonOpt, CanvasOpt, ListBoxOpt, BitmapOpt, 
		      PhotoOpt, CImageOpt, EntryOpt, CWindowOpt, ButtonOpt, 
		      MenuButtonOpt, LabelOpt

--- Events

type PCmd = Pos -> Cmd ()

data ButtonPress = ButtonPress Int PCmd | AnyButtonPress (Int -> PCmd)

data MouseEvent > ButtonPress = ButtonRelease Int PCmd
         		      | AnyButtonRelease (Int -> PCmd)
			      | Motion Int PCmd
			      | AnyMotion PCmd
			      | Double ButtonPress
			      | Triple ButtonPress

data WindowEvent = Enter (Cmd ()) | Leave (Cmd ()) | Configure PCmd |
		   Destroy (Cmd ())

data SimpleKeyEvent = KeyPress String (Cmd ())
                    | KeyRelease String (Cmd ())
                    | AnyKeyPress (String -> Cmd ())

data KeyEvent > SimpleKeyEvent = Mod [Modifier] SimpleKeyEvent

data Modifier = Alt | Control  | Lock | Meta | Shift deriving Show

data Event > MouseEvent, KeyEvent, WindowEvent

-- Unparser

instance Show Dir where show Hor = "hor"
   			show Ver = "ver"

instance Show Stretch where show None   = ""
   			    show XFill  = "-fill x"
			    show YFill  = "-fill y"
			    show XYFill = "-fill both"

instance Show ArrowType where show None  = "none"
  			      show First = "frst"
			      show Last  = "last"
			      show Both  = "both"

instance Show ArcStyleType where show Pie       = "pieslice"
  			         show Chord     = "chord"
				 show Perimeter = "arc"
 
instance Show CapStyleType where show Round = "round"
  				 show Butt  = "butt"
				 show Proj  = "projecting"

instance Show JoinStyleType where show Round = "round"
  				  show Bevel = "bevel"
				  show Miter = "miter"

instance Show AnchorType where show NW = "nw"
  			       show N  = "n"
			       show NE = "ne"
			       show W  = "w"
			       show C  = "c"
			       show E  = "e"
			       show SW = "sw"
			       show S  = "s"
			       show SE = "se"

instance Show ReliefType where show Raised = "raised"
  			       show Sunken = "sunken"
			       show Flat   = "flat"
			       show Ridge  = "ridge"
			       show Solid  = "solid"
			       show Groove = "groove"

instance Show VertSide where show Top    = "top"
  			     show Bottom = "bottom"

instance Show Color where 
   showsPrec _ (RGB r g b) rest = "#" ++ concat (map (hex 2 "") [r,g,b]) ++ rest
                    where hex 0 rs _ = rs
                          hex t rs 0 = hex (t-1) ('0':rs) 0
                          hex t rs i = hex (t-1)((chr (48+m+7*(div m 10))):rs) d
			               where m = mod i 16; d = div i 16

quoteString s = "\"" ++ concatMap quote s ++ "\"" where quote '$'  = "\\$"
		      					quote '['  = "\\["
							quote '"'  = "\\\""
							quote '\\' = "\\\\"
							quote c    = [c]

textOpt :: AllOpt -> Cmd String
textOpt opt = 
     case opt of
          Anchor a       -> return $ "anchor "++show a
          Angles (x,y)   -> return $ "start "++show x++" -extent "++show y
          ArcStyle a     -> return $ "style "++show a
          Arrow a        -> return $ "arrow "++show a
          Background c   -> return $ "background "++show c
          BorderWidth n  -> return $ "borderwidth "++show n
          BitmapData str -> return $ "data "++quoteString str
          CapStyle c     -> return $ "capstyle "++show c
          Btmp bmp       -> return $ "bitmap "++bmp.imageName
          CLabel str     -> return $ "label "++quoteString str
          Cursor str     -> return $ "cursor "++str
          Command a      -> addCallBack "command" $ const a
          CmdInt a       -> addCallBack "command" $ a . read . last . words
          Enabled True   -> return $ "state normal"
          Enabled _      -> return $ "state disabled"
          File p         -> return $ "file "++p
          Fill c         -> return $ "fill "++show c
          Foreground c   -> return $ "foreground "++show c
          Font str       -> return $ "font "++quoteString str
          NamedFont f    -> return $ "font "++quoteString (f.fontName)
          From x         -> return $ "from "++show x
          Height h       -> return $ "height "++show h
          Img image      -> return $ "image "++image.imageName
          Indicatoron b  -> return $ "indicatoron "++show b
          JoinStyle c    -> return $ "joinstyle "++show c
          Justify a      -> return $ "justify "++textAlign a
          Length n       -> return $ "length "++show n
          Orientation d  -> return $ "orient "++show d
          Outline c      -> return $ "outline "++show c
          Padx n         -> return $ "padx "++show n
          Pady n         -> return $ "pady "++show n
          Relief r       -> return $ "relief "++show r
          ScrollRegion (x1,y1) (x2,y2) 
	  		       -> return $ "scrollregion {"++
			 		   concatMap showB [x1,y1,x2,y2]++"}"
          SelectMode s         -> return $ "selectmode "++textSelect s
          SelectColor (Just c) -> return $ "selectcolor "++show c
          SelectColor _        -> return $ "selectcolor"++"\"\""
          Smooth b             -> return $ "smooth "++show b
          Stipple file	       -> return $ "stipple "++'@':file
          Text str     	       -> return $ "text "++quoteString str
          Title  str           -> return $ "title "++quoteString str
          To x                 -> return $ "to "++show x
          Underline x          -> return $ "underline "++show x
          Width  w             -> return $ "width "++show w
          Wrap w               -> return $ "wrap "++textWrap w
     where showB :: Int -> String
	   showB x = ' ':show x	   

textOpts :: [AllOpt] -> Cmd String
textOpts opts = do os <- mapM textOpt opts
  	           return $ concatMap (" -"++) os

textEv :: Event -> String
textEv ev = '<':t ev++">"
  where t (ButtonPress n _)    = "ButtonPress-"++show n
        t (AnyButtonPress _)   = "ButtonPress"
	t (ButtonRelease n _)  = "ButtonRelease-"++show n
	t (AnyButtonRelease _) = "ButtonRelease"
	t (Motion n _)         = 'B':show n++"-Motion"
	t (AnyMotion  _)       = "Motion"
	t (Double bp)          = "Double-"++t bp
	t (Triple bp)          = "Triple-"++t bp
	t (Enter _)            = "Enter"
     	t (Leave _)            = "Leave"
	t (Configure _)        = "Configure"
	t (KeyPress c _)       = "KeyPress-"++c
	t (KeyRelease c _)     = "KeyRelease-"++c
	t (AnyKeyPress _)      = "KeyPress"
	t (Mod ms sk)          = concatMap (\m -> show m++"-") ms++t sk
	t (Destroy _)          = "Destroy"     

textSelect Single   = "browse"
textSelect Multiple = "extended"

textWrap NoWrap   = "none"
textWrap CharWrap = "char"
textWrap WordWrap = "word"

textAlign LeftAlign   = "left"
textAlign CenterAlign = "center"
textAlign RightAlign  = "right"

app :: String -> Cmd String
app str = do x <- primGetPath
	     return $ if str == "." then x else str++x
	     
--- Packing

p1 <<< p2 = row [p1,p2]
p1 ^^^ p2 = col [p1,p2]

row,col :: [Packable] -> Packable
row []       = error "row []" 
row as@(a:_) = struct packIn = lpackIn Hor as
	              wname  = a.wname
col []       = error "col []" 
col as@(a:_) = struct packIn = lpackIn Ver as
	              wname  = a.wname
		    
lpackIn dir as mstr dir' fill exp = 
			      do col <- primExTcl [mstr,"configure -bg"]
			         nm0 <- app mstr
			         primExTcl_ ["frame",nm0,"-bg",last $ words col]
			         primExTcl_ ["lower",nm0]
			         wpackIn nm0 mstr dir' fill exp
			         forall a <- as do a.packIn nm0 dir XYFill True
			       
wpackIn nm mstr dir fill exp = 
                do primExTcl_ ["pack", nm, "-in", mstr, f dir, show fill, g exp]
		where f Hor = "-side left"
		      f Ver = "-side top"
		      g False = "-expand 0"
		      g _     = "-expand 1"

fill fillMode exp p = struct 
     packIn mstr dir _ _ = p.packIn mstr dir fillMode exp
     wname = p.wname

pack :: Packable -> Cmd ()
pack p = p.packIn p.wname Ver XYFill True

rigid = fill None False
fillX = fill XFill False
fillY = fill YFill False
		      
-- Event handling

bnd :: [String] -> [Event] -> Cmd ()
bnd init evs = do
    forall ev <- evs do
     let g a ps    = do nr <- primAddCallBack a
     		        primExTcl_ $ init++[textEv ev,"{doEvent \"",show nr,
					    ps++"\"}"]
         bindxy f  = do let a str = f (read xstr,read ystr) 
	 		            where [_,xstr,ystr] = words str
                        g a "%x %y"
         bindwh f  = do let a str = f (read xstr,read ystr) 
	 			    where [_,xstr,ystr] = words str
                        g a "%w %h"
         bindbxy f = do let a str = f (read but) (read xstr,read ystr) 
                                    where [_,but,xstr,ystr] = words str
                        g a "%b %x %y"
         bind_ f   = g (const f) ""
         bindK f   = do let a str = f ws where [_,ws] = words str
                        g a "%K"
     case ev of ButtonPress _ f          -> bindxy f 
                Double (ButtonPress _ f) -> bindxy f 
		Triple (ButtonPress _ f) -> bindxy f 
		ButtonRelease _ f        -> bindxy f  
		Motion _ f               -> bindxy f 
		AnyMotion f              -> bindxy f
		AnyButtonPress f         -> bindbxy f
		AnyButtonRelease f       -> bindbxy f
		Enter a                  -> bind_ a
		Leave a                  -> bind_ a
		Configure f              -> bindwh f
		KeyPress _ a             -> bind_ a
		KeyRelease _ a           -> bind_ a
		AnyKeyPress f            -> bindK f
		Destroy f                -> bind_ f
		
-- Widget templates

widget :: String -> Template (ConfWidget AllOpt)
widget nm = template in 
                    struct ident    = nm
   	                   destroy  = primExTcl_ ["destroy",nm]
		           exists   = request b <- primExTcl ["winfo exists",nm]
		      			      return $ toEnum $ read b
		           set os   = action ostr <- textOpts os
		       			     primExTcl_ [nm,"configure",ostr] 
		           focus    = primExTcl_ ["focus",nm]
			   lower    = primExTcl_ ["lower",nm]
			   raise    = primExTcl_ ["raise",nm]
			   bind evs = action bnd ["bind",nm] evs
  
men :: String -> String -> Template Menu
men wname nm = template index := -1
                        w <- widget nm in
     struct ident   = w.ident
            destroy = w.destroy
	    exists  = w.exists
	    set     = w.set
	    focus   = w.focus
	    lower   = w.lower
	    raise   = w.raise
	    bind    = w.bind
	    mButton opts = request index := index+1
          		           os <- textOpts opts
				   primExTcl_ [nm,"add command", os]
				   mbut nm index
            cascade opts = request index := index+1
          		           nm1 <- app nm
				   os <- textOpts opts
				   primExTcl_ ["menu",nm1]
				   primExTcl_ [nm,"add cascade -menu",nm1,os]
				   men wname nm1
           
mbut :: String -> Int -> Template MButton
mbut nm index = template in
   struct invoke = primExTcl_ [nm,"invoke",show index]
          set os = action forall o <- os do
            		    ostr <- textOpt o
			    primExTcl_ [nm,"entryconfigure",show index,'-':ostr]

winsetcmd :: String -> [AllOpt] -> Cmd ()
winsetcmd nm os = do
   forall o <- os do
      ostr <- textOpt o
      case o of Title str -> primExTcl_ ["wm title",nm,quoteString str]
                From _    -> primExTcl_ [nm,"configure",'-':ostr]
		_         -> primExTcl_ [nm,"configure",'-':ostr]

managedWindow :: String -> Template ManagedWindow
managedWindow nm = template in 
  let parse str = ((read w,read h),(read x,read y)) 
                  where [(w,_:str1)] = lex str
		        [(h,_:str2)] = lex str1
			[(x,_:str3)] = lex str2
			[(y,str4)]   = lex str3
      getGeometry = request str <- primExTcl ["winfo geometry",nm]
         		    return $ parse str
      setSize (w,h)     = primExTcl_ ["wm geometry",nm,show w++"x"++show h]
      setPosition (x,y) = primExTcl_ ["wm geometry",nm,"+"++show x++"+"++show y]
      iconify   = primExTcl_ ["wm iconify",nm]
      deiconify = primExTcl_ ["wm deiconify",nm]
  in struct ..ManagedWindow

scrbar :: String -> String -> Template ScrollBar
scrbar wname nm =
  template w <- widget nm in
     struct 
      ident   = w.ident
      destroy = w.destroy
      exists  = w.exists
      set     = w.set
      focus   = w.focus
      lower   = w.lower
      raise   = w.raise
      bind    = w.bind
      packIn  = wpackIn nm
      wname   = wname
      attach win dir = action
       let wn = win.ident
       case dir of
           Hor -> primExTcl_ [wn,"configure -xscrollcommand {",nm,"set}"]
                  primExTcl_ [nm,"configure -orient hor -command {",wn,"xview}"]
           Ver -> primExTcl_ [wn,"configure -yscrollcommand {",nm,"set}"]
                  primExTcl_ [nm,"configure -orient ver -command {",wn,"yview}"]

xyview :: String -> String -> Cmd (Double,Double)
xyview nm str = do xy <- primExTcl [nm,str]
        	   let [x,y] = words xy
		   return (read0 x,read0 y)
	        where read0 :: String -> Double
		      read0 "0" = 0.0
		      read0 "1" = 1.0
		      read0 str = read str
				 
ent :: String -> String -> Template Entry
ent wname nm =
  template w <- widget nm in
     struct ident      = w.ident
            destroy    = w.destroy
	    exists     = w.exists
	    set        = w.set
	    focus      = w.focus
	    lower      = w.lower
	    raise      = w.raise
	    bind       = w.bind
	    packIn     = wpackIn nm
	    wname      = wname
	    setValue a = action primExTcl_ [nm,"delete 0 end"]
           		        primExTcl_ [nm,"insert end",quoteString a]
            getValue   = primExTcl [nm,"get"]
	    mark x     = primExTcl_ [nm,"scan mark",show x]
	    drag x     = primExTcl_ [nm,"scan dragto",show x]
	    cursorPos  = request ind <- primExTcl [nm,"index insert"]
            		         return $ read ind
            xview      = request xyview nm "xview"
            yview      = request xyview nm "yview"

-- Window templates

bwin :: String -> Template (BasicWindow AllOpt)
bwin nm = 
  template w <- widget nm in
        let ident   = w.ident
            destroy = w.destroy
	    exists  = w.exists
	    set     = w.set
	    focus   = w.focus
	    lower   = w.lower
	    raise   = w.raise
	    bind    = w.bind
	    wwid wname f opts = request nm0 <- app nm
					os <- textOpts opts
					primExTcl_ [wname,nm0,os]
					f nm nm0 
            button      = wwid "button" but
	    label       = wwid "label" lab
	    canvas      = wwid "canvas" canv
	    textEditor  = wwid "text" editor
	    listBox     = wwid "listbox" lstbox
	    entry       = wwid "entry" ent
	    radioButton = wwid "radiobutton" rdbut
	    menuButton  = wwid "menubutton" menubut
	    scrollBar   = wwid "scrollbar" scrbar
	    frame opts  = request nm0 <- app nm
				  os <- textOpts opts
				  primExTcl_ ["frame",nm0,os]
				  primExTcl_ ["lower",nm0]
				  frm nm nm0 
            checkButton opts = request nm0 <- app nm
				       os <- textOpts opts
				       primExTcl_ ["checkbutton",nm0,
				       		   "-variable",nm0,os]
			               chbut nm nm0 
            slider opts = request nm0 <- app nm
				  os <- textOpts opts
				  primExTcl_ ["scale",nm0,"-variable",nm0,os]
				  sldr nm nm0 
        in struct ..BasicWindow

win :: String -> Template Window 
win nm = 
  template bw <- bwin nm 
           mw <- managedWindow nm in
  struct
      ident       = bw.ident
      destroy 	  = bw.destroy
      exists  	  = bw.exists
      set os      = action winsetcmd nm os
      focus       = bw.focus
      lower       = bw.lower
      raise       = bw.raise
      bind        = bw.bind
      button      = bw.button
      canvas      = bw.canvas
      checkButton = bw.checkButton
      entry       = bw.entry
      frame       = bw.frame
      label       = bw.label
      listBox     = bw.listBox
      menuButton  = bw.menuButton
      radioButton = bw.radioButton
      scrollBar   = bw.scrollBar
      slider      = bw.slider
      textEditor  = bw.textEditor
      getGeometry = mw.getGeometry
      setSize     = mw.setSize
      setPosition = mw.setPosition
      iconify     = mw.iconify
      deiconify   = mw.deiconify

frm :: String -> String -> Template Frame
frm wname nm =
  template wwid <- bwin nm in
     struct ident       = wwid.ident
            destroy     = wwid.destroy
	    exists      = wwid.exists
	    set         = wwid.set
	    focus       = wwid.focus
	    lower       = wwid.lower
	    raise       = wwid.raise
	    bind        = wwid.bind
	    button      = wwid.button
	    canvas      = wwid.canvas
	    checkButton = wwid.checkButton
	    entry       = wwid.entry
	    frame       = wwid.frame
	    label       = wwid.label
	    listBox     = wwid.listBox
	    menuButton  = wwid.menuButton
	    radioButton = wwid.radioButton
	    scrollBar   = wwid.scrollBar
	    slider      = wwid.slider
	    textEditor  = wwid.textEditor
	    packIn      = wpackIn nm
	    wname       = wname

but :: String -> String -> Template Button
but wname nm = 
  template w <- widget nm in
     struct ident   = w.ident
      	    destroy = w.destroy
	    exists  = w.exists
	    set     = w.set
	    focus   = w.focus
	    lower   = w.lower
	    raise   = w.raise
	    bind    = w.bind
	    packIn  = wpackIn nm 
	    wname   = wname
	    flash   = primExTcl_ [nm,"flash"]
	    invoke  = primExTcl_ [nm,"invoke"]

chbut :: String -> String -> Template CheckButton
chbut wname nm =  
  template w <- but wname nm in
     struct ident      = w.ident
            destroy    = w.destroy
	    exists     = w.exists
	    set        = w.set
	    focus      = w.focus
	    lower      = w.lower
	    raise      = w.raise
	    bind       = w.bind
	    packIn     = w.packIn
            wname      = w.wname
	    flash      = w.flash
	    invoke     = w.invoke
	    toggle     = primExTcl_ [nm,"toggle"]
	    checked    = request s <- primExTcl ["global",nm,"; set",nm]
         		         return $ toEnum $ read s

rdbut :: String -> String -> Template RadioButton
rdbut wname nm = 
  template w <- but wname nm in
     struct ident      = w.ident
            destroy    = w.destroy
	    exists     = w.exists
	    set        = w.set
	    focus      = w.focus
	    lower      = w.lower
	    raise      = w.raise
	    bind       = w.bind
	    packIn     = w.packIn
            wname      = w.wname
	    flash      = w.flash
	    invoke     = w.invoke
	    select     = primExTcl_ [nm,"select"]
	    deselect   = primExTcl_ [nm,"deselect"]
      
menubut :: String -> String -> Template MenuButton
menubut wname nm = 
  template w <- widget nm in
     struct ident      = w.ident
            destroy    = w.destroy
	    exists     = w.exists
	    set        = w.set
	    focus      = w.focus
	    lower      = w.lower
	    raise      = w.raise
	    bind       = w.bind
	    packIn     = wpackIn nm
	    wname      = wname
	    menu opts  = request nm0 <- app nm
				 os <- textOpts opts
				 primExTcl_ ["menu",nm0,"-tearoff 0",os]
				 primExTcl_ [nm,"configure -menu",nm0]
				 men wname nm0
      
sldr :: String -> String -> Template Slider
sldr wname nm = 
  template w <- widget nm in
     struct ident      = w.ident
            destroy    = w.destroy
	    exists     = w.exists
	    set        = w.set
	    focus      = w.focus
	    lower      = w.lower
	    raise      = w.raise
	    bind       = w.bind
	    packIn     = wpackIn nm
	    wname      = wname
	    setValue x = primExTcl_ [nm,"set",show x]
	    getValue   = request x <- primExTcl ["global",nm,"; set",nm]
         		         return $ read x

lab :: String -> String -> Template Label 
lab wname nm =  
  template w <- widget nm in
     struct ident      = w.ident
            destroy    = w.destroy
	    exists     = w.exists
	    set        = w.set
	    focus      = w.focus
	    lower      = w.lower
	    raise      = w.raise
	    bind       = w.bind
	    packIn     = wpackIn nm
	    wname      = wname

editor :: String -> String -> Template TextEditor
editor wname nm = 
  template w <- widget nm in
     struct ident     = w.ident
            destroy   = w.destroy
	    exists    = w.exists
	    set       = w.set
	    focus     = w.focus
	    lower     = w.lower
	    raise     = w.raise
	    bind      = w.bind
	    packIn    = wpackIn nm
	    wname     = wname
	    lines     = request x <- primExTcl [nm,"index end"]
       			        return $ read(takeWhile (/='.') x)-1
            getLine n = primExTcl[nm, "get",show n++".0",show (n+1)++".0"] 
	    insertLines n ls = primExTcl_ [nm,"insert", show n++".0", 
                                           quoteString $ unlines ls]
            deleteLine n     = primExTcl_ [nm,"delete",show n++".0",
	    				   show(n+1)++".0"]
            mark (x,y) = primExTcl_ [nm,"scan mark",show x,show y]
	    drag (x,y) = primExTcl_ [nm,"scan dragto",show x,show y]
            xview      = request xyview nm "xview"
            yview      = request xyview nm "yview"

lstbox :: String -> String -> Template ListBox
lstbox wname nm =
  template w <- widget nm in
     struct ident     = w.ident
            destroy   = w.destroy
	    exists    = w.exists
	    set       = w.set
	    focus     = w.focus
	    lower     = w.lower
	    raise     = w.raise
	    bind      = w.bind
	    packIn    = wpackIn nm
	    wname     = wname
	    lines     = request x <- primExTcl [nm, "size"]
                                return (read x)
            getLine n = primExTcl [nm, "get", show n] 
	    insertLines n ls = primExTcl_ $ [nm,"insert",show n," "]++
                                            map (\s -> quoteString s ++" ") ls
            deleteLine n     = primExTcl_ [nm,"delete",show n]  
	    setValue ns      = action primExTcl_ [nm,"selection clear 0 end"]
                                      forall n <- ns do
			                 primExTcl_ [nm,"selection set",show n]
            getValue         = request ns <- primExTcl [nm,"curselection"]   
        		               return $ map read $ words ns
            mark (x,y)       = primExTcl_ [nm,"scan mark",show x,show y]
	    drag (x,y)       = primExTcl_ [nm,"scan dragto",show x,show y]
	    view n           = primExTcl_ [nm,"see",show n]
            xview            = request xyview nm "xview"
            yview     	     = request xyview nm "yview"

canv :: String -> String -> Template Canvas
canv wname nm = 
  template w <- widget nm in
        let ident     = w.ident
            destroy   = w.destroy
	    exists    = w.exists
	    set       = w.set
	    focus     = w.focus
	    lower     = w.lower
	    raise     = w.raise
	    bind      = w.bind
	    packIn    = wpackIn nm
	    wname     = wname
	    cwid typ ps f opts = request
	    		   os <- textOpts opts
			   let g (x,y) str = show x:show y:str
			   id <- primExTcl $ nm:"create":typ:foldr g [] ps++[os]
			   f nm id  
	    oval p1 p2      = cwid "oval" [p1,p2] cwidget
	    arc p1 p2       = cwid "arc" [p1,p2] cwidget
	    rectangle p1 p2 = cwid "rectangle" [p1,p2] cwidget
	    line ps 	    = cwid "line" ps cwidget
	    polygon ps      = cwid "polygon" ps cwidget
	    text p          = cwid "text" [p] cwidget
	    image p 	    = cwid "image" [p] cwidget
	    cwindow p opts  = request cwin nm p opts
	    clear           = primExTcl_ [nm,"delete all"]
	    save file       = primExTcl_ [nm,"postscript -file",file]
	    mark (x,y) 	    = primExTcl_ [nm,"scan mark",show x,show y]
	    drag (x,y) 	    = primExTcl_ [nm,"scan dragto",show x,show y]
            xview           = request xyview nm "xview"
            yview           = request xyview nm "yview"
        in struct ..Canvas

cwin :: String -> Pos -> [CWindowOpt] -> Cmd CWindow
cwin nm (x,y) opts = do
  nmw <- app nm
  wwid <- bwin nmw
  os <- textOpts opts
  primExTcl_ ["frame",nmw]
  id <- primExTcl[nm,"create window",show x,show y,os,"-window",nmw]
  cwid <- cwidget nm id
  template in struct ident   	  = cwid.ident
      		     destroy 	  = cwid.destroy
		     exists  	  = cwid.exists
		     set          = cwid.set
		     focus   	  = cwid.focus
		     lower   	  = cwid.lower
		     raise   	  = cwid.raise
		     bind    	  = cwid.bind
		     getCoords    = cwid.getCoords
		     setCoords    = cwid.setCoords
		     move         = cwid.move
		     button       = wwid.button
		     canvas       = wwid.canvas
		     checkButton  = wwid.checkButton
		     entry        = wwid.entry
		     frame        = wwid.frame
		     label        = wwid.label
		     listBox      = wwid.listBox
		     menuButton   = wwid.menuButton
		     radioButton  = wwid.radioButton
		     scrollBar    = wwid.scrollBar
		     slider       = wwid.slider
		     textEditor   = wwid.textEditor

cwidget :: String -> String -> Template (CWidget AllOpt)
cwidget nm id = template in
        let ident     = nm++'i':show id
            destroy   = primExTcl_ [nm,"delete",id]
	    exists    = request t <- primExTcl [nm,"type",id]
	    		        return $ t/=""
            set os    = action forall o <- os
	    			  do ostr <- textOpt o 
           		             primExTcl_ [nm,"itemconfigure",id,'-':ostr]
            focus     = primExTcl_ [nm,"focus",id]
	    lower     = primExTcl_ [nm,"lower",id]
	    raise     = primExTcl_ [nm,"raise",id]
	    bind evs  = action bnd [nm,"bind",id] evs
            getCoords = request str <- primExTcl [nm,"coords",id]
        	                return $ coords $ map (round . read) $ words str
                        where coords (x:y:ps) = (x,y):coords ps
		              coords _ = []
            setCoords ps = primExTcl_ $ [nm,"coords",id]++
                                        map (\(x,y) -> show x++" "++show y) ps
            move (x,y)   = primExTcl_ [nm,"move",id,show x,show y]
        in struct ..CWidget 

-- Image and font templates

btmp :: String -> Template ConfBitmap 
btmp nm = template in
  struct imageName = nm
         set os    = action forall o <- os 
	 		       do ostr <- textOpt o 
      			          primExTcl_ [nm,"configure",'-':ostr]

phto :: String -> Template Photo
phto nm = template in
  struct
   imageName = nm
   set os    = action forall o <- os do ostr <- textOpt o 
       		      			primExTcl_ [nm,"configure",'-':ostr]
   blank = primExTcl_[nm,"blank"]
   putPixel (x,y) col = primExTcl_ [nm,"put {{",show col,"}} -to",show x,show y]
   getPixel (x,y) = request str <- primExTcl [nm,"get",show x,show y]
      			    let [r,g,b] = words str
			    return $ RGB (read r) (read g) $ read b  
   copyFrom ph = primExTcl_ [nm,"copy",ph.imageName]
   saveAs file = primExTcl_ [nm,"write",file]
		
tkfont :: String -> Int -> Int -> Int -> Bool -> Template TkFont
tkfont name asc desc lsp fixed = template in 
   struct fontName  = name
     	  ascent    = asc
	  descent   = desc
	  linespace = lsp
	  fixed     = fixed
          getTextWidth str = request
	          width <- primExTcl $ "font measure":map quoteString [name,str]
	          return $ read width

-- Tk templates

hnd :: String -> Template Runnable
hnd nm = template running := False in
            struct start = action if not running then running := True
          			                      primExTcl_ [nm]
                   stop  = action running := False
                                  primExTcl_ ["after cancel",nm]

primTk :: Template Tk
primTk = template in
   let window opts = request x <- primGetPath
         		     primExTcl_["toplevel",x]
			     winsetcmd x opts
			     win x 
       bell      = primExTcl_ ["bell"]
       delay t a = request
         n <- primNextCallBack
         tag <- primExTcl ["after",show t,"{doEvent ",show n,"}"]
         let tag' = drop 6 tag            -- all tags start with "after#"
         primAddCallBack $ const $ a tag'
         return tag'
       periodic t a = request
         n <- primAddCallBack $ const a
         let ln = "loop"++show n
         primExTcl_ ["proc",ln,"{args} {haskellEvent ",
	 	     show n,"\nupdate\nafter",show t,ln,"}"]
         hnd ln
       bitmap opts = request os <- textOpts opts
        		     nm <- primExTcl ["image create bitmap",os]
			     btmp nm 
       photo opts  = request os <- textOpts opts
         	   	     nm <- primExTcl ["image create photo",os]
			     phto nm 
       font name   = request
	 ascent <- primExTcl ["font metrics",quoteString name,"-ascent"]
	 descent <- primExTcl ["font metrics",quoteString name,"-descent"]
	 lsp <- primExTcl ["font metrics",quoteString name,"-linespace"]
	 fixed <- primExTcl ["font metrics",quoteString name,"-fixed"]
         tkfont name (read ascent) (read descent) (read lsp) (fixed=="1")
       runTclCmd cmd  = request primExecuteTcl cmd
       runTclCmd_ cmd = primExTcl_ [cmd]
   in struct ..Tk

primTkEnv :: Template TkEnv
primTkEnv =
   template tkenv <- primTk
            env   <- primStdEnvT in
   let window      = tkenv.window
       bell        = tkenv.bell
       delay       = tkenv.delay
       periodic    = tkenv.periodic
       bitmap      = tkenv.bitmap
       photo       = tkenv.photo
       font        = tkenv.font
       putChar     = env.putChar
       putStr      = env.putStr
       putStrLn    = env.putStrLn
       setReader a = action
           n <- primAddCallBack $ const $ action s <- primExTcl ["read stdin 1"]
             				         a (head s)
           primExTcl_ ["fileevent stdin readable {doEvent ",show n,"}"]
       setLineReader act = action setLineR act putChar >>= setReader
       writeFile   = env.writeFile
       appendFile  = env.appendFile
       readFile    = env.readFile
       timeOfDay   = env.timeOfDay
       progArgs    = env.progArgs
       getEnv      = env.getEnv
       devices     = env.devices
       inet        = env.inet
       sendMIDI    = env.sendMIDI
       quit        = env.quit
       runTclCmd   = tkenv.runTclCmd		-- new
       runTclCmd_  = tkenv.runTclCmd_		-- new
   in struct ..TkEnv

