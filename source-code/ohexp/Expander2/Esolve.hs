module Esolve where
 import Eterm
 import Epaint(hilbShelf,reducePath)

------------------------------ Copyright (c) peter.padawitz@udo.edu, May 3, 2020

-- Esolve contains:

-- a parser and an unparser of signatures
-- an unparser for proofs
-- the simplifier
-- algorithms for performing rewriting, narrowing, induction, coinduction and
-- the application of theorems

-- STRING PARSER into signatures and signature maps

 signature syms = concat [do key <- oneOf keywords; sigrest key syms,
		          return syms]

 sigrest key syms =
      concat [do key <- oneOf keywords; sigrest key syms,
	      do x <- sigWord
	         let [ps',cps',cs',ds',fs'] = map (`minus1` x) [ps,cps,cs,ds,fs]
	             f s = s `join1` x
	         case key of
	         "specs:"      -> sigrest key (f specs,ps,cps,cs,ds,fs,hs)    
	         "preds:"      -> sigrest key (specs,f ps,cps',cs',ds',fs',hs)
	         "copreds:"    -> sigrest key (specs,ps',f cps,cs',ds',fs',hs)
		 "constructs:" -> sigrest key (specs,ps',cps',f cs,ds',fs',hs)
		 "defuncts:"   -> sigrest key (specs,ps',cps',cs',f ds,fs',hs)
		 "fovars:"     -> sigrest key (specs,ps',cps',cs',ds',f fs,hs)
		 "hovars:"     -> do es <- instances syms
                                     sigrest key (specs,ps,cps,cs,ds,fs,
                                                  updRel hs x es),
	      return syms]
      where (specs,ps,cps,cs,ds,fs,hs) = syms
 
 instances syms = concat [do symbol "{"; es <- p; symbol "}"; return es,
           		  return []]
                  where (_,ps,cps,cs,ds,_,_) = syms
                        p = do e <- oneOf $ ps++cps++cs++ds
		               concat [do symbol ","; es <- p; return $ e:es,
	                               return [e]]
 
 sigWord = token $ some (sat item (`notElem` "{\t\n ")) ++ infixWord

 keywords = words "specs: preds: copreds: constructs: defuncts: fovars: hovars:"

 sigMap p@(f,xs) sig sig' = concat [act relational, act functional,
				    act (.isFovar), act (.isHovar), return p]
	                 where act g = do (x,y) <- assignment (g sig) $ g sig'
                                          sigMap (upd f x y,xs`join1`x) sig sig'

 assignment c d = do x <- sat sigWord c; symbol "->"
 		     y <- sat sigWord d; return (x,y)

-- SIGNATURE and SIGNATURE MAP PARSER into strings

 showSignature :: ([String],[String],[String],[String],[String],Pairs String)
		  -> String -> String

 showSignature (ps,cps,cs,ds,fs,hs) = showSymbols "preds:      " ps .
          		              showSymbols "copreds:    " cps .
				      showSymbols "constructs: " cs .
				      showSymbols "defuncts:   " ds .
				      showSymbols "fovars:     " fs .
				      showHovars hs

 showSymbols _ []   = id 
 showSymbols init s = ((splitStrings 12 85 init s ++ "\n") ++) 

 showHovars []          = id
 showHovars [(x,[])]    = ("hovars:     " ++) . (x ++) . (' ':)
 showHovars [(x,es)]    = ("hovars:     " ++) . (x ++) . showInstances es 
 showHovars ((x,[]):hs) = showHovars hs . (x ++) . (' ':)
 showHovars ((x,es):hs) = showHovars hs . (x ++) . showInstances es

 showInstances es = ('{':) . (init (tail (filter (/= '\"') (show es))) ++) .
		    ("} " ++)

 showSignatureMap (f,[x]) str  = x++" -> "++f x++str
 showSignatureMap (f,x:xs) str = x++" -> "++f x++"\n"++
 				 showSignatureMap (f,xs) str
 showSignatureMap _ str        = str

-- EVALUATION

-- signature for arithmetic expressions

 struct Arith a = parseA :: TermS -> Maybe a; zero,one :: a; inv :: a -> a
 		  plus,minus,times,div,min,max :: a -> a -> a

-- The Arith-algebras of integer numbers, real numbers, linear functions and 
-- binary relations
 
 intAlg :: Arith Int
 intAlg = struct parseA = parseInt; zero = 0; one = 1; inv i = -i; plus = (+)
 		 minus = (-); times = (*); div = div; min = min; max = max

 realAlg :: Arith Float
 realAlg = struct parseA = parseReal; zero = 0; one = 1; inv r = -r; plus = (+)
 		  minus = (-); times = (*); div = (/); min = min; max = max

 linAlg :: Arith LinEq
 linAlg = struct parseA = parseLin; zero = ([],0); one = ([],1)
 		 inv = mulLin ((-1)*); plus = addLin; minus = subLin
		 times f (_,a) = mulLin (*a) f; div f (_,a) = mulLin (/a) f
		 min _ _ = ([],0); max _ _ = ([],1)

 relAlg :: Sig -> Arith [(TermS,TermS)]
 relAlg sig = struct parseA = parseRel sig.states; zero = []
 		     one = [(x,x) | x <- sig.states]
		     inv = minus pairs; plus = join; minus = minus
		     times ps qs = [p | p@(x,y) <- pairs, any ((== x) . fst) ps,
		       		        any ((== y) . snd) qs]
		     div = meet; min = min; max = max
              where pairs = prod2 sig.states sig.states
		    
-- generic Arith-interpreter
 
 foldArith :: Eq a => Arith a -> TermS -> Maybe a
 foldArith alg = f
  where f (F "-" [t])      = do a <- f t; Just $ alg.inv a
        f (F "+" [t,u])    = do a <- f t; b <- f u; Just $ alg.plus a b
        f (F "sum" [F x ts]) | collector x = do as <- mapM f ts; Just $ sum_ as
        f (F "-" [t,u])    = do a <- f t; b <- f u; Just $ alg.minus a b
        f (F "*" [t,u])    = do a <- f t; b <- f u; Just $ alg.times a b
        f (F "product" [F x ts]) | collector x 
			   = do as <- mapM f ts; Just $ prod as
        f (F "**" [t,u])   = do a <- f t; n <- parseNat u
				Just $ prod $ replicate n a
        f (F "/" [t,u])    = do a <- f t; b <- f u; guard $ b /= alg.zero
				Just $ alg.div a b
        f (F "min" [t,u])  = do a <- f t; b <- f u; Just $ alg.min a b
	f (F "max" [t,u])  = do a <- f t; b <- f u; Just $ alg.max a b
	f (F "min" [F x ts]) | collector x
			   = do as@(_:_)<- mapM f ts; Just $ foldl1 alg.min as
	f (F "max" [F x ts]) | collector x
			   = do as@(_:_)<- mapM f ts; Just $ foldl1 alg.max as
	f t 	           = alg.parseA t
	sum_ = foldl alg.plus alg.zero
	prod = foldl alg.times alg.one

-- a signature for state formulas

 struct Modal a label = true,false :: a
 			neg :: a -> a
 			or,and :: a -> a -> a
 			ex,ax,xe,xa :: a -> a
 			dia,box,aid,xob :: label -> a -> a

-- the Modal-algebra of state sets
 
 ctlAlg :: Sig -> Modal [Int] Int
 ctlAlg sig = struct true  = sts
		     false = []
		     neg   = minus sts 
		     or    = join
		     and   = meet
		     ex    = imgsShares sts $ f sig.trans sig.transL
		     ax    = imgsSubset sts $ f sig.trans sig.transL
		     xe    = imgsShares sts $ f (parents sig) (parentsL sig)
		     xa    = imgsSubset sts $ f (parents sig) (parentsL sig)
		     dia   = imgsShares sts . flip (g sig.transL)
		     box   = imgsSubset sts . flip (g sig.transL)
		     aid   = imgsShares sts . flip (g $ parentsL sig)
		     xob   = imgsSubset sts . flip (g $ parentsL sig)
              where sts = indices_ sig.states
	            f trans transL i = join (trans!!i) $ joinMap (g transL i) 
	                                               $ indices_ sig.labels
	            g transL i k = transL!!i!!k

-- the interpreter of state formulas in ctlAlg

 type States = Maybe [Int]

 foldModal :: Sig -> TermS -> States
 foldModal sig = f $ const Nothing where
           alg = ctlAlg sig
           f :: (String -> States) -> TermS -> States
           f g (F x []) | just a       = a where a = g x
           f _ (F "true" [])	       = Just alg.true
           f _ (F "false" [])	       = Just alg.false
           f g (F "not" [t])	       = do a <- f g t; Just $ alg.neg a
           f g (F "\\/" [t,u])         = do a <- f g t; b <- f g u
        				    Just $ alg.or a b
           f g (F "/\\" [t,u])	       = do a <- f g t; b <- f g u
         			            Just $ alg.and a b
           f g (F "`then`" [t,u])      = do a <- f g t; b <- f g u
    				            Just $ alg.or (alg.neg a) b
           f g (F "EX" [t])            = do a <- f g t; Just $ alg.ex a
           f g (F "AX" [t])            = do a <- f g t; Just $ alg.ax a
           f g (F "XE" [t])            = do a <- f g t; Just $ alg.xe a
           f g (F "XA" [t])            = do a <- f g t; Just $ alg.xa a
           f g (F "<>" [lab,t])	       = do a <- f g t; k <- searchL lab
					    Just $ alg.dia k a	
           f g (F "#" [lab,t])         = do a <- f g t; k <- searchL lab
   				            Just $ alg.box k a
           f g (F "><" [lab,t])        = do a <- f g t; k <- searchL lab
					    Just $ alg.aid k a	
           f g (F "##" [lab,t])        = do a <- f g t; k <- searchL lab
   				            Just $ alg.xob k a
           f g (F ('M':'U':' ':x) [t]) = fixptM subset (step g x t) alg.false
           f g (F ('N':'U':' ':x) [t]) = fixptM supset (step g x t) alg.true
           f _ (F "$" [at,lab])        = do i <- searchA at; k <- searchL lab
  				            Just $ sig.valueL!!i!!k
           f _ at                      = do i <- searchA at; Just $ sig.value!!i
       				 
           searchA,searchL :: Term String -> Maybe Int
           searchA at  = search (== at) sig.atoms
           searchL lab = search (== lab) sig.labels
   
           step :: (String -> States) -> String -> TermS -> [Int] -> States
           step g x t a = f (upd g x $ Just a) t

-- partial evaluation

 evaluate :: Sig -> TermS -> TermS
 evaluate sig = eval
    where eval (F "==>" [t,u])               = mkImpl (eval t) $ eval u
	  eval (F "<==>" [t,u])              = F "<==>" [eval t,eval u]
	  eval (F "===>" [t,u])              = mkImpl (eval t) $ eval u
	  eval (F "<===" [t,u])              = mkImpl (eval u) $ eval t
	  eval (F "&" ts)      	             = mkConjunct $ map eval ts
	  eval (F "|" ts)                    = mkDisjunct $ map eval ts
	  eval (F ('A':'n':'y':x) [t])       = evalQ mkAny x t
 	  eval (F ('A':'l':'l':x) [t])       = evalQ mkAll x t 
	  eval (F "Not" [t])                 = mkNot sig $ eval t
	  eval (F "id" [t])		     = eval t
	  eval (F "ite" [t,u,v]) | isTrue t  = eval u
	  			 | isFalse t = eval v
	  eval (F x p@[_,_])     | isOrd x && just ip = mkRel x $ get ip
                                 | isOrd x && just rp = mkRel x $ get rp
			         | isOrd x && just fp = mkRel x $ get fp
			         | isOrd x && just sp = mkRel x $ get sp 
			                   where ip = mapM intAlg.parseA p
	                                         rp = mapM realAlg.parseA p 
	                                         fp = mapM linAlg.parseA p
	                                         sp = mapM (relAlg sig).parseA p
	  eval (F "=" [t,u])   = evalEq t u
	  eval (F "=/=" [t,u]) = evalNeq t u
	  eval (F "<+>" ts)    = mkSum $ map eval ts
 	  eval t | just i = mkConst $ get i
 		 | just r = mkConst $ get r
		 | just f = mkLin $ get f
		 | just s = mkList $ map (uncurry mkPair) $ sort (<) $ get s
		   	    where i = foldArith intAlg t
			          r = foldArith realAlg t
			          f = foldArith linAlg t
			          s = foldArith (relAlg sig) t
          eval (F x ts)   = F x $ map eval ts
	  eval t          = t
	  
	  evalQ f x t = f (words x `meet` frees sig t) $ eval t
	  
	  evalEq t u | eqTerm t u = mkTrue
	  evalEq (F "{}" ts) (F "{}" us)
	  	   | just qs && just rs = mkConst $ eqSet (==) (get qs) $ get rs
	  		                  where qs = mapM parseReal ts
					  	rs = mapM parseReal us
	  evalEq t (F "suc" [u]) | just n = evalEq (mkConst $ get n-1) u
 		 			    where n = parsePnat t
	  evalEq (F "suc" [u]) t | just n = evalEq u (mkConst $ get n-1)
 		 			    where n = parsePnat t
	  evalEq (F "[]" (t:ts)) (F ":" [u,v]) = mkConjunct 
	  				       [evalEq t u,evalEq (mkList ts) v]
	  evalEq (F ":" [u,v]) (F "[]" (t:ts)) = mkConjunct 
	  				       [evalEq u t,evalEq v $ mkList ts]
          evalEq (F x ts) (F y us) | all sig.isConstruct [x,y] =
	                                if x == y && length ts == length us 
		                        then mkConjunct $ zipWith (evalEq) ts us
				        else mkFalse
	  evalEq t u = mkEq (eval t) $ eval u
				    
	  evalNeq t u | eqTerm t u = mkFalse
	  evalNeq (F "{}" ts) (F "{}" us)
	     | just qs && just rs = mkConst $ not $ eqSet (==) (get qs) $ get rs
	  		            where qs = mapM parseReal ts
					  rs = mapM parseReal us
	  evalNeq t (F "suc" [u]) | just n = evalNeq (mkConst $ get n-1) u
 		 			     where n = parsePnat t
	  evalNeq (F "suc" [u]) t | just n = evalNeq u (mkConst $ get n-1)
 		 			     where n = parsePnat t
          evalNeq (F "[]" (t:ts)) (F ":" [u,v]) = 
 			          mkDisjunct [evalNeq t u,evalNeq (mkList ts) v]
          evalNeq (F ":" [u,v]) (F "[]" (t:ts)) = 
 			          mkDisjunct [evalNeq u t,evalNeq v $ mkList ts]
          evalNeq (F x ts) (F y us) | all sig.isConstruct [x,y] =
	                               if x == y && length ts == length us 
		                       then mkDisjunct $ zipWith (evalNeq) ts us
				       else mkTrue
	  evalNeq t u = mkNeq (eval t) $ eval u

-- FLOWGRAPH ANALYSIS

 data Flow a = In (Flow a) a | Assign String TermS (Flow a) a | 
 	       Ite TermS (Flow a) (Flow a) a | 
 	       Fork [Flow a] a | Atom a | Neg (Flow a) a | 
 	       Comb Bool [Flow a] a | Mop Bool TermS (Flow a) a | 
 	       Fix Bool (Flow a) a | Pointer [Int]
 
 parseFlow :: Bool -> Sig -> TermS -> (TermS -> Maybe a) -> Maybe (Flow a)
 
 parseFlow True sig t parseVal = f t where
      f (F x [u]) | take 8 x == "inflow::"
      		    = do g <- f u; val <- dropVal 8 x; Just $ In g val
      f (F x [V z,e,u]) | take 8 x == "assign::"
      		    = do g <- f u; val <- dropVal 8 x; Just $ Assign z e g val
      f (F x [c,u1,u2]) | take 5 x == "ite::"
      		    = do g1 <- f u1; g2 <- f u2; val <- dropVal 5 x
      		         Just $ Ite c g1 g2 val
      f (F x ts)  | take 6 x == "fork::"
      	            = do gs <- mapM f ts; val <- dropVal 6 x; Just $ Fork gs val
      f (F x [u]) | take 5 x == "not::"
      		    = do g <- f u; val <- dropVal 5 x; Just $ Neg g val
      f (F x ts)  | take 4 x == "\\/::" 
      		    = do gs <- mapM f ts; val <- dropVal 4 x
      		         Just $ Comb True gs val
      f (F x ts)  | take 4 x == "/\\::" 
      		    = do gs <- mapM f ts; val <- dropVal 4 x
      		         Just $ Comb False gs val
      f (F x [u]) | take 4 x == "EX::" 
      		    = do g <- f u; val <- dropVal 4 x
      		    	 Just $ Mop True (leaf "") g val
      f (F x [u]) | take 4 x == "AX::" 
      		    = do g <- f u; val <- dropVal 4 x
      		         Just $ Mop False (leaf "") g val
      f (F x [lab,u]) | take 4 x == "<>::" 
      		    = do g <- f u;  guard $ lab `elem` sig.labels
      		         val <- dropVal 4 x; Just $ Mop True lab g val
      f (F x [lab,u]) | take 3 x == "#::" 
      		    = do g <- f u;  guard $ lab `elem` sig.labels
      		         val <- dropVal 3 x; Just $ Mop False lab g val
      f (F x [u])       | take 4 x == "MU::"
      		    = do g <- f u; val <- dropVal 4 x; Just $ Fix True g val
      f (F x [u])       | take 4 x == "NU::" 
      		    = do g <- f u; val <- dropVal 4 x; Just $ Fix False g val
      f (V x)           | isPos x  
                    = do guard $ q `elem` positions t
			 Just $ Pointer q where q = getPos x
      f val         = do val <- parseVal val; Just $ Atom val
      dropVal n x   = do val <- parse (term sig) $ drop n x; parseVal val
      
 parseFlow _ sig t parseVal = f t where
      f (F "inflow" [u,val])        = do g <- f u; val <- parseVal val
    				         Just $ In g val
      f (F "assign" [V x,e,u,val])  = do g <- f u; val <- parseVal val
                                         Just $ Assign x e g val
      f (F "ite" [c,u1,u2,val])     = do g1 <- f u1; g2 <- f u2
					 val <- parseVal val
					 Just $ Ite c g1 g2 val
      f (F "fork" ts@(_:_))         = do gs <- mapM f $ init ts
		                         val <- parseVal $ last ts
		                         Just $ Fork gs val
      f (F "not" [u,val])           = do g <- f u; val <- parseVal val
	 				 Just $ Neg g val
      f (F "\\/" ts@(_:_))          = do gs <- mapM f $ init ts
	   				 val <- parseVal $ last ts
	   				 Just $ Comb True gs val
      f (F "/\\" ts@(_:_))          = do gs <- mapM f $ init ts
           			         val <- parseVal $ last ts
           			         Just $ Comb False gs val
      f (F "EX" [lab,u,val])        = do g <- f u; val <- parseVal val
					 Just $ Mop True (leaf "") g val
      f (F "AX" [lab,u,val])        = do g <- f u; val <- parseVal val
					 Just $ Mop False (leaf "") g val
      f (F "<>" [lab,u,val])        = do g <- f u; guard $ lab `elem` sig.labels
			                 val <- parseVal val
					 Just $ Mop True lab g val
      f (F "#" [lab,u,val])         = do g <- f u; guard $ lab `elem` sig.labels
				         val <- parseVal val
					 Just $ Mop False lab g val
      f (F "MU" [u,val])            = do g <- f u; val <- parseVal val
					 Just $ Fix True g val
      f (F "NU" [u,val])            = do g <- f u; val <- parseVal val
					 Just $ Fix False g val
      f (V x) | isPos x             = do guard $ q `elem` positions t
				         Just $ Pointer q where q = getPos x
      f val                         = do val <- parseVal val; Just $ Atom val
                          
 parseVal sig (t@(F "()" [])) = Just t
 parseVal sig t = do F "[]" ts <- Just t; guard $ ts `subset` sig.states; Just t 

-- mkFlow computes the term representation of a flow graph

 mkFlow :: Bool -> Sig -> Flow a -> (a -> TermS) -> TermS
 mkFlow b sig flow mkVal = f flow
        where f (In g val)                 = h "inflow" val [f g]
	      f (Assign x e g val)         = h "assign" val [V x,e,f g]
	      f (Ite c g1 g2 val)          = h "ite" val [c,f g1,f g2]
	      f (Fork gs val)              = h "fork" val $ map f gs
	      f (Atom val)	           = mkVal val
	      f (Neg g val)                = h "not" val [f g]
	      f (Comb True gs val)         = h "\\/" val $ map f gs
	      f (Comb _ gs val)            = h "/\\" val $ map f gs
	      f (Mop True (F "" []) g val) = h "EX" val [f g]
	      f (Mop _ (F "" []) g val)    = h "AX" val [f g]
	      f (Mop True lab g val)       = h "<>" val [lab,f g]
	      f (Mop _ lab g val)   	   = h "#" val [lab,f g]
	      f (Fix True g val)     	   = h "MU" val [f g]
	      f (Fix _ g val)              = h "NU" val [f g]
	      f (Pointer p)	     	   = mkPos p
	      h op val ts = if b then F (op ++ "::" ++ showTerm0 (mkVal val)) ts
	      			 else F op $ ts ++ [mkVal val]
 
-- getVal g p returns the value at the first non-pointer position of g that is
-- reachable from p.

 getVal :: Flow a -> [Int] -> (a,[Int])
 getVal f p = h f p p where h (In g _) (0:p) q         = h g p q
		            h (In _ val) _ q           = (val,q)
		            h (Atom val) _ q 	       = (val,q)
		            h (Assign _ _ g _) (2:p) q = h g p q
		            h (Assign _ _ _ val) _ q   = (val,q)
		            h (Ite _ g _ _) (1:p) q    = h g p q
			    h (Ite _ _ g _) (2:p) q    = h g p q
			    h (Ite _ _ _ val) _ q      = (val,q)
			    h (Fork gs _) p@(_:_) q    = h' gs p q
			    h (Fork _ val) _ q	       = (val,q)
			    h (Neg g _) (0:p) q        = h g p q
			    h (Neg _ val) _ q	       = (val,q)
			    h (Comb _ gs _) p@(_:_) q  = h' gs p q
			    h (Comb _ _ val) _ q       = (val,q)
			    h (Mop _ _ g _) (1:p) q    = h g p q
			    h (Mop _ _ _ val) _ q      = (val,q)
			    h (Fix _ g _) (0:p) q      = h g p q
			    h (Fix _ _ val) _ q	       = (val,q)
			    h (Pointer p) _ _          = h f p p
			    h' (g:_)  (0:p) = h g p
			    h' (_:gs) (n:p) = h' gs ((n-1):p)
	 
-- global model checking of state formulas (backward analysis)

 initStates :: Bool -> Sig -> TermS -> Maybe TermS
 
 initStates True sig = f
      where sts = mkList sig.states
            f (t@(V x))	         = do guard $ isPos x; Just t
            f (F "true" [])      = Just sts
            f (F "false" [])     = Just mkMt
            f (F "`then`" [t,u]) = f (F "\\/" [F "not" [t],u])
            f (F "MU" [t])       = do t <- f t; jOP "MU" "[]" [t]
            f (F "NU" [t])       = do t <- f t; jOP "NU" (showTerm0 sts) [t]
            f (F "EX" [t])       = do t <- f t; jOP "EX" "()" [t]
            f (F "AX" [t])       = do t <- f t; jOP "AX" "()" [t]
            f (F "<>" [lab,t])   = do p lab; t <- f t; jOP "<>" "()" [lab,t]
            f (F "#" [lab,t])    = do p lab; t <- f t; jOP "#" "()" [lab,t]
            f (F "not" [t])      = do t <- f t; jOP "not" "()" $ [t]
            f (F "\\/" ts)       = do ts <- mapM f ts; jOP "\\/" "()" ts
            f (F "/\\" ts)       = do ts <- mapM f ts; jOP "/\\" "()" ts
            f at                 = do i <- search (== at) sig.atoms
	                              Just $ mkList $ map (sig.states!!) 
	                             		    $ sig.value!!i
	    p lab = guard $ just $ search (== lab) sig.labels
	    jOP op val = Just . F (op++"::"++val)
	    
 initStates _ sig = f
      where sts = mkList sig.states
            f (t@(V x))	         = do guard $ isPos x; Just t
            f (F "true" [])      = Just sts
            f (F "false" [])     = Just mkMt
            f (F "`then`" [t,u]) = f (F "\\/" [F "not" [t],u])
            f (F "MU" [t])       = do t <- f t; jOP "MU" [t,mkMt]
            f (F "NU" [t])       = do t <- f t; jOP "NU" [t,sts]
            f (F "EX" [t])  	 = do t <- f t; jOP "EX" [t,unit]
            f (F "AX" [t])       = do t <- f t; jOP "AX" [t,unit]
            f (F "<>" [lab,t])   = do p lab; t <- f t; jOP "<>" [lab,t,unit]
            f (F "#" [lab,t])    = do p lab; t <- f t; jOP "#" [lab,t,unit]
            f (F "not" [t])      = do t <- f t; jOP "not" $ [t,unit]
            f (F "\\/" ts)       = do ts <- mapM f ts; jOP "\\/" $ ts++[unit]
            f (F "/\\" ts)       = do ts <- mapM f ts; jOP "/\\" $ ts++[unit]
            f at                 = do i <- search (== at) sig.atoms
	                              Just $ mkList $ map (sig.states!!) 
	                             		    $ sig.value!!i
	    p lab = guard $ just $ search (== lab) sig.labels
	    jOP op = Just . F op

 evalStates :: Sig -> TermS -> Flow TermS -> (Flow TermS,Bool)
 evalStates sig t flow = up [] flow
  where ps = maxis (<<=) $ fixPositions t
        up p (Neg g val) 
             | val1 == unit || any (p<<) ps = (Neg g1 val, b)
	     | True = if null ps then (Atom val2, True)
	     			 else (Neg g1 val2, b || val /= val2)
		          where q = p++[0]; (g1,b) = up q g
			        val1 = fst $ getVal flow q
		                val2 = mkList $ minus sig.states $ subterms val1
        up p (Comb c gs val)
             | any (== unit) vals || any (p<<) ps = (Comb c gs1 val, or bs)
 	     | True = if null ps then (Atom val1, True)
	     			 else (Comb c gs1 val1, or bs || val /= val1)
		      where qs = succsInd p gs
		            (gs1,bs) = unzip $ zipWith up qs gs
			    vals = map (fst . getVal flow) qs
			    val1 = mkList $ foldl1 (if c then join else meet) 
			    		  $ map subterms vals
        up p (Mop c lab g val)
             | val1 == unit || any (p<<) ps = (Mop c lab g1 val, b)
	     | True = if null ps then (Atom val2, True)
	     			 else (Mop c lab g1 val2, b || val /= val2)
		      where q = p++[1]; (g1,b) = up q g
			    val1 = fst $ getVal flow q
			    f True = shares; f _ = flip subset
			    val2 = mkList $ filter (f c (subterms val1) . h) 
			    		    sig.states
			    h st = map (sig.states!!) $ tr i
				   where i = get $ search (== st) sig.states
			    tr i = if lab == leaf "" then sig.trans!!i
		                   else sig.transL!!i!!j
		                   where j = get $ search (== lab) sig.labels
        up p (Fix c g val)
             | val1 == unit || any (p<<) ps = (Fix c g1 val, b)
             | True = if f c subset valL val1L 
	              then if not b && p `elem` ps then (Atom val, True)
		      			           else (Fix c g1 val, b)
			   else (Fix c g1 $ mkList $ h c valL val1L, True)
		      where f True = flip; f _ = id
		     	    h True = join; h _ = meet
		     	    q = p++[0]; (g1,b) = up q g
			    val1 = fst $ getVal flow q
			    valL = subterms val; val1L = subterms val1
	up _ g = (g,False)
 
-- mkCycles replaces the bounded variables of fixpoint subformulas by back 
-- pointers to their bodies. Alternating fixpoints may lead to wrong results.

 mkCycles :: [Int] -> TermS -> TermS
 mkCycles p t@(F x _) | isFixF x = addToPoss p $ eqsToGraph [0] eqs 
 				   where (_,eqs,_) = fixToEqs t 0
 mkCycles p (F x ts)             = F x $ zipWithSucs mkCycles p ts
 
-- used by Esolve > simplifyS stateflow
		   
 fixToEqs :: TermS -> Int -> (TermS,[IterEq],Int)
 fixToEqs (F x [t]) n | isFixF x = (F z [],Equal z (F mu [u]):eqs,k)
 			    where mu:y:_ = words x
				  b = y `elem` foldT f t
				  f x xss = if isFixF x then joinM xss `join1` y
				                        else joinM xss
					    where _:y:_ = words x
				  z = if b then y++show n else y
				  (u,eqs,k) = if b then fixToEqs (t>>>g) $ n+1
					           else fixToEqs t n
				  g = F z [] `for` y
 fixToEqs (F x ts) n = (F x us,eqs,k)
 		       where (us,eqs,k) = f ts n
                             f (t:ts) n = (u:us,eqs++eqs',k')  
                                          where (u,eqs,k) = fixToEqs t n
				                (us,eqs',k') = f ts k
			     f _ n      = ([],[],n)
 fixToEqs t n        = (t,[],n)

-- verification of postconditions (backward-all analysis)
 
 initPost :: TermS -> TermS
 initPost (F op ts) | op `elem` words "assign fork ite"
 	    = F (op++"::bool(True)") $ map initPost ts
 initPost t = t
 
 initPost' :: TermS -> TermS
 initPost' (F x ts) | x `elem` words "assign fork ite"
 	     = F x $ map initPost ts++[F "bool" [mkTrue]]
 initPost' t = t

 evalPost :: (TermS -> TermS) -> Flow TermS -> (Flow TermS,Bool)
 evalPost simplify flow = up [] flow where
                  up p (Assign x e g val) = (Assign x e g1 val2, b1 || b2)
	  	             where q = p++[2]
		                   (g1,b1) = up q g
			           val1 = f q
				   (val2,b2) = mkMeet val $ val1>>>(e`for`x)
                  up p (Ite c g1 g2 val) = (Ite c g3 g4 val3, b1 || b2 || b3)
	                     where q1 = p++[1]; q2 = p++[2]
		                   (g3,b1) = up q1 g1; (g4,b2) = up q2 g2
			           val1 = f q1; val2 = f q2
			           c1 = mkDisjunct [c,val1]
			           c2 = mkDisjunct [F "Not" [c],val2]
				   (val3,b3) = mkMeet val $ mkConjunct [c1,c2]
                  up p (Fork gs val) = (Fork gs1 val1, or bs || b)
 		             where qs = succsInd p gs
		                   (gs1,bs) = unzip $ zipWith up qs gs
		                   (val1,b) = mkMeet val $ mkConjunct $ map f qs
                  up _ g = (g,False)
	          f = fst . getVal flow
                  mkMeet val val' = if isTrue $ simplify $ mkImpl val val'
	                            then (val,False) 
	                            else (simplify $ mkConjunct [val,val'],True)

-- computation of program states (forward-any analysis)

 initSubs :: TermS -> TermS
 initSubs = f 
      where f (F "inflow" [c,val]) = h "inflow" (showTerm0 val) [initSubs c]
            f (F "assign" [x,e,c]) = h "assign" "[[]]" [x,e,initSubs c] 
            f (F "ite" [b,c,c'])   = h "ite" "[]" [b,initSubs c,initSubs c'] 
            f (F "fork" ts)        = h "fork" "[]" $ map initSubs ts
            f t = t
            h op val = F $ op++"::"++val

 initSubs' :: TermS -> TermS
 initSubs' = f 
      where f (F "inflow" [c,val]) = h "inflow" val [initSubs c]
            f (F "assign" [x,e,c]) = h "assign" (mkList [mkMt]) [x,e,initSubs c] 
            f (F "ite" [b,c,c'])   = h "ite" mkMt [b,initSubs c,initSubs c'] 
            f (F "fork" ts)        = h "fork" mkMt $ map initSubs ts
            f t = t
            h op val ts = F op $ ts++[val]

 evalSubs :: (TermS -> TermS) -> Flow [SubstS] -> [String] 
 					       -> (Flow [SubstS],Bool)
 evalSubs simplify flow xs = down flow False [] flow
   where down flow b p (In g val)         = down flow1 (b || b1) q g
 	      	              where q = p++[0]
		                    (flow1,b1) = change flow q val
         down flow b p (Assign x e g val) = down flow1 (b || b1) q g
	      	              where q = p++[2]
		                    (flow1,b1) = change flow q $ map f val 
		   	            f state = upd state x $ simplify $ e>>>state
         down flow b p (Ite c g1 g2 val) = down flow3 b3 q2 g2
	           where q1 = p++[1]; q2 = p++[2]
		   	 (flow1,b1) = change flow q1 $ filter (isTrue . f) val
		   	 (flow2,b2) = change flow1 q2 $ filter (isFalse . f) val
		   	 f = simplify . (c>>>)
	           	 (flow3,b3) = down flow2 (b || b1 || b2) q1 g1
         down flow b p (Fork gs val) = fold2 h (foldl f (flow,b) qs) qs gs
 	                   where qs = succsInd p gs
		                 f (flow,b) q = (flow1, b || b1) where
			                        (flow1,b1) = change flow q val
 		                 h (flow,b) q g = down flow b q g
         down flow b _ _ = (flow,b)
         change g p val = if subSubs xs val oldVal then (g,False) 
	                                           else (rep g q,True)
                   where (oldVal,q) = getVal g p
		         newVal = joinSubs xs val oldVal
		         rep (In g val) (0:p)         = In (rep g p) val
 	                 rep (Atom _) _               = Atom newVal
  	                 rep (Assign x e g val) (2:p) = Assign x e (rep g p) val
 	                 rep (Assign x e g _) _       = Assign x e g newVal
 	 		 rep (Ite c g1 g2 val) (1:p)  = Ite c (rep g1 p) g2 val
 	                 rep (Ite c g1 g2 val) (2:p)  = Ite c g1 (rep g2 p) val
 	 	         rep (Ite c g1 g2 _) _        = Ite c g1 g2 newVal
 	 		 rep (Fork gs val) p@(_:_)    = Fork (rep' gs p) val
 	 		 rep (Fork gs _) _            = Fork gs newVal
	                 rep' (g:gs) (0:p)            = rep g p:gs
 	 		 rep' (g:gs) (n:p)            = g:rep' gs ((n-1):p)

-- SUBSUMPTION

 subsumes sig = h
   where h t u             | eqTerm t u        = True
         h (F "Not" [t]) (F "Not" [u])         = h u t
	 h (F "==>" [t,u]) (F "==>" [v,w])     = h v t && h u w
         h t (F "|" us)    | any (h t) us      = True
         h (F "|" ts) u    | all (flip h u) ts = True 
	 h t (F "&" us)    | all (h t) us      = True
         h (F "&" ts) u    | any (flip h u) ts = True 
         h (F ('A':'n':'y':x) [t]) (F ('A':'n':'y':y) [u]) 
	            | perm (words x) ys ys t u = True where ys = words y
         h (F ('A':'l':'l':x) [t]) (F ('A':'l':'l':y) [u]) 
	            | perm (words x) ys ys t u = True where ys = words y
         h (F ('A':'n':'y':x) [t]) u | noFrees (words x) u && h t u = True
         h t (F ('A':'l':'l':x) [u]) | noFrees (words x) t && h t u = True
         h t (F ('A':'n':'y':x) [u]) | g x u t = True
         h (F ('A':'l':'l':x) [t]) u | g x t u = True
         h (F "&" ts) (F ('A':'n':'y':x) [u])  | any (g x u) $ sub mkConjunct ts  
	 				       = True
         h (F ('A':'l':'l':x) [t]) (F "|" us)  = any (g x t) $ sub mkDisjunct us
	 h _ _                                 = False
	 g x t u                               = just $ match sig (words x) u t
	 perm xs ys zs t u = h (renameFree sig (fold2 upd id xs zs) t) u ||
		             ys /= zs' && perm xs ys zs' t u
			     where zs' = nextPerm zs
         sub f ts = [f $ map (ts!!) ns | ns <- subsets $ length ts]
	 noFrees xs = disjoint xs . frees sig

-- used by Esolve > simplifyF, implToConc and Ecom > subsumeSubtrees
	 
-- SIMPLIFICATION

 simplifyIter sig = pr1 . simplifyLoop sig 100 PA

 sapply sig t  = simplifyIter sig . apply t
 
 sapplyL sig t = simplifyIter sig . applyL t
   
 simplifytoList sig x = case simplifyIter sig $ leaf x of F "[]" ts -> ts
 							  _ -> []

 reduceString :: Sig -> String -> Maybe String
 reduceString sig t = do t <- parse (term sig) t
	  	         Just $ showTerm0 $ simplifyIter sig t

 applyDrawFun :: Sig -> String -> String -> [TermS] -> [TermS]
 applyDrawFun _ "" _	    = id 
 applyDrawFun _ "title" ""  = id 
 applyDrawFun _ "title" str = map $ F "<+>" . add . addToPoss [1] 
    where add t = [path "widg blue $" [path "frame 5" [],F "text" [leaf str]],t]
          path str ts = f $ words str where f [x]    = F x ts
 		                            f (x:xs) = F x [f xs]

 applyDrawFun sig draw _ | just t = map $ wtree . simplifyIter sig
 					        . apply (get t) . addToPoss [0]
      where t = parse (term sig) draw
            wtree (F "$" [F "$" [F "wtree" [m],f],t]) | just m' = g t pt
	       where m' = parsePnat m
	             order = case get m' of 1 -> levelTerm; 2 -> preordTerm
	   			            3 -> heapTerm;  _ -> hillTerm
	             (pt,n) = order black (const id) t
	             h t k = sapplyL sig f [t,mkConst k,mkConst n]
	             g (F x ts) (F k us) | just u = F "widg" $ vs++[h (get u) k] 
		       		         | True   = F x vs
					            where vs = zipWith g ts us
					                  u = parse (term sig) x
                     g t@(V x) (F k _)   | isPos x = mkPos $ tail $ getPos x
                                         | True    = F "widg" [h t k]
                     g t _ = t
            wtree (F "$" [F "wtree" [f],t]) = g t
              where g (F x ts) | just u  = F "widg" $ vs++[sapply sig f $ get u]
                   	       | True    = F x vs where vs = map g ts
					  	        u = parse (term sig) x
	            g t@(V x)  | isPos x = mkPos $ tail $ getPos x
	                       | True    = F "widg" [sapply sig f t]
                    g t = t
            wtree t = simplifyIter sig t
 
-- used by Ecom > showSubtreePicts,showTreePicts,showTransOrKripke
			 
-- wtree(f)(t) replaces each subgraph x(t1,..,tn) of t by the subgraph 
-- widg(t1,...,tn,f(x)).

-- wtree(m,f)(t) replaces each subtree x(t1,...,tn) of t by the subtree 
-- widg(t1,...,tn,f(x,k,max)) where k is the position of x within t with respect
-- to level order (m=1), prefix order (m=2), heap order (m=3) or hill order 
-- (m>3) and max is the maximum of positions of t.

-- If the interpreter widgetTree is applied to the resulting term, node x is 
-- replaced by the widget f(x) resp. f(x,k,max).

-- simplifyLoop sig limit strat t applies simplification rules at most limit 
-- times to the maximal subtrees of t and returns the simplified tree together  
-- with the number of actually applied steps. 

 simplifyLoop :: Sig -> Int -> Strategy -> TermS -> (TermS,Int,Bool)
 simplifyLoop sig limit strat t = loop 0 limit [t] 
    where loop k m ts = if m == 0 then (redex,k,False)
                        else case step strat (simplifyOne sig) redex of 
                             Just t -> loop' t
                             _ -> case step DF (expandFix sig True []) redex of 
                                       Just t -> loop' t
                                       _ -> (redex,k,False)
	                where redex = head ts
	                      loop' t = if just $ search (== t) ts 
	                                then (t,k,True)
	                                else loop (k+1) (m-1) $ t:ts
	              		    
-- used by Esolve > simplifyIter and Ecom > simplify',simplifySubtree

 step :: Strategy -> (TermS -> [Int] -> Maybe TermS) -> TermS -> Maybe TermS
 step strat simplify t = 
     do guard $ isF t
        case strat of DF -> modifyDF [] t 			-- depthfirst
  	              BF -> modifyBF [[]] [t]	        	-- breadthfirst
  	              _  -> do let u = modifyPA t [] t	
  			       guard $ t /= u; Just u		-- parallel
     where modifyDF p u   = concat $ simplify t p:zipWithSucs modifyDF p us
	 		    where us = subterms u
           modifyBF ps us = do guard $ notnull ps
                    	       concat (map (simplify t) ps) ++ modifyBF qs vs
		            where (qs,vs) = unzip $ concat 
		   			          $ zipWith (zipWithSucs (,)) ps
		   			          $ map subterms us
           modifyPA t p u = case simplify t p of
                                 Just t -> t
                                 _ -> fold2 modifyPA t (succsInd p us) us
                            where us = subterms u
 
-- simplifyOne sig t p applies the first applicable simplification rule at 
-- position p of t.

 simplifyOne :: Sig -> TermS -> [Int] -> Maybe TermS
 simplifyOne sig t p = concat 
                       [do guard $ null (subterms reduct0) && reduct0 /= redex0
                           f $ reduct0,
                        do guard $ redex1 /= reduct1
                           f $ chgTargets reduct1,
                        do guard $ polarity True t p
                           reduct <- simplCoInd sig redex1
                           f $ chgTargets reduct,
                        do F "$" [V x,u] <- Just $ getSubterm t p
                           guard $ isPos x && root u /= "$"
                           let q = p++[0]
                               t' = dereference t [q]
                           guard $ not $ isFix $ label t' q
                           Just t', 
                        do reduct <- simplifyF sig redex0
                           guard $ null $ subterms reduct
                           f reduct,
                        do reduct <- simplifyF sig redex1
                           if pointers reduct `subset` allPoss reduct 
                              && andT (/= "hidden") reduct  -- see dropFromPossM
                              then f $ chgTargets reduct
                              else Just $ replace t p $ expand 0 t p]
                       where redex0  = mapT block $ expand0 t p
                             reduct0 = evaluate sig redex0
                             redex1  = addTargets (mapT block) t p
                             reduct1 = evaluate sig redex1
                             f = Just . mapT (unblock . delExt) . replace1 t p
                             block x = if sig.blocked x then "BLOCK"++x else x
                             unblock ('B':'L':'O':'C':'K':x) = x
                             unblock x = x

-- used by Esolve > simplifyLoop and Ecom > simplifyPar
 
-- expandFix sig t p expands the fixpoint formula of t at position p or pointed
-- to from position p. After the expansion, all copies of the fixpoint formula 
-- in the reduct are merged into a single one.

 expandFix :: Sig -> Bool -> [Int] -> TermS -> [Int] -> Maybe TermS
 expandFix sig fixRef muPos t p = 
                     concat [do F mu [body] <- Just redex
                                f mu redex body True,
 	                     do F proj [redex1@(F mu [F "()" ts])] <- Just redex
                                let i = parse (strNat "get") proj
                                    j = get i
                                f mu redex1 (ts!!j) $ just i && j < length ts,
                             do guard fixRef
                                concat [do V x <- Just $ getSubterm t p
                                           guard $ isPos x
                                           let muPos = getPos x
                                               u = dereference t [p]
                                           guard $ b $ label t muPos
                                           expandFix sig False muPos u p, 
 	                                do F proj [V x] <- Just $ getSubterm t p
 	                                   guard $ isPos x
 	                                   let muPos = getPos x
                                               i = parse (strNat "get") proj
                                               u = dereference t [p++[0]]
                                           guard $ (b $ label t muPos) && just i
                                           expandFix sig False muPos u p]]
         where b mu = isFix mu && not (sig.blocked "fixes")
               redex = addTargets id t p
               recVars mu = do guard $ isFix mu && not (sig.blocked "fixes")
                               Just $ tail $ words mu
               f mu redex body b = do xs <- recVars mu
                                      let u = collapseVars sig xs body
                                          pairs = [(i,x) | i <- indices_ xs, 
                                         	           let x = xs!!i,
                                         	           x `elem` frees sig u]
                                          (is@(i:_),zs@(x:_)) = unzip pairs
                                          q:_ = cPositions (== x) u
                                          substitute = g (length xs) redex q i
                                          h = if null zs then V 
                                              else (map substitute is) `forL` zs
 	                              guard b
 	                              Just $ mapT delExt $ replace1 t p 
 	                                   $ subFix (chgTargets u) h
               g n redex q i k | n == 1 = t q
                               | True   = F ("get"++show k) [t $ q++[0]]
                                 where t q = if fixRef then if i == k then redex 
                                                            else mkPos q
                            		     else V $ 'e':mkPos0 muPos
                            		      
-- used by Esolve > simplifyLoop

 simplCoInd,simplifyF,simplifyA,simplifyS :: Sig -> TermS -> Maybe TermS

-- remove in/equational summands or factors

 simplCoInd sig t | not sig.safeEqs && just u = u where u = removeEq sig True t

-- fixpoint induction rules

 simplCoInd _ (F "<=" [F x [t],u]) 
     | leader x "mu" = Just $ F "<=" [t>>>forL us xs,u]
		       where _:xs = words x; us = mkGets xs u

 simplCoInd _ (F "<=" [F "$" [F x [t],arg],u])
     | leader x "mu" && null (subterms arg) =
 		             Just $ mkAll [root arg] 
		 	          $ F "<=" [apply (t>>>forL us xs) arg,u]
	        	     where _:xs = words x
		 		   us = mkGets xs $ F "fun" [arg,u]

 simplCoInd sig (F "`then`" [F x [t],u])  
     | leader x "MU" && monotone sig xs t =
   	                     Just $ F "`then`" [t>>>forL us xs,u]
			     where _:xs = words x; us = mkGets xs u

 simplCoInd sig (F "==>" [F "$" [F x [t],arg],u])  
     | leader x "MU" && null (subterms arg) && monotone sig xs t =
 		             Just $ mkAll [root arg] 
			          $ mkImpl (apply (t>>>forL us xs) arg) u
	                     where _:xs = words x
			           us = mkGets xs $ F "rel" [arg,u]

-- fixpoint coinduction rules

 simplCoInd _ (F "<=" [u,F x [t]]) 
     | leader x "nu" = Just $ F "<=" [t>>>forL us xs,u]
		       where _:xs = words x; us = mkGets xs u

 simplCoInd _ (F "<=" [u,F "$" [F x [t],arg]]) 
     | leader x "nu" && null (subterms arg) =
 		             Just $ mkAll [root arg] 
		 	          $ F "<=" [u,apply (t>>>forL us xs) arg]
	        	     where _:xs = words x
		 		   us = mkGets xs $ F "fun" [arg,u]

 simplCoInd sig (F "`then`" [u,F x [t]])  
     | leader x "NU" && monotone sig xs t =
     			     Just $ F "`then`" [u,t>>>forL us xs]
			     where _:xs = words x; us = mkGets xs u

 simplCoInd sig (F "==>" [u,F "$" [F x [t],arg]])  
     | leader x "NU" && null (subterms arg) && monotone sig xs t = 
 		             Just $ mkAll [root arg] 
			          $ mkImpl u $ apply (t>>>forL us xs) arg
  	                     where _:xs = words x
			           us = mkGets xs $ F "rel" [arg,u]

 simplCoInd _ _ = Nothing
	  
-- proper-graph producing rules	

 simplifyF _ (F "$" [F "mapG" [f@(F _ ts)],F x us@(_:_)]) | collector x = 
                                   Just $ F x $ if null ts then map (apply f) us
	                                        else map g $ indices_ us
	                           where g 0 = apply f $ vs!!0
				         g i = apply (mkPos [0,0]) $ vs!!i
				         vs = changeLPoss p q us
				         p i = [1,i]; q i = [i,1]

 simplifyF _ (F "$" [F "replicateG" [t],u]) | just n =  
 	            jList $ changePoss [1] [0] u:replicate (get n-1) (mkPos [0])
		    where n = parsePnat t
	    
 simplifyF _ (F "concat" [F "[]" ts]) | all isList ts =
                               jList $ concatMap (subterms . f) ts
		               where subs i = subterms $ ts!!i
		                     f t = foldl g t $ indices_ ts
		                     g t i = foldl (h i) t $ indices_ $ subs i 
		                     h i t k = changePoss [0,i,k] [lg (i-1)+k] t
		                     lg (-1) = 0
		                     lg i    = lg (i-1) + length (subs i)

 simplifyF sig (F "$" [F "concRepl" [t],u@(F "[]" us)]) | just n = 
 	            simplifyF sig $ F "concat" [addToPoss [0] $ mkList vs]
		    where n = parsePnat t
		          vs = changePoss [1] [0] u:replicate (get n-1) 
		          			  (mkList $ map f $ indices_ us)
		          f i = mkPos [0,i] 

 simplifyF _ (F "$" [F "**" [f,t],u]) | just n = 
		Just $ if null $ subterms f then iterate (apply f) v!!m
	               else apply first $ iterate (apply $ mkPos [0]) v!!(m-1)
	        where n = parsePnat t; m = get n               
		      first = changePoss [0,0] [0] f	-- collapse into first
		      v = changePoss [1] (replicate m 1) u 
             {-    ... else iterate (apply $ mkPos funpos) (apply last v)!!(m-1)
		where funpos = replicate (m-1) 1++[0]
		      last = changePoss [0,0] funpos f  -- collapse into last -}
							      
 simplifyF _ (F ":" [t,F "[]" ts]) = jList $ t:changeLPoss p q ts
 				     where p i = [1,i]; q i = [i+1] 

 simplifyF _ (F "$" [F x [n],F "[]" ts])
            | x `elem` words "cantor hilbert mirror snake transpose" && just k =
	                             jList $ f (get k) $ changeLPoss p single ts
		                     where k = parsePnat n
                                           f = case head x of 'c' -> cantorshelf
			       			              'h' -> hilbshelf
			       			              'm' -> mirror
			       			              's' -> snake
			       			              _ -> transpose
		                           p i = [1,i]
		                  
 simplifyF _ (F x [F "bool" [t],F "bool" [u]]) | isEq x 
 			  = Just $ if x == "=/=" then F "Not" [v] else v
		            where v = F "<==>" [changePoss [0,0] [0] t,
				   		changePoss [1,0] [1] u]
		
 simplifyF _ (F "permute" (t:ts)) | isObdd t =
    	         if n == 0 then Just t
	         else if null ts 
		      then perm [t,changePoss [0] [1] t,mkConsts [0..n-1]]
	              else do [u,ns] <- Just ts; ns <- parseNats ns
		              let permute s = map (s!!) ns
			          v = addToPoss [1] $ binsToObddP (n-1) ns 
					            $ map permute $ funToBins fn
 		              perm [t,if size v < size u then v else u,
		                    mkConsts $ nextPerm ns]
                 where fn@(_,n) = obddToFun $ drop0FromPoss t
	               perm = Just . F "permute"

-- rules preferred to user-defined rules

 simplifyF _ (F "++" [F x ts,F y us]) | collectors x y = Just $ F x $ ts++us
				       
 simplifyF _ (F "++" [F "++" [t,F x ts],F y us]) 
                               | collectors x y = Just $ F "++" [t,F x $ ts++us]
				       
 simplifyF _ (F "++" [F x ts,F "++" [F y us,t]]) 
                               | collectors x y = Just $ F "++" [F x $ ts++us,t]

-- user-defined rules

 simplifyF sig t | notnull ts = Just $ mkSum ts
 		                where ts = simplReducts sig False t

 simplifyF sig (F x ts@(_:_)) | notnull us =
 		                Just $ applyL (mkSum us) $ map (mapT f) ts
 		                where us = simplReducts sig False $ F x []
			              f x = if isPos x && notnull p 
				            then mkPos0 $ head p+1:tail p else x
				            where p = getPos x

 simplifyF sig (F "$" [t,u]) | notnull ts = Just $ apply (mkSum ts) u
 		                            where ts = simplReducts sig False t

-- reduce equivalences

 simplifyF sig (F "<==>" [t,u]) | isTrue t         = Just u
 			        | isTrue u    	   = Just t
				| isFalse t 	   = Just $ mkNot sig u
				| isFalse u        = Just $ mkNot sig t
				| subsumes sig t u = Just $ mkImpl u t
				| subsumes sig u t = Just $ mkImpl t u

-- try subsumption, reduce components and replace non-constructor-headed 
-- subterms by in/equivalent constructor-headed ones

 simplifyF sig (F "==>" [t,u]) 
  | subsumes sig t u = Just mkTrue
  | notnull vsfs     = Just mkTrue
  | notnull vsf      = Just $ mkImpl (mkConjunct tsf) $ mkConjunct usf1
  | notnull vss      = Just $ mkImpl (mkDisjunct tss1) $ mkDisjunct uss
  | notnull vssf     = Just $ F "&" [mkImpl (mkDisjunct tss2) $ mkConjunct vssf,
          	                     mkImpl (mkDisjunct vssf) $ mkConjunct usf2]	            
  | just pair1       = Just $ mkImpl (mkConjunct ts1) $ mkDisjunct us1
  | just pair2       = Just $ mkImpl (mkConjunct ts2) $ mkDisjunct us2
		       where tsf = mkFactors t; tss = mkSummands t
			     usf = mkFactors u; uss = mkSummands u
			     vsfs = meetTerm tsf uss
			     vsf = meetTerm tsf usf; usf1 = removeTerms usf vsf 
			     vss = meetTerm tss uss; tss1 = removeTerms tss vss
			     vssf = meetTerm tss usf
			     tss2 = removeTerms tss vssf
			     usf2 = removeTerms usf vssf
			     pair1 = replaceTerms sig "=" tsf uss
			     pair2 = replaceTerms sig "=/=" uss tsf
			     (ts1,us1) = get pair1
			     (us2,ts2) = get pair2

-- move quantifiers out of an implication

 simplifyF sig c@(F "==>" [t,u]) | b = Just result
            where (result,b) = moveAny [] t u False
                  moveAny zs (F ('A':'n':'y':x) [t]) u b = 
	                  moveAll (zs++map f xs) (renameAll f t) u True
		          where xs = words x
		                ys = sigVars sig u
		                f = getSubAwayFrom c xs ys
	          moveAny zs t u b = moveAll zs t u b
                  moveAll zs t (F ('A':'l':'l':x) [u]) b = 
	                  (mkAll (zs++map f xs) $ mkImpl t $ renameAll f u,True)
	                  where xs = words x
	                        ys = sigVars sig t
	                        f = getSubAwayFrom c xs ys
	          moveAll zs t u b = (mkAll zs $ mkImpl t u,b)

-- move universal quantifiers out of a disjunction

 simplifyF sig c@(F "|" ts) | b = Just result
            where (result,b) = move [] ts [] False
                  move zs (F ('A':'l':'l':x) [t]:ts) us b = 
	                          move (zs++map f xs) ts (renameAll f t:us) True
		                  where xs = words x
		                        ys = joinMap (sigVars sig) $ ts++us
		                        f = getSubAwayFrom c xs ys
	          move zs (t:ts) us b = move zs ts (t:us) b
		  move zs _ us b      = (mkAll zs $ mkDisjunct us,b)
				  
-- move existential quantifiers out of a conjunction

 simplifyF sig c@(F "&" ts) | b = Just result
            where (result,b) = move [] ts [] False
                  move zs (F ('A':'n':'y':x) [t]:ts) us b =
    		                  move (zs++map f xs) ts (renameAll f t:us) True
		                  where xs = words x
		                        ys = joinMap (sigVars sig) $ ts++us
		                        f = getSubAwayFrom c xs ys
                  move zs (t:ts) us b = move zs ts (t:us) b
                  move zs _ us b      = (mkAny zs $ mkConjunct us,b)

-- restrict a disjunction to its maximal summands with respect to subsumption 
-- and replace non-constructor-headed subterms by inequivalent 
-- constructor-headed ones

 simplifyF sig (F "|" ts) | just t = t
    			  | just p = Just $ mkDisjunct $ fst $ get p
			  	     where t = subsumeDisj sig ts []
					   p = replaceTerms sig "=/=" ts []

-- restrict a conjunction to its minimal factors with respect to subsumption and
-- replace non-constructor-headed subterms by equivalent constructor-headed 
-- ones

 simplifyF sig (F "&" ts) | just t = t
   		          | just p = Just $ mkConjunct $ fst $ get p
			  	     where t = subsumeConj sig ts []
					   p = replaceTerms sig "=" ts []
						    
 simplifyF _ (F "$" [F "any" [p],F x ts]) | collector x = 
 					    Just $ mkDisjunct $ map (apply p) ts
						    
 simplifyF _ (F "$" [F "all" [p],F x ts]) | collector x = 
 			 	            Just $ mkConjunct $ map (apply p) ts
					    
 simplifyF _ (F "$" [F "allany" [r],F "()" [F x ts,F y us]]) 
 	             | collector x && collector y = Just $ mkConjunct $ map q ts
	     		                   where q t = mkDisjunct $ map (f t) us
	     		                         f t u = applyL r [t,u]

-- modus ponens

 simplifyF _ (F "&" ts) | notnull is = Just $ mkConjunct $ map f $ indices_ ts
                                 where is = [i | i <- searchAll isImpl ts, 
					         let F _ [prem,_] = ts!!i,
					      	 prem `elem` context i ts]
			               f i = if i `elem` is then conc else ts!!i
					     where F _ [_,conc] = ts!!i

 simplifyF _ (F "$" [F "prodE" ts,u])  = Just $ mkTup $ map (flip apply u) ts
			   
 simplifyF _ (F "$" [F "/\\" [t,u],v]) = Just $ F "&" [apply t v,apply u v]
			   
 simplifyF _ (F "$" [F "\\/" [t,u],v]) = Just $ F "|" [apply t v,apply u v]

-- uncurrying: shift the premise of an implication from the conclusion to the
-- premise of an outer implication

 simplifyF _ (F "==>" [t,u])
  | getOp t `notElem` words "F FS `U` EF AF `EU` `AU`" && just i 
          = Just $ mkImpl (mkConjunct [t,prem]) $ mkDisjunct $ conc:context k ts
            where ts = mkSummands u; i = search isImpl ts
		  k = get i; F _ [prem,conc] = ts!!k 

-- remove in/equational summands or factors with a quantified variable on one 
-- side.

 simplifyF sig t | just u = u where u = removeEq sig False t
{-			   			   
 simplifyF _ (F "$" [F "\\/" ts,u]) = Just $ mkDisjunct $ map (flip apply u) ts
			   
 simplifyF _ (F "$" [F "/\\" ts,u]) = Just $ mkConjunct $ map (flip apply u) ts
			   
 simplifyF (F x ts) _ | x `elem` words "\\/ /\\" && notnull is =
 		          Just $ F x $ foldl f [] $ indices_ ts
			  where is = searchAll ((x==) . root) ts
			        f us i = if i `elem` is 
				         then subterms (ts!!i)++us else ts!!i:us
 
 simplifyF _ (F "$" [F "&" (t:ts),u]) = Just $ F "&" $ apply t u:ts
			   
 simplifyF _ (F "$" [F "|" ts,t])     = Just $ F "|" $ map (flip apply t) ts

-- distribute ==> over the factors of the conclusion of an implication

 simplifyF _ (F "==>" [t,c@(F "&" ts)])  
  | getOp t `notElem` words "F FS `U` EF AF `EU` `AU`"
                                    = Just $ mkConjunct $ map (mkImpl t) ts

-- distribute ==> over the summands the premise of an implication

 simplifyF _ (F "==>" [d@(F "|" ts),t])
  | getOp t `notElem` words "G GS `W` `R` -> EF AF `EW` `AW` `ER` `AR`" 
  				    = Just $ mkConjunct $ map (flip mkImpl t) ts
-}
 simplifyF sig t = simplifyA sig t
 
 removeEq sig unsafe t =
       case t of F ('A':'l':'l':x) [F "==>" [F "&" ts,u]] | just pair 
                   -> Just $ mkAll xs $ mkImpl (mkConjunct reds) $ u>>>g
 	                     where xs = words x
			           pair = f "=" xs ts; (reds,g) = get pair
	         F ('A':'l':'l':x) [F "==>" [t,u]] | just pair 
		   -> Just $ mkAll xs $ u>>>snd (get pair)
 	                     where xs = words x
			           pair = f "=" xs [t]
                 F ('A':'l':'l':x) [F "==>" [t,F "|" us]] | just pair 
		   -> Just $ mkAll xs $ mkImpl (t>>>g) $ mkDisjunct reds
 	        	     where xs = words x
		      	           pair = f "=/=" xs us; (reds,g) = get pair
                 F ('A':'l':'l':x) [F "==>" [t,u]] | just pair 
		   -> Just $ mkAll xs $ mkNot sig $ t>>>snd (get pair)
 	        	     where xs = words x
		      	           pair = f "=/=" xs [u]
                 F ('A':'l':'l':x) [F "|" ts] | just pair 
		   -> Just $ mkAll xs $ mkDisjunct $ fst $ get pair
                             where xs = words x
	                           pair = f "=/=" xs ts
                 F ('A':'l':'l':x) ts | just $ f "=/=" (words x) ts
		   -> Just mkFalse
                 F ('A':'n':'y':x) [F "&" ts] | just pair 
		   -> Just $ mkAny xs $ mkConjunct $ fst $ get pair
                             where xs = words x
	                           pair = f "=" xs ts
                 F ('A':'n':'y':x) ts | just $ f "=" (words x) ts -> Just mkTrue
		 _ -> Nothing
       where f rel xs = g [] 
               where g rest (t@(F z us@[l,r]):ts) 
	                       | z == rel && (unsafe || xs `shares` map root us)
      	                           = case unifyS sig xs l r of
		                     Just h -> Just (map (>>>h) $ ts++rest,h)
				     _ -> g (rest++[t]) ts
	             g rest (t:ts) = g (rest++[t]) ts
		     g _ _         = Nothing

-- replaceTerms sig rel ts vs replaces l in vs by r if ts contains the
-- atom (l rel r) or (r rel l). At first, atoms (t rel t) are removed.

 replaceTerms :: Sig -> String -> [TermS] -> [TermS] -> Maybe ([TermS],[TermS])
 replaceTerms sig rel ts vs = f [] ts
                where f us ts = do t:ts <- Just ts
      		                   let vs = us++ts
				       rest = f (t:us) ts
		                   case t of F x [l,r] | x == rel
			                       -> if l == r then f us ts 
				                  else h l r vs++h r l vs++rest
			                     _ -> rest
                      h l r ts = do guard $ not (isc l || isSubterm l r) && 
                                            isc r && (or bs || or cs)
		                    Just (F rel [l,r]:ts',vs')
                                 where (ts',bs) = unmap g ts
		                       (vs',cs) = unmap g vs
				       g t | t == l = (r,True)
				       g (F x ts)   = (F x us,or bs)
			                              where (us,bs) = unmap g ts
			               g t          = (t,False) 
                      isc = sig.isConstruct . root
		      unmap g = unzip . map g

-- subsume summands

 subsumeDisj sig (t:ts) rest = if subsumes sig t u then Just u
 			       else subsumeDisj sig ts $ rest++[t]
	                       where u = mkDisjunct $ ts++rest
 subsumeDisj _ _ _           = Nothing

-- subsume factors

 subsumeConj sig (t:ts) rest = if subsumes sig u t then Just u 
 			       else subsumeConj sig ts $ rest++[t]
	                       where u = mkConjunct $ ts++rest
 subsumeConj _ _ _           = Nothing

-- LAMBDA APPLICATIONS 
-- see: https://fldit-www.cs.uni-dortmund.de/~peter/CTL.pdf, page 137 ff.

 bodyAndSub sig lazy app pat body bodyPos arg = 
        if lazy then Just (body2,forL (map f xs) xs)
                else do sub <- match sig xs arg pat; Just (body2,del . sub)
        where xs = sigVars sig pat
              f x = apply (F "fun" [pat,mkVar sig x]) arg
              ps = map (bodyPos ++) $ refPositions body
              del = mapT $ delTar (`elem` (pointers body)) 
              body1 = getSubterm1 (dereference (del app) ps) bodyPos
              h = getSubAwayFrom body1 (bounded sig body1) $ frees sig $ del arg
              body2 = collapseVars sig xs $ renameBound sig h body1                  
                               
 simplifyA sig app@(F "$" [F x [F "~" [pat],body],arg]) 				
    | lambda x && just tsub = Just $ t>>>sub where
                              tsub = bodyAndSub sig True app pat body [0,1] arg
                              (t,sub) = get tsub
 				
 simplifyA sig app@(F "$" [F x [pat,body],arg]) 				
    | lambda x && just tsub = Just $ t>>>sub where
         	              tsub = bodyAndSub sig False app pat body [0,1] arg
         	              (t,sub) = get tsub

 simplifyA sig app@(F "$" [F x ts,arg]) 
    | lambda x && lg > 2 && even lg = Just $ F "CASE" $ pr1 
    					   $ fold2 f ([],0,1) pats bodies where
      lg = length ts; pats = evens ts; bodies = odds ts
      f (cases,i,k) pat body = if just t then (cases++[get t],i+1,k+2) 
                                         else (cases,i,k+2) where
        t = (do F "||" [pat,b] <- Just pat; result pat b) ++ result pat mkTrue
        result pat b = do (u,sub) <- bodyAndSub sig False app pat body [0,k] arg
                          Just $ F "->" [b>>>sub,addToPoss [i,1] $ u>>>sub]
                                     
-- distribute over a sum

 simplifyA sig t | just n = Just $ f $ map g $ mkTerms $ ts!!i
	                where (x,ts) = getOpArgs t
	                      f = if relational sig x then mkDisjunct else mkSum
	                      n = search isSum ts; i = get n
	                      g u = updArgs t $ updList ts i u

 simplifyA _ (F "`IN`" [t,F x ts])    | collector x = jConst $ inTerm t ts

 simplifyA _ (F "`NOTIN`" [t,F x ts]) | collector x = jConst $ notInTerm t ts

 simplifyA sig (F "`in`" [t,F x ts]) 
   | collector x = do guard $ all f ts
   		      if f t then jConst $ inTerm t ts 
   		             else do guard $ orT (isVar sig) t 
    		             	     Just $ mkDisjunct $ map (mkEq t) ts
 		   where f = isValue sig

 simplifyA sig (F "`NOTin`" [t,F x ts]) 
   | collector x = do guard $ all f ts
   		      if f t then jConst $ notInTerm t ts 
   		             else do guard $ orT (isVar sig) t
   		             	     Just $ mkConjunct $ map (mkNeq t) ts
   		   where f = isValue sig

 simplifyA sig (F "`shares`" [F x ts,F y us])
   | collectors x y = do guard $ all f ts && all f us; jConst $ sharesTerm ts us
		      where f = isValue sig

 simplifyA sig (F "`NOTshares`" [F x ts,F y us])
   | collectors x y = do guard $ all f ts && all f us
   			 jConst $ disjointTerm ts us
		      where f = isValue sig

 simplifyA sig (F "disjoint" [F x ts]) 
   | all collector (x:map root ts) = do guard $ all f tss 
   				        jConst $ distinctTerms tss
		     		     where f = all $ isValue sig
				           tss = map subterms ts

 simplifyA sig (F "`subset`" [F x ts,F y us])
   | collectors x y = do guard $ all f ts && all f us; jConst $ subsetTerm ts us
		      where f = isValue sig

 simplifyA sig (F "`NOTsubset`" [F x ts,F y us])
   | collectors x y = do guard $ all f ts && all f us
   			 jConst $ not $ subsetTerm ts us
		      where f = isValue sig

 simplifyA sig (F "Int" [t]) | andT (not . isVar sig) t     
 			       = jConst $ just $ parseInt t

 simplifyA sig (F "Real" [t]) | andT (not . isVar sig) t   
 			       = jConst $ just $ parseDouble t

 simplifyA _ (F "List" [t])    = jConst $ isList t

 simplifyA sig (F "Value" [t]) = jConst $ isValue sig t

 simplifyA sig t 	       = simplifyS sig t
 
 mkStates,mkLabels,mkAtoms :: Sig -> [Int] -> TermS
 mkStates sig = mkList . map (sig.states!!)
 mkLabels sig = mkList . map (sig.labels!!)
 mkAtoms sig  = mkList . map (sig.atoms!!)

 simplifyS sig (F "`meet`" [F x ts,F y us])
  | collectors x y = do guard $ all f ts && all f us
  			jList $ meetTerm ts us
  		     where f = isValue sig

 simplifyS sig (F "`join`" [F x ts,F y us]) 
  | collectors x y = do guard $ all f ts && all f us
  		        jList $ joinTerms ts us
   		     where f = isValue sig

 simplifyS sig (F "-" [F x ts,F y us])  
  | collectors x y = do guard $ all f ts && all f us
  		        jList $ removeTerms ts us
 		     where f = isValue sig
 					     
 simplifyS sig (F "-" [F x ts,u]) 
  | collector x = do guard $ all f ts && f u; jList $ removeTerm ts u
 		  where f = isValue sig

 simplifyS _ (F "$" [F "filter" _,F "[]" []]) = Just mkMt
	
 simplifyS sig (F "$" [F "filter" [p],F x ts]) 
  | collector x && just us = Just $ F x $ fst $ get us
  			     where us = split2 (isTrue . f) (isFalse . f) ts
		                   f = sapply sig p

 simplifyS sig (F "$" [F "$" [F "sfold" [f],a],F x []]) | collector x = Just a
 
 simplifyS sig (F "$" [F "$" [F "sfold" [f],a],F x (t:ts)]) | collector x = 
        Just $ apply (apply (F "sfold" [f]) $ simplifyIter sig $ applyL f [a,t]) 
   	     $ F x ts
 		                            
-- special rules 		                       

 simplifyS sig (F "tree" [F x []]) = parse (term sig) x

 simplifyS sig (F "subst" [t,u,v]) | isVar sig x = Just $ v>>>for t x
 						   where x = root u

-- LR parsing

 simplifyS sig (F "parseLR" [str]) = 
 	      do input <- parseWords str
	         guard $ ("end":input) `subset` map root sig.labels &&
	                 all (\i -> all (det i) [0..m]) [0..n]
	         Just $ F "parseLR" [Hidden $ LRarr acttab,mkStates sig [0],
		 		     mkMt,mkList $ map leaf $ input]
              where upb@(n,m) = (length sig.states-1,length sig.labels-1)
	            bds = ((0,0),upb)
	    	    acttab = array' bds [(ik,actTable sig ik) | ik <- range bds]
		    det i k = length (sig.transL!!i!!k) <= 1
			      
 simplifyS sig (F "parseLR" [Hidden (LRarr acttab),F "[]" sts,F "[]" trees,
 		             F "[]" input])  =
             Just $ case nextLR sig acttab (getIndices (`elem` sts) sig.states) 
             			trees $ getIndices (`elem` input) sig.labels of 
	  	         Success t -> t
	                 More is trees input
		           -> F "parseLR" [Hidden $ LRarr acttab,
			   		   mkStates sig is,mkList trees,f input]
		         _ -> leaf "no parse"
             where f [] = mkList [leaf "end"] 
	           f ks = mkList $ map (sig.labels!!) ks

-- auxiliary functions for evaluating modal formulas

 simplifyS sig (F "trans" state@(_:_)) =
      	        if null sig.states then Just $ mkList $ simplReducts sig True st
                                   else do i <- search (== st) sig.states
                                           Just $ mkStates sig $ sig.trans!!i
                where st = mkTup state

 simplifyS sig (F "$" [F "transL" state@(_:_),lab]) =
 	       if null sig.states then Just $ mkList $ simplReducts sig True 
           			                     $ mkPair st lab
                                  else do i <- search (== st) sig.states
		                          k <- search (== lab) sig.labels
		                          Just $ mkStates sig $ sig.transL!!i!!k
 	       where st = mkTup state

 simplifyS sig (F "out" state@(_:_)) = 
                      if null sig.states then Just $ mkList $ filter f sig.atoms
                                         else do i <- search (== st) sig.states
                                                 Just $ mkAtoms sig $ out sig!!i
 	              where st = mkTup state
                            f = elem st . simplReducts sig True

 simplifyS sig (F "$" [F "outL" state@(_:_),lab]) =
  	          if null sig.states then Just $ mkList $ filter f sig.atoms
  	                             else do i <- search (== st) sig.states
		                             k <- search (== lab) sig.labels
		                             Just $ mkAtoms sig $ outL sig!!i!!k
 	          where st = mkTup state
 	                f = elem (mkPair st lab) . simplReducts sig True

-- functions for analyzing the Kripke model stored in the actual signature

 simplifyS sig (F "succs" state) = do i <- search (== st) sig.states
 				      Just $ mkStates sig $ fixpt subset f [i]
                                   where st = mkTup state
                                         f is = is `join` joinMap g is
				         ks = indices_ sig.labels
				         g i = sig.trans!!i `join`
				               joinMap ((sig.transL!!i)!!) ks

 simplifyS sig (F "parents" state) = do i <- search (== st) sig.states
 				        Just $ mkStates sig $ parents sig!!i
 	                             where st = mkTup state

 simplifyS sig (F "$" [F "parentsL" state,lab]) 
                                   = do i <- search (== st) sig.states
				        k <- search (== lab) sig.labels
				        Just $ mkStates sig $ parentsL sig!!i!!k
 	                             where st = mkTup state

 simplifyS sig (F "preds" state)   = do i <- search (== st) sig.states
 				        Just $ mkStates sig $ fixpt subset f [i]
                                     where st = mkTup state
                                           f is = is `join` joinMap g is
				           ks = indices_ sig.labels
				           g i = parents sig !!i `join`
				                joinMap ((parentsL sig!!i)!!) ks

 simplifyS sig (F "$" [F "traces" state,st']) =   
              		            do i <- search (== st) sig.states
              		               j <- search (== st') sig.states
              		               jList $ map (mkStates sig) $ traces f i j
                                    where st = mkTup state
                            	          ks = indices_ sig.labels
                            	          f i = sig.trans!!i `join`
				                joinMap ((sig.transL!!i)!!) ks

 simplifyS sig (F "$" [F "tracesL" state,st']) =   
              		        do i <- search (== st) sig.states
              		           j <- search (== st') sig.states
              		           jList $ map (mkLabels sig) $ tracesL ks f i j
                                where st = mkTup state
                            	      ks = indices_ sig.labels
                            	      f i k = (sig.transL!!i)!!k

-- state equivalence

 simplifyS sig (F "~" [t,u]) = do i <- search (== t) sig.states
 			          j <- search (== u) sig.states
 				  let cli = search (elem i) part
 				      clj = search (elem j) part
                                  Just $ mkConst $ cli == clj
                      where part = mkPartition (indices_ sig.states) $ bisim sig 

 simplifyS sig (F x@"equivStep" []) = Just $ F x [Hidden mat] 
 		     where sts = map showTerm0 sig.states
                           mat = ListMatL sts [(f i,f j,map (apply2 $ map f) ps) 
                         		       | (i,j,ps) <- bisim0 sig]
                           f = showTerm0 . (sig.states!!)

 simplifyS _ (F x@"equivStep" [Hidden mat]) = 
 			               do ListMatL sts s <- Just mat
                                          let new = bisimStep s
	                                  guard $ s /= new 
	                                  Just $ F x [Hidden $ ListMatL sts new]

-- global model checking

 simplifyS sig (F "eval" [phi])  = do sts <- foldModal sig phi
 				      Just $ mkStates sig sts

 simplifyS sig (F "evalG" [phi]) = do sts <- foldModal sig phi
                		      let f st | isPos st = st
                		               | st `elem` map (strs!!) sts 
                		      	                  = "dark green_"++st 
                		      	       | True     = "red_"++st
                                      Just $ mapT f $ eqsToGraph [] eqs
      where [strs,labs] = map (map showTerm0)[sig.states,sig.labels]
            (eqs,_) = if null labs then relToEqs 0 $ mkPairs strs strs sig.trans
		      else relLToEqs 0 $ mkTriples strs labs strs sig.transL
	  
-- flow graph initialization

 simplifyS sig (F "stateflow" [t]) | just u = u where u = initStates True sig t
 
 simplifyS sig (F "stateflow" [t]) = initStates True sig $ mkCycles [] t

 simplifyS sig (F "stateflowT" [t])| just u = u where u = initStates False sig t
 
 simplifyS sig (F "stateflowT" [t]) = initStates False sig $ mkCycles [] t

 simplifyS _ (F "postflow" [t]) = do eqs <- parseEqs t
		                     Just $ initPost $ eqsToGraph [0] eqs
 
 simplifyS _ (F "subsflow" [t]) = do eqs <- parseEqs t
		                     Just $ initSubs $ eqsToGraph [0] eqs
 
-- flowgraph transformation

 simplifyS sig t | just flow = do guard changed; Just $ mkFlow True sig ft id
                    where flow = parseFlow True sig t $ parseVal sig
		          (ft,changed) = evalStates sig t $ get flow

 simplifyS sig t | just flow = do guard changed; Just $ mkFlow False sig ft id
                    where flow = parseFlow False sig t $ parseVal sig
		          (ft,changed) = evalStates sig t $ get flow
		 
 simplifyS sig t | just flow = do guard changed
 				  Just $ mkFlow True sig ft $ F "bool" . single
	            where flow = parseFlow True sig t mkVal
		          (ft,changed) = evalPost (simplifyIter sig) $ get flow
		          mkVal t = do F "bool" [t] <- Just t; Just t
		    
 simplifyS sig t | just flow = do guard changed
                                  Just $ mkFlow True sig ft 
                                       $ mkList . concatMap mkSub
	            where flow = parseFlow True sig t parseSubs
		          flow' = get flow
		          xs = vars flow'
		          vars (In g _)         = vars g
		          vars (Assign x _ g _) = vars g `join1` x
		          vars (Ite _ g1 g2 _)  = vars g1 `join` vars g2
		          vars (Fork gs _)      = joinMap vars gs
		          vars flow	        = []
		          (ft,changed) = evalSubs (simplifyIter sig) flow' xs
		          parseSubs t = do F "[]" ts <- Just t; mapM parseSub ts
		          parseSub t = do s <- parseList parseEq t
	   			          let (xs,ts) = unzip s
				          Just $ forL ts xs
		          parseEq t = do F "=" [V x,t] <- Just t; Just (x,t)
		          mkSub f = if null ys then [] else [mkList $ map g ys]
			            where ys = domSub xs f
			                  g x = mkEq (V x) $ f x

-- regular expressions and acceptors

 simplifyS sig (F "showRE" [t]) | just e = Just $ showRE $ fst $ get e 
 	                        	   where e = parseRE sig t
 	            	
 simplifyS sig (F "deltaBro" [t,F x []]) 
 				 | just e && just e' = Just $ showRE $ get e' 
 	                        where e = parseRE sig t
 	                              e' = deltaBro (getRHS sig) (fst $ get e) x
 	                	            	
 simplifyS sig (F "betaBro" [t]) | just e && just b = Just $ mkConst $ get b
 	                            where e = parseRE sig t
 	                        	  b = betaBro (getRHS sig) $ fst $ get e
	                	            	
 simplifyS sig (F "$" [F "unfoldBro" [t],F "[]" xs]) 
 				 | just e && just b = Just $ mkConst $ get b
  	            where e = parseRE sig t
 	                  b = unfoldBro (getRHS sig) (fst $ get e) $ map root xs 

 simplifyS sig (F "auto" [t]) | just e = Just $ eqsToGraph [] $ fst 
 					      $ relLToEqs 0 trips
            where e = parseRE sig t
		  (e',labs) = get e
		  (sts,nda) = regToAuto e'
		  trips = [(show q,a,map show $ nda q a) | q <- sts, a <- labs]

 simplifyS sig (F "pauto" [t]) | just e = Just $ eqsToGraph [] $ fst 
 					       $ relLToEqs 0 trips
	    where e = parseRE sig t
		  (e',labs) = get e
		  (_,nda) = regToAuto e'
		  labs' = labs `minus1` "eps"
		  (sts,delta) = powerAuto nda labs' 
		  trips = [(show q,a,[show $ delta q a]) | q <- sts, a <- labs']
 	          	          	          
 simplifyS _ t = simplifyT t
			  
 getRHS :: Sig -> String -> Maybe RegExp
 getRHS sig x = do [rhs] <- Just $ simplReducts sig False $ F "step" [leaf x]
 	           (e,_) <- parseRE sig rhs; Just e
						 
-- SIGNATURE-INDEPENDENT SIMPLIFICATIONS

 simplifyT :: TermS -> Maybe TermS
 
 -- simplifyT (F "get0" [t@(F x _)]) | isFix x = Just t
 
 simplifyT (F x [F "()" ts]) | just i && k < length ts = Just $ ts!!k
                             | not $ collector x || binder x || x `elem` words 
                 			    "~ List Value sat string true false"
                                                       = Just $ F x ts 
	                       where i = parse (strNat "get") x; k = get i

 simplifyT (F x [F "skip" []])              = Just $ leaf "skip"
  	  
 simplifyT (F "$" [F x [],F "()" ts])       = Just $ F x ts
  	  
 simplifyT (F "$" [F x [],t])               = Just $ F x [t]

 simplifyT (F "$" [F "lsec" [t,F x []],u])  = Just $ F x [t,u]
 
 simplifyT (F "$" [F "rsec" [F x [],t],u])  = Just $ F x [u,t]
 
 simplifyT (F "$" [F "get" [n],t]) | just i = Just $ F ("get"++show (get i)) [t]
 					      where i = parseNat n
 					      
 simplifyT (F "$" [F "." [g,f],t])            = Just $ apply g $ apply f t 				         

 simplifyT (F "$" [F "$" [F "flip" [f],t],u]) = Just $ apply (apply f u) t
				       
 simplifyT (F "$" [F "map" [f@(F _ ts)],F x us]) | collector x 
                                              = Just $ F x $ map (apply f) us
  
 simplifyT (F "$" [F "replicate" [t],u]) | just n = jList $ replicate (get n) u
		    			            where n = parsePnat t
		    			            
 simplifyT (F "$" [F "$" [F "upd" [F x ts],n],u]) 
         | just i && collector x && k < length ts = Just $ F x $ updList ts k u 
				               where i = parseNat n; k = get i
				     
 simplifyT (F "$" [F "$" [F "upd" [F ":" [t,v]],n],u]) | just i =
               Just $ if k == 0 then F ":" [u,v] 
	 	      else F ":" [t,F "$" [F "$" [F "upd" [v],mkConst $ k-1],u]]
				               where i = parseNat n; k = get i

 simplifyT (F "$" [F "insert" [t],F x ts]) 
         | collector x && just rs = jConsts $ insert (<) q qs
 			                       where rs = mapM parseInt $ t:ts
						     q:qs = get rs

 simplifyT (F "$" [F "$" [F "insertL" [t],F "[]" ts],F "[]" all]) 
         | (t:ts) `subset` all = jList $ insert less t ts
 			         where t `less` u = getInd t all < getInd u all

 simplifyT (F "$" [F "uncurry" [f],F "()" (t:ts@(_:_))]) =
 			   Just $ F "$" [F "uncurry" [apply f t],F "()" ts]
 			    
 simplifyT (F "$" [F "uncurry" [f],F "()" [t]]) = Just $ apply f t
 
 simplifyT (F "$" [F "$" [F "foldl" [f],a],F x ts]) | collector x = 
			   Just $ foldl g a ts where g a t = applyL f [a,t]

 simplifyT (F "$" [F "foldl1" [f],F x ts]) | collector x = 
 			   Just $ foldl1 g ts where g a t = applyL f [a,t]

 simplifyT (F "$" [F "$" [F "foldr" [f],a],F x ts]) | collector x = 
	                   Just $ foldr g a ts where g t a = applyL f [t,a]

 simplifyT (F "$" [F "foldr1" [f],F x ts]) | collector x = 
	                   Just $ foldr1 g ts where g t u = applyL f [t,u]
			               
 simplifyT (F "$" [F "zip" [F x ts],F y us]) | collectors x y = 
 	                   jList $ map g $ zip ts us where g (t,u) = mkPair t u

 simplifyT (F "$" [F "$" [F "zipWith" _,F x _],F ":" _]) 
 			   | collector x = Just mkMt

 simplifyT (F "$" [F "$" [F "zipWith" _,F ":" _],F x _]) 
 			   | collector x = Just mkMt

 simplifyT (F "$" [F "$" [F "zipWith" [f],F ":" [t,ts]],F ":" [u,us]]) =
             Just $ F ":" [applyL f [t,u],apply (apply (F "zipWith" [f]) ts) us]

 simplifyT (F "$" [F "$" [F "zipWith" [f],F x ts],F y us]) | collectors x y = 
                            jList $ zipWith g ts us where g t u = applyL f [t,u]
  
 simplifyT (F "CASE" (F "->" [t,u]:_))  | isTrue t  = Just u
 
 simplifyT (F "CASE" (F "->" [t,_]:ts)) | isFalse t = Just $ F "CASE" ts
 
 simplifyT (F "CASE" [])          = Just $ leaf "undefined"
 
 simplifyT (F "length" [F x ts]) | collector x = jConst $ length ts
 
 simplifyT (F "size" [F "{}" ts]) = jConst $ length $ mkSet ts

 simplifyT (F "height" [t])       = jConst $ height t
  
 simplifyT (F "string" [t])       = Just $ leaf $ showTree False t `minus1` '\"'
  
 simplifyT (F "stringNB" [t])     = Just $ leaf $ showTree False t `minus` "()"
 
 simplifyT (F "noBrackets" [t])   = Just $ leaf $ minus (showTree False t) "()"

 simplifyT (F "getCols" [F x ts]) = Just $ if null z then F x $ map f ts
 					   else F col [F (tail z) $ map f ts]
 				    where (col,z) = span (/= '$') x
 				          f t = F "erect" [t]

 simplifyT (F "suc" [t])       | just i = jConst $ get i+1 where i = parseInt t

 simplifyT (F "range" [t,u])   | just i && just k = 
 					    jList $ map mkConst [get i..get k] 
 					    where i = parseInt t; k = parseInt u

 simplifyT (F "list" [F x ts]) | collector x = Just $ drop0FromPoss $ mkList ts

 simplifyT (F "set" [F x ts])  | collector x = Just $ F "{}" $ joinTerms [] ts

 simplifyT (F "bag" [F x ts])  | collector x = Just $ drop0FromPoss $ mkBag ts
	  
 simplifyT (F "tup" [F x ts])  | collector x = Just $ drop0FromPoss $ mkTup ts

 simplifyT (F "branch" [F x ts]) | collector x = Just $ drop0FromPoss $ mkSum ts
 
 simplifyT (F "null" [F x ts])   | collector x = jConst $ null ts

 simplifyT (F "NOTnull" [F x ts])   | collector x = jConst $ not $ null ts

 simplifyT (F "single" [F x ts])    | collector x = jConst $ length ts == 1

 simplifyT (F "!!" [F x ts,n])      | just i && collector x && k < length ts = 
 					        Just $ ts!!k
 					        where i = parseNat n; k = get i
 simplifyT (F "!!" [F ":" [t,u],n]) | just i  = Just $ if k == 0 then t else
					               F "!!" [u,mkConst $ k-1]
 					        where i = parseNat n; k = get i

 simplifyT (F "prodL" [F x ts]) | all collector (x:map root ts) = 
 			   jList $ map mkTup $ accumulate $ map subterms ts
 			   
 simplifyT t | f == "curry" && notnull tss && length us == 1 =
 			   Just $ applyL (head us) $ concat uss
 			   where (f,tss) = unCurry t; us:uss = tss

 simplifyT (F "index" [t,F x ts]) | collector x = do i <- search (eqTerm t) ts
 			                             jConst i

 simplifyT (F "indices" [F x ts]) 
 			  | collector x = jList $ map mkConst [0..length ts-1] 

 simplifyT (F "singles" [F x ts]) 
 			  | collector x = jList $ map (mkList . single) ts

 simplifyT (F "subsets" [F x ts]) | collector x = jList $ map (F x) subs
 	                where subs = [map (ts!!) ns | ns <- subsetsB lg lg]
		       	      lg = length ts

 simplifyT (F "subsetsB" [F x ts,t]) | collector x && just i = 
 		        jList $ map (F x) subs
 	                where lg = length ts; i = parseInt t
		              subs = [map (ts!!) ns | ns <- subsetsB lg $ get i]

 simplifyT (F "perms" [F "[]" ts])   = jList $ map mkList $ perms ts
           
 simplifyT (F "reverse" [F "[]" ts]) = jList $ reverse ts

 simplifyT (F "shuffle" [F x ts]) 
         | all ((== "[]") . root) ts = jList $ shuffle $ map subterms ts
       
 simplifyT (F "sort" [F "[]" ts]) 
         | all just is = jConsts $ qsort (<=) $ map get is
         | all just rs = jConsts $ qsort (<=) $ map get rs
         | True        = jList $ sort (<) ts
         		 where is = map (foldArith intAlg) ts
                               rs = map (foldArith realAlg) ts

 simplifyT (F "subperms" [F "[]" ts]) = jList $ map mkList $ subperms ts
					    
 simplifyT (F "=" [F "^" ts@(t:ts'),F "^" us@(u:us')]) = 
                   case search (eqTerm t) us of
	                Just n -> Just $ mkEq (mkBag ts') $ mkBag $ context n us
	                _ -> do n <- search (eqTerm u) ts
	                        Just $ mkEq (mkBag $ context n ts) $ mkBag us'

 simplifyT t = simplifyT1 t
				  
 simplifyT1 (F "`mod`" [t,u])       | just i && just j && k /= 0 = 
 				      Just $ mkConst $ mod (get i) k
 			    where i = parseInt t; j = parseInt u; k = get j
						 
 simplifyT1 (F x [F "+" [t,u],n])   | isOrd x && just i && just j =
                                      Just $ F x [u,mkConst $ get i-get j] 
                                    | isOrd x && just i && just k =
                                      Just $ F x [u,mkConst $ get i-get k] 
                            where i = parseInt n; j = parseInt t; k = parseInt u
						 
 simplifyT1 (F x [n,F "+" [t,u]])   | isOrd x && just i && just j =
                                      Just $ F x [mkConst $ get i-get j,u] 
                                    | isOrd x && just i && just k =
                                      Just $ F x [mkConst $ get i-get k,t] 
                            where i = parseInt n; j = parseInt t; k = parseInt u
						 
 simplifyT1 (F "+" [n,F "+" [t,u]]) | just i && just j =
 			              Just $ F "+" [mkConst $ get i+get j,u] 
 			            | just i && just k =
 			              Just $ F "+" [mkConst $ get i+get k,t] 
 			    where i = parseInt n; j = parseInt t; k = parseInt u
						 
 simplifyT1 (F "+" [F "+" [t,u],n]) | just i && just j =
 			    	      Just $ F "+" [mkConst $ get i+get j,u] 
				    | just i && just k =
 			    	      Just $ F "+" [mkConst $ get i+get k,t] 
			    where i = parseInt n; j = parseInt t; k = parseInt u
							    
 simplifyT1 (F ">" [F "()" (t:ts),F "()" (u:us)]) =
 	     Just $ F "|" [mkGr t u,F "&" [mkEq t u,mkGr (mkTup ts) $ mkTup us]]
			       
 simplifyT1 (F "color" [i,n]) = do i <- parseNat i; n <- parseNat n
				   jConst $ hue 0 red n i

 simplifyT1 (F "dnf" [t]) | isObdd t =
                            jConsts $ funToBins $ obddToFun $ drop0FromPoss t

 simplifyT1 (F "obdd" [t]) = do bins <- parseBins t; Just $ binsToObdd bins
  
 simplifyT1 (F "minimize" [t]) | just bins = jConsts $ minDNF $ get bins
			                     where bins = parseBins t

 simplifyT1 (F "minimize" [t]) | isObdd t = Just $ drop0FromPoss $ collapse True
 					         $ removeVar t

 simplifyT1 (F "minterms" [t]) | just bins = 
                                 jConsts $ concatMap minterms $ get bins
			         where bins = parseBins t

 simplifyT1 (F "gauss" [t]) | just eqs =
                              Just $ F "bool" [mkLinEqs $ get $ gauss $ get eqs]
                              where eqs = parseLinEqs t

 simplifyT1 (F x@"gaussI" [t]) | just eqs =
                                case gauss1 $ get eqs of 
                                     Just eqs -> f eqs
                                     _ -> do eqs <- gauss2 $ get eqs; f eqs
		                where eqs = parseLinEqs t
		                      f eqs = Just $ F x [mkLinEqs $ gauss3 eqs]

 simplifyT1 (F "pascal" [t]) = do n <- parseNat t; jConsts $ pascal n
 
 simplifyT1 (F "concept" [F "[]" ts,F "[]" us,F "[]" vs]) | f us && f vs =
                               Just $ h $ concept ts (g us) $ g vs
			       where f = all $ (== "()") . root
				     g = map $ map root . subterms
				     h = mkSum . map (mkTup . map leaf)

 simplifyT1 (F x ts) | (x == "[]" || x == "{}") && length us < length ts = 
 		  Just $ F x us where us = if head x == '[' then mergeActs ts
			       				    else joinTerms [] ts
							      
-- change the variable ordering of a DNF or an OBDD

 simplifyT1 (F "nextperm" [F x ns]) | collector x 
 			             = do ns <- mapM parseInt ns
				          Just $ F x $ map mkConst $ nextPerm ns

 simplifyT1 (F "permute" (t:ts)) | just bins =
                      if null s then Just t
	              else if null ts then perm [t,t,mkConsts first]
		           else do [_,ns] <- Just ts; ns <- parseNats ns
	                           let permute s = map (s!!) ns
		                   perm [t,mkConsts $ map permute s,
		    		         mkConsts $ nextPerm ns]
	              where bins = parseBins t; s = get bins
		            first = indices_ $ head s; perm = Just . F "permute"
							      
 simplifyT1 (F "light" [i,n,c]) = do i <- parseNat i; n <- parseNat n
				     c <- parseColor c
			             jConst $ nextLight n (-n`div`3+i) c

 simplifyT1 (F x@('p':'a':'t':'h':_) [F "[]" ts]) 
 				| all just ps && length qs < length ps =
                                  Just $ F x [mkList $ map mkConstPair qs]
			          where ps = map parseRealReal ts
					qs = reducePath $ map get ps
				       
 simplifyT1 (F "reduce" [F "[]" ts,F "[]" us]) 
                                | all isTup us = Just $ g $ reduceExas ts $ f us
			          where f = map $ map root . subterms
					g = mkSum . map (mkTup . map leaf)

 simplifyT1 _ = Nothing

 mergeActs (F x [t]:F y [u]:ts) 
                        | x == y && x `elem` words "J M T" && just r && just s
		          = F x [mkConst (get r+get s)]:ts where r = parseReal t
			                                         s = parseReal u
 mergeActs (F "L" []:F "R" []:ts) = ts
 mergeActs (F "R" []:F "L" []:ts) = ts
 mergeActs (t:u:ts) 		  = t:mergeActs (u:ts)
 mergeActs ts       		  = ts

-- AXIOM-BASED SIMPLIFICATION
 
-- simplReducts sig True t returns the reducts of all axioms of sig.transitions 
-- that are applicable to t.
-- simplReducts sig False t returns the reduct of the first axiom of sig.simpls 
-- that is applicable to t.

 simplReducts :: Sig -> Bool -> TermS -> [TermS]
 simplReducts sig isTrans t = if isTrans then concatMap f sig.transitions
 			      else if null reds then [] else [head reds]
  where reds = concatMap f sig.simpls
        f (u,cs,v) = case matchSubs sig xs t u of
                          Just (sub,ts,is,bag) | just sub'
		            -> map (mkBag . h is ts . toElems) 
		                   $ mkTerms $ reduce $ v>>>get sub'
	                       where sub' = g sub cs xs
			             toElems = if bag then mkElems else single
		          _ -> []
		     where xs = frees sig u
		           h is ts vs = g 0 0 ts 
		                where g i k (u:us) = if i `elem` is
		               		             then vs!!k:g (i+1) (k+1) us
			                             else ts!!i:g (i+1) k us
			              g _ _ _ = []
			   reduce = if isTrans then simplifyIter sig else id
	g sub (c:cs) xs | isTrue $ simplifyIter sig $ c>>>sub = g sub cs xs
        g sub (c@(F "=" [t,u]):cs) xs | frees sig t `subset` xs &&
				        xs `disjoint` zs && just h
		         = g (sub `andThen` get h) cs $ xs++zs
		           where zs = frees sig u
			         h = match sig zs (simplifyIter sig $ t>>>sub) u
        g sub [] _ = Just sub	
        g _ _ _    = Nothing
        
-- used by Esolve > rewriteSig/States 

-- buildTrans sig ts constructs the binary and ternary relations generated by ts
-- and the simplification and transition axioms of sig.
	  
 buildTrans :: Sig -> [TermS] -> (Pairs TermS,Triples TermS TermS)	     
 buildTrans sig ts = (zip ts $ reduce ts,
 		      zipL pairs $ reduce [mkPair t lab | (t,lab) <- pairs])
                     where reduce = map $ simplReducts sig True
                           pairs = prod2 ts sig.labels
                           
-- used by Ecom > buildKripke >=3

-- buildTransLoop sig mode constructs the transition system generated by 
-- sig.states and the simplification and transition axioms of sig.

 buildTransLoop :: Sig -> Int -> ([TermS],Pairs TermS,Triples TermS TermS)	     
 buildTransLoop sig mode = loop sig.states sig.states [] [] where
                    loop old sts ps psL = 
                       if null new then (old,rs,rsL) 
       		                   else loop (old `join` new) new rs rsL
	               where new = (joinM redss `minus` old) `join` 
	              		   (joinM redssL `minus` old) 
			     redss  = reduce sts
			     redssL = reduce [mkPair st lab | (st,lab) <- pairs]
			     reduce = map $ simplReducts sig True
			     pairs = prod2 sts sig.labels
			     qs = zip sts redss; qsL = zipL pairs redssL
			     meetNew = map $ meet new
			     (rs,rsL) = case mode of 
			    		 0 -> (ps++map removeCycles qs,
			    		       psL++map removeCyclesL qsL)
			                 1 -> (ps++zip sts (meetNew redss),
			                       psL++zipL pairs (meetNew redssL))
			    		 _ -> (ps++qs,psL++qsL)
			     removeCycles (st,reds) = (st,f st reds)
			     removeCyclesL (st,lab,reds) = (st,lab,f st reds)
			     f st sts = [st' | st' <- sts,
			    		       st `notElem` fixpt subset g [st']] 
 			     g sts = sts `join` joinMap h sts where
			     h st = (case lookup st ps of Just sts -> sts
			    				  _ -> []) ++
			            joinMap f sig.labels
			            where f lab = case lookupL st lab psL of 
			           		       Just sts -> sts; _ -> []

-- used by Ecom > buildKripke 0/1/2
                         
 pairsToInts :: [TermS] -> Pairs TermS -> [TermS] -> [[Int]]
 pairsToInts us ps = map f where f t = searchAll (`elem` (get $ lookup t ps)) us
 			 
 tripsToInts :: [TermS] -> [TermS] -> Triples TermS TermS -> [TermS] 
 							  -> [[[Int]]]
 tripsToInts us vs ps = map f
 	                where f t = map (flip (g t) us) vs
 	                      g t u = searchAll (`elem` (get $ lookupL t u ps))

-- REWRITING AND NARROWING

 data Reducts = Sum [TermS] (String -> Int) | 
 		Backward [TermS] (String -> Int) |
 		Forward [TermS] (String -> Int) | Stop |
 		MatchFailure String -- generated only by genMatchFailure

-- quantify sig add ts t quantifies the free variables xs of ts.

 quantify :: Sig -> ([String] -> TermS -> TermS) -> [TermS] -> TermS -> TermS
 quantify sig add ts reduct = add (filter (noExcl &&& (`elem` ys)) xs) reduct
         	   	      where xs = joinMap (frees sig) ts
			      	    ys = sigVars sig reduct
 
 addTo _ [] t                  = t 
 addTo True rest (F "=" [t,u]) = mkEq (mkBag $ t:rest) u
 addTo True _ t                = t
 addTo _ rest t                = mkBag $ t:rest

-- used by Esolve > searchReds,applyAx{ToTerm},applySingle

-- searchReds .. ts reds .. pars tries to unify ts with a subset of reds such
-- that the instance of a given guard by the unifier is solvable by applying 
-- given axioms. The guard and the axioms are part of pars. 

 searchReds check rewrite vc ts reds b t sig xs pars = sr Stop ts [] reds [] 
   where lg = length ts
         sr _ (t:ts) us reds vs =
                           do (reduct,red,rest) <- h us' reds vs $ length reds-1
			      sr reduct ts us' rest $ red:vs
			   where us' = t:us
         sr reduct _ _ _ _ = Just reduct
         h ts reds vs i =
	    do guard $ i >= 0
	       case unify' ts (red:vs) (V"") (V"") ps ps V sig xs of
                    Def (f,True) -> case checkOrRewrite f sig xs vc pars of
			                 Stop -> h ts reds vs (i-1)
		                         reduct -> Just (reduct,red,rest)
	            _ -> h ts reds vs (i-1)
            where red = reds!!i; rest = context i reds
		  ps = replicate lg' []; lg' = length ts
		  checkOrRewrite = if lg == lg' then rewrite $ addTo b rest t
		                                else check
		                                
-- used by Esolve > applyAx{ToTerm}

-- solveGuard guard axs applies axs by narrowing at most 100 times to guard. 
-- axs are also the axioms applied to the guards of axs. Reducts are simplified,
-- but irreducible subtrees are not removed.

 solveGuard sig cond axs vc = do guard $ notnull sols; Just sols
                   where sols = [sol | Just sol <- map f $ mkSummands $ pr1 t]
                         f = parseSol $ solEq sig
                         t = applyLoop cond 100 vc axs axs sig True 2 False True
                         
-- used by Esolve > applyAx,rewrite,rewriteTerm

-- applyLoop t 0 m ... axs preAxs sig nar match refute simplify applies axioms 
-- at most m times to the maximal subtrees of t and returns the modified tree 
-- together with the number of actually applied steps. 
-- preAxs are applied to the guards of axs. 
-- nar = True 	   --> narrowing          
-- nar = False     --> rewriting
-- match = 0 	   --> match t against axs
-- match = 1 	   --> match t against the first applicable axiom of axs
-- match = 2 	   --> unify t against axs
-- match = 3       --> unify t against the first applicable axiom of axs
-- simplify = True --> simplifyIter
-- refute = True   --> turnIntoUndef

 applyLoop :: TermS -> Int -> (String -> Int) -> [TermS] -> [TermS] -> Sig 
 		    -> Bool -> Int -> Bool -> Bool -> (TermS,Int,String -> Int)

 applyLoop t m vc axs preAxs sig nar match refute simplify = f t 0 m vc
  where f t k 0 vc = (t,k,vc)
        f t k m vc = case modify t vc [] of Just (t,vc) -> f t (k+1) (m-1) vc 
			                    _ -> (t,k,vc)
        uni = match > 1
  	simpl = if simplify then simplifyIter sig else id
        remove p = if refute then turnIntoUndef sig t p else const Nothing
        modify t vc p = case f redex t p vc' of 
	 		     Just (t',vc) -> Just (simpl t',vc)
                             _ -> case remove p redex of 
			          Just u -> Just (simpl $ replace t p u,vc')
				  _ -> concat $ map (modify t vc) 
				  	      $ succsInd p $ subterms redex
	      where redex = getSubterm t p
		    filtered = filterClauses sig redex axs
	            rules = if isVarRoot sig redex then [] 
		            else if nar then filtered 
		       		        else filter unconditional filtered
		    (axs',vc') = renameApply sig t vc rules
	            f = if nar then modifyForm $ applyAxs axs' axs' preAxs
		               else modifyTerm $ applyAxsToTerm axs' axs' preAxs
	modifyForm f redex t p vc 
	 | sig.isDefunct sym = do (q,at,r) <- atomPosition sig t p
			          Backward reds vc <- g at r
			          Just (replace t q $ mkDisjunct reds,vc)
         | notnull p && isTerm sig redex =
                               do (at@(F "->" [_,_]),0) <- Just (getSubterm t q,
			       					 last p)
			          Backward reds vc <- g at [0]
				  Just (replace t q $ mkDisjunct reds,vc)
         | sig.isPred sym    = do Backward reds vc <- g redex []
			          Just (replace t p $ mkDisjunct reds,vc)
         | True              = do guard $ sig.isCopred sym
	 			  Forward reds vc <- g redex []
			          Just (replace t p $ mkConjunct reds,vc)
	                     where sym = getOp redex; q = init p
			           g at r = Just $ fst $ f redex at r sig vc uni
	modifyTerm f redex t p vc
	                     = do Sum reds vc <- Just $ fst $ f redex sig vc
 	       			  Just (replace t p $ mkSum reds,vc)
 	       			  
-- used by Esolve > solveGuard and Ecom > narrowStep
 
 applyLoopRandom :: Int -> TermS -> Int -> (String -> Int) -> [TermS] -> [TermS]
 			-> Sig -> Bool -> Int -> Bool 
		        -> (TermS,Int,String -> Int,Int)

 applyLoopRandom rand t m vc axs preAxs sig nar match simplify = f t 0 m vc rand
  where f t k 0 vc rand = (t,k,vc,rand)
        f t k m vc rand = case modify t [] vc rand of 
                          Just (t,vc,rand) -> f t (k+1) (m-1) vc $ nextRand rand
			  _ -> (t,k,vc,rand)
        uni = match > 1
  	simpl = if simplify then simplifyIter sig else id
        modify t p vc rand = 
	      case f redex t p vc' of 
	           Just (t,vc,rand) -> Just (simpl t,vc,rand)  
		   _ -> modifyL $ succsInd p $ subterms redex
              where redex = getSubterm t p
	            filtered = filterClauses sig redex axs
		    rules = if isVarRoot sig redex then [] 
		            else if nar then filtered 
			  	        else filter unconditional filtered
	            (axs',vc') = renameApply sig t vc rules
		    f = if nar then modifyForm $ applyAxsR axs' axs' preAxs rand
		        else modifyTerm $ applyAxsToTermR axs' axs' preAxs rand
		    modifyL ps@(_:_) = modify t (ps!!i) vc (nextRand rand) ++ 
	   			       modifyL (context i ps)
			               where i = mod rand $ length ps
                    modifyL _        = Nothing
	modifyTerm f redex t p vc =
            	   do (Sum reds vc,rand) <- Just $ (pr1***pr3) $ f redex sig vc
		      Just (replace t p $ mkSum reds,vc,rand)
	modifyForm f redex t p vc 
	 | sig.isDefunct sym = do (q,at,r) <- atomPosition sig t p
		     		  (Backward reds vc,rand) <- g at r
				  Just (replace t q $ mkDisjunct reds,vc,rand)
         | notnull p && isTerm sig redex =
            	               do (at@(F "->" [_,_]),0) <- Just (getSubterm t q,
			       					 last p)
		                  (Backward reds vc,rand) <- g at [0]
		                  Just (replace t q $ mkDisjunct reds,vc,rand)
         | sig.isPred sym    = do (Backward reds vc,rand) <- g redex []
		 	          Just (replace t p $ mkDisjunct reds,vc,rand)
         | True              = do guard $ sig.isCopred sym
         			  (Forward reds vc,rand) <- g redex []
		   		  Just (replace t p $ mkConjunct reds,vc,rand)
	             where sym = getOp redex; q = init p
			   g at r = Just $ (pr1***pr3) $ f redex at r sig vc uni
			
-- used by Ecom > narrowStep

-- NARROWING OF FORMULAS

-- applyAxs cls axs preAxs redex u r computes all narrowing/rewriting reducts 
-- of the redex at position r of u that result from unifying/matching redex 
-- against axs. The guards of axs are narrowed by preAxs. cls is the original, 
-- non-extended and non-renamed version of axs. 
-- uni = True  --> The redex is unified against axs.
-- uni = False --> The redex is matched against axs.

 applyAxs (cl:cls) (ax:axs) preAxs redex at p sig vc uni =
   case applyAx ax preAxs redex at p sig vc uni of
        reduct@(Backward reds vc) 
          -> case applyAxs cls axs preAxs redex at p sig vc uni of
		  (Backward reds' vc,cls) -> (Backward (reds++reds') vc,cl:cls)
		  mf@(MatchFailure _,_) -> mf
		  _ -> (reduct,[cl])
        reduct@(Forward reds vc) 
          -> case applyAxs cls axs preAxs redex at p sig vc uni of
 		  (Forward reds' vc,cls) -> (Forward (reds++reds') vc,cl:cls)
		  mf@(MatchFailure _,_) -> mf
		  _ -> (reduct,[cl])
        mf@(MatchFailure _) -> (mf,[])
        _ -> applyAxs cls axs preAxs redex at p sig vc uni
 applyAxs _ _ _ _ _ _ _ _ _ = (Stop,[])
 
-- used by Esolve > applyLoop,applyToHeadOrBody and Ecom > narrowPar

-- applyAxsR axs preAxs rand redex t p computes the narrowing/rewriting 
-- reducts of the redex at position p of t that result from unifying/matching 
-- redex against a random element of axs. The guards of axs are narrowed by 
-- preAxs. 

 applyAxsR cls [] _ rand redex _ _ _ _ _             = (Stop,[],rand)
 applyAxsR cls axs preAxs rand redex at p sig vc uni =
   case applyAx ax preAxs redex at p sig vc uni of
        Stop -> applyAxsR cls' axs' preAxs (nextRand rand) redex at p sig vc uni
	reduct -> (reduct,[cl],rand)
   where n = rand `mod` length axs
	 cl = cls!!n; cls' = removeTerm cls cl
 	 ax = axs!!n; axs' = removeTerm axs ax
 	 
-- used by Esolve > applyLoopRandom and Ecom > narrowPar

-- applyAx ax axs redex at p sig vc applies the axiom ax to the redex at
-- position p of at. vc is the variable counter that is needed for renaming the
-- variables of ax that are introduced into t.

 applyAx (F "==>" [guard,F "<===" [F "->" [left,right],prem]]) axs redex 
	 at@(F "->" [_,r]) p sig vc uni | notnull p =
      case redex of 
      F "^" reds 
        -> case left of 
	   F "^" ts 
	     -> try (reds,[0..lg-1]) 100
	        where lg = length reds 
	              b = product [2..lg] > 100
		      xs = frees sig redex
	              pars = (guard,axs,left,right,prem,uni)
	              try (reds,s) 0 = 
		            if b then Backward [F "^" $ fst $ permute reds s] vc 
		                 else Stop
                      try (reds,s) n = case searchReds checkGuard rewrite vc ts 
		                                     reds True eq sig xs pars of
				            Just reduct -> reduct
				            _ -> try (permute reds s) $ n-1
	   _ -> foldl (applyTo reds) Stop $ indices_ reds
      _ -> applyTo [redex] Stop 0
      where eq = mkEq right r
            applyTo reds reduct i = 	      
	        case partialUnify guard axs left right prem at eq p sig vc uni $
				  reds!!i of
	        reduct'@(Backward ts vc) 
		  -> case reduct of
		          Backward reducts vc -> Backward (reducts++reducts') vc
	                  _ -> Backward reducts' vc 
	             where reducts' = map (addTo True $ context i reds) ts
	        _ -> reduct
	
 applyAx (F "==>" [guard,F "<===" [F "=" [left,right],prem]]) axs redex at p sig
      vc uni = partialUnify guard axs left right prem at (replace at p right) p
			    sig vc uni redex

 applyAx (F "==>" [guard,F "<===" [at,prem]]) axs redex _ _ sig vc uni =
      case unify0 at redex redex [] sig xs of          
      TotUni f 
        -> genMatchFailure sig uni dom at $
	           case u of F "True" _ -> Backward [mkRed []] vc
		             F "False" _ -> Stop
		             _ -> case solveGuard sig u axs vc of
		                       Just sols -> Backward (map mkRed sols) vc
	                               _ -> Stop
	   where dom = domSub xs f
                 u = simplifyIter sig $ guard>>>f
		 mkRed sol = quantify sig addAnys [at,prem] $ mkConjunct reduct
 	                     where g = f `andThen` mkSubst sol
			           reduct = prem>>>g:substToEqs g (domSub xs g)
      _ -> Stop
      where xs = frees sig redex
				
 applyAx (F "==>" [guard,F "===>" [at,conc]]) axs redex _ _ sig vc uni =
      case unify0 at redex redex [] sig xs of
      TotUni f 
        -> genMatchFailure sig uni dom at $
	           case u of F "True" _ -> Forward [mkRed []] vc
		             F "False" _ -> Stop
		             _ -> case solveGuard sig u axs vc of
                                       Just sols -> Forward (map mkRed sols) vc
		                       _ -> Stop
           where dom = domSub xs f
	         u = simplifyIter sig $ guard>>>f
		 mkRed sol = quantify sig addAlls [at,conc] $ mkDisjunct reduct
 	      	            where g = f `andThen` mkSubst sol
   	            	          reduct = conc>>>g:substToIneqs g (domSub xs g)
      _ -> Stop
      where xs = frees sig redex

 applyAx (F "==>" [guard,cl]) axs redex at p sig vc uni =
      applyAx (mkHornG guard cl mkTrue) axs redex at p sig vc uni

 applyAx (F "===>" [prem,conc]) axs redex at p sig vc uni =
      applyAx (mkCoHornG mkTrue prem conc) axs redex at p sig vc uni

 applyAx (F "<===" [conc,prem]) axs redex at p sig vc uni =
      applyAx (mkHornG mkTrue conc prem) axs redex at p sig vc uni

 applyAx at axs redex t p sig vc uni =
      applyAx (mkHorn at mkTrue) axs redex t p sig vc uni
      
-- used by Esolve > applyAxs{R}

 partialUnify guard axs left right prem at at' p sig vc uni redex =
        case unify0 left redex at p sig xs of
	     TotUni f -> rewrite at' f sig xs vc (guard,axs,left,right,prem,uni)
	     ParUni f dom -> genMatchFailure sig uni dom left $ Backward [t] vc
	                     where reduct = mkConjunct $ at>>>f:substToEqs f dom
			           t = quantify sig addAnys [left] reduct
	     _ -> Stop
        where xs = frees sig redex
      
-- used by Esolve > applyAx

 checkGuard f sig xs vc (guard,_,left,_,_,uni) =
    genMatchFailure sig uni (domSub xs f) left $
         if isFalse $ simplifyIter sig $ guard>>>f then Stop else Backward [] vc
      
-- used by Esolve > applyAx

 rewrite at f sig xs vc (guard,axs,left,right,prem,uni) =
        genMatchFailure sig uni (domSub xs f) left $
                        case t of F "True" _ -> Backward [mkRed []] vc
	                          F "False" _ -> Stop
	                          _ -> case solveGuard sig t axs vc of
		                       Just sols -> Backward (map mkRed sols) vc
				       _ -> Stop
        where t = simplifyIter sig $ guard>>>f
              mkRed sol = quantify sig addAnys [left,right,prem] reduct
	                  where reduct = mkConjunct $ at>>>g:prem>>>g:
	  					      substToEqs g (domSub xs g)
		                g = f `andThen` mkSubst sol
      
-- used by Esolve > applyAx,partialUnify

 genMatchFailure sig uni dom t reduct = 
            if uni || null dom then reduct
            else if any sig.isHovar dom then Stop 
	    else MatchFailure $ "Some redex does not match " ++ showTree False t
		                                
-- used by Esolve > applyAx,partialUnify,checkGuard,rewrite

-- REWRITING OF TERMS

 applyAxsToTerm (cl:cls) (ax:axs) preAxs redex sig vc =
   case applyAxToTerm ax preAxs redex sig vc of
   reduct@(Sum reds vc) -> case applyAxsToTerm cls axs preAxs redex sig vc of
 		           (Sum reds' vc,cls) -> (Sum (reds++reds') vc,cl:cls)
	                   _ -> (reduct,[cl])
   _ -> applyAxsToTerm cls axs preAxs redex sig vc
 applyAxsToTerm _ _ _ _ _ _ = (Stop,[])

-- used by Esolve > applyLoop and Ecom > rewritePar

 applyAxsToTermR cls [] _ rand redex _ _          = (Stop,[],rand)
 applyAxsToTermR cls axs preAxs rand redex sig vc =
   case applyAxToTerm ax preAxs redex sig vc of
	Stop -> applyAxsToTermR cls' axs' preAxs (nextRand rand) redex sig vc
	reduct -> (reduct,[cl],rand)
   where n = rand `mod` length axs
	 cl = cls!!n; cls' = removeTerm cls cl
 	 ax = axs!!n; axs' = removeTerm axs ax

-- used by Esolve > applyLoopRandom and Ecom > rewritePar
					    
-- applyAxToTerm ax is applied only within a TERM. Hence ax must be a
-- (guarded or unguarded) equation without a premise.

 applyAxToTerm (F "==>" [guard,F "->" [left,right]]) axs redex sig vc =
      case redex of 
      F "^" reds 
        -> case left of 
	   F "^" ts 
	     -> try (reds,[0..lg-1]) 100
	        where lg = length reds 
		      b = product [2..lg] > 100
		      xs = frees sig redex
	              pars = (guard,axs,left)
	              try (reds,s) 0 = 
		       if b then Sum [F "^" $ fst $ permute reds s] vc else Stop
                      try (reds,s) n = 
                        case searchReds checkGuardT rewriteTerm vc ts reds False
			                right sig xs pars of 
			     Just reduct -> reduct
			     _ -> try (permute reds s) $ n-1
	   _ -> foldl (applyTo reds) Stop $ indices_ reds
      _ -> applyTo [redex] Stop 0
      where applyTo reds reduct i =
	        case totalUnify guard axs left right sig vc $ reds!!i of
	             reduct'@(Sum ts vc)
		       -> case reduct of 
		               Sum reducts vc -> Sum (reducts++reducts') vc
			       _ -> Sum reducts' vc
			  where reducts' = map (addTo False $ context i reds) ts
	             _ -> reduct
	     
 applyAxToTerm (F "==>" [guard,F "=" [left,right]]) axs redex sig vc =
     totalUnify guard axs left right sig vc redex

 applyAxToTerm t@(F _ [_,_]) axs redex sig vc =
     applyAxToTerm (F "==>" [mkTrue,t]) axs redex sig vc

 applyAxToTerm _ _ _ _ _ = Stop

 totalUnify guard axs left right sig vc redex =
      case unify0 left redex redex [] sig xs of
	   TotUni f -> rewriteTerm right f sig xs vc (guard,axs,left)
	   _ -> Stop
      where xs = frees sig redex
      
-- used by Esolve > applyAxToTerm

 checkGuardT f sig xs vc (guard,_,left) =
      if notnull $ domSub xs f then Stop
      else if isFalse $ simplifyIter sig $ guard>>>f then Stop else Sum [] vc
	    
-- used by Esolve > applyAxToTerm

 rewriteTerm right f sig xs vc (guard,axs,left) =
      if notnull dom then Stop
      else case u of F "True" _ -> Sum [mkRed []] vc
		     F "False" _ -> Stop
		     _ -> case solveGuard sig u axs vc of
			       Just sols -> Sum (map mkRed sols) vc
		     	       _ -> Stop
      where dom = domSub xs f
            u = simplifyIter sig $ guard>>>f
	    mkRed sol = right>>>(f `andThen` mkSubst sol)
	    
-- used by Esolve > applyAxToTerm,totalUnify

-- APPLICATION OF THEOREMS

-- applySingle and applyMany work similarly to applyAxs, but apply only single
-- non-guarded clauses. In the case of applyMany, the redex splits into several
-- factors or summands of a conjunction resp. disjunction. 

 applySingle th@(F "<===" [F "False" _,prem]) redex t p sig vc =
        Just (replace t p $ mkImpl (mkNot sig redex) conc, vc)
	where conc = mkAny (frees sig prem) prem
 
 applySingle th@(F "===>" [F "True" _,conc]) redex t p sig vc =
        Just (replace t p $ mkImpl prem redex, vc)
 	where prem = mkAll (frees sig conc) conc

 applySingle th@(F "<===" [F "->" [left,right],prem]) redex t p sig vc
        | notnull p && isTerm sig redex =
 	          do (F "->" [_,r],0) <- Just (getSubterm t q,last p)
	             (f,rest) <- unify left
                     let eq = addTo True rest $ mkEq right r
		         eqs = substToEqs f $ domSub xs f
			 reduct = mkConjunct $ eq>>>f:prem>>>f:eqs
	                 reduct' = quantify sig addAnys [left,right,prem] reduct
                     Just (replace t q reduct',vc)
                  where xs = frees sig redex; reds = mkElems redex; q = init p
		  	unify (F "^" ts) = unifyAC ts reds V sig xs
			unify t          = unifyAC [t] reds V sig xs

 applySingle th@(F "<===" [at,prem]) redex t p sig vc =
     case unify0 at redex t p sig xs of
          TotUni f -> Just (replace t p $ reduct',vc)
		      where dom = domSub xs f
	                    reduct = mkConjunct $ prem>>>f:substToEqs f dom
			    reduct' = quantify sig addAnys [at,prem] reduct
          _ -> do F _ [F "=" [left,right], F "True" _] <- Just th
                  TotUni f <- Just $ unify0 left redex t p sig xs
	          let dom = domSub xs f
	              ts = prem>>>f:substToEqs f dom
		      bind = quantify sig addAnys [left,right,prem] . mkConjunct
	          (q,at,r) <- atomPosition sig t p
	          Just (replace t q $ bind $ replace at r right>>>f:ts,vc)
     where xs = frees sig redex

 applySingle th@(F "===>" [at,conc]) redex t p sig vc =
       do TotUni f <- Just $ unify0 at redex t p sig xs
	  let dom = domSub xs f
	      reduct = mkDisjunct $ conc>>>f:substToIneqs f dom
	      reduct' = quantify sig addAlls [at,conc] reduct
	  Just (replace t p $ reduct',vc)
       where xs = frees sig redex

 applySingle at redex t p sig vc = 
       applySingle (mkHorn at mkTrue) redex t p sig vc
       
-- used by Ecom > applyTheorem

 applyMany forward different left right redices t ps pred qs sig vc =
       do Def (f,True) <- Just $ unify' left redices (V"") t 
                                        (replicate (length left) []) ps V sig xs
          let dom = domSub xs f
              (mk1,mk2,addQuants,subst)
	          = if forward then (mkDisjunct,mkConjunct,addAlls,substToIneqs)
	                       else (mkConjunct,mkDisjunct,addAnys,substToEqs)
	      reduct1 = mk1 $ right>>>f:subst f dom
	      reduct2 = quantify sig addQuants (right:left) reduct1
	      newIndices = map head qs
	      ts = subterms $ getSubterm t pred
	      us = zipWith f newIndices $ map tail qs
	           where f i p = replace (ts!!i) p reduct2
	      vs = [ts!!i | i <- indices_ ts, i `notElem` newIndices]
	      reduct3 = if different then mk2 $ mk1 us:vs else mk1 $ us++vs
          Just (replace t pred reduct3,vc)
       where xs = joinMap (frees sig) redices
       
-- used by Ecom > finishDisCon
			    
 applyToHeadOrBody sig g head axs = f
  where f (t:cls) vc = 
          case t of F x [h,b] | x `elem` words "<=== ===>" 
	              -> if head then (F x [redh,b]:clsh,vch)
		       	         else (F x [h,redb]:clsb,vcb)
			 where (redh,clsh,vch) = redvc h
			       (redb,clsb,vcb) = redvc b
	            _ -> (redt:clst,vct) where (redt,clst,vct) = redvc t
          where redvc t = (reduct,cls',vc3)
		          where (axs',vc1) = renameApply sig t vc axs
		                (reduct,vc2) = applyPar axs' ps t vc1
		                (cls',vc3) = f cls vc2
		                ps = filter (g t) $ positions t
                -- applyPar axs t ps sig applies axs in parallel at all 
		-- positions of t that are in ps.
                applyPar axs (p:ps) t vc = 
                   if p `notElem` positions t || isVarRoot sig redex 
		   then proceed0
                   else if sig.isDefunct sym
                        then case atomPosition sig t p of 
		             Just (q,at,r)
			       -> case apply at r of
                                  Backward reds vc -> proceed mkDisjunct reds vc
				  _ -> proceed0
	                     _ -> proceed0
                   else if sig.isPred sym 
	                then case apply redex [] of
		                  Backward reds vc -> proceed mkDisjunct reds vc
				  _ -> proceed0
	           else if sig.isCopred sym 
	                then case apply redex [] of
                                  Forward reds vc -> proceed mkConjunct reds vc
				  _ -> proceed0
	           else proceed0
                   where redex = getSubterm t p
		         sym = getOp redex
			 cls = filterClauses sig redex axs
			 apply at r = fst $ applyAxs cls cls [] redex at r sig
			 			     vc True
                         proceed0 = applyPar axs ps t vc
			 proceed mk = applyPar axs ps . replace t p . mk
                applyPar _ _ t vc = (t,vc)
        f _ vc = ([],vc)

-- used by Ecom > applyInd

-- mkComplAxiom sig ax transforms an axiom ax for p into an axiom for NOTp. 

 mkComplAxiom sig t =
         case t of F "==>" [guard,t]        -> mkImpl guard $ mkComplAxiom sig t
                   F "===>" [t,F "False" _] -> t
	           F "===>" [t,u]           -> mkHorn (neg t) $ f $ neg u
	           F "<===" [t,u]           -> mkCoHorn (neg t) $ f $ neg u
	           t                        -> mkCoHorn t mkFalse
         where neg = mkNot sig
	       f = simplifyIter sig
	       
-- used by Ecom > negateAxioms

-- flatten k xs cl turns cl into an equivalent clause cl' such that all
-- equations t=u of cl' are flat wrt xs, i.e. either root(t) is in xs and all
-- other symbols of t or u are not in xs or u=t is flat wrt xs.

 flatten k [] cl                     = (cl,k)
 flatten k xs (F "==>" [guard,cl])   = (F "==>" [guard,cl'],n)
                                       where (cl',n) = flatten k xs cl
 flatten k xs (F "<===" [conc,prem]) = mkFlatEqs k xs conc prem
 flatten k xs at                     = mkFlatEqs' k xs at

 mkFlatEqs k xs conc prem = 
                       if null tps && null ups then (mkHorn conc prem,k)
		       else mkFlatEqs n xs conc' (mkConjunct (prem':eqs1++eqs2))
		       where tps = flatCands xs [] conc
		             ups = flatCands xs [] prem
			     (conc',eqs1,m) = mkEqs tps conc [] k
			     (prem',eqs2,n) = mkEqs ups prem [] m

 mkFlatEqs' k xs at = if null tps then (at,k) 
 			          else mkFlatEqs n xs at' (mkConjunct eqs)
	     	      where tps = flatCands xs [] at
		            (at',eqs,n) = mkEqs tps at [] k

-- mkEqs tps t [] k creates for each (u,p) in tps an equation u=zn with n>=k and
-- replaces u in t by zn.

 mkEqs ((u,p):tps) t eqs k = mkEqs tps (replace t p new) (eqs++[mkEq u new]) 
                                                         (k+1)
 			     where new = newVar k
 mkEqs _ t eqs k           = (t,eqs,k)

-- flatCands xs [] t returns the list of pairs (u,p) such that u is the subterm
-- of t at position p and the root of u is in xs, but u is not the left- or
-- right-hand side of a flat equation.

 flatCands xs p (F "=" [l,r])
   	         | x `elem` xs = concat (zipWith (flatCands xs) ps (concat tss))
		                 ++ flatCands xs p1 r
  		 | y `elem` xs = flatCands xs p0 l ++
		                 concat (zipWith (flatCands xs) qs (concat uss))
                                 where (x,tss) = unCurry l
		                       (y,uss) = unCurry r
				       p0 = p++[0]
		                       p1 = p++[1]
		                       ps = curryPositions p0 tss
		                       qs = curryPositions p1 uss
 flatCands xs p t | getOp t `elem` xs = [(t,p)]
 flatCands xs p (F _ ts) = concat $ zipWithSucs (flatCands xs) p ts
 flatCands _ _ _         = []
 
-- curryPositions [] [t1,...,tn] = ps1++...++psn implies that for all 1<=i<=n,
-- psi are the root positions of ti within f(t1)...(tn).

 curryPositions _ []   = []
 curryPositions p [ts] = succsInd p ts
 curryPositions p tss  = map (0:) (curryPositions p $ init tss) ++
 			 succsInd p (last tss)

-- preStretch prem f t checks whether the premises (prem = True) or conclusions
-- (prem = False) of t can be stretched. If so, then preStretch returns their 
-- leading function/relation x and the positions varps of all subterms to be 
-- replaced by variables. f is a condition on x.

 preStretch :: Bool -> (String -> Bool) -> TermS -> Maybe (String,[Int])
 preStretch prem f t = 
    case t of F "&" ts -> do s <- mapM g ts
		             let (xs,tss@(ts:uss)) = unzip s
				 varps = joinMap toBeReplaced tss `join`
				         [i | i <- indices_ ts,
					      any (neqTerm (ts!!i) . (!!i)) uss]
			     guard $ allEqual xs && allEqual (map length tss)
			     Just (head xs,varps)
	      F "==>" [F "=" [u,_],v] -> do (x,ts) <- g $ if prem then u else v
	      		 	            Just (x,toBeReplaced ts)
	      F "==>" [u,v] -> do (x,ts) <- g $ if prem then u else v
	      		 	  Just (x,toBeReplaced ts)
	      F "=" [u,_] -> do (x,ts) <- g $ if prem then u else t
	      			Just (x,toBeReplaced ts)
	      _ -> do guard $ not prem; (x,ts) <- g t; Just (x,toBeReplaced ts)
    where g t = do guard $ f x; Just (x,concat tss) where (x,tss) = unCurry t
	  toBeReplaced ts = [i | i <- indices_ ts, let t = ts!!i; x = root t,
	  			 isF t || not (noExcl x) ||
				 any (x `isin`) (context i ts) ]

-- stretchConc k ns t replaces the subterms of t at positions ns by variables
-- zk,z(k+1),... and turns t into a Horn axiom to be used by a proof by fixpoint
-- coinduction.

 stretchConc,stretchPrem :: Int -> [Int] -> TermS -> (TermS,Int)
 
 stretchConc k ns (F "&" (cl:cls)) = (mkHorn conc $ mkDisjunct $ prem:map g cls,
 				      n)
	 where (F "<===" [conc,prem],n) = f cl
	       f (F "==>" [prem,t]) = (mkHorn conc $ mkConjunct $ prem:eqs,n)
			  where (x,tss) = unCurry t
			        (us,eqs,n) = addEqs (concat tss) [] [] k ns 0
			        conc = mkApplys (x,mkLists us $ map length tss)
 	       f t = f $ F "==>" [mkTrue,t]
	       g (F "==>" [prem,t]) = mkConjunct $ prem:eqs
			              where (_,tss) = unCurry t
			                    eqs = addEqs0 (concat tss) [] k ns 0
 	       g t = g $ F "==>" [mkTrue,t]
 stretchConc k ns cl = stretchConc k ns $ F "&" [cl]

-- stretchPrem k ns t replaces the subterms of t at positions ns by variables
-- zk,z(k+1),... and turns t into a co-Horn axiom to be used by a proof by
-- fixpoint induction.

 stretchPrem k ns (F "&" (cl:cls)) = (mkCoHorn prem $ mkConjunct $ conc:cls',
				      maximum $ n:ks)
  where (F "===>" [prem,conc],n) = f cl
	f (F "==>" [F "=" [t,r],conc]) = (mkCoHorn prem $ mkImpl prem' conc,m)
		    where (x,tss) = unCurry t
			  (us,eqs,n) = addEqs (concat tss) [] [] k ns 0
			  u = mkApplys (x,mkLists us (map length tss))
			  (r',eqs',m) = if isV r && root r `notElem` map root us
			                then (r,eqs,n)
	                                else (new,mkEq new r:eqs,n+1)
			  prem = mkEq u r'; prem' = mkConjunct eqs'
			  new = newVar n
 	f (F "==>" [t,conc]) = (mkCoHorn prem $ mkImpl prem' conc,n)
	                  where (x,tss) = unCurry t
			        (us,eqs,n) = addEqs (concat tss) [] [] k ns 0
			        prem = mkApplys (x,mkLists us (map length tss))
			        prem' = mkConjunct eqs
	f (F "=" [t,r]) = (mkCoHorn prem $ mkImpl prem' $ mkEq new r,n+1)
			  where (x,tss) = unCurry t
				(us,eqs,n) = addEqs (concat tss) [] [] k ns 0
				u = mkApplys (x,mkLists us $ map length tss)
		                new = newVar n
				prem = mkEq u new
	                	prem' = mkConjunct eqs
	(cls',ks) = unzip $ map g cls
        g (F "==>" [F "=" [t,r],conc]) = (mkImpl (mkConjunct eqs') conc,m)
	               where (x,tss) = unCurry t
			     (us,eqs,n) = addEqs (concat tss) [] [] k ns 0
			     (eqs',m) = if isV r && root r `notElem` map root us
			                then (eqs,n)
					else (mkEq (newVar n) r:eqs,n+1)
 	g (F "==>" [t,conc]) = (mkImpl (mkConjunct eqs) conc,n)
			       where (x,tss) = unCurry t
			             eqs = addEqs0 (concat tss) [] k ns 0
	g (F "=" [t,r]) = (mkImpl (mkConjunct eqs) $ mkEq (newVar n) r,n+1)
			  where (x,tss) = unCurry t
				eqs = addEqs0 (concat tss) [] k ns 0
 stretchPrem k ns cl = stretchPrem k ns $ F "&" [cl]

-- For each term t of ts at a position in ns, addEqs ts [] [] k ns 0 replaces t
-- by a new variable zn for some n >= k and creates the equation zn=t.

 addEqs :: [TermS] -> [TermS] -> [TermS] -> Int -> [Int] -> Int 
		   -> ([TermS],[TermS],Int)
 addEqs [] us eqs k _ _  = (us,eqs,k)
 addEqs ts us eqs k [] _ = (us++ts,eqs,k)
 addEqs (t:ts) us eqs k ns n =
           if n `elem` ns then addEqs ts (us++[u]) (eqs++[mkEq u t]) (k+1) ms n'
                          else addEqs ts (us++[t]) eqs k ms n'
           where u = newVar k; ms = ns`minus1`n; n' = n+1
	     
 addEqs0 :: [TermS] -> [TermS] -> Int -> [Int] -> Int -> [TermS]
 addEqs0 [] eqs _ _ _      = eqs
 addEqs0 _ eqs _ [] _      = eqs
 addEqs0 (t:ts) eqs k ns n = addEqs0 ts eqs' k' (ns`minus1`n) (n+1)
	    where (eqs',k') = if n `elem` ns then (eqs++[mkEq (newVar k) t],k+1)
					     else (eqs,k)

 replaceByApprox i x = f 
	            where f (F "$" [t,u]) = if x == getOp t 
	                                    then applyL (F "$" [addLoop t,i]) ts
			                    else applyL t ts
				      where ts = case u of F "()" us -> map f us
					   		   _ -> [f u]
                          f (F y ts)      = if x == y 
				            then F (x++"Loop") $ i:map f ts
				            else F x $ map f ts
			  f t             = t

-- used by Esolve > mk{Co}HornLoop

 updArgsA (F "$" [t,_]) i ts = applyL (F "$" [addLoop t,i]) ts
 updArgsA (F x _) i ts       = F (x++"Loop") $ i:ts
 updArgsA t _ _              = t

-- used by Esolve > mk{Co}HornLoop

 addLoop (F "$" [t,u]) = F "$" [addLoop t,u]
 addLoop (F x ts)      = F (x++"Loop") ts
 addLoop t             = t

 mkEqsWithArgs sig zs is = zipWith mkEq zs . contextM is . getArgs

-- used by Esolve > mk{Co}HornLoop,compressAll

-- mkHornLoop sig x transforms the co-Horn axioms for x into an equivalent set
-- of three Horn axioms.

 mkHornLoop sig x axs i k = f axs
   where f axs = ([mkHorn (updArgs t zs) $ mkAll [root i] $ updArgsA t i zs,
                   updArgsA t mkZero zs,
                   mkHorn (updArgsA t (mkSuc i) zs) $ mkConjunct 
		   				    $ map mkPrem axs],
		  k')
           where t = g (head axs)
		 (x,ts) = getOpArgs t
		 k' = k+length ts
	         zs = map (V . ('z':) . show) [k..k'-1]
		 mkPrem (F "==>" [guard,F "===>" [t,u]]) = 
		 			    mkPrem $ mkCoHorn t $ mkImpl guard u
                 mkPrem cl@(F "===>" [t,u]) = simplifyIter sig conc
                               where conc = mkAll xs $ mkImpl (mkConjunct eqs) v
				     xs = frees sig cl `minus` getOpSyms t
                         	     eqs = mkEqsWithArgs sig zs [] t
				     v = replaceByApprox i x u
                 mkPrem t = t
	 g (F "==>" [_,cl])  = g cl
         g (F "===>" [at,_]) = at
 	 g t	             = t

-- used by Ecom > kleeneAxioms

-- mkCoHornLoop sig x transforms the Horn axioms for x into an equivalent set
-- of three co-Horn axioms.

 mkCoHornLoop sig x axs i k = f axs
   where f axs = ([mkCoHorn (updArgs t zs) $ mkAny [root i] $ updArgsA t i zs,
                   mkCoHorn (updArgsA t mkZero zs) mkFalse,
                   mkCoHorn (updArgsA t (mkSuc i) zs) $ mkDisjunct
			    			      $ map mkConc axs],
		  k')
           where t = g (head axs)
		 (x,ts) = getOpArgs t
		 k' = k+length ts
	         zs = map (V . ('z':) . show) [k..k'-1]
		 eqs = mkEqsWithArgs sig zs []
		 mkConc (F "==>" [guard,F "<===" [t,u]]) =
 			                mkConc $ mkHorn t $ mkConjunct [guard,u]
                 mkConc cl@(F "<===" [t,u]) = simplifyIter sig prem
                                    where prem = mkAny xs $ mkConjunct $ v:eqs t
                         	          xs = frees sig cl `minus` getOpSyms t
				          v = replaceByApprox i x u
                 mkConc t = simplifyIter sig $ mkAny xs $ mkConjunct $ eqs t
			    where xs = frees sig t `minus` getOpSyms t
	 g (F "==>" [_,cl])  = g cl
         g (F "<===" [at,_]) = at
 	 g t	             = t

-- used by Ecom > kleeneAxioms
	 
-- compressAll sig k axs transforms the axioms of axs into a single axiom
-- (if b = True) and inverts it (if b = False). 

 compressAll b sig i axs = compressOne b sig i [] axs $ h $ head axs
                           where h (F "==>" [_,cl])  = h cl
 		                 h (F "===>" [at,_]) = h at
 		                 h (F "<===" [at,_]) = h at
				 h (F "=" [t,_])     = t
				 h t	             = t
				 
-- used by Ecom > compressAxioms

 combineOne sig i ks ax axs = compressOne True sig i ks cls t
		          where cls = [axs!!i | i <- indices_ axs, all (f i) ks]
				t = h ax
				ts = getArgs t
			        tss = map (getArgs . h) axs
		                f i k = eqTerm ((tss!!i)!!k) (ts!!k) 
		                h (F "==>" [_,cl])  = h cl
 		                h (F "===>" [at,_]) = h at
 		                h (F "<===" [at,_]) = h at
		                h (F "=" [t,_])     = t
 		                h t	            = t

 compressOne b sig i ks cls t =
      if sig.isPred x then (g (updArgs t us) $ compressHorn sig eqs cls,j)
      else if sig.isDefunct x
	   then (g (mkEq (updArgs t us) z) $ compressHornEq sig eqs' cls,j+1)
	   else (h (updArgs t us) $ compressCoHorn sig eqs cls,j)
      where (x,ts) = getOpArgs t
            (us,j) = foldr f ([],i) $ indices_ ts
	    eqs = mkEqsWithArgs sig (map newVar [i..j-1]) ks
	    z = newVar j 
	    eqs' t u = mkEq z u:eqs t
	    f i (us,k) = if i `elem` ks then (ts!!i:us,k) else (newVar k:us,k+1)
	    (g,h) = if b then (mkHorn,mkCoHorn) else (mkCoHorn,mkHorn)

-- compressHorn sig eqs transforms Horn axioms for a predicate into the premise
-- of an equivalent single Horn axiom. 

 compressHorn sig eqs = mkDisjunct . map mkPrem
       where mkPrem (F "==>" [guard,F "<===" [t,u]]) =
 			                mkPrem $ mkHorn t $ mkConjunct [guard,u]
             mkPrem cl@(F "<===" [t,u]) = simplifyIter sig prem
                                    where prem = mkAny xs $ mkConjunct $ u:eqs t
                                          xs = frees sig cl `minus` getOpSyms t
             mkPrem t = simplifyIter sig $ mkAny xs $ mkConjunct $ eqs t
                        where xs = frees sig t `minus` getOpSyms t

-- compressHornEq sig eqs transforms Horn axioms for a defined function into the
-- premise of an equivalent single Horn axiom.

 compressHornEq sig eqs = mkDisjunct . map mkPrem
       where mkPrem (F "==>" [guard,F "<===" [t,u]]) =
 			                mkPrem $ mkHorn t $ mkConjunct [guard,u]
             mkPrem cl@(F "<===" [F "=" [t,u],v]) = simplifyIter sig prem
                                  where prem = mkAny xs $ mkConjunct $ v:eqs t u
			                xs = frees sig cl `minus` getOpSyms t
             mkPrem cl@(F "=" [t,u]) = simplifyIter sig prem
                                    where prem = mkAny xs $ mkConjunct $ eqs t u
				          xs = frees sig cl `minus` getOpSyms t
	     mkPrem t = t

-- compressCoHorn sig eqs transforms co-Horn axioms for a copredicate into the 
-- conclusion of an equivalent single co-Horn axiom.

 compressCoHorn sig eqs = mkConjunct . map mkConc
       where mkConc (F "==>" [guard,F "===>" [t,u]]) =
 			                    mkConc $ mkCoHorn t $ mkImpl guard u
             mkConc cl@(F "===>" [t,u]) = simplifyIter sig conc
                           where conc = mkAll xs $ mkImpl (mkConjunct (eqs t)) u
				 xs = frees sig cl `minus` getOpSyms t
	     mkConc t = t

-- moveUp sig vc x us is moves the quantifiers from positions 
-- q++[is!!0],...,q++[is!!length is-1] of t to position q. F x us is the
-- original term at position q. 
 
 moveUp sig vc "==>" us@[u,v] is = (as',es',F "==>" ts,vc')
  where split (ps,qs) i = if isAllQ t then ((i,alls t):ps,qs) 
  				      else (ps,(i,anys t):qs)
		          where t = us!!i
        [u1,v1] = zipWith g us [0,1]
	g u i = if i `elem` is then head $ subterms u else u
	h = renaming vc
	rename = renameFree sig
        (as',es',ts,vc') = 
	        case foldl split nil2 is of 
	        ([(0,as0),(1,as1)],[]) -> (map f as1,as0,[u1,rename f v1],vc1)
				          where (f,vc1) = h $ meet as0 as1
	        ([],[(0,es0),(1,es1)]) -> (es0,map f es1,[u1,rename f v1],vc1)
				          where (f,vc1) = h $ meet es0 es1
                ([(0,as)],[(1,es)])    -> ([],as++map f es,[u1,rename f v1],vc1)
		                          where (f,vc1) = h $ meet as es
	        ([(1,as)],[(0,es)])    -> (es++map f as,[],[u1,rename f v1],vc1)
	 		                  where (f,vc1) = h $ meet as es
	        ([(0,as)],[])          -> ([],map f as,[rename f u1,v1],vc1)
	      		                  where zs = frees sig v `meet` as
				                (f,vc1) = h zs
	        ([(1,as)],[])          -> (map f as,[],[u1,rename f v1],vc1)
	      		                  where zs = frees sig u `meet` as
				                (f,vc1) = h zs
	        ([],[(0,es)])          -> (map f es,[],[rename f u1,v1],vc1)
	      			          where zs = frees sig v `meet` es
				                (f,vc1) = h zs
	        ([],[(1,es)])          -> ([],map f es,[u1,rename f v1],vc1)
	       			          where zs = frees sig u `meet` es
				                (f,vc1) = h zs
 moveUp _ vc "Not" [u] _ = (anys u,alls u,F "Not" [head (subterms u)],vc)
 moveUp sig vc x us is   = (joinMap snd ps',joinMap snd qs',F x ts',vc')
	where (ps,qs) = foldl split nil2 is 
	      split (ps,qs) i = if isAllQ t then ((i,alls t):ps,qs) 
					    else (ps,(i,anys t):qs)
			        where t = us!!i
	      free = joinMap (frees sig) $ contextM is ts
	      ts = zipWith h us $ indices_ us
	           where h u i = if i `elem` is then head $ subterms u else u
	      (ts',vc',ps',qs') = loop1 (ts,vc,ps,qs) ps
	      loop1 (ts,vc,ps,qs) (p@(i,xs):ps1) = loop1 (ts',vc',ps',qs) ps1
                             where rest = ps `minus1` p
			           zs = if x == "&" then free 
				        else join free $ joinMap snd $ rest++qs
                                   (f,vc') = renaming vc (xs `meet` zs)
			           ts' = updList ts i $ renameFree sig f $ ts!!i
			           ps' = (i,map f xs):rest
	      loop1 state _ = loop2 state qs
	      loop2 (ts,vc,ps,qs) (q@(i,xs):qs1) = loop2 (ts',vc',ps,qs') qs1
                             where rest = qs `minus1` q
                                   zs = if x == "|" then free 
				        else join free $ joinMap snd $ rest++ps
                                   (f,vc') = renaming vc $ meet xs zs
			           ts' = updList ts i $ renameFree sig f $ ts!!i
			           qs' = (i,map f xs):rest
	      loop2 state _ = state
	      
-- used by Ecom > shiftQuants

 shiftSubformulas sig t ps =
   case search (isImpl . getSubterm1 t) qs of
        Just i | (p == left || p == right) && length ps > 1 && all (== r) rs
	  -> if p == left && r == right && isDisjunct conc 
	     then impl t q prem1 conc1
	     else do guard $ p == right && r == left && isConjunct prem
		     impl t q prem2 conc2
	     where p = ps!!i; q = qs!!i
		   left = q++[0]; right = q++[1]
		   r:rs = context i qs 
		   F _ [prem,conc] = getSubterm1 t q
		   ms = map last $ context i ps
		   ns cl = indices_ (subterms cl) `minus` ms
		   f i = getSubterm1 conc [i]
		   g i = getSubterm1 prem [i]
		   prem1 = mkConjunct $ map (mkNot sig . f) ms
		   conc1 = mkImpl prem $ mkDisjunct $ map f $ ns conc
		        -- mkDisjunct $ neg prem:map f (ns conc)
		   prem2 = mkConjunct $ mkNot sig conc:map g (ns prem)
		   conc2 = mkDisjunct $ map (mkNot sig . g) ms
	Nothing | notnull qs
          -> if all (== r) rs && isDisjunct u 
	     then impl t r (mkConjunct $ map (mkNot sig . v) ns) $
	     		   mkDisjunct $ map v ks
             else let r:rs = map init qs
		      u = getSubterm1 t r
		      F _ [prem,conc] = u
                      ns k = map last [ps!!i | i <- ms, last (qs!!i) == k]
	              cs = indices_ (subterms prem) `minus` ns 0
		      ds = indices_ (subterms conc) `minus` ns 1
		      pr i = getSubterm1 prem [i]
		      co i = getSubterm1 conc [i]
		      newImpl = mkImpl $ mkConjunct $ map pr $ ns 0
		      prems = map (mkNot sig . co) $ ns 1
		   -- concs = map (neg . pr) $ ns 0
		      prem1 = mkConjunct $ map pr cs++prems
		      conc1 = newImpl $ mkDisjunct $ map co ds
		           -- mkDisjunct $ concs++map co ds
		      prem2 = mkConjunct $ map pr cs
		      conc2 = newImpl conc
		           -- mkDisjunct $ conc:concs
		      prem3 = mkConjunct $ prem:prems
		      conc3 = mkDisjunct $ map co ds
		  in do guard $ all notnull qs && isImpl u && all (== r) rs
	                if isConjunct prem
		           then if isDisjunct conc then impl t r prem1 conc1
		              		           else impl t r prem2 conc2
		           else impl t r prem3 conc3
	     where r:rs = qs
                   u = getSubterm1 t r
		   ms = indices_ ps
		   ns = map last [ps!!i | i <- ms]
		   ks = indices_ (subterms u) `minus` ns
		   v i = getSubterm1 u [i]
	_ -> Nothing
   where qs = map init ps
         impl t p u v = Just $ replace1 t p $ mkImpl u v

-- used by Ecom > shiftSubs'

-- getOtherSides t p ts ps looks for in/equations eq in the premises/conclusions
-- of t such that one side of eq agrees with some u in ts. If so, u is replaced
-- by the other side of q. p and ps are the positions of t and ts, respectively.

 getOtherSides :: TermS -> [Int] -> [TermS] -> [[Int]] -> Maybe [TermS]
 getOtherSides t p ts ps =
     case t of F "==>" [F "&" prems,
     			F "|" concs] 	  -> f prems (p0 prems) concs $ p1 concs
               F "==>" [F "&" prems,conc] -> f prems (p0 prems) [conc] [p++[1]]
               F "==>" [prem,F "|" concs] -> f [prem] [p++[0]] concs $ p1 concs
               F "==>" [prem,conc]        -> f [prem] [p++[0]] [conc] [p++[1]]
	       F "&" prems	          -> f prems (succsInd p prems) [] []
	       F "|" concs                -> f [] [] concs $ succsInd p concs
	       _	                  -> Nothing
     where p0 = succsInd $ p++[0]
           p1 = succsInd $ p++[1]
	   b eqps i k = not (eqps!!i <<= ps!!k)
           f prems ps1 concs ps2 = Just (g1 ts [] prems 0)
	         where g1 ts rest1 (t@(F "=" [l,r]):rest2) i =
	                  case search (== l) ts of
	                  Just k | b ps1 i k
		            -> g1 (updList ts k r) (rest1++[t]) rest2 (i+1)
                          _ -> case search (== r) ts of
	 	               Just k | b ps1 i k
			         -> g1 (updList ts k l) (rest1++[t]) rest2 (i+1)
			       _ -> g1 ts (rest1++[t]) rest2 (i+1)
                       g1 ts rest1 (t:rest2) i = g1 ts (rest1++[t]) rest2 (i+1)
	               g1 ts _ _ _             = g2 ts [] concs 0 
	               g2 ts rest1 (t@(F "=/=" [l,r]):rest2) i =
	                  case search (== l) ts of
	                  Just k | b ps2 i k
		            -> g2 (updList ts k r) (rest1++[t]) rest2 (i+1)
                          _ -> case search (== r) ts of
	 	               Just k | b ps2 i k 
			         -> g2 (updList ts k l) (rest1++[t]) rest2 (i+1)
			       _ -> g2 ts (rest1++[t]) rest2 (i+1)
		       g2 ts rest1 (t:rest2) i = g2 ts (rest1++[t]) rest2 (i+1)
		       g2 ts _ _ _             = ts
		       
-- used by Ecom > replaceSubtrees

