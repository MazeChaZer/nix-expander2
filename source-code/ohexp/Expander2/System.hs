module System where
import Tk

----------------------------------------------------------------- March 26, 2019

infixl 6 &

data ExitCode = ExitSuccess | ExitFailure Int deriving (Eq,Ord,Read,Show)
 
primitive primSystem :: String -> Cmd Int   	 
primitive doesFileExist :: String -> Cmd Bool 	 
primitive doesDirectoryExist :: String -> Cmd Bool     
primitive createDirectory :: String -> Cmd () 	 
primitive getDirectoryContents :: String -> Cmd [String] 
primitive primGetAppDirectory :: String
primitive primGetFileSeparator :: Char

builtinLib file = "Examples" ++ fileSeparator:file

fileSeparator = primGetFileSeparator

userLibDir = primGetAppDirectory ++ fileSeparator:"ExpanderLib" 

userLib file = userLibDir ++ fileSeparator:file

pixpath file = userLib "Pix" ++ fileSeparator:file

mkDir,renewDir,system :: String -> Cmd ExitCode
mkDir dir    = do b <- doesDirectoryExist dir
	          if not b then system $ "mkdir " ++ dir else return ExitSuccess
renewDir dir = do system $ "rm -rf " ++ dir 
	          system $ "mkdir " ++ dir 
system cmd   = do ec <- primSystem cmd
                  return $ if ec == 0 then ExitSuccess else ExitFailure ec

mv :: String -> String -> Cmd ExitCode
mv file dir = system $ "mv -n " ++ file ++ ' ':dir
 
mkFile :: String -> Int -> String
mkFile dir i | i < 10  = enclose "00"
	     | i < 100 = enclose "0"
	     | True    = enclose ""
	       where enclose str = dir ++ fileSeparator:str ++ show i ++ ".png"

savePic :: String -> Canvas -> String -> Cmd String
savePic suffix canv file = case suffix of 
			   ".eps" -> do canv.save file; return file
			   ".gif" -> do canv.save epsfile 
			   		system $ "convert " ++ epsfile ++ 
			   			 ' ':file
			   	        system $ "rm -f " ++ epsfile
			   	        return file
			   ".png" -> do canv.save epsfile
			   		system $ "convert " ++ epsfile ++ 
			   			 ' ':file
			   		system $ "convert "++ file ++ " -trim "
			   			 ++ file	 
			   	        system $ "rm -f " ++ epsfile
			   	        return file
			   _ -> savePic ".png" canv $ file ++ ".png"
			   where epsfile = drop (length file-4) file ++ ".eps"

lookupLibs :: TkEnv -> String -> Cmd String
lookupLibs tk file = tk.readFile (userLib file)
		     `catch` \_ -> tk.readFile (builtinLib file) 
		     `catch` \_ -> return ""

loadPhoto :: TkEnv -> String -> Cmd Photo
loadPhoto tk file = do b <- doesFileExist path
	               if b then tk.photo [File path] 
	                    else tk.photo [File $ builtinLib file ++ ".gif"]
	                         `catch` \_ -> tk.photo []
	            where path = userLib file ++ ".gif"

(&) :: String -> String -> String
x & val = x ++ "=\"" ++ val ++ "\""

html :: TkEnv -> String -> String -> [String] -> Cmd ()
html tk dirPath dir files = 
  if null files then done
  else tk.writeFile (dirPath ++ ".html") $ 
    "<html>\n<head>\n<title>" ++ dir ++ 
    "</title>\n<script type"&"text/javascript" ++ 
    ">\nvar images = new Array(" ++ '\"':first ++ '\"':concatMap f rest ++ 
    ")\n</script>\n<script type"&"text/javascript" ++ " src"&"Painter.js" ++ 
    ">\n</script>\n</head>\n<body style"&"background-color: rgb(221,221,255)" ++
    ">\n<input type"&"button" ++ " value"&"|<<" ++ " onclick"&"backStop(this)"++ 
    ">\n<input type"&"button" ++ " value"&"<" ++ " onclick"&"back()" ++ "> " ++ 
    show n ++ " picture" ++ (if n == 1 then "" else "s") ++
    "\n<input type"&"button" ++ " value"&">" ++ " onclick"&"forth()" ++ 
    ">\n<input type"&"button" ++ " value"&">>|" ++" onclick"&"forthStop(this)"++ 
    ">\n<input type"&"button" ++ " value"&"loop" ++ " onclick"&"loop(this)" ++ 
    ">\n<br><input id"&"fileSlider" ++ " type"&"range" ++ " min"&"0" ++ 
    " max"&show (n-1) ++ " value"&"0" ++ " onchange"&"showFile(this.value)" ++ 
    ">\n<span id"&"file" ++ '>':first ++ "</span>\n<br>\n<input type"&"range" ++
    " min"&"0" ++ " max"&"2000" ++ " value"&"30" ++ 
    " onchange"&"showTime(this.value)" ++ ">\n<span id"&"time" ++ 
    ">30</span> millisecs\n<br><br><br>\n<iframe id"&"img" ++ " src"&first ++ 
    " width"&"1800" ++ " height"&"900" ++ " frameborder"&"0" ++
    ">\n</iframe>\n</body>\n</html>"
  where first:rest = files
        n = length files
        f file = ",\"" ++ file ++ "\""

mkHtml :: TkEnv -> Canvas -> String -> String -> Int -> Cmd ()
mkHtml tk canv dir dirPath n = do 
       file <- savePic ".png" canv $ mkFile dirPath n
       files <- getDirectoryContents dirPath
       html tk dirPath dir [dir++fileSeparator:file | 
                            file <- files, let lg = length file, lg > 4, 
                            drop (lg-4) file 
		                   `elem` words ".eps .gif .jpg .pdf .png .svg"] 

mkSecs t1 t2 = (t2-t1)`div`1001500

{-
data OSType = Unknown | Windows | Unix | Dos | RiscOS 
	      deriving (Eq, Read, Show, Enum, Ord)

primitive primGetOS :: Int
            
getOS :: OSType
getOS = toEnum primGetOS     
-}