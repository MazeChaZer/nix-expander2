module Tpaint where
 import Tk

 infixr 5  <:>, <++>

 main tk = do paint <- painter tk
  	      paint.buildPaint

 round2 (x,y) = (round x,round y)

 minmax4 p (0,0,0,0) = p
 minmax4 (0,0,0,0) p = p
 minmax4 (x1,y1,x2,y2) (x1',y1',x2',y2') = (min x1 x1',min y1 y1',
 					    max x2 x2',max y2 y2')

-- minmax ps computes minimal and maximal coordinates for the point list ps.

 minmax s@((x,y):_) = foldl minmax1 (x,y,x,y) s
       where minmax1 (x1,y1,x2,y2) (x,y) = (min x x1,min y y1,max x x2,max y y2)
 minmax _ 	    = (0,0,0,0)

 map2 f = map (\(x,y) -> (f x,f y))

 type Picture = [Widget_]

 type Arcs = [[Int]]

 data Widget_ = Arc Color ArcStyleType Point Float (Float,Float) | 
                Arc0 State ArcStyleType Float Float | Circ State Float | 
		CircA State Float | Dot Color Point |
		Path Color Int [Point] | Path0 State Int [Point] | 
		Poly State Int [Float] Float | Rect State Float Float | 
		RectA State Float Float | Snow State Int Float | 
		Tria State Float | Turtle State Float [TurtleAct]
 		deriving (Show,Eq)

 data TurtleAct = Move Float | MoveA Float | Jump Float | JumpA Float | 
		  Turn Float | Open Color Int | Scale Float | Close | Draw | 
		  Widg Widget_
		  deriving (Show,Eq)

 type State = (Point,Float,Color,Int)

 type Point = (Float,Float)

 type Line_ = (Point,Point)

 type TNode = (String,Point)

 instance Eq ArcStyleType where Chord == Chord = True
				Pie == Pie     = True
				_ == _         = False

 p0 :: Point
 p0 = (0,0)

 state0 :: State
 state0 = (p0,0,black,0)
 
 path0 c = Path0 (p0,0,c,0) 

 turtle = Turtle state0 1

 jturtle acts = Just [turtle acts]

 jturtles actss = Just (map turtle actss)

 open = Open black 0

 close2 = [Close,Close]

 splitPath ps = if length ps < 101 then [ps]
		                   else take 100 ps:splitPath (drop 99 ps)

-- hilbert n and hilbertC n compute the vertices of a (colored) Hilbert curve 
-- with depth n.

 hilbert n = path0 black 0 (gen n 2 (-2) 0 0 p0)
   where gen 0 _ _  _ _  p = [p]
	 gen n c c' d d' p = pict1++pict2++pict3++pict4
	     where f = gen (n-1)
		   pict1 = f d d' c c' p
	           p1 = last pict1
                   pict2 = f c c' d d' (fst p1+c,snd p1+d)
	           p2 = last pict2
                   pict3 = f c c' d d' (fst p2+d,snd p2+c)
	           p3 = last pict3
                   pict4 = f (-d) (-d') (-c) (-c') (fst p3+c',snd p3+d')
		              
-- successor p (angle p q) (distance p q) = q

 successor (x,y) a r = (x+r*cos rad,y+r*sin rad) 
		       where rad = 0.0174533*a      	-- pi/180

 angle (x1,y1) (x2,y2) = 57.2958*atan2 (y2-y1) (x2-x1)  -- 180/pi

 distance (x1,y1) (x2,y2) = sqrt (x21*x21+y21*y21) where x21 = x2-x1
	  		          			 y21 = y2-y1

 nextLight n i (RGB x y z) = RGB (f x) (f y) (f z)
		             where f x | i > 0 = x+i*(255-x)`div`n
			               | True  = x+i*x`div`n

 mkLight = nextLight 42

 light = nextLight 3 2

 dark = nextLight 3 (-1)

 grey      = RGB 200 200 200
 magenta   = RGB 255 0 255
 cyan      = RGB 0 255 255
 orange    = RGB 255 180 0
 darkGreen = RGB 0 150 0

 nextCol (RGB 255 n 0) | k >= 0   = RGB 255 k 0  	-- yellow --> red
                       | True     = RGB 255 0 (-k)      
				    where k = n-5       
 nextCol (RGB 255 0 n) | k <= 255 = RGB 255 0 (n+5)  	-- red --> magenta
                       | True     = RGB (510-k) 0 255    
				    where k = n+5    
 nextCol (RGB n 0 255) | k >= 0   = RGB k 0 255  	-- magenta --> blue
                       | True     = RGB 0 (-k) 255        
				    where k = n-5       
 nextCol (RGB 0 n 255) | k <= 255 = RGB 0 k 255  	-- blue	--> cyan
                       | True     = RGB 0 255 (510-k)       
				    where k = n+5   
 nextCol (RGB 0 255 n) | k >= 0   = RGB 0 255 k 	-- cyan	--> green 
                       | True     = RGB (-k) 255 0      
				    where k = n-5      
 nextCol (RGB n 255 0) | k <= 255 = RGB k 255 0  	-- green --> yellow
		       | True     = RGB 255 (510-k) 0
				    where k = n+5    
 nextCol _			  = grey

 nextColScale n c = iterate nextCol c!!div 306 n
                                     -- 306 = 6*255/5 = length(domain(nextCol))

-- rotate p a q rotates q by a around p.

 rotate _ 0 p                      = p 			-- cos 0 = 1, sin 0 = 0 
 rotate p@(x,y) a q@(i,j) | p == q = p 			 
                          | True   = ((i-x)*k-(j-y)*l+x,(i-x)*l+(j-y)*k+y)
                                     where (k,l) = (cos rad,sin rad) 
 		                           rad = 0.0174533*a 

 Move a<:>(Move b:acts)   = Move (a+b):acts
 MoveA a<:>(MoveA b:acts) = MoveA (a+b):acts
 Jump a<:>(Jump b:acts)   = Jump (a+b):acts
 JumpA a<:>(JumpA b:acts) = JumpA (a+b):acts
 Turn a<:>(Turn b:acts)   = Turn (a+b):acts
 act<:>(act':acts)        = act:(act'<:>acts)
 act<:>_                  = [act]

 (act:acts)<++>acts' = act<:>(acts<++>acts')
 _<++>acts           = acts

-- coeffs (x,y) (x',y') = (a,b) <==> a*y+b = x & a*y'+b = x'

 coeffs (x,y) p = (a,y-a*x) where a = slope (x,y) p

 slope (x,y) (x',y') = if x == x' then fromInt primMaxInt else (y'-y)/(x'-x) 

-- straight ps checks whether ps represents a straight line.

 straight (p:ps@(q:r:_)) = straight3 p q r && straight ps
 straight _		 = True

 straight3 p@(x1,_) q@(x2,_) r@(x3,_) | x1 == x2 = x2 == x3
				      | x2 == x3 = x1 == x2
				      | True     = coeffs p q == coeffs q r
        
 reduceP (p:ps@(q:r:s)) | straight3 p q r = reduceP (p:r:s)
                        | True            = p:reduceP ps
 reduceP ps                               = ps                    

 noColor c = c `elem` [black,white]

 whiteCol c = c == RGB 1 2 3

 outColor c i = if whiteCol c then white else mkLight i c

 fillColor c i = if noColor c || whiteCol c then white else mkLight i c

 isWidg (Arc0 _ _ _ _)         = True
 isWidg (CircA _ _)            = True    
 isWidg (Path0 _ _ _)          = True    
 isWidg (Poly _ n _ _) | n < 4 = True    
 isWidg (Rect _ _ _)           = True    
 isWidg (RectA _ _ _)          = True    
 isWidg (Tria _ _)             = True    
 isWidg _                      = False

 isPict (Poly _ 4 _ _) = True    
 isPict (Snow _ _ _)   = True
 isPict (Turtle _ _ _) = True
 isPict _              = False    

 getState (Arc c _ p _ _)     = (p,0,c,0)
 getState (Arc0 st _ _ _)     = st
 getState (Circ st _)         = st
 getState (CircA st _)        = st
 getState (Dot c p)           = (p,0,c,0)
 getState (Path c _ ps)       = (head ps,0,c,0)
 getState (Path0 st _ _)      = st
 getState (Poly st _ _ _)     = st
 getState (Rect st _ _)       = st
 getState (RectA st _ _)      = st
 getState (Snow st _ _ )      = st
 getState (Tria st _)         = st
 getState (Turtle st _ (_:_)) = st
 getState _                   = state0

 coords w = p where (p,_,_,_) = getState w
  
 updState f w@(Arc _ t _ r ab)  = Arc (mkLight i c) t p r ab 
			 	  where (p,_,c,i) = f (getState w)
 updState f (Arc0 st t r a)     = Arc0 (f st) t r a
 updState f (Circ st r)         = Circ (f st) r 
 updState f (CircA st r)        = CircA (f st) r 
 updState f w@(Dot c _)         = Dot (mkLight i c) p 
				  where (p,_,c,i) = f (getState w)
 updState f w@(Path _ n ps)     = Path (mkLight i c) n ps 
				  where (_,_,c,i) = f (getState w)
 updState f (Path0 st n ps)     = Path0 (f st) n ps
 updState f (Poly st n rs a)    = Poly (f st) n rs a
 updState f (Rect st b h)       = Rect (f st) b h
 updState f (RectA st b h)      = RectA (f st) b h
 updState f (Snow st n r)       = Snow (f st) n r
 updState f (Tria st r)         = Tria (f st) r
 updState f (Turtle st sc acts) = Turtle (f st) sc (map g acts)
			     where g (Open c n) = Open d n
						  where (_,_,d,_) = f (p0,0,c,0)
                                   g (Widg w)   = Widg (updState f w)
			           g act        = act
 updState _ w = w

 moveWidg p a = updState (\(_,_,c,i) -> (p,a,c,i))

-- mkWidg (w (p,a,c) ...) rotates widget w around p by a.
-- mkWidg is used by mkPict, points, drawWidget and hullPoints (see below).

 mkWidg (Arc0 (p,a,c,i) typ r b)     = Arc (mkLight i c) typ p r (-a,b)
 mkWidg (CircA st r)		     = Circ st r
 mkWidg (Path0 (p@(x,y),a,c,i) n ps) = Path (mkLight i c) n
                                            (map (rotate p a . f) ps) 
				       where f (i,j) = (x+i,y+j)
 mkWidg (Poly (p,a,c,i) n (r:rs) b) = Path (mkLight i c) n (last ps:ps)
 		               where (ps,_) = foldl f ([successor p a r],a+b) rs  
                                     f (ps,a) r = (successor p a r:ps,a+b) 
 mkWidg (Rect (p@(x,y),a,c,i) b h) = Path (mkLight i c) n (last qs:qs)
                                    where n = if noColor c then 0 else 2
                                          ps = [(x1,y1),(x1,y2),(x2,y2),(x2,y1)]
                                          x1 = x-b; x2 = x+b; y1 = y-h; y2 = y+h
					  qs = map (rotate p a) ps
 mkWidg (RectA st b h)		   = Rect st b h
 mkWidg (Tria (p@(x,y),a,c,i) r)   = Path (mkLight i c) n (last qs:qs)
                                        where n = if noColor c then 0 else 2
	                                      ps = [(x-lg,z),(x,y-r),(x+lg,z)]
         		                      lg = r*0.86602 
                                              -- r*(3/(2*sqrt 3)) = sidelength/2
	                                      z = y+lg*0.57735  -- y+lg*sqrt 3/3
					      qs = map (rotate p a) ps
 mkWidg w = w

 mkSlice (Poly (p,a,c,i) n (r:rs) b) = Path (mkLight i c) n (p:ps++[p])
 		               where (ps,_) = foldl f ([successor p a r],a+b) rs
                                     f (ps,a) r = (successor p a r:ps,a+b) 

-- mkPict (Poly (p,a,c,i) 4 rs b) computes the triangles of a rainbow polygon 
-- with center p, orientation a, inner color c, lightness value i, radia rs and
-- increment angle b.
-- mkPict (Snow (p,a,c,i) n r) computes the triangles of a Koch snowflake with
-- center p, orientation a, inner color c, lightness value i, depth n and radius
-- r.
-- mkPict (Turtle (p,a,c,i) sc acts) translates acts into the picture drawn by
-- a turtle that executes acts, starting out from point p with scale factor sc, 
-- orientation a, color c and lightness value i.
-- mkPict is used by points, drawWidget, splitTurts and hullPoints (see below).

 mkPict (Poly (p,a,c,i) _ (r:rs) b) = Path (h c') 2 [p,q,q']:pict
                where p' = successor p a r
                      (pict,q,a',c') = foldl f ([],p',a+b,c) rs
		      f (pict,q,a,c) r = (Path (h c) 2 [p,q,q']:pict,q',a+b,g c)
			                 where q' = successor p a r
	              g = if noColor c || whiteCol c 
                          then const c else nextColScale (length rs+1)
                      h = mkLight i
                      q' = successor p a' r
 mkPict (Snow (p,a,c,i) n r) = tria c p r True:tria c p r False:
			       f (g c) (n-1) r True [p]
    where tria c q r b = Path d n (map (rotate p a) ps)
		         where Path d n ps = mkWidg (Tria (q,0,c,i) 
						    (if b then -r else r))
          f _ 0 _ _ _  = []
          f c n r b ps = alternate b qs++f (g c) (n-1) r' b qs
                   where alternate b (p:ps) = tria c p r' b:alternate (not b) ps
 	                 alternate _ _      = []
	                 r' = r*0.333333
	  	         qs = concatMap circle ps 
                         circle p@(x,y) = take 6 (iterate (rotate p 60) (x,z))
	                                  where z = if b then y-r+r' else y+r-r'
          g = if noColor c || whiteCol c then const c else nextColScale n
 mkPict (Turtle (p,a,c,i) sc acts) = g pict c' n ps
  where (pict,(_,_,c',n,_,ps):_) = foldl f ([],[(p,a,c,0,sc,[p])]) acts
        f (pict,(p,a,c,n,sc,ps):s) (Move d)  = (pict,(q,a,c,n,sc,ps++[q]):s) 
  					       where q = successor p a (d*sc)
        f (pict,(p,a,c,n,sc,ps):s) (MoveA d) = (pict,(q,a,c,n,sc,ps++[q]):s) 
  					       where q = successor p a d
        f (pict,(p,a,c,n,sc,ps):s) (Jump d)  = 
	  (g pict c n ps,(q,a,c,n,sc,[q]):s)   where q = successor p a (d*sc)
        f (pict,(p,a,c,n,sc,ps):s) (JumpA d) = 
	  (g pict c n ps,(q,a,c,n,sc,[q]):s)   where q = successor p a d
        f (pict,(p,a,c,n,sc,ps):s) (Turn b)  = (pict,(p,a+b,c,n,sc,ps):s)
        f (pict,s@((p,a,c,m,sc,_):_)) (Open d n)   = (pict,(p,a,d,n,sc,[p]):s)
        f (pict,s@((p,a,c,n,sc,ps):_)) (Scale sc') = 
	  (pict,(p,a,c,n,sc*sc',ps):s) 
        f (pict,(_,_,c,n,_,ps):s) Close      = (g pict c n ps,s)
        f (pict,(p,a,c,n,sc,ps):s) Draw      = (g pict c n ps,(p,a,c,n,sc,[p]):s)
        f (pict,(p,a,c,n,sc,ps):s) (Widg w)  = 
          (pict++[scaleWidg sc (moveWidg p a w)],(p,a,c,n,sc,ps):s)
        f state _ = state
        g pict c n ps = if length ps < 2 
                        then pict else pict++[Path (mkLight i c) n (reduceP ps)]

 points :: Widget_ -> [Hull]
 points (Arc c t p r (a,b)) = points (g (Poly (p,a,c,0) n rs (-b/36)))
                                where g = if t == Chord then mkWidg else mkSlice
				      n = if noColor c then 0 else 2
				      rs = replicate 37 r
 points (Path c _ ps) = [(ps,[],c)]
 points w | isWidg w    = points (mkWidg w)
          | isPict w    = concatMap points (mkPict w)
 points w               = [([p],[],mkLight i c)] where (p,_,c,i) = getState w

 type Hull = ([Point],[Point],Color)

 spoints :: Hull -> [Point]
 spoints (ps,qs,_) = if null qs then ps else qs

 font9  = Font "Helvetica 9"
 font10 = Font "Helvetica 10 italic"
 font12 = Font "Helvetica 12"
 font12b = Font "Helvetica 12 bold"

-- the PAINTER template

 struct Painter = buildPaint :: Action
		  
 painter :: TkEnv -> Template Painter
 painter tk =

   template (canv,font,splitBut,win) 
		:= (undefined,undefined,undefined,undefined)
   	    (widthX,widthY,scale,picture,split) := (0,0,5,[hilbert 1],True)

   in let

	adaptPos (x,y) = do (leftDivWidthX,_) <- canv.xview
	                    (topDivWidthY,_) <- canv.yview
	                    return (x+round (leftDivWidthX*fromInt widthX),
			            y+round (topDivWidthY*fromInt widthY))

	buildPaint = action
          win0 <- tk.window [Title "Painter"]
	  win := win0
 	  win.setPosition (60,60)
  	  win.setSize (1000,600)
  	  canv0 <- win.canvas [Background white]
	  canv := canv0
  	  vsb <- win.scrollBar [Width 12]
	  vsb.attach canv Ver
	  hsb <- win.scrollBar [Width 12]
	  hsb.attach canv Hor
          closeBut <- win.button [Text "quit", font12, Command tk.quit]
          numLab <- win.label [Text "depth", font10, Anchor C]
 	  numSlider <- win.slider [Orientation Hor, From 1, To 10,
				   CmdInt moveNum]
	  numSlider.setValue 1
	  numSlider.bind [ButtonRelease 1 (const scaleAndDraw)]
	  scaleLab <- win.label [Text "size", font10, Anchor C]
	  scaleSlider <- win.slider [Orientation Hor, From 1, To 20, 
	                             CmdInt moveScale]
	  scaleSlider.setValue 5
	  scaleSlider.bind [ButtonRelease 1 (const scaleAndDraw)]
 	  splitBut0 <- win.button [Text "no split", font12, Command setSplit]
	  splitBut := splitBut0
	  pack (col [row [canv, fillY vsb],
		     fillX hsb,
		     fillX (row [col [scaleSlider,scaleLab],
		     	         col [numSlider,numLab],
				 splitBut,closeBut])])
          scaleAndDraw
          
        close = action win.iconify

	drawPict pict = action mapM_ drawWidget pict

	drawWidget (Arc c t (x,y) r ab) = action
	  canv.arc (round2 (x-r,y-r)) (round2 (x+r,y+r))
	           [Outline (outColor c 0), Fill (fillColor c 0), 
	            Angles (round2 ab), ArcStyle t]; done
	drawWidget (Circ ((x,y),_,c,i) r) = action
	  canv.oval (round2 (x-r,y-r)) (round2 (x+r,y+r))
	            [Outline (outColor c i), Fill (fillColor c i)]; done
	drawWidget (Dot c p)     = drawWidget (Circ (p,0,c,0) 5)
        drawWidget (Path c n ps) = action
	  let paths = if split then splitPath ps else [ps]
	      act f opts = mapM (flip f (opts n). map round2) paths
          if length ps > 1
	     then if n < 2 || n > 3 then act canv.line optsL; done
		                    else act canv.polygon optsP; done
          where optsL :: Int -> [LineOpt]
	        optsL 0 = [Fill (outColor c 0)]
		optsL 1 = Smooth True:optsL 0
                optsL n = Width 3:optsL (n-4)
		optsP :: Int -> [PolygonOpt]
		optsP 2 = [Outline (outColor c 0), Fill (fillColor c 0)]
                optsP _ = Smooth True:optsP 2
	drawWidget w | isWidg w = drawWidget (mkWidg w)
	             | isPict w = drawPict (mkPict w)
        drawWidget _ 		= skip

        moveScale n = action scale := fromInt n

        moveNum n = action picture := [hilbert n]

	scaleAndDraw = action
	  canv.clear
	  let (pict1,(x1,y1,x2,y2)) = f picture 0
              f (w:pict) i = (w':pict',minmax4 (widgFrame w') bds)
	                     where w' = scaleWidg scale w
				   (pict',bds) = f pict (i+1)
              f _ _        = ([],(0,0,0,0))
              pict2 = map (transXY (5-x1) (5-y1)) pict1
	  picture := map (scaleWidg (recip scale)) pict2
          widthX := max 100 (round (x2-x1+10))
	  widthY := max 100 (round (y2-y1+10))
          canv.set [ScrollRegion (0,0) (widthX,widthY)]
	  drawPict pict2

	setSplit = action 
	  split := not split
	  splitBut.set [Text (if split then "no split" else "split")]
	  scaleAndDraw

        skip = action done

      in struct ..Painter

-- SCALING and FRAMING

-- scaleWidg sc w scales w by multiplying its vertices/radia with scale. 
-- Dots and Gifs are not scaled. scaleWidg is used by mkPict, getEnclosed,
-- scaleAndDraw, scalePict (see above), widgFrame and getWidget (see below).

 scaleWidg sc (Arc0 st t r a)      = Arc0 st t (r*sc) a
 scaleWidg sc (Circ st r)          = Circ st (r*sc)
 scaleWidg sc (Path0 st n ps)      = Path0 st n (map2 (*sc) ps)
 scaleWidg sc (Poly st n rs a)     = Poly st n (map (*sc) rs) a 
 scaleWidg sc (Rect st b h)        = Rect st (b*sc) (h*sc)
 scaleWidg sc (Snow st n r)        = Snow st n (r*sc)
 scaleWidg sc (Tria st r)          = Tria st (r*sc)
 scaleWidg sc (Turtle st sc' acts) = Turtle st (sc*sc') acts
 scaleWidg _ w 			   = w

-- widgFrame w returns the leftmost-uppermost and rightmost-lowermost
-- coordinates of w. widgFrame is used by scaleAndDraw (see above), rectFrame 
-- and shelves (see below).

 widgFrame (Arc0 (p,a,c,_) t r b) = minmax ps
	                          where [(ps,_,_)] = points (Arc c t p r (a,b))
 widgFrame (Circ ((x,y),_,_,_) r)     = (x-r,y-r,x+r,y+r)
 widgFrame (CircA ((x,y),_,_,_) r)    = (x-r,y-r,x+r,y+r)
 widgFrame (Dot _ (x,y))              = (x-5,y-5,x+5,y+5)
 widgFrame (Path _ _ [])              = (0,0,0,0)
 widgFrame (Turtle st@(p,a,_,_) sc acts) =
				    minmax (fst (foldl f ([p],[(p,a,sc)]) acts))
  where f (ps,(p,a,sc):s) (Move d)        = (p:q:ps,(q,a,sc):s)
                                            where q = successor p a (d*sc)
        f (ps,(p,a,sc):s) (MoveA d)       = (p:q:ps,(q,a,sc):s)
                                            where q = successor p a d
        f (ps,(p,a,sc):s) (Jump d)        = (ps,(q,a,sc):s)
                                            where q = successor p a (d*sc)
        f (ps,(p,a,sc):s) (JumpA d)       = (ps,(q,a,sc):s)
                                            where q = successor p a d
        f (ps,(p,a,sc):s) (Turn b)        = (ps,(p,a+b,sc):s)
        f (ps,_:s) Close                  = (ps,s)
        f state Draw                      = state
        f (ps,s@((p,a,sc):_)) (Scale sc') = (ps,(p,a,sc*sc'):s)
        f (ps,s@((p,a,sc):_)) (Widg w)    = ((x1,y1):(x2,y2):ps,s)
	   where (x1,y1,x2,y2) = widgFrame (scaleWidg sc (moveWidg p a w))
        f (ps,s@(st:_)) _                 = (ps,st:s)
 widgFrame w = minmax (concatMap spoints (points w))

-- functions used for INTERACTION with the painter

-- transX/transY offset w moves w offset units to the right/bottom.

 transX 0 w      = w
 transX offset w = moveWidg (x+offset,y) a w where ((x,y),a,_,_) = getState w

 transY 0 w      = w
 transY offset w = moveWidg (x,y+offset) a w where ((x,y),a,_,_) = getState w

 transXY 0 offset w        = transY offset w
 transXY offset 0 w        = transX offset w
 transXY offsetX offsetY w = moveWidg (x+offsetX,y+offsetY) a w
                             where ((x,y),a,_,_) = getState w
