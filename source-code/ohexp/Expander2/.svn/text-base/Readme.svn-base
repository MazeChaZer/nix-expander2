______________________________________________________________________________
 __ , ___    ___   ___    ___   __________   __________
/_/  /  /   /  /  /  /   /  /  /  _______/  /  _______/   The O'Haskell User's
    /  /___/  /  /  /   /  /  /  / _____   /  /______         Gofer System
   /  ____   /  /  /   /  /  /  / /_   /  /______   /
  /  /   /  /  /  /___/  /  /  /___/  /  _______/  /          Version 0.5
 /__/   /__/  /_________/  /_________/  /_________/            Jan 2001

                Copyright (c) Mark P Jones, Johan Nordlander,
                       Bj�rn von Sydow, Magnus Carlsson,
   Oregon Graduate Institute, Chalmers University of Technology, 1994-2001.

______________________________________________________________________________

 This is the Readme file for O'Hugs 0.5, the O'Haskell User's Gofer System.

 O'Hugs 0.5  is an interpreter for O'Haskell, an object-oriented extension to 
 the functional language Haskell.  The intepreter provides an almost complete 
 implementation  of  Haskell 1.3;  the  only feature that is not supported is 
 the module system.  To  this  base  O'Haskell adds polymorphic subtyping and
 monadic reactive objects, as described in the pro tempore language report:

    http://www.cs.chalmers.se/~nordland/thesis.ps
   
 Version 0.5 of the interpreter implements all of these extensions,  with the
 exception  of  subtype constrained  type annotations.  New features  in this 
 release include:

 - A more refined interface to Tk, including support for the 
   optional Tk extension Tix.

 - Support for network programming via TCP and UDP.

 - Much improved library documentation.

 - Local universal quantification in records.

 - Local existential quantification in datatypes.

 - Templates with automatic instantiation of local sub-objects.

 - Overloading using smallest supertypes for contravariant type classes.

 - Recursive generator syntax.

 - Macintosh and Windows binaries.

 - Several bug-fixes.


 For more information and dowload options, please visit
 
    http://www.cs.chalmers.se/~nordland/ohaskell/

 Your feedback, comments, suggestions and bug reports are most welcome!
______________________________________________________________________________
Johan Nordlander                                       nordland@cse.ogi.edu
http://www.cse.ogi.edu/~nordland
Department of Computer Science and Engineering,
Oregon Graduate Institute of Science and Technology
______________________________________________________________________________
