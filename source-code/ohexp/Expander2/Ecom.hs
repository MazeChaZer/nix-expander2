module Ecom where
 import Esolve
 import Epaint

------------------------------ Copyright (c) peter.padawitz@udo.edu, May 3, 2020

-- Ecom contains the main program, the solver and the enumerator.
 
 main :: TkEnv -> Cmd ()
 main tk = do
  mkDir $ userLibDir
  mkDir $ userLib "Pix"
  win1 <- tk.window []
  win2 <- tk.window []
  fix solve1 <- solver tk "Solver1" win1 solve2 "Solver2" enum1 paint1
      solve2 <- solver tk "Solver2" win2 solve1 "Solver1" enum2 paint2
      paint1 <- painter 820 tk "Solver1" solve1 "Solver2" solve2
      paint2 <- painter 820 tk "Solver2" solve2 "Solver1" solve1 
      enum1 <- enumerator tk solve1
      enum2 <- enumerator tk solve2
  solve1.buildSolve (0,20)
  solve2.buildSolve (20,20)
  win2.iconify

 data PossAct = Add [[Int]] | Remove [Int] | Replace [[Int]]

-- PROOFS

 struct ProofElem = msg,msgL,treeMode :: String
 		    trees 	      :: [TermS]
		    treePoss	      :: [[Int]]
		    curr	      :: Int
		    perms	      :: Int -> [Int]
		    varCounter	      :: String -> Int
		    newPreds	      :: ([String],[String])
		    solPositions      :: [Int]
		    substitution      :: (SubstS,[String])
		    constraints	      :: (Bool,[String])
		    joined,safe	      :: Bool
  
 showDeriv proof trees solPositions = concat (zipWith f (indices_ proof) proof) 
 				      ++ solsMsg
	         where f i proofElem = show i ++ ". " ++ proofElem.msg ++ "\n\n"
	               sols = map (trees!!) solPositions
                       solsMsg = if null sols then "" 
		                 else "Solutions:" ++ concatMap f sols
	                         where f t = "\n\n" ++ showTree False t

 showCurr fast t tm = "\nThe current "++tm++" is given by\n\n"++ showTree fast t

 showNew fast 1 t msg n ps tm = preceding msg ps tm n ++ "a single " ++ tm ++
 			        ",\nwhich is given by\n\n" ++ showTree fast t 
 showNew fast k t msg n ps tm = preceding msg ps tm n ++ show k ++ ' ':tm ++ 
 			        "s." ++ showCurr fast t tm

 showPre fast t msg n ps tm = preceding msg ps tm n ++ '\n':'\n':showTree fast t

 preceding msg ps tm n = msg ++ (if take 3 msg `elem` ["BUI","MIN"] then ""
  			         else str1 ++ showPS ps ++ tm ++ 's':
                                     (str2 `onlyif` nrs)) ++ " leads to "  
     where str1 = if last msg == '\n' then "" else " "
	   str2 = " (" ++ if n == 1 then "one step)" else show n++ " steps)"
           nrs = take 3 msg `elem` words "NAR REW SIM"
           showPS []  = ("to " `ifnot` nrs) ++ "the preceding "
           showPS ps = "at positions" ++ concatMap f ps ++ "\nof the preceding "
	   f p = '\n':show p `minus1` ' '
 
-- PROOF TERM functions

 deriveStep (Mark _)        = False
 deriveStep (Matching _)    = False
 deriveStep (Refuting _)    = False
 deriveStep (Simplifying _) = False
 deriveStep (SetStrat _)    = False
 deriveStep _               = True

 command = concat 
           [symbol "AddAxioms" >> list linearTerm >>= return . AddAxioms,
	    symbol "ApplySubst" >> return ApplySubst,
	    do symbol "ApplySubstTo"; x <- token quoted
	       enclosed linearTerm >>= return . ApplySubstTo x,
            symbol "ApplyTransitivity" >> return ApplyTransitivity,
	    symbol "BuildKripke" >> token int >>= return . BuildKripke,
	    symbol "BuildRE" >> return BuildRE,
	    symbol "CollapseStep" >> token bool >>= return . CollapseStep,
	    symbol "CollapseVars" >> list noBlanks >>= return . CollapseVars,
	    symbol "ComposePointers" >> return ComposePointers,
	    symbol "CopySubtrees" >> return CopySubtrees,
	    symbol "CreateIndHyp" >> return CreateIndHyp,
	    symbol "CreateInvariant" >> token bool >>= return . CreateInvariant,
	    symbol "DecomposeAtom" >> return DecomposeAtom,
	    symbol "DerefNodes" >> return DerefNodes,
	    symbol "EvaluateTrees" >> return EvaluateTrees,
	    do symbol "ExpandTree"; b <- token bool
	       token int >>= return . ExpandTree b,
	    symbol "FlattenImpl" >> return FlattenImpl,
	    symbol "Generalize" >> list linearTerm >>= return . Generalize,
	    do symbol "Induction"; b <- token bool
	       token int >>= return . Induction b,
	    symbol "Mark" >> list (list int) >>= return . Mark,
	    symbol "Matching" >> token int >>= return . Matching,
	    symbol "Minimize" >> return Minimize,
	    symbol "ModifyEqs" >> token int >>= return . ModifyEqs,
	    do symbol "Narrow"; limit <- token int
	       token bool >>= return . Narrow limit,
            do symbol "NegateAxioms"; ps <- list quoted
               list quoted >>= return . NegateAxioms ps,
	    symbol "PermuteSubtrees" >> return PermuteSubtrees,
	    symbol "RandomLabels" >> return RandomLabels,
	    symbol "RandomTree" >> return RandomTree,
	    symbol "ReduceRE" >> token int >>= return . ReduceRE,
	    symbol "RefNodes" >> return RefNodes,
	    symbol "Refuting" >> token bool >>= return . Refuting,
	    symbol "ReleaseNode" >> return ReleaseNode,
	    symbol "ReleaseSubtree" >> return ReleaseSubtree,
	    symbol "ReleaseTree" >> return ReleaseTree,
	    symbol "RemoveCopies" >> return RemoveCopies,
	    symbol "RemoveEdges" >> token bool >>= return . RemoveEdges,
	    symbol "RemoveNode" >> return RemoveNode,
	    symbol "RemoveOthers" >> return RemoveOthers,
	    symbol "RemovePath" >> return RemovePath,
	    symbol "RemoveSubtrees" >> return RemoveSubtrees,
	    symbol "RenameVar" >> token quoted >>= return . RenameVar,
	    symbol "ReplaceNodes" >> token quoted >>= return . ReplaceNodes,
	    symbol "ReplaceOther" >> return ReplaceOther,
	    do symbol "ReplaceSubtrees"; ps <- list $ list int
	       list linearTerm >>= return . ReplaceSubtrees ps,
	    symbol "ReplaceText" >> token quoted >>= return . ReplaceText,
	    do symbol "ReplaceVar"; x <- token quoted; u <- enclosed linearTerm
	       list int >>= return . ReplaceVar x u,
	    symbol "ReverseSubtrees" >> return ReverseSubtrees,
	    symbol "SafeEqs" >> return SafeEqs,
            do symbol "SetAdmitted"; block <- token bool
               list quoted >>= return . SetAdmitted block,
            do symbol "SetCurr"; msg <- token quoted
               token int >>= return . SetCurr msg,
	    symbol "SetStrat" >> token strategy >>= return . SetStrat,
	    symbol "ShiftPattern" >> return ShiftPattern,
	    symbol "ShiftQuants" >> return ShiftQuants,
	    symbol "ShiftSubs" >> list (list int) >>= return . ShiftSubs,
	    do symbol "Simplify"; limit <- token int
	       token bool >>= return . Simplify limit,
            symbol "Simplifying" >> token bool >>= return . Simplifying,
            symbol "SplitTree" >> return SplitTree,
            symbol "SubsumeSubtrees" >> return SubsumeSubtrees,
            do symbol "Theorem"; b <- token bool
               enclosed linearTerm >>= return . Theorem b,
	    symbol "Transform" >> token int >>= return . Transform,
	    symbol "UnifySubtrees" >> return UnifySubtrees]

 linearTerm = concat [do symbol "F"; x <- token quoted
                         list linearTerm >>= return . F x,
                      symbol "V" >> token quoted >>= return . V]
  
-- SOLVER messages

 start = "Welcome to Expander2 (July 23, 2020)"

 startOther solve = "Load and parse a term or formula in " ++ solve ++ "!"
 
 addAxsMsg msg = "The set of axioms is extended by\n\n" ++ drop 4 msg

 applied b str = "Applying the " ++ s ++ str ++ "\n\n"
           where s = if b then if '!' `elem` str then "induction hypothesis\n\n"
				                 else "axiom or theorem\n\n"
		          else "axioms\n\n"

 appliedToSub str 1 = "A " ++str++" step has been applied to the selected tree."
 appliedToSub str n = show n ++ " " ++ str ++
                      " steps have been applied to the selected trees."

 arcInserted p q = "An arc from position " ++ show p ++ " to position " ++
 		    show q ++ " has been inserted."

 atomDecomposed = "The selected atom has been decomposed."

 atomNotDecomposed = "The selected atom cannot be decomposed."

 admitted :: Bool -> [String] -> String
 admitted True []  = "All simplifications are admitted."
 admitted block xs = (if block then "All simplifications except those for "
                               else "Only simplifications for ") ++
		     showStrList xs ++ " are admitted."

 axsRemovedFor xs = "The axioms for " ++ showStrList xs ++ " have been removed."
                   
 badNoProcs = "Enter a positive number of processes into the entry field."

 check rest = "\n--CHECK FROM HERE:\n" ++ rest

 circle p q = "The operation fails because the current tree contains a back " ++
   	      "pointer from position "++ show p ++" to position "++ show q ++"."

 circlesUnfolded 1 = "\nCircles were unfolded one time."
 circlesUnfolded n = "\nCircles were unfolded " ++ show n ++ " times."

 copiesRemoved = "Copies of the selected tree have been removed."

 collapsed = "The selected tree has been collapsed."

 collapsedVars = "The variables of the selected tree have been collapsed."
 
 complsAdded xs = "Axioms for the complement" ++ str ++ showStrList xs ++ 
 	          " have been added."
		  where str = case xs of [_] -> " of "; _ -> "s of "

 concCallsPrem 
             = "Some conclusion contains the root of the corresponding premise."

 creatingInvariant str ax 
        = str ++ " for the iterative program\n\n" ++ showTree False ax ++ "\n\n"
        
 dereferenced = "The selected pointers have been dereferenced."
 
 edgesRemoved True = "Cycles have been removed."
 edgesRemoved _    = "Forward arcs have been turned into tree edges."

 dangling = "The selected subtree cannot be removed because it is referenced."
 
 emptyPicDir = "The picture directory is empty."

 emptyProof = "The proof is empty."

 emptySubst = "The substitution is empty."
 
 endOfProof = "The end of the derivation has been reached."
 
 enterNumber = "Enter the number of a formula shown in " ++ tfield ++ "!"

 enterNumbers = "Enter numbers of formulas shown in " ++ tfield ++ "!"

 enterTfield str = "Enter " ++ str ++ " into " ++ tfield ++ "!"
 
 eqInverted = "The selected equation has been inverted."
 
 equationRemoval True = "Equation removal is safe." 
 equationRemoval _    = "Equation removal is unsafe." 
 
 eqsButMsg True = "unsafe equation removal"
 eqsButMsg _    = "safe equation removal"

 eqsModified = "The selected regular equations have been modified."

 evaluated = "The selected trees have been evaluated."

 expanded = "The selected trees have been expanded."

 extendedSubst = "The equations in " ++ tfield ++
 	         " have been added to the substitution."

 finishedNar True 1 = "A narrowing step has been performed.\n"
 finishedNar _    1 = "A rewriting step has been performed.\n"
 finishedNar True n = show n ++ " narrowing steps have been performed.\n"
 finishedNar _ n    = show n ++ " rewriting steps have been performed.\n"
		      
 finishedSimpl 1 = "A simplification step has been performed.\n"
 finishedSimpl n = show n ++ " simplification steps have been performed.\n"
 
 flattened = "The selected clause has been flattened."
 
 formString b = if b then "formula" else "term"

 generalized = "The selected formula has been generalized with the formulas" ++
	       " in " ++ tfield ++ "."

 illformed str = "The " ++ str ++ " in " ++ tfield ++ " is not well-formed."

 indApplied str = str ++ " has been applied to the selected formulas."

 indHypsCreated [x] = "The induction variable is " ++ showStr x ++
                      ".\nInduction hypotheses have been added to the theorems."
		      ++ "\nGive axioms for the induction ordering >>!"

 indHypsCreated xs = "The induction variables are " ++ showStrList xs ++
     ".\nInduction hypotheses have been added to the theorems." ++
     "\nSpecify the induction ordering >> on " ++ show (length xs) ++ "-tuples!"

 iniSigMap = "The signature map is the identity."

 iniSpec = "The specification consists of built-in symbols. "
 
 instantiateVars = "Select iterative equations and variables on right-hand " ++
    "sides to be expanded!\nRecursively defined variables are not expanded!"

 invCreated b =
    "The selected formula has been transformed into conditions on a " ++
    (if b then "Hoare" else "subgoal") ++ " invariant INV.\nAdd axioms for INV!"

 kripkeBuilt mode procs sts labs ats = 
    "A " ++ f mode ++ " Kripke model with " ++ 
    (if procs > 0 then show procs ++ " processes, " else "") ++ 
    show' sts "state" ++ ", " ++ show' ats "atom" ++ " and " ++ 
    show' labs "label" ++ " has been built from the " ++ g mode 
    where f 0 = "cycle-free"; f 1 = "pointer-free"; f _ = ""
          g 3 = "current graph."; g 4 = "regular expression."; g _ = "axioms."          

 kripkeMsg = "BUILDING A KRIPKE MODEL"
 
 leavesExpanded = "The selected trees have been expanded at their leaves."

 levelMsg i = "The current tree has been collapsed at " ++
 	      (if i == 0 then "leaf level." else "the " ++ show (i+1) ++
               " lowest levels.")

 loaded file = file ++ " has been loaded into the text field."

 minimized n = "The Kripke model has been minimized. It has " ++ 
 	        show' n "state" ++ "."

 moved = "The selected quantifiers have been moved to the parent node."

 newCls cls file = "The " ++ cls ++ str ++ file ++ " have been added."
 		   where str = if file == tfield then " in " else " of "

 newCurr = "The tree slider has been moved."

 newInterpreter eval draw = eval ++" is the actual widget-term interpreter. "++
			    (if null draw then "No draw function" else draw) ++
			    " is applied before painting."
 
 newPredicate str1 str2 x = "The " ++ str1 ++ ' ':x ++ 
 			    " has been turned into a " ++ str2 ++ "."

 newSig file = "The symbols in " ++ file ++ " have been added to the signature."

 newSigMap file = "The assignments in " ++ file ++ 
                  " have been added to the signature map."
        
 noApp :: String -> String
 noApp str = str ++ " is not applicable to the selected trees."
        
 noAppT :: Maybe Int -> String
 noAppT k = case k of Just n -> "Theorem no. " ++ show n ++ str
 		      _ -> "The theorem in the text field" ++ str       
	    where str = " is not applicable to the selected trees."

 noAxiomsFor xs = "There are no axioms for " ++ showStrList xs ++ "."

 nodesReplaced a = "The roots of the selected trees have been replaced by "
                   ++ showStr a ++ "."

 notDisCon = "The theorem to be applied is not a distributed clause."

 noTheorem k = (case k of Just n -> "No. " ++ show n
                          _ -> "The clause in the text field") ++
	       " is neither an axiom nor a theorem."

 noTheoremsFor xs = "There are no theorems for " ++ showStrList xs ++ "."

 notInstantiable = 
 	        "The selected variable cannot be instantiated at this position."

 notUnifiable k = (case k of Just n -> "Theorem no. " ++ show n
                             _ -> "The theorem in the text field") ++
	          " is not unifiable with any selected redex."

 noUnifier = "The selected subtrees are not unifiable."

 ocFailed x = x ++ " cannot be unified with a term that contains " ++ x ++
	      " and is not a variable."

 onlyRew = "\nThe selected tree is a term. " ++
           "Hence formula producing axioms are not applicable!"
	      
 orderMsg str = "The nodes of the selected tree have been labelled with " ++
 		str ++ "."

 partialUnifier = "The selected trees are only partially unifiable."

 permuted = "The list of maximal proper subtrees has been permuted."

 pointersComposed = "The pointers in the selected trees have been composed."
 
 posInSubst = "There is a pointer in a substitution."

 proofLoaded file = "The proof term has been loaded from " ++ file ++ "."
        
 referenced = "Selected nodes have been turned into pointers."

 regBuilt = "A regular expression has been built from the Kripke model."

 regReduced = "The selected regular expressions have been reduced."

 removed = "The selected trees have been removed."

 removedOthers = "All trees except the one below have been removed."

 replacedTerm = "The selected terms have been replaced by equivalent ones."

 replaceIn solve = "The subtree has been replaced by the tree of "++solve++"."

 reversed = "The list of selected (sub)trees has been reversed."
 
 see str = "See the " ++ str ++ " in the text field."
		     
 selectCorrectSubformula = "Select an implication, a conjunction or a " ++
                           "disjunction and subterms to be replaced!"
                           
 selectSub = "Select a proper non-hidden subtree!"
 
 shifted =
      "The selected subformulas have been shifted to the premise or conclusion."
 	 
 show' 1 obj = "one " ++ obj
 show' n obj = show n ++ ' ':obj ++ "s"

 sigMapError other = "The signature map does not map to the signature of " ++
                     other ++ "."

 simplifiedPar = "A simplification step has been applied to the root position "
                 ++ "of each selected tree."

 solved [n]    = "A solution is at position " ++ show n ++ "."
 solved (n:ns) = "Solutions " ++ "are at positions " ++
                 splitString 27 63 (mkRanges ns n) ++ "."
 solved _      = "There are no solutions."

 stretched = "The selected conditional equation has been stretched."

 subMsg str x = str ++ " has been substituted for " ++ x ++ "."

 subsAppliedToAll = "The substitution has been applied to all variables."

 subsAppliedTo x = "The substitution has been applied to " ++ x ++ ". "

 subsumed object = "The selected tree results from the subsumption of a " ++
 		   object ++ "."

 subtreesNarrowed k = (case k of Just n 
   				   -> if n == -1 then "Axioms are not"
                         	      else "Theorem no. " ++ show n ++ " is not"
 				 _ -> "No clause is") ++
		      " applicable to the selected trees."

 thApplied k = (case k of Just n -> if n == -1 then "Axioms have"
                         	    else "Theorem no. " ++ show n ++ " has" 
                          _ -> "The clauses in the text field have") ++
	       " been applied to the selected trees."
 
 termsStored = "Storable subterms of the selected trees have been stored."

 textInserted = "The tree in "++tfield++" replaces/precedes the selected trees."
            
 tfield = "the text field"

 transformed = "The selected graph has been transformed."

 transitivityApplied 
 	      = "The transitivity axiom has been applied to the selected atoms."

 treeParsed = 
         "The text field contains a string representation of the selected tree."

 treesSimplified = "The trees are simplified."

 unified object = "The selected tree results from the unification of two " ++
 		  object ++ "s."

 unifiedT = "The selected trees have been unified. "

 wrongArity f lg = "The number in the entry field is too big. " ++ f ++
                   " has only " ++ show lg ++ " arguments."
 
 enumerators  = words "alignment palindrome dissection" ++ ["level partition",
 		      "preord partition","heap partition","hill partition"]

 interpreters = words "tree widgets overlay matrices" ++
                ["matrix solution","linear equations","level partition",
                 "preord partition","heap partition","hill partition"]

 specfiles1 = 
   words "abp account align auto1 auto2 base bool bottle bottleac bottlef" ++
   words "btree cobintree coin coregs cse cycle dna echo echoac election" ++
   words "factflow flowers FP gauss graphs hanoi hanoilab hinze infbintree" ++
   words "kino knight kripke1 kripke2"

 specfiles2 = 
   words "lazy lift liftlab list listeval listrev log log4 lr1 lr2 micro" ++
   words "modal mutex mutexco mutexquad nat NATths needcoind newman obdd" ++ 
   words "obst partn penroseS phil philac polygons prims prog puzzle queens"

 specfiles3 = 
   words "regeqs relalg relalgdb relalgp robot ROBOTacts sendmore set" ++
   words "shelves simpl stack STACKimpl STACKimpl2 stream trans0 trans1" ++
   words "trans2 turtles widgets zip"

-- the SOLVER template

 solver :: TkEnv -> String -> Window -> Solver -> String -> Enumerator
                 -> Painter -> Template Solver
 solver tk this win solve other enum paint =

   template (applyBut,backBut,canv,canvSlider,checker,deriveBut,treeSlider,ent,
             fastBut,font,forwBut,hideBut,interpreterBut,lab,matchBut,narrowBut,
             quit,randomBut,safeBut,simplBut,splitBut,stratBut,tedit,termBut,
             lab2)
              := (undefined,undefined,undefined,undefined,undefined,undefined,
	          undefined,undefined,undefined,undefined,undefined,undefined,
		  undefined,undefined,undefined,undefined,undefined,undefined,
		  undefined,undefined,undefined,undefined,undefined,undefined,
		  undefined)
	    (ctree,node,penpos,subtree,isSubtree,suptree,osci)
	      := (Nothing,Nothing,Nothing,Nothing,Nothing,Nothing,Nothing)
	    (formula,joined,safe,firstMove,showState,firstClick)
	      := (True,True,True,True,True,True)
	    (fast,checking,checkingP,simplifying,refuting,collSimpls,
	     newTrees,restore) 
	     := (False,False,(False,False),False,False,False,False,False)
	    (canvSize,corner,curr,curr1,matching,noProcs,proofPtr,proofTPtr,
	     picNo,stateIndex,simplStrat) 
	      := ((0,0),(30,20),0,0,0,0,0,0,0,0,PA)
	    (axioms,conjects,indClauses,matchTerm,oldTreeposs,proof,proofTerm,
	     refuteTerm,ruleString,simplRules,simplTerm,solPositions,specfiles,
	     stratTerm,terms,theorems,transRules,treeposs,trees) 
	      := ([],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[])
	    numberedExps := ([],True); constraints := (True,[])
	    (drawFun,picEval,picDir) := ("","tree","picDir")
	    signatureMap := (id,[]); newPreds := nil2; part := (id,[])
	    substitution := (V,[]); treeMode := "tree"
	    symbols := iniSymbols; rand := seed; sizeState := sizes0
	    spread := (10,30); times := (0,300); maxHeap := 100; speed := 500
	    (counter,varCounter) := (const 0,const 0); perms := \n -> [0..n-1]
	    kripke := ([],[],[],[],[],[],[])

   in let
        
        mkSub m text = m.cascade [CLabel text, font12]
 	
        mkBut m text cmd = m.mButton [CLabel text, font12, Command cmd]
	  
	mkButF m cmd file = mkBut m file $ cmd file
	
	setCmd text cmd = win.button [Text text, font12, groove, Command cmd]

	buildSolve leftUpCorner = action
	
 	  win.set [Title this]
	  win.setPosition leftUpCorner
--	  win.setSize (1275,855)  		-- for 1280 x 1024, 60 Hz
--  	  win.setSize (1020,730)  		-- for 1024 x 768, 60 Hz
	  win.setSize (1440,820)  		-- for 1440 x 900

  	  canv0 <- win.canvas [Height 100, Background white]
	  canv := canv0
	  canv.bind [ButtonPress 1 catchSubtree,
	     	     ButtonPress 2 catchTree,
		     ButtonPress 3 catchNode,
	             Motion 1 moveSubtree,
	     	     Motion 2 moveTree,
		     Motion 3 moveNode,
	     	     ButtonRelease 1 releaseSubtree,
	     	     ButtonRelease 2 releaseTree,
		     ButtonRelease 3 releaseNode]

	  canvSlider0 <- win.slider [From $ -40, To 600, Orientation Ver,
				     CmdInt setExtension]
	  canvSlider := canvSlider0
	  canvSlider.setValue 100

	  let act n = action curr1 := if n < length trees then n else curr
	      takeCurr _ = action setCurr newCurr curr1
	  treeSlider0 <- win.slider [Orientation Hor, From 0, CmdInt act]
	  treeSlider := treeSlider0
	  treeSlider.bind [ButtonRelease 1 takeCurr]

 	  ent0 <- win.entry [Font "Courier 18"]
	  ent := ent0
	  ent.bind [ButtonPress 1 $ const ent.focus,
		    KeyPress "Up" $ getFileAnd $ loadText True,
	            KeyPress "Down" $ getFileAnd saveGraph,
		    KeyPress "Right" $ applyClause False False False,
		    KeyPress "Left" $ applyClause False True False,
		    KeyPress "Return" $ getFileAnd addSpecWithBase]

	  font0 <- tk.font "Helvetica 18 normal"; font := font0

   	  lab0 <- win.label [Text start, Font "Helvetica 12 italic", blueback,
			     Justify LeftAlign]
	  lab := lab0
	  lab.bind [ButtonPress 1 $ const lab.focus,
		    KeyPress "c" copySubtrees,
		    KeyPress "d" derefNodes,
		    KeyPress "i" replaceText,
		    KeyPress "l" replaceNodes,
		    KeyPress "L" randomLabels,
		    KeyPress "m" permuteSubtrees,
		    KeyPress "n" negateAxioms,
		    KeyPress "o" removeNode,
		    KeyPress "p" removePath,
		    KeyPress "r" removeSubtrees,
		    KeyPress "s" saveProof,
		    KeyPress "T" randomTree,
		    KeyPress "v" reverseSubtrees,
		    KeyPress "x" showAxiomsFor,
		    KeyPress "Left" $ incrCurr False,
		    KeyPress "Right" $ incrCurr True]
		    
   	  lab0 <- win.label [Font "Helvetica 12 italic", Justify RightAlign, 
   	  		     greenback]
          lab2 := lab0

 	  tedit0 <- win.textEditor [Height 1, Font "Courier 12"]
	  tedit := tedit0
	  tedit.bind [KeyPress "Up" parseText,
	              KeyPress "Down" parseTree]
	  	      
	  but <- setCmd "" $ setNarrow True False
	  matchBut := but
  	  but <- setCmd "" $ setNarrow False True
  	  randomBut := but
  	  but <- setCmd "" $ narrow skip
  	  narrowBut := but
	  but <- setCmd "--->" forwProof
	  forwBut := but
	  but <- setCmd "<---" backProof
	  backBut := but
	  
	  paint.setEval picEval spread
	  setPicDir True
	  buildSolve1

	buildSolve1 = action
	
	  -- closeBut <- setCmd "close" win.iconify
	  
	  but      <- setCmd "derive" setDeriveMode
	  deriveBut := but
	  minusBut <- setCmd "entry-1" $ incrEntry False
	  plusBut  <- setCmd "entry+1" $ incrEntry True
  	  but      <- setCmd "hide" hideOrShow
  	  hideBut := but
   	  paintBut <- setCmd "paint" showPicts
  	  downBut  <- setCmd "parse down" parseTree
  	  upBut    <- setCmd "parse up" parseText
 	  clearS   <- setCmd "redraw" redrawTree
  	  clearT   <- setCmd "remove text" $ clearText
				           $ action numberedExps := ([],True)
 	  but      <- setCmd "quit" tk.quit
 	  quit := but
  	  saveBut  <- setCmd "save pic" $ getFileAnd saveGraph
  	  saveDBut <- setCmd "save pic to dir" saveGraphD
	  dirBut   <- setCmd "set dir" $ setPicDir False
  	  but      <- setCmd "parallel" changeStrat
  	  stratBut := but
  	  but      <- setCmd "simplify" $ simplify skip
  	  simplBut := but
   	  but      <- setCmd "continuous text" switchFast
   	  fastBut := but
	  but      <- setCmd "split" splitTree
	  splitBut := but

	  but      <- win.menuButton [Text "apply to", font12, groove, blueback]
 	  applyBut := but

          axs     <- win.menuButton [Text "axioms", font12b]
  	  axsMenu <- axs.menu []
	  mkBut axsMenu "show axioms" $ showAxioms True
  	  mkBut axsMenu ".. for symbols (x)" showAxiomsFor
	  mkBut axsMenu (".. in " ++ other) $ showAxioms False
  	  subMenu <- mkSub axsMenu "add"
	  createSub 0 subMenu
	  mkBut axsMenu "combine for symbol" $ compressAxioms True
          mkBut axsMenu ".. in entry field" compressClauses
 	  mkBut axsMenu "invert for symbol" $ compressAxioms False
	  mkBut axsMenu "negate for symbols (n)" negateAxioms
	  mkBut axsMenu "Kleene axioms for symbols" kleeneAxioms
 	  mkBut axsMenu "remove in entry field" $ removeClauses 0
   	  mkBut axsMenu ".. for symbols" removeAxiomsFor

 	  fontBut  <- win.menuButton [Text "font", font12]
  	  fontMenu <- fontBut.menu []
	  let mkFontBut text = mkBut fontMenu text $ setFont text
	      families = words "Helvetica Times Courier"
	      mkFonts family = map (family++) [" normal"," bold"," italic"]
	  mapM_ mkFontBut $ concatMap mkFonts families
	  
	  fontLab <- win.label [Text "font size", Font "Helvetica 12 italic", 
	                        Anchor C]
	  fontSize <- win.slider [From 6, To 40, Orientation Hor, 
	                          CmdInt setFontSize]
	  fontSize.bind [ButtonRelease 1 drawNewCurr]
	  fontSize.setValue 18

	  graphBut  <- win.menuButton [Text "graph", font12]
 	  graphMenu <- graphBut.menu []
  	  mkBut graphMenu "expand" $ expandTree False
  	  mkBut graphMenu "expand one" $ expandTree True
  	  mkBut graphMenu "split cycles" $ removeEdges True
  	  mkBut graphMenu "more tree arcs" $ removeEdges False
  	  mkBut graphMenu "compose pointers" composePointers
  	  mkBut graphMenu "collapse variables" collapseVarsCom
 	  mkBut graphMenu "collapse -->" $ transformGraph 0
 	  mkBut graphMenu "collapse level --> " $ collapseStep True
 	  mkBut graphMenu "reset level" resetLevel
 	  mkBut graphMenu "collapse <--" $ transformGraph 1
 	  mkBut graphMenu "collapse level <-- " $ collapseStep False
 	  mkBut graphMenu "remove copies" removeCopies
 	  mkBut graphMenu "show graph" $ transformGraph 2
	  let mkMenu m n1 n2 n3 n4 = do
	                              mkBut m "here" $ cmd n1
  	                              mkBut m "with state equivalence" $ cmd n2
  	                              mkBut m "in painter" $ initCanvas $ cmd n3
  	                              mkBut m ("in " ++ other) $ cmd n4
  	                             where cmd = showTransOrKripke
 	  subMenu <- mkSub graphMenu "  of transitions"
 	  mkMenu subMenu 0 1 2 3
 	  subMenu <- mkSub graphMenu "  of labelled transitions"
 	  mkMenu subMenu 4 5 6 7
	  let mkMenu m n1 n2 n3 = do mkBut m "here" $ cmd n1
  	                             mkBut m "in painter" $ initCanvas $ cmd n2
  	                             mkBut m ("in " ++ other) $ cmd n3
  	                          where cmd = showTransOrKripke
 	  subMenu <- mkSub graphMenu "  of Kripke model"
 	  mkMenu subMenu 8 9 10
 	  subMenu <- mkSub graphMenu "  of labelled Kripke model"
 	  mkMenu subMenu 11 12 13
 	  mkBut graphMenu "build iterative equations" $ transformGraph 3
 	  mkBut graphMenu "connect equations" $ modifyEqs 0
	  let mkMenu m cmd n1 n2 n3 n4 = do mkBut m "of canvas" $ cmd n1
  	                                    mkBut m "of transitions" $ cmd n2
  	                                    mkBut m "of atom values" $ cmd n3
  	                                    mkBut m "of output" $ cmd n4
 	  subMenu <- mkSub graphMenu "show Boolean matrix"
 	  mkMenu subMenu (initCanvas . showMatrix) 6 0 1 2
 	  subMenu <- mkSub graphMenu "show list matrix"
 	  mkMenu subMenu (initCanvas . showMatrix) 7 3 4 5
 	  subMenu <- mkSub graphMenu "show binary relation"
 	  mkMenu subMenu showRelation 6 0 1 2
 	  subMenu <- mkSub graphMenu "show ternary relation"
 	  mkMenu subMenu showRelation 7 3 4 5
	  
	  but <- win.menuButton [Text "?", font12]
	  termBut := but
  	  treeMenu <- termBut.menu []
	  subMenu <- mkSub treeMenu "call enumerator"
  	  mapM_ (mkButF subMenu callEnum) enumerators
	  mkBut treeMenu "remove other trees" removeOthers
  	  mkBut treeMenu "show changed" showChanged
 	  mkBut treeMenu "show proof" $ showProof True
	  mkBut treeMenu (".. in " ++ other) $ showProof False
	  mkBut treeMenu "save proof to file (s)" saveProof
 	  mkBut treeMenu "show proof term" showProofTerm
	  mkBut treeMenu "check proof term from file" $ checkProofF False
	  mkBut treeMenu ".. in painter" $ checkProofF True
	  mkBut treeMenu ".. from text field" $ checkProofT False
	  mkBut treeMenu ".. in painter" $ checkProofT True
	  mkBut treeMenu "create induction hypotheses" createIndHyp
	  mkBut treeMenu ("load text from file to " ++ other) $ getFileAnd 
	  						      $ loadText False
 	  
	  nodesBut  <- win.menuButton [Text "nodes", font12]
 	  nodesMenu <- nodesBut.menu []
 	  mkBut nodesMenu "label roots with entry (l)" replaceNodes
 	  mkBut nodesMenu "reference" refNodes
 	  mkBut nodesMenu "dereference (d)" derefNodes
	  mkBut nodesMenu "greatest lower bound" showGlb
	  mkBut nodesMenu "predecessors" showPreds
	  mkBut nodesMenu "successors" showSucs
	  mkBut nodesMenu "constructors" $ showSyms constrPositions
	  mkBut nodesMenu "values" showVals
	  mkBut nodesMenu "variables" $ showSyms varPositions
	  mkBut nodesMenu "free variables" $ showSyms freePositions
	  mkBut nodesMenu "polarities" showPolarities
	  mkBut nodesMenu "positions" showPositions
	  mkBut nodesMenu "level numbers" $ showNumbers 1
	  mkBut nodesMenu "preorder numbers" $ showNumbers 2
	  mkBut nodesMenu "heap numbers" $ showNumbers 3
	  mkBut nodesMenu "hill numbers" $ showNumbers 4
	  mkBut nodesMenu "coordinates" showCoords
	  mkBut nodesMenu "cycle targets" showCycleTargets

 	  interpreterBut0 <- win.menuButton [Text "tree", font12]
 	  interpreterBut := interpreterBut0
  	  interpreterMenu <- interpreterBut.menu []
  	  mapM_ (mkButF interpreterMenu setInterpreter) interpreters
 	  
	  sig     <- win.menuButton [Text "signature", font12b]
  	  sigMenu <- sig.menu []
  	  mkBut sigMenu "admit all simplifications" $ setAdmitted' True []
	  mkBut sigMenu ".. except for symbols" $ setAdmitted True
	  mkBut sigMenu ".. for symbols" $ setAdmitted False
	  mkBut sigMenu "collapse after simplify" setCollapse
	  but <- mkBut sigMenu (eqsButMsg True) switchSafe
	  safeBut := but
	  mkBut sigMenu "show sig" showSig
 	  mkBut sigMenu "show map" showSigMap
 	  mkBut sigMenu "apply map" applySigMap
 	  mkBut sigMenu "save map to file" $ getFileAnd saveSigMap
 	  addMapMenu <- mkSub sigMenu "add map"
  	  mkBut addMapMenu "STACK2IMPL" $ addSigMap "STACK2IMPL"
  	  mkBut addMapMenu "from text field" addSigMapT
 	  mkBut addMapMenu "from file" $ getFileAnd addSigMap
 	  mkBut sigMenu "remove map" removeSigMap

	  selectBut <- win.menuButton [Text "selection", font12]
  	  streeMenu <- selectBut.menu []
	  mkBut streeMenu "induction" $ startInd True
 	  mkBut streeMenu "coinduction" $ startInd False
 	  mkBut streeMenu "stretch premise" $ stretch True
 	  mkBut streeMenu "stretch conclusion" $ stretch False
	  mkBut streeMenu "instantiate" instantiate
  	  mkBut streeMenu "unify" unifySubtrees
 	  mkBut streeMenu "generalize" generalize
 	  mkBut streeMenu "specialize" specialize
  	  mkBut streeMenu "decompose atom" decomposeAtom
	  mkBut streeMenu "replace by other sides of in/equations"
	  		  replaceSubtrees
 	  mkBut streeMenu "use transitivity" applyTransitivity
 	  subMenu <- mkSub streeMenu "apply clause"
 	  mkBut subMenu "Left to right (Left)" $ applyClause False False False
	  mkBut subMenu ".. and save redex" $ applyClause False False True
 	  mkBut subMenu "Left to right lazily" $ applyClause True False False
	  mkBut subMenu ".. and save redex" $ applyClause True False True
	  mkBut subMenu "Right to left (Right)" $ applyClause False True False
	  mkBut subMenu ".. and save redex" $ applyClause False True True
	  mkBut subMenu "Right to left lazily" $ applyClause True True False
	  mkBut subMenu ".. and save redex" $ applyClause True True True
	  mkBut streeMenu "move up quantifiers" shiftQuants
	  mkBut streeMenu "shift subformulas" shiftSubs
	  mkBut streeMenu "create Hoare invariant" $ createInvariant True
 	  mkBut streeMenu "create subgoal invariant" $ createInvariant False
	  mkBut streeMenu "flatten (co-)Horn clause" flattenImpl
 	  mkBut streeMenu "shift pattern to rhs" shiftPattern
 	  mkBut streeMenu ("replace by tree of " ++ other) replaceOther
	  mkBut streeMenu ("unify with tree of " ++ other) unifyOther
	  mkBut streeMenu "build unifier" buildUnifier
  	  mkBut streeMenu "subsume" subsumeSubtrees
  	  mkBut streeMenu "evaluate" evaluateTrees
	  mkBut streeMenu "copy (c)" copySubtrees
	  mkBut streeMenu "remove (r)" removeSubtrees
	  mkBut streeMenu "remove node (o)" removeNode
	  mkBut streeMenu "permute subtrees (m)" permuteSubtrees
	  mkBut streeMenu "reverse (v)" reverseSubtrees
 	  mkBut streeMenu "insert/replace by text (i)" replaceText
	  mkBut streeMenu "random labels (L)" randomLabels
	  mkBut streeMenu "random tree (T)" randomTree
	  mkBut streeMenu "remove path (p)" removePath

          subsBut  <- win.menuButton [Text "substitution", font12]
          subsMenu <- subsBut.menu []
	  mkBut subsMenu "add from text field" addSubst
   	  mkBut subsMenu "apply" applySubst
 	  mkBut subsMenu "rename" renameVar
 	  mkBut subsMenu "remove" removeSubst
 	  mkBut subsMenu "show" $ showSubst 0
 	  mkBut subsMenu (".. in text field of " ++ other) $ showSubst 1
 	  mkBut subsMenu (".. on canvas of " ++ other) $ showSubst 2
 	  mkBut subsMenu ".. solutions" showSolutions

          spec     <- win.menuButton [Text "specification", font12b]
  	  specMenu <- spec.menu []
  	  mkBut specMenu "re-add" reAddSpec
	  mkBut specMenu "remove" $ removeSpec $ labGreen $ iniSpec++iniSigMap
	  mkBut specMenu "remove Kripke model" removeKripke
  	  mkBut specMenu "set number of processes" setNoProcs
  	  mkBut specMenu "build Kripke model" $ buildKripke 2
  	  mkBut specMenu ".. from current graph" $ buildKripke 3
  	  mkBut specMenu ".. from regular expression" $ buildKripke 4
 	  mkBut specMenu ".. cycle-free" $ buildKripke 0
 	  mkBut specMenu ".. pointer-free" $ buildKripke 1
 	  mkBut specMenu "state equivalence" stateEquiv
 	  mkBut specMenu "minimize" minimize
	  mkBut specMenu "build regular expression" buildRegExp
	  mkBut specMenu "distribute regular expression" $ reduceRegExp 0
	  mkBut specMenu "reduce left-folded regular expression"$ reduceRegExp 1
	  mkBut specMenu ".. right-folded regular expression" $ reduceRegExp 2
 	  mkBut specMenu "regular equations" $ modifyEqs 1
 	  mkBut specMenu "substitute variables" $ modifyEqs 2
 	  mkBut specMenu "solve regular equation" $ modifyEqs 3
	  mkBut specMenu "save to file" $ getFileAnd saveSpec
  	  subMenu <- mkSub specMenu "load text"
	  createSpecMenu False subMenu
          subMenu <- mkSub specMenu "add"
	  createSpecMenu True subMenu

          ths     <- win.menuButton [Text "theorems", font12b]
  	  thsMenu <- ths.menu []
  	  mkBut thsMenu "show theorems" $ showTheorems True
  	  mkBut thsMenu ".. for symbols" showTheoremsFor
	  mkBut thsMenu (".. in " ++ other) $ showTheorems False
 	  mkBut thsMenu "remove theorems" removeTheorems
   	  mkBut thsMenu ".. in entry field" $ removeClauses 1
 	  subMenu <- mkSub thsMenu "add theorems"
	  createSub 1 subMenu
	  mkBut thsMenu "show terms" showTerms
	  mkBut thsMenu "show conjects" showConjects
   	  mkBut thsMenu ".. in entry field" $ removeClauses 3
	  mkBut thsMenu "remove conjects" removeConjects
   	  mkBut thsMenu ".. in entry field" $ removeClauses 2
	  mkBut thsMenu "show induction hypotheses" showIndClauses
          subMenu <- mkSub thsMenu "add conjects"
	  createSub 2 subMenu
	  
	  treeSize <- win.slider [From 1, To 1000, Orientation Hor, 
	                          CmdInt setMaxHeap]
	  treeSize.bind [ButtonRelease 1 drawNewCurr]
	  treeSize.setValue 100

  	  horBut <- win.slider [From 0, To 100, Orientation Hor, CmdInt blowHor]
	  horBut.bind [ButtonRelease 1 drawNewCurr]
	  horBut.setValue 15
  	  
	  hsb <- win.scrollBar [Width 12]
	  hsb.attach canv Hor

 	  tsb <- win.scrollBar [Width 12]
	  tsb.attach tedit Ver

	  verBut <- win.slider [From 0, To 60, Orientation Ver, CmdInt blowVer]
	  verBut.bind [ButtonRelease 1 drawShrinked]
	  verBut.setValue 35
	  
	  vsb <- win.scrollBar [Width 12]
	  vsb.attach canv Ver
	  
  	  pack $ col [fillX $ row [lab,lab2],
	  	      canv <<< fillY vsb,
		      fillX hsb, 
		      fillX treeSize,
		      fillX treeSlider,
		      tedit <<< fillY tsb, 
		      fillX ent, 
		      fillX $ fillY verBut <<<
			      (horBut ^^^ fontSize ^^^ 
			       (fontLab <<< fontBut)) <<<
			      (termBut ^^^ paintBut ^^^ interpreterBut ^^^
			       fastBut) <<<
			      fillY (selectBut ^^^ (splitBut <<< hideBut) ^^^
			             (minusBut <<< plusBut)) <<<
			      fillY (spec ^^^ (upBut <<< dirBut <<< downBut) ^^^
			             (matchBut <<< randomBut <<< narrowBut)) <<< 
			      fillY (sig ^^^ (clearT <<< clearS) ^^^
			             (stratBut <<< simplBut)) <<< 
			      (axs ^^^ graphBut ^^^ nodesBut ^^^ subsBut) <<<
			      (ths ^^^ saveDBut ^^^ applyBut) <<<
			      fillY canvSlider <<<
              		      (backBut ^^^ forwBut ^^^ deriveBut ^^^ quit)]

-- end of buildSolve

-- The other methods of solver are listed in alphabetical order:

	adaptPos (x,y) = do 
	  (leftDivWidthX,_) <- canv.xview
	  (topDivWidthY,_) <- canv.yview
	  let (bt,ht) = fromInt2 canvSize
	  return (x+round (leftDivWidthX*bt),y+round (topDivWidthY*ht))

	addAxioms t file continue = action
          sig <- getSignature
	  let axs = if isConjunct t then subterms t else [t]
	      cls = filter (not . (isAxiom sig ||| isSimpl)) axs
	  if null cls 
	     then solPositions := []
	          axioms := axioms `join` axs
	          simplRules := simplRules `join` trips ["==","<==>"] axs
	          transRules := transRules `join` trips ["->"] axs
	          labGreen $ newCls "axioms" file; continue
	     else enterFormulas cls
	          labRed $ "The clauses in " ++ tfield ++ " are not axioms."
	  
	addCongAxioms = action
	  if null trees then labBlue start
	  else str <- ent.getValue
	       sig <- getSignature
 	       let pars = words str
	           b par = par `elem` words "refl symm tran" ||
	                   (sig.isConstruct ||| sig.isDefunct) (init par) && 
	      	           just (parse digit [last par])
	           t = trees!!curr
	           p = emptyOrLast treeposs
	           pars' = filter b pars
	           axs = case getSubterm t p of
	           	      F equiv [_,_] -> congAxs equiv pars'
	                      _ -> []
	       if null pars' 
	          then labRed "Enter axiom names into the entry field."
	          else if null axs then
	                  labRed "Select a binary relation in the current tree."
	               else addCongAxioms' axs
	  
	addCongAxioms' axs = action
	  axioms := axioms `join` axs 
	  indClauses := indClauses `join` axs
	  enterFormulas axs
	  extendPT False False False False $ AddAxioms axs
	  let msg = "ADDC" ++ showFactors axs
	  setProof True False msg [] $ newCls "axioms" tfield
	
	addClauses treetype file = action
	  str <- if text then getTextHere else lookupLibs tk file
	  let str' = removeComment 0 str
	      file' = if text then tfield else file
	  sig <- getSignature
	  case treetype of 0 -> case parseE (implication sig) str' of
	                             Correct t -> addAxioms t file' done
				     p -> incorrect p str' $ illformed "formula"
			   1 -> case parseE (implication sig) str' of
	                       	     Correct t -> addTheorems t file' done
				     p -> incorrect p str' $ illformed "formula"
			   _ -> parseConjects sig file' str' done
	  where text = null file
	       
	addSigMap file = action str <- lookupLibs tk file
	  			parseSigMap file $ removeComment 0 str

	addSigMapT = action str <- getTextHere
			    parseSigMap tfield str

	addSpec b continue file = action
	  if not checking then
	     if b then specfiles := file:specfiles
	     str <- get
	     if null str then labRed $ file ++ " is not a file name."
	     else let (sig,axs,ths,conjs,ts) = splitSpec $ removeComment 0 str
	              act1 = action
		          sig <- getSignature
	                  if onlySpace axs then act2 sig
	                  else case parseE (implication sig) axs of
	                       Correct t -> addAxioms t file' $ delay $ act2 sig
	                       p -> incorrect p axs $ illformed "formula"
	              act2 sig = action 
		        if onlySpace ths then act3 sig
			else case parseE (implication sig) ths of
	                     Correct t -> addTheorems t file' $ delay $ act3 sig
	                     p -> incorrect p ths $ illformed "formula"
	              act3 sig = action 
		   	   if onlySpace conjs then act4 sig
		   	   else parseConjects sig file' conjs $ delay $ act4 sig
	              act4 sig = action if onlySpace ts then delay continue
		             	        else parseTerms sig file' ts continue
	          if onlySpace sig then act1 
	          else let (ps,cps,cs,ds,fs,hs) = symbols
	                   syms = ps++cps++cs++ds++fs++map fst hs
	               case parseE (signature ([],ps,cps,cs,ds,fs,hs)) sig of
	                    Correct (specs,ps,cps,cs,ds,fs,hs)
	                      -> symbols := (ps,cps,cs,ds,fs,hs)
	                         let finish = action varCounter := iniVC syms
			                             labGreen $ newSig file'
			                             delay act1
	                         foldrM (addSpec False) (delay finish) $ 
	                                                specs `minus` specfiles
	                    Partial (_,ps,cps,cs,ds,fs,hs) rest
	                      -> enterText $ showSignature (ps,cps,cs,ds,fs,hs) 
	                                   $ check rest
	                         labRed $ illformed "signature"
	                    _ -> enterText sig
	                    	 labRed $ illformed "signature"

	  where (file',get) = if null file then (tfield,getTextHere)
	                                   else (file,lookupLibs tk file)
		onlySpace = all (`elem` " \t\n")

	addSpecWithBase spec = action
	  addSpec True (if spec == "base" then skip 
	  		else addSpec True (action mapM_ act w) spec)
	          "base"
          where act x = do if nothing $ search ((== x) . root . pr1) simplRules 
          		     then simplRules := (leaf x,[],mkList []):simplRules
                w = words "states labels atoms"
                
	addSubst = action
	  str <- getTextHere
 	  sig <- getSignature
 	  case parseE (conjunct sig) str of
	  Correct t 
	    -> if hasPos t then labRed posInSubst
	       else case eqsToSubst $ mkFactors t of
	                 Just (f,dom) -> let (g,xs) = substitution
		                         setSubst (g `andThen` f, xs `join` dom)
					 labGreen extendedSubst
		         _ -> labRed $ illformed "substitution"
	  p -> incorrect p str $ illformed "substitution"

	addText ls = action forall l <- addNo 0 $ concatMap split ls
	                           do tedit.insertLines maxBound [l]
	  where addNo :: Int -> [String] -> [String]
	        addNo _ []               = []
	        addNo n ([]:ls)          = []:addNo n ls
	        -- addNo 0 ((' ':l):ls)     = (" 0> " ++ l):addNo 1 ls
	        -- addNo n ((' ':l):ls)     = ("    " ++ l):addNo n ls
	        -- addNo n (('.':l):ls)     = ("   ." ++ l):addNo n ls
	        addNo n (l:ls) | n < 10  = (' ':' ':show n ++ '>':l):f n
	                       | n < 100 = (' ':show n ++ '>':l):f n
	                       | True    = (show n ++ '>':l):f n
                                           where f n = addNo (n+1) ls
	        split [] 		 = []
	        split l | length l <= 85 = [l]
	                | True           = take 85 l:split ('.':drop 85 l)
	
	addTheorems t file continue = action
	  sig <- getSignature
	  theorems := theorems `join` if isConjunct t then subterms t else [t]
	  labGreen $ newCls "theorems" file
	  continue

	applyClause lazy invert saveRedex = action
 	  if null trees then labBlue start
	  else str <- ent.getValue
	       let (exps,b) = numberedExps
	       case parse nat str of
	            k@(Just n) | n < length exps
	              -> if b then if lazy then stretchAndApply k $ exps!!n
	              			   else finish k $ exps!!n
		              else labMag $ enterTfield "formulas"
                    _ -> str <- getTextHere
 	                 sig <- getSignature
	                 case parseE (implication sig) str of
	                      Correct cl | lazy -> stretchAndApply Nothing cl
	                      	         | True -> finish Nothing cl
	                      p -> incorrect p str $ illformed "formula"
	  where stretchAndApply k cl = action 
	         let zNo = varCounter "z"
                 case preStretch True (const True) cl of
	              Just (_,varps) -> setZcounter n; finish k clp
			                where (clp,n) = stretchPrem zNo varps cl
		      _ -> case preStretch False (const True) cl of
	                        Just (_,varps) -> setZcounter n; finish k clc
			                where (clc,n) = stretchConc zNo varps cl
		                _ -> notStretchable "The left-hand side"
	        finish k cl = applyTheorem saveRedex k $
                                          if invert then invertClause cl else cl

	applyCoinduction limit = action
	  sig <- getSignature
	  let t = trees!!curr
	      qs@(p:ps) = emptyOrAll treeposs
	      rs@(r:_) = map init qs
	      u = getSubterm t r
	      us = subterms u
	      rest = map (us!!) $ indices_ us `minus` map last qs
	      rule = "COINDUCTION"
	      g = stretchConc $ varCounter "z"
	      getAxioms = flip noSimplsFor axioms
	      h x = if x `elem` fst newPreds then getPrevious x else x
	      conjs@(conj:_) = map (mapT h) $ map (getSubterm t) qs
	      f = preStretch False sig.isCopred
	  if notnull qs && any null ps then labRed $ noApp rule
	  else if null ps && universal sig t p conj
	         then case f conj of
	              Just (x,varps)
		        -> let (conj',k) = g varps conj; axs = getAxioms [x]
	  	           setZcounter k
	  	           applyInd limit False rule [conj'] [x] axs t p [] 
		      _ -> notStretchable "The conclusion"
	         else if allEqual rs && isConjunct u && universal sig t r u then
	                 case mapM f conjs of
		         Just symvarpss
		           -> let (xs,varpss) = unzip symvarpss
				  (conjs',ks) = unzip $ zipWith g varpss conjs
			          ys = mkSet xs; axs = getAxioms ys
 			      varCounter := updMax varCounter "z" ks
 			      applyInd limit False rule conjs' ys axs t r rest
		         _ -> notStretchable "Some conclusion"
	              else labRed $ noApp rule

	applyDisCon k (F "|" ts) redices t ps sig msg =
	  applyDisCon k (F "<===" [F "|" ts,mkTrue]) redices t ps sig msg
	applyDisCon k (F "&" ts) redices t ps sig msg =
	  applyDisCon k (F "<===" [F "&" ts,mkTrue]) redices t ps sig msg
	applyDisCon k (F "<===" [F "|" ts,prem]) redices t ps sig msg = action
	  let pred = glbPos ps
	      u = getSubterm t pred
	      qs = map (restPos pred) ps
	  if all noQuantsOrConsts ts && polarity True t pred && isDisjunct u &&
	     all (isProp sig ||| isAnyQ) (sucTerms u qs)
	     then finishDisCon k False True ts prem redices t ps pred qs sig msg
	  else labRed $ noAppT k
	applyDisCon k (F "<===" [F "&" ts,prem]) redices t ps sig msg = action
	  let pred = glbPos ps
	      u = getSubterm t pred
	      qs = map (restPos pred) ps
	  if all noQuantsOrConsts ts && polarity True t pred && isConjunct u && 
	     all (isProp sig ||| isAllQ) (sucTerms u qs)
	    then finishDisCon k False False ts prem redices t ps pred qs sig msg
	  else labRed $ noAppT k
	applyDisCon k (F "===>" [F "&" ts,conc]) redices t ps sig msg = action
	  let pred = glbPos ps
	      u = getSubterm t pred
	      qs = map (restPos pred) ps
	  if all noQuantsOrConsts ts && polarity False t pred && isConjunct u &&
	     all (isProp sig ||| isAllQ) (sucTerms u qs)
	     then finishDisCon k True True ts conc redices t ps pred qs sig msg
	  else labRed $ noAppT k
	applyDisCon k (F "===>" [F "|" ts,conc]) redices t ps sig msg = action
	  let pred = glbPos ps
	      u = getSubterm t pred
	      qs = map (restPos pred) ps
	  if all noQuantsOrConsts ts && polarity False t pred && isDisjunct u &&
	     all (isProp sig ||| isAnyQ) (sucTerms u qs)
	     then finishDisCon k True False ts conc redices t ps pred qs sig msg
	  else labRed $ noAppT k
	  
	applyInd limit ind rule conjs indsyms axs t p rest = action
	  sig <- getSignature
	  let (nps,ncps) = newPreds
	      syms = if ind then ncps else nps
	      vc1 = decrVC varCounter syms
	      indsyms' = indsyms `join` map getPrevious syms
	      (f,vc2) = renaming vc1 indsyms'
	      axs0 = map mergeWithGuard axs
	      (axs1,vc3) = iterate h (axs0,vc2)!!limit
	      h (axs,vc) = applyToHeadOrBody sig (((`elem` indsyms') .) . label)
	      					 False axs0 axs vc
	      newAxs = map mkAx conjs
	      mkAx (F x [t,u]) = F x [g t,u]
	      mkAx _           = error "applyInd"
              -- g replaces a logical predicate r by f(r) and an equation h(t)=u
              -- with h in xs by the logical atom f(h)(t,u).
              g eq@(F "=" [F x ts,u]) = if x `elem` indsyms' 
	     			        then F (f x) $ ts++[u] else eq
              g (F x ts)              = F (f x) $ map g ts
              g t                     = t
	      rels = map f indsyms'
	      (ps',cps') = if ind then ([],rels) else (rels,[])
	      (ps,cps,cs,ds,fs,hs) = symbols
	  symbols := (ps `join` ps',cps `join` cps',cs,ds,fs,hs)
	  newPreds := (nps `join` ps',ncps `join` cps')
	  sig <- getSignature
	  let (cls,vc4) = applyToHeadOrBody sig (const2 True) True newAxs 
	  				        (map g axs1) vc3
	      conj = mkConjunct cls
	      xs = [x | x <- frees sig conj, noExcl x]
	      u = replace t p $ mkConjunct $ mkAll xs conj:rest
	      msg = "ADDI" ++ showFactors newAxs ++ "\n\nApplying " ++ rule ++
	            " wrt\n\n" ++ showFactors axs1
	  indClauses := indClauses `join` newAxs
	  axioms := axioms `join` newAxs
	  varCounter := vc4
	  extendPT False False True True $ Induction ind limit
	  maybeSimplify sig u $ makeTrees sig 
	  		      $ setTreesFrame [] $ setProof True True msg [p] 
				 	         $ indApplied rule

 	applyInduction limit = action
	  let t = trees!!curr
	      qs@(p:ps) = emptyOrAll treeposs
	      rs@(r:_) = map init qs
	      u = getSubterm t r
	      us = subterms u
	      rest = map (us!!) $ indices_ us `minus` map last qs
	      rule = "FIXPOINT INDUCTION"
	      g = stretchPrem $ varCounter "z"
	  if notnull qs && any null ps then labRed $ noApp rule
	  else sig <- getSignature
	       let h (F x ts) = if x `elem` snd newPreds 
		   		then if sig.isPred z then F z ts
			             else mkEq (F z $ init ts) $ last ts
			        else F x $ map h ts where z = getPrevious x
		   h t 	      = t
		   conjs@(conj:_) = map (h . getSubterm t) qs
		   f = preStretch True $ sig.isPred ||| sig.isDefunct
		   getAxioms ks xs = unzip $ map g $ noSimplsFor xs axioms
			where g = flatten (maximum ks) $ filter sig.isDefunct xs
	       if null ps && universal sig t p conj 
	          then case f conj of
	               Just (x,varps)
		         -> let (conj',k) = g varps conj
		    	        (axs,ms) = getAxioms [k] [x]
		            varCounter := updMax varCounter "z" ms
			    applyInd limit True rule [conj'] [x] axs t p []
		       _ -> notStretchable "The premise"
               else if allEqual rs && isConjunct u && universal sig t r u then 
	       	       case mapM f conjs of
		       Just symvarpss
		         -> let (xs,varpss) = unzip symvarpss
			        (conjs',ks) = unzip $ zipWith g varpss conjs
				ys = mkSet xs; (axs,ms) = getAxioms ks ys
		            varCounter := updMax varCounter "z" ms
			    applyInd limit True rule conjs' ys axs t r rest
		       _ -> notStretchable "Some premise"
		    else labRed $ noApp rule

	applySigMap = action
	  if null trees then labBlue start
	  else sig <- getSignature
	       sig' <- solve.getSignatureR
	       case applySignatureMap sig sig' (fst signatureMap) $ trees!!curr
	            of Just t -> solve.bigWin; solve.enterTree formula t
	       	       _ -> labRed $ sigMapError other

	applySubst = action
          if null trees then labBlue start
	  else sig <- getSignature
	       let t = trees!!curr
	           (g,dom) = substitution
		   f t p = replace t p $ getSubterm t p>>>g
	       	   ts = substToEqs g dom
		   ps = emptyOrAll treeposs
		   msg = "The substitution\n\n" ++ showFactors ts ++ "\n\n"
	       trees := updList trees curr $ foldl f t ps
	       extendPT False False False False ApplySubst
	       setProof False False msg ps subsAppliedToAll
	       clearTreeposs; drawCurr

	applySubstTo x = action
	  if null trees then labBlue start 
	                else applySubstTo' x $ fst substitution x

	applySubstTo' x v = action
          let t = trees!!curr
	      p = emptyOrLast treeposs
	      msg = "SUBSTITUTING " ++ showTerm0 v ++ " for " ++ x
	  sig <- getSignature
	  case isAny t x p of
	  Just q | polarity True t q -> finish (q++[0]) t sig msg True
	  _ -> case isAll t x p of
	       Just q | polarity False t q -> finish (q++[0]) t sig msg True
	       _ -> finish p t sig msg False
	  where finish p t sig msg b = action
	  	        trees := updList trees curr t'
	  	        extendPT False False False False $ ApplySubstTo x v
			drawThis t' (map (p++) $ freeXPositions sig x u) "green"
			setProof b False msg [p] $ subsAppliedTo x
			let (f,dom) = substitution 
			setSubst (upd f x $ V x, dom `minus1` x)
			where t' = replace t p $ u>>>for v x
			      u = getSubterm t p

 	applyTheorem saveRedex k th = action
	  sig <- getSignature
	  extendPT False False True True $ Theorem saveRedex th
	  let t = trees!!curr
	      ps = emptyOrAll treeposs
	      redices = map (getSubterm t) ps
	      n = length ps
              ([th'],vc) = renameApply sig t varCounter
                                       [if saveRedex then copyRedex th else th] 
	      f t (redex:rest) (p:ps) qs vc 
                            = action case applySingle th' redex t p sig vc of
		                          Just (t,vc) -> f t rest ps (p:qs) vc
	                                  _ -> f t rest ps qs vc
	      f _ _ _ [] _  = labRed $ notUnifiable k
	      f t _ _ qs vc = action varCounter := vc
			             maybeSimplify sig t $ makeTrees sig 
				     			 $ finish qs
              str = showTree False $ case th of F "===>" [F "True" [],th] -> th 
					        F "<===" [F "False" [],th] 
				                  -> mkNot sig th
					        _ -> th 
              finish qs = action
                     setTreesFrame [] $ setProof True True (applied True str) qs
		      		      $ thApplied k
	  if nothing k then enterText str
	  if isTaut th then varCounter := vc; f t redices ps [] vc 
	  else if n > 1 && isDisCon th && n == noOfComps th 
	          then varCounter := vc
	               applyDisCon k th' redices t ps sig $ applied True str
	       else if saveRedex || isSimpl th || all (correctPolarity th t) ps
	               then if isAxiom sig th then
		               narrowOrRewritePar t sig k [th] saveRedex ps skip
		            else if isTheorem th 
		                    then varCounter := vc; f t redices ps [] vc 
		                    else labRed $ noTheorem k
	               else labRed $ noAppT k

	applyTransitivity = action
 	  if null trees then labBlue start
	  else let t = trees!!curr
	           ps = emptyOrAll treeposs
		   redices = map (getSubterm1 t) ps
	       case redices of
	       F x [l,r]:ts
	         -> let p:qs = ps
		        n = varCounter "z"
			z = 'z':show n
			n' = n+1
		    if isOrd x && null qs && polarity True t p 
		       then let u = anyConj [z] [F x [l,V z],F x [V z,r]]
		            trees := updList trees curr $ replace1 t p u
		            setZcounter n'
 		    else let z' = 'z':show n'
			     u = if qs == [p++[0]]
				 then anyConj [z] [mkEq l $ V z,F x [V z,r]]
			         else if qs == [p++[1]]
				      then anyConj [z] [F x [l,V z],
				      			mkEq (V z) r]
			              else anyConj [z,z'] [mkEq l $ V z,
						           F x [V z,V z'],
					                   mkEq (V z') r]
			 trees := updList trees curr $ replace1 t p u
		         setZcounter $ n'+1
                    finish ps
	       _ -> if any null ps then labMag "Select proper subtrees!"
                    else let qs = map init ps
			     q = head qs
		             u = getSubterm t q
	                 if allEqual qs && isConjunct u then
		            case transClosure redices of
	                    Just v
			      -> if polarity False t q then
				    let us = v:removeTerms (subterms u) redices
		                        t' = replace1 t q $ mkConjunct us
				    trees := updList trees curr t'
			            finish ps
			         else labRed $ noApp "Transitivity"
		            _ -> labMag "Select composable atoms!"
                         else labRed $ noApp "Transitivity"
	  where anyConj xs = mkAny xs . F "&"
	        finish ps = action 
	               extendPT False False False False ApplyTransitivity
	               setProof True False "TRANSITIVITY" ps transitivityApplied
	               clearTreeposs; drawCurr

	backProof = action 
	  if restore then treeposs := oldTreeposs; restore := False; drawCurr
          else if checking then checkBackward
	       else if proofPtr < 1 then labMag emptyProof
                    else let ps = (proof!!proofPtr).treePoss
			 proofPtr := proofPtr-1; changeState proofPtr ps
	            let n = searchback deriveStep $ take proofTPtr proofTerm
		    proofTPtr := if just n then get n else 0

	backWin = action win.iconify
	
	bigWin = action win.deiconify; win.raise

	blowHor i = action spread := (i,snd spread) 
	                   paint.setEval picEval spread

	blowVer i = action spread := (fst spread,i)
			   paint.setEval picEval spread

	buildKripke 3 = action 		              -- from current graph
	  if null trees then labBlue start
          else let t = trees!!curr
	           (states,atoms,rules) = graphToTransRules t
               kripke := (states,[],atoms,[],[],[],[])
               changeSimpl "states" $ mkList states
               changeSimpl "labels" $ mkList []	           
               changeSimpl "atoms"  $ mkList atoms	           
               transRules := rules
               sig <- getSignature
               let (rs1,_) = buildTrans sig sig.states
                   (rs2,_) = buildTrans sig sig.atoms
                   tr = pairsToInts states rs1 states
   	           va = pairsToInts states rs2 atoms
 	       kripke := (states,[],atoms,tr,[],va,[])
	       delay $ setProof True False kripkeMsg []
	             $ kripkeBuilt 3 0 (length states) 0 $ length atoms
	                     
	buildKripke 4 = action 			      -- from regular expression
	  if null trees then labBlue start
	  else sig <- getSignature
	       let t = trees!!curr
	  	   p = emptyOrLast treeposs
	       case parseRE sig $ getSubterm t p of
	       	 Just (e,as) 
	       	   -> let (_,nda) = regToAuto e
	       	          as' = as `minus1` "eps"
	       	          (sts,delta) = powerAuto nda as' 
	       	          finals = searchAll (elem 1) sts
	       	          mkTerm = mkList . map mkConst
	       	          f (st,a) = (mkPair (mkTerm st) $ leaf a,[],
	       	                      mkTerm $ delta st a)
	       	          states = map mkTerm sts
	       	          labels = map leaf as'
	       	          atom = leaf "final"            
	       	      transRules := map f $ prod2 sts as'     
	              kripke := (states,labels,[atom],[],[],[],[])
	       	      changeSimpl "states" $ mkList states
	       	      changeSimpl "labels" $ mkList labels
	       	      changeSimpl "atoms"  $ mkList [atom]
                      sig <- getSignature
                      let (_,rsL) = buildTrans sig sig.states
                          trL = tripsToInts states labels rsL states
  	              kripke := (states,labels,[atom],[],trL,[finals],[])
	              delay $ setProof True False kripkeMsg []
	                    $ kripkeBuilt 4 0 (length sts) (length as') 1
 	       	 _ -> labMag "Select a regular expression!"
	
	buildKripke mode = action                     -- from transition axioms
	  if null trees then enterTree False $ V""
	  sig <- getSignature
 	  let states = simplifytoList sig "states"
              labels = simplifytoList sig "labels" 
   	  kripke := (states,labels,[],[],[],[],[])
  	  changeSimpl "states" $ mkList states
  	  changeSimpl "labels" $ mkList labels
 	  sig <- getSignature
  	  let (states,rs,rsL) = buildTransLoop sig mode    -- build trans/L
  	      tr  = pairsToInts states rs states
	      trL = tripsToInts states labels rsL states
	  kripke := (states,labels,[],tr,trL,[],[])
	  changeSimpl "states" $ mkList states
	  delay $ buildKripke1 mode 
	
	buildKripke1 mode = action	  
	  sig <- getSignature
	  let atoms = simplifytoList sig "atoms" 
	  kripke := (sig.states,sig.labels,atoms,sig.trans,sig.transL,[],[])
	  changeSimpl "atoms" $ mkList atoms
 	  sig <- getSignature
	  let (rs,rsL) = buildTrans sig atoms              -- build value/L
 	      va  = pairsToInts sig.states rs atoms
	      vaL = tripsToInts sig.states sig.labels rsL atoms
	      [l,m,n] = map length [sig.states,sig.labels,atoms]
 	  kripke := (sig.states,sig.labels,atoms,sig.trans,sig.transL,va,vaL)
	  setProof True False kripkeMsg [] $ kripkeBuilt mode noProcs l m n
	        
	buildRegExp = action
          if null trees then labBlue start
	  else str <- ent.getValue
	       sig <- getSignature
	       let finish start = action 
	              trees := updList trees curr $ showRE $ autoToReg sig start
	              extendPT False False False False BuildRE
	       	      setProof False False "BUILDING A REGULAR EXPRESSION" []
	       	       			   regBuilt
	       	      clearTreeposs; drawCurr
	       case parse (term sig) str of 
	            Just start | start `elem` sig.states -> finish start
	            _ -> let start = label (trees!!curr) $ emptyOrLast treeposs
	                 case parse (term sig) $ takeWhile (/= ':') start of
	                      Just start | start `elem` sig.states
	                        -> finish start
	                      _ -> labRed "Enter or select an initial state!"

	buildUnifier = action if length treeposs /= 2 || any null treeposs
	                         then labMag "Select two proper subtrees!"
	                         else let t = trees!!curr
		 		          [p,q] = treeposs
		   			  u = getSubterm t p
		   			  v = getSubterm t q
	       			      unifyAct u v t t p q

	callEnum obj = action
	  (formula,joined) := (False,False)
	  matching := 0; matchBut.set [Text "match"]
	  splitBut.set [Text "join"]
	  clearTreeposs
	  setInterpreter obj
	  sig <- getSignature
	  let ts = case simplifyIter sig $ F "compl" [] of F "[]" us -> us
	  						   _ -> []
	      compl = curry $ setToFun $ zipWith f (evens ts) $ odds ts
	              where f t u = (showTerm0 t,showTerm0 u)
	  enum.buildEnum obj $ if obj `elem` ["alignment","palindrome"]
	  		       then compl else const2 False

	catchNode pt = action
	  if null treeposs then labMag "Select a target node!"
	  else (x,y) <- adaptPos pt
               case getSubtree (get ctree) x y of
               Just (p,cu) -> let z = root cu
			      if last treeposs <<= p then drawNode z cyan
	    					     else drawNode z magenta
			      node := Just (z,p)
			      canv.set [Cursor "sb_up_arrow"]
	       _ -> done

   	catchSubtree pt = action
	  if notnull trees
	     then (x,y) <- adaptPos pt
                  let Just ct = ctree
                  case getSubtree ct x y of
                  Just (p,cu) 
                     | p `elem` treeposs -> setTreeposs $ Remove p
				            drawSubtrees     -- deselect subtree
		     | True -> isSubtree := Just False       -- select subtree
		               setTreeposs $ Add [p]
	                       node := Just (root cu,p)
		               penpos := Just pt
		               subtree := Just cu
		               suptree := Just ct
	   	               canv.set [Cursor "hand2"]
		               drawSubtrees 
	          _ -> if notnull treeposs		     -- deselect last
		          then setTreeposs $ Remove $ last treeposs
	                       drawSubtrees 

   	catchTree pt = action
	  if notnull trees then
	     (x,y) <- adaptPos pt
	     let Just ct = ctree
	     case getSubtree ct x y of
	          Just (p,cu) | p `elem` treeposs 
	            -> isSubtree := Just True
		       let a = root cu	  		     -- cut subtree
		       node := Just (a,p); penpos := Just pt
		       subtree := Just cu
		       suptree := Just $ replace0 ct p $ V a
		       canv.set [Cursor "hand2"]
	          _ -> isSubtree := Just False
		       penpos := Just pt
		       canv.set [Cursor "crosshair"]         -- move tree
	  
	changeMode t = action 
	  sig <- getSignature
	  let b = isFormula sig t
	      str = if b then "formula" else "term"
	  if b /= formula then formula := b; treeMode := "tree"; trees := [t]
	  		       counter := upd counter 't' 1
			       curr := 0; treeSlider.set [To 0] 
			       paint.setCurrInPaint 0; termBut.set [Text str]
			       setNarrow False False
			  else trees := updList trees curr t
	
	changeSimpl x t = action 
	  case search ((== x) . root . pr1) simplRules of 
               Just i -> simplRules := updList simplRules i rule
	       _ -> simplRules := rule:simplRules
	  where rule = (leaf x,[],t)

	changeState ptr ps = action 
	  let proofElem = proof!!ptr
	  trees := proofElem.trees
	  counter := upd counter 't' $ length trees
	  treeMode := proofElem.treeMode
	  curr := proofElem.curr
          perms := proofElem.perms
	  varCounter := proofElem.varCounter
	  newPreds := proofElem.newPreds
	  solPositions := proofElem.solPositions
	  constraints := proofElem.constraints
	  joined := proofElem.joined
	  safe := proofElem.safe
	  setTreesFrame ps $ setSubst proofElem.substitution
	  labGreen proofElem.msgL
	  safeBut.set [CLabel $ eqsButMsg safe]	  
	  splitBut.set [Text $ if joined then "split" else "join"]
	  	
	changeStrat = action 
	  simplStrat := case simplStrat of DF -> BF; BF -> PA; PA -> DF
	  setStrat
	  
	checkBackward = action 
	  if proofTPtr < 1 
	     then labMag emptyProof; paint.labSolver emptyProof; enterRef
	  else proofTPtr := proofTPtr-1
	       case proofTerm!!proofTPtr of 
                    Mark _        -> treeposs := []; drawCurr
		    Matching _    -> if not $ null matchTerm 
		    		        then matchTerm := tail matchTerm
		    		             matching := head matchTerm
		    Refuting _    -> if not $ null refuteTerm 
		    		        then refuteTerm := tail refuteTerm
			                     refuting := head refuteTerm
		    SetStrat _    -> if not $ null stratTerm 
		    		        then stratTerm := tail stratTerm
			                     simplStrat := head stratTerm
		    Simplifying _ -> if not $ null simplTerm 
		    		        then simplTerm := tail simplTerm
			                     simplifying := head simplTerm
		    NegateAxioms ps1 cps1 
		      -> symbols := (minus ps ps2,minus cps cps2,cs,ds,fs,hs)
		         axioms := axioms `minus` axs
			 if proofPtr > 0 
			    then proofPtr := proofPtr-1
			         labColorToPaint green (proof!!proofPtr).msgL
		         where (ps,cps,cs,ds,fs,hs) = symbols
			       ps2 = map mkComplSymbol cps1
			       cps2 = map mkComplSymbol ps1
			       axs = noSimplsFor (ps2++cps2) axioms
	  	    _ -> if proofPtr > 0 then
		            proofPtr := proofPtr-1
		            changeState proofPtr $ (proof!!proofPtr).treePoss
	       enterRef

        checkForward = action
          if proofTPtr >= length proofTerm 
             then labMag endOfProof; paint.labSolver endOfProof; enterRef
          else let step = proofTerm!!proofTPtr
                   k = proofPtr+1
	       if deriveStep step && k < length proof then proofPtr := k
               case step of
		    AddAxioms axs -> addCongAxioms' axs
		    ApplySubst -> applySubst
		    ApplySubstTo x t -> applySubstTo' x t
	            ApplyTransitivity -> applyTransitivity
		    BuildKripke m -> buildKripke m
		    BuildRE -> buildRegExp
	            CollapseStep b -> collapseStep b
		    CollapseVars xs -> collapseVars' xs
		    ComposePointers -> composePointers
		    CopySubtrees -> copySubtrees
		    CreateIndHyp -> createIndHyp
		    CreateInvariant b -> createInvariant b
		    DecomposeAtom -> decomposeAtom
		    EvaluateTrees -> evaluateTrees
		    ExpandTree b n -> expandTree' b n
		    FlattenImpl -> flattenImpl
		    Generalize cls -> generalize' cls
		    Induction True n -> applyInduction n
		    Induction _ n -> applyCoinduction n
		    Mark ps -> treeposs := ps; drawCurr
		    Matching n -> matching := n; matchTerm := n:matchTerm
		    Minimize -> minimize
		    ModifyEqs m -> modifyEqs m
	            Narrow limit sub -> narrow' limit sub skip
		    NegateAxioms ps cps -> negateAxioms' ps cps
		    PermuteSubtrees -> permuteSubtrees
		    RandomLabels -> randomLabels
		    RandomTree -> randomTree
		    ReduceRE m -> reduceRegExp m
		    Refuting b -> refuting := b; refuteTerm := b:refuteTerm
		    ReleaseNode -> releaseNode (0,0)
		    ReleaseSubtree -> releaseSubtree (0,0)
		    ReleaseTree -> releaseTree (0,0)
		    RemoveCopies -> removeCopies
		    RemoveEdges b -> removeEdges b
		    RemoveNode -> removeNode
		    RemoveOthers -> removeOthers
		    RemovePath -> removePath
		    RemoveSubtrees -> removeSubtrees
		    RenameVar x -> renameVar' x
		    ReplaceNodes x -> replaceNodes' x
		    ReplaceOther -> replaceOther
		    ReplaceSubtrees ps ts -> replaceSubtrees' ps ts
		    ReplaceText x -> replaceText' x
		    ReplaceVar x u p -> replaceVar x u p
		    ReverseSubtrees -> reverseSubtrees
		    SafeEqs -> switchSafe
		    SetAdmitted block xs -> setAdmitted' block xs
		    SetCurr msg n -> setCurr msg n
		    SetStrat s -> simplStrat := s; stratTerm := s:stratTerm
		    Simplify limit sub -> simplify' limit sub skip
		    Simplifying b -> simplifying := b; simplTerm := b:simplTerm
		    ShiftPattern -> shiftPattern
		    ShiftQuants -> shiftQuants
		    ShiftSubs ps -> shiftSubs' ps
		    SplitTree -> splitTree
		    StretchConclusion -> stretch False
		    StretchPremise -> stretch True
		    SubsumeSubtrees -> subsumeSubtrees
		    Theorem b th -> applyTheorem b Nothing th
		    Transform m -> transformGraph m
		    UnifySubtrees -> unifySubtrees
	       proofTPtr := proofTPtr+1; enterRef

	checkInSolver = action checkingP := (False,True)
			       
	checkProof pterm inPainter file = action
	  if null trees then labBlue start
	  else case parse (list command) $ removeComment 0 pterm of
	       Just pt -> checking := True
	                  if inPainter then checkingP := (True,False)
	              		            paint.buildPaint True showTreePicts
		          proofTerm := pt; proofTPtr := 0; enterRef
		          initialize [] $ showTree fast $ trees!!curr
		          quit.set [Text "quit check", Command setDeriveMode]
		          labGreen $ proofLoaded file
		          runChecker False
	       _       -> labRed $ "There is no proof term in " ++ file ++ "."
          where f str act = [Text str, Command act]

	checkProofF inPainter = action 
	  str <- ent.getValue
	  case words str of 
	       [file,sp] -> let newSpeed = parse pnat sp
	       			fileT = file++"T"
	                    if just newSpeed then speed := get newSpeed
	                    pterm <- lookupLibs tk fileT
	                    checkProof pterm inPainter fileT
	       [file]    -> let fileT = file++"T"
	       		    pterm <- lookupLibs tk fileT
	                    checkProof pterm inPainter fileT
	       _         -> labMag "Enter a file name!"

	checkProofT inPainter = action 
	  sp <- ent.getValue
	  let newSpeed = parse pnat sp
	  if just newSpeed then speed := get newSpeed
	  pterm <- getTextHere
	  checkProof pterm inPainter tfield

	clearTreeposs = setTreeposs $ Replace []
				       			 
 	clearText continue = action 
 	  lg <- tedit.lines
	  sequence $ take lg $ repeat $ tedit.deleteLine 1
	  continue

	collapseStep b = action
	  if null trees then labBlue start
	  else let t = trees!!curr
	           p = emptyOrLast treeposs
		   u = getSubterm1 t p
		   n = counter 'c'
		   (v,part') = collapseLoop b (u,part) n
		   setPr = setProof True False "COLLAPSING THE SUBTREE" [p]
	       extendPT False False False False $ CollapseStep b
	       if u == v then setPr collapsed; clearTreeposs; resetLevel
	       		      part := (id,[])
	       else trees := updList trees curr $ replace1 t p v
	            setPr $ levelMsg n
	            drawCurr; counter := incr counter 'c'; part := part'

	collapseVarsCom = action
	  if null trees then labBlue start
	                else str <- ent.getValue
	                     collapseVars' $ words str

	collapseVars' xs = action
	  if null trees then labBlue start
	  else sig <- getSignature
	       let t = trees!!curr
		   ps = emptyOrAll treeposs
		   ts = map (collapseVars sig xs . getSubterm1 t) ps
	       trees := updList trees curr $ fold2 replace1 t ps ts
	       extendPT False False False False $ CollapseVars xs
	       setProof True False "COLLAPSING THE VARIABLES" ps collapsedVars
	       drawCurr

	composePointers = action
	  if null trees then labBlue start
	  else sig <- getSignature
	       let t = trees!!curr
		   ps = emptyOrAll treeposs
		   ts = map (composePtrs . getSubterm1 t) ps
	       trees := updList trees curr $ fold2 replace1 t ps ts
	       extendPT False False False False ComposePointers
	       setProof True False "COMBINING THE POINTERS OF THE TREES" ps 
	                pointersComposed
	       drawCurr

	compressAxioms b = action
	  sig <- getSignature
	  str <- ent.getValue
	  let x = if null trees || null treeposs then str
	          else label (trees!!curr) $ head treeposs
	      axs = noSimplsFor [x] axioms
	  if sig.isPred x || sig.isDefunct x || sig.isCopred x
             then if null axs then labRed $ noAxiomsFor [x]
   	          else let (th,k) = compressAll b sig (varCounter "z") axs
		       theorems := th:theorems
	               setZcounter k
	               enterFormulas [th]
	               labGreen $ newCls "theorems" tfield
	  else labMag "Enter a predicate, a defined function or a copredicate!"

	compressClauses = action
	  sig <- getSignature
	  str <- ent.getValue
	  let (exps,b) = numberedExps
	  case parse intRanges str of
	  Just (n:ks) | n < length exps
	    -> if b then let i = varCounter "z"
		             (th,k) = combineOne sig i ks (exps!!n) exps
			 theorems := th:theorems
			 setZcounter k
			 enterFormulas [th]
			 labGreen $ newCls "theorems" tfield
	            else labMag $ enterTfield "clauses"
	  _ -> labMag $ enterNumber ++ " Enter argument positions!"

	copySubtrees = action
	  if null trees then labBlue start
	  else let ps = emptyOrAll treeposs
	           t = trees!!curr
	       if any null ps then labMag "Select proper subtrees!"
	       else trees := updList trees curr $ copy ps t
	            extendPT False False False False CopySubtrees
	            let b = all idempotent $ map (label t . init) ps
	            setProof b False "COPYING THE SUBTREES" ps 
	            		     "The selected trees have been copied."
		    drawCurr
	  where copy (p:ps) t = copy (map (rshiftPos0 p) ps) t3
	  		      where t1 = rshiftPos p t; q = init p; k = last p+1
			            F x ts = getSubterm t1 q
				    u = F x $ take k ts++V"":drop k ts
				    t2 = mapT dec $ replace0 t1 q u
				    n = length p-1; p' = incrPos p n
				    t3 = replace0 t2 p' $ getSubterm t1 p
				    dec x = if isPos x && p' <<= q
			                    then mkPos0 $ decrPos q n else x
					    where q = getPos x 
	        copy _ t      = t

	createIndHyp = action
	  if null trees then labBlue start
	  else if null treeposs then labMag "Select induction variables!"
	       else let t = trees!!curr
	                xs = map (label t) treeposs
	            sig <- getSignature
	            if all sig.isFovar xs && 
		       and (zipWith (isAllOrFree t) xs treeposs)
		       then let f x = V $ if x `elem` xs then '!':x else x
				g f = mkTup $ map f xs
                                hyps = mkIndHyps t $ F ">>" [g f,g V]
                            trees := updList trees curr $ 
			                  mkAll (frees sig t `minus` xs) $ t>>>f
	                    indClauses := hyps; theorems := hyps++theorems
	                    extendPT False False False False CreateIndHyp
		            setProof True False "SELECTING INDUCTION VARIABLES"
			             treeposs $ indHypsCreated xs
			    clearTreeposs; drawCurr
	 	       else labMag "Select induction variables!"

	createInvariant b = action
	  if null trees then labBlue start
	  else sig <- getSignature
               let t = trees!!curr
	           (p,conj,i) = case treeposs of
                                [] -> ([],t,0)
				[q] | isFormula sig u -> (q,u,0) 
				    | notnull q -> ([],t,last q)		
				      where u = getSubterm t q	
			        [q,r] | notnull r -> (q,getSubterm t q,last r)
				_ -> ([],t,-1)	
	       if i > -1 && universal sig t p conj then
	          case preStretch True sig.isDefunct conj of
	          Just (f,varps)
		    -> case stretchPrem (varCounter "z") varps conj of
	  	       (F "===>" [F "=" [F _ xs,d],conc],m)
	                 -> let lg = length xs
			    if i < lg then 
                             case derivedFun sig f xs i lg axioms of
			     Just (loop,inits,ax)
                                           -- ax is f(xs)=loop(take i xs++inits) 
			       -> let n = m+length inits
				      (as,bs) = splitAt (lg-i) xs
				      cs = map newVar [m..n-1]
				      u = mkInvs b loop as bs cs inits d conc
			          trees := updList trees curr $ replace t p u
    	  		          setZcounter n
    	  		          extendPT False False False False $ 
    	  		          	   CreateInvariant b
	       		          setProof True False (creatingInvariant str ax) 
					   [p] $ invCreated b
	  		          clearTreeposs; drawCurr
			     _ -> labRed $ f ++ " is not a derived function."
                            else labRed $ wrongArity f lg
		       _ -> labRed concCallsPrem
		  _ -> notStretchable "The premise"
               else labRed $ noApp str
	  where str = if b then "HOARE INVARIANT CREATION"
	                   else "SUBGOAL INVARIANT CREATION"

	createSpecMenu add m = action
  	  mapM_ (mkButF m cmd) specfiles1
	  mkBut m (if add then "from file (Return)" else "from file")
	          $ getFileAnd cmd
	  if add then mkBut m "from text field" $ addSpecWithBase ""; done
  	  subMenu <- mkSub m "more"
  	  mapM_ (mkButF subMenu cmd) specfiles2
  	  subMenu <- mkSub subMenu "more"
  	  mapM_ (mkButF subMenu cmd) specfiles3
	  done
	  where cmd = if add then addSpecWithBase else loadText True

	createSub mode menu = action
	  mkBut menu "from file" $ getFileAnd $ addClauses mode
	  mkBut menu "from text field" $ addClauses mode ""
	  if mode == 0 then mkBut menu "from entry field" addCongAxioms; done
 	  mkBut menu ("from "++ other) $ do tree <- solve.getTree
	                                    case tree of 
                                                 Just t 
                                                   -> addTheorems t other done
	                                         _ -> labBlue $ startOther other
	  done

	decomposeAtom = action
 	  if null trees then labBlue start
	  else let t = trees!!curr
	           p = emptyOrLast treeposs
		   b = polarity True t p
		   F x [l,r] = getSubterm1 t p
		   finish u = action 
		    	       trees := updList trees curr $ replace1 t p u
		    	       extendPT False False False False DecomposeAtom
		               setProof True False "DECOMPOSING THE ATOM" [p] 
		                        atomDecomposed
			       clearTreeposs; drawCurr
	       sig <- getSignature
	       case x of "=" | b -> finish $ splitEq sig True l r
	                 "=/=" | not b -> finish $ splitEq sig False l r 
		         _ -> labRed atomNotDecomposed
	
	delay act = action tk.delay 100 $ const act; done

	derefNodes = action
	  if null trees then labBlue start
	  else let t = trees!!curr
		   ps = emptyOrAll treeposs
	       if all (isPos . root) $ map (getSubterm t) ps then
	          trees := updList trees curr $ dereference t ps
	          extendPT False False False False DerefNodes
	          setProof True False "DEREFERENCING THE NODES" ps dereferenced
	          clearTreeposs; drawCurr
	       else labMag "Select pointers!"
	
	draw ct = action
	  canv.clear
	  ctree := Just ct
	  let (_,_,maxx,maxy) = minmax $ foldT (bds sizeState) ct
	      size@(_,h) = (max 100 maxx,max 100 maxy)
	  canvSize := size
	  i <- canvSlider.getValue
	  if h > 100 then tedit.set [Height 0]
	  canv.set [ScrollRegion (0,0) size, Height $ min h 600]
	  canvSlider.setValue h
	  if null treeposs then drawTree ct ct black blue []
	  else drawTree2 ct ct black blue red darkGreen [] treeposs
          where bds (n,w) (a,(x,y)) pss = (x-x',y-y'):(x+x',y+y'):concat pss
                                where (x',y') = fromInt2 (nodeWidth w a,n`div`2)

	drawArc (x,y) ct color = action
	  if isPos a then done
	  else let (above,below) = textheight font
	       canv.line [(x,y+below),(x',y'-above)] [Fill color]; done
	  where (a,(x',y')) = root ct
	  
	drawCurr = action drawThis (trees!!curr) [] ""

	drawNewCurr _ = action if notnull trees then drawCurr

	drawNode (a,p) c = action
          if isPos a then done
          else canv.text p [Text $ delQuotes a,NamedFont font,Fill c',
          		    Justify CenterAlign]; done
	  where c' = case parse colPre a of Just (c,_) -> c; _ -> c

	drawRef ct ac p mid@(x,y) q = action
	  let (above,below) = textheight font
	      arc = [(x1,y1+below),(x,y-above)]
	      target = (x',if y <= y' then y'-above else y'+above)
	  if q `elem` allPoss ct then
	     draw (arc++[mid,target]) $ f $ if q << p then orange else magenta
	     done
	  else draw (arc++[(x-10,y+10)]) $ f red; done	     -- dangling pointer
 	  where (_,(x1,y1)) = label ct $ init p 	     -- pred of source
	        (_,(x',y')) = label ct q        	     -- target
		f color = if ac == white then white else color
		draw path color = do canv.line path 
				             [Fill color,Arrow Last,Smooth True]
	
	drawShrinked _ = action 
	  if notnull trees 
	     then draw $ shrinkTree (snd corner) (snd spread) $ get ctree

	drawSubtrees = action
	  let Just ct = ctree
          drawTree3 ct ct black blue red darkGreen [] treeposs $
		    minis (<<=) $ treeposs `join` oldTreeposs

        drawThis t ps col = action 
	  let qs = if showState then [] else treeposs
	      u = cutTree maxHeap (colHidden t) ps col qs
	  sizes@(_,w) <- mkSizes font $ nodeLabels u
	  sizeState := sizes
	  draw $ coordTree w spread corner u

-- drawTree ct ct nc ac [] draws the nodes of ct in color nc and the arcs of ct
-- in color ac.

 	drawTree (F cx@(_,q) cts) ct nc ac p = action
			               drawNode cx nc
				       drawTrees cts $ succsInd p cts
                                       where drawTrees (ct':cts) (p:ps) = action
			                                 drawArc q ct' ac
						         drawTree ct' ct nc ac p
							 drawTrees cts ps
				             drawTrees _ _ = done

	drawTree (V cx@(a,q)) ct nc ac p 
	  | isPos a = drawRef ct ac p q $ getPos a
          | True    = drawNode cx nc

-- drawTree2 ct ct nc ac nc' ac' [] qs draws the nested ones among the subtrees
-- at positions qs alternately in (text,line)-colors (nc,ac) and (nc',ac').

  	drawTree2 ct@(V _) ct0 nc ac nc' ac' p qs
	  | any (== p) qs = drawTree ct ct0 nc' ac' p
	  | True          = drawTree ct ct0 nc ac p
	drawTree2 (F cx@(_,q) cts) ct nc ac nc' ac' p qs
          | any (== p) qs = action drawNode cx nc'
	  			   drawTrees2 q cts nc' ac' nc ac ps
	  | True          = action drawNode cx nc
	  		           drawTrees2 q cts nc ac nc' ac' ps
	                    where ps = succsInd p cts
                                  drawTrees2 q (ct':cts) nc ac nc' ac' (p:ps) = 
			    	      action drawArc q ct' ac
				    	     drawTree2 ct' ct nc ac nc' ac' p qs
					     drawTrees2 q cts nc ac nc' ac' ps
	                          drawTrees2 _ _ _ _ _ _ _ = done

-- drawTree3 ct .. rs applies drawTree2 ct .. to the subtrees of ct at positions
-- rs.

  	drawTree3 ct' ct nc ac nc' ac' p qs rs
	  | any (<<= p) rs 		= drawTree2 ct' ct nc ac nc' ac' p qs
  	drawTree3 (V _) _ _ _ _ _ _ _ _ = done
	drawTree3 (F (_,q) cts) ct nc ac nc' ac' p qs rs = 
	  		   action drawTrees3 q cts nc ac nc' ac' ps
	                   where ps = succsInd p cts
                                 drawTrees3 q (ct':cts) nc ac nc' ac' (p:ps) = 
				   action drawTree3 ct' ct nc ac nc' ac' p qs rs
	            			  drawTrees3 q cts nc ac nc' ac' ps
			         drawTrees3 _ _ _ _ _ _ _ = done
	
	enterFormulas fs = action 
	  if not checking then clearText $ addText $ lines $ showFactors fs
	                       numberedExps := (fs,True)

	enterRef = action clearText $ addText $ lines $ show 
				    $ addPtr proofTPtr proofTerm
                   where addPtr 0 (step:pt) = POINTER step:pt
 			 addPtr n (step:pt) = step:addPtr (n-1) pt
 			 addPtr _ pt        = pt
	
        enterTerms ts = action 
	  if not checking then clearText $ addText $ lines $ showSum ts
	         	       numberedExps := (ts,False)

	enterText str = action 
	  if not checking then clearText $ addText $ lines str
	  		       numberedExps := ([],True)

	enterTree b t = action
	  setTime
	  formula := b; treeMode := "tree"; trees := [t]
	  counter := upd counter 't' 1
  	  proofTerm := []; proofTPtr := 0
	  clearTreeposs
	  sig <- getSignature
	  makeTrees sig $ action initialize (sigVars sig t) $ showTree fast t
				 setTreesFrame [] $ labGreen $ objects++str
	  where objects = if b then "Formulas" else "Terms"
                str = " are displayed on the canvas." 

	evaluateTrees = action
	  if null trees then labBlue start
	  else sig <- getSignature
	       let t = trees!!curr
		   ps = emptyOrAll treeposs
		   ts = map (evaluate sig . getSubterm1 t) ps
	       trees := updList trees curr $ fold2 replace1 t ps ts
	       extendPT False False False False EvaluateTrees
	       setProof True False "EVALUATING THE TREES" ps evaluated
	       clearTreeposs; drawCurr

	expandTree b = action
	  if null trees then labBlue start
	  else limit <- ent.getValue
	       expandTree' b $ case parse pnat limit of Just n -> n; _ -> 0

	expandTree' b limit = action
	  let t = trees!!curr
	      ps = emptyOrAll treeposs
	      f u p = replace0 u p $ (if b then expandOne else expand) limit t p 
	  trees := updList trees curr $ moreTree $ foldl f t ps
	  extendPT False False False False $ ExpandTree b limit
	  setProof True False "EXPANDING THE SUBTREES" ps $
	           expanded ++ circlesUnfolded limit
	  clearTreeposs; drawCurr
	  
	extendPT m r s t step = action
	  if not checking then 
	     let cm = m && (null matchTerm  || head matchTerm /= matching)
	         cr = r && (null refuteTerm || head refuteTerm /= refuting)
	         cs = s && (null simplTerm  || head simplTerm /= simplifying)
	         ct = t && (null stratTerm  || head stratTerm /= simplStrat) 
	     let steps = (if cm then [Matching matching]        else []) ++
                         (if cr then [Refuting refuting]        else []) ++
                         (if cs then [Simplifying simplifying]  else []) ++
	                  if ct then [SetStrat simplStrat,step] else [step]
	     if cm then matchTerm  := matching:matchTerm
	     if cr then refuteTerm := refuting:refuteTerm
	     if cs then simplTerm  := simplifying:simplTerm
	     if ct then stratTerm  := simplStrat:stratTerm
             proofTerm := take proofTPtr proofTerm ++ steps 
	     proofTPtr := length proofTerm

        finishDisCon k b c ts reduct redices t ps pred qs sig msg = action
	  case applyMany b c ts reduct redices t ps pred qs sig varCounter of
               Just (t,vc) -> maybeSimplify sig t $ action varCounter := vc
			      setProof True True msg ps $ thApplied k
			      clearTreeposs; drawCurr
	       _ -> labRed $ notUnifiable k

	finishRelease t p step = action
	  trees := updList trees curr t
	  extendPT False False False False step
	  clearTreeposs; drawCurr
	  setProof False False "REPLACING THE SUBTREE" [p] 
	  		       "The selected tree has been replaced."

	flattenImpl = action
	  if null trees || not formula
	     then labMag "Load and parse a Horn or co-Horn clause!"
	     else sig <- getSignature
                  let t = trees!!curr
	              xs = if null treeposs then defuncts sig t
		           else filter sig.isDefunct $ map (label t) treeposs
		      (u,n) = flatten (varCounter "z") xs t
	          trees := updList trees curr u
	          setZcounter n
	          extendPT False False False False FlattenImpl
	          setProof True False "FLATTENING" [[]] flattened
	          clearTreeposs; drawCurr

        foldrM act continue (x:s) = action act (foldrM act continue s) x
        foldrM _ continue _       = continue

	forwProof = action
          if checking then checkForward
	  else let k = proofPtr+1
		   lg = length proofTerm
	       if k >= length proof then labMag endOfProof 
			            else proofPtr := k; changeState k []
	       if proofTPtr < lg
                  then let n = search deriveStep $ drop proofTPtr proofTerm
	               proofTPtr := if just n then proofTPtr+get n+1 else lg

	generalize = action
	  if null trees then labBlue start
	  else let t = trees!!curr
	           p = emptyOrLast treeposs
	           cl = getSubterm t p
               sig <- getSignature
               if isTerm sig cl then labRed "Select a formula!"
	       else str <- ent.getValue
	            let (exps,b) = numberedExps
	            case parse intRanges str of
	            Just ns | all (< (length exps)) ns
	              -> if b then generalizeEnd t p cl $ map (exps!!) ns
		         else labMag $ enterTfield "formulas"
                    _ -> str <- getTextHere
 	                 case parseE (implication sig) str of
	                      Correct cl' -> generalizeEnd t p cl [cl']
	                      p -> incorrect p str $ illformed "formula"

	generalize' cls = action
          let t = trees!!curr
	      p = emptyOrLast treeposs
	  generalizeEnd t p (getSubterm t p) cls

	generalizeEnd t p cl cls = action 
	  trees := updList trees curr $ replace t p $ f $ cl:cls
	  enterFormulas cls
	  extendPT False False False False $ Generalize cls
	  setProof True False "GENERALIZING" [p] generalized
	  clearTreeposs; drawCurr
	  where f = if polarity True t p then mkConjunct else mkDisjunct

	getEntry = request ent.getValue

	getFileAnd act = action
	  file <- ent.getValue
	  if null file then labMag "Enter a file name!" else act file

        getFont = request return font
				   
	getInterpreter = do
	  sig <- getSignature
	  return $ case picEval of 
	                "tree"               -> widgetTree sig tk
			"matrices"           -> searchPic matrix tk
	                "matrix solution"    -> solPic sig matrix tk
		        "linear equations"   -> linearEqs tk
	                "level partition"    -> searchPic (partition 0) tk
	                "preord partition"   -> searchPic (partition 1) tk	   
	                "heap partition"     -> searchPic (partition 2) tk
	                "hill partition"     -> searchPic (partition 3) tk
	                "alignment"          -> searchPic alignment tk
	                "palindrome"         -> searchPic alignment tk
	                "dissection"         -> searchPic dissection tk
	                _                    -> searchPic (widgets sig black) tk
				   
	getPicNo = request return picNo

	getSignature = do
	  let (ps,cps,cs,ds,fs,hs) = symbols
	      (sts,labs,ats,tr,trL,va,vaL) = kripke
	      isPred       = (`elem` ps)  ||| projection
	      isCopred     = (`elem` cps) ||| projection
	      isConstruct  = (`elem` cs)  ||| just . parse int ||| 
	                     just . parse real ||| just . parse quoted ||| 
			     just . parse (strNat "inj")
	      isDefunct    = (`elem` ds) ||| projection
	      isFovar      = (`elem` fs) . base
	      isHovar      = (`elem` (map fst hs)) . base
	      hovarRel x y = isHovar x &&
	      		     case lookup (base x) hs of
			          Just es@(_:_) -> isHovar y || y `elem` es
				  _ -> not $ isFovar y
	      (block,xs)   = constraints
	      blocked x    = if block then z `elem` xs else z `notElem` xs
	                     where z = head $ words x
	      simpls = simplRules; transitions = transRules
 	      states = sts; atoms = ats; labels = labs
 	      trans = tr; transL = trL; value = va; valueL = vaL
 	      safeEqs = safe; redexPos = []
	  return $ struct ..Sig
	  where base = pr1 . splitVar
				   
	getSignatureR = request getSignature

	getSolver = request return this

	getText = request getTextHere

	getTextHere = do 
	  lg <- tedit.lines
	  strs <- mapM tedit.getLine [1..lg]
	  return $ removeDot $ unlines $ map (removeCommentL . f) strs
	  where f (' ':' ':x:'>':str) | isDigit x	    = str
                f (' ':x:y:'>':str)   | all isDigit [x,y]   = str
                f (x:y:z:'>':str)     | all isDigit [x,y,z] = str
                f str = str

	getTree = request if null trees then labBlue start
				             return Nothing
			                else return $ Just $ trees!!curr
				
	hideOrShow = action
	  if null trees then labBlue start 
	  else drawCurr; showState := not showState
	       hideBut.set [Text $ if showState then "hide" else "show"]
	  
	incorrect p str error = action
	  case p of Partial t rest -> enterText $ showTree False t ++ check rest
	            _ -> enterText str
	  labRed error

	incrCurr b = action let next = if b then curr+1 else curr-1
	                    setCurr newCurr $ next `mod` length trees

	incrEntry b = action 
	  str <- ent.getValue
	  let k = parse nat str; f = ent.setValue . show
	  if b then case k of Just n -> f $ n+1
	  		      _ -> ent.setValue "0"
	       else case k of Just n | n > 0 -> f $ n-1
	                      _ -> ent.setValue ""
	       
	initCanvas continue = action 
	  if null trees then enterTree False $ F "see painter" []
	  		     delay continue
			else continue

	initialize vars str = action
	  let (ps,cps,cs,ds,fs,hs) = symbols
	      (nps,ncps) = newPreds
	      (ps',cps') = (ps `minus` nps,cps `minus` ncps)
	      (block,xs) = constraints
          symbols := (ps',cps',cs,ds,fs,hs)
	  axioms := removeTerms axioms indClauses
	  theorems := removeTerms theorems indClauses
	  indClauses := []; newPreds := nil2; solPositions := []
	  varCounter := iniVC $ ps'++cps'++cs++ds++fs++map fst hs++vars
	  setSubst (V,[]); part := (id,[])
	  perms := \n -> [0..n-1]
	  proof := [struct msg = "Derivation of\n\n"++str++
	                         '\n':'\n':admitted block xs++
	  			 '\n':equationRemoval safe
		           msgL = ""; treeMode = treeMode; trees = trees
		           treePoss = []; curr = 0; varCounter = varCounter
		           perms = perms; newPreds = nil2; solPositions = []
			   substitution = (V,[]); constraints = constraints
			   joined = joined; safe = safe]
	  (proofPtr,counter,curr) := (0,const 0,0)	
	       
	instantiate = action
	  if null trees then labBlue start
          else sig <- getSignature
               str <- ent.getValue
	       let str' = removeComment 0 str
	       case parseE (term sig) str' of
	            Correct t -> replaceQuant t (emptyOrLast treeposs) $
	       				        labRed notInstantiable
	            p -> incorrect p str' $ illformed "term"

	isSolPos n = request return $ n `elem` solPositions
			 
	kleeneAxioms = action
	  sig <- getSignature
	  str <- ent.getValue
	  let x = if null trees || null treeposs then str
	          else label (trees!!curr) $ head treeposs
	      copred = sig.isCopred x
	      f = if copred then mkHornLoop else mkCoHornLoop
	  if copred || sig.isPred x  then
	     let axs = noSimplsFor [x] axioms
	     if null axs then labRed $ noAxiomsFor [x]
   	     else let i = V $ 'i':show (varCounter "i")
		      (axs',k) = f sig x axs i $ varCounter "z"
                      (ps,cps,cs,ds,fs,hs) = symbols
		      ps' = x:(x++"Loop"):ps
                  symbols := (ps',cps`minus1`x,cs,ds,fs,hs)
		  axioms := axioms `minus` axs `join` axs'
                  varCounter := upd (incr varCounter "i") "z" k
	          enterFormulas axs'
	          labGreen $ if copred then newPredicate str1 str2 x
				       else newPredicate str2 str1 x
	  else labMag "Enter either a predicate or a copredicate!"
	  where (str1,str2) = ("copredicate","predicate")

	labBlue str = labColor str $ light blue

	labColor str col = action 
	  if just osci then (get osci).stopRun0
	  if col == red then osci0 <- oscillator tk act act 1 10 249
			     osci0.startRun 30
                             osci := Just osci0
                        else lab.set [Text str, Background col]
          where act n = action lab.set [Text str, Background $ RGB 255 n 0]

	labColorToPaint col str = action labColor str $ light col
					 paint.labSolver str

	labColorToPaintT col str = action 
	  let (time0,timeout) = times
	  if time0 == 0 then labColor str $ light col
	  else time <- tk.timeOfDay; times := (0,300)
	       labColor (str++'\n':show (mkSecs time0 time)++" sec") $ light col
	  paint.labSolver str

	labGreen str = labColor str $ light green

	labMag str = labColor str $ light magenta
	
	labRed str = labColor str red

   	loadText b file = action 
          str <- lookupLibs tk file
	  if null str then labRed $ file ++ " is not a file name." 
                      else if b then enterText str; labGreen $ loaded file
                                else solve.bigWin; solve.enterText str

	makeTrees sig continue = action
	  case treeMode of
	  "tree"    -> if joined then continue 
		       else case trees of 
		            [t@(F "|" _)]   -> solPositions := []
		                 	       split (mkSummands t) "summand"
			    [t@(F "&" _)]   -> solPositions := []
					       split (mkFactors t) "factor"
			    [t@(F "<+>" _)] -> split (mkTerms t) "term"
			    _               -> continue
	  "summand" -> if null ts then actMore [mkFalse] "tree" 
	  			  else actMore ts treeMode
	               where ts = mkSummands $ F "|" trees
	  "factor"  -> if null ts then actMore [mkTrue] "tree" 
	  			  else actMore ts treeMode
	               where ts = mkFactors $ F "&" trees
	  _         -> if null ts then actMore [unit] "tree" 
	  			  else actMore ts treeMode 
	               where ts = mkTerms $ F "<+>" trees
	  where split = actMore . map (dropnFromPoss 1)
	        actMore ts mode = action 
		          newTrees := newTrees || lg /= length trees
		          curr := curr `mod` lg
			  trees := ts; counter := upd counter 't' lg
			  treeMode := if lg == 1 then "tree" else mode
			  solPositions := if formula then solPoss sig ts else []
			  continue
			  where lg = length ts
			  
	maybeSimplify sig t continue = 
	  do trees := updList trees curr $ if simplifying 
	  				   then simplifyIter sig t else t
	     continue

	minimize = action
	  sig <- getSignature
	  if notnull sig.states then
	     let (states,tr,trL,va,vaL) = mkQuotient sig
	     kripke := (states,sig.labels,sig.atoms,tr,trL,va,vaL)
	     changeSimpl "states" $ mkList states
	     extendPT False False False False Minimize
	     setProof True False "MINIMIZING THE KRIPKE MODEL" [] $ minimized 
	  						         $ length states

	modifyEqs m = action
	  if null trees then labBlue start
	  else sig <- getSignature
	       let t = trees!!curr
	      	   ps@(p:qs) = emptyOrAll treeposs
	      	   u = getSubterm1 t p
		   act p u = action 
		       trees := updList trees curr $ replace1 t p u
		       extendPT False False False False $ ModifyEqs m
	       	       setProof False False "MODIFYING THE EQUATIONS" [p] $
                                            eqsModified
		       clearTreeposs; drawCurr
 	       case m of 0 -> case parseEqs u of       -- connect equations
			           Just eqs 
				     -> let t = connectEqs eqs
					if firstClick
					   then act p t
					        substitution := (const t,[])
					        firstClick := False
					   else let (f,_) = substitution
					            u = eqsToGraphs $ f ""
					        act p u
					        substitution := (V,[])
					        firstClick := True
			           _ -> labMag "Select iterative equations!"
			 1 -> case parseEqs u of       -- regular equations
			           Just eqs -> act p $ eqsTerm
			           		     $ autoEqsToRegEqs sig eqs
			           _ -> labMag "Select iterative equations!"
			 2 -> if isV u then
			       case parseEqs t of -- substitute variables in eqs
			            Just eqs | just v -> act [] $ get v
			           	       where v = substituteVars t eqs ps
			            _ -> labMag instantiateVars
			      else 
			       case parseEqs u of -- substitute variables in eqs
			            Just eqs | just v -> act p $ get v
			           	       where v = substituteVars t eqs qs
			            _ -> labMag instantiateVars
			 _ -> case parseIterEq u of    -- solve regular equation
			           Just eq | just t -> act p $ get t 
			                     where t = solveRegEq sig eq
			           _ -> labMag "Select a regular equation!"
 
	moveNode pt = action
	  if null treeposs then labMag "Select a target node!"
	  else let f p a col1 col2 = drawNode a $ if last treeposs <<= p
	                                          then col1 else col2
	       case node of Just (z,p) -> f p z red black; done
	                    _ -> done
	       (x,y) <- adaptPos pt
               case getSubtree (get ctree) x y of 
	            Just (p,cu) -> let a = root cu
			           f p a cyan magenta
			           node := Just (a,p)
	            _ -> node := Nothing

       	moveSubtree pt@(x1,y1) = action
	  if just isSubtree && just penpos 
	     then let Just ct = ctree
		      Just cu = subtree
		      (x0,y0) = get penpos
                      cu' = transTree2 (x1-x0,y1-y0) cu
	              p = last treeposs
                  if firstMove && not (get isSubtree)
                     then drawTree cu ct black blue p
		     else drawTree cu ct white white p
		  firstMove := False
		  penpos := Just pt
		  subtree := Just cu'
		  if just node then drawNode (fst $ get node) black
   		  drawTree cu' ct red darkGreen p
	       	  (x,y) <- adaptPos pt
	   	  case getSubtree (get suptree) x y of
		       Just (p,cu) -> let a = root cu
			              drawNode a magenta
			              node := Just (a,p)
	               _ -> node := Nothing

        moveTree p@(x,y) = action
	  case isSubtree of
	  Just True -> moveSubtree p
	  Just _ -> if just penpos then let (x0,y0) = get penpos
	                                    q@(i,j) = (x-x0,y-y0)
			                draw $ transTree2 q $ get ctree
			                corner := (fst corner+i,snd corner+j)
			                penpos := Just p
	  _ -> done

	narrow continue = action 
	  if null trees then labBlue start
	  else str <- ent.getValue 
	       case parse pnat str of
	            Just limit -> narrow' limit True continue
		    _ -> case parse pnatSecs str of
		              Just n -> times := (fst times,n)
			      		narrow' (-1) True continue
		              _ -> narrow' 1 False continue

	narrow' limit sub continue = action
	  sig <- getSignature
	  ruleString := if formula then "NARROWING" else "REWRITING"
	  counter := upd counter 'd' 0
	  let t = trees!!curr
	      cls = filter (not . isSimpl) axioms
	  if null treeposs 
	     then extendPT True True True True $ Narrow limit True; setTime
                  narrowLoop sig cls 0 limit continue
	  else if sub then setTime; narrowSubtree t sig cls limit continue
	       else extendPT True True True True $ Narrow 0 False
		    narrowOrRewritePar t sig (Just $ -1) cls False treeposs 
		    		       continue

	narrowLoop sig cls k limit continue | limit == 0 = finish k
					    | True       = action
	  let t = trees!!curr
	  case treeMode of
	  "tree" | formula ->
	                if isSol sig t then solPositions := [0]; finishNar k
	                else solPositions := []
		             if joined then mkStep t $ finish k
		             else case trees of [F "|" ts] -> split ts "summand"
			                        [F "&" ts] -> split ts "factor"
			                        _ -> mkStep t $ finish k
	         | True -> if joined then mkStep t $ finish k
		           else case trees of [F "<+>" ts] -> split ts "term"
			                      _ -> mkStep t $ finish k
	  "summand" | simplifying -> case t of F "|" ts -> actMore ts treeMode
		                    	       F "&" ts -> actMore ts "factor"
					       _ -> actMore [t] "tree"
	            | null ts -> actMore [mkFalse] "tree" 
		    | True    -> actMore ts treeMode
		                 where t = simplifyIter sig $ mkDisjunct trees
		                       ts = mkSummands $ F "|" trees
	  "factor"  | simplifying -> case t of F "|" ts -> actMore ts "summand"
		                     	       F "&" ts -> actMore ts treeMode
					       _ -> actMore [t] "tree"
	            | null ts -> actMore [mkTrue] "tree" 
		    | True    -> actMore ts treeMode
	                         where t = simplifyIter sig $ mkConjunct trees
				       ts = mkFactors $ F "&" trees
	  _ | simplifying -> case t of F "<+>" ts -> actMore ts "term"
		                       _ -> actMore [t] "tree"
	    | null ts -> actMore [unit] "tree" 
	    | True    -> actMore ts treeMode
	                 where t = simplifyIter sig $ mkSum trees
			       ts = mkTerms $ F "<+>" trees
	  where finish = makeTrees sig . finishNar
                finishNar k = action 
			        setProof True True ruleString [] $
			    	    finishedNar formula k ++ solved solPositions
                                setTreesFrame [] continue
	        mkStep t stop = action narrowStep sig cls limit t proceed stop 
						  formula
	        proceed t n vc = action varCounter := vc				 
		      			counter := upd counter 'd' k'
                      			trees := updList trees curr t
					narrowLoop sig cls k' (limit-n) continue
	              			where k' = k+n
	        split = actMore . map (dropnFromPoss 1)
		actMore ts mode = action 
		  let b n = newTrees || lg /= length trees || curr /= n
		      searchRedex (n:ns) = do newTrees := b n; curr := n
		                              mkStep (trees!!n) $ searchRedex ns
		      searchRedex _ = do newTrees := b 0; curr := 0; finish k
		      ks = drop curr is++take curr is
		  trees := ts; counter := upd counter 't' lg
	          treeMode := if lg == 1 then "tree" else mode
		  solPositions := if formula then solPoss sig ts else []
		  searchRedex $ ks `minus` solPositions
		  where lg = length ts; is = indices_ ts

	narrowOrRewritePar t sig k cls saveRedex ps continue = action
	  let f g = g t sig k cls saveRedex [] ps [] varCounter continue
	  if formula || ps `subset` boolPositions sig t then f narrowPar 
	  						else f rewritePar
        -- used by applyTheorem,narrow' 

	narrowPar t sig k cls saveRedex used (p:ps) qs vc continue = action
	  if p `notElem` positions t -- || isVarRoot sig redex
	     then narrowPar t sig k cls saveRedex used ps qs vc continue
	  else let b = matching > 1
	           apply at r = applyAxs cls1 cls3 axioms redex at r sig vc' b
	           applyR at r = applyAxsR cls1 cls3 axioms rand redex at r sig
		   			   vc' b
	       if sig.isDefunct sym then
		  case atomPosition sig t p of
		  Just (q,at,r) 
		    -> if even matching then 
                          case apply at r of
                               (Backward reds vc,used')
	                         -> proceed q mkDisjunct reds used' vc
			       (MatchFailure str,_) -> labMsg str
			       _ -> next
		       else case applyR at r of 
		       	         (Backward reds vc,used',rand')
	                           -> rand := rand'
				      proceed q mkDisjunct reds used' vc 
			         (MatchFailure str,_,_) -> labMsg str
  	                         _ -> next
  	          _ -> next
	       else if notnull p && isTerm sig redex then
	               let q = init p
	               case (getSubterm t q,last p) of
	               (at@(F "->" [_,_]),0)
	                 -> if even matching then 
                               case apply at [0] of
                                    (Backward reds vc,used')
	                              -> proceed q mkDisjunct reds used' vc
 			            (MatchFailure str,_) -> labMsg str
 	                            _ -> next
			    else case applyR at [0] of 
                                      (Backward reds vc,used',rand')
	                                -> rand := rand'
				           proceed q mkDisjunct reds used' vc
 			              (MatchFailure str,_,_) -> labMsg str
 	                              _ -> next
 	               _ -> next
	            else if sig.isPred sym then
		            if even matching then
                               case apply redex [] of
                                    (Backward reds vc,used') 
		                      -> proceed p mkDisjunct reds used' vc
			            (Forward reds vc,used') 
			              -> proceed p mkConjunct reds used' vc
			            (MatchFailure str,_) -> labMsg str
   	                            _ -> next
		            else case applyR redex [] of
                                      (Backward reds vc,used',rand') 
		                        -> rand := rand'
				           proceed p mkDisjunct reds used' vc
	                              (Forward reds vc,used',rand') 
			                -> rand := rand'
				           proceed p mkConjunct reds used' vc
			              (MatchFailure str,_,_) -> labMsg str
				      _ -> next
	                 else if sig.isCopred sym then
		                 if even matching then
                                    case apply redex [] of
                                         (Backward reds vc,used') 
		                           -> proceed p mkDisjunct reds used' vc
                                         (Forward reds vc,used') 
					   -> proceed p mkConjunct reds used' vc
			                 (MatchFailure str,_) -> labMsg str
   	                                 _ -> next
		                 else case applyR redex [] of
                                      (Backward reds vc,used',rand') 
		                        -> rand := rand'
				           proceed p mkDisjunct reds used' vc
	                              (Forward reds vc,used',rand') 
			                -> rand := rand'
				           proceed p mkConjunct reds used' vc
				      (MatchFailure str,_,_) -> labMsg str
				      _ -> next
	                      else next
	  where redex = getSubterm t p; sym = getOp redex
	        cls1 = filterClauses sig redex cls
		cls2 = if saveRedex then map copyRedex cls1 else cls1
	        (cls3,vc') = renameApply sig t vc cls2
                proceed q f reds used' vc = 
	                  narrowPar (replace t q $ f reds) sig k cls saveRedex 
			            (used `join` used') ps (p:qs) vc continue
	        next = narrowPar t sig k cls saveRedex used ps qs vc continue
		labMsg = labColorToPaint magenta
	narrowPar _ _ k _ _ [] _ _ _ _ = 
	  labColorToPaint magenta $ subtreesNarrowed k
	narrowPar t sig k _ _ used _ qs vc continue = action
	  varCounter := vc
	  maybeSimplify sig t $ makeTrees sig $ finish $ thApplied k
	  where finish msg = action counter := upd counter 'd' 1
				    setProof True True (applied b str) qs msg
				    setTreesFrame [] continue
	        b = length used == 1
		str = showFactors used
	 
	narrowStep sig cls limit t proceed stop nar = action
	  time <- tk.timeOfDay
	  let (time0,timeout) = times
	      m = if limit < 0 then 1 else limit
	  if mkSecs time0 time > timeout then stop
	  else if even matching then
	          let (u,n,vc) = applyLoop t m varCounter cls axioms sig nar 
		    		           matching refuting simplifying 
                  if n == 0 then stop else proceed u n vc
               else let (u,n,vc,rand') = applyLoopRandom rand t m varCounter cls 
	       				     axioms sig nar matching simplifying
	            rand := rand'
                    if n == 0 then stop else proceed u n vc
 	
        -- used by narrowLoop,narrowSubtree
        
	narrowSubtree t sig cls limit continue = action
	  let p = emptyOrLast treeposs
	      u = getSubterm t p
	      nar = isFormula sig u
	      sn = subtreesNarrowed (Nothing :: Maybe Int)
	      (str,str') = if nar then ("NARROWING",sn)
	                          else ("REWRITING",sn++onlyRew)
	      proceed u n vc = action
	           let v = if simplifying then simplifyIter sig u else u
		   trees := updList trees curr $ replace t p v
	           varCounter := vc
	           extendPT True True True True $ Narrow limit True
	           counter := upd counter 'd' n
	           setProof True True str [p] $ appliedToSub (map toLower str) n
	           setTreesFrame [] continue
	      stop = action labColorToPaint magenta str'; continue
	  narrowStep sig cls limit u proceed stop nar
	  
        -- used by narrow' 
	
	negateAxioms = action
	  str <- ent.getValue
	  sig <- getSignature
	  let xs = if null trees || null treeposs then words str
	           else map (label $ trees!!curr) treeposs 
	      (ps,cps,cs,ds,fs,hs) = symbols
	      ps1 = filter sig.isPred $ meet xs ps
	      cps1 = filter sig.isCopred $ meet xs cps
	  negateAxioms' ps1 cps1
	
        negateAxioms' ps1 cps1 = action
	  let (ps,cps,cs,ds,fs,hs) = symbols
	      xs = ps1++cps1
	      axs1 = noSimplsFor xs axioms
	      ps2 = map mkComplSymbol cps1
	      cps2 = map mkComplSymbol ps1
	      str = complsAdded xs
	      msg = init str ++ " (see " ++ tfield ++ ")."
	  if null axs1 then labRed $ "There are no axioms for "++ showStrList xs
	     else symbols := (ps `join` ps2,cps `join` cps2,cs,ds,fs,hs)
	          sig <- getSignature
	          let axs2 = map (mkComplAxiom sig) axs1
	          axioms := axioms `join` axs2
	          enterFormulas axs2
	          if null trees then labGreen msg
		                else extendPT False False False False $ 
		  		              NegateAxioms ps1 cps1
		                     setProof True False str [] msg

        notStretchable str = labRed $ str ++ " cannot be stretched."
	
	parseConjects sig file conjs continue = action 
	  case parseE (implication sig) conjs of
	       Correct t -> let ts = if isConjunct t then subterms t else [t]
		            conjects := conjects `join` ts 
                            labGreen $ newCls "conjectures" file; continue
	       p -> incorrect p conjs $ illformed "formula"

	parseSigMap file str = action
          sig <- getSignature
          sig' <- solve.getSignatureR
	  case parseE (sigMap signatureMap sig sig') str of
	       Correct f -> signatureMap := f
	                    labGreen $ newSigMap file
	       Partial f rest -> enterText $ showSignatureMap f $ check rest
	                         labRed $ illformed "signature map"
	       _ -> enterText str
	            labRed $ illformed "signature map"

	parseText = action
	  str <- ent.getValue
	  let (exps,b) = numberedExps
	  case parse intRanges str of
	       Just ns | all (< (length exps)) ns
	         -> ent.setValue ""
		    let exps' = map (exps!!) ns
		    if b then enterTree True $ mkConjunct exps'
		         else enterTree False $ mkSum exps'
	       _ -> str <- getTextHere
	            sig <- getSignature
	  	    case parseE (implication sig) str of
	                 Correct t -> enterTree True t
	                 _ -> case parseE (term sig) str of
	                      Correct cl -> enterTree False cl
	                      p -> incorrect p str $ illformed "term or formula"
	
	parseTerms sig file ts continue = action 
	  case parseE (term sig) ts of
	       Correct t -> let ts = if isSum t then subterms t else [t]
	                    terms := ts `join` terms
	                    labGreen $ newCls "terms" file; continue
	       p -> incorrect p ts $ illformed "term"
	
	parseTree = action
	  if null trees then labBlue start
	  else let t = trees!!curr
                   enter _ _ [t] = enterText $ showTree fast t
	           enter b "" ts = if b then enter True "&" ts
	                                else enter False "<+>" ts
		   enter _ x ts  = enterText $ showTree fast $ F x ts
	       if null treeposs 
                  then enter formula "" [t]; labGreen treeParsed
	          else sig <- getSignature
		       let ts@(u:us) = map (getSubterm1 t) treeposs
			   b = isFormula sig u
	               if null $ tail treeposs
			  then enter b "" [u]; labGreen treeParsed
	                  else if all (== b) $ map (isFormula sig) us
	                          then x <- ent.getValue
			               enter b x $ addNatsToPoss ts
	                               labGreen $ init treeParsed ++ "s."
			          else labMag "Select either formulas or terms!"
			          
	permuteSubtrees = action 
          if null trees then labBlue start
	  else let t = trees!!curr
	  	   p = emptyOrLast treeposs
               case getSubterm1 t p of 
                    F x ts@(_:_:_)
		      -> let n = length ts
		         perms := upd perms n $ nextPerm $ perms n
		         trees := updList trees curr $ replace1 t p $ F x 
		         			     $ map (ts!!) $ perms n
		         extendPT False False False False PermuteSubtrees
		         setProof (permutative x) False 
		                  "PERMUTING THE SUBTREES" [p] permuted
		         drawCurr
		    _ -> done 
	
	randomLabels = action
	  if null trees then labBlue start
	  else str <- ent.getValue
	       if null str then labRed "Enter labels!"
	       else let (t,rand') = labelRandomly rand str $ trees!!curr
	            trees := updList trees curr t
	            rand := rand'
	            extendPT False False False False RandomLabels
	            setProof False False "LABELING NODES" [] 
	       			         "The nodes have been labeled randomly."
	            drawCurr
		
	randomTree = action
	  str <- ent.getValue
	  if null str then labRed "Enter labels!"
	  else let (t,rand') = buildTreeRandomly rand str
	       enterTree False t
	       rand := rand'
	       extendPT False False False False RandomTree 
	       delay $ setProof False False "GENERATING A TREE" [] 
	       		        "A tree has been generated randomly."
		
	reAddSpec = action if null specfiles then labRed iniSpec
	                   else removeSpec $ addSpecWithBase $ head specfiles

	redrawTree = action if notnull treeposs then restore := True
	                               		     clearTreeposs
			    drawCurr
			    
	reduceRegExp mode = action 	
	  if null trees then labBlue start
	  else sig <- getSignature
	       let ps = emptyOrAll treeposs
	           t = trees!!curr
	           ts = map (getSubterm t) ps
	           es = map (parseRE sig) ts
	           f (Just (e,_)) = showRE $ g e
	           g = case mode of 0 -> distribute
	           	            1 -> reduceLeft
	           	            _ -> reduceRight
	       if any nothing es then labMag "Select regular expressions!"
	       else trees := updList trees curr $ fold2 replace0 t ps $ map f es
	       	    extendPT False False False False $ ReduceRE mode
	       	    setProof False False "REDUCING THE REGULAR EXPRESSIONS" 
	       	       			 ps regReduced
	       	    clearTreeposs; drawCurr

	refNodes = action
	  if null trees then labBlue start
	  else let t = trees!!curr
		   p:ps = emptyOrAll treeposs
               if all (eqTerm $ expand0 t p) $ map (expand0 t) ps then
	          trees := updList trees curr $ reference t p ps
	          extendPT False False False False RefNodes
	          setProof True False "REFERENCING THE NODES" ps referenced 
	          clearTreeposs; drawCurr	
	       else labMag "Select roots of equal subterms!"
	       	    
	releaseNode _ = action
	  if null treeposs then labMag "Select a target node!"
	  else let p = last treeposs
	       case node of 
	            Just (z,q) | notnull q && p /= q
                      -> let t = replace (trees!!curr) q $ mkPos p
                         if p `elem` allPoss t
	                    then trees := updList trees curr t
	                         let r = init q
	                         extendPT False False False False ReleaseNode
	                         setProof False False "INSERTING AN ARC" [r,p] $ 
	                         	  arcInserted r p
	                    else labRed referenced
		         drawCurr
		         node := Nothing
		         canv.set [Cursor "left_ptr"]
	            _ -> labMag "Select a non-root position!"

       	releaseSubtree _ = action
	  if just isSubtree then
	     let source = last treeposs
		 finish = action drawTree (get subtree) (get ctree) white white 
		 		          source; drawSubtrees
	     case node of
	          Just (_,target)
		    -> let t = trees!!curr
			   u = getSubterm1 t source
		       if source == target then finish
	                  else tk.bell; if null target then changeMode u
			       replaceQuant u target
                              	   $ finishRelease (replace2 t source t target)
			       	                   source ReleaseSubtree
                  _ -> setTreeposs $ Remove source; finish
	     resetCatch

        releaseTree _ = action
	  case isSubtree of 
          Just True -> let t = trees!!curr
	  	           p = last treeposs
			   u = getSubterm1 t p
			   z = 'z':show (varCounter "z")
			   (g,dom) = substitution
			   f t = finishRelease t p ReleaseTree
		       case node of Just (_,q) -> if p /= q then 
		       				     tk.bell; f $ replace1 t q u
				    _ -> f $ replace t p $ V z
	               setSubst (g `andThen` for u z, dom `join1` z)
		       varCounter := incr varCounter "z"
		       resetCatch
 	  Just False -> penpos := Nothing
	                canv.set [Cursor "left_ptr"]
			resetCatch
          _ -> done

	removeAxiomsFor = action 
	  str <- ent.getValue
	  let xs = if null trees || null treeposs then words str
	           else mkSet $ map (label $ trees!!curr) treeposs
	      axs = axiomsFor xs axioms
	  if null axs then labRed $ noAxiomsFor xs
	              else axioms := removeTerms axioms axs
	              	   simplRules := trips ["==","<==>"] axioms
	              	   transRules := trips ["->"] axioms
	                   labGreen $ axsRemovedFor xs

	removeClauses n = action
	  str <- ent.getValue
	  let (exps,_) = numberedExps
	  case parse intRanges str of
	       Just ns | all (< (length exps)) ns
	         -> let ts = map (exps!!) ns
		    case n of 0 -> axioms := removeTerms axioms ts
		                   simplRules := trips ["==","<==>"] axioms
	                           transRules := trips ["->"] axioms
		                   showAxioms True
		              1 -> theorems := removeTerms theorems ts
				   showTheorems True
			      2 -> conjects := removeTerms conjects ts
				   showConjects
			      _ -> terms := removeTerms terms ts; showTerms
	       _ -> labMag enterNumbers

	removeConjects = action conjects := []; indClauses := []
		                labGreen $ "There are neither conjectures nor " 
		                           ++ "induction hypotheses."

	removeCopies = action
	  if null trees then labBlue start
	  else let t = trees!!curr
	           p = emptyOrLast treeposs
	       if isHidden t || null p 
	          then labMag selectSub
	          else trees := updList trees curr $ removeAllCopies t p
	               extendPT False False False False RemoveCopies
	               setProof True False "REMOVING COPIES OF THE SUBTREE" [p]
		       	        copiesRemoved
		       clearTreeposs; drawCurr

	removeEdges b = action
	  if null trees then labBlue start
	  else trees := updList trees curr $ f $ trees!!curr
	       extendPT False False False False $ RemoveEdges b
	       setProof False False "REMOVING EDGES" [] $ edgesRemoved b
	       clearTreeposs; drawCurr
	  where f = if b then removeCyclePtrs else moreTree
	  
	removeKripke = action kripke := ([],[],[],[],[],[],[])
			      labGreen "The Kripke model has been removed."
	
	removeNode = action
	  if null trees then labBlue start
	  else let t = trees!!curr
	           p = emptyOrLast treeposs
	       if isHidden t || null p 
	          then labMag selectSub
	          else trees := updList trees curr $ removeNonRoot t p
	               extendPT False False False False RemoveNode
	               setProof False False "REMOVING THE NODE" [p]  
	               			   "The selected node has been removed."
		       clearTreeposs; drawCurr

	removeOthers = action
	  if null trees then labBlue start
	  else if treeMode == "tree" then labGreen "There is only one tree."
	       else solPositions := if curr `elem` solPositions then [0] else []
	            treeMode := "tree"; trees := [trees!!curr]
		    counter := upd counter 't' 1; curr := 0
	            extendPT False False False False RemoveOthers
	            setTreesFrame [] $ setProof (treeMode == "summand") False
	                          "REMOVING THE OTHER TREES" [] removedOthers

        removePath = action
	  if null trees || null treeposs then labMag selectSub
	  else let p = last treeposs
	       trees := updList trees curr $ removeTreePath p $ trees!!curr
	       extendPT False False False False RemovePath
	       setProof False False "REMOVING THE PATH" [p] 
	        "The selected subtree and its ancestor nodes have been removed."
	       clearTreeposs; drawCurr
	       
	removeSigMap = action signatureMap := (id,[]); labGreen iniSigMap

	removeSpec continue = action 
	  (specfiles,axioms,simplRules,transRules,theorems,conjects,terms) 
	         := ([],[],[],[],[],[],[])
	  kripke := ([],[],[],[],[],[],[])
	  symbols := iniSymbols; signatureMap := (id,[])
	  continue

	removeSubst = action setSubst (V,[]); labGreen emptySubst

	removeSubtrees = action
	  if null trees then labBlue start
	  else let t = trees!!curr
		   lg = length trees
	           ps = if null treeposs then [[]] else minis (<<=) treeposs
	       if ps == [[]] then 
	          if lg < 2 then labRed "There is at most one tree."
              	  else solPositions := shift curr solPositions
	               trees := context curr trees
		       counter := decr counter 't'
	               let b = treeMode == "summand" 
	               treeMode := if lg == 2 then "tree" else treeMode
	               curr := if curr < length trees then curr else 0
	               extendPT False False False False RemoveSubtrees 
	               setTreesFrame [] $ setProof b False 
       				          "REMOVING THE CURRENT TREE" [] 
       				          "The current tree has been removed."
	       else if any null ps then labRed $ noApp "Subtree removal"
		    else sig <- getSignature
	                 let qs = map init ps
		             q = head qs
		             u = getSubterm t q
			     r:rs = ps
			     b = last r == 0 && null rs && isImpl u && c || 
				 allEqual qs && (isDisjunct u && c ||
					         isConjunct u && not c)
			     c = polarity True t q
			     t' = lshiftPos (foldl expandInto t ps) ps
			 trees := updList trees curr $ if b && simplifying 
			 			       then simplifyIter sig t'
			     			       else t'
		         extendPT False False True True RemoveSubtrees
		         setProof b True "REMOVING SUBTREES" ps removed
		         clearTreeposs; drawCurr

	removeTheorems = action theorems := []
				labGreen "There are no theorems."

	renameVar = action if null trees then labBlue start
	                                 else str <- ent.getValue
	       				      renameVar' str

	renameVar' str = action
          sig <- getSignature
	  case parse (conjunct sig) str of
	       Just (F "&" ts) -> proceed ts
	       Just t -> proceed [t]
	       _ -> labMag "Enter a conjunction of equations between variables."
	  where proceed ts = action
	           case parseRenaming ts of
	           Just f 
                     -> let t = trees!!curr
	                    ps = emptyOrAll treeposs
			    ts = map (getSubterm t) ps
			    us = map (renameAll f) ts
		        trees := updList trees curr $ fold2 replace t ps us
		        extendPT False False False False $ RenameVar str
		        setProof False False "RENAMING VARIABLES" ps 
		        		     "Variables have been renamed."
	                clearTreeposs; drawCurr
	           _ -> labMag "Enter equations between variables."

	replaceNodes = action
	  if null trees then labBlue start
	  else str <- ent.getValue
	       case words str of [x] -> replaceNodes' x
			     	 _ -> labRed "Enter a single nonempty word."
	
	replaceNodes' x = action
	  sig <- getSignature
	  let t = trees!!curr
	      ps = emptyOrAll treeposs
	      ts = map (changeRoot . getSubterm t) ps
	      new = if sig.isFovar x then V x else F x []
	      changeRoot (V _)    = new
              changeRoot (F _ []) = new
              changeRoot (F _ ts) = F x ts 
	      changeRoot t        = t
          trees := updList trees curr $ fold2 replace0 t ps ts
	  extendPT False False False False $ ReplaceNodes x; drawCurr
	  setProof False False "REPLACING NODES" ps $ nodesReplaced x

	replaceOther = action 
	  tree <- solve.getTree
	  case tree of
	  Just t 
            -> let p = emptyOrLast treeposs
	       trees := updList trees curr $ replace1 (trees!!curr) p t
	       extendPT False False False False ReplaceOther
	       setProof False False "REPLACING A SUBTREE" [p] $ replaceIn other
	       clearTreeposs; drawCurr
	  _ -> labBlue $ startOther other

	replaceQuant u target continue = action
	  let t = trees!!curr
	      x = label t target
	  if x == root u then continue
	  else case isAny t x target of
	            Just q | polarity True t q -> replaceVar x u q
	            _ -> case isAll t x target of 
		              Just q | polarity False t q -> replaceVar x u q
			      _ -> continue

	replaceSubtrees = action
	  if null treeposs then labMag selectCorrectSubformula
	     else let t = trees!!curr
		      p:ps = case treeposs of [p] -> [[],p]; _ -> treeposs
	              u:us = map (getSubterm1 t) (p:ps)
	              ts = getOtherSides u p us ps
		  if just ts then replaceSubtrees' ps $ get ts
		             else labMag selectCorrectSubformula

	replaceSubtrees' ps ts = action
	  sig <- getSignature
	  extendPT False False True True $ ReplaceSubtrees ps ts
	  let t = fold2 replace1 (trees!!curr) ps ts
	  maybeSimplify sig t $ makeTrees sig $ setTreesFrame [] 
	  		      $ setProof True True "REPLACING THE SUBTREES" ps 
	  		                 replacedTerm

	replaceText = action if null trees then labBlue start
	                                   else str <- getTextHere
	                                        replaceText' str

 	replaceText' str = action
	  sig <- getSignature
	  let ps = emptyOrAll treeposs
	  case parseE (implication sig ++ term sig) str of
	       Correct u -> finish u ps
               p -> incorrect p str $ illformed "term or formula"
	  where finish u ps@(p:_) = action
	            case changeTerm (trees!!curr) u ps of
		    Wellformed t 
		      -> if null p then changeMode t
		          	   else trees := updList trees curr t
		         extendPT False False False False $ ReplaceText str
		         setProof False False "REPLACING THE SUBTREES" ps 
				  textInserted
		         clearTreeposs; drawCurr
		    Bad str -> labMag str

	replaceVar x u p = action
	  sig <- getSignature
          extendPT False False True True $ ReplaceVar x u p
	  let t = trees!!curr
	      F z [v] = getSubterm t p
	      quant:xs = words z
	      zs = xs `join` [x | x <- frees sig u `minus` frees sig t, 
	                	  nothing $ isAny t x p, 
				  nothing $ isAll t x p]
	      t1 = replace t p $ F (unwords $ quant:zs) [v>>>for u x]
	  maybeSimplify sig t1 $ makeTrees sig finish
	  where str = showTerm0 u
	        msg = "SUBSTITUTING " ++ str ++ " FOR " ++ x 
		finish = action setTreesFrame [] $ setProof True True msg [p]
					         $ subMsg str x

	resetCatch = action 
	  (node,penpos,subtree,suptree,isSubtree) := (Nothing,Nothing,Nothing,
	  					      Nothing,Nothing)
	  firstMove := True; canv.set [Cursor "left_ptr"]

	resetLevel = action counter := upd counter 'c' 0
	
	reverseSubtrees = action
	  if null trees then labBlue start
	  else if forsomeThereis (<<) treeposs treeposs
	          then labMag "Select non-enclosing subtrees!"
	       else let t = trees!!curr
		        ps = emptyOrAll treeposs
			x = label t $ init $ head ps
			b = allEqual (map init ps) && permutative x
	            case ps of [p] -> let u = getSubterm t p
	                                  ps = succsInd p $ subterms u
			              finish t ps $ permutative $ root u
	                       _ -> if any null ps
			               then labRed $ noApp "Subtree reversal"
	                               else finish t ps b
	  where finish t ps b = action
	  	 trees := updList trees curr $ fold2 exchange t (f ps) 
	  	 			     $ f $ reverse ps
		 extendPT False False False False ReverseSubtrees
		 setProof b False "REVERSING THE LIST OF (SUB)TREES" ps reversed
		 clearTreeposs; drawCurr
		 where f = take $ length ps`div`2

	rewritePar t sig k cls saveRedex used (p:ps) qs vc continue = action
          if p `notElem` positions t -- || isVarRoot sig redex
	     then rewritePar t sig k cls saveRedex used ps qs vc continue
          else let cls1 = filterClauses sig redex $ filter unconditional cls
	           cls2 = if saveRedex then map copyRedex cls1 else cls1
		   (cls3,vc') = renameApply sig t vc cls2
               if even matching then
                  case applyAxsToTerm cls1 cls3 axioms redex sig vc of
	          (Sum reds vc,used')
	            -> rewritePar (replace t p $ mkSum reds) sig k cls saveRedex
		                  (used `join` used') ps (p:qs) vc continue
   	          _ -> rewritePar t sig k cls saveRedex used ps qs vc' continue
               else case applyAxsToTermR cls1 cls3 axioms rand redex sig vc of
	            (Sum reds vc,used',rand')
	              -> rand := rand'
                         rewritePar (replace t p $ mkSum reds) sig k cls 
			  saveRedex (used `join` used') ps (p:qs) vc continue
   	            _ -> rewritePar t sig k cls saveRedex used ps qs vc' 
		                    continue
	  where redex = getSubterm t p
	rewritePar t sig k cls saveRedex used _ qs vc continue =
	  narrowPar t sig k cls saveRedex used [] qs vc continue

	runChecker b = action 
	  if b then sp <- ent.getValue
	            let newSpeed = parse pnat sp
	            if just newSpeed then speed := get newSpeed
	            if fst checkingP then paint.setButton 3 runOpts
	  deriveBut.set runOpts
	  runProof <- runner tk $ action checkForward
	  			         if fst checkingP then showPicts
	  checker := runProof; runProof.startRun speed
          where runOpts = [Text "stop run", Command stopRun]

	saveFile file str = 
	  tk.writeFile (userLib file) $ "-- " ++ file ++'\n':str

	saveGraph dir = action
	  if null trees then labBlue start
	  else if lg < 5 || suffix `notElem` words ".eps .png .gif" then
	          saveFile dir $ showTree fast $ joinTrees treeMode trees
	          labGreen $ saved "trees" dir
	       else let f n = action curr := n; treeSlider.setValue n
	       			     clearTreeposs; drawCurr
	                             delay $ saveGraphDH True canv dir dirPath n
	            case trees of []  -> labBlue start 
  	                          [_] -> file <- savePic suffix canv dirPath
  	                                 lab2.set [Text $ saved "tree" file]
	                          _   -> renewDir dirPath
	            	                 saver <- runner2 tk f $ length trees-1
	            	                 saver.startRun 500
	            	                 labGreen $ saved "trees" dirPath
	  where dirPath = pixpath dir
	        lg = length dir
                (prefix,suffix) = splitAt (lg-4) dir
                
	saveGraphD = action
  	  if null trees then labBlue start 
  	  else str <- ent.getValue
  	       case str of 'p':str | just n -> picNo := get n
  	                                       where n = parse nat str
  	                   _ -> done
	       saveGraphDP True canv picNo
          
	saveGraphDP b screen n = action
  	  if notnull picDir then saveGraphDH b screen picDir (pixpath picDir) n
  	  	  		 picNo := n+1

	saveGraphDH b screen dir dirPath n = action
  	  mkHtml tk screen dir dirPath n
  	  let pic = if b then "tree" else "graph in the painter"
	  lab2.set [Text $ saved pic $ mkFile dirPath n]

	saveProof = action
	  if null proof then labMag $ "The proof is empty" 
	  else file <- ent.getValue
	       let filePath = userLib file
	           pfile = filePath ++ "P"
                   tfile = filePath ++ "T"
	       write pfile $ '\n':showDeriv proof trees solPositions
	       write tfile $ show proofTerm
	       labGreen $ saved "proof" pfile ++ '\n':saved "proof term" tfile
          where write file str = tk.writeFile file $ "-- " ++ file ++ '\n':str

	saveSigMap file = action
	  saveFile file $ showSignatureMap signatureMap ""
	  labGreen $ saved "signature map" file

     	saveSpec file = action
	  saveFile file $ showSignature (minus6 symbols iniSymbols)
	      			     $ f "\naxioms:\n" showFactors axioms ++
				       f "\ntheorems:\n" showFactors theorems ++
				       f "\nconjects:\n" showFactors conjects ++
				       f "\nterms:\n" showSum terms
	  labGreen $ saved "specification" file
	  where f str g cls = if null cls then "" else str ++ g cls

	setAdmitted block = action 
	  str <- ent.getValue
	  setAdmitted' block $ if null trees || null treeposs then words str
	                       else map (label $ trees!!curr) treeposs
			   
        setAdmitted' block xs = action 
	  let msg = admitted block xs
	  constraints := (block,xs)
	  if null trees then labGreen msg
	  else extendPT False False False False $ SetAdmitted block xs
	       setProof True False "ADMITTED" [] msg 

	setCollapse = action 
	  collSimpls := not collSimpls
	  setStrat

	setCurr msg n = action 
	  if n /= curr then curr := n; treeSlider.setValue n
			    paint.setCurrInPaint n
			    extendPT False False False False $ SetCurr msg n
	                    setProof True False "MOVED" [] msg
	          	    clearTreeposs; drawCurr
		       else labColorToPaint magenta msg
	  
        setCurrInSolve n continue = action if n < length trees 
					      then setCurr newCurr n; continue

        setDeriveMode = action	
	  if checking then
	     checker.stopRun0; checking := False
             isNew <- paint.getNewCheck
	     if snd checkingP then paint.setNewCheck
	     	  	           paint.setButton 1 $ f "narrow/rewrite" narrow
	     	  	           paint.setButton 2 $ f "simplify" simplify
	     	  	           paint.setButton 3 [Text "", Command skip]
	     checkingP := (False,False); speed := 500
	     (matchTerm,refuteTerm,simplTerm,stratTerm) := ([],[],[],[])
	     set "derive"; quit.set [Text "quit", Command tk.quit]
	  else case (simplifying,refuting) of 
	            (False,False) -> simplifying := True
	            		     set "derive & simplify"
	     	    (True,False)  -> refuting := True
	     	       		     set "derive & simplify & refute"
	     	    (True,_)      -> simplifying := False; set "derive & refute"
	     	    _ 	          -> refuting := False; set "derive"
	  where set str = action deriveBut.set [Text str, Command setDeriveMode]
	        f str act = [Text str, Command $ paint.remote $ act showPicts]
	        
	setExtension i = action 
	  if i > 0 then if i > 100 then tedit.set [Height 0]
	                canv.set [Height i]
		   else tedit.set [Height $ -i]

	setFont fo = action let [family,style] = words fo
	     		        [_,size,_] = words font.fontName
	  		    font0 <- tk.font $ unwords [family,size,style]
	  		    font := font0
			    if notnull trees then drawCurr

	setFontSize size = action 
	  let [family,_,style] = words font.fontName
	      str = show size
	      lstr = show (size-3)
	  font0 <- tk.font $ unwords [family,str,style]
	  font := font0
	  tedit.set [Font $ "Courier "++str]
	  ent.set [Font $ "Courier "++str]
	  lab.set [Font $ "Helvetica "++lstr++" italic"]
	  lab2.set [Font $ "Helvetica "++lstr++" italic"]

	setForw opts = action forwBut.set opts

	setInterpreter eval = action
	  sig <- getSignature
	  picEval := eval
	  interpreterBut.set [Text eval]
	  paint.setEval eval spread
	  draw <- ent.getValue
	  drawFun := draw
	  labGreen $ newInterpreter eval drawFun

        setMaxHeap n = action maxHeap := n

        setNarrow chgMatch chgRandom = action 
	  sig <- getSignature 
	  let nar = formula || notnull treeposs &&
	  		       treeposs `subset` boolPositions sig (trees!!curr)
	      set str1 str2 = do matchBut.set  [Text str1]
	      	      		 randomBut.set [Text str2]
	      	      		 narrowBut.set [Text $ if nar then "narrow" 
	      	      		 			      else "rewrite"]
	  if chgMatch && nar then matching := (matching+2) `mod` 4
 	  if chgRandom then matching := if even matching then matching+1 
	  						 else matching-1
          case matching of 0 -> set "match" "all"
			   1 -> set "match" "random"
			   2 -> set "unify" "all"
			   _ -> set "unify" "random"

	setNewTrees ts typ = action 
	  trees := ts; counter := upd counter 't' $ length ts; treeMode := typ 
	  initialize [] "trees"; setTreesFrame [] done
	  
        setNoProcs = action		      
	  str <- ent.getValue
	  case parse pnat str of 
	       Just n -> noProcs := n
	                 kripke := ([],[],[],[],[],[],[])
	       		 changeSimpl "noProcs" $ mkConst n
	                 labGreen $ "The Kripke model has been removed.\n" ++
	                            "There are " ++ show n ++ " processes."
	                            
	       _ -> labRed badNoProcs
	       
	setPicDir b = action
	  dir <- ent.getValue
	  picDir := if b || null dir then "picDir" else dir
	  lab2.set [Text $ picDir ++ " is the current directory"]
	  mkDir $ pixpath picDir
	  picNo := 0

	setProof correct simplPossible msg ps labMsg = action
	  let oldProofElem = proof!!proofPtr
	      n = counter 'd'
	      msgAE = msg == "ADMITTED" || msg == "EQS"
	      msgSP = msg == "SPLIT"
	      msgMV = msg == "MOVED" || msg == "JOIN"
	      msgAD = take 4 msg == "ADDC"
	      t = trees!!curr
	      str = if msgAE then labMsg
	            else if msgSP 
	                 then labMsg ++ show (length trees) ++ ' ':treeMode ++ 
	                      "s." ++ showCurr fast t treeMode
		    else if msgMV then labMsg ++ showCurr fast t treeMode
		    else if msgAD then addAxsMsg msg
  		    else if take 4 msg == "ADDI" 
		         then showNew fast (length trees) t (addAxsMsg msg) n ps 
		         	      treeMode
		    else if newTrees 
		         then showNew fast (length trees) t msg n ps treeMode
	            else showPre fast t msg n ps treeMode
	      str0 = "\nThe axioms have been MATCHED against their redices."
	             `onlyif` matching < 2
	      str1 = ("\nThe reducts have been simplified " ++ 
	      	      stratWord simplStrat ++ ".") `onlyif` simplifying
              str2 str = "\nFailure "++ str ++" have been removed."	
		         `onlyif` refuting	 
	      str3 = if correct then case ruleString of 
	      			     "NARROWING" -> str0++str1++str2 "atoms"
			             "REWRITING" -> str1++str2 "terms"
			             _ -> str1 `onlyif` simplPossible
		     else "\nCAUTION: This step may be semantically incorrect!"
	      (msgP,msgL) = if null str3 then (str,labMsg)
		                         else (str++'\n':str3,labMsg++str3)
	      noChange = newTrees || msgAE || msgSP || msgMV || msgAD ||
	                 trees /= oldProofElem.trees ||
	                 notnull msgL && head msgL == ' '
	      msg1 = msgL ++ if noChange then ""
	                                 else "\nThe "++ formString formula ++
	                                      " has not been modified."
	      u = joinTrees treeMode trees
	      us = map (joinTrees treeMode . (.trees)) proof
	      pred =  search (== u) us 
	      cmsg = if noChange || nothing pred then ""
	             else "\nThe " ++ formString formula ++ 
	                  " coincides with no. " ++ show (get pred)
	      msg3 = msg1 ++ cmsg
	  if null ruleString || n > 0 then
	     proofPtr := proofPtr+1
	     let next = struct msg = msgP ++ cmsg; msgL = msg3
	                       treeMode = treeMode
	     		       trees = trees; treePoss = ps; curr = curr
	     		       perms = perms; varCounter = varCounter
	     		       newPreds = newPreds; solPositions = solPositions
			       substitution = substitution; safe = safe
			       constraints = constraints; joined = joined
	     proof := take proofPtr proof++[next]
	  newTrees := False; ruleString := ""
	  labColorToPaint green $ show proofPtr ++ ". " ++ msg3

	setQuit opts = action quit.set opts

	setSubst subst@(f,dom) = action
	  applyBut.set [if null dom then blueback else redback]
	  domMenu <- applyBut.menu []
	  mapM_ (mkButF domMenu applySubstTo) dom
	  substitution := subst

	setSubtrees = action					     -- not used
	  if null trees then labBlue start
	  else str <- ent.getValue
	       let f = natToLabel $ posTree [] $ trees!!curr
	       case parse intRanges str of
	            Just ns | all (just . f) ns
		      -> let qs = map get $ map f ns
		         setTreeposs $ Add $ qs `minus` treeposs
		         drawSubtrees
			 labGreen $ case qs of
			            [q] -> "The tree at position " ++ show q ++ 
				           " has been selected."
		                    _ -> "The trees at position " ++ 
			                 init (tail $ show qs `minus1` ' ') ++
					 " have been selected."
	            _ -> labMag "Enter tree positions in heap order!"
	       
	setStrat = action
	  let str = stratWord simplStrat
	  stratBut.set [Text $ if collSimpls then str++"C" else str]
          
	setTime = action time <- tk.timeOfDay; times := (time,snd times)

	setTreeposs step = action 
          oldTreeposs := treeposs
	  treeposs := case step of Add ps -> treeposs++ps
				   Remove p -> treeposs`minus1`p
				   Replace ps -> ps
          if not $ null treeposs 
             then extendPT False False False False $ Mark treeposs
	  setNarrow False False

	setTreesFrame ps continue = action
	  let lg = length trees
	      str = case treeMode of "tree" -> formString formula
				     _ -> show lg ++ ' ':treeMode ++ "s"
	  treeSlider.set [To (lg-1)]; treeSlider.setValue curr
	  paint.setCurrInPaint curr; termBut.set [Text str]
	  setTreeposs $ Replace ps
	  drawCurr; continue
	
	setZcounter n = action varCounter := upd varCounter "z" n

	shiftPattern = action
 	  if null trees then labBlue start
	  else let [p,q] = case treeposs of [r] -> [[],r]; _ -> treeposs
	  	   r = drop (length p) q
	           t = trees!!curr
	       if null treeposs || length treeposs > 2 || not (p << q)
	          then labMag "Select a (conditional) equation and a pattern!"
	       else sig <- getSignature
                    case makeLambda sig (getSubterm1 t p) r of
		    Just cl -> trees := updList trees curr $ replace1 t p cl
		    	       extendPT False False False False ShiftPattern
		               setProof True False "SHIFTING A PATTERN" [[]] 
			                "A pattern has been shifted."
		               clearTreeposs; drawCurr
		    _ -> labMag "The selected pattern cannot be shifted."

	shiftQuants = action
	  if null trees then labBlue start
	  else let t = trees!!curr
		   ps = treeposs
	       if null ps || any null ps then errorMsg
	       else let qs = map init ps
			ts = map (getSubterm1 t) ps
			qvars = map (\t -> alls t++anys t) ts
                    if allEqual qs && any notnull qvars then
                       sig <- getSignature
	               let q = head qs
		           u = getSubterm1 t q
		           x = root u
		       if isF u && x `elem` words "==> Not & |" then
			  let (as,es,v,vc) = moveUp sig varCounter x 
			      			    (subterms u) $ map last ps
			      v' = simplifyIter sig v
                          finish (replace1 t q $ mkAll as $ mkAny es v') ps vc
		       else errorMsg
 		    else errorMsg
          where finish t ps vc = action 
			    trees := updList trees curr t
			    varCounter := vc
			    extendPT False False False False ShiftQuants 
	            	    setProof True False "MOVING UP QUANTIFIERS" ps moved
       	                    clearTreeposs; drawCurr
		errorMsg = labRed $ noApp "Move up quantifiers"

	shiftSubs = action
	  if null trees then labBlue start
	                else if null treeposs || any null treeposs 
		                then labRed $ noApp "Shift subformulas"
	       	                else shiftSubs' treeposs 

	shiftSubs' ps = action
          sig <- getSignature
	  case shiftSubformulas sig (trees!!curr) ps of
	       Just t -> trees := updList trees curr t
	                 extendPT False False False False $ ShiftSubs ps
	                 setProof True False "SHIFTING SUBFORMULAS" ps shifted
       	                 clearTreeposs; drawCurr
	       _ -> labRed $ noApp "Shift subformulas"

	showAxioms b = action
	  if null axioms then labGreen "There are no axioms."
	  else if b then enterFormulas axioms
	                 labGreen $ see "axioms"
	            else solve.bigWin; solve.enterFormulas axioms

	showAxiomsFor = action
	  str <- ent.getValue
	  let xs = if null trees || null treeposs && notnull str then words str
	           else mkSet $ map (label $ trees!!curr) $ emptyOrAll treeposs
	      axs = axiomsFor xs axioms
	  if null axs then labRed $ noAxiomsFor xs
	  	      else enterFormulas axs
	       		   labGreen $ see $ "axioms for " ++ showStrList xs

	showChanged = action
          if proofPtr <= 0 then labMag "The proof is empty."
	  else restore := True
	       let proofElem = proof!!(proofPtr-1)
	           k = proofElem.curr
		   ts = proofElem.trees
	       if k == curr then let ps = changedPoss (ts!!k) (trees!!k)
	                         setTreeposs $ Replace ps; drawCurr
			    else labMag newCurr

	showConjects = action
	  if null conjects then labGreen "There are no conjectures."
	                   else enterFormulas conjects
	                        labGreen $ see "conjectures"

        showCoords = action
          if null trees then labBlue start
	                else restore := True
	                     drawThis (cTree (trees!!curr) $ get ctree) [] ""

	showCycleTargets = action
	  if null trees then labBlue start 
	  		else restore := True
               		     let t = trees!!curr
			     drawThis t (cycleTargets t) "green"
	       
	showGlb = action
          if null treeposs then labMag "Select subtrees!"
	  else restore := True
	       drawThis (trees!!curr) [glbPos treeposs] "green"

	showIndClauses = action
	  if null indClauses then labGreen "There are no induction hypotheses."
	  else enterFormulas indClauses
	       labGreen $ see "induction hypotheses"
	 
        showMatrix m = action
	  sig <- getSignature
	  let [sts,ats,labs] 
	           = map (map showTerm0) [sig.states,sig.atoms,sig.labels]
	      p:ps = emptyOrAll treeposs
	      t = getSubterm1 (trees!!curr) p
	      f = if null ps then id else drop $ length p
	      is = [i | [i,1] <- map f ps]
	  pict <- (matrix tk sizes0 spread t).runT
	  let u = case m of 0 -> Hidden $ BoolMat sts sts 
	  				$ deAssoc0 $ mkPairs sts sts sig.trans
		            1 -> Hidden $ BoolMat ats sts 
		            		$ deAssoc0 $ mkPairs ats sts sig.value
		            2 -> Hidden $ BoolMat sts ats 
		            		$ deAssoc0 $ mkPairs sts ats $ out sig
		            3 -> Hidden $ ListMat sts (labs' trips) $ trips
		       		 where trips = mkTriples sts labs sts sig.transL
		            4 -> Hidden $ ListMat ats (labs' trips) $ trips 
		       		 where trips = mkTriples ats labs sts sig.valueL
		            5 -> Hidden $ ListMat sts (labs' trips) $ trips  
		       		 where trips = mkTriples sts labs ats $ outL sig
		            _ -> case parseEqs t of
		                      Just eqs -> mat m $ eqsToGraph is eqs
		                      _ -> if just pict then t else mat m t 
          pict <- (matrix tk sizes0 spread u).runT
	  if m > 5 && null trees then labBlue start
	  else if nothing pict then labMag "The matrix is empty."
	       else sizes <- mkSizes font $ stringsInPict $ get pict
		    paint.setEval "" spread
		    pict <- (matrix tk sizes spread u).runT
	            paint.callPaint [get pict] [curr] False curr "white"
	  where labs' trips = mkSet [x | (_,x,_:_) <- trips]
	        mat 6 t = Hidden $ BoolMat dom1 dom2 ps
	  		  where ps = deAssoc0 $ graphToRel t
			        (dom1,dom2) = sortDoms ps
	        mat _ t = Hidden $ ListMat dom1 dom2 ts
	  		  where ts = graphToRel2 (evenNodes t) t
			        (dom1,dom2) = sortDoms $ map (pr1 *** pr2) ts
	
        showNumbers m = action
          if null trees then labBlue start		  
	  else restore := True
	       col <- ent.getValue
	       let t = trees!!curr
	       	   p = emptyOrLast treeposs
		   u = getSubterm1 t p
		   c = case parse color col of Just d -> d; _ -> black
		   (v,n) = order c label u
	       drawThis (replace1 t p v) [] ""
	  where label (RGB 0 0 0) n = show n
		label c n           = show c++'$':show n
		(order,str) = case m of 1 -> (levelTerm,"level numbers")
				        2 -> (preordTerm,"preorder positions")
					3 -> (heapTerm,"heap positions")
					_ -> (hillTerm,"hill positions")

	showPicts = action
	  if null trees then labBlue start
          else if checking then
                  checkingP := (True,snd checkingP)
                  isNew <- paint.getNewCheck
                  if isNew then paint.buildPaint True $
                       		 action paint.setButton 3 runOpts; showTreePicts
                       	   else showTreePicts
	       else if null treeposs then showTreePicts else showSubtreePicts
	  where runOpts = [Text "run proof", Command $ runChecker True]

        showPolarities = action
          if null trees then labBlue start
	  else restore := True
	       let t = trees!!curr
		   ps = polTree True [] t
	       drawThis (colorWith2 "green" "red" ps t) [] ""

        showPositions = action
	  if null trees then labBlue start
	  else restore := True
	       let t = trees!!curr
	       drawThis (mapT f $ posTree [] t) (cPositions isPos t) "red"
	  where f = unwords . map show

	showPreds = action
	  if null trees then labBlue start
	  else restore := True
	       let t = trees!!curr
	           ps = concatMap (nodePreds t) $ emptyOrAll treeposs
               drawThis t ps "green"

	showProof b = action
	  if null proof then labMag "The proof is empty."
	  else let str = '\n':showDeriv proof trees solPositions
               if b then enterText str; labGreen $ see "proof"
	            else solve.bigWin; solve.enterText str

	showProofTerm = action
	  if null proofTerm then labMag "The proof term is empty."
	                    else labGreen $ see "proof term"; enterRef

	showRelation m = action
	  sig <- getSignature
	  let [sts,ats,labs] 
	          = map (map showTerm0) [sig.states,sig.atoms,sig.labels]
	      t = trees!!curr
	      p:ps = emptyOrAll treeposs
	      u = getSubterm1 t p
	      f = if null ps then id else drop $ length p
	      is = [i | [i,1] <- map f ps]
	  case m of 0 -> act1 $ mkRelConstsI sts sts sig.trans 
	 	    1 -> act1 $ mkRelConstsI ats sts sig.value
	            2 -> act1 $ mkRelConstsI sts ats $ out sig
		    3 -> act1 $ mkRel2ConstsI sts labs sts sig.transL
	            4 -> act1 $ mkRel2ConstsI ats labs sts sig.valueL
		    5 -> act1 $ mkRel2ConstsI sts labs ats $ outL sig
	            _ -> if null trees then labBlue start
		         else act2 t p $ case parseEqs u of
			                      Just eqs -> eqsToGraph is eqs
					      _ -> u
	  where act1 ts = enterTree False $ h ts
	        act2 t p u = action
	             trees := updList trees curr $ replace1 t p $ h $ f m u
	             clearTreeposs; drawCurr
		     where f 6 = mkRelConsts . graphToRel
                   	   f _ = mkRel2Consts . graphToRel2 (evenNodes u)
		g t@(F "()" ts) us = case last ts of F "[]" [] -> us
						     _ -> t:us
		h = mkList . foldr g []

	showSig = action 
	  enterText $ showSignature (minus6 symbols iniSymbols) ""
	  let (block,xs) = constraints
	  labGreen $ see "signature" ++ '\n':admitted block xs 
	  			     ++ '\n':equationRemoval safe

	showSigMap = action 
	  if null $ snd signatureMap then labGreen iniSigMap
          else enterText $ showSignatureMap signatureMap ""
	       labGreen $ see "signature map"

	showSolutions = action 
	  sig <- getSignature
	  labGreen $ solved $ if formula then solPoss sig trees else []

	showSubst mode = action
	  let (f,dom) = substitution
	      ts = substToEqs f dom
	      typ = if length ts == 1 then "tree" else "factor"
	  if null dom then labGreen emptySubst
          else case mode of 0 -> enterFormulas ts; labGreen $ see "substitution"
          	            1 -> solve.bigWin; solve.enterFormulas ts
          	            _ -> solve.bigWin; solve.setNewTrees ts typ

	showSubtreePicts = action			
	  sig <- getSignature
	  eval <- getInterpreter			
	  str <- ent.getValue
	  let t = trees!!curr
	      ts = applyDrawFun sig drawFun str $ map (closedSub t) treeposs
	      picts = map (eval sizes0 spread) ts
	  picts <- mapM (.runT) picts			
	  sizes <- mkSizes font $ concatMap (stringsInPict . getJust) picts
	  setTime
	  back <- ent.getValue
	  let picts = map (eval sizes spread) ts
	  picts <- mapM (.runT) picts			
	  paint.callPaint [concatMap getJust picts] [] True curr back

	showSucs = action
	  if null trees then labBlue start
	  else restore := True
               let t = trees!!curr
               drawThis t (concatMap (nodeSucs t) $ emptyOrAll treeposs) "green"

	showSyms f = action
	  if null trees then labBlue start
	  else restore := True
               sig <- getSignature
	       let t = trees!!curr
	           p = emptyOrLast treeposs
	       drawThis t (map (p++) $ f sig $ getSubterm t p) "green"

	showTerms = action if null terms 
			      then labGreen "There are no terms to be reduced."
	                   else enterTerms terms
	                        labGreen $ see "terms"

	showTheorems b = action
	  if null theorems then labGreen "There are no theorems."
	  else if b then enterFormulas theorems
			 labGreen $ see "theorems"
		    else solve.bigWin; solve.enterFormulas theorems

	showTheoremsFor = action
	  str <- ent.getValue
	  let xs = if null trees || null treeposs then words str
	           else map (label $ trees!!curr) treeposs
	      cls = [thm | thm <- theorems, any (`isin` thm) xs]
	  if null cls then labRed $ noTheoremsFor xs
	  	      else enterFormulas cls
	  	           labGreen $ see $ "theorems for " ++ showStrList xs

	showTransOrKripke m = action
	  sig <- getSignature
	  str <- ent.getValue
	  let [sts,ats,labs] = map (map showTerm0) 
	  			   [sig.states,sig.atoms,sig.labels]
	      vcz = varCounter "z"
              (eqs,zn)    = relToEqs vcz $ mkPairs sts sts sig.trans
              (eqsL,zn')  = relLToEqs zn $ mkTriples sts labs sts sig.transL
	      trGraph     = eqsToGraph [] eqs
	      trGraphL    = eqsToGraph [] eqsL
	      atGraph     = if all null (sig.trans) then emptyGraph 
	      	            else outGraph sts ats (out sig) trGraph
	      atGraphL    = if null (sig.labels) then emptyGraph
	      	            else outGraphL sts labs ats (out sig) (outL sig) 
	      	         				          trGraphL
              inPainter t = action
		           let u = head $ applyDrawFun sig drawFun str [t]
		           pict <- (widgetTree sig tk sizes0 spread u).runT
		           if nothing pict then labMag "The tree is empty."
		           else sizes <- mkSizes font $ stringsInPict $ get pict
		                paint.setEval "tree" spread
		                pict <- (widgetTree sig tk sizes spread u).runT
		                paint.callPaint [get pict] [curr] False curr 
		       				                        "white"
	  setZcounter zn'
	  case m of 0  -> enterTree False trGraph
	       	    1  -> enterTree False $ colorClasses sig trGraph
	       	    2  -> inPainter trGraph
	       	    3  -> solve.bigWin; solve.enterTree False trGraph
	       	    4  -> enterTree False trGraphL
	       	    5  -> enterTree False $ colorClasses sig trGraphL
	       	    6  -> inPainter trGraphL
	       	    7  -> solve.bigWin; solve.enterTree False trGraphL
	       	    8  -> enterTree False atGraph
	       	    9  -> inPainter atGraph
	       	    10 -> solve.bigWin; solve.enterTree False atGraph
	       	    11 -> enterTree False atGraphL
	       	    12 -> inPainter atGraphL
	       	    _  -> solve.bigWin; solve.enterTree False atGraphL

	showTreePicts = action				-- without transformer:
	  sig  <- getSignature
	  eval <- getInterpreter			-- getInterpreter
	  str  <- ent.getValue
	  let ts = applyDrawFun sig drawFun str trees
	      picts = map (eval sizes0 spread) ts
	  picts <- mapM (.runT) picts			-- done
	  sizes <- mkSizes font $ concatMap (stringsInPict . getJust) picts
	  setTime
	  back <- ent.getValue
	  let picts = map (eval sizes spread) ts
	  picts <- mapM (.runT) picts			-- done
	  paint.callPaint (map getJust picts) (indices_ ts) False curr back

	showVals = action
	  if null trees then labBlue start
	  else restore := True
	       let t = trees!!curr
               drawThis t (valPositions t) "green"

	simplify continue = action
	  if null trees then labBlue start
	  else str <- ent.getValue
	       let act limit sub = simplify' limit sub continue
	       case parse pnat str of Just limit -> act limit True
		                      _ -> act 100 False

	simplify' limit sub continue = action 
	  ruleString := "SIMPLIFYING " ++ stratWord simplStrat
	  extendPT False False False True $ Simplify limit sub
	  sig <- getSignature
	  let t = trees!!curr
	  if null treeposs then 
	     setTime
	     let (u,n,cyclic) = simplifyLoop sig limit simplStrat t
	         v = if collSimpls then collapse False u else u
	         msg0 = "The " ++ (if treeMode == "tree" then formString formula
		                   else "previous " ++ treeMode) 
		               ++ " is simplified."
	         msg = finishedSimpl n ++ solved solPositions ++
	             ("\nThe simplification became cyclically." `onlyif` cyclic)
	     if n == 0 then counter := decr counter 't'
	                    if counter 't' > 0 
			       then setCurr msg0 $ (curr+1) `mod` length trees
			    	    continue
                               else labMag treesSimplified
			            paint.labSolver treesSimplified
	     else trees := updList trees curr v
	          makeTrees sig $ action counter := upd counter 'd' n
	                                 setProof True False ruleString [] msg
			                 setTreesFrame [] continue
	  else if sub then simplifySubtree t sig limit simplStrat continue
                      else simplifyPar t sig treeposs [] continue

	simplifyPar t sig (p:ps) qs continue = action
 	  case simplifyOne sig t p of
	       Just t -> simplifyPar t sig ps (p:qs) continue
	       _ -> simplifyPar t sig ps qs continue
	simplifyPar _ _ [] [] _ = labColorToPaint magenta
	            "The selected trees are simplified at their root positions."
	simplifyPar t _ _ qs continue = action
          trees := updList trees curr t
	  counter := upd counter 'd' 1
	  setProof True False "SIMPLIFYING THE SUBTREES" qs simplifiedPar
	  clearTreeposs; drawCurr
          continue
          
        -- used by simplify' 

	simplifySubtree t sig limit strat continue = action
 	  let p = emptyOrLast treeposs
	      (u,n,cyclic) = simplifyLoop sig limit strat $ getSubterm t p
	      v = if collSimpls then collapse False u else u
	      msg = appliedToSub "simplification" n ++
	            ("\nThe simplification became cyclical." `onlyif` cyclic)
	  if n == 0 then 
	     labColorToPaint magenta "The tree selected at last is simplified."
	     continue
          else trees := updList trees curr $ replace1 t p v
               counter := upd counter 'd' n
               setProof True False ruleString [p] msg
	       clearTreeposs; drawCurr
 	       continue
 	       
        -- used by simplify' 

	skip = action done
	
	specialize = action
          if null trees then labBlue start
	  else str <- ent.getValue
	       let (exps,b) = numberedExps
	       case parse nat str of
	            k@(Just n) | n < length exps
	              -> if b then finish k $ exps!!n
		              else labMag $ enterTfield "formulas"
	            _ -> str <- getTextHere
 	                 sig <- getSignature
	                 case parseE (implication sig) str of
	                      Correct th -> finish Nothing th
	                      p -> incorrect p str $ illformed "formula"
	  where finish k th = applyTheorem False k $ 
	  		           case th of F "Not" [th] -> mkHorn mkFalse th
			                      _ -> mkCoHorn mkTrue th

	splitTree = action
	  if notnull trees then 
	     sig <- getSignature
	     extendPT False False False False SplitTree
	     if joined then joined := False; splitBut.set [Text "join"]
                            makeTrees sig $ setTreesFrame [] 
		   		          $ setProof True False "SPLIT" []
					  $ "The tree has been split."
	     else joined := True; splitBut.set [Text "split"]
	          let t = joinTrees treeMode trees
	          treeMode := "tree"; trees := [t]; curr := 0
	          counter := upd counter 't' 1
		  solPositions := if formula && isSol sig t then [0] else [] 
	          setTreesFrame [] $ setProof True False "JOIN" [] 
		  		   $ "The trees have been joined."

	startInd ind = action
          if null trees then labBlue start
          else str <- ent.getValue
               let pars = words str 
               case pars of
               	  ["ext",limit] | just k -> f $ get k where k = parse pnat limit
              	  _ -> f 0
          where f = if ind then applyInduction else applyCoinduction
 
	stateEquiv = action 
	  sig <- getSignature
	  let f (i,j) = mkTup [sig.states!!i,sig.states!!j]
	  enterTree False $ mkList $ map f $ bisim sig 
	  
        stopRun = action 
	  if checking then checker.stopRun0; deriveBut.set runOpts
	  if fst checkingP then paint.setButton 3 runOpts
	  where runOpts = [Text "run proof", Command $ runChecker True]

	stretch prem = action
	  if null trees then labBlue start
	  else let t = trees!!curr
		   p = emptyOrLast treeposs
		   u = getSubterm t p
		   (f,step,str) = 
		             if prem then (stretchPrem,StretchPremise,"PREMISE")
			     else (stretchConc,StretchConclusion,"CONCLUSION")
	       case preStretch prem (const True) u of
	       Just (_,varps)
	         -> let (v,n) = f (varCounter "z") varps u
	  	    trees := updList trees curr $ replace t p v
	            setZcounter n
	            extendPT False False False False step
	            setProof True False ("STRETCHING THE "++str) [p] stretched
	            clearTreeposs; drawCurr
	       _ -> notStretchable $ "The "++str

	subsumeSubtrees = action
	  if length treeposs /= 2 || any null treeposs
	     then labMag "Select two proper subtrees!"
	  else let t = trees!!curr
		   ps@[p,q] = treeposs
		   prem = getSubterm t p
		   conc = getSubterm t q
		   r = init p
		   s = init q
		   u = getSubterm t r
	       sig <- getSignature
	       if subsumes sig prem conc then
	          if r == s then
	             if isImpl u then
	                trees := updList trees curr $ replace t r mkTrue
		        finish ps "premise"
		     else if isConjunct u then
	                     let u' = F "&" $ context (last q) $ subterms u
		             trees := updList trees curr $ replace1 t r u'
		             finish ps "factor"
	                  else if isDisjunct u then
	                          let u' = F "|" $ context (last p) $ subterms u
		                  trees := updList trees curr $ replace t r u'
		                  finish ps "summand"
		               else labGreen msg
		  else labGreen msg
	       else labRed "The selected trees are not subsumable."
	  where msg = "The selected trees are subsumable."
	        finish ps str = action 
		             extendPT False False False False SubsumeSubtrees
		    	     setProof True False "SUBSUMPTION" ps $ subsumed str
			     clearTreeposs; drawCurr

        switchFast = action 
          fast := not fast
	  fastBut.set [Text $ if fast then "indented text" 
	  			      else "continuous text"]

        switchSafe = action 
	  safe := not safe
	  extendPT False False False False SafeEqs
	  setProof True False "EQS" [] $ equationRemoval safe
	  safeBut.set [CLabel $ eqsButMsg safe]

	transformGraph mode = action
	  if null trees then labBlue start
	  else sig <- getSignature
	       let t = trees!!curr
	      	   vcz = varCounter "z"
	      	   relToEqs1 = relToEqs vcz . deAssoc1
	      	   relToEqs3 = relLToEqs vcz . deAssoc3
	      	   p:ps = emptyOrAll treeposs
	      	   u = getSubterm1 t p
	      	   (q,f,r) = if null ps then ([],id,p) 
			     	        else (p,drop $ length p,head ps)
                   (eqs,zn) = graphToEqs vcz (getSubterm1 t q) $ f r
                   is = [i | [i,1] <- map f ps]
                   x = label t r
		   act zn p u = action 
		   	    trees := updList trees curr $ replace1 t p u
			    if mode == 3 then setZcounter zn
		            extendPT False False False False $ Transform mode
		            setProof False False "TRANSFORMING THE GRAPH" [p] $ 
		            	     transformed
			    clearTreeposs; drawCurr
 	       case mode of 0 -> act 0 p $ collapse True u
			    1 -> act 0 p $ collapse False u
			    2 -> case parseColl parseConsts2 u of
			         Just rel -- from pairs to a graph
			           -> let (eqs,zn) = relToEqs1 rel
			              act zn p $ eqsToGraphx x eqs
	                         _ -> case parseColl parseConsts3 u of
			              Just rel -- from triples to a graph
				        -> let (eqs,zn) = relToEqs3 rel
				           act zn p $ eqsToGraphx x eqs
	                              _ -> case parseEqs u of
				           Just eqs -- from equations to a graph
					     -> act vcz p $ eqsToGraph is eqs
				           _ -> -- from a graph to a graph
					     act vcz q $ eqsToGraphx x eqs  
			    _ -> case parseColl parseConsts2 u of
	                         Just rel -- from pairs to equations
			           -> let (eqs,zn) = relToEqs1 rel
			              act zn p $ eqsTerm eqs
	                         _ -> case parseColl parseConsts3 u of
	                              Just rel -- from triples to equations
	                                -> let (eqs,zn) = relToEqs3 rel
				           act zn p $ eqsTerm eqs
		                      _ -> -- from a graph to equations
					   act vcz p $ eqsTerm eqs
			    
	unifyAct u u' t t' p q = action
	  restore := True
	  sig <- getSignature
	  case unify u u' t t' p q V sig [] of
	       Def (f,True)
	         -> let xs = frees sig u ++ frees sig u'
	       		dom = domSub xs f
		    if null dom then labGreen $ unifiedT ++ emptySubst
                    else if any hasPos $ map f dom then labRed posInSubst
     	                 else setSubst (f,dom)
			      labGreen $ unifiedT ++ "See substitution > show."
	       Def (_,False) -> labRed partialUnifier
	       BadOrder -> labRed noUnifier
	       Circle p q -> labRed $ circle p q
	       NoPos p -> setTreeposs $ Replace [p]; drawCurr; labRed dangling
	       NoUni -> labRed noUnifier
	       OcFailed x -> labRed $ ocFailed x

	unifyOther = action 
	  tree <- solve.getTree
	  case tree of Just t -> let p = emptyOrLast treeposs
	                             t' = trees!!curr
	                             u = getSubterm t' p
		                 unifyAct t u t t' [] p
	               _ -> labBlue $ startOther other

	unifySubtrees = action
	  if length treeposs /= 2 || any null treeposs
	     then labMag "Select two proper subtrees!"
	  else let t = trees!!curr
		   ps@[p,q] = treeposs
		   u = getSubterm1 t p
		   u' = getSubterm1 t q
		   r = init p
		   v = getSubterm1 t r
		   b = polarity True t r
	       if r == init q then
                  sig <- getSignature
	          if isConjunct v && b then
		     let xs = if null r then []
		     			else anys $ getSubterm1 t $ init r
		     case unifyS sig xs u u' of
		     Just f -> let us = map (>>>f) $ init $ subterms v
		                   t' = replace1 t r $ mkConjunct us
			       trees := updList trees curr t'
		               extendPT False False False False UnifySubtrees
			       setProof True False "FACTOR UNIFICATION" ps $
			                unified "factor"
			       clearTreeposs; drawCurr
		     _ -> labRed noUnifier
	          else if isDisjunct v && not b then
		          let xs = if null r then []
		                             else alls $ getSubterm1 t $ init r
	                  case unifyS sig xs u u' of
		  	  Just f 
		  	    -> let us = map (>>>f) $ init $ subterms v
		                   t' = replace1 t r $ mkDisjunct us
			       trees := updList trees curr t'
			       extendPT False False False False UnifySubtrees
			       setProof True False "SUMMAND UNIFICATION" ps $ 
			       	        unified "summand"
			       clearTreeposs; drawCurr
		          _ -> labRed noUnifier
		       else labRed $ noApp "Subtree unification"
	       else labMag "Select subtrees with the same predecessor!"

      in struct ..Solver
          	       
-- ENUMERATOR messages

 badConstraint = "The constraint is not well-formed."

 howMany 1 object ""     = "There is one " ++ object ++ "."
 howMany 1 object constr = "There is one " ++ object ++ " satisfying " ++ constr
 			   ++ "."
 howMany n object ""     = "There are " ++ show n ++ " " ++ object ++ "s."
 howMany n object constr = "There are " ++ show n ++ " " ++ object ++
   		           "s satisfying " ++ constr ++ "."

 none object "" = "There is no " ++ object ++ "."
 none object c  = "There is no " ++ object ++ " satisfying " ++ c ++ "."

 startEnum object = (init $ enterTfield str) ++ 
                    (case object of "palindrome" -> "!"
                    		    _ -> more)
       where str = case object of
                   "palindrome" -> "a sequence of strings"
		   "alignment" -> "two sequences of strings separated by blanks"
                   "dissection"
	             -> "the breadth > 0 and the height > 0 of a rectangle"
                   _ -> "the length of a list"
	     more = "\nand a constraint into the entry field (see the manual)!"
	     
-- the ENUMERATOR template

 struct Enumerator = buildEnum :: String -> (String -> String -> Bool) -> Action

 enumerator :: TkEnv -> Solver -> Template Enumerator

 enumerator tk solve =

   template object := ""; compl := const2 False

   in let

        buildEnum obj f = action
	  object := obj
	  compl := f
	  solve.labBlue $ startEnum obj
  	  solve.setForw [Text "go", Command $ getInput obj, redback]
	  solve.setQuit [Text $ "quit "++obj, Command finish]

	finish = action
	  solve.labBlue start
	  solve.setForw [Text "--->", Command solve.forwProof, greenback]
	  solve.setQuit [Text "quit", Command tk.quit]
	  solve.setInterpreter "tree"

	getInput "alignment" = action
	  str <- solve.getText; constr <- solve.getEntry
	  let global = notnull constr && head constr == 'g'
	      (xs,ys) = break (== '\n') str
	  if null ys then
	     solve.labRed $ enterTfield "two sequences of strings"
	  else showResult constr $ map (alignToTerm . compress) 
	                         $ mkAlign global (words xs) (words ys) compl
	                         
	getInput "palindrome" = action 
          str <- solve.getText; showResult "" $ map (alignToTerm . compress) 
	  				      $ mkPali (words str) compl
	  				      
	getInput "dissection" = action
	  str <- solve.getText
	  case parse size str of
	  Just (b,h)
	    -> constr <- solve.getEntry
	       case parse (disjunct sig) constr of
	       Just t -> case dissConstr b h t of
		         Just (c,ns,c') 
			   -> showResult (showEnum t) $ mkDissects c c' ns b h
	                 _ -> solve.labRed badConstraint
	       _ -> solve.labRed badConstraint
	  _ -> solve.labBlue $ enterTfield "two numbers > 0"
	  where size = do b <- token pnat; h <- token pnat; return (b,h)
	  
	getInput _ = action
	  str <- solve.getText
          case parse (token pnat) str of
	  Just n | n > 1
	    -> constr <- solve.getEntry
	       case parse (disjunct sig) constr of
	       Just t -> case partConstr t of
		         Just c -> showResult (showEnum t) $ mkPartitions c n t
	                 _ -> solve.labRed badConstraint
	       _ -> solve.labRed badConstraint
	  _ -> solve.labBlue $ enterTfield "a number > 1"

	showResult constr terms = action
	  if null terms then solve.labGreen $ none object constr
	                else let n = length terms
	                         typ = if n == 1 then "tree" else "term"
			     solve.setNewTrees terms typ
	            	     solve.labGreen $ howMany n object constr

      in struct ..Enumerator
      
   where sig = predSig $ words "alter area bal brick eqarea eqout factor hei" ++
		         words "hori levmin levmax sizes sym out vert"
      
         alignToTerm :: Align_ String -> TermS
         alignToTerm t = case t of 
	 		 Compl x y t -> F "compl" [V x,alignToTerm t,V y]
			 Equal_ s t -> F "equal" $ alignToTerm t:map V s
			 Ins s t -> F "insert" $ alignToTerm t:map V s
			 Del s t -> F "delete" $ alignToTerm t:map V s
			 End s -> F "end" $ map V s
		      
         showEnum t = showSummands (mkSummands t) `minus`" \n"

