<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<html> <head>
<title>The O'Haskell interface to Tk -- an overview</title>
</head>
<body bgcolor="abcdef">

<h1 align=center>The O'Haskell interface to Tk -- an overview</h1>

<div align=center>
<a href=#counter>The Counter Example</a><br>
<a href=#widgets>Widgets</a><br>
<a href=#config>Configurable widgets</a><br>
<a href=#events>Events</a><br>
<a href=#options>Options</a><br>
<a href=#wwidgets>Window and canvas widgets</a><br>
<a href=#tkenv>The Tk environment</a><br>
<a href=#layout>Layout</a><br>
</div>
<a name=counter><h4>The Counter Example</h4></a>
As a first example -- the "Hello world" example of GUI programming in
O'Haskell --  we show here the complete code of a simple program that
maintains an integer counter. When started, the program opens the
following window on the screen:
<p align=center>
<img src="counter1.gif">
<p>
The current counter value is displayed in the upper half of the window,
while the lower half holds holds two buttons, one to quit the
application and one to increase the counter value by one.
In addition, the image above shows the window manager-specific
decorations on top of the application window.
Here is the code:
<pre>
module Counter where

import Tk

struct Counter =
  incr  :: Action
  read :: Request Int

counter :: Template Counter
counter = template
            val := 0
          in struct
              incr = action val := val+1
              read = request return val

main tk = do  
  win <- tk.window [Title "Counter Example"]
  cnt <- counter
  lab <- win.label [Background white, Font "helvetica 16", 
                     Text "0", Anchor E]
  let incr = do
      cnt.incr
      v <- cnt.read
      lab.set [Text (show v)]
  qBut <- win.button [Text "Quit", Command tk.quit]
  iBut <- win.button [Text "Incr", Command incr]
  pack (lab ^^^ (qBut <<< iBut))
</pre>
The program makes use of a record type <tt>Counter</tt> and a template for
creating counter objects, which will not be discussed here.
<p>
The <tt>main</tt> procedure takes
the Tk environment as argument. The type of <tt>main</tt> is (inferred
to be) <tt> TkEnv -> Cmd ()</tt>; executing a procedure of this type
causes ohugs to start a Tk interpreter and set up the communication
mentioned in the introduction.
<p>
<tt>main</tt> delegates to the environment <tt>tk</tt> to create
the desired widgets: an application
window <tt>win</tt> and, in this window, a label (a widget that can
display (non-editable) text) and two buttons. On creation, each of the
widgets is configured by passing it a list of <em>options</em>.
The label is configured with background color, font and initial text,
and anchor information (E means East, i.e. justify text
to the right). Each of the
buttons is passed an associated <tt>Command</tt> to be executed when
the user clicks the mouse while the pointer is within the button.
For the Incr button, this command sequence is defined in a locally
declared brocedure.
In the final line of code, the three widgets are packed
using the layout combinators <tt>^^^</tt> (vertical
composition) and <tt><<<</tt> (horizontal composition). Packing also
causes the window to be mapped onto the screen. <tt>main</tt> then
terminates and the application 
silently waits for, and reacts to, user actions. 

<a name=widgets><h4>Widgets</h4></a>
The central notion in the Tk interface is that of a <em>widget</em>. 
A widget is an object of (some subtype of) the record type
<pre>
struct Widget =
    ident   :: String
    destroy :: Action
    exists  :: Request Bool 
    focus, raise, lower :: Action
    bind    :: [Event] -> Action
</pre>
Widgets are objects that have a unique identity (created by the
system), that can be created and destroyed, that can take keyboard
focus, that can be raised and lowered in the stacking order on screen
and that, most importantly, can <em>bind</em> actions to events.
For example, consider adding the following line to the <tt>main</tt>
procedure in the counter example:
<pre>
lab.bind [ButtonPress 1 (\_ -> lab.set [Background red])]
</pre>
This gives the application an added behavior: if the user presses
mouse button 1 when
the mouse pointer is within the label <tt>lab</tt>, the background
color of the label changes to red. To understand the details
of this, we need to understand the notion of configurable widgets
(which support the method <tt>set</tt>) and the datatype of events
(which has <tt>ButtonPress</tt> as one of its constructors).

<p>
As we saw in the Counter example, buttons have as one option
a <tt>Command</tt> to be executed when the user clicks mouse button
1. This binding could have been specified using the method
<tt>bind</tt>; the option is just a convenience for a common and
simple use of buttons. Buttons also have other default behaviors;
they are highlighted when the mouse pointer passes over them, they
change appearance when the mouse button is pressed over them etc.

<a name=config><h4>Configurable widgets</h4></a>
All widgets are configurable, i.e. their options can be set (or
changed) not only at creation, but also later by calling their
<tt>set</tt> method. This is expressed by the declarations
<pre>
struct Configurable a = 
    set     :: [a] -> Action
--  get     :: a -> Request String

struct ConfWidget a < Widget, Configurable a
</pre>
The <tt>ConfWidget</tt> type is parameterized with the type of options
it supports; this varies between widget types. While some options
are general (i.e. <tt>Background</tt>), others are particular to
just one widget type (i.e. <tt>Accelerator</tt>, which only applies
to menu entries).
<p> In the present version, the <tt>get</tt> method is not supported,
since its type as given above is not very satisfactory; a better
solution is envisaged when locally quantified types are added to
O'Haskell.
<p>
One reason not to include the <tt>set</tt> method in the type
<tt>Widget</tt> (and hence to dispose of <tt>Configurable</tt>) is
that also some objects that are not widgets are configurable (e.g.
images and menu entries).

<a name=events><h4>Events</h4></a>
A subset of the hierarchy of datatypes of events presently supported is
<pre>
type Pos = (Int, Int)

data ButtonPress = ButtonPress Int (Pos -> Cmd ())
                 | AnyButtonPress  (Int -> Pos -> Cmd ())

data MouseEvent > ButtonPress = 
           ButtonRelease Int (Pos -> Cmd ()) 
         | AnyButtonRelease (Int -> Pos -> Cmd ())
         | Motion Int (Pos -> Cmd ())
         | AnyMotion  (Pos -> Cmd ())
         | Double ButtonPress
         | Triple ButtonPress

data Event > MouseEvent, KeyEvent, WindowEvent 
</pre>
Each constructor takes as its last argument a <em>callback
procedure</em>, which describes the commands to be executed when the
user performs the action indicated by the constructor name.
This procedure may have parameters, as for example for
<tt>ButtonPress</tt>, where the callback procedure is before execution
applied to the position (in pixel coordinates) within the widget 
of the mouse pointer. For <tt>AnyButtonPress</tt>, also the button number
is given as argument to the call-back procedure.
<p>
In the extension to
the counter example above, we did not care where in the label the button was
clicked; anyhow we had to provide a function that ignores its
argument as callback procedure.

<a name=options><h4>Options</h4></a><br>
The options are also represented as a hierarchy of data types, with a
bottom layer of some thirty single-constructor types, including the follwing:
<pre>
data Background  = Background Color
data BorderWidth = BorderWidth Int
data Command     = Command (Cmd ())
data File        = File FilePath
data Fill        = Fill Color
data Font        = Font String
</pre>
Three of these we already used in the counter example; the other three
are used to specify width of the widget border in pixels, 
a file path from which to read data (an option for images) and a fill
color for geometrical figures in canvases (see
below under window widgets).
<p>
These single-constructor types are the base types in the formation of
supertypes in two ways:
<ul>
  <li> grouping of related options as in
<pre>
data BasicOpt       > Background, BorderWidth, Cursor, Relief
data BasicWOpt      > BasicOpt, Width
data DimOpt         > Height, Width
data StdOpt         > BasicWOpt, DimOpt
data FontOpt        > Font, Foreground, Anchor, Justify
</pre>
<li> specifying widget-specific option types such as
<pre>
data CanvasOpt      > StdOpt, ScrollRegion 
type FrameOpt       = StdOpt
</pre>
</ul>
<a name=wwidgets><h4>Window widgets</h4></a><br>
Most widgets (buttons, listboxes, sliders, etc) are created as
children of a window, using one of the methods of the following record
type:
<pre>
-- top level windows

struct Window < BasicWindow WindowOpt, ManagedWindow

struct BasicWindow a < ConfWidget a, Container =
    button      :: [ButtonOpt]      -> Request Button
    canvas      :: [CanvasOpt]      -> Request Canvas
    checkButton :: [CheckButtonOpt] -> Request CheckButton
    entry       :: [EntryOpt]       -> Request Entry
    frame       :: [FrameOpt]       -> Request Frame
    label       :: [LabelOpt]       -> Request Label
    listBox     :: [ListBoxOpt]     -> Request ListBox
    menuButton  :: [MenuButtonOpt]  -> Request MenuButton
    radioButton :: [RadioButtonOpt] -> Request RadioButton
    scrollBar   :: [ScrollBarOpt]   -> Request ScrollBar
    slider      :: [SliderOpt]      -> Request Slider
    textEditor  :: [TextEditorOpt]  -> Request TextEditor
 
type Pos = (Int,Int)

struct ManagedWindow = 
    getGeometry :: Request (Pos,Pos)   -- size,position
    setSize     :: Pos -> Action
    setPosition :: Pos -> Action
    iconify     :: Action
    deiconify   :: Action

</pre>
A <tt>Window</tt> is thus both a <tt>BasicWindow</tt> (with the
possibility to contain various widgets) and a <tt>ManagedWindow</tt>
(with methods to interact with the window manager).
Before creating window widgets, we must already have created a window,
using a method in the <a href=#tkenv>Tk Environment</a>.
<p>
We give here only a brief summary of the window widget types; for
details see the module excerpts page.
<ul>
  <li> Button. A widget that displays a text string or an image and
       that among its options has <tt>Command</tt>.
  <li> Canvas. A widget that can be used to display various geometrical
       shapes, images and text strings, all freely positioned on the
       canvas and all widgets in their own right (i.e. actions can be
       bound to events on individual canvas items).
  <li> CheckButton. A Button with an optional indicator that maintains
       an on/off state that can be set and read by the program.
  <li> Entry. A widget used for one-line text entry by the user; text
       can be set and read by the program.
  <li> Frame. A widget that can contain other widgets, since it is a
       <tt>BasicWindow</tt>, thus forming a compound widget.
  <li> Label. A widget that holds a text string or an image without
       additional behaviour.
  <li> ListBox. A widget that shows a list of one-line text strings
       that can be selected by the user. Two modes are available for
       multiple or single selection. Selections can be set and read by
       the program.
  <li> MenuButton. A widget which is shown as a button, but which can
       be associated with a menu, which is popped up when the user
       clicks on the menubutton.
  <li> RadioButton. A Button with an associated
       indicator. Radiobuttons can be assembled in radio groups, so
       that at most one button in each group can be selected. The
       selection can be set and read by the program.
  <li> ScrollBar. A widget that can be attached to and allow for navigation
       in some widgets (canvases, entries, listboxes, texteditors)
       when they are shown only partly on screen. Scrollbars
       can be horizontal and vertical (except for entries that can
       have only horizontal scrollbars attached). The use of
       scrollbars is quite cumbersome; more programmer-friendly tools
       are provided in the module <tt>Tix</tt>.
  <li> Slider. A widget for rapid selection of a value in an interval
       of integers. Selected value can be set and read by the program.
  <li> TextEditor. A widget that allows multiple-line editing of text.
       The content can be read and set on a line-by-line basis.
</ul>

<a name=tkenv><h4>The Tk environment</h4></a><br>
The primitive Tk environment is an instance of 
<pre>
struct Tk =
    window    :: [WindowOpt]   -> Request Window
    bitmap    :: [BitmapOpt]   -> Request ConfBitmap
    photo     :: [PhotoOpt]    -> Request Photo
    delay     :: Int -> (String -> Cmd ()) -> Request String
    periodic  :: Int -> Cmd () -> Request Runnable
    bell      :: Action

struct TkEnv < Tk, StdEnv

struct Runnable = 
   start :: Action
   stop  :: Action

</pre>
The method <tt>window</tt> creates a new top-level window. The window
is not mapped onto screen until it is packed. <tt>bitmap</tt> creates
a new bitmap (with only a foreground and a background color)
read from a file (given by the <tt>File</tt> option);
<tt>photo</tt> creates a full-color image, again from a file in GIF or
PPM format. <tt>delay</tt> schedules a procedure for execution after a
prescribed number of milliseconds, while <tt>periodic</tt> returns an
object that schedules a procedure for periodic execution with
prescribed time interval between executions. Both these two methods
return immediately. Finally, <tt>bell</tt> just rings the bell.

<a name=layout><h4>Layout</h4></a><br>
Creating a window and various widgets in the window does not cause the
window to be displayed on the screen. This is instead achieved by
<em>packing</em> the widgets of the window, thereby specifying their
layout. Sometimes one wants to build a reusable compound component with
a predefined layout. This is achieved by creating the ingredients of
the component in a <tt>Frame</tt> of the window, which allows them to
be packed separately. This is used in <tt>TkUtil</tt>,
where functions that build e.g. menu bars and radio groups are provided.

<p>
Layout, i.e. the relative placement of widgets within the window is
prescribed by using a collection of functions working on
<tt>Packable</tt> objects. Packable objects are all window widgets
and, additionally, <em>rows</em> and <em>columns</em> of packable
objects. Thus a <em>Packable</em> has a tree structure with window
widgets at the leaves.

Packables are built using 
<pre>
row, col :: [Packable] -> Packable
</pre>
with the common shorthand combinators
<pre>
p1 <<< p2 = row [p1,p2]
p1 ^^^ p2 = col [p1,p2]
</pre>
Every window widget has
a preferred size (width and height), which is determined either by the
size of its content (text, image, etc) or by explicit setting of
<tt>Width</tt> and <tt>Height</tt> options.
It is then easy to determine a preferred size for a row (column)
of widgets; just add their widths (heights) and take the maximum of their
heights (widths). Packing components created in a window for some packable
<tt>p</tt> causes the window's size to be set to the preferred size
of <tt>p</tt>, the components to be laid out and the window to be
mapped on the screen. Accordingly, windows do not support the
<tt>Width</tt> and <tt>Height</tt> options.
<p>
The above description is adequate when all the packables fit perfectly
in the assigned window size. In general, extra space for a packable
may be available in both dimensions. By default, widgets expand to
fill this space. for situations when this is not appropriate, further
functions are provided.

</body> </html>
