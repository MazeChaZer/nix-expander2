let pkgs = import (builtins.fetchTarball {
        name = "nixos-18.03-3e1be2206b4c1eb3299fb633b8ce9f5ac1c32898";
        url = "https://github.com/nixos/nixpkgs/archive/3e1be2206b4c1eb3299fb633b8ce9f5ac1c32898.tar.gz";
        sha256 = "11d01fdb4d1avi7564h7w332h3jjjadsqwzfzpyh5z56s6rfksxc";
    }) {};
in rec {
    ohugs = pkgs.stdenv.mkDerivation {
        name = "expander2";
        src = source-code/ohexp/src;
        hardeningDisable = [ "all" ];
        patches = [ ./nix-compatibility.patch ];
        buildInputs = with pkgs; [
            bison
            readline
            tcl
            tk
            ncurses
            xorg.libX11
        ];
        buildPhase = ''
            make
        '';
        installPhase = ''
            install -D --target-directory=$out/bin/ ohugs rohugs
        '';
    };

    expander2 = pkgs.writeShellScriptBin "expander2" ''
        set -euo pipefail
        cd ${./source-code/ohexp/Expander2}
        ${ohugs}/bin/rohugs Ecom.hs
    '';
}
