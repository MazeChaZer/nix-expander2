# nix-expander2

Nix packaging for the
[Expander2](https://fldit-www.cs.uni-dortmund.de/~peter/ExpNeu/Welcome.html)
workbench.

## Using the Package

Run this command to install the package or update to the latest packaged
version:

```
nix-env -f https://gitlab.com/MazeChaZer/nix-expander2/-/archive/master/nix-expander2-master.tar.gz -iA expander2
```

To uninstall the package, simply run

```
nix-env -e expander2
```

## Developing on the Package

The default.nix offers two derivations that can be selected. `ohugs`, the
O'Hugs interpreter used to run Expander2 and `expander2`, a launch script
that bundles O'Hugs with the Expander2 source files. The package is currently
built on the NixOS 18.03 release because newer Linux distributions seem to
cause problems.

### Update Upstream Sources

The source code of Expander2 has been included in this repository to make the
builds reproducible, as the provided upstream sources often change. It is
obtained from the [Expander2 Download
Page](https://fldit-www.cs.uni-dortmund.de/~peter/ExpNeu/Download.html). To
update the Expander2 sources, simply run `./fetch-upstream`. This will delete
the source-code/ directory and replace it with the latest Expander2 sources
published on the official website.

### Working on the Patch File

To make Expander2 compile in a Nix derivation, the patch
nix-compatibility.patch is needed. To work on this patch file, simply run
`./work-on-patch-file`. The script will apply the patch to the source-code/
directory and drop you in a nix-shell where O'Hugs can be compiled. When
you're done, simply exit the shell and all your changes will be written back
into the patch file and the source-code/ directory will be cleaned.

## License

### Content of the source-code/ Directory

Copyright (c) peter.padawitz@udo.edu, May 3, 2020

### Everything Else

MIT License

Copyright (c) 2020 Jonas Schürmann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
